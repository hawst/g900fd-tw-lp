.class public final Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InAppPurchaseInformationResponseProto"
.end annotation


# instance fields
.field public purchaseResult:Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

.field public signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

.field public statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4133
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4134
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    .line 4135
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4138
    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    .line 4139
    invoke-static {}, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;->emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    .line 4140
    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->purchaseResult:Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    .line 4141
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->cachedSize:I

    .line 4142
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 4167
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 4168
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    if-eqz v3, :cond_0

    .line 4169
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4172
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 4173
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 4174
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    aget-object v0, v3, v1

    .line 4175
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    if-eqz v0, :cond_1

    .line 4176
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4173
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4181
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    .end local v1    # "i":I
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->purchaseResult:Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    if-eqz v3, :cond_3

    .line 4182
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->purchaseResult:Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4185
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 4193
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4194
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4198
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4199
    :sswitch_0
    return-object p0

    .line 4204
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    if-nez v5, :cond_1

    .line 4205
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    .line 4207
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4211
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4213
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    if-nez v5, :cond_3

    move v1, v4

    .line 4214
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    .line 4216
    .local v2, "newArray":[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    if-eqz v1, :cond_2

    .line 4217
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4219
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 4220
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;-><init>()V

    aput-object v5, v2, v1

    .line 4221
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4222
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4219
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4213
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    array-length v1, v5

    goto :goto_1

    .line 4225
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    :cond_4
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;-><init>()V

    aput-object v5, v2, v1

    .line 4226
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4227
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    goto :goto_0

    .line 4231
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->purchaseResult:Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    if-nez v5, :cond_5

    .line 4232
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->purchaseResult:Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    .line 4234
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->purchaseResult:Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 4194
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4107
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4148
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    if-eqz v2, :cond_0

    .line 4149
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->signedResponse:Lcom/google/android/finsky/protos/VendingProtos$SignedDataProto;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4151
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 4152
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 4153
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->statusBarNotification:[Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;

    aget-object v0, v2, v1

    .line 4154
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    if-eqz v0, :cond_1

    .line 4155
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4152
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4159
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$StatusBarNotificationProto;
    .end local v1    # "i":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->purchaseResult:Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    if-eqz v2, :cond_3

    .line 4160
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;->purchaseResult:Lcom/google/android/finsky/protos/VendingProtos$PurchaseResultProto;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4162
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4163
    return-void
.end method
