.class public final Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AssetInstallState"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;


# instance fields
.field public assetId:Ljava/lang/String;

.field public assetReferrer:Ljava/lang/String;

.field public assetState:I

.field public hasAssetId:Z

.field public hasAssetReferrer:Z

.field public hasAssetState:Z

.field public hasInstallTime:Z

.field public hasPackageName:Z

.field public hasUninstallTime:Z

.field public hasVersionCode:Z

.field public installTime:J

.field public packageName:Ljava/lang/String;

.field public uninstallTime:J

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->clear()Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    .line 72
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    .locals 2

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    if-nez v0, :cond_1

    .line 32
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 34
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    sput-object v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    .line 37
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 75
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetId:Ljava/lang/String;

    .line 76
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetId:Z

    .line 77
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetState:I

    .line 78
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetState:Z

    .line 79
    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->installTime:J

    .line 80
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasInstallTime:Z

    .line 81
    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->uninstallTime:J

    .line 82
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasUninstallTime:Z

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->packageName:Ljava/lang/String;

    .line 84
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasPackageName:Z

    .line 85
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->versionCode:I

    .line 86
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasVersionCode:Z

    .line 87
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetReferrer:Ljava/lang/String;

    .line 88
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetReferrer:Z

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->cachedSize:I

    .line 90
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 122
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 123
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetId:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 124
    :cond_0
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_1
    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetState:Z

    if-eqz v1, :cond_3

    .line 128
    :cond_2
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetState:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasInstallTime:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->installTime:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 132
    :cond_4
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->installTime:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasUninstallTime:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->uninstallTime:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 136
    :cond_6
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->uninstallTime:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasPackageName:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->packageName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 140
    :cond_8
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->packageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasVersionCode:Z

    if-nez v1, :cond_a

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->versionCode:I

    if-eqz v1, :cond_b

    .line 144
    :cond_a
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetReferrer:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetReferrer:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 148
    :cond_c
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetReferrer:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    :cond_d
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 159
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 160
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 164
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 165
    :sswitch_0
    return-object p0

    .line 170
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetId:Ljava/lang/String;

    .line 171
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetId:Z

    goto :goto_0

    .line 175
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 176
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 189
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetState:I

    .line 190
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetState:Z

    goto :goto_0

    .line 196
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->installTime:J

    .line 197
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasInstallTime:Z

    goto :goto_0

    .line 201
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->uninstallTime:J

    .line 202
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasUninstallTime:Z

    goto :goto_0

    .line 206
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->packageName:Ljava/lang/String;

    .line 207
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasPackageName:Z

    goto :goto_0

    .line 211
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->versionCode:I

    .line 212
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasVersionCode:Z

    goto :goto_0

    .line 216
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetReferrer:Ljava/lang/String;

    .line 217
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetReferrer:Z

    goto :goto_0

    .line 160
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x20 -> :sswitch_2
        0x28 -> :sswitch_3
        0x30 -> :sswitch_4
        0x3a -> :sswitch_5
        0x40 -> :sswitch_6
        0x4a -> :sswitch_7
    .end sparse-switch

    .line 176
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 96
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetId:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 97
    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 99
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetState:Z

    if-eqz v0, :cond_3

    .line 100
    :cond_2
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetState:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 102
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasInstallTime:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->installTime:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 103
    :cond_4
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->installTime:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 105
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasUninstallTime:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->uninstallTime:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 106
    :cond_6
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->uninstallTime:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 108
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasPackageName:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->packageName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 109
    :cond_8
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 111
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasVersionCode:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->versionCode:I

    if-eqz v0, :cond_b

    .line 112
    :cond_a
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 114
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->hasAssetReferrer:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetReferrer:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 115
    :cond_c
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$AssetInstallState;->assetReferrer:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 117
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 118
    return-void
.end method
