.class public Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayEditorialAppCardView.java"

# interfaces
.implements Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method


# virtual methods
.method public getCardType()I
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x5

    return v0
.end method

.method public getImageTypePreference()[I
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->PROMO_IMAGE_TYPES:[I

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 43
    const/high16 v0, 0x3efa0000    # 0.48828125f

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->mThumbnailAspectRatio:F

    .line 44
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayEditorialAppCardView;->measureThumbnailSpanningWidth(I)V

    .line 46
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;->onMeasure(II)V

    .line 47
    return-void
.end method
