.class public final Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UploadDeviceConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/UploadDeviceConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UploadDeviceConfigResponse"
.end annotation


# instance fields
.field public hasUploadDeviceConfigToken:Z

.field public uploadDeviceConfigToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 153
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->clear()Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    .line 154
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;
    .locals 1

    .prologue
    .line 157
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->uploadDeviceConfigToken:Ljava/lang/String;

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->hasUploadDeviceConfigToken:Z

    .line 159
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->cachedSize:I

    .line 160
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 174
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 175
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->hasUploadDeviceConfigToken:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->uploadDeviceConfigToken:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 176
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->uploadDeviceConfigToken:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 188
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 192
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 193
    :sswitch_0
    return-object p0

    .line 198
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->uploadDeviceConfigToken:Ljava/lang/String;

    .line 199
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->hasUploadDeviceConfigToken:Z

    goto :goto_0

    .line 188
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->hasUploadDeviceConfigToken:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->uploadDeviceConfigToken:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;->uploadDeviceConfigToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 169
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 170
    return-void
.end method
