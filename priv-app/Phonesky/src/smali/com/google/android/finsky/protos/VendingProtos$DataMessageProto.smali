.class public final Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DataMessageProto"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;


# instance fields
.field public appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

.field public category:Ljava/lang/String;

.field public hasCategory:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4763
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4764
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    .line 4765
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    .locals 2

    .prologue
    .line 4745
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    if-nez v0, :cond_1

    .line 4746
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 4748
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    if-nez v0, :cond_0

    .line 4749
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    sput-object v0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    .line 4751
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4753
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    return-object v0

    .line 4751
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    .locals 1

    .prologue
    .line 4768
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->category:Ljava/lang/String;

    .line 4769
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->hasCategory:Z

    .line 4770
    invoke-static {}, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    .line 4771
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->cachedSize:I

    .line 4772
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 4794
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 4795
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->hasCategory:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->category:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 4796
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->category:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4799
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 4800
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 4801
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    aget-object v0, v3, v1

    .line 4802
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    if-eqz v0, :cond_2

    .line 4803
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4800
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4808
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    .end local v1    # "i":I
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 4816
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4817
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4821
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4822
    :sswitch_0
    return-object p0

    .line 4827
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->category:Ljava/lang/String;

    .line 4828
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->hasCategory:Z

    goto :goto_0

    .line 4832
    :sswitch_2
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4834
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    if-nez v5, :cond_2

    move v1, v4

    .line 4835
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    .line 4837
    .local v2, "newArray":[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    if-eqz v1, :cond_1

    .line 4838
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4840
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 4841
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;-><init>()V

    aput-object v5, v2, v1

    .line 4842
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4843
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4840
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4834
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    array-length v1, v5

    goto :goto_1

    .line 4846
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;-><init>()V

    aput-object v5, v2, v1

    .line 4847
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4848
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    goto :goto_0

    .line 4817
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4739
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4778
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->hasCategory:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->category:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 4779
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->category:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4781
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 4782
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 4783
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->appData:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    aget-object v0, v2, v1

    .line 4784
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    if-eqz v0, :cond_2

    .line 4785
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4782
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4789
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4790
    return-void
.end method
