.class public Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;
.super Landroid/widget/LinearLayout;
.source "WarmWelcomeCardButton.java"

# interfaces
.implements Lcom/google/android/finsky/adapters/Recyclable;


# instance fields
.field private mIcon:Lcom/google/android/play/image/FifeImageView;

.field private mText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method


# virtual methods
.method public alignCenter()V
    .locals 1

    .prologue
    .line 56
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->setGravity(I)V

    .line 57
    return-void
.end method

.method public alignStart()V
    .locals 1

    .prologue
    .line 52
    const v0, 0x800013

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->setGravity(I)V

    .line 53
    return-void
.end method

.method public configure(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "icon"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->mText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    if-eqz p2, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->mIcon:Lcom/google/android/play/image/FifeImageView;

    iget-object v1, p2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v2, p2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v0, v1, v2, p3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->mIcon:Lcom/google/android/play/image/FifeImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->mIcon:Lcom/google/android/play/image/FifeImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 37
    const v0, 0x7f0a0081

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->mIcon:Lcom/google/android/play/image/FifeImageView;

    .line 38
    const v0, 0x7f0a0082

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->mText:Landroid/widget/TextView;

    .line 39
    return-void
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->mIcon:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 62
    return-void
.end method
