.class public Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;
.super Ljava/lang/Object;
.source "RecommendationsViewFactory.java"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# instance fields
.field private final mAppWidgetId:I

.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private final mContentHeightLandDp:I

.field private final mContentHeightPortDp:I

.field private final mContext:Landroid/content/Context;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

.field private final mLibrary:Lcom/google/android/finsky/library/Library;

.field private final mMaxImageHeight:I

.field private final mTypeMap:Lcom/google/android/finsky/widget/WidgetTypeMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;III)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetId"    # I
    .param p3, "heightLandDp"    # I
    .param p4, "heightPortDp"    # I

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    .line 78
    iput p2, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mAppWidgetId:I

    .line 79
    iput p3, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContentHeightLandDp:I

    .line 80
    iput p4, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContentHeightPortDp:I

    .line 81
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mLibrary:Lcom/google/android/finsky/library/Library;

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0188

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mMaxImageHeight:I

    .line 84
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 85
    invoke-static {p1}, Lcom/google/android/finsky/widget/WidgetTypeMap;->get(Landroid/content/Context;)Lcom/google/android/finsky/widget/WidgetTypeMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mTypeMap:Lcom/google/android/finsky/widget/WidgetTypeMap;

    .line 86
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mAppWidgetId:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;)Lcom/google/android/finsky/widget/WidgetTypeMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mTypeMap:Lcom/google/android/finsky/widget/WidgetTypeMap;

    return-object v0
.end method

.method private getChildViewAt(III)Landroid/widget/RemoteViews;
    .locals 15
    .param p1, "position"    # I
    .param p2, "orientation"    # I
    .param p3, "widgetBackend"    # I

    .prologue
    .line 140
    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    move/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->get(I)Lcom/google/android/finsky/widget/recommendation/Recommendation;

    move-result-object v9

    .line 141
    .local v9, "item":Lcom/google/android/finsky/widget/recommendation/Recommendation;
    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget v3, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mMaxImageHeight:I

    invoke-static {v1, v9, v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getBitmap(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/widget/recommendation/Recommendation;I)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 143
    .local v13, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v9}, Lcom/google/android/finsky/widget/recommendation/Recommendation;->getImageType()I

    move-result v2

    .line 144
    .local v2, "imageType":I
    move/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->getLastMeasuredWidth(I)I

    move-result v5

    .line 146
    .local v5, "widthInDips":I
    const/4 v1, 0x2

    move/from16 v0, p2

    if-ne v0, v1, :cond_0

    iget v6, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContentHeightLandDp:I

    .line 148
    .local v6, "contentHeight":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Lcom/google/android/finsky/widget/recommendation/Recommendation;->getBackend()I

    move-result v4

    move/from16 v3, p3

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->getLayoutRes(Landroid/content/Context;IIIII)I

    move-result v14

    .line 151
    .local v14, "layoutRes":I
    new-instance v8, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v8, v1, v14}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 153
    .local v8, "child":Landroid/widget/RemoteViews;
    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    invoke-static {v1, v8, v9, v13, v14}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->populateItem(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/finsky/widget/recommendation/Recommendation;Landroid/graphics/Bitmap;I)V

    .line 154
    iget-object v7, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    iget v11, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mAppWidgetId:I

    move/from16 v10, p1

    move/from16 v12, p3

    invoke-static/range {v7 .. v12}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->setIntents(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/finsky/widget/recommendation/Recommendation;III)V

    .line 155
    return-object v8

    .line 146
    .end local v6    # "contentHeight":I
    .end local v8    # "child":Landroid/widget/RemoteViews;
    .end local v14    # "layoutRes":I
    :cond_0
    iget v6, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContentHeightPortDp:I

    goto :goto_0
.end method

.method private getLastMeasuredWidth(I)I
    .locals 3
    .param p1, "orientation"    # I

    .prologue
    .line 184
    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    iget v2, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mAppWidgetId:I

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetOptions(I)Landroid/os/Bundle;

    move-result-object v0

    .line 185
    .local v0, "result":Landroid/os/Bundle;
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const-string v1, "appWidgetMinWidth"

    :goto_0
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    return v1

    :cond_0
    const-string v1, "appWidgetMaxWidth"

    goto :goto_0
.end method

.method private static getLayoutRes(Landroid/content/Context;IIIII)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imageType"    # I
    .param p2, "widgetBackend"    # I
    .param p3, "itemBackend"    # I
    .param p4, "widthInDips"    # I
    .param p5, "contentHeightInDips"    # I

    .prologue
    const v2, 0x7f04017f

    const v4, 0x7f04017e

    const v1, 0x7f04017a

    const v3, 0x7f040180

    .line 259
    const-wide/high16 v6, 0x4004000000000000L    # 2.5

    int-to-double v8, p5

    mul-double/2addr v6, v8

    double-to-int v0, v6

    .line 261
    .local v0, "promoBreakpointDips":I
    packed-switch p2, :pswitch_data_0

    .line 303
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid backend: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 263
    :pswitch_1
    const/4 v4, 0x2

    if-ne p1, v4, :cond_2

    .line 264
    if-ge p4, v0, :cond_1

    .line 301
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 267
    goto :goto_0

    :cond_2
    move v1, v3

    .line 270
    goto :goto_0

    .line 274
    :pswitch_2
    packed-switch p3, :pswitch_data_1

    .line 282
    :pswitch_3
    packed-switch p1, :pswitch_data_2

    move v1, v3

    .line 290
    goto :goto_0

    :pswitch_4
    move v1, v3

    .line 276
    goto :goto_0

    :pswitch_5
    move v1, v4

    .line 280
    goto :goto_0

    .line 284
    :pswitch_6
    if-lt p4, v0, :cond_0

    move v1, v2

    .line 287
    goto :goto_0

    :pswitch_7
    move v1, v3

    .line 296
    goto :goto_0

    :pswitch_8
    move v1, v4

    .line 301
    goto :goto_0

    .line 261
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_8
        :pswitch_7
        :pswitch_1
        :pswitch_8
        :pswitch_0
        :pswitch_8
    .end packed-switch

    .line 274
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_5
    .end packed-switch

    .line 282
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_6
    .end packed-switch
.end method

.method private getRecommendationItems(I)Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    .locals 4
    .param p1, "widgetBackend"    # I

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget v2, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mAppWidgetId:I

    iget-object v3, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mLibrary:Lcom/google/android/finsky/library/Library;

    invoke-static {v0, v1, p1, v2, v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecommendationsOrShowError(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;IILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method private getWidgetBackend()I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 191
    new-instance v1, Ljava/util/concurrent/Semaphore;

    invoke-direct {v1, v5}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 193
    .local v1, "lock":Ljava/util/concurrent/Semaphore;
    const/4 v3, 0x1

    new-array v0, v3, [I

    .line 194
    .local v0, "backend":[I
    new-instance v2, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory$1;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory$1;-><init>(Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;[ILjava/util/concurrent/Semaphore;)V

    .line 207
    .local v2, "runnable":Ljava/lang/Runnable;
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v4

    if-eq v3, v4, :cond_0

    .line 210
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v3, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 211
    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 214
    :cond_0
    aget v3, v0, v5

    return v3
.end method

.method public static varargs notifyDataSetChanged(Landroid/content/Context;[I)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetIds"    # [I

    .prologue
    .line 310
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 312
    .local v0, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 313
    :cond_0
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object p1

    .line 317
    :cond_1
    const v1, 0x7f0a0335

    invoke-virtual {v0, p1, v1}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    .line 318
    return-void
.end method

.method private static populateItem(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/finsky/widget/recommendation/Recommendation;Landroid/graphics/Bitmap;I)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "item"    # Lcom/google/android/finsky/widget/recommendation/Recommendation;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "layoutRes"    # I

    .prologue
    const v5, 0x7f0a028b

    .line 220
    invoke-virtual {p2}, Lcom/google/android/finsky/widget/recommendation/Recommendation;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 222
    .local v0, "document":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v5, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 223
    const v2, 0x7f0a033e

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 224
    const v2, 0x7f0a033f

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDescriptionReason()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 225
    const v2, 0x7f0a033c

    invoke-virtual {p1, v2, p3}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 226
    const v2, 0x7f0a033a

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getRecommendationWidgetStripResourceId(I)I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 229
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getTitleContentDescriptionResourceId(Landroid/content/res/Resources;I)I

    move-result v1

    .line 232
    .local v1, "titleDescriptionResourceId":I
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v5, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 234
    return-void
.end method

.method private static setIntents(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/google/android/finsky/widget/recommendation/Recommendation;III)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "item"    # Lcom/google/android/finsky/widget/recommendation/Recommendation;
    .param p3, "itemIndex"    # I
    .param p4, "appWidgetId"    # I
    .param p5, "widgetBackendId"    # I

    .prologue
    .line 238
    invoke-virtual {p2}, Lcom/google/android/finsky/widget/recommendation/Recommendation;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    invoke-static {p0, v3, p5, p4}, Lcom/google/android/finsky/widget/recommendation/OpenRecommendationReceiver;->getIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;II)Landroid/app/PendingIntent;

    move-result-object v2

    .line 240
    .local v2, "pendingView":Landroid/app/PendingIntent;
    const v3, 0x7f0a0343

    invoke-virtual {p1, v3, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 242
    invoke-static {p0, p4}, Lcom/google/android/finsky/widget/AdvanceFlipperReceiver;->getIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 243
    .local v0, "advance":Landroid/app/PendingIntent;
    const v3, 0x7f0a0344

    invoke-virtual {p1, v3, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 245
    invoke-virtual {p2}, Lcom/google/android/finsky/widget/recommendation/Recommendation;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    invoke-static {p0, p4, v3, p5, p3}, Lcom/google/android/finsky/services/DismissRecommendationService;->getDismissPendingIntent(Landroid/content/Context;ILcom/google/android/finsky/api/model/Document;II)Landroid/app/PendingIntent;

    move-result-object v1

    .line 248
    .local v1, "pendingDismiss":Landroid/app/PendingIntent;
    const v3, 0x7f0a033d

    invoke-virtual {p1, v3, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 249
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    invoke-virtual {v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 170
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 160
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f04017b

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 115
    invoke-direct {p0}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->getWidgetBackend()I

    move-result v0

    .line 116
    .local v0, "widgetBackend":I
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 117
    iget-object v2, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    const-class v3, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;

    new-array v4, v5, [I

    iget v5, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mAppWidgetId:I

    aput v5, v4, v6

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/widget/BaseWidgetProvider;->update(Landroid/content/Context;Ljava/lang/Class;[I)V

    .line 134
    :cond_0
    :goto_0
    return-object v1

    .line 120
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    if-nez v2, :cond_2

    .line 121
    invoke-direct {p0, v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->getRecommendationItems(I)Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    .line 122
    iget-object v2, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    if-eqz v2, :cond_0

    .line 127
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    invoke-virtual {v2}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->size()I

    move-result v2

    if-lt p1, v2, :cond_3

    .line 130
    const-string v2, "Item out of bounds (pos = %d, size = %d)"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    invoke-virtual {v4}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 134
    :cond_3
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-direct {p0, p1, v3, v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->getChildViewAt(III)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-direct {p0, p1, v5, v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->getChildViewAt(III)Landroid/widget/RemoteViews;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x4

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method public onDataSetChanged()V
    .locals 6

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->getWidgetBackend()I

    move-result v0

    .line 95
    .local v0, "widgetBackend":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mContext:Landroid/content/Context;

    const-class v2, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    iget v5, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mAppWidgetId:I

    aput v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/widget/BaseWidgetProvider;->update(Landroid/content/Context;Ljava/lang/Class;[I)V

    .line 102
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 101
    invoke-direct {p0, v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->getRecommendationItems(I)Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->mItems:Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 106
    return-void
.end method
