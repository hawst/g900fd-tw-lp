.class public final Lcom/google/android/finsky/protos/Response$Payload;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Payload"
.end annotation


# instance fields
.field public acceptTosResponse:Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

.field public ackNotificationResponse:Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

.field public backupDeviceChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

.field public backupDocumentChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

.field public billingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

.field public browseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

.field public bulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

.field public buyResponse:Lcom/google/android/finsky/protos/Buy$BuyResponse;

.field public challengeResponse:Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

.field public checkIabPromoResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

.field public checkInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

.field public checkPromoOfferResponse:Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

.field public commitPurchaseResponse:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

.field public consumePurchaseResponse:Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

.field public createInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

.field public debugSettingsResponse:Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

.field public deliveryResponse:Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

.field public detailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

.field public earlyUpdateResponse:Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

.field public flagContentResponse:Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

.field public getInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

.field public initiateAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

.field public instrumentSetupInfoResponse:Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

.field public libraryReplicationResponse:Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

.field public listResponse:Lcom/google/android/finsky/protos/DocList$ListResponse;

.field public logResponse:Lcom/google/android/finsky/protos/Log$LogResponse;

.field public modifyLibraryResponse:Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

.field public myAccountResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

.field public plusOneResponse:Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

.field public plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

.field public preloadsResponse:Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

.field public preparePurchaseResponse:Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

.field public purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

.field public rateSuggestedContentResponse:Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

.field public recordUserActivityResponse:Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

.field public redeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

.field public redeemGiftCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

.field public resolveLinkResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

.field public reviewResponse:Lcom/google/android/finsky/protos/Rev$ReviewResponse;

.field public revokeResponse:Lcom/google/android/finsky/protos/RevokeResponse;

.field public searchResponse:Lcom/google/android/finsky/protos/Search$SearchResponse;

.field public searchSuggestResponse:Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

.field public selfUpdateResponse:Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

.field public tocResponse:Lcom/google/android/finsky/protos/Toc$TocResponse;

.field public updateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

.field public uploadDeviceConfigResponse:Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

.field public userActivitySettingsResponse:Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

.field public verifyAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 392
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 393
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$Payload;->clear()Lcom/google/android/finsky/protos/Response$Payload;

    .line 394
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Response$Payload;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 397
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->listResponse:Lcom/google/android/finsky/protos/DocList$ListResponse;

    .line 398
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->detailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    .line 399
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->reviewResponse:Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .line 400
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->buyResponse:Lcom/google/android/finsky/protos/Buy$BuyResponse;

    .line 401
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchResponse:Lcom/google/android/finsky/protos/Search$SearchResponse;

    .line 402
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->tocResponse:Lcom/google/android/finsky/protos/Toc$TocResponse;

    .line 403
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->browseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    .line 404
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    .line 405
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->updateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .line 406
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->logResponse:Lcom/google/android/finsky/protos/Log$LogResponse;

    .line 407
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    .line 408
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusOneResponse:Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    .line 409
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->flagContentResponse:Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    .line 410
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->ackNotificationResponse:Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    .line 411
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->initiateAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    .line 412
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->verifyAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    .line 413
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->libraryReplicationResponse:Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    .line 414
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->revokeResponse:Lcom/google/android/finsky/protos/RevokeResponse;

    .line 415
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->bulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    .line 416
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->resolveLinkResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 417
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->deliveryResponse:Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    .line 418
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->acceptTosResponse:Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    .line 419
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->rateSuggestedContentResponse:Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    .line 420
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkPromoOfferResponse:Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    .line 421
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->instrumentSetupInfoResponse:Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    .line 422
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemGiftCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    .line 423
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->modifyLibraryResponse:Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    .line 424
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->uploadDeviceConfigResponse:Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    .line 425
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .line 426
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->consumePurchaseResponse:Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    .line 427
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->billingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    .line 428
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->preparePurchaseResponse:Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    .line 429
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->commitPurchaseResponse:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    .line 430
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->debugSettingsResponse:Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

    .line 431
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkIabPromoResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    .line 432
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->userActivitySettingsResponse:Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    .line 433
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->recordUserActivityResponse:Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

    .line 434
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    .line 435
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->selfUpdateResponse:Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    .line 436
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchSuggestResponse:Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    .line 437
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->getInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    .line 438
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->createInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .line 439
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->challengeResponse:Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    .line 440
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDeviceChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    .line 441
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDocumentChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    .line 442
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->earlyUpdateResponse:Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    .line 443
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->preloadsResponse:Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    .line 444
    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->myAccountResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    .line 445
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->cachedSize:I

    .line 446
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 601
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 602
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->listResponse:Lcom/google/android/finsky/protos/DocList$ListResponse;

    if-eqz v1, :cond_0

    .line 603
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->listResponse:Lcom/google/android/finsky/protos/DocList$ListResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 606
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->detailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    if-eqz v1, :cond_1

    .line 607
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->detailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 610
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->reviewResponse:Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    if-eqz v1, :cond_2

    .line 611
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->reviewResponse:Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 614
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->buyResponse:Lcom/google/android/finsky/protos/Buy$BuyResponse;

    if-eqz v1, :cond_3

    .line 615
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->buyResponse:Lcom/google/android/finsky/protos/Buy$BuyResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 618
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchResponse:Lcom/google/android/finsky/protos/Search$SearchResponse;

    if-eqz v1, :cond_4

    .line 619
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchResponse:Lcom/google/android/finsky/protos/Search$SearchResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 622
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->tocResponse:Lcom/google/android/finsky/protos/Toc$TocResponse;

    if-eqz v1, :cond_5

    .line 623
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->tocResponse:Lcom/google/android/finsky/protos/Toc$TocResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 626
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->browseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    if-eqz v1, :cond_6

    .line 627
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->browseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 630
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    if-eqz v1, :cond_7

    .line 631
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 634
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->updateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    if-eqz v1, :cond_8

    .line 635
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->updateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 638
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->logResponse:Lcom/google/android/finsky/protos/Log$LogResponse;

    if-eqz v1, :cond_9

    .line 639
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->logResponse:Lcom/google/android/finsky/protos/Log$LogResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 642
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    if-eqz v1, :cond_a

    .line 643
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 646
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusOneResponse:Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    if-eqz v1, :cond_b

    .line 647
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusOneResponse:Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 650
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->flagContentResponse:Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    if-eqz v1, :cond_c

    .line 651
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->flagContentResponse:Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 654
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->ackNotificationResponse:Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    if-eqz v1, :cond_d

    .line 655
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->ackNotificationResponse:Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 658
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->initiateAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    if-eqz v1, :cond_e

    .line 659
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->initiateAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 662
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->verifyAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    if-eqz v1, :cond_f

    .line 663
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->verifyAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 666
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->libraryReplicationResponse:Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    if-eqz v1, :cond_10

    .line 667
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->libraryReplicationResponse:Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 670
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->revokeResponse:Lcom/google/android/finsky/protos/RevokeResponse;

    if-eqz v1, :cond_11

    .line 671
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->revokeResponse:Lcom/google/android/finsky/protos/RevokeResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 674
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->bulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    if-eqz v1, :cond_12

    .line 675
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->bulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 678
    :cond_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->resolveLinkResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-eqz v1, :cond_13

    .line 679
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->resolveLinkResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 682
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->deliveryResponse:Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    if-eqz v1, :cond_14

    .line 683
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->deliveryResponse:Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 686
    :cond_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->acceptTosResponse:Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    if-eqz v1, :cond_15

    .line 687
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->acceptTosResponse:Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 690
    :cond_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->rateSuggestedContentResponse:Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    if-eqz v1, :cond_16

    .line 691
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->rateSuggestedContentResponse:Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 694
    :cond_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkPromoOfferResponse:Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    if-eqz v1, :cond_17

    .line 695
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkPromoOfferResponse:Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 698
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->instrumentSetupInfoResponse:Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    if-eqz v1, :cond_18

    .line 699
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->instrumentSetupInfoResponse:Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 702
    :cond_18
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemGiftCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    if-eqz v1, :cond_19

    .line 703
    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemGiftCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 706
    :cond_19
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->modifyLibraryResponse:Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    if-eqz v1, :cond_1a

    .line 707
    const/16 v1, 0x1b

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->modifyLibraryResponse:Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 710
    :cond_1a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->uploadDeviceConfigResponse:Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    if-eqz v1, :cond_1b

    .line 711
    const/16 v1, 0x1c

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->uploadDeviceConfigResponse:Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 714
    :cond_1b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    if-eqz v1, :cond_1c

    .line 715
    const/16 v1, 0x1d

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 718
    :cond_1c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->consumePurchaseResponse:Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    if-eqz v1, :cond_1d

    .line 719
    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->consumePurchaseResponse:Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 722
    :cond_1d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->billingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    if-eqz v1, :cond_1e

    .line 723
    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->billingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 726
    :cond_1e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preparePurchaseResponse:Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    if-eqz v1, :cond_1f

    .line 727
    const/16 v1, 0x20

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->preparePurchaseResponse:Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 730
    :cond_1f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->commitPurchaseResponse:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    if-eqz v1, :cond_20

    .line 731
    const/16 v1, 0x21

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->commitPurchaseResponse:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 734
    :cond_20
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->debugSettingsResponse:Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

    if-eqz v1, :cond_21

    .line 735
    const/16 v1, 0x22

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->debugSettingsResponse:Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 738
    :cond_21
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkIabPromoResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    if-eqz v1, :cond_22

    .line 739
    const/16 v1, 0x23

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkIabPromoResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 742
    :cond_22
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->userActivitySettingsResponse:Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    if-eqz v1, :cond_23

    .line 743
    const/16 v1, 0x24

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->userActivitySettingsResponse:Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 746
    :cond_23
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->recordUserActivityResponse:Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

    if-eqz v1, :cond_24

    .line 747
    const/16 v1, 0x25

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->recordUserActivityResponse:Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 750
    :cond_24
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    if-eqz v1, :cond_25

    .line 751
    const/16 v1, 0x26

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 754
    :cond_25
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->selfUpdateResponse:Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    if-eqz v1, :cond_26

    .line 755
    const/16 v1, 0x27

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->selfUpdateResponse:Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 758
    :cond_26
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchSuggestResponse:Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    if-eqz v1, :cond_27

    .line 759
    const/16 v1, 0x28

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchSuggestResponse:Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 762
    :cond_27
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->getInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    if-eqz v1, :cond_28

    .line 763
    const/16 v1, 0x29

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->getInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 766
    :cond_28
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->createInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    if-eqz v1, :cond_29

    .line 767
    const/16 v1, 0x2a

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->createInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 770
    :cond_29
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->challengeResponse:Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    if-eqz v1, :cond_2a

    .line 771
    const/16 v1, 0x2b

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->challengeResponse:Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 774
    :cond_2a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDeviceChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    if-eqz v1, :cond_2b

    .line 775
    const/16 v1, 0x2c

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDeviceChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 778
    :cond_2b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDocumentChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    if-eqz v1, :cond_2c

    .line 779
    const/16 v1, 0x2d

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDocumentChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 782
    :cond_2c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->earlyUpdateResponse:Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    if-eqz v1, :cond_2d

    .line 783
    const/16 v1, 0x2e

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->earlyUpdateResponse:Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 786
    :cond_2d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preloadsResponse:Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    if-eqz v1, :cond_2e

    .line 787
    const/16 v1, 0x2f

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->preloadsResponse:Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 790
    :cond_2e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->myAccountResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    if-eqz v1, :cond_2f

    .line 791
    const/16 v1, 0x30

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$Payload;->myAccountResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 794
    :cond_2f
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Response$Payload;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 802
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 803
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 807
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 808
    :sswitch_0
    return-object p0

    .line 813
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->listResponse:Lcom/google/android/finsky/protos/DocList$ListResponse;

    if-nez v1, :cond_1

    .line 814
    new-instance v1, Lcom/google/android/finsky/protos/DocList$ListResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocList$ListResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->listResponse:Lcom/google/android/finsky/protos/DocList$ListResponse;

    .line 816
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->listResponse:Lcom/google/android/finsky/protos/DocList$ListResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 820
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->detailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    if-nez v1, :cond_2

    .line 821
    new-instance v1, Lcom/google/android/finsky/protos/Details$DetailsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Details$DetailsResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->detailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    .line 823
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->detailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 827
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->reviewResponse:Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    if-nez v1, :cond_3

    .line 828
    new-instance v1, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Rev$ReviewResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->reviewResponse:Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .line 830
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->reviewResponse:Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 834
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->buyResponse:Lcom/google/android/finsky/protos/Buy$BuyResponse;

    if-nez v1, :cond_4

    .line 835
    new-instance v1, Lcom/google/android/finsky/protos/Buy$BuyResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Buy$BuyResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->buyResponse:Lcom/google/android/finsky/protos/Buy$BuyResponse;

    .line 837
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->buyResponse:Lcom/google/android/finsky/protos/Buy$BuyResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 841
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchResponse:Lcom/google/android/finsky/protos/Search$SearchResponse;

    if-nez v1, :cond_5

    .line 842
    new-instance v1, Lcom/google/android/finsky/protos/Search$SearchResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Search$SearchResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchResponse:Lcom/google/android/finsky/protos/Search$SearchResponse;

    .line 844
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchResponse:Lcom/google/android/finsky/protos/Search$SearchResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 848
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->tocResponse:Lcom/google/android/finsky/protos/Toc$TocResponse;

    if-nez v1, :cond_6

    .line 849
    new-instance v1, Lcom/google/android/finsky/protos/Toc$TocResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Toc$TocResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->tocResponse:Lcom/google/android/finsky/protos/Toc$TocResponse;

    .line 851
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->tocResponse:Lcom/google/android/finsky/protos/Toc$TocResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 855
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->browseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    if-nez v1, :cond_7

    .line 856
    new-instance v1, Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Browse$BrowseResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->browseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    .line 858
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->browseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 862
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    if-nez v1, :cond_8

    .line 863
    new-instance v1, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    .line 865
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 869
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->updateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    if-nez v1, :cond_9

    .line 870
    new-instance v1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->updateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .line 872
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->updateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 876
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->logResponse:Lcom/google/android/finsky/protos/Log$LogResponse;

    if-nez v1, :cond_a

    .line 877
    new-instance v1, Lcom/google/android/finsky/protos/Log$LogResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Log$LogResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->logResponse:Lcom/google/android/finsky/protos/Log$LogResponse;

    .line 879
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->logResponse:Lcom/google/android/finsky/protos/Log$LogResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 883
    :sswitch_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    if-nez v1, :cond_b

    .line 884
    new-instance v1, Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    .line 886
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 890
    :sswitch_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusOneResponse:Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    if-nez v1, :cond_c

    .line 891
    new-instance v1, Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusOneResponse:Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    .line 893
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusOneResponse:Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 897
    :sswitch_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->flagContentResponse:Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    if-nez v1, :cond_d

    .line 898
    new-instance v1, Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->flagContentResponse:Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    .line 900
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->flagContentResponse:Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 904
    :sswitch_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->ackNotificationResponse:Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    if-nez v1, :cond_e

    .line 905
    new-instance v1, Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->ackNotificationResponse:Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    .line 907
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->ackNotificationResponse:Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 911
    :sswitch_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->initiateAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    if-nez v1, :cond_f

    .line 912
    new-instance v1, Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->initiateAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    .line 914
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->initiateAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 918
    :sswitch_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->verifyAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    if-nez v1, :cond_10

    .line 919
    new-instance v1, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->verifyAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    .line 921
    :cond_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->verifyAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 925
    :sswitch_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->libraryReplicationResponse:Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    if-nez v1, :cond_11

    .line 926
    new-instance v1, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->libraryReplicationResponse:Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    .line 928
    :cond_11
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->libraryReplicationResponse:Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 932
    :sswitch_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->revokeResponse:Lcom/google/android/finsky/protos/RevokeResponse;

    if-nez v1, :cond_12

    .line 933
    new-instance v1, Lcom/google/android/finsky/protos/RevokeResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/RevokeResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->revokeResponse:Lcom/google/android/finsky/protos/RevokeResponse;

    .line 935
    :cond_12
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->revokeResponse:Lcom/google/android/finsky/protos/RevokeResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 939
    :sswitch_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->bulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    if-nez v1, :cond_13

    .line 940
    new-instance v1, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->bulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    .line 942
    :cond_13
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->bulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 946
    :sswitch_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->resolveLinkResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-nez v1, :cond_14

    .line 947
    new-instance v1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->resolveLinkResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 949
    :cond_14
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->resolveLinkResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 953
    :sswitch_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->deliveryResponse:Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    if-nez v1, :cond_15

    .line 954
    new-instance v1, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->deliveryResponse:Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    .line 956
    :cond_15
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->deliveryResponse:Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 960
    :sswitch_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->acceptTosResponse:Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    if-nez v1, :cond_16

    .line 961
    new-instance v1, Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->acceptTosResponse:Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    .line 963
    :cond_16
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->acceptTosResponse:Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 967
    :sswitch_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->rateSuggestedContentResponse:Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    if-nez v1, :cond_17

    .line 968
    new-instance v1, Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/RateSuggestedContentResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->rateSuggestedContentResponse:Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    .line 970
    :cond_17
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->rateSuggestedContentResponse:Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 974
    :sswitch_18
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkPromoOfferResponse:Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    if-nez v1, :cond_18

    .line 975
    new-instance v1, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkPromoOfferResponse:Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    .line 977
    :cond_18
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkPromoOfferResponse:Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 981
    :sswitch_19
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->instrumentSetupInfoResponse:Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    if-nez v1, :cond_19

    .line 982
    new-instance v1, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->instrumentSetupInfoResponse:Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    .line 984
    :cond_19
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->instrumentSetupInfoResponse:Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 988
    :sswitch_1a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemGiftCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    if-nez v1, :cond_1a

    .line 989
    new-instance v1, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemGiftCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    .line 991
    :cond_1a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemGiftCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 995
    :sswitch_1b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->modifyLibraryResponse:Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    if-nez v1, :cond_1b

    .line 996
    new-instance v1, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->modifyLibraryResponse:Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    .line 998
    :cond_1b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->modifyLibraryResponse:Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1002
    :sswitch_1c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->uploadDeviceConfigResponse:Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    if-nez v1, :cond_1c

    .line 1003
    new-instance v1, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->uploadDeviceConfigResponse:Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    .line 1005
    :cond_1c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->uploadDeviceConfigResponse:Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1009
    :sswitch_1d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    if-nez v1, :cond_1d

    .line 1010
    new-instance v1, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    .line 1012
    :cond_1d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1016
    :sswitch_1e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->consumePurchaseResponse:Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    if-nez v1, :cond_1e

    .line 1017
    new-instance v1, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ConsumePurchaseResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->consumePurchaseResponse:Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    .line 1019
    :cond_1e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->consumePurchaseResponse:Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1023
    :sswitch_1f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->billingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    if-nez v1, :cond_1f

    .line 1024
    new-instance v1, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->billingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    .line 1026
    :cond_1f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->billingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1030
    :sswitch_20
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preparePurchaseResponse:Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    if-nez v1, :cond_20

    .line 1031
    new-instance v1, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preparePurchaseResponse:Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    .line 1033
    :cond_20
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preparePurchaseResponse:Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1037
    :sswitch_21
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->commitPurchaseResponse:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    if-nez v1, :cond_21

    .line 1038
    new-instance v1, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->commitPurchaseResponse:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    .line 1040
    :cond_21
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->commitPurchaseResponse:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1044
    :sswitch_22
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->debugSettingsResponse:Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

    if-nez v1, :cond_22

    .line 1045
    new-instance v1, Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->debugSettingsResponse:Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

    .line 1047
    :cond_22
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->debugSettingsResponse:Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1051
    :sswitch_23
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkIabPromoResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    if-nez v1, :cond_23

    .line 1052
    new-instance v1, Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkIabPromoResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    .line 1054
    :cond_23
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkIabPromoResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1058
    :sswitch_24
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->userActivitySettingsResponse:Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    if-nez v1, :cond_24

    .line 1059
    new-instance v1, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->userActivitySettingsResponse:Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    .line 1061
    :cond_24
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->userActivitySettingsResponse:Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1065
    :sswitch_25
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->recordUserActivityResponse:Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

    if-nez v1, :cond_25

    .line 1066
    new-instance v1, Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->recordUserActivityResponse:Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

    .line 1068
    :cond_25
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->recordUserActivityResponse:Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1072
    :sswitch_26
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    if-nez v1, :cond_26

    .line 1073
    new-instance v1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    .line 1075
    :cond_26
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1079
    :sswitch_27
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->selfUpdateResponse:Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    if-nez v1, :cond_27

    .line 1080
    new-instance v1, Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->selfUpdateResponse:Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    .line 1082
    :cond_27
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->selfUpdateResponse:Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1086
    :sswitch_28
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchSuggestResponse:Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    if-nez v1, :cond_28

    .line 1087
    new-instance v1, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchSuggestResponse:Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    .line 1089
    :cond_28
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchSuggestResponse:Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1093
    :sswitch_29
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->getInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    if-nez v1, :cond_29

    .line 1094
    new-instance v1, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->getInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    .line 1096
    :cond_29
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->getInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1100
    :sswitch_2a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->createInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    if-nez v1, :cond_2a

    .line 1101
    new-instance v1, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->createInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .line 1103
    :cond_2a
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->createInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1107
    :sswitch_2b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->challengeResponse:Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    if-nez v1, :cond_2b

    .line 1108
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->challengeResponse:Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    .line 1110
    :cond_2b
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->challengeResponse:Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1114
    :sswitch_2c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDeviceChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    if-nez v1, :cond_2c

    .line 1115
    new-instance v1, Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDeviceChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    .line 1117
    :cond_2c
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDeviceChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1121
    :sswitch_2d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDocumentChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    if-nez v1, :cond_2d

    .line 1122
    new-instance v1, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDocumentChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    .line 1124
    :cond_2d
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDocumentChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1128
    :sswitch_2e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->earlyUpdateResponse:Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    if-nez v1, :cond_2e

    .line 1129
    new-instance v1, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->earlyUpdateResponse:Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    .line 1131
    :cond_2e
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->earlyUpdateResponse:Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1135
    :sswitch_2f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preloadsResponse:Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    if-nez v1, :cond_2f

    .line 1136
    new-instance v1, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preloadsResponse:Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    .line 1138
    :cond_2f
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preloadsResponse:Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1142
    :sswitch_30
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->myAccountResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    if-nez v1, :cond_30

    .line 1143
    new-instance v1, Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->myAccountResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    .line 1145
    :cond_30
    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->myAccountResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 803
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
        0xda -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xea -> :sswitch_1d
        0xf2 -> :sswitch_1e
        0xfa -> :sswitch_1f
        0x102 -> :sswitch_20
        0x10a -> :sswitch_21
        0x112 -> :sswitch_22
        0x11a -> :sswitch_23
        0x122 -> :sswitch_24
        0x12a -> :sswitch_25
        0x132 -> :sswitch_26
        0x13a -> :sswitch_27
        0x142 -> :sswitch_28
        0x14a -> :sswitch_29
        0x152 -> :sswitch_2a
        0x15a -> :sswitch_2b
        0x162 -> :sswitch_2c
        0x16a -> :sswitch_2d
        0x172 -> :sswitch_2e
        0x17a -> :sswitch_2f
        0x182 -> :sswitch_30
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 231
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Response$Payload;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Response$Payload;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->listResponse:Lcom/google/android/finsky/protos/DocList$ListResponse;

    if-eqz v0, :cond_0

    .line 453
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->listResponse:Lcom/google/android/finsky/protos/DocList$ListResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->detailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    if-eqz v0, :cond_1

    .line 456
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->detailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 458
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->reviewResponse:Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    if-eqz v0, :cond_2

    .line 459
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->reviewResponse:Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 461
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->buyResponse:Lcom/google/android/finsky/protos/Buy$BuyResponse;

    if-eqz v0, :cond_3

    .line 462
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->buyResponse:Lcom/google/android/finsky/protos/Buy$BuyResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 464
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchResponse:Lcom/google/android/finsky/protos/Search$SearchResponse;

    if-eqz v0, :cond_4

    .line 465
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchResponse:Lcom/google/android/finsky/protos/Search$SearchResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 467
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->tocResponse:Lcom/google/android/finsky/protos/Toc$TocResponse;

    if-eqz v0, :cond_5

    .line 468
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->tocResponse:Lcom/google/android/finsky/protos/Toc$TocResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 470
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->browseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    if-eqz v0, :cond_6

    .line 471
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->browseResponse:Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 473
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    if-eqz v0, :cond_7

    .line 474
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->purchaseStatusResponse:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 476
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->updateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    if-eqz v0, :cond_8

    .line 477
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->updateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 479
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->logResponse:Lcom/google/android/finsky/protos/Log$LogResponse;

    if-eqz v0, :cond_9

    .line 480
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->logResponse:Lcom/google/android/finsky/protos/Log$LogResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 482
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    if-eqz v0, :cond_a

    .line 483
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckInstrumentResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 485
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusOneResponse:Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    if-eqz v0, :cond_b

    .line 486
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusOneResponse:Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 488
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->flagContentResponse:Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    if-eqz v0, :cond_c

    .line 489
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->flagContentResponse:Lcom/google/android/finsky/protos/ContentFlagging$FlagContentResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 491
    :cond_c
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->ackNotificationResponse:Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    if-eqz v0, :cond_d

    .line 492
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->ackNotificationResponse:Lcom/google/android/finsky/protos/AckNotification$AckNotificationResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 494
    :cond_d
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->initiateAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    if-eqz v0, :cond_e

    .line 495
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->initiateAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$InitiateAssociationResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 497
    :cond_e
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->verifyAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    if-eqz v0, :cond_f

    .line 498
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->verifyAssociationResponse:Lcom/google/android/finsky/protos/CarrierBilling$VerifyAssociationResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 500
    :cond_f
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->libraryReplicationResponse:Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    if-eqz v0, :cond_10

    .line 501
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->libraryReplicationResponse:Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 503
    :cond_10
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->revokeResponse:Lcom/google/android/finsky/protos/RevokeResponse;

    if-eqz v0, :cond_11

    .line 504
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->revokeResponse:Lcom/google/android/finsky/protos/RevokeResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 506
    :cond_11
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->bulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    if-eqz v0, :cond_12

    .line 507
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->bulkDetailsResponse:Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 509
    :cond_12
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->resolveLinkResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-eqz v0, :cond_13

    .line 510
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->resolveLinkResponse:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 512
    :cond_13
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->deliveryResponse:Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    if-eqz v0, :cond_14

    .line 513
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->deliveryResponse:Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 515
    :cond_14
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->acceptTosResponse:Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    if-eqz v0, :cond_15

    .line 516
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->acceptTosResponse:Lcom/google/android/finsky/protos/Tos$AcceptTosResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 518
    :cond_15
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->rateSuggestedContentResponse:Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    if-eqz v0, :cond_16

    .line 519
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->rateSuggestedContentResponse:Lcom/google/android/finsky/protos/RateSuggestedContentResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 521
    :cond_16
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkPromoOfferResponse:Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    if-eqz v0, :cond_17

    .line 522
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkPromoOfferResponse:Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 524
    :cond_17
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->instrumentSetupInfoResponse:Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    if-eqz v0, :cond_18

    .line 525
    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->instrumentSetupInfoResponse:Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 527
    :cond_18
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemGiftCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    if-eqz v0, :cond_19

    .line 528
    const/16 v0, 0x1a

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemGiftCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$RedeemGiftCardResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 530
    :cond_19
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->modifyLibraryResponse:Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    if-eqz v0, :cond_1a

    .line 531
    const/16 v0, 0x1b

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->modifyLibraryResponse:Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 533
    :cond_1a
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->uploadDeviceConfigResponse:Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    if-eqz v0, :cond_1b

    .line 534
    const/16 v0, 0x1c

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->uploadDeviceConfigResponse:Lcom/google/android/finsky/protos/UploadDeviceConfig$UploadDeviceConfigResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 536
    :cond_1b
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    if-eqz v0, :cond_1c

    .line 537
    const/16 v0, 0x1d

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->plusProfileResponse:Lcom/google/android/finsky/protos/PlusProfile$PlusProfileResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 539
    :cond_1c
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->consumePurchaseResponse:Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    if-eqz v0, :cond_1d

    .line 540
    const/16 v0, 0x1e

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->consumePurchaseResponse:Lcom/google/android/finsky/protos/ConsumePurchaseResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 542
    :cond_1d
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->billingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    if-eqz v0, :cond_1e

    .line 543
    const/16 v0, 0x1f

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->billingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 545
    :cond_1e
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->preparePurchaseResponse:Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    if-eqz v0, :cond_1f

    .line 546
    const/16 v0, 0x20

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preparePurchaseResponse:Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 548
    :cond_1f
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->commitPurchaseResponse:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    if-eqz v0, :cond_20

    .line 549
    const/16 v0, 0x21

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->commitPurchaseResponse:Lcom/google/android/finsky/protos/Purchase$CommitPurchaseResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 551
    :cond_20
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->debugSettingsResponse:Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

    if-eqz v0, :cond_21

    .line 552
    const/16 v0, 0x22

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->debugSettingsResponse:Lcom/google/android/finsky/protos/DebugSettings$DebugSettingsResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 554
    :cond_21
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkIabPromoResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    if-eqz v0, :cond_22

    .line 555
    const/16 v0, 0x23

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->checkIabPromoResponse:Lcom/google/android/finsky/protos/BuyInstruments$CheckIabPromoResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 557
    :cond_22
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->userActivitySettingsResponse:Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    if-eqz v0, :cond_23

    .line 558
    const/16 v0, 0x24

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->userActivitySettingsResponse:Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 560
    :cond_23
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->recordUserActivityResponse:Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

    if-eqz v0, :cond_24

    .line 561
    const/16 v0, 0x25

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->recordUserActivityResponse:Lcom/google/android/finsky/protos/UserActivity$RecordUserActivityResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 563
    :cond_24
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    if-eqz v0, :cond_25

    .line 564
    const/16 v0, 0x26

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->redeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 566
    :cond_25
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->selfUpdateResponse:Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    if-eqz v0, :cond_26

    .line 567
    const/16 v0, 0x27

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->selfUpdateResponse:Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 569
    :cond_26
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchSuggestResponse:Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    if-eqz v0, :cond_27

    .line 570
    const/16 v0, 0x28

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->searchSuggestResponse:Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 572
    :cond_27
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->getInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    if-eqz v0, :cond_28

    .line 573
    const/16 v0, 0x29

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->getInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 575
    :cond_28
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->createInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    if-eqz v0, :cond_29

    .line 576
    const/16 v0, 0x2a

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->createInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 578
    :cond_29
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->challengeResponse:Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    if-eqz v0, :cond_2a

    .line 579
    const/16 v0, 0x2b

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->challengeResponse:Lcom/google/android/finsky/protos/ChallengeAction$ChallengeResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 581
    :cond_2a
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDeviceChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    if-eqz v0, :cond_2b

    .line 582
    const/16 v0, 0x2c

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDeviceChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDeviceChoicesResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 584
    :cond_2b
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDocumentChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    if-eqz v0, :cond_2c

    .line 585
    const/16 v0, 0x2d

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->backupDocumentChoicesResponse:Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 587
    :cond_2c
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->earlyUpdateResponse:Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    if-eqz v0, :cond_2d

    .line 588
    const/16 v0, 0x2e

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->earlyUpdateResponse:Lcom/google/android/finsky/protos/EarlyUpdate$EarlyUpdateResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 590
    :cond_2d
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->preloadsResponse:Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    if-eqz v0, :cond_2e

    .line 591
    const/16 v0, 0x2f

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->preloadsResponse:Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 593
    :cond_2e
    iget-object v0, p0, Lcom/google/android/finsky/protos/Response$Payload;->myAccountResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    if-eqz v0, :cond_2f

    .line 594
    const/16 v0, 0x30

    iget-object v1, p0, Lcom/google/android/finsky/protos/Response$Payload;->myAccountResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 596
    :cond_2f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 597
    return-void
.end method
