.class public Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterView;
.source "PlayCardRateClusterView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayCardDismissListener;


# instance fields
.field private mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

.field private mEmptyDone:Landroid/widget/TextView;

.field private mRejectTouchEvents:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mRejectTouchEvents:Z

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;
    .param p1, "x1"    # Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->fadeOutCard(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;
    .param p1, "x1"    # Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->startSlideTransition(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;
    .param p1, "x1"    # Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->onSlideTransitionCompleted(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;)Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    return-object v0
.end method

.method private fadeOutCard(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V
    .locals 5
    .param p1, "rateCard"    # Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v2, 0x96

    new-instance v4, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$2;

    invoke-direct {v4, p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$2;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 169
    .local v0, "fadeOutAnimation":Landroid/view/animation/Animation;
    invoke-virtual {p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->startAnimation(Landroid/view/animation/Animation;)V

    .line 170
    return-void
.end method

.method public static isClusterDismissed(Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/Document;)Z
    .locals 5
    .param p0, "cache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 97
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 98
    invoke-virtual {p1, v1}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 100
    .local v0, "childDoc":Lcom/google/android/finsky/api/model/Document;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/utils/ClientMutationCache;->isDismissedRecommendation(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 97
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/finsky/utils/ClientMutationCache;->getCachedReview(Ljava/lang/String;Lcom/google/android/finsky/protos/DocumentV2$Review;)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v2

    .line 106
    .local v2, "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-nez v2, :cond_0

    .line 107
    const/4 v3, 0x0

    .line 110
    .end local v0    # "childDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v2    # "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    :goto_1
    return v3

    :cond_2
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private onSlideTransitionCompleted(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V
    .locals 5
    .param p1, "rateCard"    # Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .prologue
    const/4 v4, 0x0

    .line 232
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->removeView(Landroid/view/View;)V

    .line 233
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->addView(Landroid/view/View;)V

    .line 235
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;

    .line 237
    .local v0, "mRateContent":Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->syncIndexMapping()V

    .line 238
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->hasItemsToRate()Z

    move-result v3

    if-nez v3, :cond_0

    .line 240
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->transitionToEmptyState(Z)V

    .line 251
    :goto_0
    iput-boolean v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mRejectTouchEvents:Z

    .line 252
    return-void

    .line 243
    :cond_0
    invoke-virtual {p1, v4}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setState(I)V

    .line 244
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->clearRating()V

    .line 245
    invoke-virtual {p1, v4}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setVisibility(I)V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getCardChildCount()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .line 248
    .local v2, "newTileIndex":I
    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->tileIndexToDocumentIndex(I)I

    move-result v1

    .line 249
    .local v1, "newDocIndex":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v3, v2, v1, p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->bindCardAt(IILcom/google/android/finsky/layout/play/PlayCardDismissListener;)V

    goto :goto_0
.end method

.method private startSlideTransition(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V
    .locals 11
    .param p1, "rateCard"    # Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .prologue
    const/4 v2, 0x0

    .line 175
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildIndex(Landroid/view/View;)I

    move-result v7

    .line 177
    .local v7, "cardIndex":I
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getWidth()I

    move-result v6

    .line 178
    .local v6, "fullSlideDistance":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getCardChildCount()I

    move-result v8

    .line 180
    .local v8, "cardCount":I
    new-array v9, v8, [I

    .line 181
    .local v9, "cardOriginalLeftPositions":[I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v8, :cond_0

    .line 182
    invoke-virtual {p0, v10}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/layout/PlayCardViewBase;->getLeft()I

    move-result v1

    aput v1, v9, v10

    .line 181
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 184
    :cond_0
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$3;

    int-to-float v4, v6

    move-object v1, p0

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$3;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;FFFFIII[I)V

    .line 200
    .local v0, "translateAnimation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 201
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 202
    new-instance v1, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$4;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 226
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->startAnimation(Landroid/view/animation/Animation;)V

    .line 227
    return-void
.end method

.method private transitionToEmptyState(Z)V
    .locals 8
    .param p1, "toAnimate"    # Z

    .prologue
    const-wide/16 v6, 0xfa

    const/4 v4, 0x0

    .line 255
    if-nez p1, :cond_0

    .line 256
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setVisibility(I)V

    .line 257
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    :goto_0
    return-void

    .line 262
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$5;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$5;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;)V

    invoke-static {v2, v6, v7, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 277
    .local v1, "fadeOutAnimation":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->startAnimation(Landroid/view/animation/Animation;)V

    .line 278
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v6, v7, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 280
    .local v0, "fadeInAnimation":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 283
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 13
    .param p1, "metadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p2, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p3, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .param p7, "cardHeap"    # Lcom/google/android/finsky/layout/play/PlayCardHeap;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 121
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object v6, p0

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-super/range {v0 .. v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getCardChildCount()I

    move-result v9

    .line 125
    .local v9, "cardCount":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v9, :cond_0

    .line 126
    invoke-virtual {p0, v10}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .line 127
    .local v11, "rateCard":Lcom/google/android/finsky/layout/play/PlayCardViewRate;
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$1;

    invoke-direct {v0, p0, v11}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    invoke-virtual {v11, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setRateListener(Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;)V

    .line 143
    const/4 v0, 0x0

    invoke-virtual {v11, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setState(I)V

    .line 125
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 146
    .end local v11    # "rateCard":Lcom/google/android/finsky/layout/play/PlayCardViewRate;
    :cond_0
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    check-cast v12, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;

    .line 147
    .local v12, "rateContent":Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;
    invoke-virtual {v12}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterViewContent;->hasItemsToRate()Z

    move-result v0

    if-nez v0, :cond_1

    .line 148
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->transitionToEmptyState(Z)V

    .line 155
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mRejectTouchEvents:Z

    .line 156
    return-void
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0x19c

    return v0
.end method

.method public onDismissDocument(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V
    .locals 2
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->dismissRecommendation(Ljava/lang/String;)V

    .line 89
    check-cast p2, Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    .end local p2    # "card":Lcom/google/android/play/layout/PlayCardViewBase;
    invoke-direct {p0, p2}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->fadeOutCard(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    .line 90
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->onFinishInflate()V

    .line 72
    const v0, 0x7f0a02d7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    .line 73
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mRejectTouchEvents:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v6, 0x0

    .line 313
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getWidth()I

    move-result v2

    .line 314
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getPaddingTop()I

    move-result v3

    .line 316
    .local v3, "y":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    .line 317
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {v4, v6, v3, v2, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->layout(IIII)V

    .line 318
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 321
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredHeight()I

    move-result v0

    .line 322
    .local v0, "contentScrollerHeight":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    add-int v5, v3, v0

    invoke-virtual {v4, v6, v3, v2, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->layout(IIII)V

    .line 323
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v0, v4

    div-int/lit8 v4, v4, 0x2

    add-int v1, v3, v4

    .line 324
    .local v1, "emptyDoneY":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v4, v6, v1, v2, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 325
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v4, 0x0

    .line 293
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 294
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->getPaddingBottom()I

    move-result v3

    add-int v0, v2, v3

    .line 296
    .local v0, "height":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 297
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v2, p1, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->measure(II)V

    .line 298
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 301
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v2, p1, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->measure(II)V

    .line 302
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 304
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mEmptyDone:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredWidth()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredHeight()I

    move-result v4

    const/high16 v5, -0x80000000

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->measure(II)V

    .line 308
    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->setMeasuredDimension(II)V

    .line 309
    return-void
.end method

.method public setClusterFadeOutListener(Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;)V
    .locals 0
    .param p1, "clusterFadeOutListener"    # Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    .line 81
    return-void
.end method
