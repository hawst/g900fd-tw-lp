.class public final Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SmsCodeChallenge"
.end annotation


# instance fields
.field public descriptionHtml:Ljava/lang/String;

.field public hasDescriptionHtml:Z

.field public hasTitle:Z

.field public resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

.field public smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

.field public submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1852
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1853
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->clear()Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    .line 1854
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1857
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->title:Ljava/lang/String;

    .line 1858
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->hasTitle:Z

    .line 1859
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->descriptionHtml:Ljava/lang/String;

    .line 1860
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->hasDescriptionHtml:Z

    .line 1861
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    .line 1862
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 1863
    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 1864
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->cachedSize:I

    .line 1865
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1891
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1892
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->hasTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1893
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1896
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->hasDescriptionHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->descriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1897
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1900
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v1, :cond_4

    .line 1901
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1904
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v1, :cond_5

    .line 1905
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1908
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v1, :cond_6

    .line 1909
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1912
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1920
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1921
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1925
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1926
    :sswitch_0
    return-object p0

    .line 1931
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->title:Ljava/lang/String;

    .line 1932
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->hasTitle:Z

    goto :goto_0

    .line 1936
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->descriptionHtml:Ljava/lang/String;

    .line 1937
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->hasDescriptionHtml:Z

    goto :goto_0

    .line 1941
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-nez v1, :cond_1

    .line 1942
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    .line 1944
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1948
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-nez v1, :cond_2

    .line 1949
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 1951
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1955
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-nez v1, :cond_3

    .line 1956
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 1958
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1921
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1818
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1871
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->hasTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1872
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1874
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->hasDescriptionHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->descriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1875
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1877
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    if-eqz v0, :cond_4

    .line 1878
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->smsCode:Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1880
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v0, :cond_5

    .line 1881
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->resendCodeButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1883
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v0, :cond_6

    .line 1884
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1886
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1887
    return-void
.end method
