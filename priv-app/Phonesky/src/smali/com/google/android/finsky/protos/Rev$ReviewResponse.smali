.class public final Lcom/google/android/finsky/protos/Rev$ReviewResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Rev.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Rev;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReviewResponse"
.end annotation


# instance fields
.field public criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

.field public getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

.field public hasNextPageUrl:Z

.field public hasSuggestionsListUrl:Z

.field public nextPageUrl:Ljava/lang/String;

.field public suggestionsListUrl:Ljava/lang/String;

.field public updatedReview:Lcom/google/android/finsky/protos/DocumentV2$Review;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 450
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 451
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->clear()Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .line 452
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Rev$ReviewResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 455
    iput-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    .line 456
    iput-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    .line 457
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->nextPageUrl:Ljava/lang/String;

    .line 458
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->hasNextPageUrl:Z

    .line 459
    iput-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->updatedReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 460
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->suggestionsListUrl:Ljava/lang/String;

    .line 461
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->hasSuggestionsListUrl:Z

    .line 462
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->cachedSize:I

    .line 463
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 489
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 490
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    if-eqz v1, :cond_0

    .line 491
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 494
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->hasNextPageUrl:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->nextPageUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 495
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->nextPageUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 498
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->updatedReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-eqz v1, :cond_3

    .line 499
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->updatedReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 502
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->hasSuggestionsListUrl:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->suggestionsListUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 503
    :cond_4
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->suggestionsListUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 506
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    if-eqz v1, :cond_6

    .line 507
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 510
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Rev$ReviewResponse;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 519
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 523
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 524
    :sswitch_0
    return-object p0

    .line 529
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    if-nez v1, :cond_1

    .line 530
    new-instance v1, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    .line 532
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 536
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->nextPageUrl:Ljava/lang/String;

    .line 537
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->hasNextPageUrl:Z

    goto :goto_0

    .line 541
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->updatedReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-nez v1, :cond_2

    .line 542
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$Review;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->updatedReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 544
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->updatedReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 548
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->suggestionsListUrl:Ljava/lang/String;

    .line 549
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->hasSuggestionsListUrl:Z

    goto :goto_0

    .line 553
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    if-nez v1, :cond_3

    .line 554
    new-instance v1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    .line 556
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 519
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 416
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    if-eqz v0, :cond_0

    .line 470
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 472
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->hasNextPageUrl:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->nextPageUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 473
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->nextPageUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 475
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->updatedReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-eqz v0, :cond_3

    .line 476
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->updatedReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 478
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->hasSuggestionsListUrl:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->suggestionsListUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 479
    :cond_4
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->suggestionsListUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 481
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    if-eqz v0, :cond_6

    .line 482
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 484
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 485
    return-void
.end method
