.class Lcom/google/android/finsky/receivers/InstallerTask$3;
.super Ljava/lang/Object;
.source "InstallerTask.java"

# interfaces
.implements Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/receivers/InstallerTask;->requestDeliveryData(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/receivers/InstallerTask;

.field final synthetic val$deliveryToken:Ljava/lang/String;

.field final synthetic val$errorListener:Lcom/android/volley/Response$ErrorListener;

.field final synthetic val$finalAccount:Landroid/accounts/Account;

.field final synthetic val$finalAccountName:Ljava/lang/String;

.field final synthetic val$finalCertificateHash:Ljava/lang/String;

.field final synthetic val$finalPreviousVersion:Ljava/lang/Integer;

.field final synthetic val$finskyApp:Lcom/google/android/finsky/FinskyApp;

.field final synthetic val$isEarlyUpdate:Z

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$packageVersion:I

.field final synthetic val$responseListener:Lcom/android/volley/Response$Listener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerTask;ZLjava/lang/String;Lcom/google/android/finsky/FinskyApp;ILjava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Landroid/accounts/Account;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1039
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iput-boolean p2, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$isEarlyUpdate:Z

    iput-object p3, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$packageName:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finskyApp:Lcom/google/android/finsky/FinskyApp;

    iput p5, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$packageVersion:I

    iput-object p6, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalPreviousVersion:Ljava/lang/Integer;

    iput-object p7, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalCertificateHash:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$deliveryToken:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$responseListener:Lcom/android/volley/Response$Listener;

    iput-object p10, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$errorListener:Lcom/android/volley/Response$ErrorListener;

    iput-object p11, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalAccount:Landroid/accounts/Account;

    iput-object p12, p0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalAccountName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenReceived(Ljava/lang/String;)V
    .locals 22
    .param p1, "consistencyToken"    # Ljava/lang/String;

    .prologue
    .line 1042
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$isEarlyUpdate:Z

    if-eqz v1, :cond_1

    .line 1043
    const-string v1, "Request earlyDelivery for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$packageName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1044
    const/4 v7, 0x0

    .line 1046
    .local v7, "certificateHashMd5":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finskyApp:Lcom/google/android/finsky/FinskyApp;

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1047
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finskyApp:Lcom/google/android/finsky/FinskyApp;

    invoke-static {v1}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->getCertificateHashSelfUpdateMD5(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    .line 1050
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finskyApp:Lcom/google/android/finsky/FinskyApp;

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApiNonAuthenticated()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$packageName:Ljava/lang/String;

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$packageVersion:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalPreviousVersion:Ljava/lang/Integer;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->SUPPORTED_PATCH_FORMATS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/receivers/InstallerTask;->access$900()[Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalCertificateHash:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$deliveryToken:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$responseListener:Lcom/android/volley/Response$Listener;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$errorListener:Lcom/android/volley/Response$ErrorListener;

    move-object/from16 v10, p1

    invoke-interface/range {v1 .. v12}, Lcom/google/android/finsky/api/DfeApi;->earlyDelivery(Ljava/lang/String;ILjava/lang/Integer;Ljava/lang/Integer;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 1065
    .end local v7    # "certificateHashMd5":Ljava/lang/String;
    :goto_0
    return-void

    .line 1055
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finskyApp:Lcom/google/android/finsky/FinskyApp;

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalAccount:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v21

    .line 1057
    .local v21, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    sget-object v1, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_APPS:Ljava/lang/String;

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/AccountLibrary;->getServerToken(Ljava/lang/String;)[B

    move-result-object v11

    .line 1059
    .local v11, "serverToken":[B
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finskyApp:Lcom/google/android/finsky/FinskyApp;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalAccountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$packageName:Ljava/lang/String;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$packageVersion:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalPreviousVersion:Ljava/lang/Integer;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->SUPPORTED_PATCH_FORMATS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/receivers/InstallerTask;->access$900()[Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$finalCertificateHash:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$deliveryToken:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$responseListener:Lcom/android/volley/Response$Listener;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$3;->val$errorListener:Lcom/android/volley/Response$ErrorListener;

    move-object/from16 v20, v0

    move-object/from16 v18, p1

    invoke-interface/range {v8 .. v20}, Lcom/google/android/finsky/api/DfeApi;->delivery(Ljava/lang/String;I[BLjava/lang/Integer;Ljava/lang/Integer;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    goto :goto_0
.end method
