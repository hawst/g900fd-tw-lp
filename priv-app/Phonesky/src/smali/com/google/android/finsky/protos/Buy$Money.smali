.class public final Lcom/google/android/finsky/protos/Buy$Money;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Buy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Buy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Money"
.end annotation


# instance fields
.field public currencyCode:Ljava/lang/String;

.field public formattedAmount:Ljava/lang/String;

.field public hasCurrencyCode:Z

.field public hasFormattedAmount:Z

.field public hasMicros:Z

.field public micros:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1613
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1614
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Buy$Money;->clear()Lcom/google/android/finsky/protos/Buy$Money;

    .line 1615
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Buy$Money;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1618
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->micros:J

    .line 1619
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasMicros:Z

    .line 1620
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->currencyCode:Ljava/lang/String;

    .line 1621
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasCurrencyCode:Z

    .line 1622
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->formattedAmount:Ljava/lang/String;

    .line 1623
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasFormattedAmount:Z

    .line 1624
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->cachedSize:I

    .line 1625
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 1645
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1646
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasMicros:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Buy$Money;->micros:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 1647
    :cond_0
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Buy$Money;->micros:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1650
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasCurrencyCode:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$Money;->currencyCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1651
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$Money;->currencyCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1654
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasFormattedAmount:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$Money;->formattedAmount:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1655
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$Money;->formattedAmount:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1658
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$Money;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1666
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1667
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1671
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1672
    :sswitch_0
    return-object p0

    .line 1677
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/Buy$Money;->micros:J

    .line 1678
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasMicros:Z

    goto :goto_0

    .line 1682
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Buy$Money;->currencyCode:Ljava/lang/String;

    .line 1683
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasCurrencyCode:Z

    goto :goto_0

    .line 1687
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Buy$Money;->formattedAmount:Ljava/lang/String;

    .line 1688
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasFormattedAmount:Z

    goto :goto_0

    .line 1667
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1584
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Buy$Money;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$Money;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1631
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasMicros:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->micros:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1632
    :cond_0
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Buy$Money;->micros:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1634
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasCurrencyCode:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->currencyCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1635
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$Money;->currencyCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1637
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->hasFormattedAmount:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$Money;->formattedAmount:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1638
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$Money;->formattedAmount:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1640
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1641
    return-void
.end method
