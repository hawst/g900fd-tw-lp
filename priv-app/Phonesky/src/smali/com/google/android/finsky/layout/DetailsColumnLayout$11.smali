.class Lcom/google/android/finsky/layout/DetailsColumnLayout$11;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DetailsColumnLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsColumnLayout;->collapseCurrentlyExpandedSectionIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V
    .locals 0

    .prologue
    .line 558
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$11;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$11;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setVisibility(I)V

    .line 562
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$11;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->resetContent()V

    .line 565
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$11;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->setSeparatorsVisible(Z)V

    .line 568
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$11;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->unblockLayoutRequests()V

    .line 570
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$11;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->requestLayout()V

    .line 571
    return-void
.end method
