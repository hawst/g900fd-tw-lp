.class public final Lcom/google/android/finsky/protos/ChallengeProto$Country;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Country"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Country;


# instance fields
.field public displayName:Ljava/lang/String;

.field public hasDisplayName:Z

.field public hasRegionCode:Z

.field public regionCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1312
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1313
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$Country;->clear()Lcom/google/android/finsky/protos/ChallengeProto$Country;

    .line 1314
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$Country;
    .locals 2

    .prologue
    .line 1293
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    if-nez v0, :cond_1

    .line 1294
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1296
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    if-nez v0, :cond_0

    .line 1297
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/ChallengeProto$Country;

    sput-object v0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    .line 1299
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1301
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    return-object v0

    .line 1299
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$Country;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1317
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->regionCode:Ljava/lang/String;

    .line 1318
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->hasRegionCode:Z

    .line 1319
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->displayName:Ljava/lang/String;

    .line 1320
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->hasDisplayName:Z

    .line 1321
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->cachedSize:I

    .line 1322
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1339
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1340
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->hasRegionCode:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->regionCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1341
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->regionCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1344
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->hasDisplayName:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->displayName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1345
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->displayName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1348
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$Country;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1356
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1357
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1361
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1362
    :sswitch_0
    return-object p0

    .line 1367
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->regionCode:Ljava/lang/String;

    .line 1368
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->hasRegionCode:Z

    goto :goto_0

    .line 1372
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->displayName:Ljava/lang/String;

    .line 1373
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->hasDisplayName:Z

    goto :goto_0

    .line 1357
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1287
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$Country;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$Country;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1328
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->hasRegionCode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->regionCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1329
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->regionCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1331
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->hasDisplayName:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->displayName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1332
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Country;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1334
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1335
    return-void
.end method
