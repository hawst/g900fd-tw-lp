.class public Lcom/google/android/finsky/layout/EpisodeSnippet;
.super Lcom/google/android/play/layout/ForegroundRelativeLayout;
.source "EpisodeSnippet.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;
    }
.end annotation


# instance fields
.field private mAddedDrawable:Landroid/view/View;

.field private mAddedState:Landroid/widget/TextView;

.field private final mBaseRowHeight:I

.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mCollapsedStateChangedListener:Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;

.field private mEpisode:Lcom/google/android/finsky/api/model/Document;

.field private mEpisodeNumber:Landroid/widget/TextView;

.field private mEpisodeTitle:Landroid/widget/TextView;

.field private mExpandedContent:Landroid/view/View;

.field private mExpandedDescription:Landroid/widget/TextView;

.field private mExpandedEpisodeScreencap:Lcom/google/android/finsky/layout/HeroGraphicView;

.field private mExpandedViewStub:Landroid/view/ViewStub;

.field private final mHandler:Landroid/os/Handler;

.field private mIsNewPurchase:Z

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mScrollerRunnable:Ljava/lang/Runnable;

.field private mSeasonDocument:Lcom/google/android/finsky/api/model/Document;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 70
    const/16 v0, 0x1f7

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 76
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mHandler:Landroid/os/Handler;

    .line 77
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBaseRowHeight:I

    .line 78
    new-instance v0, Lcom/google/android/finsky/layout/EpisodeSnippet$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/EpisodeSnippet$1;-><init>(Lcom/google/android/finsky/layout/EpisodeSnippet;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mScrollerRunnable:Ljava/lang/Runnable;

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/EpisodeSnippet;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/EpisodeSnippet;

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBaseRowHeight:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/EpisodeSnippet;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/EpisodeSnippet;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->toggleExpandedVisibility()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/EpisodeSnippet;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/EpisodeSnippet;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->logCurrentState()V

    return-void
.end method

.method private static clearBuyButtonStyle(Lcom/google/android/play/layout/PlayActionButton;)V
    .locals 3
    .param p0, "button"    # Lcom/google/android/play/layout/PlayActionButton;

    .prologue
    .line 397
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayActionButton;->setDrawAsLabel(Z)V

    .line 398
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayActionButton;->setActionStyle(I)V

    .line 399
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 400
    const/4 v0, 0x4

    invoke-virtual {p0}, Lcom/google/android/play/layout/PlayActionButton;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 401
    return-void
.end method

.method public static hasSeasonOffer(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 7
    .param p0, "season"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x1

    .line 372
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 373
    .local v0, "currentAccount":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    .line 374
    .local v1, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-static {p0, v1, v0}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v4

    .line 375
    .local v4, "owner":Landroid/accounts/Account;
    if-eqz v4, :cond_1

    .line 387
    :cond_0
    :goto_0
    return v5

    .line 380
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v3

    .line 381
    .local v3, "offers":[Lcom/google/android/finsky/protos/Common$Offer;
    invoke-static {v3}, Lcom/google/android/finsky/utils/DocUtils;->getNumberOfValidOffers([Lcom/google/android/finsky/protos/Common$Offer;)I

    move-result v2

    .line 382
    .local v2, "numValidOffers":I
    if-gtz v2, :cond_0

    .line 387
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private inflateViewStubIfNecessary()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    if-nez v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedViewStub:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    .line 170
    const v0, 0x7f0a01d8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedDescription:Landroid/widget/TextView;

    .line 171
    const v0, 0x7f0a01d7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/HeroGraphicView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedEpisodeScreencap:Lcom/google/android/finsky/layout/HeroGraphicView;

    .line 173
    :cond_0
    return-void
.end method

.method private logCurrentState()V
    .locals 4

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->isExpanded()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v0, 0x10f

    .line 138
    .local v0, "eventType":I
    :goto_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 139
    return-void

    .line 136
    .end local v0    # "eventType":I
    :cond_0
    const/16 v0, 0x110

    goto :goto_0
.end method

.method private static setBuyButtonStyle(Lcom/google/android/play/layout/PlayActionButton;)V
    .locals 1
    .param p0, "button"    # Lcom/google/android/play/layout/PlayActionButton;

    .prologue
    .line 391
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayActionButton;->setDrawAsLabel(Z)V

    .line 392
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayActionButton;->setActionStyle(I)V

    .line 393
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 394
    return-void
.end method

.method private setExpandedContentVisibility(I)V
    .locals 10
    .param p1, "visibility"    # I

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->inflateViewStubIfNecessary()V

    .line 177
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    .line 181
    if-ne p1, v8, :cond_3

    .line 182
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 183
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 190
    :goto_0
    if-nez p1, :cond_1

    .line 193
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedEpisodeScreencap:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b010f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/HeroGraphicView;->setMaximumHeight(I)V

    .line 195
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedEpisodeScreencap:Lcom/google/android/finsky/layout/HeroGraphicView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    new-array v6, v1, [I

    const/16 v7, 0xd

    aput v7, v6, v2

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/finsky/layout/HeroGraphicView;->load(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;[I)V

    .line 197
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedEpisodeScreencap:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v3, v9}, Lcom/google/android/finsky/layout/HeroGraphicView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 199
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "episodeDescription":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 201
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 202
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c01f4

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->releaseDate:Ljava/lang/String;

    aput-object v7, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 205
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedDescription:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    .end local v0    # "episodeDescription":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mCollapsedStateChangedListener:Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;

    if-eqz v3, :cond_2

    .line 210
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mCollapsedStateChangedListener:Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;

    if-ne p1, v8, :cond_4

    :goto_1
    invoke-interface {v3, p0, v1}, Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;->onCollapsedStateChanged(Lcom/google/android/finsky/layout/EpisodeSnippet;Z)V

    .line 216
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mScrollerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 217
    return-void

    .line 185
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 188
    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 210
    goto :goto_1
.end method

.method private toggleExpandedVisibility()V
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->isExpanded()Z

    move-result v0

    .line 164
    .local v0, "isExpanded":Z
    if-eqz v0, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setExpandedContentVisibility(I)V

    .line 165
    return-void

    .line 164
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static updateBuyButtonState(Landroid/content/res/Resources;Lcom/google/android/play/layout/PlayActionButton;Landroid/widget/TextView;Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;ZLcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 27
    .param p0, "resources"    # Landroid/content/res/Resources;
    .param p1, "buyButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p2, "addedButton"    # Landroid/widget/TextView;
    .param p3, "addedDrawable"    # Landroid/view/View;
    .param p4, "parentDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p6, "isNewPurchase"    # Z
    .param p7, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 268
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 269
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v5

    .line 270
    .local v5, "currentAccount":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v19

    .line 271
    .local v19, "libraries":Lcom/google/android/finsky/library/Libraries;
    move-object/from16 v0, p5

    move-object/from16 v1, v19

    invoke-static {v0, v1, v5}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v24

    .line 273
    .local v24, "owner":Landroid/accounts/Account;
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v23

    .line 274
    .local v23, "offers":[Lcom/google/android/finsky/protos/Common$Offer;
    invoke-static/range {v23 .. v23}, Lcom/google/android/finsky/utils/DocUtils;->getNumberOfValidOffers([Lcom/google/android/finsky/protos/Common$Offer;)I

    move-result v21

    .line 275
    .local v21, "numValidOffers":I
    if-eqz v24, :cond_2

    .line 277
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setBuyButtonStyle(Lcom/google/android/play/layout/PlayActionButton;)V

    .line 279
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    const/16 v6, 0x13

    if-ne v4, v6, :cond_1

    .line 280
    const v14, 0x7f0c01ca

    .line 281
    .local v14, "buyButtonTextId":I
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 287
    :goto_0
    const/4 v4, 0x4

    new-instance v6, Lcom/google/android/finsky/layout/EpisodeSnippet$3;

    move-object/from16 v0, p8

    move-object/from16 v1, p7

    move-object/from16 v2, v24

    move-object/from16 v3, p5

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/google/android/finsky/layout/EpisodeSnippet$3;-><init>(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14, v6}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 359
    .end local v14    # "buyButtonTextId":I
    :goto_1
    if-eqz p2, :cond_0

    .line 360
    if-eqz p6, :cond_10

    const/16 v25, 0x0

    .line 361
    .local v25, "visibility":I
    :goto_2
    move-object/from16 v0, p3

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 362
    move-object/from16 v0, p2

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 364
    .end local v25    # "visibility":I
    :cond_0
    return-void

    .line 284
    :cond_1
    const v14, 0x7f0c01e4

    .line 285
    .restart local v14    # "buyButtonTextId":I
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    goto :goto_0

    .line 296
    .end local v14    # "buyButtonTextId":I
    :cond_2
    if-lez v21, :cond_e

    .line 298
    const/4 v4, 0x1

    move-object/from16 v0, v23

    invoke-static {v0, v4}, Lcom/google/android/finsky/utils/DocUtils;->getLowestPricedOffer([Lcom/google/android/finsky/protos/Common$Offer;Z)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v20

    .line 299
    .local v20, "lowestPricedOffer":Lcom/google/android/finsky/protos/Common$Offer;
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setBuyButtonStyle(Lcom/google/android/play/layout/PlayActionButton;)V

    .line 300
    const/4 v13, 0x0

    .line 303
    .local v13, "buyButtonText":Ljava/lang/String;
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    const/16 v6, 0x13

    if-ne v4, v6, :cond_6

    .line 304
    const/4 v15, 0x0

    .line 305
    .local v15, "hasBuyButton":Z
    const/16 v16, 0x0

    .line 307
    .local v16, "hasRentButton":Z
    move-object/from16 v12, v23

    .local v12, "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v0, v12

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    :goto_3
    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_5

    aget-object v22, v12, v17

    .line 308
    .local v22, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    move-object/from16 v0, v22

    iget v7, v0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 309
    .local v7, "offerType":I
    sget-object v4, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->RENTAL:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    invoke-virtual {v4, v7}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->matches(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 310
    const/16 v16, 0x1

    .line 307
    :cond_3
    :goto_4
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 311
    :cond_4
    sget-object v4, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->PURCHASE:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    invoke-virtual {v4, v7}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->matches(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 312
    const/4 v15, 0x1

    goto :goto_4

    .line 316
    .end local v7    # "offerType":I
    .end local v22    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_5
    if-eqz v15, :cond_8

    if-eqz v16, :cond_8

    .line 317
    const v4, 0x7f0c0201

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, v20

    iget-object v9, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v9, v6, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 341
    .end local v12    # "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    .end local v15    # "hasBuyButton":Z
    .end local v16    # "hasRentButton":Z
    .end local v17    # "i$":I
    .end local v18    # "len$":I
    :cond_6
    :goto_5
    if-nez v13, :cond_7

    .line 342
    move-object/from16 v0, v20

    iget-boolean v4, v0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-eqz v4, :cond_c

    move-object/from16 v0, v20

    iget-object v13, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    .line 346
    :cond_7
    :goto_6
    const/4 v4, 0x1

    move/from16 v0, v21

    if-ne v0, v4, :cond_d

    move-object/from16 v0, v20

    iget v7, v0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 347
    .restart local v7    # "offerType":I
    :goto_7
    const/16 v26, 0x4

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0xc8

    move-object/from16 v4, p7

    move-object/from16 v6, p5

    move-object/from16 v11, p8

    invoke-virtual/range {v4 .. v11}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v4

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v1, v13, v4}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 319
    .end local v7    # "offerType":I
    .restart local v12    # "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    .restart local v15    # "hasBuyButton":Z
    .restart local v16    # "hasRentButton":Z
    .restart local v17    # "i$":I
    .restart local v18    # "len$":I
    :cond_8
    if-eqz v15, :cond_a

    .line 321
    const/4 v4, 0x1

    move/from16 v0, v21

    if-ne v0, v4, :cond_9

    .line 322
    const v4, 0x7f0c01f9

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, v20

    iget-object v9, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v9, v6, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    goto :goto_5

    .line 325
    :cond_9
    const v4, 0x7f0c0200

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, v20

    iget-object v9, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v9, v6, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    goto :goto_5

    .line 328
    :cond_a
    if-eqz v16, :cond_6

    .line 330
    const/4 v4, 0x1

    move/from16 v0, v21

    if-ne v0, v4, :cond_b

    .line 331
    const v4, 0x7f0c01fa

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, v20

    iget-object v9, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v9, v6, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    goto :goto_5

    .line 334
    :cond_b
    const v4, 0x7f0c01fd

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, v20

    iget-object v9, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v9, v6, v8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    goto/16 :goto_5

    .line 342
    .end local v12    # "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    .end local v15    # "hasBuyButton":Z
    .end local v16    # "hasRentButton":Z
    .end local v17    # "i$":I
    .end local v18    # "len$":I
    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_6

    .line 346
    :cond_d
    const/4 v7, 0x0

    goto :goto_7

    .line 351
    .end local v13    # "buyButtonText":Ljava/lang/String;
    .end local v20    # "lowestPricedOffer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_e
    if-eqz p4, :cond_f

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/utils/DocUtils;->getNumberOfValidOffers([Lcom/google/android/finsky/protos/Common$Offer;)I

    move-result v4

    if-lez v4, :cond_f

    .line 353
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/layout/EpisodeSnippet;->clearBuyButtonStyle(Lcom/google/android/play/layout/PlayActionButton;)V

    .line 354
    const/4 v4, 0x4

    const v6, 0x7f0c0338

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6, v8}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 356
    :cond_f
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    goto/16 :goto_1

    .line 360
    :cond_10
    const/16 v25, 0x8

    goto/16 :goto_2
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 421
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public collapseWithoutNotifyingListeners()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    if-nez v0, :cond_0

    .line 149
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public expand()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setExpandedContentVisibility(I)V

    .line 153
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public isExpanded()Z
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedContent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 10

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onAttachedToWindow()V

    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTvEpisodeDetails()Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    move-result-object v9

    .line 110
    .local v9, "episodeDetails":Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;
    if-nez v9, :cond_0

    .line 111
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setVisibility(I)V

    .line 133
    :goto_0
    return-void

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeNumber:Landroid/widget/TextView;

    iget v1, v9, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->episodeIndex:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeNumber:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c03ff

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, v9, Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;->episodeIndex:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    iget-object v2, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mAddedState:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mAddedDrawable:Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mSeasonDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v5, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    iget-boolean v6, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mIsNewPurchase:Z

    iget-object v7, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object v8, p0

    invoke-static/range {v0 .. v8}, Lcom/google/android/finsky/layout/EpisodeSnippet;->updateBuyButtonState(Landroid/content/res/Resources;Lcom/google/android/play/layout/PlayActionButton;Landroid/widget/TextView;Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;ZLcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 126
    new-instance v0, Lcom/google/android/finsky/layout/EpisodeSnippet$2;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/EpisodeSnippet$2;-><init>(Lcom/google/android/finsky/layout/EpisodeSnippet;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mScrollerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 222
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onDetachedFromWindow()V

    .line 223
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 227
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onFinishInflate()V

    .line 229
    const v0, 0x7f0a01ca

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mExpandedViewStub:Landroid/view/ViewStub;

    .line 230
    const v0, 0x7f0a01c5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeNumber:Landroid/widget/TextView;

    .line 231
    const v0, 0x7f0a0138

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 232
    const v0, 0x7f0a01c6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisodeTitle:Landroid/widget/TextView;

    .line 233
    const v0, 0x7f0a01c9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mAddedState:Landroid/widget/TextView;

    .line 234
    const v0, 0x7f0a01c8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EpisodeSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mAddedDrawable:Landroid/view/View;

    .line 235
    return-void
.end method

.method public setEpisodeDetails(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "season"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "episode"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "isNewPurchase"    # Z
    .param p6, "listener"    # Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mSeasonDocument:Lcom/google/android/finsky/api/model/Document;

    .line 241
    iput-object p2, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mEpisode:Lcom/google/android/finsky/api/model/Document;

    .line 242
    iput-object p4, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 243
    iput-object p3, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 244
    iput-boolean p5, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mIsNewPurchase:Z

    .line 245
    iput-object p6, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mCollapsedStateChangedListener:Lcom/google/android/finsky/layout/EpisodeSnippet$OnCollapsedStateChanged;

    .line 246
    iput-object p7, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 248
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 251
    iget-object v0, p0, Lcom/google/android/finsky/layout/EpisodeSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 252
    return-void
.end method
