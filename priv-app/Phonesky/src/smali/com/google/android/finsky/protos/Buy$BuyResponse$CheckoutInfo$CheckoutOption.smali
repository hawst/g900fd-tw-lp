.class public final Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Buy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckoutOption"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;


# instance fields
.field public disabledReason:[Ljava/lang/String;

.field public encodedAdjustedCart:Ljava/lang/String;

.field public footerHtml:[Ljava/lang/String;

.field public footnoteHtml:[Ljava/lang/String;

.field public formOfPayment:Ljava/lang/String;

.field public hasEncodedAdjustedCart:Z

.field public hasFormOfPayment:Z

.field public hasInstrumentFamily:Z

.field public hasInstrumentId:Z

.field public hasPurchaseCookie:Z

.field public hasSelectedInstrument:Z

.field public instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

.field public instrumentFamily:I

.field public instrumentId:Ljava/lang/String;

.field public item:[Lcom/google/android/finsky/protos/Buy$LineItem;

.field public purchaseCookie:Ljava/lang/String;

.field public selectedInstrument:Z

.field public subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

.field public summary:Lcom/google/android/finsky/protos/Buy$LineItem;

.field public total:Lcom/google/android/finsky/protos/Buy$LineItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 312
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->clear()Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    .line 313
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    .locals 2

    .prologue
    .line 252
    sget-object v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->_emptyArray:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    if-nez v0, :cond_1

    .line 253
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 255
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->_emptyArray:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    if-nez v0, :cond_0

    .line 256
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    sput-object v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->_emptyArray:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    .line 258
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->_emptyArray:[Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    return-object v0

    .line 258
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 316
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->formOfPayment:Ljava/lang/String;

    .line 317
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasFormOfPayment:Z

    .line 318
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentId:Ljava/lang/String;

    .line 319
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasInstrumentId:Z

    .line 320
    iput v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentFamily:I

    .line 321
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasInstrumentFamily:Z

    .line 322
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    .line 323
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->selectedInstrument:Z

    .line 324
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasSelectedInstrument:Z

    .line 325
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->summary:Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 326
    invoke-static {}, Lcom/google/android/finsky/protos/Buy$LineItem;->emptyArray()[Lcom/google/android/finsky/protos/Buy$LineItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 327
    invoke-static {}, Lcom/google/android/finsky/protos/Buy$LineItem;->emptyArray()[Lcom/google/android/finsky/protos/Buy$LineItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 328
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->total:Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 329
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    .line 330
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    .line 331
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->encodedAdjustedCart:Ljava/lang/String;

    .line 332
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasEncodedAdjustedCart:Z

    .line 333
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 334
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->purchaseCookie:Ljava/lang/String;

    .line 335
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasPurchaseCookie:Z

    .line 336
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->cachedSize:I

    .line 337
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 415
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 416
    .local v4, "size":I
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasFormOfPayment:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->formOfPayment:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 417
    :cond_0
    const/4 v5, 0x6

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->formOfPayment:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 420
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasEncodedAdjustedCart:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->encodedAdjustedCart:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 421
    :cond_2
    const/4 v5, 0x7

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->encodedAdjustedCart:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 424
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasInstrumentId:Z

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentId:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 425
    :cond_4
    const/16 v5, 0xf

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentId:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 428
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v5, v5

    if-lez v5, :cond_7

    .line 429
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v5, v5

    if-ge v3, v5, :cond_7

    .line 430
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    aget-object v2, v5, v3

    .line 431
    .local v2, "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    if-eqz v2, :cond_6

    .line 432
    const/16 v5, 0x10

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 429
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 437
    .end local v2    # "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    .end local v3    # "i":I
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v5, v5

    if-lez v5, :cond_9

    .line 438
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v5, v5

    if-ge v3, v5, :cond_9

    .line 439
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    aget-object v2, v5, v3

    .line 440
    .restart local v2    # "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    if-eqz v2, :cond_8

    .line 441
    const/16 v5, 0x11

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 438
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 446
    .end local v2    # "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    .end local v3    # "i":I
    :cond_9
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->total:Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v5, :cond_a

    .line 447
    const/16 v5, 0x12

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->total:Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 450
    :cond_a
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    if-eqz v5, :cond_d

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_d

    .line 451
    const/4 v0, 0x0

    .line 452
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 453
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_c

    .line 454
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 455
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_b

    .line 456
    add-int/lit8 v0, v0, 0x1

    .line 457
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 453
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 461
    .end local v2    # "element":Ljava/lang/String;
    :cond_c
    add-int/2addr v4, v1

    .line 462
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 464
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_d
    iget v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentFamily:I

    if-nez v5, :cond_e

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasInstrumentFamily:Z

    if-eqz v5, :cond_f

    .line 465
    :cond_e
    const/16 v5, 0x1d

    iget v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentFamily:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 468
    :cond_f
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasSelectedInstrument:Z

    if-nez v5, :cond_10

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->selectedInstrument:Z

    if-eqz v5, :cond_11

    .line 469
    :cond_10
    const/16 v5, 0x20

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->selectedInstrument:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 472
    :cond_11
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->summary:Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v5, :cond_12

    .line 473
    const/16 v5, 0x21

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->summary:Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 476
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_15

    .line 477
    const/4 v0, 0x0

    .line 478
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 479
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_14

    .line 480
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 481
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_13

    .line 482
    add-int/lit8 v0, v0, 0x1

    .line 483
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 479
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 487
    .end local v2    # "element":Ljava/lang/String;
    :cond_14
    add-int/2addr v4, v1

    .line 488
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 490
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v5, :cond_16

    .line 491
    const/16 v5, 0x2b

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 494
    :cond_16
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasPurchaseCookie:Z

    if-nez v5, :cond_17

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->purchaseCookie:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_18

    .line 495
    :cond_17
    const/16 v5, 0x2d

    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->purchaseCookie:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 498
    :cond_18
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    if-eqz v5, :cond_1b

    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_1b

    .line 499
    const/4 v0, 0x0

    .line 500
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 501
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1a

    .line 502
    iget-object v5, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 503
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_19

    .line 504
    add-int/lit8 v0, v0, 0x1

    .line 505
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 501
    :cond_19
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 509
    .end local v2    # "element":Ljava/lang/String;
    :cond_1a
    add-int/2addr v4, v1

    .line 510
    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    .line 512
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_1b
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 520
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 521
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 525
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 526
    :sswitch_0
    return-object p0

    .line 531
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->formOfPayment:Ljava/lang/String;

    .line 532
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasFormOfPayment:Z

    goto :goto_0

    .line 536
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->encodedAdjustedCart:Ljava/lang/String;

    .line 537
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasEncodedAdjustedCart:Z

    goto :goto_0

    .line 541
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentId:Ljava/lang/String;

    .line 542
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasInstrumentId:Z

    goto :goto_0

    .line 546
    :sswitch_4
    const/16 v6, 0x82

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 548
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-nez v6, :cond_2

    move v1, v5

    .line 549
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 551
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    if-eqz v1, :cond_1

    .line 552
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 554
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 555
    new-instance v6, Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Buy$LineItem;-><init>()V

    aput-object v6, v2, v1

    .line 556
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 557
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 554
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 548
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v1, v6

    goto :goto_1

    .line 560
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Buy$LineItem;-><init>()V

    aput-object v6, v2, v1

    .line 561
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 562
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    goto :goto_0

    .line 566
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    :sswitch_5
    const/16 v6, 0x8a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 568
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-nez v6, :cond_5

    move v1, v5

    .line 569
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 571
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    if-eqz v1, :cond_4

    .line 572
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 574
    :cond_4
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_6

    .line 575
    new-instance v6, Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Buy$LineItem;-><init>()V

    aput-object v6, v2, v1

    .line 576
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 577
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 574
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 568
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v1, v6

    goto :goto_3

    .line 580
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    :cond_6
    new-instance v6, Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Buy$LineItem;-><init>()V

    aput-object v6, v2, v1

    .line 581
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 582
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    goto/16 :goto_0

    .line 586
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Buy$LineItem;
    :sswitch_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->total:Lcom/google/android/finsky/protos/Buy$LineItem;

    if-nez v6, :cond_7

    .line 587
    new-instance v6, Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Buy$LineItem;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->total:Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 589
    :cond_7
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->total:Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 593
    :sswitch_7
    const/16 v6, 0x9a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 595
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    if-nez v6, :cond_9

    move v1, v5

    .line 596
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 597
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_8

    .line 598
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 600
    :cond_8
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_a

    .line 601
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 602
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 600
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 595
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_9
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_5

    .line 605
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 606
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    goto/16 :goto_0

    .line 610
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 611
    .local v4, "value":I
    sparse-switch v4, :sswitch_data_1

    goto/16 :goto_0

    .line 623
    :sswitch_9
    iput v4, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentFamily:I

    .line 624
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasInstrumentFamily:Z

    goto/16 :goto_0

    .line 630
    .end local v4    # "value":I
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->selectedInstrument:Z

    .line 631
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasSelectedInstrument:Z

    goto/16 :goto_0

    .line 635
    :sswitch_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->summary:Lcom/google/android/finsky/protos/Buy$LineItem;

    if-nez v6, :cond_b

    .line 636
    new-instance v6, Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Buy$LineItem;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->summary:Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 638
    :cond_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->summary:Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 642
    :sswitch_c
    const/16 v6, 0x11a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 644
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    if-nez v6, :cond_d

    move v1, v5

    .line 645
    .restart local v1    # "i":I
    :goto_7
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 646
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_c

    .line 647
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 649
    :cond_c
    :goto_8
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_e

    .line 650
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 651
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 649
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 644
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_d
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_7

    .line 654
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 655
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    goto/16 :goto_0

    .line 659
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_d
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-nez v6, :cond_f

    .line 660
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 662
    :cond_f
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 666
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->purchaseCookie:Ljava/lang/String;

    .line 667
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasPurchaseCookie:Z

    goto/16 :goto_0

    .line 671
    :sswitch_f
    const/16 v6, 0x182

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 673
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    if-nez v6, :cond_11

    move v1, v5

    .line 674
    .restart local v1    # "i":I
    :goto_9
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 675
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_10

    .line 676
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 678
    :cond_10
    :goto_a
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_12

    .line 679
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 680
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 678
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 673
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_11
    iget-object v6, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_9

    .line 683
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 684
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    goto/16 :goto_0

    .line 521
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x32 -> :sswitch_1
        0x3a -> :sswitch_2
        0x7a -> :sswitch_3
        0x82 -> :sswitch_4
        0x8a -> :sswitch_5
        0x92 -> :sswitch_6
        0x9a -> :sswitch_7
        0xe8 -> :sswitch_8
        0x100 -> :sswitch_a
        0x10a -> :sswitch_b
        0x11a -> :sswitch_c
        0x15a -> :sswitch_d
        0x16a -> :sswitch_e
        0x182 -> :sswitch_f
    .end sparse-switch

    .line 611
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_9
        0x1 -> :sswitch_9
        0x2 -> :sswitch_9
        0x3 -> :sswitch_9
        0x4 -> :sswitch_9
        0x5 -> :sswitch_9
        0x6 -> :sswitch_9
        0x7 -> :sswitch_9
        0x8 -> :sswitch_9
        0x9 -> :sswitch_9
        0x64 -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 343
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasFormOfPayment:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->formOfPayment:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 344
    :cond_0
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->formOfPayment:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 346
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasEncodedAdjustedCart:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->encodedAdjustedCart:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 347
    :cond_2
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->encodedAdjustedCart:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 349
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasInstrumentId:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 350
    :cond_4
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 352
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 353
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 354
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->item:[Lcom/google/android/finsky/protos/Buy$LineItem;

    aget-object v0, v2, v1

    .line 355
    .local v0, "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    if-eqz v0, :cond_6

    .line 356
    const/16 v2, 0x10

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 353
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 360
    .end local v0    # "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 361
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 362
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->subItem:[Lcom/google/android/finsky/protos/Buy$LineItem;

    aget-object v0, v2, v1

    .line 363
    .restart local v0    # "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    if-eqz v0, :cond_8

    .line 364
    const/16 v2, 0x11

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 361
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 368
    .end local v0    # "element":Lcom/google/android/finsky/protos/Buy$LineItem;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->total:Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v2, :cond_a

    .line 369
    const/16 v2, 0x12

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->total:Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 371
    :cond_a
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 372
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 373
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footerHtml:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 374
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_b

    .line 375
    const/16 v2, 0x13

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 372
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 379
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_c
    iget v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentFamily:I

    if-nez v2, :cond_d

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasInstrumentFamily:Z

    if-eqz v2, :cond_e

    .line 380
    :cond_d
    const/16 v2, 0x1d

    iget v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrumentFamily:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 382
    :cond_e
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasSelectedInstrument:Z

    if-nez v2, :cond_f

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->selectedInstrument:Z

    if-eqz v2, :cond_10

    .line 383
    :cond_f
    const/16 v2, 0x20

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->selectedInstrument:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 385
    :cond_10
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->summary:Lcom/google/android/finsky/protos/Buy$LineItem;

    if-eqz v2, :cond_11

    .line 386
    const/16 v2, 0x21

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->summary:Lcom/google/android/finsky/protos/Buy$LineItem;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 388
    :cond_11
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_13

    .line 389
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_13

    .line 390
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->footnoteHtml:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 391
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_12

    .line 392
    const/16 v2, 0x23

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 389
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 396
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_13
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v2, :cond_14

    .line 397
    const/16 v2, 0x2b

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 399
    :cond_14
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->hasPurchaseCookie:Z

    if-nez v2, :cond_15

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->purchaseCookie:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    .line 400
    :cond_15
    const/16 v2, 0x2d

    iget-object v3, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->purchaseCookie:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 402
    :cond_16
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_18

    .line 403
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_18

    .line 404
    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$BuyResponse$CheckoutInfo$CheckoutOption;->disabledReason:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 405
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_17

    .line 406
    const/16 v2, 0x30

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 403
    :cond_17
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 410
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_18
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 411
    return-void
.end method
