.class public Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;
.source "SetupWizardAddDcb3Activity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/content/Intent;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "instrumentStatus"    # Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    .param p2, "setupWizardParams"    # Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .prologue
    .line 25
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 26
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, p1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;->putIntentExtras(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;Landroid/content/Intent;)V

    .line 27
    const-string v1, "InstrumentActivity.setup_wizard_params"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 28
    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;->finish()V

    .line 51
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/SetupWizardUtils;->animateSliding(Landroid/app/Activity;Z)V

    .line 52
    return-void
.end method

.method protected getActivityLayout()I
    .locals 1

    .prologue
    .line 56
    const v0, 0x7f0401a5

    return v0
.end method

.method protected getBillingUiMode()I
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 80
    const/16 v0, 0x37e

    return v0
.end method

.method public hideProgress()V
    .locals 2

    .prologue
    .line 72
    const v0, 0x7f0a0388

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 73
    const v0, 0x7f0a0121

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 74
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 33
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "InstrumentActivity.setup_wizard_params"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .line 35
    .local v1, "params":Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;
    invoke-virtual {v1}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isLightTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    const v6, 0x7f0d01bf

    .line 37
    .local v6, "themeId":I
    :goto_0
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;->setTheme(I)V

    .line 39
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const/4 v2, 0x0

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/SetupWizardUtils;->configureBasicUi(Landroid/app/Activity;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;IZZZ)V

    .line 43
    return-void

    .line 35
    .end local v6    # "themeId":I
    :cond_0
    const v6, 0x7f0d01be

    goto :goto_0
.end method

.method public showProgress(I)V
    .locals 2
    .param p1, "messageId"    # I

    .prologue
    .line 66
    const v0, 0x7f0a0388

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 67
    const v0, 0x7f0a0121

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardAddDcb3Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 68
    return-void
.end method
