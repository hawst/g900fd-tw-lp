.class public final Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
.super Lcom/google/protobuf/nano/MessageNano;
.source "LibraryUpdateProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/LibraryUpdateProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibraryUpdate"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;


# instance fields
.field public corpus:I

.field public hasCorpus:Z

.field public hasHasMore:Z

.field public hasLibraryId:Z

.field public hasMore:Z

.field public hasServerToken:Z

.field public hasStatus:Z

.field public libraryId:Ljava/lang/String;

.field public mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

.field public serverToken:[B

.field public status:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 55
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .locals 2

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-nez v0, :cond_1

    .line 20
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 22
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    sput-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 25
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->status:I

    .line 59
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasStatus:Z

    .line 60
    iput v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->corpus:I

    .line 61
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasCorpus:Z

    .line 62
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->libraryId:Ljava/lang/String;

    .line 63
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasLibraryId:Z

    .line 64
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->serverToken:[B

    .line 65
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasServerToken:Z

    .line 66
    invoke-static {}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->emptyArray()[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    .line 67
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasMore:Z

    .line 68
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasHasMore:Z

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->cachedSize:I

    .line 70
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 104
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 105
    .local v2, "size":I
    iget v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->status:I

    if-ne v3, v4, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasStatus:Z

    if-eqz v3, :cond_1

    .line 106
    :cond_0
    iget v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->status:I

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 109
    :cond_1
    iget v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->corpus:I

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasCorpus:Z

    if-eqz v3, :cond_3

    .line 110
    :cond_2
    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->corpus:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 113
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasServerToken:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->serverToken:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_5

    .line 114
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->serverToken:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 117
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 118
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 119
    iget-object v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    aget-object v0, v3, v1

    .line 120
    .local v0, "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    if-eqz v0, :cond_6

    .line 121
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 118
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 126
    .end local v0    # "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    .end local v1    # "i":I
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasHasMore:Z

    if-nez v3, :cond_8

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasMore:Z

    if-eqz v3, :cond_9

    .line 127
    :cond_8
    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasMore:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 130
    :cond_9
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasLibraryId:Z

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->libraryId:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 131
    :cond_a
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->libraryId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 134
    :cond_b
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 142
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 143
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 147
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 148
    :sswitch_0
    return-object p0

    .line 153
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 154
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 158
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->status:I

    .line 159
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasStatus:Z

    goto :goto_0

    .line 165
    .end local v4    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 166
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 178
    :pswitch_2
    iput v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->corpus:I

    .line 179
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasCorpus:Z

    goto :goto_0

    .line 185
    .end local v4    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->serverToken:[B

    .line 186
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasServerToken:Z

    goto :goto_0

    .line 190
    :sswitch_4
    const/16 v6, 0x22

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 192
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    if-nez v6, :cond_2

    move v1, v5

    .line 193
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    .line 195
    .local v2, "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    if-eqz v1, :cond_1

    .line 196
    iget-object v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 198
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 199
    new-instance v6, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;-><init>()V

    aput-object v6, v2, v1

    .line 200
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 201
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 198
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 192
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    array-length v1, v6

    goto :goto_1

    .line 204
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;-><init>()V

    aput-object v6, v2, v1

    .line 205
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 206
    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    goto :goto_0

    .line 210
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasMore:Z

    .line 211
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasHasMore:Z

    goto :goto_0

    .line 215
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->libraryId:Ljava/lang/String;

    .line 216
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasLibraryId:Z

    goto/16 :goto_0

    .line 143
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    .line 154
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 166
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 76
    iget v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->status:I

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasStatus:Z

    if-eqz v2, :cond_1

    .line 77
    :cond_0
    iget v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->status:I

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 79
    :cond_1
    iget v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->corpus:I

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasCorpus:Z

    if-eqz v2, :cond_3

    .line 80
    :cond_2
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->corpus:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 82
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasServerToken:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->serverToken:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_5

    .line 83
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->serverToken:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 85
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 86
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 87
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    aget-object v0, v2, v1

    .line 88
    .local v0, "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    if-eqz v0, :cond_6

    .line 89
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 86
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    .end local v0    # "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    .end local v1    # "i":I
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasHasMore:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasMore:Z

    if-eqz v2, :cond_9

    .line 94
    :cond_8
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasMore:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 96
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasLibraryId:Z

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->libraryId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 97
    :cond_a
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->libraryId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 99
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 100
    return-void
.end method
