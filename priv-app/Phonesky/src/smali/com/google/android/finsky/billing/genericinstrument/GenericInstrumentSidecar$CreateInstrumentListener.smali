.class Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;
.super Ljava/lang/Object;
.source "GenericInstrumentSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateInstrumentListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$1;

    .prologue
    .line 325
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;-><init>(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;)V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 5
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 367
    const-string v0, "Error received for %s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getInstrumentType()Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$800(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 368
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    const/16 v1, 0x2c8

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(IILcom/android/volley/VolleyError;)V
    invoke-static {v0, v1, v4, p1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$600(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;IILcom/android/volley/VolleyError;)V

    .line 371
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mErrorHtml:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$302(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;Ljava/lang/String;)Ljava/lang/String;

    .line 372
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    const/4 v1, 0x3

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V
    invoke-static {v0, v1, v3}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$1500(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V

    .line 373
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;)V
    .locals 7
    .param p1, "response"    # Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x2c8

    const/4 v4, 0x2

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 329
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    # setter for: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;
    invoke-static {v0, p1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$1002(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;)Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .line 330
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    iget-object v1, p1, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    # setter for: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mErrorHtml:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$302(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;Ljava/lang/String;)Ljava/lang/String;

    .line 331
    iget v0, p1, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->result:I

    packed-switch v0, :pswitch_data_0

    .line 360
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown create instrument flow state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->result:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    const/16 v1, 0x2c7

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(I)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$400(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;I)V

    .line 335
    iget-object v0, p1, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->instrumentId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 336
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing instrument with success"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V
    invoke-static {v0, v4, v2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$1100(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V

    .line 363
    :goto_0
    return-void

    .line 342
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    const/16 v1, 0x2c9

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(I)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$400(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;I)V

    .line 344
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$1200(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V

    goto :goto_0

    .line 347
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(IILcom/android/volley/VolleyError;)V
    invoke-static {v0, v5, v3, v6}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$600(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;IILcom/android/volley/VolleyError;)V

    .line 350
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V
    invoke-static {v0, v3, v2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$1300(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V

    goto :goto_0

    .line 353
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(IILcom/android/volley/VolleyError;)V
    invoke-static {v0, v5, v4, v6}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$600(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;IILcom/android/volley/VolleyError;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->this$0:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V
    invoke-static {v0, v3, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->access$1400(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V

    goto :goto_0

    .line 331
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 325
    check-cast p1, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;->onResponse(Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;)V

    return-void
.end method
