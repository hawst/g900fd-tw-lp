.class public Lcom/google/android/finsky/layout/CreditCardNumberEditText;
.super Landroid/widget/EditText;
.source "CreditCardNumberEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/CreditCardNumberEditText$1;,
        Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;,
        Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;,
        Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;
    }
.end annotation


# instance fields
.field private mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

.field private mOnCreditCardTypeChangedListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

.field private mOnNumberEnteredListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;

.field private mOriginalTextColors:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .line 41
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/finsky/layout/CreditCardNumberEditText;Lcom/google/android/finsky/billing/creditcard/CreditCardType;)Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/CreditCardNumberEditText;
    .param p1, "x1"    # Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOnCreditCardTypeChangedListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOnNumberEnteredListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Landroid/content/res/ColorStateList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOriginalTextColors:Landroid/content/res/ColorStateList;

    return-object v0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Landroid/widget/EditText;->onFinishInflate()V

    .line 46
    const-string v0, "0123456789 "

    invoke-static {v0}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 47
    new-instance v0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;-><init>(Lcom/google/android/finsky/layout/CreditCardNumberEditText;Lcom/google/android/finsky/layout/CreditCardNumberEditText$1;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOriginalTextColors:Landroid/content/res/ColorStateList;

    .line 49
    return-void
.end method

.method public setOnCreditCardTypeChangedListener(Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;)V
    .locals 0
    .param p1, "onCreditCardTypeChangedListener"    # Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOnCreditCardTypeChangedListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

    .line 54
    return-void
.end method

.method public setOnNumberEnteredListener(Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;)V
    .locals 0
    .param p1, "onNumberEnteredListener"    # Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOnNumberEnteredListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;

    .line 58
    return-void
.end method
