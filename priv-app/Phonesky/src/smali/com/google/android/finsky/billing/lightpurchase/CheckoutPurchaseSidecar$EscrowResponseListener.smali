.class Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;
.super Ljava/lang/Object;
.source "CheckoutPurchaseSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EscrowResponseListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)V
    .locals 0

    .prologue
    .line 613
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$1;

    .prologue
    .line 613
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;-><init>(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 613
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;->onResponse(Ljava/lang/String;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;)V
    .locals 3
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 616
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2800(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x132

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(I[B)V

    .line 618
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    # setter for: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->mCvcEscrowHandle:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$2902(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;Ljava/lang/String;)Ljava/lang/String;

    .line 619
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar$EscrowResponseListener;->this$0:Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;

    const/16 v1, 0x8

    const/4 v2, 0x0

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->setState(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;->access$3000(Lcom/google/android/finsky/billing/lightpurchase/CheckoutPurchaseSidecar;II)V

    .line 620
    return-void
.end method
