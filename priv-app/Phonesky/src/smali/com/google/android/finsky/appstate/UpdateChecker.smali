.class public Lcom/google/android/finsky/appstate/UpdateChecker;
.super Ljava/lang/Object;
.source "UpdateChecker.java"


# instance fields
.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mContext:Landroid/content/Context;

.field private mDfeModel:Lcom/google/android/finsky/api/model/MultiWayUpdateController;

.field private final mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

.field private final mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private final mNotifier:Lcom/google/android/finsky/utils/Notifier;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/utils/Notifier;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p3, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p4, "installPolicies"    # Lcom/google/android/finsky/installer/InstallPolicies;
    .param p5, "installer"    # Lcom/google/android/finsky/receivers/Installer;
    .param p6, "notifier"    # Lcom/google/android/finsky/utils/Notifier;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mContext:Landroid/content/Context;

    .line 57
    iput-object p2, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 58
    iput-object p3, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 59
    iput-object p4, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    .line 60
    iput-object p5, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    .line 61
    iput-object p6, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/appstate/UpdateChecker;)Lcom/google/android/finsky/library/Libraries;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/UpdateChecker;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/appstate/UpdateChecker;)Lcom/google/android/finsky/appstate/AppStates;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/UpdateChecker;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/appstate/UpdateChecker;)Lcom/google/android/finsky/api/model/MultiWayUpdateController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/UpdateChecker;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mDfeModel:Lcom/google/android/finsky/api/model/MultiWayUpdateController;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/finsky/appstate/UpdateChecker;Lcom/google/android/finsky/api/model/MultiWayUpdateController;)Lcom/google/android/finsky/api/model/MultiWayUpdateController;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/UpdateChecker;
    .param p1, "x1"    # Lcom/google/android/finsky/api/model/MultiWayUpdateController;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mDfeModel:Lcom/google/android/finsky/api/model/MultiWayUpdateController;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/finsky/appstate/UpdateChecker;Ljava/util/List;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/UpdateChecker;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/appstate/UpdateChecker;->handleUpdates(Ljava/util/List;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/appstate/UpdateChecker;)Lcom/google/android/finsky/receivers/Installer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/UpdateChecker;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    return-object v0
.end method

.method private handleUpdates(Ljava/util/List;Ljava/lang/String;Z)V
    .locals 16
    .param p2, "logReason"    # Ljava/lang/String;
    .param p3, "invokedInJobService"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 201
    .local p1, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->autoUpdateFirstTimeForAccounts:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/android/finsky/config/G;->autoUpdateDeliveryHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v14, 0x0

    cmp-long v2, v4, v14

    if-lez v2, :cond_3

    const/4 v6, 0x1

    .line 203
    .local v6, "deferred":Z
    :goto_0
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->autoUpdateFirstTimeForAccounts:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 206
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/installer/InstallPolicies;->getApplicationsWithUpdates(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 208
    .local v9, "appsWithUpdates":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 209
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual {v2, v9}, Lcom/google/android/finsky/installer/InstallPolicies;->getApplicationsEligibleForAutoUpdate(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 212
    .local v3, "autoUpdateApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 213
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->isProjecting()Z

    move-result v10

    .line 214
    .local v10, "gearheadIsProjecting":Z
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 215
    .local v7, "autoUpdateWifiOnly":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual {v2}, Lcom/google/android/finsky/installer/InstallPolicies;->isWifiNetwork()Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual {v2}, Lcom/google/android/finsky/installer/InstallPolicies;->isMobileHotspot()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v11, 0x1

    .line 217
    .local v11, "isOkToUpdateOnWifi":Z
    :goto_1
    invoke-static {}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;->isJobSchedulerEnabled()Z

    move-result v12

    .line 227
    .local v12, "jobSchedulerEnabled":Z
    if-nez v10, :cond_5

    if-eqz v7, :cond_0

    if-nez p3, :cond_0

    if-eqz v11, :cond_5

    if-nez v12, :cond_5

    .line 229
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    const-string v4, "auto_update"

    const/4 v5, 0x0

    const/4 v8, 0x3

    invoke-interface/range {v2 .. v8}, Lcom/google/android/finsky/receivers/Installer;->updateInstalledApps(Ljava/util/List;Ljava/lang/String;ZZZI)V

    .line 238
    if-eqz v6, :cond_1

    .line 239
    const-string v2, "Auto-update of %d packages will defer for %d ms"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    sget-object v8, Lcom/google/android/finsky/config/G;->autoUpdateDeliveryHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 241
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v4, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v5, Lcom/google/android/finsky/appstate/UpdateChecker$2;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/finsky/appstate/UpdateChecker$2;-><init>(Lcom/google/android/finsky/appstate/UpdateChecker;)V

    sget-object v2, Lcom/google/android/finsky/config/G;->autoUpdateDeliveryHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v4, v5, v14, v15}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 248
    :cond_1
    invoke-interface {v9, v3}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 251
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/appstate/UpdateChecker;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v2}, Lcom/google/android/finsky/utils/Notifier;->hideUpdatesAvailableMessage()V

    .line 252
    if-eqz v7, :cond_2

    .line 253
    const/4 v2, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-static {v2, v0, v4}, Lcom/google/android/finsky/appstate/UpdateChecker;->logWifiAutoUpdate(ZLjava/lang/String;Z)V

    .line 276
    .end local v3    # "autoUpdateApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    .end local v7    # "autoUpdateWifiOnly":Z
    .end local v10    # "gearheadIsProjecting":Z
    .end local v11    # "isOkToUpdateOnWifi":Z
    .end local v12    # "jobSchedulerEnabled":Z
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/finsky/appstate/UpdateChecker;->showUpdateNotifications(Ljava/util/List;)V

    .line 277
    return-void

    .line 201
    .end local v6    # "deferred":Z
    .end local v9    # "appsWithUpdates":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    :cond_3
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 215
    .restart local v3    # "autoUpdateApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    .restart local v6    # "deferred":Z
    .restart local v7    # "autoUpdateWifiOnly":Z
    .restart local v9    # "appsWithUpdates":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    .restart local v10    # "gearheadIsProjecting":Z
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 255
    .restart local v11    # "isOkToUpdateOnWifi":Z
    .restart local v12    # "jobSchedulerEnabled":Z
    :cond_5
    if-eqz v10, :cond_6

    .line 256
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;->scheduleCheck(I)V

    .line 258
    const/4 v2, 0x0

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-static {v2, v0, v4}, Lcom/google/android/finsky/appstate/UpdateChecker;->logWifiAutoUpdate(ZLjava/lang/String;Z)V

    goto :goto_2

    .line 260
    :cond_6
    if-eqz v7, :cond_2

    .line 262
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;->scheduleCheck(I)V

    .line 264
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-static {v2, v0, v4}, Lcom/google/android/finsky/appstate/UpdateChecker;->logWifiAutoUpdate(ZLjava/lang/String;Z)V

    goto :goto_2

    .line 268
    .end local v7    # "autoUpdateWifiOnly":Z
    .end local v10    # "gearheadIsProjecting":Z
    .end local v11    # "isOkToUpdateOnWifi":Z
    .end local v12    # "jobSchedulerEnabled":Z
    :cond_7
    invoke-static {}, Lcom/google/android/finsky/appstate/UpdateChecker;->logNoWifiAutoUpdate()V

    goto :goto_2

    .line 272
    .end local v3    # "autoUpdateApps":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    :cond_8
    invoke-static {}, Lcom/google/android/finsky/appstate/UpdateChecker;->logNoWifiAutoUpdate()V

    goto :goto_2
.end method

.method public static logNoWifiAutoUpdate()V
    .locals 4

    .prologue
    .line 540
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->wifiAutoUpdateFailedAttempts:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 541
    .local v0, "numFailedAttempts":I
    if-lez v0, :cond_0

    .line 542
    const/4 v1, 0x1

    const-string v2, "auto_update_obsolete"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/appstate/UpdateChecker;->logWifiAutoUpdate(ZLjava/lang/String;Z)V

    .line 544
    :cond_0
    return-void
.end method

.method public static logWifiAutoUpdate(ZLjava/lang/String;Z)V
    .locals 16
    .param p0, "success"    # Z
    .param p1, "logReason"    # Ljava/lang/String;
    .param p2, "projecting"    # Z

    .prologue
    .line 492
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    .line 493
    .local v3, "log":Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 494
    .local v4, "currentTimeMs":J
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;-><init>()V

    .line 496
    .local v2, "attempt":Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;
    sget-object v9, Lcom/google/android/finsky/utils/FinskyPreferences;->wifiAutoUpdateFailedAttempts:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v9}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 497
    .local v8, "numFailedAttempts":I
    if-eqz p0, :cond_2

    .line 498
    iput v8, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->numFailedAttempts:I

    .line 499
    const/4 v8, 0x0

    .line 504
    :goto_0
    const/4 v9, 0x1

    iput-boolean v9, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasNumFailedAttempts:Z

    .line 506
    move/from16 v0, p2

    iput-boolean v0, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->skippedDueToProjection:Z

    .line 508
    sget-object v9, Lcom/google/android/finsky/utils/FinskyPreferences;->wifiAutoUpdateFirstFailMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v9}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 509
    .local v6, "firstFailMs":J
    const-wide/16 v10, 0x0

    .line 510
    .local v10, "timeSinceFirstFailMs":J
    const-wide/16 v14, 0x0

    cmp-long v9, v6, v14

    if-eqz v9, :cond_0

    .line 511
    sub-long v10, v4, v6

    .line 513
    :cond_0
    iput-wide v10, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->timeSinceFirstFailMs:J

    .line 514
    const/4 v9, 0x1

    iput-boolean v9, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasTimeSinceFirstFailMs:Z

    .line 515
    if-eqz p0, :cond_3

    .line 516
    const-wide/16 v6, 0x0

    .line 523
    :cond_1
    :goto_1
    sget-object v9, Lcom/google/android/finsky/config/G;->autoUpdateWifiCheckIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v9}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 524
    .local v12, "wifiCheckIntervalMs":J
    move/from16 v0, p0

    iput-boolean v0, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->autoUpdateSuccess:Z

    .line 525
    const/4 v9, 0x1

    iput-boolean v9, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasAutoUpdateSuccess:Z

    .line 526
    iput-wide v12, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->wifiCheckIntervalMs:J

    .line 527
    const/4 v9, 0x1

    iput-boolean v9, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasWifiCheckIntervalMs:Z

    .line 529
    move-object/from16 v0, p1

    invoke-virtual {v3, v2, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logWifiAutoUpdateAttempt(Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;Ljava/lang/String;)V

    .line 531
    sget-object v9, Lcom/google/android/finsky/utils/FinskyPreferences;->wifiAutoUpdateFailedAttempts:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v9, v14}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 532
    sget-object v9, Lcom/google/android/finsky/utils/FinskyPreferences;->wifiAutoUpdateFirstFailMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v9, v14}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 533
    return-void

    .line 501
    .end local v6    # "firstFailMs":J
    .end local v10    # "timeSinceFirstFailMs":J
    .end local v12    # "wifiCheckIntervalMs":J
    :cond_2
    add-int/lit8 v8, v8, 0x1

    .line 502
    iput v8, v2, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->numFailedAttempts:I

    goto :goto_0

    .line 518
    .restart local v6    # "firstFailMs":J
    .restart local v10    # "timeSinceFirstFailMs":J
    :cond_3
    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 519
    move-wide v6, v4

    goto :goto_1
.end method

.method private markAppsAsNotified(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 351
    .local p1, "docsNotified":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 352
    .local v1, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    .line 353
    .local v0, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    iget-object v4, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 354
    .local v4, "packageName":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v6}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v6

    iget-object v7, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-interface {v6, v7}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v3

    .line 356
    .local v3, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    iget v5, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    .line 357
    .local v5, "version":I
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getLastNotifiedVersion()I

    move-result v6

    if-le v5, v6, :cond_0

    .line 359
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v6}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v6

    invoke-interface {v6, v4, v5}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setLastNotifiedVersion(Ljava/lang/String;I)V

    goto :goto_0

    .line 362
    .end local v0    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    .end local v1    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v5    # "version":I
    :cond_2
    return-void
.end method

.method public static migrateAllAppsToUseGlobalUpdateSetting(Lcom/google/android/finsky/appstate/AppStates;ZLcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;Ljava/lang/String;)V
    .locals 12
    .param p0, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p1, "clobberUserSettings"    # Z
    .param p2, "defaultState"    # Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;
    .param p3, "logReason"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 462
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x194

    move-object v3, p3

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 466
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v9

    .line 467
    .local v9, "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    invoke-interface {v9}, Lcom/google/android/finsky/appstate/InstallerDataStore;->getAll()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 469
    .local v8, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v8}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isGmsCore(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    invoke-virtual {v8}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAutoUpdate()Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    move-result-object v10

    .line 475
    .local v10, "oldState":Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;
    if-nez p1, :cond_1

    sget-object v0, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->DEFAULT:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    if-ne v10, v0, :cond_0

    .line 477
    :cond_1
    invoke-virtual {v8}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v11

    .line 478
    .local v11, "packageName":Ljava/lang/String;
    invoke-interface {v9, v11, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setAutoUpdate(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)V

    .line 479
    const-string v0, "Migrate %s autoupdate from default to %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v11, v1, v4

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 483
    .end local v8    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .end local v10    # "oldState":Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;
    .end local v11    # "packageName":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public static migrateAutoUpdateSettings(Lcom/google/android/finsky/appstate/AppStates;)V
    .locals 8
    .param p0, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 384
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 385
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/AppStates;->isLoaded()Z

    move-result v5

    if-nez v5, :cond_0

    .line 386
    const-string v5, "Require loaded app states to migrate auto-update state."

    new-array v7, v3, [Ljava/lang/Object;

    invoke-static {v5, v7}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390
    :cond_0
    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_BY_DEFAULT:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v2

    .line 391
    .local v2, "oldGlobalExists":Z
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v1

    .line 393
    .local v1, "newGlobalExists":Z
    if-nez v2, :cond_1

    if-eqz v1, :cond_1

    .line 443
    :goto_0
    return-void

    .line 404
    :cond_1
    if-nez v2, :cond_2

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getVersionCodeOfLastRun()I

    move-result v5

    const/4 v7, -0x1

    if-ne v5, v7, :cond_2

    .line 406
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 407
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0

    .line 413
    :cond_2
    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_BY_DEFAULT:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 415
    .local v0, "autoUpdateByDefault":Z
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 419
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v5

    if-nez v5, :cond_4

    .line 420
    const/4 v4, 0x1

    .line 421
    .local v4, "wifiOnly":Z
    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->exists()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 422
    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 424
    :cond_3
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 429
    .end local v4    # "wifiOnly":Z
    :cond_4
    if-nez v0, :cond_5

    move v3, v6

    .line 433
    .local v3, "shouldClobberSettings":Z
    :cond_5
    sget-object v5, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->USE_GLOBAL:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    const-string v7, "version"

    invoke-static {p0, v3, v5, v7}, Lcom/google/android/finsky/appstate/UpdateChecker;->migrateAllAppsToUseGlobalUpdateSetting(Lcom/google/android/finsky/appstate/AppStates;ZLcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;Ljava/lang/String;)V

    .line 437
    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_BY_DEFAULT:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 438
    sget-object v5, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 442
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->hadPreJsAutoUpdateSettings:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setAllAppsToUseGlobalDefault(Lcom/google/android/finsky/appstate/AppStates;)V
    .locals 3
    .param p0, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;

    .prologue
    .line 449
    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->USE_GLOBAL:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    const-string v2, "cleanup"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/finsky/appstate/UpdateChecker;->migrateAllAppsToUseGlobalUpdateSetting(Lcom/google/android/finsky/appstate/AppStates;ZLcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;Ljava/lang/String;)V

    .line 451
    return-void
.end method

.method private showUpdateNotifications(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "allUpdates":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    iget-object v10, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    invoke-virtual {v10, p1}, Lcom/google/android/finsky/installer/InstallPolicies;->getApplicationsEligibleForNewUpdateNotification(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    .line 301
    .local v3, "newUpdates":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-direct {p0, v3}, Lcom/google/android/finsky/appstate/UpdateChecker;->markAppsAsNotified(Ljava/util/List;)V

    .line 304
    sget-object v10, Lcom/google/android/finsky/utils/VendingPreferences;->NOTIFY_UPDATES:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v10}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 308
    iget-object v10, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    const/4 v11, 0x1

    invoke-virtual {v10, p1, v11}, Lcom/google/android/finsky/installer/InstallPolicies;->getAppsThatRequireUpdateWarnings(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v0

    .line 310
    .local v0, "allUpdatesApprovalRequired":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v10, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    const/4 v11, 0x1

    invoke-virtual {v10, v3, v11}, Lcom/google/android/finsky/installer/InstallPolicies;->getAppsThatRequireUpdateWarnings(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v4

    .line 314
    .local v4, "newUpdatesApprovalRequired":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    sget-object v10, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v10}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 316
    .local v1, "autoUpdateEnabled":Z
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    .line 317
    .local v6, "numAllUpdates":I
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    .line 319
    .local v5, "numAllApproval":I
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    .line 320
    .local v7, "numNewUpdates":I
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_2

    const/4 v2, 0x1

    .line 322
    .local v2, "doNewUpdatesNeedApproval":Z
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    sget-object v10, Lcom/google/android/finsky/utils/FinskyPreferences;->lastUpdateAvailNotificationTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v10}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long v8, v12, v10

    .line 326
    .local v8, "timeSinceLastUpdateNoti":J
    if-nez v1, :cond_3

    if-lez v7, :cond_3

    .line 327
    const-string v10, "Notifying user that [%d/%d] applications have new updates."

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 329
    iget-object v10, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v10, v3, v6}, Lcom/google/android/finsky/utils/Notifier;->showNewUpdatesAvailableMessage(Ljava/util/List;I)V

    .line 330
    sget-object v10, Lcom/google/android/finsky/utils/FinskyPreferences;->lastUpdateAvailNotificationTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 320
    .end local v2    # "doNewUpdatesNeedApproval":Z
    .end local v8    # "timeSinceLastUpdateNoti":J
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 333
    .restart local v2    # "doNewUpdatesNeedApproval":Z
    .restart local v8    # "timeSinceLastUpdateNoti":J
    :cond_3
    if-eqz v1, :cond_4

    if-eqz v2, :cond_4

    .line 334
    const-string v10, "Notifying user [%d/%d] applications have updates that require approval."

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 337
    iget-object v10, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v10, v0}, Lcom/google/android/finsky/utils/Notifier;->showUpdatesNeedApprovalMessage(Ljava/util/List;)V

    .line 338
    sget-object v10, Lcom/google/android/finsky/utils/FinskyPreferences;->lastUpdateAvailNotificationTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 341
    :cond_4
    sget-object v10, Lcom/google/android/finsky/config/G;->outstandingNotificationTimeDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v10}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v10, v8, v10

    if-lez v10, :cond_0

    .line 342
    const-string v10, "Notifying user that %d applications have outstanding updates."

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 344
    iget-object v10, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mNotifier:Lcom/google/android/finsky/utils/Notifier;

    invoke-interface {v10, p1}, Lcom/google/android/finsky/utils/Notifier;->showOutstandingUpdatesMessage(Ljava/util/List;)V

    .line 345
    sget-object v10, Lcom/google/android/finsky/utils/FinskyPreferences;->lastUpdateAvailNotificationTimestampMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public checkForUpdates(Ljava/lang/Runnable;Ljava/lang/Runnable;Ljava/lang/String;Z)V
    .locals 10
    .param p1, "successRunnable"    # Ljava/lang/Runnable;
    .param p2, "errorRunnable"    # Ljava/lang/Runnable;
    .param p3, "logReason"    # Ljava/lang/String;
    .param p4, "invokedInJobService"    # Z

    .prologue
    const/4 v9, 0x0

    .line 82
    if-nez p4, :cond_0

    .line 83
    invoke-static {}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;->cancelCheck()V

    .line 88
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v2

    .line 89
    .local v2, "currentAccount":Landroid/accounts/Account;
    if-nez v2, :cond_2

    .line 90
    if-eqz p1, :cond_1

    .line 91
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 195
    :cond_1
    :goto_0
    return-void

    .line 96
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/AppStates;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_3

    .line 97
    const-string v0, "Require loaded app states to perform update check."

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    if-eqz p2, :cond_1

    .line 99
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 103
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v0}, Lcom/google/android/finsky/library/Libraries;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_4

    .line 104
    const-string v0, "Require loaded libraries to perform update check."

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 105
    if-eqz p2, :cond_1

    .line 106
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 110
    :cond_4
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->isReady()Z

    move-result v0

    if-nez v0, :cond_5

    .line 111
    const-string v0, "Require initialized Gearhead monitor to perform update check."

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    if-eqz p2, :cond_1

    .line 113
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 118
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-static {v0}, Lcom/google/android/finsky/appstate/UpdateChecker;->migrateAutoUpdateSettings(Lcom/google/android/finsky/appstate/AppStates;)V

    .line 120
    new-instance v5, Lcom/google/android/finsky/appstate/GmsCoreHelper;

    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v1, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v4, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mContext:Landroid/content/Context;

    invoke-direct {v5, v0, v1, v4}, Lcom/google/android/finsky/appstate/GmsCoreHelper;-><init>(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Landroid/content/Context;)V

    .line 121
    .local v5, "gmsCoreHelper":Lcom/google/android/finsky/appstate/GmsCoreHelper;
    iget-object v0, p0, Lcom/google/android/finsky/appstate/UpdateChecker;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v0}, Lcom/google/android/finsky/library/Libraries;->getLoadedAccountHash()I

    move-result v3

    .line 124
    .local v3, "accountHashBeforeAsyncTask":I
    new-instance v0, Lcom/google/android/finsky/appstate/UpdateChecker$1;

    move-object v1, p0

    move-object v4, p2

    move-object v6, p3

    move v7, p4

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/finsky/appstate/UpdateChecker$1;-><init>(Lcom/google/android/finsky/appstate/UpdateChecker;Landroid/accounts/Account;ILjava/lang/Runnable;Lcom/google/android/finsky/appstate/GmsCoreHelper;Ljava/lang/String;ZLjava/lang/Runnable;)V

    new-array v1, v9, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/UpdateChecker$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
