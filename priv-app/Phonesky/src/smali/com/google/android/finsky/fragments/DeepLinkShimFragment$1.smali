.class final Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;
.super Ljava/lang/Object;
.source "DeepLinkShimFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->saveExternalReferrer(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$appStates:Lcom/google/android/finsky/appstate/AppStates;

.field final synthetic val$externalReferrer:Ljava/lang/String;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/appstate/AppStates;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$appStates:Lcom/google/android/finsky/appstate/AppStates;

    iput-object p2, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$externalReferrer:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 293
    iget-object v6, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$appStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v7, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    .line 294
    .local v0, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    iget-object v6, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$appStates:Lcom/google/android/finsky/appstate/AppStates;

    invoke-virtual {v6}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v4

    .line 295
    .local v4, "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v3

    .line 296
    .local v3, "installer":Lcom/google/android/finsky/receivers/Installer;
    iget-object v6, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$packageName:Ljava/lang/String;

    invoke-interface {v3, v6}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v5

    .line 297
    .local v5, "installerState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    iget-object v6, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$externalReferrer:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$packageName:Ljava/lang/String;

    invoke-static {v6, v7, v0, v5, v4}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->saveReferrer(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates$AppState;Lcom/google/android/finsky/receivers/Installer$InstallerState;Lcom/google/android/finsky/appstate/InstallerDataStore;)Ljava/lang/String;

    move-result-object v1

    .line 301
    .local v1, "dropReason":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 302
    const-string v6, "Capture referrer for %s"

    new-array v7, v10, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$packageName:Ljava/lang/String;

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    const/16 v6, 0x203

    iget-object v7, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$packageName:Ljava/lang/String;

    const/4 v8, -0x1

    const/4 v9, 0x0

    # invokes: Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->logExternalReferrer(ILjava/lang/String;ILjava/lang/String;)V
    invoke-static {v6, v7, v8, v9}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->access$000(ILjava/lang/String;ILjava/lang/String;)V

    .line 314
    :goto_0
    return-void

    .line 306
    :cond_0
    const-string v6, "Dropped referrer for %s because %s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$packageName:Ljava/lang/String;

    aput-object v8, v7, v9

    aput-object v1, v7, v10

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    const/4 v2, -0x1

    .line 308
    .local v2, "installedVersion":I
    if-eqz v0, :cond_1

    iget-object v6, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v6, :cond_1

    .line 309
    iget-object v6, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget v2, v6, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 311
    :cond_1
    const/16 v6, 0x204

    iget-object v7, p0, Lcom/google/android/finsky/fragments/DeepLinkShimFragment$1;->val$packageName:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->logExternalReferrer(ILjava/lang/String;ILjava/lang/String;)V
    invoke-static {v6, v7, v2, v1}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->access$000(ILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method
