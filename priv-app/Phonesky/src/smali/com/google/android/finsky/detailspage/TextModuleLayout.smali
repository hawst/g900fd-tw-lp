.class public Lcom/google/android/finsky/detailspage/TextModuleLayout;
.super Landroid/widget/LinearLayout;
.source "TextModuleLayout.java"


# instance fields
.field private mBodyTopPaddingView:Landroid/view/View;

.field private mBodyView:Lcom/google/android/play/layout/PlayTextView;

.field private mContentView:Landroid/view/ViewGroup;

.field private mFooterLabel:Landroid/widget/TextView;

.field private mHeaderView:Landroid/widget/TextView;

.field private mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

.field private mMaxLines:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/detailspage/TextModuleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 58
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mMaxLines:I

    .line 59
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 63
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 65
    const v0, 0x7f0a0173

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/detailspage/TextModuleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mHeaderView:Landroid/widget/TextView;

    .line 66
    const v0, 0x7f0a00b4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/detailspage/TextModuleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mContentView:Landroid/view/ViewGroup;

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mContentView:Landroid/view/ViewGroup;

    const v1, 0x7f0a03ad

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mBodyTopPaddingView:Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mContentView:Landroid/view/ViewGroup;

    const v1, 0x7f0a018d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    .line 69
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mBodyView:Lcom/google/android/play/layout/PlayTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mContentView:Landroid/view/ViewGroup;

    const v1, 0x7f0a0191

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mContentView:Landroid/view/ViewGroup;

    const v1, 0x7f0a0193

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mFooterLabel:Landroid/widget/TextView;

    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/TextModuleLayout;->mFooterLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/detailspage/TextModuleLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0383

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void
.end method
