.class public Lcom/google/android/finsky/utils/PermissionsBucketer;
.super Ljava/lang/Object;
.source "PermissionsBucketer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;,
        Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    }
.end annotation


# static fields
.field protected static final BILLING_PERMISSIONS_BUCKET:[Ljava/lang/String;

.field protected static final BLUETOOTH_CONNECTION_INFORMATION:[Ljava/lang/String;

.field protected static final CALENDAR_PERMISSIONS:[Ljava/lang/String;

.field protected static final CAMERA_PERMISSIONS:[Ljava/lang/String;

.field protected static final CONTACTS_PERMISSIONS:[Ljava/lang/String;

.field protected static final DEVICE_AND_APP_HISTORY_PERMISSIONS:[Ljava/lang/String;

.field protected static final DEVICE_ID_AND_CALL_INFORMATION:[Ljava/lang/String;

.field protected static final IDENTITY_PERMISSIONS:[Ljava/lang/String;

.field protected static final IGNORED_PERMISSIONS:[Ljava/lang/String;

.field protected static final LOCATION_PERMISSIONS:[Ljava/lang/String;

.field protected static final MIC_PERMISSIONS:[Ljava/lang/String;

.field protected static final PHONE_PERMISSIONS:[Ljava/lang/String;

.field protected static final PHOTOS_MEDIA_FILES_PERMISSIONS:[Ljava/lang/String;

.field protected static final REDIRECT_INTERNET_TRAFFIC:[Ljava/lang/String;

.field protected static final SMS_PERMISSIONS:[Ljava/lang/String;

.field protected static final WEARABLE_ACTIVITY_PERMISSIONS:[Ljava/lang/String;

.field protected static final WIFI_CONNECTION_INFORMATION:[Ljava/lang/String;

.field private static sCachedIgnoredPermissionSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sCachedPermissionsMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "com.android.vending.BILLING"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->BILLING_PERMISSIONS_BUCKET:[Ljava/lang/String;

    .line 40
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "android.permission.GET_ACCOUNTS"

    aput-object v1, v0, v3

    const-string v1, "android.permission.MANAGE_ACCOUNTS"

    aput-object v1, v0, v4

    const-string v1, "android.permission.READ_PROFILE"

    aput-object v1, v0, v5

    const-string v1, "android.permission.WRITE_PROFILE"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->IDENTITY_PERMISSIONS:[Ljava/lang/String;

    .line 47
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "android.permission.READ_CALENDAR"

    aput-object v1, v0, v3

    const-string v1, "android.permission.WRITE_CALENDAR"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->CALENDAR_PERMISSIONS:[Ljava/lang/String;

    .line 52
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "android.permission.READ_CONTACTS"

    aput-object v1, v0, v3

    const-string v1, "android.permission.WRITE_CONTACTS"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->CONTACTS_PERMISSIONS:[Ljava/lang/String;

    .line 57
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v1, v0, v3

    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v1, v0, v4

    const-string v1, "android.permission.ACCESS_LOCATION_EXTRA_COMMANDS"

    aput-object v1, v0, v5

    const-string v1, "android.permission.ACCESS_GPS"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->LOCATION_PERMISSIONS:[Ljava/lang/String;

    .line 64
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.RECEIVE_SMS"

    aput-object v1, v0, v3

    const-string v1, "android.permission.READ_SMS"

    aput-object v1, v0, v4

    const-string v1, "android.permission.WRITE_SMS"

    aput-object v1, v0, v5

    const-string v1, "android.permission.SEND_SMS"

    aput-object v1, v0, v6

    const-string v1, "android.permission.RECEIVE_MMS"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "android.permission.RECEIVE_WAP_PUSH"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->SMS_PERMISSIONS:[Ljava/lang/String;

    .line 73
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CALL_PHONE"

    aput-object v1, v0, v3

    const-string v1, "android.permission.WRITE_CALL_LOG"

    aput-object v1, v0, v4

    const-string v1, "android.permission.READ_CALL_LOG"

    aput-object v1, v0, v5

    const-string v1, "android.permission.CALL_PRIVILEGED"

    aput-object v1, v0, v6

    const-string v1, "android.permission.PROCESS_OUTGOING_CALLS"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "android.permission.MODIFY_PHONE_STATE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->PHONE_PERMISSIONS:[Ljava/lang/String;

    .line 82
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v4

    const-string v1, "android.permission.MOUNT_FORMAT_FILESYSTEMS"

    aput-object v1, v0, v5

    const-string v1, "android.permission.MOUNT_UNMOUNT_FILESYSTEMS"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->PHOTOS_MEDIA_FILES_PERMISSIONS:[Ljava/lang/String;

    .line 89
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v3

    const-string v1, "android.permission.RECORD_VIDEO"

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->CAMERA_PERMISSIONS:[Ljava/lang/String;

    .line 94
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->MIC_PERMISSIONS:[Ljava/lang/String;

    .line 98
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "android.permission.READ_LOGS"

    aput-object v1, v0, v3

    const-string v1, "android.permission.GET_TASKS"

    aput-object v1, v0, v4

    const-string v1, "android.permission.DUMP"

    aput-object v1, v0, v5

    const-string v1, "com.android.browser.permission.READ_HISTORY_BOOKMARKS"

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->DEVICE_AND_APP_HISTORY_PERMISSIONS:[Ljava/lang/String;

    .line 105
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.WRITE_APN_SETTINGS"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->REDIRECT_INTERNET_TRAFFIC:[Ljava/lang/String;

    .line 109
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.ACCESS_WIFI_STATE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->WIFI_CONNECTION_INFORMATION:[Ljava/lang/String;

    .line 113
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->BLUETOOTH_CONNECTION_INFORMATION:[Ljava/lang/String;

    .line 117
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.READ_PHONE_STATE"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->DEVICE_ID_AND_CALL_INFORMATION:[Ljava/lang/String;

    .line 122
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "android.permission.BODY_SENSORS"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->WEARABLE_ACTIVITY_PERMISSIONS:[Ljava/lang/String;

    .line 130
    const/16 v0, 0x31

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.ACCESS_MOCK_LOCATION"

    aput-object v1, v0, v3

    const-string v1, "android.permission.ACCESS_NETWORK_STATE"

    aput-object v1, v0, v4

    const-string v1, "android.permission.ACCOUNT_MANAGER"

    aput-object v1, v0, v5

    const-string v1, "android.permission.AUTHENTICATE_ACCOUNTS"

    aput-object v1, v0, v6

    const-string v1, "android.permission.BATTERY_STATS"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "android.permission.BLUETOOTH"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "android.permission.BROADCAST_STICKY"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "android.permission.CHANGE_CONFIGURATION"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "android.permission.CHANGE_NETWORK_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "android.permission.CHANGE_WIFI_MULTICAST_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "android.permission.CHANGE_WIFI_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "android.permission.CHANGE_WIMAX_STATE"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "android.permission.CLEAR_APP_CACHE"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "android.permission.DISABLE_KEYGUARD"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "android.permission.EXPAND_STATUS_BAR"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "android.permission.FLASHLIGHT"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "android.permission.GET_PACKAGE_SIZE"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "android.permission.INTERNET"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "android.permission.KILL_BACKGROUND_PROCESSES"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "android.permission.MODIFY_AUDIO_SETTINGS"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "android.permission.NFC"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "android.permission.PERSISTENT_ACTIVITY"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "android.permission.READ_SYNC_SETTINGS"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "android.permission.READ_USER_DICTIONARY"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "android.permission.RECEIVE_BOOT_COMPLETED"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "android.permission.REORDER_TASKS"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "android.permission.SERIAL_PORT"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "android.permission.SET_ALWAYS_FINISH"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "android.permission.SET_ANIMATION_SCALE"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "android.permission.SET_DEBUG_APP"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "android.permission.SET_PREFERRED_APPLICATIONS"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "android.permission.SET_PROCESS_LIMIT"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "android.permission.SET_TIME_ZONE"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "android.permission.SET_WALLPAPER"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "android.permission.SIGNAL_PERSISTENT_PROCESSES"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "android.permission.SYSTEM_ALERT_WINDOW"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "android.permission.USE_CREDENTIALS"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "android.permission.USE_SIP"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "android.permission.VIBRATE"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "android.permission.WAKE_LOCK"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "android.permission.WRITE_SETTINGS"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "android.permission.WRITE_SYNC_SETTINGS"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "android.permission.WRITE_USER_DICTIONARY"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "com.android.alarm.permission.SET_ALARM"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "com.android.browser.permission.WRITE_HISTORY_BOOKMARKS"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "com.android.launcher.permission.INSTALL_SHORTCUT"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "com.android.launcher.permission.UNINSTALL_SHORTCUT"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "com.android.vending.CHECK_LICENSE"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "com.google.android.providers.gsf.permission.READ_GSERVICES"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->IGNORED_PERMISSIONS:[Ljava/lang/String;

    .line 205
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->sCachedPermissionsMap:Ljava/util/Map;

    .line 207
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->sCachedIgnoredPermissionSet:Ljava/util/Set;

    return-void
.end method

.method private static bucketPermissions(Ljava/util/Map;Ljava/util/Set;Z)[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .locals 8
    .param p2, "showIgnoredPermissionsInOtherBucket"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)[",
            "Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;"
        }
    .end annotation

    .prologue
    .line 442
    .local p0, "permissionToBucketMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    .local p1, "permissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 443
    .local v4, "mPermissionBuckets":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 445
    .local v5, "permission":Ljava/lang/String;
    invoke-interface {p0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 446
    .local v1, "bucketId":Ljava/lang/Integer;
    if-nez v1, :cond_3

    .line 452
    invoke-static {}, Lcom/google/android/finsky/utils/PermissionsBucketer;->getIgnoredPermissionSet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 453
    .local v3, "isIgnoredPermission":Z
    if-eqz v3, :cond_1

    if-nez p2, :cond_2

    :cond_1
    if-nez v3, :cond_0

    invoke-static {v5}, Lcom/google/android/finsky/utils/PermissionsBucketer;->isDangerousPlatformDefinedPermission(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 457
    :cond_2
    const/16 v7, 0x10

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 464
    .end local v3    # "isIgnoredPermission":Z
    :cond_3
    if-nez v4, :cond_4

    .line 465
    const/16 v7, 0x11

    new-array v4, v7, [Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    .line 468
    :cond_4
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aget-object v0, v4, v7

    .line 469
    .local v0, "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    if-nez v0, :cond_5

    .line 470
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-static {v7}, Lcom/google/android/finsky/utils/PermissionsBucketer;->getBucketByBucketId(I)Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    move-result-object v0

    .line 471
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput-object v0, v4, v7

    .line 473
    :cond_5
    invoke-static {v5}, Lcom/google/android/finsky/utils/PermissionsBucketer;->getHumanReadablePermissionString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 474
    .local v6, "permissionName":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 475
    invoke-virtual {v0, v6}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->addPermission(Ljava/lang/String;)V

    goto :goto_0

    .line 478
    .end local v0    # "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v1    # "bucketId":Ljava/lang/Integer;
    .end local v5    # "permission":Ljava/lang/String;
    .end local v6    # "permissionName":Ljava/lang/String;
    :cond_6
    return-object v4
.end method

.method protected static generatePermissionsMap()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 527
    sget-object v5, Lcom/google/android/finsky/utils/PermissionsBucketer;->sCachedPermissionsMap:Ljava/util/Map;

    if-eqz v5, :cond_0

    .line 528
    sget-object v5, Lcom/google/android/finsky/utils/PermissionsBucketer;->sCachedPermissionsMap:Ljava/util/Map;

    .line 581
    .local v0, "arr$":[Ljava/lang/String;
    .local v1, "i$":I
    .local v2, "len$":I
    .local v4, "permissionsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :goto_0
    return-object v5

    .line 531
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v4    # "permissionsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v4

    .line 532
    .restart local v4    # "permissionsMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->BILLING_PERMISSIONS_BUCKET:[Ljava/lang/String;

    .restart local v0    # "arr$":[Ljava/lang/String;
    array-length v2, v0

    .restart local v2    # "len$":I
    const/4 v1, 0x0

    .restart local v1    # "i$":I
    :goto_1
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 533
    .local v3, "permission":Ljava/lang/String;
    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 532
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 535
    .end local v3    # "permission":Ljava/lang/String;
    :cond_1
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->IDENTITY_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 536
    .restart local v3    # "permission":Ljava/lang/String;
    const/4 v5, 0x3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 538
    .end local v3    # "permission":Ljava/lang/String;
    :cond_2
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->CONTACTS_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 539
    .restart local v3    # "permission":Ljava/lang/String;
    const/4 v5, 0x5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 538
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 541
    .end local v3    # "permission":Ljava/lang/String;
    :cond_3
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->CALENDAR_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v2, :cond_4

    aget-object v3, v0, v1

    .line 542
    .restart local v3    # "permission":Ljava/lang/String;
    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 541
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 544
    .end local v3    # "permission":Ljava/lang/String;
    :cond_4
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->LOCATION_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v2, :cond_5

    aget-object v3, v0, v1

    .line 545
    .restart local v3    # "permission":Ljava/lang/String;
    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 544
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 547
    .end local v3    # "permission":Ljava/lang/String;
    :cond_5
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->SMS_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v2, :cond_6

    aget-object v3, v0, v1

    .line 548
    .restart local v3    # "permission":Ljava/lang/String;
    const/4 v5, 0x7

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 547
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 550
    .end local v3    # "permission":Ljava/lang/String;
    :cond_6
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->PHONE_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_7
    if-ge v1, v2, :cond_7

    aget-object v3, v0, v1

    .line 551
    .restart local v3    # "permission":Ljava/lang/String;
    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 553
    .end local v3    # "permission":Ljava/lang/String;
    :cond_7
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->PHOTOS_MEDIA_FILES_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_8
    if-ge v1, v2, :cond_8

    aget-object v3, v0, v1

    .line 554
    .restart local v3    # "permission":Ljava/lang/String;
    const/16 v5, 0x9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 553
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 556
    .end local v3    # "permission":Ljava/lang/String;
    :cond_8
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->CAMERA_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_9
    if-ge v1, v2, :cond_9

    aget-object v3, v0, v1

    .line 557
    .restart local v3    # "permission":Ljava/lang/String;
    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 556
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 559
    .end local v3    # "permission":Ljava/lang/String;
    :cond_9
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->MIC_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_a
    if-ge v1, v2, :cond_a

    aget-object v3, v0, v1

    .line 560
    .restart local v3    # "permission":Ljava/lang/String;
    const/16 v5, 0xb

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 559
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 562
    .end local v3    # "permission":Ljava/lang/String;
    :cond_a
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->DEVICE_AND_APP_HISTORY_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_b
    if-ge v1, v2, :cond_b

    aget-object v3, v0, v1

    .line 563
    .restart local v3    # "permission":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 565
    .end local v3    # "permission":Ljava/lang/String;
    :cond_b
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->REDIRECT_INTERNET_TRAFFIC:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_c
    if-ge v1, v2, :cond_c

    aget-object v3, v0, v1

    .line 566
    .restart local v3    # "permission":Ljava/lang/String;
    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 565
    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    .line 568
    .end local v3    # "permission":Ljava/lang/String;
    :cond_c
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->WIFI_CONNECTION_INFORMATION:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_d
    if-ge v1, v2, :cond_d

    aget-object v3, v0, v1

    .line 569
    .restart local v3    # "permission":Ljava/lang/String;
    const/16 v5, 0xc

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 568
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 571
    .end local v3    # "permission":Ljava/lang/String;
    :cond_d
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->BLUETOOTH_CONNECTION_INFORMATION:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_e
    if-ge v1, v2, :cond_e

    aget-object v3, v0, v1

    .line 572
    .restart local v3    # "permission":Ljava/lang/String;
    const/16 v5, 0xd

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 571
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 574
    .end local v3    # "permission":Ljava/lang/String;
    :cond_e
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->DEVICE_ID_AND_CALL_INFORMATION:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_f
    if-ge v1, v2, :cond_f

    aget-object v3, v0, v1

    .line 575
    .restart local v3    # "permission":Ljava/lang/String;
    const/16 v5, 0xe

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574
    add-int/lit8 v1, v1, 0x1

    goto :goto_f

    .line 577
    .end local v3    # "permission":Ljava/lang/String;
    :cond_f
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->WEARABLE_ACTIVITY_PERMISSIONS:[Ljava/lang/String;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_10
    if-ge v1, v2, :cond_10

    aget-object v3, v0, v1

    .line 578
    .restart local v3    # "permission":Ljava/lang/String;
    const/16 v5, 0xf

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 577
    add-int/lit8 v1, v1, 0x1

    goto :goto_10

    .line 580
    .end local v3    # "permission":Ljava/lang/String;
    :cond_10
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v5

    sput-object v5, Lcom/google/android/finsky/utils/PermissionsBucketer;->sCachedPermissionsMap:Ljava/util/Map;

    .line 581
    sget-object v5, Lcom/google/android/finsky/utils/PermissionsBucketer;->sCachedPermissionsMap:Ljava/util/Map;

    goto/16 :goto_0
.end method

.method protected static getBucketByBucketId(I)Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .locals 5
    .param p0, "bucketId"    # I

    .prologue
    .line 589
    packed-switch p0, :pswitch_data_0

    .line 676
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "invalid permission bucket."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 591
    :pswitch_0
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/4 v1, 0x0

    const v2, 0x7f02011b

    const v3, 0x7f0c0183

    const v4, 0x7f0c0184

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    .line 671
    :goto_0
    return-object v0

    .line 596
    :pswitch_1
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/4 v1, 0x3

    const v2, 0x7f02011a

    const v3, 0x7f0c0185

    const v4, 0x7f0c0186

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto :goto_0

    .line 601
    :pswitch_2
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/4 v1, 0x4

    const v2, 0x7f020110

    const v3, 0x7f0c0187

    const v4, 0x7f0c0188

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto :goto_0

    .line 606
    :pswitch_3
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/4 v1, 0x5

    const v2, 0x7f020116

    const v3, 0x7f0c0189

    const v4, 0x7f0c018a

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto :goto_0

    .line 611
    :pswitch_4
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/4 v1, 0x6

    const v2, 0x7f02011c

    const v3, 0x7f0c018b

    const v4, 0x7f0c018c

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto :goto_0

    .line 616
    :pswitch_5
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/16 v1, 0x8

    const v2, 0x7f020120

    const v3, 0x7f0c018f

    const v4, 0x7f0c0190

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto :goto_0

    .line 621
    :pswitch_6
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/4 v1, 0x7

    const v2, 0x7f02011e

    const v3, 0x7f0c018d

    const v4, 0x7f0c018e

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto :goto_0

    .line 626
    :pswitch_7
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/16 v1, 0x9

    const v2, 0x7f02011d

    const v3, 0x7f0c0191

    const v4, 0x7f0c0192

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto :goto_0

    .line 631
    :pswitch_8
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/16 v1, 0xa

    const v2, 0x7f020111

    const v3, 0x7f0c0193

    const v4, 0x7f0c0194

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto/16 :goto_0

    .line 636
    :pswitch_9
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/16 v1, 0xb

    const v2, 0x7f02011f

    const v3, 0x7f0c0195

    const v4, 0x7f0c0196

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto/16 :goto_0

    .line 641
    :pswitch_a
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/4 v1, 0x1

    const v2, 0x7f020119

    const v3, 0x7f0c0197

    const v4, 0x7f0c0198

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto/16 :goto_0

    .line 646
    :pswitch_b
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/4 v1, 0x2

    const v2, 0x7f020117

    const v3, 0x7f0c0199

    const v4, 0x7f0c019a

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto/16 :goto_0

    .line 651
    :pswitch_c
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/16 v1, 0xc

    const v2, 0x7f020121

    const v3, 0x7f0c019f

    const v4, 0x7f0c01a0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto/16 :goto_0

    .line 656
    :pswitch_d
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/16 v1, 0xd

    const v2, 0x7f02010e

    const v3, 0x7f0c01a1

    const v4, 0x7f0c01a2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto/16 :goto_0

    .line 661
    :pswitch_e
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/16 v1, 0xe

    const v2, 0x7f020118

    const v3, 0x7f0c019b

    const v4, 0x7f0c019c

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto/16 :goto_0

    .line 666
    :pswitch_f
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/16 v1, 0xf

    const v2, 0x7f02010f

    const v3, 0x7f0c01a3

    const v4, 0x7f0c01a4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto/16 :goto_0

    .line 671
    :pswitch_10
    new-instance v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    const/16 v1, 0x10

    const v2, 0x7f020123

    const v3, 0x7f0c019d

    const v4, 0x7f0c019e

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;-><init>(IIII)V

    goto/16 :goto_0

    .line 589
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private static getHumanReadablePermissionString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "permission"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 501
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p0, v4}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v0

    .line 503
    .local v0, "permissionInfo":Landroid/content/pm/PermissionInfo;
    if-eqz v0, :cond_0

    .line 504
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/pm/PermissionInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 506
    .local v1, "sequence":Ljava/lang/CharSequence;
    if-nez v1, :cond_1

    .line 509
    .end local v0    # "permissionInfo":Landroid/content/pm/PermissionInfo;
    .end local v1    # "sequence":Ljava/lang/CharSequence;
    :cond_0
    :goto_0
    return-object v2

    .line 506
    .restart local v0    # "permissionInfo":Landroid/content/pm/PermissionInfo;
    .restart local v1    # "sequence":Ljava/lang/CharSequence;
    :cond_1
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 508
    .end local v0    # "permissionInfo":Landroid/content/pm/PermissionInfo;
    .end local v1    # "sequence":Ljava/lang/CharSequence;
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method protected static getIgnoredPermissionSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 516
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->sCachedIgnoredPermissionSet:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 517
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->IGNORED_PERMISSIONS:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/utils/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->sCachedIgnoredPermissionSet:Ljava/util/Set;

    .line 519
    :cond_0
    sget-object v0, Lcom/google/android/finsky/utils/PermissionsBucketer;->sCachedIgnoredPermissionSet:Ljava/util/Set;

    return-object v0
.end method

.method public static getPermissionBuckets([Ljava/lang/String;Ljava/util/Set;Z)Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;
    .locals 1
    .param p0, "permissionsFromLatestVersion"    # [Ljava/lang/String;
    .param p2, "hadAcceptedBucketedPermissions"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;"
        }
    .end annotation

    .prologue
    .line 277
    .local p1, "permissionsFromInstalledVersion":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/finsky/utils/PermissionsBucketer;->getPermissionBuckets([Ljava/lang/String;Ljava/util/Set;ZZ)Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    move-result-object v0

    return-object v0
.end method

.method public static getPermissionBuckets([Ljava/lang/String;Ljava/util/Set;ZZ)Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;
    .locals 11
    .param p0, "permissionsFromLatestVersion"    # [Ljava/lang/String;
    .param p2, "hadAcceptedBucketedPermissions"    # Z
    .param p3, "showIgnoredPermissionsInOtherBucket"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)",
            "Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;"
        }
    .end annotation

    .prologue
    .line 301
    .local p1, "permissionsFromInstalledVersion":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez p0, :cond_0

    .line 302
    new-instance v10, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    invoke-direct {v10}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;-><init>()V

    .line 390
    :goto_0
    return-object v10

    .line 305
    :cond_0
    const/4 v2, 0x0

    .line 307
    .local v2, "forcePermissionPrompt":Z
    invoke-static {}, Lcom/google/android/finsky/utils/PermissionsBucketer;->generatePermissionsMap()Ljava/util/Map;

    move-result-object v9

    .line 308
    .local v9, "permissionToBucketMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/4 v7, 0x0

    .line 312
    .local v7, "newlyAddedPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez p2, :cond_6

    .line 313
    if-eqz p1, :cond_2

    .line 314
    invoke-static {p0}, Lcom/google/android/finsky/utils/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v7

    .line 316
    invoke-interface {v7, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 319
    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v10

    invoke-static {v10, v7}, Lcom/google/android/finsky/utils/PermissionsBucketer;->hasDangerousNewPermissions(Ljava/util/Set;Ljava/util/Set;)Z

    move-result v10

    if-nez v10, :cond_1

    .line 321
    new-instance v10, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    invoke-direct {v10}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;-><init>()V

    goto :goto_0

    .line 325
    :cond_1
    const/4 v2, 0x1

    .line 333
    :cond_2
    const/4 p1, 0x0

    .line 334
    invoke-static {p0}, Lcom/google/android/finsky/utils/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v7

    .line 345
    :cond_3
    :goto_1
    invoke-static {v9, v7, p3}, Lcom/google/android/finsky/utils/PermissionsBucketer;->bucketPermissions(Ljava/util/Map;Ljava/util/Set;Z)[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    move-result-object v4

    .line 355
    .local v4, "latestBucketedPermissions":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    const/4 v8, 0x0

    .line 356
    .local v8, "oldVersionBucketedPermissions":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    if-eqz p1, :cond_4

    .line 357
    invoke-static {v9, p1, p3}, Lcom/google/android/finsky/utils/PermissionsBucketer;->bucketPermissions(Ljava/util/Map;Ljava/util/Set;Z)[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    move-result-object v8

    .line 365
    :cond_4
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 366
    .local v6, "newPermissions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 367
    .local v1, "existingPermissions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    const/16 v10, 0x10

    if-ge v3, v10, :cond_a

    .line 368
    if-eqz v4, :cond_7

    aget-object v5, v4, v3

    .line 370
    .local v5, "latestPermission":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    :goto_3
    if-eqz v8, :cond_8

    aget-object v0, v8, v3

    .line 372
    .local v0, "existingBucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    :goto_4
    if-eqz v0, :cond_9

    .line 374
    aget-object v10, v8, v3

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 367
    :cond_5
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 336
    .end local v0    # "existingBucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v1    # "existingPermissions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;>;"
    .end local v3    # "i":I
    .end local v4    # "latestBucketedPermissions":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v5    # "latestPermission":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v6    # "newPermissions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;>;"
    .end local v8    # "oldVersionBucketedPermissions":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    :cond_6
    invoke-static {p0}, Lcom/google/android/finsky/utils/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v7

    .line 339
    if-eqz p1, :cond_3

    .line 340
    invoke-interface {v7, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 368
    .restart local v1    # "existingPermissions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;>;"
    .restart local v3    # "i":I
    .restart local v4    # "latestBucketedPermissions":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .restart local v6    # "newPermissions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;>;"
    .restart local v8    # "oldVersionBucketedPermissions":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    :cond_7
    const/4 v5, 0x0

    goto :goto_3

    .line 370
    .restart local v5    # "latestPermission":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    :cond_8
    const/4 v0, 0x0

    goto :goto_4

    .line 375
    .restart local v0    # "existingBucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    :cond_9
    if-eqz v5, :cond_5

    .line 378
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 381
    .end local v0    # "existingBucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v5    # "latestPermission":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    :cond_a
    if-eqz v8, :cond_b

    const/16 v10, 0x10

    aget-object v10, v8, v10

    if-eqz v10, :cond_b

    .line 383
    const/16 v10, 0x10

    aget-object v10, v8, v10

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 385
    :cond_b
    if-eqz v4, :cond_c

    const/16 v10, 0x10

    aget-object v10, v4, v10

    if-eqz v10, :cond_c

    .line 387
    const/16 v10, 0x10

    aget-object v10, v4, v10

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    :cond_c
    new-instance v10, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    invoke-direct {v10, v6, v1, v2}, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;-><init>(Ljava/util/List;Ljava/util/List;Z)V

    goto/16 :goto_0
.end method

.method protected static hasDangerousNewPermissions(Ljava/util/Set;Ljava/util/Set;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p0, "dangerousPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .local p1, "passedInNewPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v3, 0x1

    .line 411
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 412
    .local v2, "permission":Ljava/lang/String;
    invoke-interface {p0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 429
    .end local v2    # "permission":Ljava/lang/String;
    :goto_0
    return v3

    .line 420
    :cond_1
    invoke-static {p1}, Lcom/google/android/finsky/utils/Sets;->newHashSet(Ljava/util/Collection;)Ljava/util/HashSet;

    move-result-object v1

    .line 421
    .local v1, "newPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1, p0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 423
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 424
    .restart local v2    # "permission":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/utils/PermissionsBucketer;->isDangerousPlatformDefinedPermission(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_0

    .line 429
    .end local v2    # "permission":Ljava/lang/String;
    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static isDangerousPlatformDefinedPermission(Ljava/lang/String;)Z
    .locals 5
    .param p0, "permission"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 486
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p0, v4}, Landroid/content/pm/PackageManager;->getPermissionInfo(Ljava/lang/String;I)Landroid/content/pm/PermissionInfo;

    move-result-object v0

    .line 488
    .local v0, "permissionInfo":Landroid/content/pm/PermissionInfo;
    if-eqz v0, :cond_0

    iget v3, v0, Landroid/content/pm/PermissionInfo;->protectionLevel:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v3, v1, :cond_0

    .line 493
    .end local v0    # "permissionInfo":Landroid/content/pm/PermissionInfo;
    :goto_0
    return v1

    .line 492
    :catch_0
    move-exception v1

    :cond_0
    move v1, v2

    .line 493
    goto :goto_0
.end method

.method public static setAcceptedNewBuckets(Ljava/lang/String;)V
    .locals 2
    .param p0, "docId"    # Ljava/lang/String;

    .prologue
    .line 398
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v0

    .line 400
    .local v0, "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setPermissionsVersion(Ljava/lang/String;I)V

    .line 401
    return-void
.end method
