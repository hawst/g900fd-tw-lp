.class Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;
.super Ljava/lang/Object;
.source "DownloadProgressManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/download/DownloadProgressManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ProgressRunnable"
.end annotation


# instance fields
.field private final downloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

.field private mProgressMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;"
        }
    .end annotation
.end field

.field private final newUris:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final oldUris:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/download/DownloadQueue;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)V
    .locals 0
    .param p1, "downloadQueue"    # Lcom/google/android/finsky/download/DownloadQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/download/DownloadQueue;",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 91
    .local p2, "oldUris":Ljava/util/Set;, "Ljava/util/Set<Landroid/net/Uri;>;"
    .local p3, "newUris":Ljava/util/Set;, "Ljava/util/Set<Landroid/net/Uri;>;"
    .local p4, "progressMap":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/Uri;Lcom/google/android/finsky/download/DownloadProgress;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->downloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    .line 93
    iput-object p2, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->oldUris:Ljava/util/Set;

    .line 94
    iput-object p3, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->newUris:Ljava/util/Set;

    .line 95
    iput-object p4, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->mProgressMap:Ljava/util/Map;

    .line 96
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/16 v8, 0xc6

    .line 100
    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->oldUris:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 101
    .local v3, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->downloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v4, v3}, Lcom/google/android/finsky/download/DownloadQueue;->getDownloadByContentUri(Landroid/net/Uri;)Lcom/google/android/finsky/download/Download;

    move-result-object v0

    .line 102
    .local v0, "download":Lcom/google/android/finsky/download/Download;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getState()Lcom/google/android/finsky/download/Download$DownloadState;

    move-result-object v4

    sget-object v5, Lcom/google/android/finsky/download/Download$DownloadState;->DOWNLOADING:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/download/Download$DownloadState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 103
    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->downloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v4, v0}, Lcom/google/android/finsky/download/DownloadQueue;->cancel(Lcom/google/android/finsky/download/Download;)V

    goto :goto_0

    .line 108
    .end local v0    # "download":Lcom/google/android/finsky/download/Download;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->newUris:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 109
    .restart local v3    # "uri":Landroid/net/Uri;
    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->downloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v4, v3}, Lcom/google/android/finsky/download/DownloadQueue;->getDownloadByContentUri(Landroid/net/Uri;)Lcom/google/android/finsky/download/Download;

    move-result-object v0

    .line 110
    .restart local v0    # "download":Lcom/google/android/finsky/download/Download;
    if-eqz v0, :cond_2

    .line 144
    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->mProgressMap:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/download/DownloadProgress;

    .line 145
    .local v1, "downloadProgress":Lcom/google/android/finsky/download/DownloadProgress;
    iget v4, v1, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    if-ne v4, v8, :cond_3

    .line 147
    const-string v4, "%s: Caught error 198 while state=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getState()Lcom/google/android/finsky/download/Download$DownloadState;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getState()Lcom/google/android/finsky/download/Download$DownloadState;

    move-result-object v4

    sget-object v5, Lcom/google/android/finsky/download/Download$DownloadState;->DOWNLOADING:Lcom/google/android/finsky/download/Download$DownloadState;

    if-ne v4, v5, :cond_2

    .line 150
    invoke-interface {v0, v8}, Lcom/google/android/finsky/download/Download;->setHttpStatus(I)V

    .line 152
    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->downloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    sget-object v5, Lcom/google/android/finsky/download/Download$DownloadState;->ERROR:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-interface {v4, v0, v5}, Lcom/google/android/finsky/download/DownloadQueue;->setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V

    goto :goto_1

    .line 157
    :cond_3
    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;->downloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v4, v0, v1}, Lcom/google/android/finsky/download/DownloadQueue;->notifyProgress(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V

    goto :goto_1

    .line 160
    .end local v0    # "download":Lcom/google/android/finsky/download/Download;
    .end local v1    # "downloadProgress":Lcom/google/android/finsky/download/DownloadProgress;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_4
    return-void
.end method
