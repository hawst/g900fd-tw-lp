.class public Lcom/google/android/finsky/layout/scroll/GestureScrollView;
.super Landroid/widget/ScrollView;
.source "GestureScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;
    }
.end annotation


# instance fields
.field private mFlingToDismissListener:Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;

.field private mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

.field private mOnGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    new-instance v0, Lcom/google/android/finsky/layout/scroll/GestureScrollView$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/scroll/GestureScrollView$1;-><init>(Lcom/google/android/finsky/layout/scroll/GestureScrollView;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->mOnGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 78
    new-instance v0, Landroid/support/v4/view/GestureDetectorCompat;

    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->mOnGestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0, p1, v1}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/scroll/GestureScrollView;)Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->mFlingToDismissListener:Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;

    return-object v0
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 88
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setFlingToDismissListener(Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->mFlingToDismissListener:Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;

    .line 83
    return-void
.end method
