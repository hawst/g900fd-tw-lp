.class public final Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UsernamePasswordForm"
.end annotation


# instance fields
.field public actionTitleText:Ljava/lang/String;

.field public hasActionTitleText:Z

.field public hasHelpUrl:Z

.field public helpUrl:Ljava/lang/String;

.field public logoImage:Lcom/google/android/finsky/protos/Common$Image;

.field public passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

.field public tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

.field public usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 767
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 768
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->clear()Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    .line 769
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 772
    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    .line 773
    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    .line 774
    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    .line 775
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->actionTitleText:Ljava/lang/String;

    .line 776
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->hasActionTitleText:Z

    .line 777
    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 778
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->helpUrl:Ljava/lang/String;

    .line 779
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->hasHelpUrl:Z

    .line 780
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->cachedSize:I

    .line 781
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 810
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 811
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    if-eqz v1, :cond_0

    .line 812
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 815
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    if-eqz v1, :cond_1

    .line 816
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 819
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    if-eqz v1, :cond_2

    .line 820
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 823
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->hasActionTitleText:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->actionTitleText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 824
    :cond_3
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->actionTitleText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 827
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->hasHelpUrl:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->helpUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 828
    :cond_5
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->helpUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 831
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_7

    .line 832
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 835
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 843
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 844
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 848
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 849
    :sswitch_0
    return-object p0

    .line 854
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    if-nez v1, :cond_1

    .line 855
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    .line 857
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 861
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    if-nez v1, :cond_2

    .line 862
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    .line 864
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 868
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    if-nez v1, :cond_3

    .line 869
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    .line 871
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 875
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->actionTitleText:Ljava/lang/String;

    .line 876
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->hasActionTitleText:Z

    goto :goto_0

    .line 880
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->helpUrl:Ljava/lang/String;

    .line 881
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->hasHelpUrl:Z

    goto :goto_0

    .line 885
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_4

    .line 886
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 888
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 844
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 730
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 787
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    if-eqz v0, :cond_0

    .line 788
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 790
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    if-eqz v0, :cond_1

    .line 791
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 793
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    if-eqz v0, :cond_2

    .line 794
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 796
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->hasActionTitleText:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->actionTitleText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 797
    :cond_3
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->actionTitleText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 799
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->hasHelpUrl:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->helpUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 800
    :cond_5
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->helpUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 802
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_7

    .line 803
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 805
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 806
    return-void
.end method
