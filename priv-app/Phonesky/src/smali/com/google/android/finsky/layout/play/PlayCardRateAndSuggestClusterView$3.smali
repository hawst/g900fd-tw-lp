.class Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$3;
.super Ljava/lang/Object;
.source "PlayCardRateAndSuggestClusterView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->onDataChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

.field final synthetic val$suggestions:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$3;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$3;->val$suggestions:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$3;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    # getter for: Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mContentScroller:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;
    invoke-static {v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->access$300(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;)Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->scrollAwayRateCard()V

    .line 507
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$3;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$3;->val$suggestions:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView$3;->val$suggestions:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v2, v2, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->replaceTitles(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 509
    return-void
.end method
