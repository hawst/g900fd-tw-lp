.class public Lcom/google/android/finsky/installer/PackageInstallerImpl;
.super Ljava/lang/Object;
.source "PackageInstallerImpl.java"

# interfaces
.implements Lcom/google/android/finsky/installer/PackageInstallerFacade;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mOpenSessionMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/pm/PackageInstaller$Session;",
            ">;"
        }
    .end annotation
.end field

.field private final mPackageInstaller:Landroid/content/pm/PackageInstaller;

.field private final mSessionInfoMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/content/pm/PackageInstaller$SessionInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mContext:Landroid/content/Context;

    .line 59
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/pm/PackageManager;->getPackageInstaller()Landroid/content/pm/PackageInstaller;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    .line 63
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    .line 64
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    invoke-virtual {v5}, Landroid/content/pm/PackageInstaller;->getMySessions()Ljava/util/List;

    move-result-object v1

    .line 65
    .local v1, "mySessions":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInstaller$SessionInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 66
    .local v4, "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    invoke-virtual {v4}, Landroid/content/pm/PackageInstaller$SessionInfo;->getAppPackageName()Ljava/lang/String;

    move-result-object v3

    .line 67
    .local v3, "packageName":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 70
    .local v2, "oldInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    if-eqz v2, :cond_0

    .line 71
    const-string v5, "Multiple sessions for %s found. Removing %d & keeping %d."

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v7, 0x1

    invoke-virtual {v2}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-virtual {v4}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 75
    .end local v2    # "oldInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/installer/PackageInstallerImpl;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/installer/PackageInstallerImpl;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/installer/PackageInstallerImpl;Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)Landroid/content/IntentSender;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/installer/PackageInstallerImpl;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->getCommitCallback(Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)Landroid/content/IntentSender;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/installer/PackageInstallerImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/installer/PackageInstallerImpl;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/installer/PackageInstallerImpl;Landroid/content/Intent;Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/installer/PackageInstallerImpl;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->handleCommitCallback(Landroid/content/Intent;Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V

    return-void
.end method

.method private cancelSession(ILjava/lang/String;)V
    .locals 5
    .param p1, "sessionId"    # I
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 139
    invoke-direct {p0, p2}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->closeSession(Ljava/lang/String;)V

    .line 141
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    :try_start_0
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    invoke-virtual {v1, p1}, Landroid/content/pm/PackageInstaller;->abandonSession(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    const-string v1, "Canceling session %d for %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 151
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 146
    .local v0, "se":Ljava/lang/SecurityException;
    goto :goto_0
.end method

.method private closeSession(Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 255
    iget-object v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInstaller$Session;

    .line 256
    .local v1, "session":Landroid/content/pm/PackageInstaller$Session;
    if-eqz v1, :cond_0

    .line 259
    :try_start_0
    invoke-virtual {v1}, Landroid/content/pm/PackageInstaller$Session;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Unexpected error closing session for %s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getCommitCallback(Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)Landroid/content/IntentSender;
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "sessionId"    # I
    .param p3, "callback"    # Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    .prologue
    .line 408
    new-instance v2, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/finsky/installer/PackageInstallerImpl$2;-><init>(Lcom/google/android/finsky/installer/PackageInstallerImpl;Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V

    .line 416
    .local v2, "broadcastReceiver":Landroid/content/BroadcastReceiver;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "com.android.vending.INTENT_PACKAGE_INSTALL_COMMIT."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 417
    .local v0, "action":Ljava/lang/String;
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 418
    .local v3, "intentFilter":Landroid/content/IntentFilter;
    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 419
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 422
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 423
    .local v1, "broadcastIntent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v6

    const/high16 v7, 0x48000000    # 131072.0f

    invoke-static {v5, v6, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 425
    .local v4, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {v4}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v5

    return-object v5
.end method

.method private getSession(Ljava/lang/String;)Landroid/content/pm/PackageInstaller$Session;
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 214
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInstaller$Session;

    .line 215
    .local v2, "session":Landroid/content/pm/PackageInstaller$Session;
    if-eqz v2, :cond_1

    .line 219
    :try_start_0
    invoke-virtual {v2}, Landroid/content/pm/PackageInstaller$Session;->getNames()[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v4, v2

    .line 248
    :cond_0
    :goto_0
    return-object v4

    .line 221
    :catch_0
    move-exception v0

    .line 222
    .local v0, "ioe":Ljava/io/IOException;
    const-string v5, "Stale open session for %s: %s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v8

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 223
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    .end local v0    # "ioe":Ljava/io/IOException;
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 231
    .local v3, "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    if-eqz v3, :cond_0

    .line 236
    :try_start_1
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    invoke-virtual {v3}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v2

    .line 247
    iget-object v4, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;

    invoke-interface {v4, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v2

    .line 248
    goto :goto_0

    .line 224
    .end local v3    # "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    :catch_1
    move-exception v1

    .line 225
    .local v1, "se":Ljava/lang/SecurityException;
    const-string v5, "Stale open session for %s: %s"

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v8

    invoke-virtual {v1}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 226
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 237
    .end local v1    # "se":Ljava/lang/SecurityException;
    .restart local v3    # "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    :catch_2
    move-exception v1

    .line 238
    .restart local v1    # "se":Ljava/lang/SecurityException;
    const-string v5, "SessionInfo was stale for %s - deleting info"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object p1, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 239
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 241
    .end local v1    # "se":Ljava/lang/SecurityException;
    :catch_3
    move-exception v0

    .line 242
    .restart local v0    # "ioe":Ljava/io/IOException;
    const-string v5, "IOException opening old session for %s - deleting info"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 244
    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private handleCommitCallback(Landroid/content/Intent;Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "sessionId"    # I
    .param p4, "callback"    # Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    .prologue
    const/4 v7, 0x0

    const/high16 v4, -0x80000000

    .line 439
    const-string v3, "android.content.pm.extra.STATUS_MESSAGE"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 440
    .local v2, "statusMessage":Ljava/lang/String;
    const-string v3, "android.content.pm.extra.STATUS"

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 441
    .local v1, "status":I
    if-nez v1, :cond_0

    .line 445
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v3

    invoke-interface {v3, p2}, Lcom/google/android/finsky/appstate/PackageStateRepository;->invalidate(Ljava/lang/String;)V

    .line 447
    invoke-direct {p0, p3, p2}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->cancelSession(ILjava/lang/String;)V

    .line 448
    invoke-interface {p4}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installSucceeded()V

    .line 471
    :goto_0
    return-void

    .line 449
    :cond_0
    const/4 v3, -0x1

    if-ne v1, v3, :cond_1

    .line 452
    invoke-direct {p0, p3, p2}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->cancelSession(ILjava/lang/String;)V

    .line 453
    const/16 v0, 0x3d0

    .line 454
    .local v0, "errorCode":I
    invoke-interface {p4, v0, v7}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installFailed(ILjava/lang/String;)V

    goto :goto_0

    .line 456
    .end local v0    # "errorCode":I
    :cond_1
    invoke-direct {p0, p3, p2}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->cancelSession(ILjava/lang/String;)V

    .line 463
    if-ne v1, v4, :cond_2

    .line 464
    const/16 v0, 0x3d1

    .line 468
    .restart local v0    # "errorCode":I
    :goto_1
    const-string v3, "Error %d while installing %s: %s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    const/4 v5, 0x2

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469
    invoke-interface {p4, v0, v7}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installFailed(ILjava/lang/String;)V

    goto :goto_0

    .line 466
    .end local v0    # "errorCode":I
    :cond_2
    rsub-int v0, v1, -0x1f4

    .restart local v0    # "errorCode":I
    goto :goto_1
.end method

.method private innerCreateSession(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "installSize"    # J
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "appIcon"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 89
    iget-object v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 90
    const-string v3, "Creating session for %s when one already exists"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v7

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    :goto_0
    return-void

    .line 93
    :cond_0
    new-instance v0, Landroid/content/pm/PackageInstaller$SessionParams;

    invoke-direct {v0, v6}, Landroid/content/pm/PackageInstaller$SessionParams;-><init>(I)V

    .line 94
    .local v0, "params":Landroid/content/pm/PackageInstaller$SessionParams;
    if-eqz p5, :cond_1

    .line 95
    invoke-virtual {v0, p5}, Landroid/content/pm/PackageInstaller$SessionParams;->setAppIcon(Landroid/graphics/Bitmap;)V

    .line 97
    :cond_1
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 98
    invoke-virtual {v0, p4}, Landroid/content/pm/PackageInstaller$SessionParams;->setAppLabel(Ljava/lang/CharSequence;)V

    .line 100
    :cond_2
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageInstaller$SessionParams;->setAppPackageName(Ljava/lang/String;)V

    .line 105
    const-wide/16 v4, 0x0

    cmp-long v3, p2, v4

    if-lez v3, :cond_3

    .line 106
    invoke-virtual {v0, p2, p3}, Landroid/content/pm/PackageInstaller$SessionParams;->setSize(J)V

    .line 110
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageInstaller;->createSession(Landroid/content/pm/PackageInstaller$SessionParams;)I

    move-result v1

    .line 111
    .local v1, "sessionId":I
    iget-object v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    invoke-virtual {v3, v1}, Landroid/content/pm/PackageInstaller;->getSessionInfo(I)Landroid/content/pm/PackageInstaller$SessionInfo;

    move-result-object v2

    .line 112
    .local v2, "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    iget-object v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    const-string v3, "Created session %d for %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    aput-object p1, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public cancelSession(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 126
    iget-object v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 127
    .local v1, "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    if-nez v1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-virtual {v1}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v0

    .line 131
    .local v0, "sessionId":I
    invoke-direct {p0, v0, p1}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->cancelSession(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public createSession(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "installSize"    # J
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "appIcon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 80
    :try_start_0
    invoke-direct/range {p0 .. p5}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->innerCreateSession(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "ioe":Ljava/io/IOException;
    const-string v1, "Couldn\'t create session for %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public getAppIconSize()I
    .locals 3

    .prologue
    .line 193
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mContext:Landroid/content/Context;

    const-string v2, "activity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 194
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getLauncherLargeIconSize()I

    move-result v1

    return v1
.end method

.method public hasSession(Ljava/lang/String;)Z
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public install(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
    .locals 17
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "expectedSize"    # J
    .param p5, "expectedSignature"    # Ljava/lang/String;
    .param p6, "isForwardLocked"    # Z
    .param p7, "packageName"    # Ljava/lang/String;
    .param p8, "callback"    # Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    .prologue
    .line 283
    const/4 v15, 0x0

    .line 284
    .local v15, "session":Landroid/content/pm/PackageInstaller$Session;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    move-object/from16 v0, p7

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 285
    .local v16, "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    if-eqz v16, :cond_0

    .line 287
    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-direct {v0, v1}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->getSession(Ljava/lang/String;)Landroid/content/pm/PackageInstaller$Session;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v15

    .line 291
    :cond_0
    if-nez v15, :cond_1

    .line 293
    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p7

    move-wide/from16 v4, p3

    move-object/from16 v6, p2

    :try_start_1
    invoke-direct/range {v2 .. v7}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->innerCreateSession(Ljava/lang/String;JLjava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 301
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    move-object/from16 v0, p7

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    check-cast v16, Landroid/content/pm/PackageInstaller$SessionInfo;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 303
    .restart local v16    # "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    invoke-virtual/range {v16 .. v16}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageInstaller;->openSession(I)Landroid/content/pm/PackageInstaller$Session;

    move-result-object v15

    .line 304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;

    move-object/from16 v0, p7

    invoke-interface {v3, v0, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 312
    :cond_1
    :try_start_4
    invoke-virtual/range {v16 .. v16}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v11

    .line 313
    .local v11, "sessionId":I
    move-object v9, v15

    .line 316
    .local v9, "finalSession":Landroid/content/pm/PackageInstaller$Session;
    new-instance v2, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;

    move-object/from16 v3, p0

    move-wide/from16 v4, p3

    move-object/from16 v6, p1

    move-object/from16 v7, p5

    move-object/from16 v8, p7

    move-object/from16 v10, p8

    invoke-direct/range {v2 .. v11}, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;-><init>(Lcom/google/android/finsky/installer/PackageInstallerImpl;JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageInstaller$Session;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;I)V

    .line 392
    .local v2, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 399
    .end local v2    # "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/Integer;>;"
    .end local v9    # "finalSession":Landroid/content/pm/PackageInstaller$Session;
    .end local v11    # "sessionId":I
    .end local v16    # "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    :goto_0
    return-void

    .line 294
    .restart local v16    # "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    :catch_0
    move-exception v13

    .line 295
    .local v13, "ioe":Ljava/io/IOException;
    const-string v3, "Can\'t create session for %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p7, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v13}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 296
    const/16 v3, 0x3ca

    invoke-virtual {v13}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p8

    invoke-interface {v0, v3, v4}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installFailed(ILjava/lang/String;)V

    .line 298
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    move-object/from16 v0, p7

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 394
    .end local v13    # "ioe":Ljava/io/IOException;
    .end local v16    # "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    :catch_1
    move-exception v12

    .line 395
    .local v12, "e":Ljava/lang/Exception;
    const-string v3, "Unexpected exception while installing %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p7, v4, v5

    invoke-static {v12, v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 396
    const/16 v3, 0x3cf

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p8

    invoke-interface {v0, v3, v4}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installFailed(ILjava/lang/String;)V

    goto :goto_0

    .line 305
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v16    # "sessionInfo":Landroid/content/pm/PackageInstaller$SessionInfo;
    :catch_2
    move-exception v14

    .line 306
    .local v14, "se":Ljava/lang/SecurityException;
    :try_start_5
    const-string v3, "Can\'t open session for %s: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p7, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v14}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 307
    const/16 v3, 0x3cb

    const/4 v4, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v3, v4}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installFailed(ILjava/lang/String;)V

    .line 308
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    move-object/from16 v0, p7

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0
.end method

.method public pruneSessions(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 159
    .local p1, "packagesToRetain":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/utils/Sets;->newHashSet(Ljava/util/Collection;)Ljava/util/HashSet;

    move-result-object v2

    .line 161
    .local v2, "sessionsToRemove":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2, p1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 163
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 164
    .local v1, "sessionPackageName":Ljava/lang/String;
    const-string v3, "Pruning stale session for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->cancelSession(Ljava/lang/String;)V

    goto :goto_0

    .line 167
    .end local v1    # "sessionPackageName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public reportProgress(Ljava/lang/String;JJ)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "bytesCompleted"    # J
    .param p4, "bytesTotal"    # J

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->getSession(Ljava/lang/String;)Landroid/content/pm/PackageInstaller$Session;

    move-result-object v2

    .line 178
    .local v2, "session":Landroid/content/pm/PackageInstaller$Session;
    if-eqz v2, :cond_0

    .line 180
    const-wide/16 v4, 0x0

    cmp-long v3, p4, v4

    if-lez v3, :cond_0

    .line 181
    long-to-float v3, p2

    long-to-float v4, p4

    div-float v1, v3, v4

    .line 182
    .local v1, "progress":F
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/content/pm/PackageInstaller$Session;->setStagingProgress(F)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    .end local v1    # "progress":F
    :cond_0
    :goto_0
    return-void

    .line 184
    .restart local v1    # "progress":F
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Session for %s unexpectedly closed: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    iget-object v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setAppIcon(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appIcon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 198
    iget-object v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mSessionInfoMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInstaller$SessionInfo;

    .line 199
    .local v0, "info":Landroid/content/pm/PackageInstaller$SessionInfo;
    if-eqz v0, :cond_0

    .line 201
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl;->mPackageInstaller:Landroid/content/pm/PackageInstaller;

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v3

    invoke-virtual {v2, v3, p2}, Landroid/content/pm/PackageInstaller;->updateSessionAppIcon(ILandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 202
    :catch_0
    move-exception v1

    .line 204
    .local v1, "se":Ljava/lang/SecurityException;
    const-string v2, "Stale session id %d for %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/content/pm/PackageInstaller$SessionInfo;->getSessionId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public uninstallPackage(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 475
    invoke-static {p1}, Lcom/google/android/finsky/utils/PackageManagerHelper;->uninstallPackage(Ljava/lang/String;)V

    .line 476
    return-void
.end method
