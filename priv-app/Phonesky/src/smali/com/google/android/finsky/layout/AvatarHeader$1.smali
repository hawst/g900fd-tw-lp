.class Lcom/google/android/finsky/layout/AvatarHeader$1;
.super Ljava/lang/Object;
.source "AvatarHeader.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/AvatarHeader;->bind(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/AvatarHeader;

.field final synthetic val$avatarImageType:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/AvatarHeader;I)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/google/android/finsky/layout/AvatarHeader$1;->this$0:Lcom/google/android/finsky/layout/AvatarHeader;

    iput p2, p0, Lcom/google/android/finsky/layout/AvatarHeader$1;->val$avatarImageType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvatarHeader$1;->this$0:Lcom/google/android/finsky/layout/AvatarHeader;

    # getter for: Lcom/google/android/finsky/layout/AvatarHeader;->mDoc:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v0}, Lcom/google/android/finsky/layout/AvatarHeader;->access$000(Lcom/google/android/finsky/layout/AvatarHeader;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/layout/AvatarHeader$1;->val$avatarImageType:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvatarHeader$1;->this$0:Lcom/google/android/finsky/layout/AvatarHeader;

    # getter for: Lcom/google/android/finsky/layout/AvatarHeader;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;
    invoke-static {v0}, Lcom/google/android/finsky/layout/AvatarHeader;->access$100(Lcom/google/android/finsky/layout/AvatarHeader;)Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/AvatarHeader$1;->this$0:Lcom/google/android/finsky/layout/AvatarHeader;

    # getter for: Lcom/google/android/finsky/layout/AvatarHeader;->mDoc:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v1}, Lcom/google/android/finsky/layout/AvatarHeader;->access$000(Lcom/google/android/finsky/layout/AvatarHeader;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/finsky/layout/AvatarHeader$1;->val$avatarImageType:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToImagesLightbox(Lcom/google/android/finsky/api/model/Document;II)V

    .line 127
    :cond_0
    return-void
.end method
