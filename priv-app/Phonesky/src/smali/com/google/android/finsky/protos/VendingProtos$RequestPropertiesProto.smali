.class public final Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RequestPropertiesProto"
.end annotation


# instance fields
.field public aid:Ljava/lang/String;

.field public clientId:Ljava/lang/String;

.field public hasAid:Z

.field public hasClientId:Z

.field public hasLoggingId:Z

.field public hasOperatorName:Z

.field public hasOperatorNumericName:Z

.field public hasProductNameAndVersion:Z

.field public hasSimOperatorName:Z

.field public hasSimOperatorNumericName:Z

.field public hasSoftwareVersion:Z

.field public hasUserAuthToken:Z

.field public hasUserAuthTokenSecure:Z

.field public hasUserCountry:Z

.field public hasUserLanguage:Z

.field public loggingId:Ljava/lang/String;

.field public operatorName:Ljava/lang/String;

.field public operatorNumericName:Ljava/lang/String;

.field public productNameAndVersion:Ljava/lang/String;

.field public simOperatorName:Ljava/lang/String;

.field public simOperatorNumericName:Ljava/lang/String;

.field public softwareVersion:I

.field public userAuthToken:Ljava/lang/String;

.field public userAuthTokenSecure:Z

.field public userCountry:Ljava/lang/String;

.field public userLanguage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2520
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2521
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;

    .line 2522
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2525
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthToken:Ljava/lang/String;

    .line 2526
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserAuthToken:Z

    .line 2527
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthTokenSecure:Z

    .line 2528
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserAuthTokenSecure:Z

    .line 2529
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->softwareVersion:I

    .line 2530
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSoftwareVersion:Z

    .line 2531
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->aid:Ljava/lang/String;

    .line 2532
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasAid:Z

    .line 2533
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->productNameAndVersion:Ljava/lang/String;

    .line 2534
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasProductNameAndVersion:Z

    .line 2535
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userLanguage:Ljava/lang/String;

    .line 2536
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserLanguage:Z

    .line 2537
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userCountry:Ljava/lang/String;

    .line 2538
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserCountry:Z

    .line 2539
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorName:Ljava/lang/String;

    .line 2540
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasOperatorName:Z

    .line 2541
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorName:Ljava/lang/String;

    .line 2542
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSimOperatorName:Z

    .line 2543
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorNumericName:Ljava/lang/String;

    .line 2544
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasOperatorNumericName:Z

    .line 2545
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorNumericName:Ljava/lang/String;

    .line 2546
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSimOperatorNumericName:Z

    .line 2547
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->clientId:Ljava/lang/String;

    .line 2548
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasClientId:Z

    .line 2549
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->loggingId:Ljava/lang/String;

    .line 2550
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasLoggingId:Z

    .line 2551
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->cachedSize:I

    .line 2552
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2602
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2603
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserAuthToken:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthToken:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2604
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthToken:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2607
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserAuthTokenSecure:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthTokenSecure:Z

    if-eqz v1, :cond_3

    .line 2608
    :cond_2
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthTokenSecure:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2611
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSoftwareVersion:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->softwareVersion:I

    if-eqz v1, :cond_5

    .line 2612
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->softwareVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2615
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasAid:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->aid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2616
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->aid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2619
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasProductNameAndVersion:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->productNameAndVersion:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2620
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->productNameAndVersion:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2623
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserLanguage:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userLanguage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 2624
    :cond_a
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userLanguage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2627
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserCountry:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userCountry:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 2628
    :cond_c
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userCountry:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2631
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasOperatorName:Z

    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 2632
    :cond_e
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2635
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSimOperatorName:Z

    if-nez v1, :cond_10

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 2636
    :cond_10
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2639
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasOperatorNumericName:Z

    if-nez v1, :cond_12

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorNumericName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 2640
    :cond_12
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorNumericName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2643
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSimOperatorNumericName:Z

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorNumericName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 2644
    :cond_14
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorNumericName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2647
    :cond_15
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasClientId:Z

    if-nez v1, :cond_16

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->clientId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 2648
    :cond_16
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->clientId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2651
    :cond_17
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasLoggingId:Z

    if-nez v1, :cond_18

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->loggingId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 2652
    :cond_18
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->loggingId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2655
    :cond_19
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2663
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2664
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2668
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2669
    :sswitch_0
    return-object p0

    .line 2674
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthToken:Ljava/lang/String;

    .line 2675
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserAuthToken:Z

    goto :goto_0

    .line 2679
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthTokenSecure:Z

    .line 2680
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserAuthTokenSecure:Z

    goto :goto_0

    .line 2684
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->softwareVersion:I

    .line 2685
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSoftwareVersion:Z

    goto :goto_0

    .line 2689
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->aid:Ljava/lang/String;

    .line 2690
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasAid:Z

    goto :goto_0

    .line 2694
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->productNameAndVersion:Ljava/lang/String;

    .line 2695
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasProductNameAndVersion:Z

    goto :goto_0

    .line 2699
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userLanguage:Ljava/lang/String;

    .line 2700
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserLanguage:Z

    goto :goto_0

    .line 2704
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userCountry:Ljava/lang/String;

    .line 2705
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserCountry:Z

    goto :goto_0

    .line 2709
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorName:Ljava/lang/String;

    .line 2710
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasOperatorName:Z

    goto :goto_0

    .line 2714
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorName:Ljava/lang/String;

    .line 2715
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSimOperatorName:Z

    goto :goto_0

    .line 2719
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorNumericName:Ljava/lang/String;

    .line 2720
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasOperatorNumericName:Z

    goto :goto_0

    .line 2724
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorNumericName:Ljava/lang/String;

    .line 2725
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSimOperatorNumericName:Z

    goto :goto_0

    .line 2729
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->clientId:Ljava/lang/String;

    .line 2730
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasClientId:Z

    goto :goto_0

    .line 2734
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->loggingId:Ljava/lang/String;

    .line 2735
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasLoggingId:Z

    goto/16 :goto_0

    .line 2664
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2451
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2558
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserAuthToken:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthToken:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2559
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthToken:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2561
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserAuthTokenSecure:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthTokenSecure:Z

    if-eqz v0, :cond_3

    .line 2562
    :cond_2
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userAuthTokenSecure:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2564
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSoftwareVersion:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->softwareVersion:I

    if-eqz v0, :cond_5

    .line 2565
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->softwareVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2567
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasAid:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->aid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2568
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->aid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2570
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasProductNameAndVersion:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->productNameAndVersion:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2571
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->productNameAndVersion:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2573
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserLanguage:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userLanguage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 2574
    :cond_a
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userLanguage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2576
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasUserCountry:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userCountry:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 2577
    :cond_c
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->userCountry:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2579
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasOperatorName:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 2580
    :cond_e
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2582
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSimOperatorName:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 2583
    :cond_10
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2585
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasOperatorNumericName:Z

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorNumericName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 2586
    :cond_12
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->operatorNumericName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2588
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasSimOperatorNumericName:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorNumericName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 2589
    :cond_14
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->simOperatorNumericName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2591
    :cond_15
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasClientId:Z

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->clientId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 2592
    :cond_16
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->clientId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2594
    :cond_17
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->hasLoggingId:Z

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->loggingId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 2595
    :cond_18
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$RequestPropertiesProto;->loggingId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2597
    :cond_19
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2598
    return-void
.end method
