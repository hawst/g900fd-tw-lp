.class Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;
.super Ljava/lang/Object;
.source "DownloadQueueImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/download/DownloadQueueImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StartNextDownloadRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/download/DownloadQueueImpl;)V
    .locals 0

    .prologue
    .line 427
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadQueueImpl$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/download/DownloadQueueImpl;
    .param p2, "x1"    # Lcom/google/android/finsky/download/DownloadQueueImpl$1;

    .prologue
    .line 427
    invoke-direct {p0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 431
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 432
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    # getter for: Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;
    invoke-static {v13}, Lcom/google/android/finsky/download/DownloadQueueImpl;->access$100(Lcom/google/android/finsky/download/DownloadQueueImpl;)Ljava/util/HashMap;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    move-result v13

    iget-object v14, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    # getter for: Lcom/google/android/finsky/download/DownloadQueueImpl;->mMaxConcurrent:I
    invoke-static {v14}, Lcom/google/android/finsky/download/DownloadQueueImpl;->access$200(Lcom/google/android/finsky/download/DownloadQueueImpl;)I

    move-result v14

    if-lt v13, v14, :cond_1

    .line 490
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    const/4 v10, 0x0

    .line 437
    .local v10, "toStart":Lcom/google/android/finsky/download/Download;
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 438
    .local v9, "toRemove":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Ljava/lang/String;>;"
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    # getter for: Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;
    invoke-static {v13}, Lcom/google/android/finsky/download/DownloadQueueImpl;->access$300(Lcom/google/android/finsky/download/DownloadQueueImpl;)Ljava/util/LinkedHashMap;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .line 440
    .local v12, "urls":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_2
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 441
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 442
    .local v8, "key":Ljava/lang/String;
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    # getter for: Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;
    invoke-static {v13}, Lcom/google/android/finsky/download/DownloadQueueImpl;->access$300(Lcom/google/android/finsky/download/DownloadQueueImpl;)Ljava/util/LinkedHashMap;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/download/Download;

    .line 443
    .local v0, "current":Lcom/google/android/finsky/download/Download;
    invoke-virtual {v9, v8}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 445
    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getState()Lcom/google/android/finsky/download/Download$DownloadState;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/download/Download$DownloadState;->QUEUED:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/download/Download$DownloadState;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 446
    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getMaximumSize()J

    move-result-wide v4

    .line 448
    .local v4, "downloadSize":J
    invoke-static {}, Lcom/google/android/finsky/download/Storage;->dataPartitionAvailableSpace()J

    move-result-wide v2

    .line 449
    .local v2, "dataSpace":J
    invoke-static {}, Lcom/google/android/finsky/download/Storage;->externalStorageAvailableSpace()J

    move-result-wide v6

    .line 452
    .local v6, "externalStorageSpace":J
    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getRequestedDestination()Landroid/net/Uri;

    move-result-object v13

    if-eqz v13, :cond_3

    .line 454
    cmp-long v13, v6, v4

    if-gez v13, :cond_4

    .line 455
    const/16 v13, 0xc6

    invoke-interface {v0, v13}, Lcom/google/android/finsky/download/Download;->setHttpStatus(I)V

    .line 457
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    sget-object v14, Lcom/google/android/finsky/download/Download$DownloadState;->ERROR:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {v13, v0, v14}, Lcom/google/android/finsky/download/DownloadQueueImpl;->setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V

    goto :goto_1

    .line 466
    :cond_3
    cmp-long v13, v2, v4

    if-gez v13, :cond_4

    .line 469
    const/16 v13, 0xc6

    invoke-interface {v0, v13}, Lcom/google/android/finsky/download/Download;->setHttpStatus(I)V

    .line 471
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    sget-object v14, Lcom/google/android/finsky/download/Download$DownloadState;->ERROR:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {v13, v0, v14}, Lcom/google/android/finsky/download/DownloadQueueImpl;->setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V

    goto :goto_1

    .line 475
    :cond_4
    move-object v10, v0

    .line 480
    .end local v0    # "current":Lcom/google/android/finsky/download/Download;
    .end local v2    # "dataSpace":J
    .end local v4    # "downloadSize":J
    .end local v6    # "externalStorageSpace":J
    .end local v8    # "key":Ljava/lang/String;
    :cond_5
    invoke-virtual {v9}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 481
    .local v11, "url":Ljava/lang/String;
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    # getter for: Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;
    invoke-static {v13}, Lcom/google/android/finsky/download/DownloadQueueImpl;->access$300(Lcom/google/android/finsky/download/DownloadQueueImpl;)Ljava/util/LinkedHashMap;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 484
    .end local v11    # "url":Ljava/lang/String;
    :cond_6
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    invoke-virtual {v13, v10}, Lcom/google/android/finsky/download/DownloadQueueImpl;->startDownload(Lcom/google/android/finsky/download/Download;)V

    .line 486
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    # getter for: Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;
    invoke-static {v13}, Lcom/google/android/finsky/download/DownloadQueueImpl;->access$100(Lcom/google/android/finsky/download/DownloadQueueImpl;)Ljava/util/HashMap;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/HashMap;->size()I

    move-result v13

    if-nez v13, :cond_0

    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    # getter for: Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;
    invoke-static {v13}, Lcom/google/android/finsky/download/DownloadQueueImpl;->access$400(Lcom/google/android/finsky/download/DownloadQueueImpl;)Lcom/google/android/finsky/download/DownloadProgressManager;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 487
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    # getter for: Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;
    invoke-static {v13}, Lcom/google/android/finsky/download/DownloadQueueImpl;->access$400(Lcom/google/android/finsky/download/DownloadQueueImpl;)Lcom/google/android/finsky/download/DownloadProgressManager;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/download/DownloadProgressManager;->cleanup()V

    .line 488
    iget-object v13, p0, Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;->this$0:Lcom/google/android/finsky/download/DownloadQueueImpl;

    const/4 v14, 0x0

    # setter for: Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;
    invoke-static {v13, v14}, Lcom/google/android/finsky/download/DownloadQueueImpl;->access$402(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadProgressManager;)Lcom/google/android/finsky/download/DownloadProgressManager;

    goto/16 :goto_0
.end method
