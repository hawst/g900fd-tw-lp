.class public interface abstract Lcom/google/android/finsky/download/Download;
.super Ljava/lang/Object;
.source "Download.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/download/Download$DownloadState;
    }
.end annotation


# virtual methods
.method public abstract getContentUri()Landroid/net/Uri;
.end method

.method public abstract getCookieName()Ljava/lang/String;
.end method

.method public abstract getCookieValue()Ljava/lang/String;
.end method

.method public abstract getHttpStatus()I
.end method

.method public abstract getInvisible()Z
.end method

.method public abstract getMaximumSize()J
.end method

.method public abstract getObb()Lcom/google/android/finsky/download/obb/Obb;
.end method

.method public abstract getPackageName()Ljava/lang/String;
.end method

.method public abstract getProgress()Lcom/google/android/finsky/download/DownloadProgress;
.end method

.method public abstract getRequestedDestination()Landroid/net/Uri;
.end method

.method public abstract getState()Lcom/google/android/finsky/download/Download$DownloadState;
.end method

.method public abstract getTitle()Ljava/lang/String;
.end method

.method public abstract getUrl()Ljava/lang/String;
.end method

.method public abstract getWifiOnly()Z
.end method

.method public abstract isCompleted()Z
.end method

.method public abstract isObb()Z
.end method

.method public abstract setContentUri(Landroid/net/Uri;)V
.end method

.method public abstract setHttpStatus(I)V
.end method

.method public abstract setProgress(Lcom/google/android/finsky/download/DownloadProgress;)V
.end method

.method public abstract setState(Lcom/google/android/finsky/download/Download$DownloadState;)V
.end method
