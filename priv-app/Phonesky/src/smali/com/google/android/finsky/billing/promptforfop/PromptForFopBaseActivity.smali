.class public abstract Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;
.source "PromptForFopBaseActivity.java"

# interfaces
.implements Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;


# instance fields
.field protected mAccount:Landroid/accounts/Account;

.field protected mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field protected mServerLogsCookie:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;-><init>()V

    return-void
.end method

.method protected static putIntentExtras(Landroid/accounts/Account;[BLandroid/content/Intent;)V
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "serverLogsCookie"    # [B
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 38
    const-string v0, "PromptForFopBaseActivity.account"

    invoke-virtual {p2, v0, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 40
    const-string v0, "authAccount"

    iget-object v1, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    const-string v0, "PromptForFopBaseActivity.server_logs_cookie"

    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 42
    return-void
.end method


# virtual methods
.method protected abstract createContentFragment()Landroid/support/v4/app/Fragment;
.end method

.method protected abstract displayMessage(IILjava/lang/String;)V
.end method

.method protected abstract getActivityLayout()I
.end method

.method protected abstract getAlreadySetupEventType()I
.end method

.method protected abstract getBillingProfileErrorEventType()I
.end method

.method protected abstract getSnoozeEventType()I
.end method

.method public onAlreadySetup()V
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->getAlreadySetupEventType()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mServerLogsCookie:[B

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(I[B)V

    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/PromptForFopHelper;->expireHasNoFop(Ljava/lang/String;)V

    .line 113
    const v0, 0x7f0c0122

    const/16 v1, 0x3eb

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->displayMessage(IILjava/lang/String;)V

    .line 115
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->getActivityLayout()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->setContentView(I)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PromptForFopBaseActivity.account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mAccount:Landroid/accounts/Account;

    .line 49
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "PromptForFopBaseActivity.server_logs_cookie"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mServerLogsCookie:[B

    .line 50
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 51
    if-nez p1, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/PromptForFopHelper;->recordFopSelectorImpression(Ljava/lang/String;)V

    .line 54
    :cond_0
    return-void
.end method

.method public onFatalError(Ljava/lang/String;)V
    .locals 3
    .param p1, "errorMessageHtml"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->getBillingProfileErrorEventType()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mServerLogsCookie:[B

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(I[B)V

    .line 105
    const v0, 0x7f0c0123

    const/16 v1, 0x3ec

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->displayMessage(IILjava/lang/String;)V

    .line 107
    return-void
.end method

.method public onInstrumentCreated(Ljava/lang/String;)V
    .locals 2
    .param p1, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/PromptForFopHelper;->recordFopAdded(Ljava/lang/String;)V

    .line 89
    const v0, 0x7f0c0122

    const/16 v1, 0x3ed

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->displayMessage(IILjava/lang/String;)V

    .line 91
    return-void
.end method

.method public onNoneSelected()V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->getSnoozeEventType()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mServerLogsCookie:[B

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(I[B)V

    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/PromptForFopHelper;->snooze(Ljava/lang/String;)V

    .line 97
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->setResult(I)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->finish()V

    .line 99
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 58
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onResume()V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "PromptForFopBaseActivity.fragment"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 60
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-nez v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->createContentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 62
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0a00c4

    const-string v3, "PromptForFopBaseActivity.fragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 66
    :cond_0
    return-void
.end method
