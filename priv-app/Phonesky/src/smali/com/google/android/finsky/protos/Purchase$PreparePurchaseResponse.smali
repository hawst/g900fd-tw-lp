.class public final Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Purchase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreparePurchaseResponse"
.end annotation


# instance fields
.field public applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public appliedVoucherIndex:[I

.field public cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

.field public challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

.field public changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

.field public hasServerLogsCookie:Z

.field public libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

.field public purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

.field public serverLogsCookie:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 909
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 910
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->clear()Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    .line 911
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 914
    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    .line 915
    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 916
    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .line 917
    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    .line 918
    invoke-static {}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->emptyArray()[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 919
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie:[B

    .line 920
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasServerLogsCookie:Z

    .line 921
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 922
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    .line 923
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cachedSize:I

    .line 924
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 971
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v3

    .line 972
    .local v3, "size":I
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    if-eqz v4, :cond_0

    .line 973
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 976
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v4, :cond_1

    .line 977
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 980
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    if-eqz v4, :cond_2

    .line 981
    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 984
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v4, v4

    if-lez v4, :cond_4

    .line 985
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v4, v4

    if-ge v2, v4, :cond_4

    .line 986
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    aget-object v1, v4, v2

    .line 987
    .local v1, "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    if-eqz v1, :cond_3

    .line 988
    const/4 v4, 0x4

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 985
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 993
    .end local v1    # "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .end local v2    # "i":I
    :cond_4
    iget-boolean v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasServerLogsCookie:Z

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie:[B

    sget-object v5, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_6

    .line 994
    :cond_5
    const/4 v4, 0x5

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie:[B

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v4

    add-int/2addr v3, v4

    .line 997
    :cond_6
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v4, v4

    if-lez v4, :cond_8

    .line 998
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v4, v4

    if-ge v2, v4, :cond_8

    .line 999
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v1, v4, v2

    .line 1000
    .local v1, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_7

    .line 1001
    const/4 v4, 0x6

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 998
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1006
    .end local v1    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v2    # "i":I
    :cond_8
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    if-eqz v4, :cond_a

    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    array-length v4, v4

    if-lez v4, :cond_a

    .line 1007
    const/4 v0, 0x0

    .line 1008
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    array-length v4, v4

    if-ge v2, v4, :cond_9

    .line 1009
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    aget v1, v4, v2

    .line 1010
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 1008
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1013
    .end local v1    # "element":I
    :cond_9
    add-int/2addr v3, v0

    .line 1014
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    .line 1016
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_a
    iget-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    if-eqz v4, :cond_b

    .line 1017
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1020
    :cond_b
    return v3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1028
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 1029
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 1033
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1034
    :sswitch_0
    return-object p0

    .line 1039
    :sswitch_1
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    if-nez v8, :cond_1

    .line 1040
    new-instance v8, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    .line 1042
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1046
    :sswitch_2
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-nez v8, :cond_2

    .line 1047
    new-instance v8, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 1049
    :cond_2
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1053
    :sswitch_3
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    if-nez v8, :cond_3

    .line 1054
    new-instance v8, Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/Purchase$ClientCart;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .line 1056
    :cond_3
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1060
    :sswitch_4
    const/16 v8, 0x22

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1062
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-nez v8, :cond_5

    move v1, v7

    .line 1063
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 1065
    .local v4, "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    if-eqz v1, :cond_4

    .line 1066
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1068
    :cond_4
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_6

    .line 1069
    new-instance v8, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    aput-object v8, v4, v1

    .line 1070
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1071
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1068
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1062
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_5
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v1, v8

    goto :goto_1

    .line 1074
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_6
    new-instance v8, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    aput-object v8, v4, v1

    .line 1075
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1076
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    goto :goto_0

    .line 1080
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie:[B

    .line 1081
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasServerLogsCookie:Z

    goto/16 :goto_0

    .line 1085
    :sswitch_6
    const/16 v8, 0x32

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1087
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v8, :cond_8

    move v1, v7

    .line 1088
    .restart local v1    # "i":I
    :goto_3
    add-int v8, v1, v0

    new-array v4, v8, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1090
    .local v4, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_7

    .line 1091
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1093
    :cond_7
    :goto_4
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_9

    .line 1094
    new-instance v8, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v8, v4, v1

    .line 1095
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1096
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1093
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1087
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_8
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v8

    goto :goto_3

    .line 1099
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_9
    new-instance v8, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v8, v4, v1

    .line 1100
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1101
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 1105
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :sswitch_7
    const/16 v8, 0x38

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1107
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    if-nez v8, :cond_b

    move v1, v7

    .line 1108
    .restart local v1    # "i":I
    :goto_5
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 1109
    .local v4, "newArray":[I
    if-eqz v1, :cond_a

    .line 1110
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1112
    :cond_a
    :goto_6
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_c

    .line 1113
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 1114
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1112
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1107
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_b
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    array-length v1, v8

    goto :goto_5

    .line 1117
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 1118
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    goto/16 :goto_0

    .line 1122
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 1123
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 1125
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 1126
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 1127
    .local v5, "startPos":I
    :goto_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_d

    .line 1128
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 1129
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1131
    :cond_d
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 1132
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    if-nez v8, :cond_f

    move v1, v7

    .line 1133
    .restart local v1    # "i":I
    :goto_8
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 1134
    .restart local v4    # "newArray":[I
    if-eqz v1, :cond_e

    .line 1135
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1137
    :cond_e
    :goto_9
    array-length v8, v4

    if-ge v1, v8, :cond_10

    .line 1138
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 1137
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 1132
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_f
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    array-length v1, v8

    goto :goto_8

    .line 1140
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_10
    iput-object v4, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    .line 1141
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 1145
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[I
    .end local v5    # "startPos":I
    :sswitch_9
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    if-nez v8, :cond_11

    .line 1146
    new-instance v8, Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    .line 1148
    :cond_11
    iget-object v8, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1029
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 867
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 930
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    if-eqz v2, :cond_0

    .line 931
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->purchaseStatus:Lcom/google/android/finsky/protos/Purchase$PurchaseStatus;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 933
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-eqz v2, :cond_1

    .line 934
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 936
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    if-eqz v2, :cond_2

    .line 937
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->cart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 939
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 940
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 941
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->libraryUpdate:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    aget-object v0, v2, v1

    .line 942
    .local v0, "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    if-eqz v0, :cond_3

    .line 943
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 940
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 947
    .end local v0    # "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .end local v1    # "i":I
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->hasServerLogsCookie:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_6

    .line 948
    :cond_5
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->serverLogsCookie:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 950
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 951
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 952
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->applicableVoucher:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 953
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_7

    .line 954
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 951
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 958
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    array-length v2, v2

    if-lez v2, :cond_9

    .line 959
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 960
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->appliedVoucherIndex:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 959
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 963
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    if-eqz v2, :cond_a

    .line 964
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/finsky/protos/Purchase$PreparePurchaseResponse;->changeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 966
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 967
    return-void
.end method
