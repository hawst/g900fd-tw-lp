.class public Lcom/google/android/finsky/billing/BillingProfileFragment;
.super Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
.source "BillingProfileFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;
    }
.end annotation


# instance fields
.field private mActionsHeader:Landroid/widget/TextView;

.field private mActionsHeaderSeparator:Landroid/view/View;

.field private mActionsView:Landroid/view/ViewGroup;

.field private mExistingInstrumentsView:Landroid/view/ViewGroup;

.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;-><init>()V

    .line 50
    const/16 v0, 0x320

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 359
    return-void
.end method

.method private addEntry(Landroid/view/ViewGroup;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;ZLjava/lang/String;)V
    .locals 9
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "actionEntry"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .param p3, "isDefault"    # Z
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 117
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 119
    .local v3, "inflater":Landroid/view/LayoutInflater;
    if-nez p4, :cond_1

    .line 120
    const v6, 0x7f040039

    invoke-virtual {v3, v6, p1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 121
    .local v4, "rowView":Landroid/view/View;
    if-eqz p3, :cond_0

    .line 122
    const v6, 0x7f0a010b

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 124
    :cond_0
    iget-object v6, p2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->action:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    :goto_0
    const v6, 0x7f0a009c

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 131
    .local v5, "titleView":Landroid/widget/TextView;
    iget-object v6, p2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->displayTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    const v6, 0x7f0a00f9

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/image/FifeImageView;

    .line 134
    .local v2, "imageView":Lcom/google/android/play/image/FifeImageView;
    iget-object v1, p2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 135
    .local v1, "image":Lcom/google/android/finsky/protos/Common$Image;
    if-nez v1, :cond_2

    .line 136
    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 141
    :goto_1
    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 142
    return-void

    .line 126
    .end local v1    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v2    # "imageView":Lcom/google/android/play/image/FifeImageView;
    .end local v4    # "rowView":Landroid/view/View;
    .end local v5    # "titleView":Landroid/widget/TextView;
    :cond_1
    const v6, 0x7f040038

    invoke-virtual {v3, v6, p1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 127
    .restart local v4    # "rowView":Landroid/view/View;
    const v6, 0x7f0a010a

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 128
    .local v0, "errorView":Landroid/widget/TextView;
    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 138
    .end local v0    # "errorView":Landroid/widget/TextView;
    .restart local v1    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .restart local v2    # "imageView":Lcom/google/android/play/image/FifeImageView;
    .restart local v5    # "titleView":Landroid/widget/TextView;
    :cond_2
    iget-object v6, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v7, v1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v8

    invoke-virtual {v2, v6, v7, v8}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    goto :goto_1
.end method

.method private getListener()Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;
    .locals 2

    .prologue
    .line 266
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;

    if-eqz v0, :cond_0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;

    .line 270
    :goto_0
    return-object v0

    .line 269
    :cond_0
    const-string v0, "No listener registered."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 270
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;I)Lcom/google/android/finsky/billing/BillingProfileFragment;
    .locals 4
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "purchaseContextToken"    # Ljava/lang/String;
    .param p2, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;
    .param p3, "offerType"    # I

    .prologue
    .line 71
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 72
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "BillingProfileFragment.account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 73
    const-string v2, "BillingProfileFragment.purchaseContextToken"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v2, "BillingProfileFragment.docid"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 75
    const-string v2, "BillingProfileFragment.offer_type"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 76
    new-instance v1, Lcom/google/android/finsky/billing/BillingProfileFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/BillingProfileFragment;-><init>()V

    .line 77
    .local v1, "result":Lcom/google/android/finsky/billing/BillingProfileFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->setArguments(Landroid/os/Bundle;)V

    .line 78
    return-object v1
.end method

.method private notifyListenerOnCancel()V
    .locals 1

    .prologue
    .line 259
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->getListener()Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;

    move-result-object v0

    .line 260
    .local v0, "listener":Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;
    if-eqz v0, :cond_0

    .line 261
    invoke-interface {v0}, Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;->onCancel()V

    .line 263
    :cond_0
    return-void
.end method


# virtual methods
.method protected getBackgroundEventServerLogsCookie()[B
    .locals 1

    .prologue
    .line 300
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getBillingProfileRequestEnum()I
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x1

    return v0
.end method

.method protected getCreditCardEventType()I
    .locals 1

    .prologue
    .line 275
    const/16 v0, 0x140

    return v0
.end method

.method protected getDcbEventType()I
    .locals 1

    .prologue
    .line 280
    const/16 v0, 0x141

    return v0
.end method

.method protected getGenericInstrumentEventType()I
    .locals 1

    .prologue
    .line 295
    const/16 v0, 0x144

    return v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected getRedeemEventType()I
    .locals 1

    .prologue
    .line 285
    const/16 v0, 0x142

    return v0
.end method

.method protected getTopupEventType()I
    .locals 1

    .prologue
    .line 290
    const/16 v0, 0x143

    return v0
.end method

.method protected logLoading()V
    .locals 4

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v1, 0xd5

    invoke-virtual {v0, v2, v3, v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 311
    return-void
.end method

.method protected logScreen()V
    .locals 4

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v1, 0x321

    invoke-virtual {v0, v2, v3, v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 316
    return-void
.end method

.method protected notifyListenerOnInstrumentSelected(Ljava/lang/String;)V
    .locals 1
    .param p1, "instrumentId"    # Ljava/lang/String;

    .prologue
    .line 245
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->getListener()Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;

    move-result-object v0

    .line 246
    .local v0, "listener":Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;
    if-eqz v0, :cond_0

    .line 247
    invoke-interface {v0, p1}, Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;->onInstrumentSelected(Ljava/lang/String;)V

    .line 249
    :cond_0
    return-void
.end method

.method protected notifyListenerOnPromoCodeRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)V
    .locals 1
    .param p1, "redeemCodeResult"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .prologue
    .line 252
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->getListener()Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;

    move-result-object v0

    .line 253
    .local v0, "listener":Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;
    if-eqz v0, :cond_0

    .line 254
    invoke-interface {v0, p1}, Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;->onPromoCodeRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)V

    .line 256
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 84
    if-nez p1, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 87
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    const v1, 0x7f040036

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 94
    .local v0, "mainView":Landroid/view/ViewGroup;
    const v1, 0x7f0a0105

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    .line 95
    const v1, 0x7f0a0108

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    .line 96
    const v1, 0x7f0a0109

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mProgressIndicator:Landroid/view/View;

    .line 97
    const v1, 0x7f0a0104

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mProfileView:Landroid/view/View;

    .line 98
    const v1, 0x7f0a0106

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsHeader:Landroid/widget/TextView;

    .line 99
    iget-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsHeader:Landroid/widget/TextView;

    const v2, 0x7f0c0111

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/BillingProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    const v1, 0x7f0a0107

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsHeaderSeparator:Landroid/view/View;

    .line 101
    return-object v0
.end method

.method protected onFatalError(Ljava/lang/String;)V
    .locals 5
    .param p1, "errorMessageHtml"    # Ljava/lang/String;

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->renderProfile()V

    .line 236
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 237
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c02a0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const/16 v3, 0xa

    const/4 v4, 0x0

    invoke-virtual {v2, p0, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 240
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 241
    .local v1, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "BillingProfileFragment.errorDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 242
    return-void
.end method

.method protected onInstrumentCreated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "instrumentId"    # Ljava/lang/String;
    .param p2, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 208
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileFragment;->notifyListenerOnInstrumentSelected(Ljava/lang/String;)V

    .line 209
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 338
    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    .line 339
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->notifyListenerOnCancel()V

    .line 343
    :goto_0
    return-void

    .line 342
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onNegativeClick(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 329
    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    .line 330
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->notifyListenerOnCancel()V

    .line 334
    :goto_0
    return-void

    .line 333
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onPositiveClick(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onPromoCodeRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)V
    .locals 0
    .param p1, "redeemCodeResult"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .prologue
    .line 230
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileFragment;->notifyListenerOnPromoCodeRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)V

    .line 231
    return-void
.end method

.method protected onStoredValueAdded(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "existingStoredValueInstrumentId"    # Ljava/lang/String;
    .param p2, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 214
    if-eqz p1, :cond_0

    .line 215
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileFragment;->notifyListenerOnInstrumentSelected(Ljava/lang/String;)V

    .line 226
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mProfileView:Landroid/view/View;

    new-instance v1, Lcom/google/android/finsky/billing/BillingProfileFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/billing/BillingProfileFragment$2;-><init>(Lcom/google/android/finsky/billing/BillingProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected redeemCheckoutCode()V
    .locals 6

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 321
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v3, "BillingProfileFragment.docid"

    invoke-static {v0, v3}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Common$Docid;

    .line 322
    .local v1, "docid":Lcom/google/android/finsky/protos/Common$Docid;
    iget-object v3, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x3

    const-string v5, "BillingProfileFragment.offer_type"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v3, v4, v1, v5}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->createBuyFlowIntent(Ljava/lang/String;ILcom/google/android/finsky/protos/Common$Docid;I)Landroid/content/Intent;

    move-result-object v2

    .line 324
    .local v2, "intent":Landroid/content/Intent;
    const/4 v3, 0x4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/billing/BillingProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 325
    return-void
.end method

.method protected renderActions(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "actionEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;>;"
    iget-object v3, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 147
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    .line 148
    .local v0, "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    iget-object v3, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, v3, v0, v4, v5}, Lcom/google/android/finsky/billing/BillingProfileFragment;->addEntry(Landroid/view/ViewGroup;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;ZLjava/lang/String;)V

    goto :goto_0

    .line 150
    .end local v0    # "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-lez v3, :cond_1

    .line 151
    iget-object v3, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    .line 154
    .local v2, "lastActionRow":Lcom/google/android/finsky/layout/SeparatorLinearLayout;
    invoke-virtual {v2}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->hideSeparator()V

    .line 156
    .end local v2    # "lastActionRow":Lcom/google/android/finsky/layout/SeparatorLinearLayout;
    :cond_1
    return-void
.end method

.method protected renderInstruments([Lcom/google/android/finsky/protos/CommonDevice$Instrument;)V
    .locals 16
    .param p1, "instruments"    # [Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .prologue
    .line 160
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    invoke-virtual {v13}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 161
    move-object/from16 v0, p1

    array-length v13, v0

    if-nez v13, :cond_0

    .line 162
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsHeader:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsHeaderSeparator:Landroid/view/View;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 169
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iget-object v4, v13, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->selectedExternalInstrumentId:Ljava/lang/String;

    .line 170
    .local v4, "defaultInstrumentId":Ljava/lang/String;
    move-object/from16 v2, p1

    .local v2, "arr$":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    array-length v12, v2

    .local v12, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v12, :cond_2

    aget-object v7, v2, v6

    .line 171
    .local v7, "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    iget-object v13, v7, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    array-length v13, v13

    if-lez v13, :cond_1

    iget-object v13, v7, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    iget-object v5, v13, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledMessageHtml:Ljava/lang/String;

    .line 174
    .local v5, "error":Ljava/lang/String;
    :goto_2
    iget-object v8, v7, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId:Ljava/lang/String;

    .line 175
    .local v8, "instrumentId":Ljava/lang/String;
    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    .line 176
    .local v10, "isDefault":Z
    new-instance v9, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    invoke-direct {v9}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;-><init>()V

    .line 177
    .local v9, "instrumentInfo":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
    iget v13, v7, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    iput v13, v9, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->instrumentFamily:I

    .line 178
    const/4 v13, 0x1

    iput-boolean v13, v9, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasInstrumentFamily:Z

    .line 179
    iput-boolean v10, v9, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->isDefault:Z

    .line 180
    const/4 v13, 0x1

    iput-boolean v13, v9, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasIsDefault:Z

    .line 182
    new-instance v3, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {v3}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 183
    .local v3, "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    iput-object v9, v3, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    .line 185
    new-instance v1, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    iget-object v13, v7, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle:Ljava/lang/String;

    iget-object v14, v7, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    new-instance v15, Lcom/google/android/finsky/billing/BillingProfileFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v3, v8}, Lcom/google/android/finsky/billing/BillingProfileFragment$1;-><init>(Lcom/google/android/finsky/billing/BillingProfileFragment;Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Ljava/lang/String;)V

    invoke-direct {v1, v13, v14, v15}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V

    .line 196
    .local v1, "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v1, v10, v5}, Lcom/google/android/finsky/billing/BillingProfileFragment;->addEntry(Landroid/view/ViewGroup;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;ZLjava/lang/String;)V

    .line 170
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 165
    .end local v1    # "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .end local v2    # "arr$":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .end local v3    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    .end local v4    # "defaultInstrumentId":Ljava/lang/String;
    .end local v5    # "error":Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v7    # "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .end local v8    # "instrumentId":Ljava/lang/String;
    .end local v9    # "instrumentInfo":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
    .end local v10    # "isDefault":Z
    .end local v12    # "len$":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsHeader:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mActionsHeaderSeparator:Landroid/view/View;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 171
    .restart local v2    # "arr$":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .restart local v4    # "defaultInstrumentId":Ljava/lang/String;
    .restart local v6    # "i$":I
    .restart local v7    # "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .restart local v12    # "len$":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_2

    .line 198
    .end local v7    # "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    invoke-virtual {v13}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v13

    if-lez v13, :cond_3

    .line 199
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/BillingProfileFragment;->mExistingInstrumentsView:Landroid/view/ViewGroup;

    invoke-virtual {v14}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    .line 202
    .local v11, "lastInstrumentRow":Lcom/google/android/finsky/layout/SeparatorLinearLayout;
    invoke-virtual {v11}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->hideSeparator()V

    .line 204
    .end local v11    # "lastInstrumentRow":Lcom/google/android/finsky/layout/SeparatorLinearLayout;
    :cond_3
    return-void
.end method
