.class public Lcom/google/android/finsky/layout/SubscriptionView;
.super Landroid/widget/RelativeLayout;
.source "SubscriptionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;,
        Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;
    }
.end annotation


# instance fields
.field private mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mCancelListener:Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;

.field private mDateInfo:Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;

.field private mDescriptionCollapser:Landroid/view/View;

.field private mDescriptionExpander:Landroid/view/View;

.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field private mExpansionState:I

.field private mSubscriptionDescription:Landroid/widget/TextView;

.field private mSubscriptionPrice:Landroid/widget/TextView;

.field private mSubscriptionRenewal:Landroid/widget/TextView;

.field private mSubscriptionStatus:Landroid/widget/TextView;

.field private mSubscriptionTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/SubscriptionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mExpansionState:I

    .line 71
    new-instance v0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;

    invoke-direct {v0}, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDateInfo:Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/SubscriptionView;)Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SubscriptionView;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mCancelListener:Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/SubscriptionView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SubscriptionView;

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mExpansionState:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/finsky/layout/SubscriptionView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SubscriptionView;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mExpansionState:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/SubscriptionView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SubscriptionView;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SubscriptionView;->collapseDescription()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/SubscriptionView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SubscriptionView;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SubscriptionView;->expandDescription()V

    return-void
.end method

.method private collapseDescription()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 237
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDescriptionExpander:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 238
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDescriptionCollapser:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 240
    return-void
.end method

.method private expandDescription()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDescriptionExpander:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 232
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 233
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDescriptionCollapser:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 234
    return-void
.end method

.method public static fillSubscriptionDateInfo(Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;Landroid/content/res/Resources;)V
    .locals 8
    .param p0, "info"    # Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;
    .param p1, "subscriptionDetails"    # Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
    .param p2, "res"    # Landroid/content/res/Resources;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 96
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 97
    .local v0, "now":J
    invoke-virtual {p1}, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->getValidUntilTimestampMs()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/DateUtils;->formatShortDisplayDate(J)Ljava/lang/String;

    move-result-object v3

    .line 99
    .local v3, "validUntil":Ljava/lang/String;
    iget-boolean v4, p1, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->isAutoRenewing:Z

    if-eqz v4, :cond_2

    .line 100
    iget-wide v4, p1, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->trialUntilTimestampMs:J

    cmp-long v4, v0, v4

    if-gez v4, :cond_0

    .line 101
    iget-wide v4, p1, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->trialUntilTimestampMs:J

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/DateUtils;->formatShortDisplayDate(J)Ljava/lang/String;

    move-result-object v2

    .line 103
    .local v2, "trialUntil":Ljava/lang/String;
    const v4, 0x7f0c021a

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {p2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->renewalDescription:Ljava/lang/CharSequence;

    .line 105
    const v4, 0x7f0c0221

    iput v4, p0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->statusResourceId:I

    .line 118
    .end local v2    # "trialUntil":Ljava/lang/String;
    :goto_0
    iput-boolean v7, p0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->canCancel:Z

    .line 125
    :goto_1
    return-void

    .line 107
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->getValidUntilTimestampMs()J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-gez v4, :cond_1

    .line 108
    const v4, 0x7f0c021b

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v6

    invoke-virtual {p2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->renewalDescription:Ljava/lang/CharSequence;

    .line 115
    :goto_2
    const v4, 0x7f0c0220

    iput v4, p0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->statusResourceId:I

    goto :goto_0

    .line 113
    :cond_1
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->renewalDescription:Ljava/lang/CharSequence;

    goto :goto_2

    .line 120
    :cond_2
    const v4, 0x7f0c021c

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v6

    invoke-virtual {p2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->renewalDescription:Ljava/lang/CharSequence;

    .line 122
    const v4, 0x7f0c0222

    iput v4, p0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->statusResourceId:I

    .line 123
    iput-boolean v6, p0, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->canCancel:Z

    goto :goto_1
.end method

.method private hideDescription()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 225
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionDescription:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 226
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDescriptionCollapser:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 227
    iget-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDescriptionExpander:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 228
    return-void
.end method


# virtual methods
.method public configure(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;Landroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 9
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "subscriptionDetails"    # Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
    .param p3, "cancelListener"    # Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;
    .param p4, "savedState"    # Landroid/os/Bundle;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 130
    iput-object p1, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 131
    iput-object p3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mCancelListener:Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;

    .line 133
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    invoke-virtual {p1, v7}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    .line 136
    .local v0, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    if-eqz v3, :cond_1

    .line 137
    iget-object v3, v0, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionTerms:Lcom/google/android/finsky/protos/Common$SubscriptionTerms;

    iget-object v1, v3, Lcom/google/android/finsky/protos/Common$SubscriptionTerms;->formattedPriceWithRecurrencePeriod:Ljava/lang/String;

    .line 138
    .local v1, "priceWithPeriod":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 139
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionPrice:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionPrice:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    .end local v1    # "priceWithPeriod":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionStatus:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SubscriptionView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 155
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SubscriptionView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 156
    .local v2, "res":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDateInfo:Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;

    invoke-static {v3, p2, v2}, Lcom/google/android/finsky/layout/SubscriptionView;->fillSubscriptionDateInfo(Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;Landroid/content/res/Resources;)V

    .line 157
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDateInfo:Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;

    iget-object v3, v3, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->renewalDescription:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 158
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionRenewal:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionStatus:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDateInfo:Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;

    iget v4, v4, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->statusResourceId:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 164
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDateInfo:Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;

    iget-boolean v3, v3, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->canCancel:Z

    if-eqz v3, :cond_3

    .line 165
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v3, v6}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 166
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    const v5, 0x7f0c021d

    new-instance v6, Lcom/google/android/finsky/layout/SubscriptionView$1;

    invoke-direct {v6, p0, p1, p5, p2}, Lcom/google/android/finsky/layout/SubscriptionView$1;-><init>(Lcom/google/android/finsky/layout/SubscriptionView;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;)V

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 183
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v3}, Lcom/google/android/play/layout/PlayActionButton;->getId()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/SubscriptionView;->setNextFocusRightId(I)V

    .line 184
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SubscriptionView;->getId()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/play/layout/PlayActionButton;->setNextFocusLeftId(I)V

    .line 191
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 192
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SubscriptionView;->hideDescription()V

    .line 222
    :goto_3
    return-void

    .line 142
    .end local v2    # "res":Landroid/content/res/Resources;
    .restart local v1    # "priceWithPeriod":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionPrice:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 143
    const-string v3, "Document for %s does not contain a formatted price."

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 147
    .end local v1    # "priceWithPeriod":Ljava/lang/String;
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionPrice:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 148
    const-string v3, "Document for %s does not contain a subscription offer or terms."

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 160
    .restart local v2    # "res":Landroid/content/res/Resources;
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionRenewal:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionRenewal:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDateInfo:Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;

    iget-object v4, v4, Lcom/google/android/finsky/layout/SubscriptionView$SubscriptionDateInfo;->renewalDescription:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 186
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v3, v8}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 188
    const/4 v3, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/SubscriptionView;->setNextFocusRightId(I)V

    goto :goto_2

    .line 196
    :cond_4
    iget-object v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionDescription:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    iget v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mExpansionState:I

    if-gez v3, :cond_5

    .line 199
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-static {p4, v3, v7}, Lcom/google/android/finsky/utils/ExpandableUtils;->getSavedExpansionState(Landroid/os/Bundle;Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mExpansionState:I

    .line 203
    :cond_5
    iget v3, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mExpansionState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 204
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SubscriptionView;->expandDescription()V

    .line 210
    :goto_4
    new-instance v3, Lcom/google/android/finsky/layout/SubscriptionView$2;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/layout/SubscriptionView$2;-><init>(Lcom/google/android/finsky/layout/SubscriptionView;)V

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/SubscriptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 206
    :cond_6
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SubscriptionView;->collapseDescription()V

    goto :goto_4
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 78
    const v0, 0x7f0a039e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SubscriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionTitle:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0a03a0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SubscriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionPrice:Landroid/widget/TextView;

    .line 80
    const v0, 0x7f0a03a1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SubscriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionRenewal:Landroid/widget/TextView;

    .line 81
    const v0, 0x7f0a039f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SubscriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionStatus:Landroid/widget/TextView;

    .line 82
    const v0, 0x7f0a03a2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SubscriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 83
    const v0, 0x7f0a03a4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SubscriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mSubscriptionDescription:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0a03a3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SubscriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDescriptionExpander:Landroid/view/View;

    .line 85
    const v0, 0x7f0a03a5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SubscriptionView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDescriptionCollapser:Landroid/view/View;

    .line 86
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 243
    iget-object v1, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    .line 244
    .local v0, "docId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 245
    iget v1, p0, Lcom/google/android/finsky/layout/SubscriptionView;->mExpansionState:I

    invoke-static {p1, v0, v1}, Lcom/google/android/finsky/utils/ExpandableUtils;->saveExpansionState(Landroid/os/Bundle;Ljava/lang/String;I)V

    .line 247
    :cond_0
    return-void
.end method
