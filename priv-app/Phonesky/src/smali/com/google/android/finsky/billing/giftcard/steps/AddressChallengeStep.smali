.class public Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "AddressChallengeStep.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

.field private mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

.field private mBillingUiMode:I

.field private mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

.field private mCountries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;",
            ">;"
        }
    .end annotation
.end field

.field private mMainView:Landroid/view/ViewGroup;

.field private mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mSavedInstanceState:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 52
    const/16 v0, 0x450

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)Lcom/google/android/finsky/layout/BillingAddress;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->onBillingCountriesLoaded()V

    return-void
.end method

.method private clearErrorMessages()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/BillingAddress;->clearErrorMessage()V

    .line 209
    return-void
.end method

.method private displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;
    .locals 1
    .param p1, "error"    # Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/BillingAddress;->displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method private displayErrors(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p1, "inputValidationErrors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->clearErrorMessages()V

    .line 255
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 256
    .local v1, "errorFields":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/widget/TextView;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .line 257
    .local v0, "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;

    move-result-object v3

    .line 258
    .local v3, "textView":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 259
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 263
    .end local v0    # "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .end local v3    # "textView":Landroid/widget/TextView;
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    invoke-static {v5, v1}, Lcom/google/android/finsky/billing/BillingUtils;->getTopMostView(Landroid/view/ViewGroup;Ljava/util/Collection;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 264
    .local v4, "topMostErrorField":Landroid/widget/TextView;
    if-eqz v4, :cond_2

    .line 265
    invoke-virtual {v4}, Landroid/widget/TextView;->requestFocus()Z

    .line 267
    :cond_2
    return-void
.end method

.method private getAddressOrShowErrors()Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 2

    .prologue
    .line 285
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/BillingAddress;->getAddressValidationErrors()Ljava/util/List;

    move-result-object v0

    .line 286
    .local v0, "validationErrors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;>;"
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->displayErrors(Ljava/util/List;)V

    .line 287
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 288
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/BillingAddress;->getAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v1

    .line 290
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getCheckedCheckboxIds()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 294
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 295
    .local v2, "checkedCheckboxIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v1, v4

    .line 296
    .local v1, "checkboxCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 297
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    aget-object v5, v5, v3

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 298
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 299
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    aget-object v4, v4, v3

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->id:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 302
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    return-object v4
.end method

.method private initializeCountriesFromChallenge()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 169
    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    array-length v5, v5

    invoke-static {v5}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mCountries:Ljava/util/List;

    .line 170
    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v0, v5, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/ChallengeProto$Country;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 171
    .local v2, "inCountry":Lcom/google/android/finsky/protos/ChallengeProto$Country;
    new-instance v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    invoke-direct {v4}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;-><init>()V

    .line 172
    .local v4, "outCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    iget-object v5, v2, Lcom/google/android/finsky/protos/ChallengeProto$Country;->regionCode:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryCode:Ljava/lang/String;

    .line 173
    iput-boolean v6, v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->hasCountryCode:Z

    .line 174
    iget-object v5, v2, Lcom/google/android/finsky/protos/ChallengeProto$Country;->displayName:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryName:Ljava/lang/String;

    .line 175
    iput-boolean v6, v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->hasCountryName:Z

    .line 176
    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mCountries:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 178
    .end local v2    # "inCountry":Lcom/google/android/finsky/protos/ChallengeProto$Country;
    .end local v4    # "outCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0, v5}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->setupWidgets(Landroid/os/Bundle;)V

    .line 179
    return-void
.end method

.method private loadBillingCountries()V
    .locals 4

    .prologue
    .line 182
    new-instance v1, Lcom/google/android/finsky/billing/GetBillingCountriesAction;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/GetBillingCountriesAction;-><init>()V

    .line 183
    .local v1, "getBillingCountriesAction":Lcom/google/android/finsky/billing/GetBillingCountriesAction;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "authAccount"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 184
    .local v0, "account":Ljava/lang/String;
    new-instance v2, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$3;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$3;-><init>(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/finsky/billing/GetBillingCountriesAction;->run(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 190
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;I)Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    .param p2, "mode"    # I

    .prologue
    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 75
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v2, "ConfirmationStep.challenge"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 77
    const-string v2, "ui_mode"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 78
    new-instance v1, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;-><init>()V

    .line 79
    .local v1, "result":Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->setArguments(Landroid/os/Bundle;)V

    .line 80
    return-object v1
.end method

.method private onBillingCountriesLoaded()V
    .locals 2

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getBillingCountries()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mCountries:Ljava/util/List;

    .line 197
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mCountries:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mCountries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->setupWidgets(Landroid/os/Bundle;)V

    goto :goto_0

    .line 200
    :cond_1
    const-string v0, "BillingCountries not loaded."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private setupWidgets(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 220
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mCountries:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/BillingAddress;->setBillingCountries(Ljava/util/List;)V

    .line 221
    if-eqz p1, :cond_0

    .line 222
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/layout/BillingAddress;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 244
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    iget-object v2, v2, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 226
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    iget-object v2, v2, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mCountries:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/BillingUtils;->findCountry(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    move-result-object v0

    .line 228
    .local v0, "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/finsky/layout/BillingAddress;->setAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;Lcom/google/android/finsky/protos/BillingAddress$Address;)V

    .line 235
    .end local v0    # "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    :goto_1
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$4;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$4;-><init>(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 230
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/BillingUtils;->getDefaultCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mCountries:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/BillingUtils;->findCountry(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    move-result-object v1

    .line 232
    .local v1, "defaultCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    iget-object v3, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/finsky/layout/BillingAddress;->setAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;)V

    goto :goto_1
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 307
    const v0, 0x7f0c020d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onContinueButtonClicked()V
    .locals 3

    .prologue
    .line 312
    const/16 v1, 0x451

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->logClick(I)V

    .line 313
    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getAddressOrShowErrors()Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v0

    .line 314
    .local v0, "address":Lcom/google/android/finsky/protos/BillingAddress$Address;
    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getCheckedCheckboxIds()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->addressChallenge(Lcom/google/android/finsky/protos/BillingAddress$Address;[Ljava/lang/String;)V

    .line 317
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ConfirmationStep.challenge"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 87
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ui_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingUiMode:I

    .line 88
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mSavedInstanceState:Landroid/os/Bundle;

    .line 95
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingUiMode:I

    if-nez v12, :cond_3

    const v8, 0x7f040182

    .line 98
    .local v8, "layoutId":I
    :goto_0
    const/4 v12, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v8, v1, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    .line 99
    new-instance v12, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-direct {v12}, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    .line 100
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    array-length v10, v12

    .line 101
    .local v10, "oldLength":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    new-array v13, v10, [I

    iput-object v13, v12, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    .line 102
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    iget-object v14, v14, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    const/4 v15, 0x0

    invoke-static {v12, v13, v14, v15, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 105
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorHtml:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_0

    if-nez p3, :cond_0

    .line 108
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    new-instance v13, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$1;-><init>(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)V

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 117
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    const v13, 0x7f0a009c

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 118
    .local v11, "title":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_2

    .line 119
    if-eqz v11, :cond_1

    .line 120
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_1
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingUiMode:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_2

    .line 123
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v13, v13, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    invoke-virtual {v12, v13}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 128
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    const v13, 0x7f0a00bb

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 129
    .local v5, "description":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 130
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v12

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual {v5}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v12

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 132
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v12

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 138
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    invoke-static {v12}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    .line 139
    .local v9, "layoutInflater":Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    const v13, 0x7f0a00b4

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 140
    .local v4, "contentView":Landroid/view/ViewGroup;
    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v12

    add-int/lit8 v2, v12, 0x1

    .line 141
    .local v2, "baseIndex":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v12, v12

    if-ge v7, v12, :cond_5

    .line 142
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    aget-object v6, v12, v7

    .line 143
    .local v6, "formCheckbox":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    const v12, 0x7f04002d

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    const/4 v14, 0x0

    invoke-virtual {v9, v12, v13, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    .line 145
    .local v3, "checkBox":Landroid/widget/CheckBox;
    iget-object v12, v6, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    invoke-virtual {v3, v12}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 146
    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 147
    iget-boolean v12, v6, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    invoke-virtual {v3, v12}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 148
    add-int v12, v2, v7

    invoke-virtual {v4, v3, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 141
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 95
    .end local v2    # "baseIndex":I
    .end local v3    # "checkBox":Landroid/widget/CheckBox;
    .end local v4    # "contentView":Landroid/view/ViewGroup;
    .end local v5    # "description":Landroid/widget/TextView;
    .end local v6    # "formCheckbox":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    .end local v7    # "i":I
    .end local v8    # "layoutId":I
    .end local v9    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v10    # "oldLength":I
    .end local v11    # "title":Landroid/widget/TextView;
    :cond_3
    const v8, 0x7f0401a7

    goto/16 :goto_0

    .line 134
    .restart local v5    # "description":Landroid/widget/TextView;
    .restart local v8    # "layoutId":I
    .restart local v10    # "oldLength":I
    .restart local v11    # "title":Landroid/widget/TextView;
    :cond_4
    const/16 v12, 0x8

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 152
    .restart local v2    # "baseIndex":I
    .restart local v4    # "contentView":Landroid/view/ViewGroup;
    .restart local v7    # "i":I
    .restart local v9    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_5
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    const v13, 0x7f0a00e6

    invoke-virtual {v12, v13}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/layout/BillingAddress;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    .line 153
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    new-instance v13, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$2;-><init>(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)V

    invoke-virtual {v12, v13}, Lcom/google/android/finsky/layout/BillingAddress;->setBillingCountryChangeListener(Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v12, v12, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    array-length v12, v12

    if-lez v12, :cond_6

    .line 160
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->initializeCountriesFromChallenge()V

    .line 165
    :goto_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mMainView:Landroid/view/ViewGroup;

    return-object v12

    .line 162
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->loadBillingCountries()V

    goto :goto_3
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 213
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/BillingAddress;->saveInstanceState(Landroid/os/Bundle;)V

    .line 217
    :cond_0
    return-void
.end method
