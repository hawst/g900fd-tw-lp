.class Lcom/google/android/finsky/layout/OrderHistorySection$1;
.super Ljava/lang/Object;
.source "OrderHistorySection.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/OrderHistorySection;->renderOrderHistoryList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/OrderHistorySection;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/OrderHistorySection;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/finsky/layout/OrderHistorySection$1;->this$0:Lcom/google/android/finsky/layout/OrderHistorySection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection$1;->this$0:Lcom/google/android/finsky/layout/OrderHistorySection;

    # getter for: Lcom/google/android/finsky/layout/OrderHistorySection;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistorySection;->access$000(Lcom/google/android/finsky/layout/OrderHistorySection;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0xa28

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/OrderHistorySection$1;->this$0:Lcom/google/android/finsky/layout/OrderHistorySection;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistorySection$1;->this$0:Lcom/google/android/finsky/layout/OrderHistorySection;

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistorySection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistorySection$1;->this$0:Lcom/google/android/finsky/layout/OrderHistorySection;

    # getter for: Lcom/google/android/finsky/layout/OrderHistorySection;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;
    invoke-static {v1}, Lcom/google/android/finsky/layout/OrderHistorySection;->access$100(Lcom/google/android/finsky/layout/OrderHistorySection;)Lcom/google/android/finsky/api/model/DfeList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->getInitialUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/OrderHistorySection$1;->this$0:Lcom/google/android/finsky/layout/OrderHistorySection;

    # getter for: Lcom/google/android/finsky/layout/OrderHistorySection;->mTitleView:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/finsky/layout/OrderHistorySection;->access$200(Lcom/google/android/finsky/layout/OrderHistorySection;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/OrderHistorySection$1;->this$0:Lcom/google/android/finsky/layout/OrderHistorySection;

    # getter for: Lcom/google/android/finsky/layout/OrderHistorySection;->mToc:Lcom/google/android/finsky/api/model/DfeToc;
    invoke-static {v3}, Lcom/google/android/finsky/layout/OrderHistorySection;->access$300(Lcom/google/android/finsky/layout/OrderHistorySection;)Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToOrderHistory(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 123
    return-void
.end method
