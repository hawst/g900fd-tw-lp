.class public final Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;
.super Lcom/google/protobuf/nano/MessageNano;
.source "SingleFopPaymentsIntegrator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SingleFopPaymentsIntegratorContext"
.end annotation


# instance fields
.field public commonToken:[B

.field public hasCommonToken:Z

.field public hasInstrumentToken:Z

.field public instrumentToken:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->clear()Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    .line 35
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->commonToken:[B

    .line 39
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->hasCommonToken:Z

    .line 40
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->instrumentToken:[B

    .line 41
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->hasInstrumentToken:Z

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->cachedSize:I

    .line 43
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 60
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 61
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->hasCommonToken:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->commonToken:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 62
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->commonToken:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 65
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->hasInstrumentToken:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->instrumentToken:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 66
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->instrumentToken:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 77
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 78
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 82
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    :sswitch_0
    return-object p0

    .line 88
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->commonToken:[B

    .line 89
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->hasCommonToken:Z

    goto :goto_0

    .line 93
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->instrumentToken:[B

    .line 94
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->hasInstrumentToken:Z

    goto :goto_0

    .line 78
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->hasCommonToken:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->commonToken:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 50
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->commonToken:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 52
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->hasInstrumentToken:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->instrumentToken:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_3

    .line 53
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->instrumentToken:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 55
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 56
    return-void
.end method
