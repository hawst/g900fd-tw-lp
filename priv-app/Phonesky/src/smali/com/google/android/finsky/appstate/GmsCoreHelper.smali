.class public Lcom/google/android/finsky/appstate/GmsCoreHelper;
.super Ljava/lang/Object;
.source "GmsCoreHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/appstate/GmsCoreHelper$GMSCoreNotifier;
    }
.end annotation


# static fields
.field private static final NLP_PACKAGE_NAME:Ljava/lang/String;

.field private static final NLP_SHARED_USER_ID:Ljava/lang/String;

.field private static sNlpDownload:Lcom/google/android/finsky/download/Download;

.field private static sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;


# instance fields
.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mContext:Landroid/content/Context;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 63
    sget-object v0, Lcom/google/android/finsky/config/G;->nlpSharedUserId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->NLP_SHARED_USER_ID:Ljava/lang/String;

    .line 64
    sget-object v0, Lcom/google/android/finsky/config/G;->nlpPackageName:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->NLP_PACKAGE_NAME:Ljava/lang/String;

    .line 74
    sput-object v1, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .line 75
    sput-object v1, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpDownload:Lcom/google/android/finsky/download/Download;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Landroid/content/Context;)V
    .locals 0
    .param p1, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p2, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-object p1, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 79
    iput-object p2, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 80
    iput-object p3, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mContext:Landroid/content/Context;

    .line 81
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->NLP_PACKAGE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->NLP_SHARED_USER_ID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Lcom/google/android/finsky/download/Download;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpDownload:Lcom/google/android/finsky/download/Download;

    return-object v0
.end method

.method static synthetic access$300()Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/download/Download;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/download/Download;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-static {p0, p1}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->installNlpCleanup(Lcom/google/android/finsky/download/Download;Ljava/lang/String;)V

    return-void
.end method

.method private static checkForNlpDamage(Lcom/google/android/finsky/FinskyApp;Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;)Z
    .locals 19
    .param p0, "finskyApp"    # Lcom/google/android/finsky/FinskyApp;
    .param p1, "report"    # Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    .prologue
    .line 444
    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v15, Lcom/google/android/finsky/config/G;->nlpCleanupSdkVersionMin:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move/from16 v0, v16

    if-ge v0, v15, :cond_0

    .line 445
    const/4 v15, 0x4

    move-object/from16 v0, p1

    iput v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 446
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 447
    const/4 v15, 0x0

    .line 540
    :goto_0
    return v15

    .line 449
    :cond_0
    sget v16, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v15, Lcom/google/android/finsky/config/G;->nlpCleanupSdkVersionMax:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move/from16 v0, v16

    if-le v0, v15, :cond_1

    .line 450
    const/4 v15, 0x4

    move-object/from16 v0, p1

    iput v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 451
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 452
    const/4 v15, 0x0

    goto :goto_0

    .line 456
    :cond_1
    const-string v15, "location"

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/finsky/FinskyApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/location/LocationManager;

    .line 458
    .local v7, "locationManager":Landroid/location/LocationManager;
    invoke-virtual {v7}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v13

    .line 459
    .local v13, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    .line 460
    .local v12, "providerName":Ljava/lang/String;
    const-string v15, "network"

    invoke-virtual {v15, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 461
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 462
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 463
    const/4 v15, 0x0

    goto :goto_0

    .line 473
    .end local v12    # "providerName":Ljava/lang/String;
    :cond_3
    :try_start_0
    sget-object v15, Lcom/google/android/finsky/config/G;->nlpPackageName:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 474
    .local v11, "packageName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    .line 475
    .local v10, "packageManager":Landroid/content/pm/PackageManager;
    const/16 v15, 0x2040

    invoke-virtual {v10, v11, v15}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 477
    .local v9, "packageInfo":Landroid/content/pm/PackageInfo;
    invoke-virtual {v10, v11}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 485
    .local v8, "packageEnabled":I
    iget-object v15, v9, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v15, Landroid/content/pm/ApplicationInfo;->flags:I

    .line 486
    .local v4, "flags":I
    move-object/from16 v0, p1

    iput v4, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->flags:I

    .line 487
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFlags:Z

    .line 488
    iget v14, v9, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 489
    .local v14, "versionCode":I
    move-object/from16 v0, p1

    iput v14, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    .line 490
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasVersionCode:Z

    .line 491
    move-object/from16 v0, p1

    iput v8, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->enabled:I

    .line 492
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasEnabled:Z

    .line 498
    sget-object v15, Lcom/google/android/finsky/config/G;->nlpCleanupFlagsMask:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    and-int v16, v4, v15

    sget-object v15, Lcom/google/android/finsky/config/G;->nlpCleanupFlagsSet:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    move/from16 v0, v16

    if-eq v0, v15, :cond_4

    .line 499
    const-string v15, "NLP incorrect flags %d"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 500
    const/4 v15, 0x6

    move-object/from16 v0, p1

    iput v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 501
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 502
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 478
    .end local v4    # "flags":I
    .end local v8    # "packageEnabled":I
    .end local v9    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v10    # "packageManager":Landroid/content/pm/PackageManager;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v14    # "versionCode":I
    :catch_0
    move-exception v1

    .line 479
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v15, "NLP package not found"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 480
    const/4 v15, 0x5

    move-object/from16 v0, p1

    iput v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 481
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 482
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 505
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "flags":I
    .restart local v8    # "packageEnabled":I
    .restart local v9    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v10    # "packageManager":Landroid/content/pm/PackageManager;
    .restart local v11    # "packageName":Ljava/lang/String;
    .restart local v14    # "versionCode":I
    :cond_4
    sget-object v15, Lcom/google/android/finsky/config/G;->nlpCleanupNlpVersionMin:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    if-ge v14, v15, :cond_5

    .line 506
    const-string v15, "NLP version %d too low"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 507
    const/4 v15, 0x7

    move-object/from16 v0, p1

    iput v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 508
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 509
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 511
    :cond_5
    sget-object v15, Lcom/google/android/finsky/config/G;->nlpCleanupNlpVersionMax:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v15

    if-le v14, v15, :cond_6

    .line 512
    const-string v15, "NLP version %d too high"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 513
    const/4 v15, 0x7

    move-object/from16 v0, p1

    iput v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 514
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 515
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 518
    :cond_6
    sget-object v15, Lcom/google/android/finsky/config/G;->nlpCleanupExpectedSignature:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 519
    .local v2, "expected_release":Ljava/lang/String;
    sget-object v15, Lcom/google/android/finsky/config/G;->nlpCleanupExpectedSignatureTestKeys:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v15}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 520
    .local v3, "expected_testKeys":Ljava/lang/String;
    iget-object v15, v9, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/16 v16, 0x0

    aget-object v15, v15, v16

    invoke-virtual {v15}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v15

    invoke-static {v15}, Lcom/google/android/finsky/utils/Sha1Util;->secureHash([B)Ljava/lang/String;

    move-result-object v5

    .line 521
    .local v5, "hash":Ljava/lang/String;
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 537
    :goto_1
    const-string v15, "NLP package found but reported inactive"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538
    const/4 v15, 0x2

    move-object/from16 v0, p1

    iput v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 539
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 540
    const/4 v15, 0x1

    goto/16 :goto_0

    .line 523
    :cond_7
    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 525
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->foundTestKeys:Z

    .line 526
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasFoundTestKeys:Z

    goto :goto_1

    .line 528
    :cond_8
    const-string v15, "NLP signature hash mismatch %s"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v5, v16, v17

    invoke-static/range {v15 .. v16}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 529
    move-object/from16 v0, p1

    iput-object v5, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->signatureHash:Ljava/lang/String;

    .line 530
    if-eqz v5, :cond_9

    const/4 v15, 0x1

    :goto_2
    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasSignatureHash:Z

    .line 531
    const/16 v15, 0x8

    move-object/from16 v0, p1

    iput v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 532
    const/4 v15, 0x1

    move-object/from16 v0, p1

    iput-boolean v15, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 533
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 530
    :cond_9
    const/4 v15, 0x0

    goto :goto_2
.end method

.method public static cleanupNlp(Lcom/google/android/finsky/FinskyApp;)V
    .locals 10
    .param p0, "finskyApp"    # Lcom/google/android/finsky/FinskyApp;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 379
    sget-object v5, Lcom/google/android/finsky/config/G;->nlpCleanupConfigurationId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 380
    .local v1, "newConfigurationId":I
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupConfigurationId:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 381
    .local v3, "oldConfigurationId":I
    if-eq v1, v3, :cond_0

    .line 382
    const-string v5, "Resetting state because new config id %d"

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 384
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupHoldoffUntilBoot:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 385
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupHoldoffAfterInstall:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 387
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupConfigurationId:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 391
    :cond_0
    new-instance v4, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    invoke-direct {v4}, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;-><init>()V

    .line 392
    .local v4, "report":Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;
    invoke-static {p0, v4}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->checkForNlpDamage(Lcom/google/android/finsky/FinskyApp;Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;)Z

    move-result v2

    .line 393
    .local v2, "nlpDamaged":Z
    const-string v5, "result=%b type=%d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v6, v9

    iget v7, v4, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 396
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupHoldoffUntilBoot:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iput-boolean v5, v4, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffUntilBoot:Z

    .line 397
    iput-boolean v8, v4, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffUntilBoot:Z

    .line 398
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupHoldoffAfterInstall:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iput-boolean v5, v4, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->holdoffAfterInstall:Z

    .line 399
    iput-boolean v8, v4, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasHoldoffAfterInstall:Z

    .line 402
    if-eqz v2, :cond_2

    .line 403
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupHoldoffUntilBoot:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_1

    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupHoldoffAfterInstall:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 405
    :cond_1
    const-string v5, "Skip repair because holdoff set"

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 406
    const/4 v5, 0x3

    iput v5, v4, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    .line 407
    iput-boolean v8, v4, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->hasRepairStatus:Z

    .line 414
    :cond_2
    :goto_0
    sget-object v5, Lcom/google/android/finsky/config/G;->nlpCleanupLogCommonStatuses:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 415
    .local v0, "logCommonStatuses":Z
    if-nez v0, :cond_3

    iget v5, v4, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->repairStatus:I

    invoke-static {v5}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isCommonNlpRepairStatus(I)Z

    move-result v5

    if-nez v5, :cond_4

    .line 416
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logNlpCleanupData(Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;)V

    .line 418
    :cond_4
    return-void

    .line 409
    .end local v0    # "logCommonStatuses":Z
    :cond_5
    invoke-static {p0, v4}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->downloadAndInstallNlpCleanup(Lcom/google/android/finsky/FinskyApp;Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;)V

    goto :goto_0
.end method

.method private static downloadAndInstallNlpCleanup(Lcom/google/android/finsky/FinskyApp;Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;)V
    .locals 17
    .param p0, "finskyApp"    # Lcom/google/android/finsky/FinskyApp;
    .param p1, "report"    # Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;

    .prologue
    .line 550
    new-instance v3, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v3}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    sput-object v3, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .line 551
    sget-object v3, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    iput v5, v3, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 552
    sget-object v3, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 554
    sget-object v3, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 555
    sget-object v3, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    .line 557
    sget-object v3, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    iput v5, v3, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 558
    sget-object v3, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 564
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->foundTestKeys:Z

    if-eqz v3, :cond_2

    .line 565
    sget-object v3, Lcom/google/android/finsky/config/G;->nlpCleanupUrlTestKeys:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 566
    .local v4, "url":Ljava/lang/String;
    sget-object v3, Lcom/google/android/finsky/config/G;->nlpCleanupCookieNameTestKeys:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 567
    .local v7, "cookieName":Ljava/lang/String;
    sget-object v3, Lcom/google/android/finsky/config/G;->nlpCleanupCookieValueTestKeys:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 574
    .local v8, "cookieValue":Ljava/lang/String;
    :goto_0
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    move-object/from16 v0, p1

    iget v9, v0, Lcom/google/android/finsky/analytics/PlayStore$NlpRepairStatus;->versionCode:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 576
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 577
    :cond_0
    const/4 v7, 0x0

    .line 578
    const/4 v8, 0x0

    .line 582
    :cond_1
    new-instance v3, Lcom/google/android/finsky/download/DownloadImpl;

    const-string v5, ""

    const/4 v6, 0x0

    const/4 v9, 0x0

    const-wide/16 v10, -0x1

    const-wide/16 v12, -0x1

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x1

    invoke-direct/range {v3 .. v16}, Lcom/google/android/finsky/download/DownloadImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/finsky/download/obb/Obb;ZZ)V

    sput-object v3, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpDownload:Lcom/google/android/finsky/download/Download;

    .line 584
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getDownloadQueue()Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v2

    .line 585
    .local v2, "downloadQueue":Lcom/google/android/finsky/download/DownloadQueue;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v5

    sget-object v3, Lcom/google/android/finsky/config/G;->nlpPackageName:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v5, v3}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->getListener(Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;)Lcom/google/android/finsky/download/DownloadQueueListener;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/finsky/download/DownloadQueue;->addListener(Lcom/google/android/finsky/download/DownloadQueueListener;)V

    .line 586
    sget-object v3, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpDownload:Lcom/google/android/finsky/download/Download;

    invoke-interface {v2, v3}, Lcom/google/android/finsky/download/DownloadQueue;->add(Lcom/google/android/finsky/download/Download;)V

    .line 588
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v9

    const/16 v10, 0x64

    sget-object v3, Lcom/google/android/finsky/config/G;->nlpPackageName:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    sget-object v15, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-virtual/range {v9 .. v15}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 591
    return-void

    .line 569
    .end local v2    # "downloadQueue":Lcom/google/android/finsky/download/DownloadQueue;
    .end local v4    # "url":Ljava/lang/String;
    .end local v7    # "cookieName":Ljava/lang/String;
    .end local v8    # "cookieValue":Ljava/lang/String;
    :cond_2
    sget-object v3, Lcom/google/android/finsky/config/G;->nlpCleanupUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 570
    .restart local v4    # "url":Ljava/lang/String;
    sget-object v3, Lcom/google/android/finsky/config/G;->nlpCleanupCookieName:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 571
    .restart local v7    # "cookieName":Ljava/lang/String;
    sget-object v3, Lcom/google/android/finsky/config/G;->nlpCleanupCookieValue:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .restart local v8    # "cookieValue":Ljava/lang/String;
    goto :goto_0
.end method

.method private static getListener(Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;)Lcom/google/android/finsky/download/DownloadQueueListener;
    .locals 1
    .param p0, "eventLogger"    # Lcom/google/android/finsky/analytics/FinskyEventLog;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 599
    new-instance v0, Lcom/google/android/finsky/appstate/GmsCoreHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/appstate/GmsCoreHelper$1;-><init>(Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;)V

    return-object v0
.end method

.method public static insertGmsCore(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p0, "docIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isAutoUpdateEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    const-string v0, "com.google.android.gms"

    invoke-interface {p0, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    const-string v0, "com.google.android.gms"

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    const-string v0, "GMS Core auto updating is disabled"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private installGmsCore(Lcom/google/android/finsky/api/model/Document;)V
    .locals 12
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 176
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v7

    .line 178
    .local v7, "installer":Lcom/google/android/finsky/receivers/Installer;
    const-string v1, "com.google.android.gms"

    invoke-interface {v7, v1}, Lcom/google/android/finsky/receivers/Installer;->setMobileDataAllowed(Ljava/lang/String;)V

    .line 180
    const-string v1, "com.google.android.gms"

    invoke-interface {v7, v1, v6, v6, v6}, Lcom/google/android/finsky/receivers/Installer;->setVisibility(Ljava/lang/String;ZZZ)V

    .line 183
    const-string v1, "com.google.android.gms"

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    iget-object v11, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {v1, v4, v5, v11}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->getAppDetailsAccount(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)Ljava/lang/String;

    move-result-object v10

    .line 185
    .local v10, "updateAccountName":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mContext:Landroid/content/Context;

    invoke-static {v10, v1}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 186
    .local v0, "updateAccount":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cannot update com.google.android.gms because cannot determine update account "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v10}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 207
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v9

    .line 195
    .local v9, "updateAccountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    if-nez v9, :cond_1

    .line 196
    const-string v1, "Cannot update com.google.android.gms because cannot find library for %s."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v10}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 200
    :cond_1
    invoke-static {p1, v9}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v8

    .line 201
    .local v8, "isOwnedByUpdateAccount":Z
    if-nez v8, :cond_2

    move-object v1, p1

    move-object v4, v3

    move v5, v2

    .line 202
    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PurchaseInitiator;->makeFreePurchase(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;ZZ)V

    goto :goto_0

    .line 205
    :cond_2
    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/PurchaseInitiator;->initiateDownload(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V

    goto :goto_0
.end method

.method private static installNlpCleanup(Lcom/google/android/finsky/download/Download;Ljava/lang/String;)V
    .locals 9
    .param p0, "download"    # Lcom/google/android/finsky/download/Download;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 658
    new-instance v8, Lcom/google/android/finsky/appstate/GmsCoreHelper$2;

    invoke-direct {v8, p1}, Lcom/google/android/finsky/appstate/GmsCoreHelper$2;-><init>(Ljava/lang/String;)V

    .line 682
    .local v8, "listener":Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;
    sget-object v0, Lcom/google/android/finsky/config/G;->nlpCleanupDowngradeFlag:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 685
    .local v7, "allowDowngrade":Z
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x6a

    const/4 v4, 0x0

    sget-object v6, Lcom/google/android/finsky/appstate/GmsCoreHelper;->sNlpLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object v2, p1

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 688
    invoke-interface {p0}, Lcom/google/android/finsky/download/Download;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v8, v1, p1, v7}, Lcom/google/android/finsky/utils/PackageManagerHelper;->installPackageWithDowngrade(Landroid/net/Uri;Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;ZLjava/lang/String;Z)V

    .line 690
    return-void
.end method

.method private static isAutoUpdateEnabled()Z
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/finsky/config/G;->gmsCoreAutoUpdateEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private static isCommonNlpRepairStatus(I)Z
    .locals 2
    .param p0, "status"    # I

    .prologue
    const/4 v0, 0x1

    .line 421
    if-eq p0, v0, :cond_0

    const/4 v1, 0x4

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isGmsCore(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 2
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    .line 99
    .local v0, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    if-nez v0, :cond_0

    .line 100
    const/4 v1, 0x0

    .line 103
    :goto_0
    return v1

    :cond_0
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isGmsCore(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isGmsCore(Ljava/lang/String;)Z
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    .line 107
    const-string v0, "com.google.android.gms"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static onBootCompleted()V
    .locals 1

    .prologue
    .line 433
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->nlpCleanupHoldoffUntilBoot:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 434
    return-void
.end method

.method public static removeGmsCore(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "docIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v0, "com.google.android.gms"

    invoke-interface {p0, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 95
    return-void
.end method


# virtual methods
.method public updateGmsCore(Lcom/google/android/finsky/api/model/Document;)V
    .locals 8
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x0

    .line 115
    invoke-static {}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isAutoUpdateEnabled()Z

    move-result v6

    if-nez v6, :cond_1

    .line 116
    const-string v6, "GMS Core auto update is disabled"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/appstate/GmsCoreHelper;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {p1, v6, v7}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 121
    const-string v6, "GMS Core is not available"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 125
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    .line 126
    .local v0, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    if-nez v0, :cond_3

    .line 127
    const-string v6, "Unable to install something without app details"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 131
    :cond_3
    const/4 v1, -0x1

    .line 132
    .local v1, "installedVersion":I
    const/4 v4, 0x0

    .line 133
    .local v4, "systemAppWithoutUpdate":Z
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v6

    const-string v7, "com.google.android.gms"

    invoke-interface {v6, v7}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v3

    .line 135
    .local v3, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-eqz v3, :cond_5

    .line 136
    iget-boolean v6, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabledByUser:Z

    if-eqz v6, :cond_4

    .line 137
    const-string v6, "Not updating com.google.android.gms (disabled)"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 140
    :cond_4
    iget v1, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 141
    iget-boolean v6, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    if-eqz v6, :cond_7

    iget-boolean v6, v3, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isUpdatedSystemApp:Z

    if-nez v6, :cond_7

    const/4 v4, 0x1

    .line 161
    :cond_5
    :goto_1
    const/4 v6, -0x1

    if-eq v1, v6, :cond_6

    if-eqz v4, :cond_8

    .line 162
    :cond_6
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v6

    const-string v7, "com.google.android.gms"

    invoke-interface {v6, v7}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v2

    .line 163
    .local v2, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAutoUpdate()Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    move-result-object v6

    sget-object v7, Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;->DISABLED:Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    if-ne v6, v7, :cond_8

    .line 165
    const-string v6, "Not updating com.google.android.gms (was removed)"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v6, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .end local v2    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_7
    move v4, v5

    .line 141
    goto :goto_1

    .line 170
    :cond_8
    iget v5, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    if-ge v1, v5, :cond_0

    .line 171
    invoke-direct {p0, p1}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->installGmsCore(Lcom/google/android/finsky/api/model/Document;)V

    goto :goto_0
.end method
