.class public Lcom/google/android/finsky/utils/AppSupport;
.super Ljava/lang/Object;
.source "AppSupport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/AppSupport$RefundListener;
    }
.end annotation


# direct methods
.method public static showRefundFailureDialog(Landroid/support/v4/app/FragmentManager;)V
    .locals 4
    .param p0, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 113
    new-instance v1, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 114
    .local v1, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v2, 0x7f0c02a8

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c02a0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 115
    invoke-virtual {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    .line 116
    .local v0, "alert":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v2, "refund_failure"

    invoke-virtual {v0, p0, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public static showUninstallConfirmationDialog(Ljava/lang/String;Landroid/support/v4/app/Fragment;ZZZ)V
    .locals 8
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "target"    # Landroid/support/v4/app/Fragment;
    .param p2, "isSystemPackage"    # Z
    .param p3, "isOwned"    # Z
    .param p4, "hasSubscriptions"    # Z

    .prologue
    .line 79
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 80
    .local v3, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v7, "uninstall_confirm"

    invoke-virtual {v3, v7}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 109
    :goto_0
    return-void

    .line 85
    :cond_0
    const v6, 0x7f0c02a0

    .line 86
    .local v6, "positiveId":I
    const v5, 0x7f0c0134

    .line 87
    .local v5, "negativeId":I
    if-eqz p3, :cond_3

    .line 88
    if-eqz p2, :cond_1

    .line 89
    const v4, 0x7f0c01ea

    .line 101
    .local v4, "messageId":I
    :goto_1
    new-instance v1, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 102
    .local v1, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v1, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 103
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 104
    .local v2, "extraArgs":Landroid/os/Bundle;
    const-string v7, "package_name"

    invoke-virtual {v2, v7, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v7, 0x1

    invoke-virtual {v1, p1, v7, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 107
    invoke-virtual {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v0

    .line 108
    .local v0, "alert":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v7, "uninstall_confirm"

    invoke-virtual {v0, v3, v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 90
    .end local v0    # "alert":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .end local v1    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v2    # "extraArgs":Landroid/os/Bundle;
    .end local v4    # "messageId":I
    :cond_1
    if-eqz p4, :cond_2

    .line 91
    const v4, 0x7f0c01ec

    .line 92
    .restart local v4    # "messageId":I
    const v6, 0x7f0c01b1

    .line 93
    const v5, 0x7f0c01b2

    goto :goto_1

    .line 95
    .end local v4    # "messageId":I
    :cond_2
    const v4, 0x7f0c01eb

    .restart local v4    # "messageId":I
    goto :goto_1

    .line 98
    .end local v4    # "messageId":I
    :cond_3
    const v4, 0x7f0c01ed

    .restart local v4    # "messageId":I
    goto :goto_1
.end method

.method public static silentRefund(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/finsky/utils/AppSupport$RefundListener;)V
    .locals 6
    .param p0, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "tryUninstall"    # Z
    .param p4, "listener"    # Lcom/google/android/finsky/utils/AppSupport$RefundListener;

    .prologue
    .line 42
    invoke-interface {p4}, Lcom/google/android/finsky/utils/AppSupport$RefundListener;->onRefundStart()V

    .line 43
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    .line 44
    .local v0, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/finsky/library/RevokeListenerWrapper;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    new-instance v5, Lcom/google/android/finsky/utils/AppSupport$1;

    invoke-direct {v5, p3, p1, p4}, Lcom/google/android/finsky/utils/AppSupport$1;-><init>(ZLjava/lang/String;Lcom/google/android/finsky/utils/AppSupport$RefundListener;)V

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/finsky/library/RevokeListenerWrapper;-><init>(Lcom/google/android/finsky/library/LibraryReplicators;Landroid/accounts/Account;Lcom/android/volley/Response$Listener;)V

    new-instance v3, Lcom/google/android/finsky/utils/AppSupport$2;

    invoke-direct {v3, p0, p4}, Lcom/google/android/finsky/utils/AppSupport$2;-><init>(Landroid/support/v4/app/FragmentManager;Lcom/google/android/finsky/utils/AppSupport$RefundListener;)V

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/google/android/finsky/api/DfeApi;->revoke(Ljava/lang/String;ILcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 69
    return-void
.end method
