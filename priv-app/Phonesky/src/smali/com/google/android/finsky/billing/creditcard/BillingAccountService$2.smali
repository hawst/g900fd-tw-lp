.class Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;
.super Ljava/lang/Object;
.source "BillingAccountService.java"

# interfaces
.implements Lcom/google/android/finsky/utils/GetTocHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->fetchToc(Lcom/google/android/finsky/api/DfeApi;I)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

.field final synthetic val$resultCodeWrapper:[I

.field final synthetic val$semaphore:Ljava/util/concurrent/Semaphore;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Ljava/util/concurrent/Semaphore;[I)V
    .locals 0

    .prologue
    .line 220
    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;->this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

    iput-object p2, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;->val$semaphore:Ljava/util/concurrent/Semaphore;

    iput-object p3, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;->val$resultCodeWrapper:[I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v2, 0x0

    .line 230
    const-string v0, "Error while loading toc: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 231
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;->val$resultCodeWrapper:[I

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;->this$0:Lcom/google/android/finsky/billing/creditcard/BillingAccountService;

    # invokes: Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->convertErrorCode(Ljava/lang/Throwable;)I
    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->access$200(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Ljava/lang/Throwable;)I

    move-result v1

    aput v1, v0, v2

    .line 232
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 233
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/Toc$TocResponse;)V
    .locals 1
    .param p1, "response"    # Lcom/google/android/finsky/protos/Toc$TocResponse;

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 226
    return-void
.end method
