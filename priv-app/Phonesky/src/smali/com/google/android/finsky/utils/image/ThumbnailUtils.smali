.class public Lcom/google/android/finsky/utils/image/ThumbnailUtils;
.super Ljava/lang/Object;
.source "ThumbnailUtils.java"


# direct methods
.method public static getBestImage(Ljava/util/List;II)Lcom/google/android/finsky/protos/Common$Image;
    .locals 10
    .param p1, "minWidth"    # I
    .param p2, "minHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Common$Image;",
            ">;II)",
            "Lcom/google/android/finsky/protos/Common$Image;"
        }
    .end annotation

    .prologue
    .local p0, "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    const/4 v8, 0x0

    .line 47
    if-nez p0, :cond_1

    move-object v1, v8

    .line 82
    :cond_0
    :goto_0
    return-object v1

    .line 51
    :cond_1
    const v7, 0x7fffffff

    .line 52
    .local v7, "selectedWidth":I
    const v5, 0x7fffffff

    .line 53
    .local v5, "selectedHeight":I
    const/4 v6, 0x0

    .line 54
    .local v6, "selectedImage":Lcom/google/android/finsky/protos/Common$Image;
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 55
    .local v2, "imageCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_3

    .line 56
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Common$Image;

    .line 57
    .local v1, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-boolean v9, v1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-nez v9, :cond_0

    .line 59
    iget-object v9, v1, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    if-eqz v9, :cond_2

    .line 60
    iget-object v9, v1, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    iget v4, v9, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    .line 61
    .local v4, "imgWidth":I
    iget-object v9, v1, Lcom/google/android/finsky/protos/Common$Image;->dimension:Lcom/google/android/finsky/protos/Common$Image$Dimension;

    iget v3, v9, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    .line 64
    .local v3, "imgHeight":I
    if-lt v4, p1, :cond_2

    if-lt v3, p2, :cond_2

    if-lt v7, v4, :cond_2

    if-lt v5, v3, :cond_2

    .line 66
    move v7, v4

    .line 67
    move v5, v3

    .line 68
    move-object v6, v1

    .line 55
    .end local v3    # "imgHeight":I
    .end local v4    # "imgWidth":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 74
    .end local v1    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :cond_3
    if-eqz v6, :cond_4

    move-object v1, v6

    .line 75
    goto :goto_0

    .line 79
    :cond_4
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_5

    .line 80
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/protos/Common$Image;

    move-object v1, v8

    goto :goto_0

    :cond_5
    move-object v1, v8

    .line 82
    goto :goto_0
.end method

.method public static getImageFromDocument(Lcom/google/android/finsky/api/model/Document;II[I)Lcom/google/android/finsky/protos/Common$Image;
    .locals 4
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "imageTypes"    # [I

    .prologue
    .line 20
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p3

    if-ge v0, v3, :cond_1

    .line 21
    aget v1, p3, v0

    .line 22
    .local v1, "imageType":I
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v3

    invoke-static {v3, p1, p2}, Lcom/google/android/finsky/utils/image/ThumbnailUtils;->getBestImage(Ljava/util/List;II)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v2

    .line 23
    .local v2, "result":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v2, :cond_0

    .line 27
    .end local v1    # "imageType":I
    .end local v2    # "result":Lcom/google/android/finsky/protos/Common$Image;
    :goto_1
    return-object v2

    .line 20
    .restart local v1    # "imageType":I
    .restart local v2    # "result":Lcom/google/android/finsky/protos/Common$Image;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 27
    .end local v1    # "imageType":I
    .end local v2    # "result":Lcom/google/android/finsky/protos/Common$Image;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static getPromoBitmapFromDocument(Lcom/google/android/finsky/api/model/Document;II)Lcom/google/android/finsky/protos/Common$Image;
    .locals 1
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 40
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/finsky/utils/image/ThumbnailUtils;->getBestImage(Ljava/util/List;II)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    return-object v0
.end method
