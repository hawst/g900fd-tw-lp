.class public final Lcom/google/android/finsky/protos/DocList$ListResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ListResponse"
.end annotation


# instance fields
.field public bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

.field public doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocList$ListResponse;->clear()Lcom/google/android/finsky/protos/DocList$ListResponse;

    .line 33
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocList$ListResponse;
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/android/finsky/protos/DocList$Bucket;->emptyArray()[Lcom/google/android/finsky/protos/DocList$Bucket;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    .line 37
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->cachedSize:I

    .line 39
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 66
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 67
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 68
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 69
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    aget-object v0, v3, v1

    .line 70
    .local v0, "element":Lcom/google/android/finsky/protos/DocList$Bucket;
    if-eqz v0, :cond_0

    .line 71
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 68
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 76
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocList$Bucket;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 77
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 78
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 79
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_2

    .line 80
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 77
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 85
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocList$ListResponse;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 93
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 94
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 98
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 99
    :sswitch_0
    return-object p0

    .line 104
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 106
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    if-nez v5, :cond_2

    move v1, v4

    .line 107
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocList$Bucket;

    .line 109
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocList$Bucket;
    if-eqz v1, :cond_1

    .line 110
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 112
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 113
    new-instance v5, Lcom/google/android/finsky/protos/DocList$Bucket;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocList$Bucket;-><init>()V

    aput-object v5, v2, v1

    .line 114
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 115
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 112
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 106
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocList$Bucket;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v1, v5

    goto :goto_1

    .line 118
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocList$Bucket;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocList$Bucket;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocList$Bucket;-><init>()V

    aput-object v5, v2, v1

    .line 119
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 120
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    goto :goto_0

    .line 124
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocList$Bucket;
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 126
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_5

    move v1, v4

    .line 127
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 129
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_4

    .line 130
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 132
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 133
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 134
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 135
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 132
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 126
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_3

    .line 138
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 139
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 140
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 94
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocList$ListResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocList$ListResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 46
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 47
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->bucket:[Lcom/google/android/finsky/protos/DocList$Bucket;

    aget-object v0, v2, v1

    .line 48
    .local v0, "element":Lcom/google/android/finsky/protos/DocList$Bucket;
    if-eqz v0, :cond_0

    .line 49
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 46
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocList$Bucket;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 54
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 55
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 56
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_2

    .line 57
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 54
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 61
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 62
    return-void
.end method
