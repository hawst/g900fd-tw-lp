.class public Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "SuccessStep.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private mBillingUiMode:I

.field private mMainView:Landroid/view/View;

.field private mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 35
    const/16 v0, 0x375

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method public static newInstance(Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;I)Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;
    .locals 4
    .param p0, "redemptionSuccess"    # Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    .param p1, "mode"    # I

    .prologue
    .line 47
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 48
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "SuccessStep.redemption_success"

    invoke-static {p0}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 49
    const-string v2, "ui_mode"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 50
    new-instance v1, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;-><init>()V

    .line 52
    .local v1, "result":Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;
    iput-object p0, v1, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    .line 53
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v1
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->buttonLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onContinueButtonClicked()V
    .locals 1

    .prologue
    .line 143
    const/16 v0, 0x376

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->logClick(I)V

    .line 144
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->performSuccessActionAndFinish()V

    .line 145
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SuccessStep.redemption_success"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    .line 62
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ui_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mBillingUiMode:I

    .line 63
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/16 v10, 0x8

    .line 70
    const/4 v0, 0x0

    .line 72
    .local v0, "hasLayoutWithTitle":Z
    iget v9, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mBillingUiMode:I

    if-ne v9, v7, :cond_1

    .line 73
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const v9, 0x7f0c00a4

    invoke-virtual {p0, v9}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    invoke-virtual {v7}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->hideCancelButton()V

    .line 75
    const v3, 0x7f0401ab

    .line 81
    .local v3, "layoutId":I
    :goto_0
    invoke-virtual {p1, v3, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mMainView:Landroid/view/View;

    .line 83
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mMainView:Landroid/view/View;

    const v8, 0x7f0a00c3

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 84
    .local v4, "messageView":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v7, v7, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 85
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v7, v7, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 87
    invoke-virtual {v4}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 92
    :goto_1
    if-eqz v0, :cond_0

    .line 93
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mMainView:Landroid/view/View;

    const v8, 0x7f0a009c

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 94
    .local v6, "titleView":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v7, v7, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mMainView:Landroid/view/View;

    const v8, 0x7f0a0347

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 97
    .local v5, "titleBylineView":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v7, v7, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->titleBylineHtml:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 98
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v7, v7, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->titleBylineHtml:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 100
    invoke-virtual {v5}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 105
    :goto_2
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mMainView:Landroid/view/View;

    const v8, 0x7f0a0099

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/image/FifeImageView;

    .line 106
    .local v2, "imageView":Lcom/google/android/play/image/FifeImageView;
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v1, v7, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 107
    .local v1, "image":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v1, :cond_6

    .line 108
    iget-object v7, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v8, v1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v9

    invoke-virtual {v2, v7, v8, v9}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 114
    .end local v1    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v2    # "imageView":Lcom/google/android/play/image/FifeImageView;
    .end local v5    # "titleBylineView":Landroid/widget/TextView;
    .end local v6    # "titleView":Landroid/widget/TextView;
    :cond_0
    :goto_3
    iget-object v7, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mMainView:Landroid/view/View;

    return-object v7

    .line 77
    .end local v3    # "layoutId":I
    .end local v4    # "messageView":Landroid/widget/TextView;
    :cond_1
    iget-object v9, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v9, v9, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    move v0, v7

    .line 78
    :goto_4
    if-eqz v0, :cond_3

    const v3, 0x7f040186

    .restart local v3    # "layoutId":I
    :goto_5
    goto/16 :goto_0

    .end local v3    # "layoutId":I
    :cond_2
    move v0, v8

    .line 77
    goto :goto_4

    .line 78
    :cond_3
    const v3, 0x7f040185

    goto :goto_5

    .line 89
    .restart local v3    # "layoutId":I
    .restart local v4    # "messageView":Landroid/widget/TextView;
    :cond_4
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 102
    .restart local v5    # "titleBylineView":Landroid/widget/TextView;
    .restart local v6    # "titleView":Landroid/widget/TextView;
    :cond_5
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 111
    .restart local v1    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .restart local v2    # "imageView":Lcom/google/android/play/image/FifeImageView;
    :cond_6
    invoke-virtual {v2, v10}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_3
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 119
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onResume()V

    .line 124
    const/4 v0, 0x0

    .line 125
    .local v0, "textToAnnounce":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v1, v1, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 126
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v1, v1, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 131
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mMainView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mMainView:Landroid/view/View;

    invoke-static {v1, v0, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 134
    :cond_1
    return-void

    .line 127
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v1, v1, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/steps/SuccessStep;->mRedemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v0, v1, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    goto :goto_0
.end method
