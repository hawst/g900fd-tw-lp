.class public final Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CvnChallenge"
.end annotation


# instance fields
.field public creditCardType:I

.field public descriptionHtml:Ljava/lang/String;

.field public escrowHandleParam:Ljava/lang/String;

.field public hasCreditCardType:Z

.field public hasDescriptionHtml:Z

.field public hasEscrowHandleParam:Z

.field public hasTitle:Z

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2010
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2011
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->clear()Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    .line 2012
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2015
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->title:Ljava/lang/String;

    .line 2016
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasTitle:Z

    .line 2017
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->descriptionHtml:Ljava/lang/String;

    .line 2018
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasDescriptionHtml:Z

    .line 2019
    iput v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->creditCardType:I

    .line 2020
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasCreditCardType:Z

    .line 2021
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->escrowHandleParam:Ljava/lang/String;

    .line 2022
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasEscrowHandleParam:Z

    .line 2023
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->cachedSize:I

    .line 2024
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2047
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2048
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2049
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2052
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasDescriptionHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->descriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2053
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2056
    :cond_3
    iget v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->creditCardType:I

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasCreditCardType:Z

    if-eqz v1, :cond_5

    .line 2057
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->creditCardType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2060
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasEscrowHandleParam:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->escrowHandleParam:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2061
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->escrowHandleParam:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2064
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2072
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2073
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2077
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2078
    :sswitch_0
    return-object p0

    .line 2083
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->title:Ljava/lang/String;

    .line 2084
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasTitle:Z

    goto :goto_0

    .line 2088
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->descriptionHtml:Ljava/lang/String;

    .line 2089
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasDescriptionHtml:Z

    goto :goto_0

    .line 2093
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2094
    .local v1, "value":I
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    .line 2105
    :sswitch_4
    iput v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->creditCardType:I

    .line 2106
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasCreditCardType:Z

    goto :goto_0

    .line 2112
    .end local v1    # "value":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->escrowHandleParam:Ljava/lang/String;

    .line 2113
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasEscrowHandleParam:Z

    goto :goto_0

    .line 2073
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_5
    .end sparse-switch

    .line 2094
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x3 -> :sswitch_4
        0x4 -> :sswitch_4
        0x5 -> :sswitch_4
        0x6 -> :sswitch_4
        0x7 -> :sswitch_4
        0xa -> :sswitch_4
        0x1b -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1977
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2030
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2031
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2033
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasDescriptionHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->descriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2034
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2036
    :cond_3
    iget v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->creditCardType:I

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasCreditCardType:Z

    if-eqz v0, :cond_5

    .line 2037
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->creditCardType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2039
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->hasEscrowHandleParam:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->escrowHandleParam:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2040
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->escrowHandleParam:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2042
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2043
    return-void
.end method
