.class Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;
.super Ljava/lang/Object;
.source "AddCreditCardSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EscrowResponseListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$1;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 87
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;->onResponse(Ljava/lang/String;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;)V
    .locals 2
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    # getter for: Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    invoke-static {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->access$000(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    iput-object p1, v0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowHandle:Ljava/lang/String;

    .line 92
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    # getter for: Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    invoke-static {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->access$000(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->creditCard:Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasEscrowHandle:Z

    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    # invokes: Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->performRequest()V
    invoke-static {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->access$100(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;)V

    .line 94
    return-void
.end method
