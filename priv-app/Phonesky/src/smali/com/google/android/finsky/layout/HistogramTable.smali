.class public Lcom/google/android/finsky/layout/HistogramTable;
.super Landroid/widget/TableLayout;
.source "HistogramTable.java"


# instance fields
.field private final mBarHeight:I

.field private final mBarMargin:I

.field private final mLabelsOn:Z

.field private final mMaxBarWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/HistogramTable;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/TableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    sget-object v1, Lcom/android/vending/R$styleable;->HistogramTable:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 38
    .local v0, "attrArray":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/HistogramTable;->mLabelsOn:Z

    .line 39
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/HistogramTable;->mBarHeight:I

    .line 40
    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/HistogramTable;->mMaxBarWidth:I

    .line 42
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/HistogramTable;->mBarMargin:I

    .line 43
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 44
    return-void
.end method


# virtual methods
.method public getBaseline()I
    .locals 5

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HistogramTable;->getMeasuredHeight()I

    move-result v2

    .line 124
    .local v2, "measuredHeight":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HistogramTable;->getChildCount()I

    move-result v1

    .line 125
    .local v1, "childCount":I
    if-nez v1, :cond_0

    .line 129
    .end local v2    # "measuredHeight":I
    :goto_0
    return v2

    .line 128
    .restart local v2    # "measuredHeight":I
    :cond_0
    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/HistogramTable;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 129
    .local v0, "bottomChild":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v2, v3

    invoke-virtual {v0}, Landroid/view/View;->getBaseline()I

    move-result v4

    add-int v2, v3, v4

    goto :goto_0
.end method

.method public setHistogram([I)V
    .locals 21
    .param p1, "histogram"    # [I

    .prologue
    .line 47
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/HistogramTable;->removeAllViews()V

    .line 49
    const-wide/16 v12, 0x0

    .line 50
    .local v12, "max":D
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    const/16 v17, 0x5

    move/from16 v0, v17

    if-ge v9, v0, :cond_1

    .line 51
    aget v17, p1, v9

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v18, v0

    cmpl-double v17, v18, v12

    if-lez v17, :cond_0

    .line 52
    aget v17, p1, v9

    move/from16 v0, v17

    int-to-double v12, v0

    .line 50
    :cond_0
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 55
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/HistogramTable;->getContext()Landroid/content/Context;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    .line 56
    .local v10, "inflater":Landroid/view/LayoutInflater;
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v8

    .line 57
    .local v8, "formatter":Ljava/text/NumberFormat;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/HistogramTable;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 58
    .local v14, "res":Landroid/content/res/Resources;
    const/4 v9, 0x0

    :goto_1
    const/16 v17, 0x5

    move/from16 v0, v17

    if-ge v9, v0, :cond_5

    .line 59
    const v17, 0x7f0400b1

    const/16 v18, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    move/from16 v2, v18

    invoke-virtual {v10, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TableRow;

    .line 60
    .local v16, "tableRow":Landroid/widget/TableRow;
    const v17, 0x7f0a021d

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Lcom/google/android/finsky/layout/StarLabel;

    .line 61
    .local v15, "starLabel":Lcom/google/android/finsky/layout/StarLabel;
    const v17, 0x7f0a021f

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 62
    .local v7, "countLabel":Landroid/widget/TextView;
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/finsky/layout/StarLabel;->setMaxStars(I)V

    .line 63
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HistogramTable;->mBarHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/finsky/layout/StarLabel;->setStarHeight(I)V

    .line 64
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/HistogramTable;->mLabelsOn:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    .line 65
    rsub-int/lit8 v17, v9, 0x5

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/finsky/layout/StarLabel;->setNumStars(I)V

    .line 66
    aget v17, p1, v9

    move/from16 v0, v17

    int-to-long v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v8, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    const/16 v17, 0x2

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TableRow;->setBaselineAlignedChildIndex(I)V

    .line 73
    :goto_2
    const v17, 0x7f0a021e

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TableRow;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/HistogramBar;

    .line 74
    .local v4, "bar":Lcom/google/android/finsky/layout/HistogramBar;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HistogramTable;->mMaxBarWidth:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Lcom/google/android/finsky/layout/HistogramBar;->setMaxBarWidth(D)V

    .line 75
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HistogramTable;->mBarHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/layout/HistogramBar;->setBarHeight(I)V

    .line 76
    aget v17, p1, v9

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v18, v18, v12

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Lcom/google/android/finsky/layout/HistogramBar;->setWidthPercentage(D)V

    .line 77
    new-instance v11, Landroid/widget/TableLayout$LayoutParams;

    const/16 v17, -0x2

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v11, v0, v1}, Landroid/widget/TableLayout$LayoutParams;-><init>(II)V

    .line 79
    .local v11, "params":Landroid/widget/TableLayout$LayoutParams;
    if-eqz v9, :cond_2

    .line 80
    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/HistogramTable;->mBarMargin:I

    move/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/TableLayout$LayoutParams;->setMargins(IIII)V

    .line 84
    :cond_2
    packed-switch v9, :pswitch_data_0

    .line 106
    const v5, 0x7f0900ab

    .line 107
    .local v5, "colorId":I
    const v6, 0x7f0c023a

    .line 109
    .local v6, "contentDescriptionId":I
    :goto_3
    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/HistogramBar;->setColor(I)V

    .line 110
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/HistogramTable;->mLabelsOn:Z

    move/from16 v17, v0

    if-eqz v17, :cond_4

    invoke-virtual {v14, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    :goto_4
    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/layout/HistogramBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 111
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v11}, Lcom/google/android/finsky/layout/HistogramTable;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 58
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 69
    .end local v4    # "bar":Lcom/google/android/finsky/layout/HistogramBar;
    .end local v5    # "colorId":I
    .end local v6    # "contentDescriptionId":I
    .end local v11    # "params":Landroid/widget/TableLayout$LayoutParams;
    :cond_3
    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v15, v0}, Lcom/google/android/finsky/layout/StarLabel;->setVisibility(I)V

    .line 70
    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    const/16 v17, 0x1

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TableRow;->setBaselineAlignedChildIndex(I)V

    goto/16 :goto_2

    .line 86
    .restart local v4    # "bar":Lcom/google/android/finsky/layout/HistogramBar;
    .restart local v11    # "params":Landroid/widget/TableLayout$LayoutParams;
    :pswitch_0
    const v5, 0x7f0900a7

    .line 87
    .restart local v5    # "colorId":I
    const v6, 0x7f0c0236

    .line 88
    .restart local v6    # "contentDescriptionId":I
    goto :goto_3

    .line 90
    .end local v5    # "colorId":I
    .end local v6    # "contentDescriptionId":I
    :pswitch_1
    const v5, 0x7f0900a8

    .line 91
    .restart local v5    # "colorId":I
    const v6, 0x7f0c0237

    .line 92
    .restart local v6    # "contentDescriptionId":I
    goto :goto_3

    .line 94
    .end local v5    # "colorId":I
    .end local v6    # "contentDescriptionId":I
    :pswitch_2
    const v5, 0x7f0900a9

    .line 95
    .restart local v5    # "colorId":I
    const v6, 0x7f0c0238

    .line 96
    .restart local v6    # "contentDescriptionId":I
    goto :goto_3

    .line 98
    .end local v5    # "colorId":I
    .end local v6    # "contentDescriptionId":I
    :pswitch_3
    const v5, 0x7f0900aa

    .line 99
    .restart local v5    # "colorId":I
    const v6, 0x7f0c0239

    .line 100
    .restart local v6    # "contentDescriptionId":I
    goto :goto_3

    .line 102
    .end local v5    # "colorId":I
    .end local v6    # "contentDescriptionId":I
    :pswitch_4
    const v5, 0x7f0900ab

    .line 103
    .restart local v5    # "colorId":I
    const v6, 0x7f0c023a

    .line 104
    .restart local v6    # "contentDescriptionId":I
    goto :goto_3

    .line 110
    :cond_4
    const/16 v17, 0x0

    goto :goto_4

    .line 113
    .end local v4    # "bar":Lcom/google/android/finsky/layout/HistogramBar;
    .end local v5    # "colorId":I
    .end local v6    # "contentDescriptionId":I
    .end local v7    # "countLabel":Landroid/widget/TextView;
    .end local v11    # "params":Landroid/widget/TableLayout$LayoutParams;
    .end local v15    # "starLabel":Lcom/google/android/finsky/layout/StarLabel;
    .end local v16    # "tableRow":Landroid/widget/TableRow;
    :cond_5
    return-void

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
