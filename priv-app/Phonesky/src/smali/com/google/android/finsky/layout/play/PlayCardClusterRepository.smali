.class public Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;
.super Ljava/lang/Object;
.source "PlayCardClusterRepository.java"


# static fields
.field private static final sClusters16x9:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sClusters1x1:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 17

    .prologue
    .line 47
    new-instance v12, Landroid/util/SparseArray;

    invoke-direct {v12}, Landroid/util/SparseArray;-><init>()V

    sput-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters1x1:Landroid/util/SparseArray;

    .line 50
    new-instance v12, Landroid/util/SparseArray;

    invoke-direct {v12}, Landroid/util/SparseArray;-><init>()V

    sput-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters16x9:Landroid/util/SparseArray;

    .line 62
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v11

    .line 63
    .local v11, "clustersTwoColumns1x1":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 70
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 77
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x2

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 82
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x2

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 87
    const/4 v12, 0x4

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v11, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 95
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters1x1:Landroid/util/SparseArray;

    const/4 v13, 0x0

    invoke-virtual {v12, v13, v11}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 97
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 98
    .local v7, "clustersThreeColumns1x1":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v7, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 106
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v7, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 112
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v7, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 118
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v7, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 124
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v7, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 130
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters1x1:Landroid/util/SparseArray;

    const/4 v13, 0x1

    invoke-virtual {v12, v13, v7}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 132
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 133
    .local v9, "clustersThreeTallColumns1x1":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v9, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 141
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v9, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 147
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x6

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x3

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x3

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x3

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v9, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 156
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x6

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x3

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v9, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 163
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x6

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x3

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v9, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 170
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters1x1:Landroid/util/SparseArray;

    const/4 v13, 0x2

    invoke-virtual {v12, v13, v9}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 172
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 173
    .local v3, "clustersFourColumns1x1":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v3, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 182
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v3, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 189
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v3, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 196
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v3, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 203
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v3, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 210
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters1x1:Landroid/util/SparseArray;

    const/4 v13, 0x3

    invoke-virtual {v12, v13, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 212
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 213
    .local v5, "clustersFourTallColumns1x1":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v5, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 222
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v5, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 229
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x6

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x2

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v5, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 237
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x6

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x3

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x3

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v5, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 246
    const/4 v12, 0x4

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/16 v15, 0x8

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x6

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x6

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v5, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 254
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters1x1:Landroid/util/SparseArray;

    const/4 v13, 0x4

    invoke-virtual {v12, v13, v5}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 256
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 257
    .local v1, "clustersFiveColumns1x1":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/16 v15, 0x8

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v1, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 267
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/16 v15, 0x8

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v1, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 275
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/16 v15, 0x8

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v1, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 283
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/16 v15, 0x8

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v1, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 291
    const/4 v12, 0x4

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x3

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/16 v15, 0x8

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v1, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 298
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters1x1:Landroid/util/SparseArray;

    const/4 v13, 0x5

    invoke-virtual {v12, v13, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 300
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v10

    .line 301
    .local v10, "clustersTwoColumns16x9":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v10, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 308
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v10, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 315
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x2

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v10, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 320
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x2

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v10, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 325
    const/4 v12, 0x4

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x4

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v10, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 332
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters16x9:Landroid/util/SparseArray;

    const/4 v13, 0x0

    invoke-virtual {v12, v13, v10}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 334
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 335
    .local v6, "clustersThreeColumns16x9":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v6, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 343
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v6, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 349
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v6, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 355
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v6, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 361
    const/4 v12, 0x4

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v6, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 367
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters16x9:Landroid/util/SparseArray;

    const/4 v13, 0x1

    invoke-virtual {v12, v13, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 369
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 370
    .local v8, "clustersThreeTallColumns16x9":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v8, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 378
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/16 v15, 0x8

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v8, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 386
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/16 v15, 0x8

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v8, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 394
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/16 v15, 0x8

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v8, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 402
    const/4 v12, 0x4

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/4 v14, 0x6

    const/16 v15, 0x8

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v8, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 410
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters16x9:Landroid/util/SparseArray;

    const/4 v13, 0x2

    invoke-virtual {v12, v13, v8}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 412
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 413
    .local v2, "clustersFourColumns16x9":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v2, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 422
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v2, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 429
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v2, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 436
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v2, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 443
    const/4 v12, 0x4

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v2, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 450
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters16x9:Landroid/util/SparseArray;

    const/4 v13, 0x3

    invoke-virtual {v12, v13, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 452
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 453
    .local v4, "clustersFourTallColumns16x9":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v4, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 462
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUMPLUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v4, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 469
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/16 v15, 0x8

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x2

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x6

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v4, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 478
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/16 v15, 0x8

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v4, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 487
    const/4 v12, 0x4

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0x8

    const/16 v15, 0x8

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGE_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x2

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x4

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x6

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v4, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 496
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters16x9:Landroid/util/SparseArray;

    const/4 v13, 0x4

    invoke-virtual {v12, v13, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 498
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 499
    .local v0, "clustersFiveColumns16x9":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;"
    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x2

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x4

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/16 v15, 0x8

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildThumbnailAspectRatio()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setRespectChildHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 509
    const/4 v12, 0x1

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/16 v15, 0x8

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 516
    const/4 v12, 0x2

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x2

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 523
    const/4 v12, 0x3

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/16 v15, 0x8

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 530
    const/4 v12, 0x4

    new-instance v13, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    const/16 v14, 0xa

    const/4 v15, 0x4

    invoke-direct {v13, v14, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;-><init>(II)V

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_LARGEMINUS_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_MEDIUM_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    const/4 v15, 0x6

    const/16 v16, 0x2

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->addFlexiTile(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    sget-object v14, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->CARD_SMALL_16x9:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->setCardMetadataForMinCellHeight(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v13

    invoke-interface {v0, v12, v13}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 537
    sget-object v12, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters16x9:Landroid/util/SparseArray;

    const/4 v13, 0x5

    invoke-virtual {v12, v13, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 538
    return-void
.end method

.method public static getConfigurationKey(IZ)I
    .locals 4
    .param p0, "columns"    # I
    .param p1, "useTallTemplates"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 545
    packed-switch p0, :pswitch_data_0

    .line 555
    const-string v2, "Unsupported number of columns %d"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 556
    :cond_0
    :goto_0
    return v0

    :pswitch_0
    move v0, v1

    .line 547
    goto :goto_0

    .line 549
    :pswitch_1
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 551
    :pswitch_2
    if-eqz p1, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    :cond_1
    const/4 v0, 0x3

    goto :goto_0

    .line 553
    :pswitch_3
    const/4 v0, 0x5

    goto :goto_0

    .line 545
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getMetadata(IIZI)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 4
    .param p0, "documentType"    # I
    .param p1, "columns"    # I
    .param p2, "useTallTemplates"    # Z
    .param p3, "signalStrength"    # I

    .prologue
    .line 562
    invoke-static {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v0

    .line 563
    .local v0, "aspectRatio":F
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v0, v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    .line 564
    .local v2, "use1x1":Z
    :goto_0
    if-eqz v2, :cond_1

    sget-object v1, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters1x1:Landroid/util/SparseArray;

    .line 565
    .local v1, "source":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;>;"
    :goto_1
    invoke-static {p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->getConfigurationKey(IZ)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    return-object v3

    .line 563
    .end local v1    # "source":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;>;>;"
    .end local v2    # "use1x1":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 564
    .restart local v2    # "use1x1":Z
    :cond_1
    sget-object v1, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->sClusters16x9:Landroid/util/SparseArray;

    goto :goto_1
.end method
