.class public final Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Details.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BulkDetailsRequest"
.end annotation


# instance fields
.field public docid:[Ljava/lang/String;

.field public hasIncludeChildDocs:Z

.field public hasIncludeDetails:Z

.field public hasIncludeSplitDetailsForAllApps:Z

.field public hasIncludeSplitDetailsForNewerVersions:Z

.field public hasSourcePackageName:Z

.field public includeChildDocs:Z

.field public includeDetails:Z

.field public includeSplitDetailsForAllApps:Z

.field public includeSplitDetailsForNewerVersions:Z

.field public installedVersionCode:[I

.field public sourcePackageName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 270
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->clear()Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;

    .line 271
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 274
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    .line 275
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeChildDocs:Z

    .line 277
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeChildDocs:Z

    .line 278
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeDetails:Z

    .line 279
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeDetails:Z

    .line 280
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->sourcePackageName:Ljava/lang/String;

    .line 281
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasSourcePackageName:Z

    .line 282
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForAllApps:Z

    .line 283
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeSplitDetailsForAllApps:Z

    .line 284
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForNewerVersions:Z

    .line 285
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeSplitDetailsForNewerVersions:Z

    .line 286
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->cachedSize:I

    .line 287
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 326
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 327
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 328
    const/4 v0, 0x0

    .line 329
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 330
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 331
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 332
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 333
    add-int/lit8 v0, v0, 0x1

    .line 334
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 330
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 338
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 339
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 341
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeChildDocs:Z

    if-nez v5, :cond_3

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeChildDocs:Z

    const/4 v6, 0x1

    if-eq v5, v6, :cond_4

    .line 342
    :cond_3
    const/4 v5, 0x2

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeChildDocs:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 345
    :cond_4
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeDetails:Z

    if-nez v5, :cond_5

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeDetails:Z

    if-eqz v5, :cond_6

    .line 346
    :cond_5
    const/4 v5, 0x3

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeDetails:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 349
    :cond_6
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasSourcePackageName:Z

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->sourcePackageName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_8

    .line 350
    :cond_7
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->sourcePackageName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 353
    :cond_8
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeSplitDetailsForAllApps:Z

    if-nez v5, :cond_9

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForAllApps:Z

    if-eqz v5, :cond_a

    .line 354
    :cond_9
    const/4 v5, 0x5

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForAllApps:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 357
    :cond_a
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeSplitDetailsForNewerVersions:Z

    if-nez v5, :cond_b

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForNewerVersions:Z

    if-eqz v5, :cond_c

    .line 358
    :cond_b
    const/4 v5, 0x6

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForNewerVersions:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 361
    :cond_c
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    array-length v5, v5

    if-lez v5, :cond_e

    .line 362
    const/4 v1, 0x0

    .line 363
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    array-length v5, v5

    if-ge v3, v5, :cond_d

    .line 364
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    aget v2, v5, v3

    .line 365
    .local v2, "element":I
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v5

    add-int/2addr v1, v5

    .line 363
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 368
    .end local v2    # "element":I
    :cond_d
    add-int/2addr v4, v1

    .line 369
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    array-length v5, v5

    mul-int/lit8 v5, v5, 0x1

    add-int/2addr v4, v5

    .line 371
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_e
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;
    .locals 10
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 379
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 380
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 384
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 385
    :sswitch_0
    return-object p0

    .line 390
    :sswitch_1
    const/16 v8, 0xa

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 392
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    if-nez v8, :cond_2

    move v1, v7

    .line 393
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [Ljava/lang/String;

    .line 394
    .local v4, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 395
    iget-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 397
    :cond_1
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_3

    .line 398
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 399
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 397
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 392
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    array-length v1, v8

    goto :goto_1

    .line 402
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v1

    .line 403
    iput-object v4, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    goto :goto_0

    .line 407
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Ljava/lang/String;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeChildDocs:Z

    .line 408
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeChildDocs:Z

    goto :goto_0

    .line 412
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeDetails:Z

    .line 413
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeDetails:Z

    goto :goto_0

    .line 417
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->sourcePackageName:Ljava/lang/String;

    .line 418
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasSourcePackageName:Z

    goto :goto_0

    .line 422
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForAllApps:Z

    .line 423
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeSplitDetailsForAllApps:Z

    goto :goto_0

    .line 427
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForNewerVersions:Z

    .line 428
    iput-boolean v9, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeSplitDetailsForNewerVersions:Z

    goto :goto_0

    .line 432
    :sswitch_7
    const/16 v8, 0x38

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 434
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    if-nez v8, :cond_5

    move v1, v7

    .line 435
    .restart local v1    # "i":I
    :goto_3
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 436
    .local v4, "newArray":[I
    if-eqz v1, :cond_4

    .line 437
    iget-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 439
    :cond_4
    :goto_4
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_6

    .line 440
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 441
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 439
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 434
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_5
    iget-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    array-length v1, v8

    goto :goto_3

    .line 444
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 445
    iput-object v4, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    goto/16 :goto_0

    .line 449
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 450
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 452
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 453
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 454
    .local v5, "startPos":I
    :goto_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_7

    .line 455
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 456
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 458
    :cond_7
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 459
    iget-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    if-nez v8, :cond_9

    move v1, v7

    .line 460
    .restart local v1    # "i":I
    :goto_6
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 461
    .restart local v4    # "newArray":[I
    if-eqz v1, :cond_8

    .line 462
    iget-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 464
    :cond_8
    :goto_7
    array-length v8, v4

    if-ge v1, v8, :cond_a

    .line 465
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 464
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 459
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_9
    iget-object v8, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    array-length v1, v8

    goto :goto_6

    .line 467
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_a
    iput-object v4, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    .line 468
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 380
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x3a -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 226
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 293
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 294
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 295
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->docid:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 296
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 297
    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 294
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 301
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeChildDocs:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeChildDocs:Z

    if-eq v2, v3, :cond_3

    .line 302
    :cond_2
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeChildDocs:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 304
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeDetails:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeDetails:Z

    if-eqz v2, :cond_5

    .line 305
    :cond_4
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeDetails:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 307
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasSourcePackageName:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->sourcePackageName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 308
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->sourcePackageName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 310
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeSplitDetailsForAllApps:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForAllApps:Z

    if-eqz v2, :cond_9

    .line 311
    :cond_8
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForAllApps:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 313
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->hasIncludeSplitDetailsForNewerVersions:Z

    if-nez v2, :cond_a

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForNewerVersions:Z

    if-eqz v2, :cond_b

    .line 314
    :cond_a
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->includeSplitDetailsForNewerVersions:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 316
    :cond_b
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    array-length v2, v2

    if-lez v2, :cond_c

    .line 317
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 318
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$BulkDetailsRequest;->installedVersionCode:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 317
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 321
    .end local v1    # "i":I
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 322
    return-void
.end method
