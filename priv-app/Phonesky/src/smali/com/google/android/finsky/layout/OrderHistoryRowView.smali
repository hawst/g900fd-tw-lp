.class public Lcom/google/android/finsky/layout/OrderHistoryRowView;
.super Lcom/google/android/play/layout/ForegroundRelativeLayout;
.source "OrderHistoryRowView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;,
        Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;
    }
.end annotation


# instance fields
.field private final mBaseRowHeight:I

.field private final mBaseRowHeightExpanded:I

.field private mCanOpen:Z

.field private mCanRefund:Z

.field private final mCollapseAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

.field private mCollapsedBackgroundResourceId:I

.field private mDateView:Landroid/widget/TextView;

.field private final mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private final mExpandAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

.field private mExpanded:Z

.field private mHasPurchaseDetails:Z

.field private mOpenButton:Landroid/widget/TextView;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mPriceView:Landroid/widget/TextView;

.field private mPurchaseDetailsView:Landroid/widget/TextView;

.field private mRefundButton:Landroid/widget/TextView;

.field private mStatusView:Landroid/widget/TextView;

.field private mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

.field private mThumbnailAspectRatio:F

.field private mTitleView:Landroid/widget/TextView;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const-wide/16 v2, 0x96

    .line 99
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 84
    const/16 v0, 0xa2a

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 101
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mBaseRowHeight:I

    .line 103
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0101

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mBaseRowHeightExpanded:I

    .line 105
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 107
    new-instance v0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpandAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    .line 108
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpandAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->setDuration(J)V

    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpandAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    new-instance v1, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;-><init>(Lcom/google/android/finsky/layout/OrderHistoryRowView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 134
    new-instance v0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCollapseAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCollapseAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->setDuration(J)V

    .line 136
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCollapseAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    new-instance v1, Lcom/google/android/finsky/layout/OrderHistoryRowView$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView$2;-><init>(Lcom/google/android/finsky/layout/OrderHistoryRowView;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 152
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mHasPurchaseDetails:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPurchaseDetailsView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCanOpen:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .prologue
    .line 41
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCanRefund:Z

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method private syncBackground()V
    .locals 2

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    if-eqz v0, :cond_1

    .line 320
    const v0, 0x7f09009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->setBackgroundResource(I)V

    .line 324
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 325
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    if-eqz v0, :cond_2

    const/high16 v0, 0x40800000    # 4.0f

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->setElevation(F)V

    .line 327
    :cond_0
    return-void

    .line 322
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCollapsedBackgroundResourceId:I

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->setBackgroundResource(I)V

    goto :goto_0

    .line 325
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/Boolean;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ILcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;)V
    .locals 22
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "selected"    # Ljava/lang/Boolean;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p6, "collapsedBackgroundResourceId"    # I
    .param p7, "onRefundActionListener"    # Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;

    .prologue
    .line 202
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->getContext()Landroid/content/Context;

    move-result-object v8

    .line 203
    .local v8, "context":Landroid/content/Context;
    invoke-virtual/range {p4 .. p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    .line 204
    move/from16 v0, p6

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCollapsedBackgroundResourceId:I

    .line 205
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-object/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v20

    invoke-static/range {v19 .. v20}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 210
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 212
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v19

    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mThumbnailAspectRatio:F

    .line 214
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v17

    check-cast v17, Lcom/google/android/finsky/layout/DocImageView;

    .line 217
    .local v17, "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    const/4 v11, 0x0

    .line 218
    .local v11, "hasImage":Z
    sget-object v5, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->CORE_IMAGE_TYPES:[I

    .local v5, "arr$":[I
    array-length v13, v5

    .local v13, "len$":I
    const/4 v12, 0x0

    .local v12, "i$":I
    :goto_0
    if-ge v12, v13, :cond_0

    aget v18, v5, v12

    .line 219
    .local v18, "type":I
    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v19

    if-eqz v19, :cond_1

    .line 220
    const/4 v11, 0x1

    .line 224
    .end local v18    # "type":I
    :cond_0
    if-eqz v11, :cond_2

    .line 225
    sget-object v19, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->CORE_IMAGE_TYPES:[I

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/google/android/play/layout/PlayCardThumbnail;->setVisibility(I)V

    .line 231
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mTitleView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getPurchaseHistoryDetails()Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    move-result-object v9

    .line 233
    .local v9, "details":Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;
    iget-boolean v0, v9, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseTimestampMsec:Z

    move/from16 v19, v0

    if-eqz v19, :cond_3

    .line 234
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mDateView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    iget-wide v0, v9, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseTimestampMsec:J

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Lcom/google/android/finsky/utils/DateUtils;->formatShortDisplayDate(J)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mDateView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 240
    :goto_2
    iget-object v14, v9, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    .line 241
    .local v14, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v14, :cond_4

    iget-boolean v0, v14, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    move/from16 v19, v0

    if-eqz v19, :cond_4

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPriceView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    iget-object v0, v9, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPriceView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    :goto_3
    iget-boolean v0, v9, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseStatus:Z

    move/from16 v19, v0

    if-eqz v19, :cond_5

    .line 248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mStatusView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    iget-object v0, v9, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseStatus:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mStatusView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    .line 254
    :goto_4
    iget-boolean v0, v9, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->hasPurchaseDetailsHtml:Z

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mHasPurchaseDetails:Z

    .line 255
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mHasPurchaseDetails:Z

    move/from16 v19, v0

    if-eqz v19, :cond_7

    .line 256
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPurchaseDetailsView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    iget-object v0, v9, Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;->purchaseDetailsHtml:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPurchaseDetailsView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPurchaseDetailsView:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    move/from16 v19, v0

    if-eqz v19, :cond_6

    const/16 v19, 0x0

    :goto_5
    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 262
    :goto_6
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->hasClickListener(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCanOpen:Z

    .line 263
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCanOpen:Z

    move/from16 v19, v0

    if-eqz v19, :cond_9

    .line 264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const v20, 0x7f0c020c

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    new-instance v6, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v19, 0xa2d

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    move-object/from16 v3, p0

    invoke-direct {v6, v0, v1, v2, v3}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 267
    .local v6, "clickLogNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v20

    move/from16 v0, v20

    invoke-static {v8, v0}, Lcom/google/android/play/utils/PlayCorpusUtils;->getPrimaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    move/from16 v19, v0

    if-eqz v19, :cond_8

    const/16 v19, 0x0

    :goto_7
    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 275
    .end local v6    # "clickLogNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v19

    const/16 v20, 0x3

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    .line 278
    new-instance v4, Lcom/google/android/finsky/activities/AppActionAnalyzer;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v19

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v20

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v21

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/finsky/activities/AppActionAnalyzer;-><init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V

    .line 280
    .local v4, "analyzer":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    iget-boolean v0, v4, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isRefundable:Z

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCanRefund:Z

    .line 281
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCanRefund:Z

    move/from16 v19, v0

    if-eqz v19, :cond_b

    .line 282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const v20, 0x7f0c02a3

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    move/from16 v19, v0

    if-eqz v19, :cond_a

    const/16 v19, 0x0

    :goto_9
    move-object/from16 v0, v20

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    new-instance v20, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, p7

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/OrderHistoryRowView$3;-><init>(Lcom/google/android/finsky/layout/OrderHistoryRowView;Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/activities/AppActionAnalyzer;)V

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    .end local v4    # "analyzer":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    :goto_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->getPaddingTop()I

    move-result v16

    .line 303
    .local v16, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->getPaddingBottom()I

    move-result v15

    .line 304
    .local v15, "paddingBottom":I
    add-int v19, v16, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mThumbnailAspectRatio:F

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mBaseRowHeight:I

    move/from16 v21, v0

    sub-int v21, v21, v16

    sub-int v21, v21, v15

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int v7, v19, v20

    .line 308
    .local v7, "collapsedHeight":I
    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mBaseRowHeightExpanded:I

    .line 309
    .local v10, "expandedHeight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    move/from16 v19, v0

    if-eqz v19, :cond_d

    move/from16 v19, v10

    :goto_b
    move/from16 v0, v19

    move-object/from16 v1, v20

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpandAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v7, v10}, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->setHeights(II)V

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCollapseAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10, v7}, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->setHeights(II)V

    .line 314
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->syncBackground()V

    .line 315
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->requestLayout()V

    .line 316
    return-void

    .line 218
    .end local v7    # "collapsedHeight":I
    .end local v9    # "details":Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;
    .end local v10    # "expandedHeight":I
    .end local v14    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v15    # "paddingBottom":I
    .end local v16    # "paddingTop":I
    .restart local v18    # "type":I
    :cond_1
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_0

    .line 228
    .end local v18    # "type":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Lcom/google/android/play/layout/PlayCardThumbnail;->setVisibility(I)V

    goto/16 :goto_1

    .line 238
    .restart local v9    # "details":Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mDateView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 245
    .restart local v14    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPriceView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 251
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mStatusView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 258
    :cond_6
    const/16 v19, 0x8

    goto/16 :goto_5

    .line 260
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPurchaseDetailsView:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 271
    .restart local v6    # "clickLogNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :cond_8
    const/16 v19, 0x8

    goto/16 :goto_7

    .line 273
    .end local v6    # "clickLogNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    .line 283
    .restart local v4    # "analyzer":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    :cond_a
    const/16 v19, 0x8

    goto/16 :goto_9

    .line 295
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_a

    .line 298
    .end local v4    # "analyzer":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    :cond_c
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCanRefund:Z

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;

    move-object/from16 v19, v0

    const/16 v20, 0x8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_a

    .restart local v7    # "collapsedHeight":I
    .restart local v10    # "expandedHeight":I
    .restart local v15    # "paddingBottom":I
    .restart local v16    # "paddingTop":I
    :cond_d
    move/from16 v19, v7

    .line 309
    goto/16 :goto_b
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 343
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onFinishInflate()V

    .line 157
    const v0, 0x7f0a01b5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardThumbnail;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 158
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mTitleView:Landroid/widget/TextView;

    .line 159
    const v0, 0x7f0a029c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mDateView:Landroid/widget/TextView;

    .line 160
    const v0, 0x7f0a0246

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPriceView:Landroid/widget/TextView;

    .line 161
    const v0, 0x7f0a029d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mStatusView:Landroid/widget/TextView;

    .line 162
    const v0, 0x7f0a023c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPurchaseDetailsView:Landroid/widget/TextView;

    .line 163
    const v0, 0x7f0a029e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;

    .line 164
    const v0, 0x7f0a029f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;

    .line 165
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 169
    iget-object v3, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v3}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 174
    .local v2, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 175
    .local v0, "availableHeight":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->getPaddingTop()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int/2addr v3, v4

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v1, v3, v4

    .line 177
    .local v1, "thumbnailHeight":I
    int-to-float v3, v1

    iget v4, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mThumbnailAspectRatio:F

    div-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 179
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onMeasure(II)V

    .line 180
    return-void
.end method

.method public toggleExpansion()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    if-nez v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpandAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 193
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    .line 194
    invoke-direct {p0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->syncBackground()V

    .line 195
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mExpanded:Z

    return v0

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCollapseAnimation:Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 193
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
