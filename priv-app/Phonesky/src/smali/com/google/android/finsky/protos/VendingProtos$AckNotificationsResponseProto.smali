.class public final Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AckNotificationsResponseProto"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4530
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4531
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    .line 4532
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;
    .locals 1

    .prologue
    .line 4535
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;->cachedSize:I

    .line 4536
    return-object p0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4544
    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4545
    .local v0, "tag":I
    packed-switch v0, :pswitch_data_0

    .line 4549
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4550
    :pswitch_0
    return-object p0

    .line 4545
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4513
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    move-result-object v0

    return-object v0
.end method
