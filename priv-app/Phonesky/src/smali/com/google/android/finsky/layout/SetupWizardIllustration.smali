.class public Lcom/google/android/finsky/layout/SetupWizardIllustration;
.super Landroid/widget/LinearLayout;
.source "SetupWizardIllustration.java"


# instance fields
.field private final mAspectRatio:F

.field private final mBaselineGridSize:F

.field private final mCollapsable:Z

.field private mImageView:Lcom/google/android/play/image/FifeImageView;

.field private final mStatusBarOffset:I

.field private mTitleView:Landroid/widget/TextView;

.field private final mUseTabletGraphic:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/SetupWizardIllustration;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    sget-object v1, Lcom/android/vending/R$styleable;->SetupWizardIllustration:[I

    invoke-virtual {p1, p2, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 62
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mCollapsable:Z

    .line 63
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mAspectRatio:F

    .line 64
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0128

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mBaselineGridSize:F

    .line 68
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0129

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mStatusBarOffset:I

    .line 70
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mUseTabletGraphic:Z

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/SetupWizardIllustration;)Lcom/google/android/play/image/FifeImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SetupWizardIllustration;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/SetupWizardIllustration;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SetupWizardIllustration;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mUseTabletGraphic:Z

    return v0
.end method


# virtual methods
.method public configure(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "imageUrl"    # Ljava/lang/String;
    .param p2, "collapseIfPossible"    # Z

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    if-nez v0, :cond_0

    .line 140
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mCollapsable:Z

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_0

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    new-instance v1, Lcom/google/android/finsky/layout/SetupWizardIllustration$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/SetupWizardIllustration$1;-><init>(Lcom/google/android/finsky/layout/SetupWizardIllustration;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setOnLoadedListener(Lcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 75
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 76
    const v0, 0x7f0a038f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    .line 77
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mTitleView:Landroid/widget/TextView;

    .line 78
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v3, 0x0

    .line 144
    iget-object v2, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v2}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v2

    const/16 v4, 0x8

    if-eq v2, v4, :cond_2

    const/4 v0, 0x1

    .line 148
    .local v0, "hasImage":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mTitleView:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_3

    :goto_1
    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 154
    :cond_0
    if-eqz v0, :cond_1

    iget v2, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mAspectRatio:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_1

    .line 155
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mAspectRatio:F

    div-float/2addr v2, v3

    float-to-int v1, v2

    .line 156
    .local v1, "height":I
    int-to-float v2, v1

    int-to-float v3, v1

    iget v4, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mBaselineGridSize:F

    rem-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v1, v2

    .line 157
    iget-object v2, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v2}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 159
    .end local v1    # "height":I
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 160
    return-void

    .end local v0    # "hasImage":Z
    :cond_2
    move v0, v3

    .line 144
    goto :goto_0

    .line 149
    .restart local v0    # "hasImage":Z
    :cond_3
    iget v3, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration;->mStatusBarOffset:I

    goto :goto_1
.end method
