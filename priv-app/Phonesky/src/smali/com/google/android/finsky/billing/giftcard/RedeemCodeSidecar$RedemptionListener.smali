.class Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;
.super Ljava/lang/Object;
.source "RedeemCodeSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RedemptionListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;)V
    .locals 0

    .prologue
    .line 335
    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$1;

    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;-><init>(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;)V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 7
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v2, 0x0

    .line 374
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    # setter for: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mVolleyError:Lcom/android/volley/VolleyError;
    invoke-static {v0, p1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$1002(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;

    .line 375
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    # getter for: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$1100(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x321

    const/4 v4, -0x1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    move-object v3, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    const/4 v1, 0x3

    const/4 v2, 0x1

    # invokes: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$1200(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V

    .line 378
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;)V
    .locals 6
    .param p1, "response"    # Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    .prologue
    const/4 v5, 0x0

    .line 340
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    # setter for: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mLastRedeemCodeResponse:Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;
    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$102(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;)Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    .line 341
    iget v0, p1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->result:I

    .line 342
    .local v0, "resultCode":I
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    const/16 v2, 0x321

    iget-object v3, p1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->serverLogsCookie:[B

    # invokes: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->logBackgroundEvent(II[B)V
    invoke-static {v1, v2, v0, v3}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$200(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II[B)V

    .line 344
    packed-switch v0, :pswitch_data_0

    .line 365
    :pswitch_0
    const-string v1, "Received error/unknown result: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->result:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    iget-object v2, p1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->errorMessageHtml:Ljava/lang/String;

    # setter for: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mErrorHtml:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$802(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;Ljava/lang/String;)Ljava/lang/String;

    .line 367
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    const/4 v2, 0x3

    # invokes: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V
    invoke-static {v1, v2, v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$900(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V

    .line 370
    :goto_0
    return-void

    .line 346
    :pswitch_1
    iget-object v1, p1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v1, v1, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v1, :cond_0

    .line 348
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    # getter for: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;
    invoke-static {v2}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$300(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    iget-object v3, v3, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    const-string v4, "redeem-code-sidecar"

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/finsky/library/LibraryReplicators;->applyLibraryUpdate(Landroid/accounts/Account;Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)V

    .line 352
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    const/4 v2, 0x2

    # invokes: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V
    invoke-static {v1, v2, v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$400(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V

    goto :goto_0

    .line 355
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    const/4 v2, 0x4

    # invokes: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V
    invoke-static {v1, v2, v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$500(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V

    goto :goto_0

    .line 358
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    const/4 v2, 0x5

    # invokes: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V
    invoke-static {v1, v2, v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$600(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V

    goto :goto_0

    .line 361
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->this$0:Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;

    const/4 v2, 0x6

    # invokes: Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->setState(II)V
    invoke-static {v1, v2, v5}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;->access$700(Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar;II)V

    goto :goto_0

    .line 344
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 335
    check-cast p1, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeSidecar$RedemptionListener;->onResponse(Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;)V

    return-void
.end method
