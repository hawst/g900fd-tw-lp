.class public Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;
.super Lcom/google/android/finsky/layout/SeparatorLinearLayout;
.source "SetupWizardRestoreAppsSelector.java"


# instance fields
.field private mMainText:Landroid/widget/TextView;

.field private mSecondaryText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;-><init>(Landroid/content/Context;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method private syncContentDescription()V
    .locals 3

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->mMainText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->mMainText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->mSecondaryText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 54
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->mSecondaryText:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c02e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 58
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->onFinishInflate()V

    .line 32
    const v0, 0x7f0a0392

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->mMainText:Landroid/widget/TextView;

    .line 33
    const v0, 0x7f0a0393

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->mSecondaryText:Landroid/widget/TextView;

    .line 34
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->syncContentDescription()V

    .line 35
    return-void
.end method

.method public setTexts(Ljava/lang/String;)V
    .locals 1
    .param p1, "mainText"    # Ljava/lang/String;

    .prologue
    .line 38
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setTexts(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public setTexts(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mainText"    # Ljava/lang/String;
    .param p2, "secondaryText"    # Ljava/lang/String;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->mMainText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->mSecondaryText:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->syncContentDescription()V

    .line 45
    return-void
.end method
