.class public Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;
.super Lcom/google/android/finsky/fragments/SidecarFragment;
.source "BillingProfileSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/fragments/SidecarFragment;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mBillingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mErrorMessageHtml:Ljava/lang/String;

.field private mExtraPostParams:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPurchaseContextToken:Ljava/lang/String;

.field private mVolleyError:Lcom/android/volley/VolleyError;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "purchaseContextToken"    # Ljava/lang/String;

    .prologue
    .line 69
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 70
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "BillingProfileSidecar.account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 71
    const-string v2, "BillingProfileSidecar.purchaseContextToken"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;-><init>()V

    .line 73
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setArguments(Landroid/os/Bundle;)V

    .line 74
    return-object v1
.end method


# virtual methods
.method public getBillingProfile()Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mBillingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->billingProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    return-object v0
.end method

.method public getErrorMessageHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mErrorMessageHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getVolleyError()Lcom/android/volley/VolleyError;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BillingProfileSidecar.account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mAccount:Landroid/accounts/Account;

    .line 80
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 81
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "BillingProfileSidecar.purchaseContextToken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mPurchaseContextToken:Ljava/lang/String;

    .line 82
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onCreate(Landroid/os/Bundle;)V

    .line 83
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    .line 137
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    .line 138
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;)V
    .locals 3
    .param p1, "billingProfileResponse"    # Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    .line 119
    iget v0, p1, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->result:I

    packed-switch v0, :pswitch_data_0

    .line 129
    const v0, 0x7f0c01e0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mErrorMessageHtml:Ljava/lang/String;

    .line 130
    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    .line 132
    :goto_0
    return-void

    .line 121
    :pswitch_0
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mBillingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    .line 122
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    goto :goto_0

    .line 125
    :pswitch_1
    iget-object v0, p1, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;->userMessageHtml:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mErrorMessageHtml:Ljava/lang/String;

    .line 126
    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    goto :goto_0

    .line 119
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->onResponse(Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 88
    const-string v0, "BillingProfileSidecar.billingProfileResponse"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mBillingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 90
    return-void
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    .line 95
    const-string v0, "BillingProfileSidecar.billingProfileResponse"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mBillingProfileResponse:Lcom/google/android/finsky/protos/BuyInstruments$BillingProfileResponse;

    .line 97
    return-void
.end method

.method public start(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p1, "extraPostParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->setState(II)V

    .line 101
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mExtraPostParams:Ljava/util/Map;

    .line 102
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mPurchaseContextToken:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->mExtraPostParams:Ljava/util/Map;

    invoke-interface {v0, v1, v2, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->billingProfile(Ljava/lang/String;Ljava/util/Map;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 103
    return-void
.end method
