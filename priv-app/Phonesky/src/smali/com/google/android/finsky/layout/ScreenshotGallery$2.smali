.class Lcom/google/android/finsky/layout/ScreenshotGallery$2;
.super Landroid/os/AsyncTask;
.source "ScreenshotGallery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/ScreenshotGallery;->startImageLoadingTaskIfNecessary()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/ScreenshotGallery;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$2;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 181
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/ScreenshotGallery$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 184
    const/4 v0, 0x0

    .line 186
    .local v0, "imageIndex":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ScreenshotGallery$2;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$2;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # getter for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mImageStripAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;
    invoke-static {v1}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$000(Lcom/google/android/finsky/layout/ScreenshotGallery;)Lcom/google/android/finsky/adapters/ImageStripAdapter;

    move-result-object v1

    if-nez v1, :cond_1

    .line 200
    :cond_0
    const/4 v1, 0x0

    return-object v1

    .line 189
    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/ScreenshotGallery$2;->publishProgress([Ljava/lang/Object;)V

    .line 190
    add-int/lit8 v0, v0, 0x1

    .line 191
    iget-object v1, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$2;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # getter for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mCombinedImagesToLoad:Ljava/util/List;
    invoke-static {v1}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$100(Lcom/google/android/finsky/layout/ScreenshotGallery;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 195
    const-wide/16 v2, 0x190

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 196
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 2
    .param p1, "values"    # [Ljava/lang/Integer;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$2;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # invokes: Lcom/google/android/finsky/layout/ScreenshotGallery;->loadNextImage(I)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$200(Lcom/google/android/finsky/layout/ScreenshotGallery;I)V

    .line 207
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 181
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/ScreenshotGallery$2;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
