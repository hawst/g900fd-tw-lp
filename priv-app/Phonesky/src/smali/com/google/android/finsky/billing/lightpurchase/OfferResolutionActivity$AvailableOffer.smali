.class public Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
.super Ljava/lang/Object;
.source "OfferResolutionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AvailableOffer"
.end annotation


# instance fields
.field public final doc:Lcom/google/android/finsky/api/model/Document;

.field public final offer:Lcom/google/android/finsky/protos/Common$Offer;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;)V
    .locals 0
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "offer"    # Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    .line 374
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 375
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->doc:Lcom/google/android/finsky/api/model/Document;

    .line 376
    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    .line 377
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "x1"    # Lcom/google/android/finsky/protos/Common$Offer;
    .param p3, "x2"    # Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;

    .prologue
    .line 370
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;-><init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;)V

    return-void
.end method
