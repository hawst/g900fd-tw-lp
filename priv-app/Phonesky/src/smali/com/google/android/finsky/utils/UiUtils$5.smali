.class final Lcom/google/android/finsky/utils/UiUtils$5;
.super Ljava/lang/Object;
.source "UiUtils.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/UiUtils;->enableActionBarOverlayScrolling(Lcom/google/android/finsky/layout/ObservableScrollView;ILcom/google/android/finsky/layout/actionbar/ActionBarController;)Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

.field final synthetic val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

.field final synthetic val$scrollView:Lcom/google/android/finsky/layout/ObservableScrollView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/google/android/finsky/utils/UiUtils$5;->val$scrollView:Lcom/google/android/finsky/layout/ObservableScrollView;

    iput-object p2, p0, Lcom/google/android/finsky/utils/UiUtils$5;->val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    iput-object p3, p0, Lcom/google/android/finsky/utils/UiUtils$5;->val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 4

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/finsky/utils/UiUtils$5;->val$scrollView:Lcom/google/android/finsky/layout/ObservableScrollView;

    iget-object v1, p0, Lcom/google/android/finsky/utils/UiUtils$5;->val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    iget-object v2, p0, Lcom/google/android/finsky/utils/UiUtils$5;->val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    iget-object v3, p0, Lcom/google/android/finsky/utils/UiUtils$5;->val$scrollView:Lcom/google/android/finsky/layout/ObservableScrollView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/ObservableScrollView;->getScrollY()I

    move-result v3

    # invokes: Lcom/google/android/finsky/utils/UiUtils;->syncActionBarAlpha(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/UiUtils;->access$000(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V

    .line 462
    iget-object v0, p0, Lcom/google/android/finsky/utils/UiUtils$5;->val$scrollView:Lcom/google/android/finsky/layout/ObservableScrollView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/ObservableScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 463
    const/4 v0, 0x1

    return v0
.end method
