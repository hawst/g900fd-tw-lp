.class public final Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InAppPurchaseInformationRequestProto"
.end annotation


# instance fields
.field public billingApiVersion:I

.field public hasBillingApiVersion:Z

.field public hasNonce:Z

.field public hasSignatureAlgorithm:Z

.field public nonce:J

.field public notificationId:[Ljava/lang/String;

.field public signatureAlgorithm:Ljava/lang/String;

.field public signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3807
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3808
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    .line 3809
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3812
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 3813
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->nonce:J

    .line 3814
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasNonce:Z

    .line 3815
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    .line 3816
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureAlgorithm:Ljava/lang/String;

    .line 3817
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasSignatureAlgorithm:Z

    .line 3818
    iput v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->billingApiVersion:I

    .line 3819
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasBillingApiVersion:Z

    .line 3820
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->cachedSize:I

    .line 3821
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 10

    .prologue
    .line 3852
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 3853
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    if-eqz v5, :cond_0

    .line 3854
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3857
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasNonce:Z

    if-nez v5, :cond_1

    iget-wide v6, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->nonce:J

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_2

    .line 3858
    :cond_1
    const/4 v5, 0x2

    iget-wide v6, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->nonce:J

    invoke-static {v5, v6, v7}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v5

    add-int/2addr v4, v5

    .line 3861
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_5

    .line 3862
    const/4 v0, 0x0

    .line 3863
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 3864
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_4

    .line 3865
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 3866
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 3867
    add-int/lit8 v0, v0, 0x1

    .line 3868
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3864
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3872
    .end local v2    # "element":Ljava/lang/String;
    :cond_4
    add-int/2addr v4, v1

    .line 3873
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 3875
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_5
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasSignatureAlgorithm:Z

    if-nez v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureAlgorithm:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 3876
    :cond_6
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureAlgorithm:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3879
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasBillingApiVersion:Z

    if-nez v5, :cond_8

    iget v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->billingApiVersion:I

    if-eqz v5, :cond_9

    .line 3880
    :cond_8
    const/4 v5, 0x5

    iget v6, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->billingApiVersion:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 3883
    :cond_9
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 3891
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3892
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3896
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3897
    :sswitch_0
    return-object p0

    .line 3902
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    if-nez v5, :cond_1

    .line 3903
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    .line 3905
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3909
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->nonce:J

    .line 3910
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasNonce:Z

    goto :goto_0

    .line 3914
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3916
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    if-nez v5, :cond_3

    move v1, v4

    .line 3917
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 3918
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 3919
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3921
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 3922
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3923
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3921
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3916
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 3926
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 3927
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    goto :goto_0

    .line 3931
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureAlgorithm:Ljava/lang/String;

    .line 3932
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasSignatureAlgorithm:Z

    goto :goto_0

    .line 3936
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->billingApiVersion:I

    .line 3937
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasBillingApiVersion:Z

    goto :goto_0

    .line 3892
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3772
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3827
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    if-eqz v2, :cond_0

    .line 3828
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureHash:Lcom/google/android/finsky/protos/VendingProtos$SignatureHashProto;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3830
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasNonce:Z

    if-nez v2, :cond_1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->nonce:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 3831
    :cond_1
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->nonce:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 3833
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 3834
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 3835
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->notificationId:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 3836
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 3837
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3834
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3841
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasSignatureAlgorithm:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureAlgorithm:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 3842
    :cond_5
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->signatureAlgorithm:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3844
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->hasBillingApiVersion:Z

    if-nez v2, :cond_7

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->billingApiVersion:I

    if-eqz v2, :cond_8

    .line 3845
    :cond_7
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationRequestProto;->billingApiVersion:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3847
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3848
    return-void
.end method
