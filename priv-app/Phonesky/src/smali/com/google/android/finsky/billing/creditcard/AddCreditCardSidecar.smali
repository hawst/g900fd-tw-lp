.class public Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;
.super Lcom/google/android/finsky/fragments/SidecarFragment;
.source "AddCreditCardSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$1;,
        Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowErrorListener;,
        Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/fragments/SidecarFragment;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mErrorHtml:Ljava/lang/String;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

.field private mVolleyError:Lcom/android/volley/VolleyError;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;-><init>()V

    .line 97
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;)Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->performRequest()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->logError(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;Lcom/android/volley/VolleyError;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;
    .param p1, "x1"    # Lcom/android/volley/VolleyError;
    .param p2, "x2"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setVolleyError(Lcom/android/volley/VolleyError;I)V

    return-void
.end method

.method private clearState()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 174
    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 175
    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mErrorHtml:Ljava/lang/String;

    .line 176
    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    .line 177
    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .line 178
    return-void
.end method

.method private static isInputValidationResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)Z
    .locals 2
    .param p0, "updateFopResponse"    # Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .prologue
    .line 181
    iget v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSuccess(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)Z
    .locals 1
    .param p1, "updateFopResponse"    # Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .prologue
    .line 185
    iget v0, p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private logError(ILjava/lang/String;)V
    .locals 7
    .param p1, "errorCode"    # I
    .param p2, "exceptionType"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 226
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x14c

    move-object v3, v2

    move v4, p1

    move-object v5, p2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 228
    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 120
    new-instance v1, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;-><init>()V

    .line 121
    .local v1, "result":Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 122
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setArguments(Landroid/os/Bundle;)V

    .line 124
    return-object v1
.end method

.method private performRequest()V
    .locals 2

    .prologue
    .line 258
    new-instance v0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;-><init>()V

    .line 259
    .local v0, "request":Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    iput-object v1, v0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 260
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v1, v0, p0, p0}, Lcom/google/android/finsky/api/DfeApi;->updateInstrument(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentRequest;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 261
    return-void
.end method

.method private queueEscrowCredentialsRequest(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "creditCardNumber"    # Ljava/lang/String;
    .param p2, "cvc"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 205
    new-instance v0, Lcom/google/android/finsky/billing/creditcard/EscrowRequest;

    new-instance v1, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;

    invoke-direct {v1, p0, v3}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowResponseListener;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$1;)V

    new-instance v2, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowErrorListener;

    invoke-direct {v2, p0, v3}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowErrorListener;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$1;)V

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/google/android/finsky/billing/creditcard/EscrowRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 207
    .local v0, "escrowRequest":Lcom/google/android/finsky/billing/creditcard/EscrowRequest;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 208
    return-void
.end method

.method private setErrorWithMessage(Ljava/lang/String;)V
    .locals 1
    .param p1, "htmlMessage"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x3

    .line 211
    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mErrorHtml:Ljava/lang/String;

    .line 212
    invoke-virtual {p0, v0, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setState(II)V

    .line 213
    return-void
.end method

.method private setErrorWithMessageWithChoice(Ljava/lang/String;)V
    .locals 2
    .param p1, "htmlMessage"    # Ljava/lang/String;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mErrorHtml:Ljava/lang/String;

    .line 217
    const/4 v0, 0x3

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setState(II)V

    .line 218
    return-void
.end method

.method private setVolleyError(Lcom/android/volley/VolleyError;I)V
    .locals 1
    .param p1, "error"    # Lcom/android/volley/VolleyError;
    .param p2, "substate"    # I

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    .line 222
    const/4 v0, 0x3

    invoke-virtual {p0, v0, p2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setState(II)V

    .line 223
    return-void
.end method


# virtual methods
.method public getErrorHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mErrorHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getResponse()Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    return-object v0
.end method

.method public getVolleyError()Lcom/android/volley/VolleyError;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mVolleyError:Lcom/android/volley/VolleyError;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 129
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 131
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v1}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 132
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onCreate(Landroid/os/Bundle;)V

    .line 133
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 253
    const-string v0, "Error received: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 254
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setVolleyError(Lcom/android/volley/VolleyError;I)V

    .line 255
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V
    .locals 7
    .param p1, "response"    # Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .prologue
    const/4 v1, 0x5

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 234
    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .line 235
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->isSuccess(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x14b

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 238
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v4}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setState(II)V

    .line 249
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-static {v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->isInputValidationResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    const/4 v0, 0x4

    invoke-direct {p0, v0, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->logError(ILjava/lang/String;)V

    .line 241
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setState(II)V

    goto :goto_0

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasUserMessageHtml:Z

    if-eqz v0, :cond_2

    .line 243
    invoke-direct {p0, v1, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->logError(ILjava/lang/String;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setErrorWithMessageWithChoice(Ljava/lang/String;)V

    goto :goto_0

    .line 246
    :cond_2
    invoke-direct {p0, v4, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->logError(ILjava/lang/String;)V

    .line 247
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    const v1, 0x7f0c00de

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setErrorWithMessage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 34
    check-cast p1, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->onResponse(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 137
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 138
    const-string v0, "AddCreditCardSidecar.message"

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mErrorHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    const-string v0, "AddCreditCardSidecar.addCreditCardResponse"

    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 140
    return-void
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 144
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    .line 145
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getSubstate()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 147
    invoke-virtual {p0, v2, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setState(II)V

    .line 149
    :cond_0
    const-string v0, "AddCreditCardSidecar.message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mErrorHtml:Ljava/lang/String;

    .line 150
    const-string v0, "AddCreditCardSidecar.addCreditCardResponse"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mAddCreditCardResponse:Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .line 152
    return-void
.end method

.method public saveCreditCard(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$Instrument;)V
    .locals 8
    .param p1, "creditCardNumber"    # Ljava/lang/String;
    .param p2, "cvc"    # Ljava/lang/String;
    .param p3, "instrument"    # Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 162
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->getState()I

    move-result v0

    if-ne v0, v7, :cond_0

    .line 163
    const-string v0, "saveCreditCard() called while RUNNING."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x14a

    move-object v3, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 167
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->clearState()V

    .line 168
    iput-object p3, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->mInstrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 169
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->queueEscrowCredentialsRequest(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p0, v7, v4}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setState(II)V

    .line 171
    return-void
.end method
