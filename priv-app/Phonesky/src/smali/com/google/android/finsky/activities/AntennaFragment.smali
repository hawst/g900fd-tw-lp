.class public Lcom/google/android/finsky/activities/AntennaFragment;
.super Lcom/google/android/finsky/activities/DetailsDataBasedFragment;
.source "AntennaFragment.java"


# instance fields
.field private final mDescriptionPaddingLeftRight:I

.field private final mExtraPaddingLeftRight:I

.field private final mMaxRelatedItemRows:I

.field private final mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;-><init>()V

    .line 34
    new-instance v1, Lcom/google/android/finsky/activities/SongListViewBinder;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/SongListViewBinder;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    .line 54
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 55
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0e000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mMaxRelatedItemRows:I

    .line 57
    const v1, 0x7f0b00f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mDescriptionPaddingLeftRight:I

    .line 59
    const v1, 0x7f0b0086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mExtraPaddingLeftRight:I

    .line 61
    return-void
.end method

.method public static newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Lcom/google/android/finsky/activities/AntennaFragment;
    .locals 2
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/finsky/activities/AntennaFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/AntennaFragment;-><init>()V

    .line 48
    .local v0, "fragment":Lcom/google/android/finsky/activities/AntennaFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/activities/AntennaFragment;->setDfeTocAndUrl(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    .line 49
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/activities/AntennaFragment;->setInitialDocument(Lcom/google/android/finsky/api/model/Document;)V

    .line 50
    return-object v0
.end method


# virtual methods
.method protected bindPromoHeroImage(Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    check-cast v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v0, p1, v1, p0}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->bindDetailsAntenna(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 145
    return-void
.end method

.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 70
    const v0, 0x7f040023

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x6

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 161
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 163
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/AntennaFragment;->configureContentView(Landroid/view/View;)V

    .line 165
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AntennaFragment;->recordState()V

    .line 174
    iget-object v0, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SongListViewBinder;->onDestroyView()V

    .line 176
    invoke-super {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->onDestroyView()V

    .line 177
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/activities/SongListViewBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 66
    return-void
.end method

.method public rebindActionBar()V
    .locals 4

    .prologue
    .line 149
    iget-object v1, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AntennaFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 151
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AntennaFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 152
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    if-eqz v0, :cond_0

    .line 153
    iget-object v1, p0, Lcom/google/android/finsky/activities/AntennaFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 156
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    return-void
.end method

.method protected rebindViews(Landroid/os/Bundle;)V
    .locals 28
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AntennaFragment;->rebindActionBar()V

    .line 76
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AntennaFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v5

    .line 77
    .local v5, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getAntennaInfo()Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    move-result-object v23

    .line 79
    .local v23, "antennaInfo":Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AntennaFragment;->getView()Landroid/view/View;

    move-result-object v24

    .line 81
    .local v24, "fragmentView":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v24

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v5, v2}, Lcom/google/android/finsky/activities/AntennaFragment;->configurePromoHeroImage(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Landroid/os/Bundle;)V

    .line 83
    const v4, 0x7f0a00cb

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/DetailsTextSection;

    .line 85
    .local v3, "descriptionPanel":Lcom/google/android/finsky/layout/DetailsTextSection;
    if-eqz v3, :cond_1

    .line 88
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mDescriptionPaddingLeftRight:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mExtraPaddingLeftRight:I

    add-int v25, v4, v6

    .line 90
    .local v25, "fullDescriptionPaddingLeftRight":I
    invoke-virtual {v3}, Lcom/google/android/finsky/layout/DetailsTextSection;->getPaddingTop()I

    move-result v4

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/DetailsTextSection;->getPaddingBottom()I

    move-result v6

    move/from16 v0, v25

    move/from16 v1, v25

    invoke-virtual {v3, v0, v4, v1, v6}, Lcom/google/android/finsky/layout/DetailsTextSection;->setPadding(IIII)V

    .line 95
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AntennaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    .line 96
    .local v26, "res":Landroid/content/res/Resources;
    const v4, 0x7f09009c

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 97
    .local v9, "backgroundFillColor":I
    const/4 v4, 0x1

    invoke-virtual {v5, v4}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 98
    move-object/from16 v0, v23

    invoke-static {v0, v9}, Lcom/google/android/finsky/utils/UiUtils;->getFillColor(Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;I)I

    move-result v9

    .line 101
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    const/4 v6, 0x1

    move-object/from16 v7, p1

    move-object/from16 v8, p0

    move-object/from16 v10, p0

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/layout/DetailsTextSection;->bindEditorialDescription(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;ZLandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ILcom/google/android/finsky/activities/TextSectionStateListener;)V

    .line 105
    .end local v9    # "backgroundFillColor":I
    .end local v25    # "fullDescriptionPaddingLeftRight":I
    .end local v26    # "res":Landroid/content/res/Resources;
    :cond_1
    const v4, 0x7f0a00cc

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/layout/SongList;

    .line 106
    .local v11, "songList":Lcom/google/android/finsky/layout/SongList;
    if-eqz v11, :cond_2

    .line 107
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionTracks:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-object/from16 v27, v0

    .line 108
    .local v27, "sectionTracks":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mSongListViewBinder:Lcom/google/android/finsky/activities/SongListViewBinder;

    const/4 v12, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AntennaFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0339

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v27

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    move-object/from16 v0, v27

    iget-object v15, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    const/16 v16, 0x0

    const v17, 0x7fffffff

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AntennaFragment;->hasDetailsDataLoaded()Z

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AntennaFragment;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v20, v0

    move-object/from16 v21, p0

    invoke-virtual/range {v10 .. v21}, Lcom/google/android/finsky/activities/SongListViewBinder;->bind(Lcom/google/android/finsky/layout/SongList;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZLcom/google/android/finsky/library/Libraries;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 115
    .end local v27    # "sectionTracks":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    :cond_2
    const v4, 0x7f0a00ce

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/layout/DetailsPackSection;

    .line 117
    .local v12, "bodyOfWorkPanel":Lcom/google/android/finsky/layout/DetailsPackSection;
    if-eqz v12, :cond_3

    .line 118
    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->sectionAlbums:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-object/from16 v22, v0

    .line 119
    .local v22, "albumMetadata":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AntennaFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v8

    invoke-virtual {v12, v4, v6, v7, v8}, Lcom/google/android/finsky/layout/DetailsPackSection;->init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 120
    move-object/from16 v0, v22

    iget-object v14, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v15, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->descriptionHtml:Ljava/lang/String;

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mCardItemsPerRow:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mMaxRelatedItemRows:I

    move/from16 v19, v0

    const/16 v20, 0x1

    move-object v13, v5

    move-object/from16 v21, p0

    invoke-virtual/range {v12 .. v21}, Lcom/google/android/finsky/layout/DetailsPackSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 127
    .end local v22    # "albumMetadata":Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    :cond_3
    const v4, 0x7f0a00cd

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;

    .line 129
    .local v13, "secondaryActionsPanel":Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;
    if-eqz v13, :cond_4

    .line 132
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mExtraPaddingLeftRight:I

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->getPaddingTop()I

    move-result v6

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mExtraPaddingLeftRight:I

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->getPaddingBottom()I

    move-result v8

    invoke-virtual {v13, v4, v6, v7, v8}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->setPadding(IIII)V

    .line 138
    const/4 v15, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mUrl:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AntennaFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v18, v0

    move-object v14, v5

    move-object/from16 v17, p1

    move-object/from16 v19, p0

    invoke-virtual/range {v13 .. v19}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->bind(Lcom/google/android/finsky/api/model/Document;ZLjava/lang/String;Landroid/os/Bundle;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 140
    :cond_4
    return-void
.end method

.method protected recordState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AntennaFragment;->getView()Landroid/view/View;

    move-result-object v2

    .line 182
    .local v2, "view":Landroid/view/View;
    if-nez v2, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    const v3, 0x7f0a00cb

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 187
    .local v0, "descriptionPanel":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 191
    :cond_2
    const v3, 0x7f0a00cd

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;

    .line 193
    .local v1, "secondaryActionsPanel":Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;
    if-eqz v1, :cond_0

    .line 194
    invoke-virtual {v1, p1}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->onSavedInstanceState(Landroid/os/Bundle;)V

    goto :goto_0
.end method
