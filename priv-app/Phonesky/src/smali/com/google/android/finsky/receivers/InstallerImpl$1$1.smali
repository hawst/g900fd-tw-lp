.class Lcom/google/android/finsky/receivers/InstallerImpl$1$1;
.super Ljava/lang/Object;
.source "InstallerImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/receivers/InstallerImpl$1;->onPostExecute(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/receivers/InstallerImpl$1;

.field final synthetic val$runningDownloads:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerImpl$1;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;->this$1:Lcom/google/android/finsky/receivers/InstallerImpl$1;

    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;->val$runningDownloads:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 197
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;->this$1:Lcom/google/android/finsky/receivers/InstallerImpl$1;

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;->val$runningDownloads:Ljava/util/List;

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->recoverRunningDownloads(Ljava/util/List;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$500(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/util/List;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;->this$1:Lcom/google/android/finsky/receivers/InstallerImpl$1;

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->pruneSessions()V
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$600(Lcom/google/android/finsky/receivers/InstallerImpl;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;->this$1:Lcom/google/android/finsky/receivers/InstallerImpl$1;

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # setter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mRunning:Z
    invoke-static {v0, v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$702(Lcom/google/android/finsky/receivers/InstallerImpl;Z)Z

    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;->this$1:Lcom/google/android/finsky/receivers/InstallerImpl$1;

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->kick(Z)V
    invoke-static {v0, v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$800(Lcom/google/android/finsky/receivers/InstallerImpl;Z)V

    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;->this$1:Lcom/google/android/finsky/receivers/InstallerImpl$1;

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->val$afterStartedRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 203
    return-void
.end method
