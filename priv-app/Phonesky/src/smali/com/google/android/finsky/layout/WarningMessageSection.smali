.class public Lcom/google/android/finsky/layout/WarningMessageSection;
.super Landroid/widget/LinearLayout;
.source "WarningMessageSection.java"

# interfaces
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoBottomSeparator;
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoTopSeparator;


# instance fields
.field private mDetailsWarningInfoFirstLineText:Landroid/widget/TextView;

.field private mDetailsWarningInfoIcon:Landroid/widget/ImageView;

.field private mDetailsWarningInfoSecondLineText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method private getAlternateAccountOwnerText(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/AccountLibrary;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/DfeToc;)Ljava/lang/CharSequence;
    .locals 10
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "accountLibrary"    # Lcom/google/android/finsky/library/AccountLibrary;
    .param p3, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p4, "currentAccount"    # Landroid/accounts/Account;
    .param p5, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    const v9, 0x7f0c0311

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 151
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v5

    if-ne v5, v7, :cond_0

    move-object v5, v6

    .line 179
    :goto_0
    return-object v5

    .line 156
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v5, v6

    .line 157
    goto :goto_0

    .line 159
    :cond_1
    invoke-static {p1, p3}, Lcom/google/android/finsky/utils/LibraryUtils;->getFirstOwner(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;)Landroid/accounts/Account;

    move-result-object v2

    .line 160
    .local v2, "owner":Landroid/accounts/Account;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/WarningMessageSection;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 161
    .local v0, "context":Landroid/content/Context;
    if-eqz v2, :cond_2

    .line 162
    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {v0, v9, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    goto :goto_0

    .line 164
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 165
    invoke-static {p1, p5, p3}, Lcom/google/android/finsky/utils/DocUtils;->getSubscriptions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Ljava/util/List;

    move-result-object v4

    .line 166
    .local v4, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-static {v4, p3, p4}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Ljava/util/List;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v5

    if-eqz v5, :cond_3

    move-object v5, v6

    .line 168
    goto :goto_0

    .line 171
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_5

    .line 172
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/api/model/Document;

    invoke-static {v5, p3}, Lcom/google/android/finsky/utils/LibraryUtils;->getFirstOwner(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;)Landroid/accounts/Account;

    move-result-object v3

    .line 173
    .local v3, "subscriptionOwner":Landroid/accounts/Account;
    if-eqz v3, :cond_4

    .line 174
    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {v0, v9, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    goto :goto_0

    .line 171
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v1    # "i":I
    .end local v3    # "subscriptionOwner":Landroid/accounts/Account;
    .end local v4    # "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    :cond_5
    move-object v5, v6

    .line 179
    goto :goto_0
.end method

.method private hasLicenseTerm(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 5
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 140
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 141
    .local v3, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget-object v4, v3, Lcom/google/android/finsky/protos/Common$Offer;->licenseTerms:Lcom/google/android/finsky/protos/Common$LicenseTerms;

    if-eqz v4, :cond_0

    .line 142
    const/4 v4, 0x1

    .line 145
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :goto_1
    return v4

    .line 140
    .restart local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 145
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)V
    .locals 25
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p3, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p4, "currentAccount"    # Landroid/accounts/Account;

    .prologue
    .line 76
    invoke-virtual/range {p3 .. p4}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v7

    .line 77
    .local v7, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v7}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v19

    .line 78
    .local v19, "isAvailable":Z
    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/google/android/finsky/utils/LibraryUtils;->hasApplicableVouchers(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/AccountLibrary;)Z

    move-result v16

    .line 79
    .local v16, "hasApplicableVoucher":Z
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasWarningMessage()Z

    move-result v18

    .line 80
    .local v18, "hasWarningMessage":Z
    invoke-direct/range {p0 .. p1}, Lcom/google/android/finsky/layout/WarningMessageSection;->hasLicenseTerm(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v17

    .local v17, "hasLicenseInfo":Z
    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p2

    .line 81
    invoke-direct/range {v5 .. v10}, Lcom/google/android/finsky/layout/WarningMessageSection;->getAlternateAccountOwnerText(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/AccountLibrary;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/DfeToc;)Ljava/lang/CharSequence;

    move-result-object v11

    .line 84
    .local v11, "accountOwnerWarning":Ljava/lang/CharSequence;
    if-eqz v19, :cond_0

    if-nez v17, :cond_0

    if-nez v18, :cond_0

    if-nez v16, :cond_0

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 86
    const/16 v5, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/WarningMessageSection;->setVisibility(I)V

    .line 137
    :goto_0
    return-void

    .line 90
    :cond_0
    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/WarningMessageSection;->setVisibility(I)V

    .line 91
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoSecondLineText:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 93
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v12

    .line 94
    .local v12, "backend":I
    if-nez v19, :cond_1

    .line 95
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoFirstLineText:Landroid/widget/TextView;

    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/utils/DocUtils;->getAvailabilityRestrictionResourceId(Lcom/google/android/finsky/api/model/Document;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 97
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoIcon:Landroid/widget/ImageView;

    invoke-static {v12}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getWarningDrawable(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 117
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/WarningMessageSection;->getContext()Landroid/content/Context;

    move-result-object v15

    .line 119
    .local v15, "context":Landroid/content/Context;
    invoke-static {v15, v12}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v24

    .line 121
    .local v24, "secondaryTextColor":Landroid/content/res/ColorStateList;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoFirstLineText:Landroid/widget/TextView;

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 122
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoSecondLineText:Landroid/widget/TextView;

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 125
    invoke-static {v15, v12}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v13

    .line 126
    .local v13, "backendFill":I
    const/4 v5, -0x1

    const v6, 0x3e19999a    # 0.15f

    invoke-static {v13, v5, v6}, Lcom/google/android/finsky/utils/UiUtils;->interpolateColor(IIF)I

    move-result v14

    .line 128
    .local v14, "blendedFill":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/WarningMessageSection;->getPaddingTop()I

    move-result v23

    .line 129
    .local v23, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/WarningMessageSection;->getPaddingBottom()I

    move-result v20

    .line 130
    .local v20, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/WarningMessageSection;->getPaddingRight()I

    move-result v22

    .line 131
    .local v22, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/WarningMessageSection;->getPaddingLeft()I

    move-result v21

    .line 132
    .local v21, "paddingLeft":I
    new-instance v5, Landroid/graphics/drawable/LayerDrawable;

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/graphics/drawable/Drawable;

    const/4 v8, 0x0

    new-instance v9, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v9, v14}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v9, v6, v8

    const/4 v8, 0x1

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02017e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    aput-object v9, v6, v8

    invoke-direct {v5, v6}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/layout/WarningMessageSection;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 136
    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v23

    move/from16 v3, v22

    move/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/WarningMessageSection;->setPadding(IIII)V

    goto/16 :goto_0

    .line 99
    .end local v13    # "backendFill":I
    .end local v14    # "blendedFill":I
    .end local v15    # "context":Landroid/content/Context;
    .end local v20    # "paddingBottom":I
    .end local v21    # "paddingLeft":I
    .end local v22    # "paddingRight":I
    .end local v23    # "paddingTop":I
    .end local v24    # "secondaryTextColor":Landroid/content/res/ColorStateList;
    :cond_1
    if-eqz v17, :cond_2

    .line 100
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoFirstLineText:Landroid/widget/TextView;

    const v6, 0x7f0c0409

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 101
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoIcon:Landroid/widget/ImageView;

    const v6, 0x7f0200cb

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 102
    :cond_2
    if-eqz v18, :cond_3

    .line 103
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoFirstLineText:Landroid/widget/TextView;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getWarningMessage()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoFirstLineText:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 105
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoIcon:Landroid/widget/ImageView;

    invoke-static {v12}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getWarningDrawable(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 107
    :cond_3
    if-eqz v16, :cond_4

    .line 108
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoFirstLineText:Landroid/widget/TextView;

    const v6, 0x7f0c0108

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(I)V

    .line 109
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoIcon:Landroid/widget/ImageView;

    invoke-static {v12}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getWarningDrawable(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 112
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoFirstLineText:Landroid/widget/TextView;

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoIcon:Landroid/widget/ImageView;

    invoke-static {v12}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getWarningDrawable(I)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 67
    const v0, 0x7f0a01a5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/WarningMessageSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoFirstLineText:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0a01a6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/WarningMessageSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoSecondLineText:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0a01a4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/WarningMessageSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/WarningMessageSection;->mDetailsWarningInfoIcon:Landroid/widget/ImageView;

    .line 72
    return-void
.end method
