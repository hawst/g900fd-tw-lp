.class public Lcom/google/android/finsky/library/Libraries;
.super Ljava/lang/Object;
.source "Libraries.java"

# interfaces
.implements Lcom/google/android/finsky/library/Library;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/library/Libraries$Listener;
    }
.end annotation


# instance fields
.field private final mAccounts:Lcom/google/android/finsky/library/Accounts;

.field private final mBackgroundHandler:Landroid/os/Handler;

.field private mCurrentAccounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation
.end field

.field private final mLibraries:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/finsky/library/AccountLibrary;",
            ">;"
        }
    .end annotation
.end field

.field private mLibraryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/library/AccountLibrary;",
            ">;"
        }
    .end annotation
.end field

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/library/Libraries$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadHasBeenCalled:Z

.field private mLoadedAccountHash:I

.field private final mLoadingCallbacks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotificationHandler:Landroid/os/Handler;

.field private final mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/library/Accounts;Lcom/google/android/finsky/library/SQLiteLibrary;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 1
    .param p1, "accounts"    # Lcom/google/android/finsky/library/Accounts;
    .param p2, "sqLiteLibrary"    # Lcom/google/android/finsky/library/SQLiteLibrary;
    .param p3, "notificationHandler"    # Landroid/os/Handler;
    .param p4, "backgroundHandler"    # Landroid/os/Handler;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mCurrentAccounts:Ljava/util/List;

    .line 59
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    .line 62
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadingCallbacks:Ljava/util/List;

    .line 64
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mListeners:Ljava/util/List;

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadHasBeenCalled:Z

    .line 74
    iput-object p1, p0, Lcom/google/android/finsky/library/Libraries;->mAccounts:Lcom/google/android/finsky/library/Accounts;

    .line 75
    iput-object p2, p0, Lcom/google/android/finsky/library/Libraries;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    .line 76
    iput-object p4, p0, Lcom/google/android/finsky/library/Libraries;->mBackgroundHandler:Landroid/os/Handler;

    .line 77
    iput-object p3, p0, Lcom/google/android/finsky/library/Libraries;->mNotificationHandler:Landroid/os/Handler;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/library/Libraries;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/finsky/library/Libraries;->fireAllLibrariesLoaded()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/library/Libraries;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/finsky/library/Libraries;->runAndClearLoadingCallbacks()V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/finsky/library/Libraries;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/library/Libraries;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/google/android/finsky/library/Libraries;->mLoadedAccountHash:I

    return p1
.end method

.method static synthetic access$300(Lcom/google/android/finsky/library/Libraries;)Lcom/google/android/finsky/library/SQLiteLibrary;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/library/Libraries;)Lcom/google/android/finsky/library/Accounts;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mAccounts:Lcom/google/android/finsky/library/Accounts;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/library/Libraries;
    .param p1, "x1"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/finsky/library/Libraries;->notifyLibraryChanged(Lcom/google/android/finsky/library/AccountLibrary;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/library/Libraries;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mListeners:Ljava/util/List;

    return-object v0
.end method

.method private computeAccountHash(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/accounts/Account;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 243
    .local p1, "accounts":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    const/4 v2, 0x0

    .line 244
    .local v2, "result":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 245
    .local v0, "account":Landroid/accounts/Account;
    invoke-virtual {v0}, Landroid/accounts/Account;->hashCode()I

    move-result v3

    add-int/2addr v2, v3

    .line 246
    goto :goto_0

    .line 247
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    return v2
.end method

.method private fireAllLibrariesLoaded()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mNotificationHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/library/Libraries$6;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/library/Libraries$6;-><init>(Lcom/google/android/finsky/library/Libraries;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 285
    return-void
.end method

.method private notifyLibraryChanged(Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 2
    .param p1, "library"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mNotificationHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/library/Libraries$5;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/library/Libraries$5;-><init>(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/library/AccountLibrary;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 272
    return-void
.end method

.method private declared-synchronized runAndClearLoadingCallbacks()V
    .locals 3

    .prologue
    .line 288
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/library/Libraries;->mLoadingCallbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 289
    .local v1, "loadingCallback":Ljava/lang/Runnable;
    if-eqz v1, :cond_0

    .line 290
    iget-object v2, p0, Lcom/google/android/finsky/library/Libraries;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 288
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "loadingCallback":Ljava/lang/Runnable;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 293
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/finsky/library/Libraries;->mLoadingCallbacks:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294
    monitor-exit p0

    return-void
.end method

.method private setupAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;
    .locals 2
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 251
    new-instance v0, Lcom/google/android/finsky/library/AccountLibrary;

    iget-object v1, p0, Lcom/google/android/finsky/library/Libraries;->mNotificationHandler:Landroid/os/Handler;

    invoke-direct {v0, p1, v1}, Lcom/google/android/finsky/library/AccountLibrary;-><init>(Landroid/accounts/Account;Landroid/os/Handler;)V

    .line 252
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    new-instance v1, Lcom/google/android/finsky/library/Libraries$4;

    invoke-direct {v1, p0, v0}, Lcom/google/android/finsky/library/Libraries$4;-><init>(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/library/AccountLibrary;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/AccountLibrary;->addListener(Lcom/google/android/finsky/library/AccountLibrary$Listener;)V

    .line 258
    return-object v0
.end method


# virtual methods
.method public declared-synchronized addListener(Lcom/google/android/finsky/library/Libraries$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/finsky/library/Libraries$Listener;

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    monitor-exit p0

    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public blockingLoad()V
    .locals 4

    .prologue
    .line 117
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/library/Libraries;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v2, v3, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/library/Libraries;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v2, v3, :cond_1

    .line 119
    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    .line 122
    :cond_1
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 123
    .local v0, "countDownLatch":Ljava/util/concurrent/CountDownLatch;
    new-instance v2, Lcom/google/android/finsky/library/Libraries$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/finsky/library/Libraries$1;-><init>(Lcom/google/android/finsky/library/Libraries;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/library/Libraries;->load(Ljava/lang/Runnable;)V

    .line 130
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    return-void

    .line 131
    :catch_0
    move-exception v1

    .line 132
    .local v1, "e":Ljava/lang/InterruptedException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public declared-synchronized cleanup()V
    .locals 2

    .prologue
    .line 232
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/library/Libraries$3;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/library/Libraries$3;-><init>(Lcom/google/android/finsky/library/Libraries;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    monitor-exit p0

    return-void

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized contains(Lcom/google/android/finsky/library/LibraryEntry;)Z
    .locals 4
    .param p1, "key"    # Lcom/google/android/finsky/library/LibraryEntry;

    .prologue
    .line 306
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 307
    .local v2, "libraryCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 308
    iget-object v3, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/AccountLibrary;

    .line 309
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/library/AccountLibrary;->contains(Lcom/google/android/finsky/library/LibraryEntry;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_0

    .line 310
    const/4 v3, 0x1

    .line 313
    .end local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :goto_1
    monitor-exit p0

    return v3

    .line 307
    .restart local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 313
    .end local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 306
    .end local v1    # "i":I
    .end local v2    # "libraryCount":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public dumpState()V
    .locals 4

    .prologue
    .line 440
    const-string v2, "FinskyLibrary"

    const-string v3, "| Libraries {"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    iget-object v2, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/AccountLibrary;

    .line 442
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    const-string v2, "|   "

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/library/AccountLibrary;->dumpState(Ljava/lang/String;)V

    goto :goto_0

    .line 444
    .end local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_0
    const-string v2, "FinskyLibrary"

    const-string v3, "| }"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    return-void
.end method

.method public declared-synchronized get(Lcom/google/android/finsky/library/LibraryEntry;)Lcom/google/android/finsky/library/LibraryEntry;
    .locals 5
    .param p1, "key"    # Lcom/google/android/finsky/library/LibraryEntry;

    .prologue
    .line 318
    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 319
    .local v3, "libraryCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 320
    iget-object v4, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/AccountLibrary;

    .line 321
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/library/AccountLibrary;->get(Lcom/google/android/finsky/library/LibraryEntry;)Lcom/google/android/finsky/library/LibraryEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 322
    .local v1, "entry":Lcom/google/android/finsky/library/LibraryEntry;
    if-eqz v1, :cond_0

    .line 326
    .end local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .end local v1    # "entry":Lcom/google/android/finsky/library/LibraryEntry;
    :goto_1
    monitor-exit p0

    return-object v1

    .line 319
    .restart local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .restart local v1    # "entry":Lcom/google/android/finsky/library/LibraryEntry;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 326
    .end local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .end local v1    # "entry":Lcom/google/android/finsky/library/LibraryEntry;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 318
    .end local v2    # "i":I
    .end local v3    # "libraryCount":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized getAccountLibraries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/library/AccountLibrary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 297
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;
    .locals 1
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 301
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/AccountLibrary;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAppEntries(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "certificateHashes"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/library/LibraryAppEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 374
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 375
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryAppEntry;>;"
    iget-object v5, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 376
    .local v3, "libraryCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 377
    iget-object v5, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/AccountLibrary;

    .line 378
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/library/AccountLibrary;->getAppEntry(Ljava/lang/String;)Lcom/google/android/finsky/library/LibraryAppEntry;

    move-result-object v1

    .line 379
    .local v1, "appEntry":Lcom/google/android/finsky/library/LibraryAppEntry;
    if-eqz v1, :cond_0

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/library/LibraryAppEntry;->hasMatchingCertificateHash([Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 380
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 376
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 383
    .end local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .end local v1    # "appEntry":Lcom/google/android/finsky/library/LibraryAppEntry;
    :cond_1
    monitor-exit p0

    return-object v4

    .line 374
    .end local v2    # "i":I
    .end local v3    # "libraryCount":I
    .end local v4    # "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryAppEntry;>;"
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method public declared-synchronized getAppOwners(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation

    .prologue
    .line 363
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/finsky/library/LibraryAppEntry;->ANY_CERTIFICATE_HASHES:[Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAppOwners(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "certificateHashes"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/accounts/Account;",
            ">;"
        }
    .end annotation

    .prologue
    .line 337
    monitor-enter p0

    const/4 v5, 0x0

    .line 338
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    :try_start_0
    iget-object v6, p0, Lcom/google/android/finsky/library/Libraries;->mCurrentAccounts:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    .line 339
    .local v1, "accountCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_2

    .line 340
    iget-object v6, p0, Lcom/google/android/finsky/library/Libraries;->mCurrentAccounts:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 341
    .local v0, "account":Landroid/accounts/Account;
    iget-object v6, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/library/AccountLibrary;

    .line 342
    .local v2, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-virtual {v2, p1}, Lcom/google/android/finsky/library/AccountLibrary;->getAppEntry(Ljava/lang/String;)Lcom/google/android/finsky/library/LibraryAppEntry;

    move-result-object v3

    .line 343
    .local v3, "appEntry":Lcom/google/android/finsky/library/LibraryAppEntry;
    if-eqz v3, :cond_1

    invoke-virtual {v3, p2}, Lcom/google/android/finsky/library/LibraryAppEntry;->hasMatchingCertificateHash([Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 344
    if-nez v5, :cond_0

    .line 345
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 347
    :cond_0
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 350
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v2    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .end local v3    # "appEntry":Lcom/google/android/finsky/library/LibraryAppEntry;
    :cond_2
    if-eqz v5, :cond_3

    .end local v5    # "result":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    :goto_1
    monitor-exit p0

    return-object v5

    .restart local v5    # "result":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    :cond_3
    :try_start_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    goto :goto_1

    .line 337
    .end local v1    # "accountCount":I
    .end local v4    # "i":I
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6
.end method

.method public declared-synchronized getLoadedAccountHash()I
    .locals 1

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadedAccountHash:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasSubscriptions()Z
    .locals 4

    .prologue
    .line 390
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    .line 391
    .local v2, "libraryCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 392
    iget-object v3, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/AccountLibrary;

    .line 393
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-virtual {v0}, Lcom/google/android/finsky/library/AccountLibrary;->getInAppSubscriptionsList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_0

    .line 394
    const/4 v3, 0x1

    .line 397
    .end local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :goto_1
    monitor-exit p0

    return v3

    .line 391
    .restart local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 397
    .end local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 390
    .end local v1    # "i":I
    .end local v2    # "libraryCount":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized isLoaded()Z
    .locals 1

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadHasBeenCalled:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/finsky/library/LibraryEntry;",
            ">;"
        }
    .end annotation

    .prologue
    .line 433
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public declared-synchronized load(Ljava/lang/Runnable;)V
    .locals 13
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    const/4 v11, 0x1

    .line 143
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadHasBeenCalled:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadedAccountHash:I

    iget-object v1, p0, Lcom/google/android/finsky/library/Libraries;->mAccounts:Lcom/google/android/finsky/library/Accounts;

    invoke-interface {v1}, Lcom/google/android/finsky/library/Accounts;->getAccounts()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/library/Libraries;->computeAccountHash(Ljava/util/List;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 145
    if-eqz p1, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 151
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadingCallbacks:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadingCallbacks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v11, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mAccounts:Lcom/google/android/finsky/library/Accounts;

    invoke-interface {v0}, Lcom/google/android/finsky/library/Accounts;->getAccounts()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mCurrentAccounts:Ljava/util/List;

    .line 158
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mCurrentAccounts:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/library/Libraries;->computeAccountHash(Ljava/util/List;)I

    move-result v5

    .line 162
    .local v5, "accountsHash":I
    const/4 v8, 0x0

    .line 163
    .local v8, "librariesToUnload":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    .line 164
    .local v2, "account":Landroid/accounts/Account;
    if-nez v8, :cond_3

    .line 165
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 167
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mCurrentAccounts:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 168
    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 143
    .end local v2    # "account":Landroid/accounts/Account;
    .end local v5    # "accountsHash":I
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "librariesToUnload":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 171
    .restart local v5    # "accountsHash":I
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v8    # "librariesToUnload":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    :cond_4
    if-eqz v8, :cond_5

    .line 172
    :try_start_2
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    .line 173
    .restart local v2    # "account":Landroid/accounts/Account;
    const-string v0, "Unloading AccountLibrary for account: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v12}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v1, v11

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 180
    .end local v2    # "account":Landroid/accounts/Account;
    :cond_5
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 181
    .local v4, "librariesToLoad":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mCurrentAccounts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    .line 182
    .restart local v2    # "account":Landroid/accounts/Account;
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 183
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    invoke-direct {p0, v2}, Lcom/google/android/finsky/library/Libraries;->setupAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v6

    .line 185
    .local v6, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    invoke-interface {v0, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 190
    .end local v2    # "account":Landroid/accounts/Account;
    .end local v6    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    .line 193
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_8

    .line 194
    invoke-direct {p0}, Lcom/google/android/finsky/library/Libraries;->fireAllLibrariesLoaded()V

    .line 195
    invoke-direct {p0}, Lcom/google/android/finsky/library/Libraries;->runAndClearLoadingCallbacks()V

    .line 196
    iput v5, p0, Lcom/google/android/finsky/library/Libraries;->mLoadedAccountHash:I

    .line 197
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadHasBeenCalled:Z

    goto/16 :goto_0

    .line 201
    :cond_8
    const/4 v0, 0x1

    new-array v3, v0, [I

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput v1, v3, v0

    .line 202
    .local v3, "loadedLibrariesCount":[I
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    .line 203
    .restart local v2    # "account":Landroid/accounts/Account;
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mLibraries:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/finsky/library/AccountLibrary;

    .line 204
    .local v9, "library":Lcom/google/android/finsky/library/AccountLibrary;
    new-instance v10, Lcom/google/android/finsky/library/LibraryLoader;

    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    iget-object v1, p0, Lcom/google/android/finsky/library/Libraries;->mNotificationHandler:Landroid/os/Handler;

    iget-object v11, p0, Lcom/google/android/finsky/library/Libraries;->mBackgroundHandler:Landroid/os/Handler;

    invoke-direct {v10, v0, v9, v1, v11}, Lcom/google/android/finsky/library/LibraryLoader;-><init>(Lcom/google/android/finsky/library/SQLiteLibrary;Lcom/google/android/finsky/library/AccountLibrary;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 206
    .local v10, "loader":Lcom/google/android/finsky/library/LibraryLoader;
    new-instance v0, Lcom/google/android/finsky/library/Libraries$2;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/library/Libraries$2;-><init>(Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;[ILjava/util/List;I)V

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/library/LibraryLoader;->load(Ljava/lang/Runnable;)V

    goto :goto_4

    .line 223
    .end local v2    # "account":Landroid/accounts/Account;
    .end local v9    # "library":Lcom/google/android/finsky/library/AccountLibrary;
    .end local v10    # "loader":Lcom/google/android/finsky/library/LibraryLoader;
    :cond_9
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/library/Libraries;->mLoadHasBeenCalled:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public remove(Lcom/google/android/finsky/library/LibraryEntry;)V
    .locals 1
    .param p1, "key"    # Lcom/google/android/finsky/library/LibraryEntry;

    .prologue
    .line 412
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public declared-synchronized removeListener(Lcom/google/android/finsky/library/Libraries$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/finsky/library/Libraries$Listener;

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/library/Libraries;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    monitor-exit p0

    return-void

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized size()I
    .locals 5

    .prologue
    .line 417
    monitor-enter p0

    const/4 v3, 0x0

    .line 418
    .local v3, "size":I
    :try_start_0
    iget-object v4, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 419
    .local v2, "libraryCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 420
    iget-object v4, p0, Lcom/google/android/finsky/library/Libraries;->mLibraryList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/AccountLibrary;

    .line 421
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-virtual {v0}, Lcom/google/android/finsky/library/AccountLibrary;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    add-int/2addr v3, v4

    .line 419
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 423
    .end local v0    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_0
    monitor-exit p0

    return v3

    .line 417
    .end local v1    # "i":I
    .end local v2    # "libraryCount":I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method
