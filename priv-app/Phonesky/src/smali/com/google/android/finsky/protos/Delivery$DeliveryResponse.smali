.class public final Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Delivery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Delivery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeliveryResponse"
.end annotation


# instance fields
.field public appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

.field public hasStatus:Z

.field public status:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 41
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->clear()Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    .line 42
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->status:I

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->hasStatus:Z

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->cachedSize:I

    .line 49
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 66
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 67
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->status:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->hasStatus:Z

    if-eqz v1, :cond_1

    .line 68
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->status:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 71
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v1, :cond_2

    .line 72
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 84
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 88
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 89
    :sswitch_0
    return-object p0

    .line 94
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 95
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 102
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->status:I

    .line 103
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->hasStatus:Z

    goto :goto_0

    .line 109
    .end local v1    # "value":I
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-nez v2, :cond_1

    .line 110
    new-instance v2, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 112
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 84
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    .line 95
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 55
    iget v0, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->status:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->hasStatus:Z

    if-eqz v0, :cond_1

    .line 56
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->status:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v0, :cond_2

    .line 59
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 61
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 62
    return-void
.end method
