.class public Lcom/google/android/finsky/layout/SuggestionBarLayout;
.super Lcom/google/android/finsky/layout/IdentifiableRelativeLayout;
.source "SuggestionBarLayout.java"


# instance fields
.field private mFitsInOneLine:Z

.field private mHeaderHeight:I

.field private mLayoutHeight:I

.field private mSuggestionBarUnderlinePadding:I

.field private mSuggestionBarVerticalPadding:I

.field private mSuggestionLine1:Landroid/widget/TextView;

.field private mSuggestionLine2:Landroid/widget/TextView;

.field private mSuggestionLineFull:Landroid/widget/LinearLayout;

.field private mSuggestionLineQuery:Landroid/widget/TextView;

.field private mSuggestionLineText:Landroid/widget/TextView;

.field private mSuggestionUnderline:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b012d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionBarUnderlinePadding:I

    .line 58
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b012e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionBarVerticalPadding:I

    .line 60
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b013a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mHeaderHeight:I

    .line 61
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Lcom/google/android/finsky/layout/IdentifiableRelativeLayout;->onFinishInflate()V

    .line 67
    const v0, 0x7f0a03a6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineFull:Landroid/widget/LinearLayout;

    .line 68
    const v0, 0x7f0a03a7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineText:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0a03a8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineQuery:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0a03a9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine1:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0a03aa

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0a03ab

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionUnderline:Landroid/view/View;

    .line 73
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 18
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 135
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->getWidth()I

    move-result v11

    .line 136
    .local v11, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->getHeight()I

    move-result v1

    .line 138
    .local v1, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->getPaddingLeft()I

    move-result v12

    .line 140
    .local v12, "xText":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineFull:Landroid/widget/LinearLayout;

    invoke-virtual {v13}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v5

    .line 141
    .local v5, "suggestionLineFullWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine1:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    .line 142
    .local v3, "suggestionLine1Width":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    .line 143
    .local v4, "suggestionLine2Width":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionUnderline:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    sub-int v13, v1, v13

    div-int/lit8 v2, v13, 0x2

    .line 145
    .local v2, "middleY":I
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mFitsInOneLine:Z

    if-eqz v13, :cond_0

    .line 146
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineFull:Landroid/widget/LinearLayout;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 147
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine1:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 148
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineFull:Landroid/widget/LinearLayout;

    invoke-virtual {v13}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v6

    .line 151
    .local v6, "textHeight":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineFull:Landroid/widget/LinearLayout;

    div-int/lit8 v14, v6, 0x2

    sub-int v14, v2, v14

    add-int v15, v12, v5

    div-int/lit8 v16, v6, 0x2

    add-int v16, v16, v2

    move/from16 v0, v16

    invoke-virtual {v13, v12, v14, v15, v0}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 170
    .end local v6    # "textHeight":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionUnderline:Landroid/view/View;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionBarUnderlinePadding:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionUnderline:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredHeight()I

    move-result v16

    sub-int v15, v15, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionBarUnderlinePadding:I

    move/from16 v16, v0

    sub-int v16, v11, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    move/from16 v17, v0

    invoke-virtual/range {v13 .. v17}, Landroid/view/View;->layout(IIII)V

    .line 174
    return-void

    .line 155
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineFull:Landroid/widget/LinearLayout;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 156
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine1:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 157
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine1:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    .line 159
    .local v7, "textHeight1":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    invoke-virtual {v13}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    .line 162
    .local v8, "textHeight2":I
    add-int v13, v7, v8

    div-int/lit8 v13, v13, 0x2

    sub-int v9, v2, v13

    .line 163
    .local v9, "textTop1":I
    add-int v10, v9, v7

    .line 164
    .local v10, "textTop2":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine1:Landroid/widget/TextView;

    add-int v14, v12, v3

    add-int v15, v9, v7

    invoke-virtual {v13, v12, v9, v14, v15}, Landroid/widget/TextView;->layout(IIII)V

    .line 166
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    add-int v14, v12, v4

    add-int v15, v10, v8

    invoke-virtual {v13, v12, v10, v14, v15}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v6, 0x0

    .line 92
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 93
    .local v3, "paddedWidth":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->getPaddingLeft()I

    move-result v1

    .line 94
    .local v1, "leftPadding":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->getPaddingRight()I

    move-result v4

    .line 95
    .local v4, "rightPadding":I
    sub-int v7, v3, v1

    sub-int v0, v7, v4

    .line 100
    .local v0, "availableTextWidth":I
    const/high16 v7, -0x80000000

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 102
    .local v5, "textViewWidthSpec":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineFull:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v6, v6}, Landroid/widget/LinearLayout;->measure(II)V

    .line 103
    iget-object v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine1:Landroid/widget/TextView;

    invoke-virtual {v7, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 104
    iget-object v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    invoke-virtual {v7, v5, v6}, Landroid/widget/TextView;->measure(II)V

    .line 105
    iget-object v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionUnderline:Landroid/view/View;

    iget-object v8, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionUnderline:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v6, v8}, Landroid/view/View;->measure(II)V

    .line 110
    iget-object v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineFull:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v2

    .line 112
    .local v2, "lineFullWidth":I
    if-gt v2, v0, :cond_0

    const/4 v6, 0x1

    :cond_0
    iput-boolean v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mFitsInOneLine:Z

    .line 115
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mFitsInOneLine:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineFull:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v6

    :goto_0
    iput v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    .line 119
    iget v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    iget v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionBarVerticalPadding:I

    mul-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    iput v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    .line 122
    iget v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    iget-object v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionUnderline:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    iput v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    .line 125
    iget v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    iget v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mHeaderHeight:I

    if-ge v6, v7, :cond_1

    .line 126
    iget v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mHeaderHeight:I

    iput v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    .line 129
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    iget v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mLayoutHeight:I

    invoke-virtual {p0, v6, v7}, Lcom/google/android/finsky/layout/SuggestionBarLayout;->setMeasuredDimension(II)V

    .line 131
    return-void

    .line 115
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine1:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v6, v7

    goto :goto_0
.end method

.method public setDisplayLine(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-static {p2}, Lcom/google/android/finsky/utils/Utils;->getItalicSafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "formattedQuery":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine1:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v1, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v1, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLine2:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 84
    iget-object v1, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineText:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object v1, p0, Lcom/google/android/finsky/layout/SuggestionBarLayout;->mSuggestionLineQuery:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method
