.class public Lcom/google/android/finsky/activities/FlagItemFragment;
.super Lcom/google/android/finsky/fragments/UrlBasedPageFragment;
.source "FlagItemFragment.java"

# interfaces
.implements Lcom/google/android/finsky/activities/FlagItemUserMessageDialog$Listener;
.implements Lcom/google/android/finsky/layout/ButtonBar$ClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;,
        Lcom/google/android/finsky/activities/FlagItemFragment$AppFlagType;,
        Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;
    }
.end annotation


# instance fields
.field private mDetailsPanel:Landroid/view/ViewGroup;

.field private mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mFlagMessage:Ljava/lang/String;

.field private mFlagRadioButtons:Landroid/widget/RadioGroup;

.field private mSelectedRadioButtonId:I

.field private mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;-><init>()V

    .line 80
    const/16 v0, 0x12d

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 413
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/FlagItemFragment;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/FlagItemFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/FlagItemFragment;)Lcom/google/android/finsky/api/model/DfeDetails;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/FlagItemFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/FlagItemFragment;)Lcom/google/android/finsky/fragments/PageFragmentHost;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/FlagItemFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/FlagItemFragment;)Lcom/google/android/finsky/fragments/PageFragmentHost;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/FlagItemFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    return-object v0
.end method

.method private getAppFlagTypes()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 302
    iget-object v3, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v3

    iget-object v2, v3, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 303
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 304
    .local v1, "owners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    .line 305
    .local v0, "ownedApp":Z
    :goto_0
    invoke-static {v0}, Lcom/google/android/finsky/activities/FlagItemFragment$AppFlagType;->getAppFlags(Z)Ljava/util/List;

    move-result-object v3

    return-object v3

    .line 304
    .end local v0    # "ownedApp":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getFlagTypesForCurrentCorpus(I)Ljava/util/List;
    .locals 2
    .param p1, "backendId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 294
    invoke-direct {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getAppFlagTypes()Ljava/util/List;

    move-result-object v0

    .line 296
    :goto_0
    return-object v0

    .line 295
    :cond_0
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 296
    invoke-static {}, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;->getMusicFlags()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 298
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unsupported backend type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getSelectedFlagType()Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 309
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagRadioButtons:Landroid/widget/RadioGroup;

    invoke-virtual {v3}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-object v0

    .line 312
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagRadioButtons:Landroid/widget/RadioGroup;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getView()Landroid/view/View;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagRadioButtons:Landroid/widget/RadioGroup;

    invoke-virtual {v5}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RadioGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 314
    .local v2, "index":I
    iget-object v3, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/google/android/finsky/activities/FlagItemFragment;->getFlagTypesForCurrentCorpus(I)Ljava/util/List;

    move-result-object v1

    .line 315
    .local v1, "currentTypes":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 318
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;

    .line 319
    .local v0, "currentFlag":Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;
    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/finsky/activities/FlagItemFragment;
    .locals 2
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/finsky/activities/FlagItemFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/FlagItemFragment;-><init>()V

    .line 88
    .local v0, "fragment":Lcom/google/android/finsky/activities/FlagItemFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->setDfeTocAndUrl(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    .line 89
    return-object v0
.end method

.method private postFlag()V
    .locals 4

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v0}, Lcom/google/android/finsky/fragments/PageFragmentHost;->goBack()V

    .line 289
    invoke-direct {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getSelectedFlagType()Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagMessage:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;->postFlag(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)V

    .line 290
    return-void
.end method


# virtual methods
.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 152
    const v0, 0x7f040098

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 159
    if-eqz p1, :cond_0

    .line 160
    const-string v0, "flag_free_text_message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagMessage:Ljava/lang/String;

    .line 161
    const-string v0, "flag_selected_button_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "flag_selected_button_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSelectedRadioButtonId:I

    .line 164
    :cond_0
    if-eqz p1, :cond_2

    const-string v0, "doc"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    const-string v0, "doc"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/FlagItemFragment;->onDocumentLoaded(Lcom/google/android/finsky/api/model/Document;)V

    .line 191
    :goto_1
    return-void

    .line 161
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 167
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->switchToLoading()V

    .line 168
    new-instance v0, Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mUrl:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 169
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    new-instance v1, Lcom/google/android/finsky/activities/FlagItemFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/FlagItemFragment$2;-><init>(Lcom/google/android/finsky/activities/FlagItemFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    new-instance v1, Lcom/google/android/finsky/activities/FlagItemFragment$3;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/FlagItemFragment$3;-><init>(Lcom/google/android/finsky/activities/FlagItemFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->onDestroyView()V

    .line 97
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onDestroyView()V

    .line 98
    return-void
.end method

.method public onDocumentLoaded(Lcom/google/android/finsky/api/model/Document;)V
    .locals 13
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 194
    iput-object p1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 195
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->switchToData()V

    .line 197
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 199
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    if-nez v0, :cond_0

    .line 200
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v2}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/activities/BinderFactory;->getSummaryViewBinder(Lcom/google/android/finsky/api/model/DfeToc;ILandroid/accounts/Account;)Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setDynamicFeaturesVisibility(Z)V

    .line 207
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setCompactMode(Z)V

    .line 208
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v3, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object v4, p0

    move-object v7, v6

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/fragments/PageFragment;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagRadioButtons:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 213
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    .line 214
    .local v11, "inflater":Landroid/view/LayoutInflater;
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getFlagTypesForCurrentCorpus(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;

    .line 215
    .local v12, "type":Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;
    const v0, 0x7f04016c

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagRadioButtons:Landroid/widget/RadioGroup;

    invoke-virtual {v11, v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RadioButton;

    .line 217
    .local v9, "button":Landroid/widget/RadioButton;
    iget v0, v12, Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;->stringId:I

    invoke-virtual {v9, v0}, Landroid/widget/RadioButton;->setText(I)V

    .line 218
    invoke-virtual {v9, v12}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagRadioButtons:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v9}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 220
    iget v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSelectedRadioButtonId:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSelectedRadioButtonId:I

    iget v1, v12, Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;->stringId:I

    if-ne v0, v1, :cond_1

    .line 221
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagRadioButtons:Landroid/widget/RadioGroup;

    invoke-virtual {v9}, Landroid/widget/RadioButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0

    .line 226
    .end local v9    # "button":Landroid/widget/RadioButton;
    .end local v12    # "type":Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 227
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->onDataChanged()V

    .line 228
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 4

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 103
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0a0175

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDetailsPanel:Landroid/view/ViewGroup;

    .line 105
    const v2, 0x7f0a01db

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioGroup;

    iput-object v2, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagRadioButtons:Landroid/widget/RadioGroup;

    .line 107
    const v2, 0x7f0a0114

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ButtonBar;

    .line 108
    .local v0, "buttonBar":Lcom/google/android/finsky/layout/ButtonBar;
    const v2, 0x7f0c0307

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonTitle(I)V

    .line 109
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonEnabled(Z)V

    .line 110
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/ButtonBar;->setClickListener(Lcom/google/android/finsky/layout/ButtonBar$ClickListener;)V

    .line 112
    iget-object v2, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagRadioButtons:Landroid/widget/RadioGroup;

    new-instance v3, Lcom/google/android/finsky/activities/FlagItemFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/finsky/activities/FlagItemFragment$1;-><init>(Lcom/google/android/finsky/activities/FlagItemFragment;Lcom/google/android/finsky/layout/ButtonBar;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 118
    return-void
.end method

.method public onNegativeButtonClick()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v0}, Lcom/google/android/finsky/fragments/PageFragmentHost;->goBack()V

    .line 144
    return-void
.end method

.method public onPositiveButtonClick()V
    .locals 4

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getSelectedFlagType()Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;

    move-result-object v0

    .line 123
    .local v0, "currentFlag":Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;
    if-nez v0, :cond_1

    .line 139
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;->requireUserComment()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 128
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 129
    .local v2, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v3, "flag_item_dialog"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_0

    .line 132
    iget v3, v0, Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;->textEntryStringId:I

    invoke-static {v3}, Lcom/google/android/finsky/activities/FlagItemUserMessageDialog;->newInstance(I)Lcom/google/android/finsky/activities/FlagItemUserMessageDialog;

    move-result-object v1

    .line 134
    .local v1, "dialog":Lcom/google/android/finsky/activities/FlagItemUserMessageDialog;
    const/4 v3, 0x0

    invoke-virtual {v1, p0, v3}, Lcom/google/android/finsky/activities/FlagItemUserMessageDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 135
    const-string v3, "flag_item_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/FlagItemUserMessageDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 137
    .end local v1    # "dialog":Lcom/google/android/finsky/activities/FlagItemUserMessageDialog;
    .end local v2    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->postFlag()V

    goto :goto_0
.end method

.method public onPositiveClick(Ljava/lang/String;)V
    .locals 0
    .param p1, "flagMessage"    # Ljava/lang/String;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagMessage:Ljava/lang/String;

    .line 283
    invoke-direct {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->postFlag()V

    .line 284
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 270
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/UrlBasedPageFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    .line 272
    const-string v0, "doc"

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 273
    const-string v0, "flag_free_text_message"

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mFlagMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    invoke-direct {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getSelectedFlagType()Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 275
    const-string v0, "flag_selected_button_id"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->getSelectedFlagType()Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;

    move-result-object v1

    iget v1, v1, Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;->stringId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 278
    :cond_0
    return-void
.end method

.method public rebindActionBar()V
    .locals 3

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0c02f4

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v1, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 261
    return-void
.end method

.method public rebindViews()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 232
    iget-object v4, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v4, :cond_1

    .line 233
    iget-object v4, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDataView:Landroid/view/ViewGroup;

    const v5, 0x7f0a01da

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 234
    .local v0, "desc":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    const v4, 0x7f0c02f6

    :goto_0
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 238
    iget-object v4, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 239
    iget-object v4, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDataView:Landroid/view/ViewGroup;

    const v5, 0x7f0a01dc

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 240
    .local v1, "footer":Landroid/widget/TextView;
    const v4, 0x7f0c02f9

    new-array v5, v7, [Ljava/lang/Object;

    sget-object v6, Lcom/google/android/finsky/config/G;->musicDmcaReportLink:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {p0, v4, v5}, Lcom/google/android/finsky/activities/FlagItemFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 242
    .local v2, "footerText":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 244
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    .end local v1    # "footer":Landroid/widget/TextView;
    .end local v2    # "footerText":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    iget-object v5, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    new-array v6, v7, [Landroid/view/View;

    iget-object v7, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDetailsPanel:Landroid/view/ViewGroup;

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v8, v6}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V

    .line 249
    iget-object v4, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDataView:Landroid/view/ViewGroup;

    const v5, 0x7f0a01d9

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 250
    .local v3, "summaryTrailing":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/finsky/activities/FlagItemFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 253
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemFragment;->rebindActionBar()V

    .line 255
    .end local v0    # "desc":Landroid/widget/TextView;
    .end local v3    # "summaryTrailing":Landroid/view/View;
    :cond_1
    return-void

    .line 234
    .restart local v0    # "desc":Landroid/widget/TextView;
    :cond_2
    const v4, 0x7f0c02f8

    goto :goto_0
.end method

.method protected requestData()V
    .locals 0

    .prologue
    .line 148
    return-void
.end method
