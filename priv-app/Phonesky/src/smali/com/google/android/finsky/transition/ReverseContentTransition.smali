.class public Lcom/google/android/finsky/transition/ReverseContentTransition;
.super Landroid/transition/Visibility;
.source "ReverseContentTransition.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/transition/Visibility;-><init>()V

    .line 25
    return-void
.end method

.method private captureContentHeight(Landroid/transition/TransitionValues;)V
    .locals 19
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 28
    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    .line 29
    .local v4, "originalView":Landroid/view/View;
    move-object v15, v4

    .line 30
    .local v15, "view":Landroid/view/View;
    const/4 v9, 0x0

    .line 34
    .local v9, "scroller":Landroid/widget/ScrollView;
    :goto_0
    if-eqz v15, :cond_0

    .line 35
    invoke-virtual {v15}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 36
    .local v5, "parent":Landroid/view/View;
    instance-of v0, v5, Landroid/widget/ScrollView;

    move/from16 v16, v0

    if-eqz v16, :cond_1

    move-object v9, v5

    .line 37
    check-cast v9, Landroid/widget/ScrollView;

    .line 43
    .end local v5    # "parent":Landroid/view/View;
    :cond_0
    if-nez v9, :cond_2

    .line 80
    :goto_1
    return-void

    .line 40
    .restart local v5    # "parent":Landroid/view/View;
    :cond_1
    move-object v15, v5

    .line 41
    goto :goto_0

    .line 50
    .end local v5    # "parent":Landroid/view/View;
    :cond_2
    const/4 v2, 0x0

    .line 53
    .local v2, "isOriginalViewVisibleInScroll":Z
    const/4 v7, 0x0

    .line 54
    .local v7, "scrollHeight":I
    invoke-virtual {v9}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v8

    .line 55
    .local v8, "scrollTop":I
    invoke-virtual {v9}, Landroid/widget/ScrollView;->getHeight()I

    move-result v16

    add-int v6, v8, v16

    .local v6, "scrollBottom":I
    move-object v13, v15

    .line 57
    check-cast v13, Landroid/view/ViewGroup;

    .line 58
    .local v13, "sectionStack":Landroid/view/ViewGroup;
    invoke-virtual {v13}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v12

    .line 59
    .local v12, "sectionCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v12, :cond_7

    .line 60
    invoke-virtual {v13, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 61
    .local v10, "section":Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v16

    if-eqz v16, :cond_4

    .line 59
    :cond_3
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 64
    :cond_4
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v14

    .line 65
    .local v14, "sectionTop":I
    invoke-virtual {v10}, Landroid/view/View;->getBottom()I

    move-result v11

    .line 67
    .local v11, "sectionBottom":I
    if-le v11, v8, :cond_6

    if-ge v14, v6, :cond_6

    const/4 v3, 0x1

    .line 69
    .local v3, "isSectionVisibleInScroll":Z
    :goto_4
    if-eqz v3, :cond_5

    .line 70
    invoke-virtual {v10}, Landroid/view/View;->getHeight()I

    move-result v16

    add-int v7, v7, v16

    .line 72
    :cond_5
    if-ne v4, v10, :cond_3

    .line 73
    move v2, v3

    goto :goto_3

    .line 67
    .end local v3    # "isSectionVisibleInScroll":Z
    :cond_6
    const/4 v3, 0x0

    goto :goto_4

    .line 78
    .end local v10    # "section":Landroid/view/View;
    .end local v11    # "sectionBottom":I
    .end local v14    # "sectionTop":I
    :cond_7
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    move-object/from16 v16, v0

    const-string v17, "translate:y"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    move-object/from16 v16, v0

    const-string v17, "participate"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    invoke-interface/range {v16 .. v18}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method


# virtual methods
.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 0
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/transition/Visibility;->captureStartValues(Landroid/transition/TransitionValues;)V

    .line 86
    invoke-direct {p0, p1}, Lcom/google/android/finsky/transition/ReverseContentTransition;->captureContentHeight(Landroid/transition/TransitionValues;)V

    .line 87
    return-void
.end method

.method public onAppear(Landroid/view/ViewGroup;Landroid/view/View;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 1
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "startValues"    # Landroid/transition/TransitionValues;
    .param p4, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDisappear(Landroid/view/ViewGroup;Landroid/view/View;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 11
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "startValues"    # Landroid/transition/TransitionValues;
    .param p4, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    const/4 v6, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 98
    if-eqz p3, :cond_0

    iget-object v5, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v7, "translate:y"

    invoke-interface {v5, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v7, "participate"

    invoke-interface {v5, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    move-object v5, v6

    .line 114
    :goto_0
    return-object v5

    .line 103
    :cond_1
    iget-object v5, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v7, "participate"

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 104
    .local v2, "shouldParticipate":Z
    if-nez v2, :cond_2

    move-object v5, v6

    .line 105
    goto :goto_0

    .line 108
    :cond_2
    iget-object v5, p3, Landroid/transition/TransitionValues;->values:Ljava/util/Map;

    const-string v6, "translate:y"

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 110
    .local v4, "translateY":I
    const-string v5, "translationY"

    new-array v6, v8, [F

    const/4 v7, 0x0

    aput v7, v6, v9

    int-to-float v7, v4

    aput v7, v6, v10

    invoke-static {p2, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 111
    .local v3, "translateAnimation":Landroid/animation/Animator;
    const-string v5, "alpha"

    new-array v6, v8, [F

    fill-array-data v6, :array_0

    invoke-static {p2, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 112
    .local v0, "alpha":Landroid/animation/Animator;
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 113
    .local v1, "result":Landroid/animation/AnimatorSet;
    new-array v5, v8, [Landroid/animation/Animator;

    aput-object v3, v5, v9

    aput-object v0, v5, v10

    invoke-virtual {v1, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 114
    new-instance v5, Lcom/google/android/finsky/transition/NoPauseAnimator;

    invoke-direct {v5, v1}, Lcom/google/android/finsky/transition/NoPauseAnimator;-><init>(Landroid/animation/Animator;)V

    goto :goto_0

    .line 111
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3f4ccccd    # 0.8f
    .end array-data
.end method
