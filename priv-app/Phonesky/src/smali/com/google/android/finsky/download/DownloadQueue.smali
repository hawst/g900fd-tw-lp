.class public interface abstract Lcom/google/android/finsky/download/DownloadQueue;
.super Ljava/lang/Object;
.source "DownloadQueue.java"


# virtual methods
.method public abstract add(Lcom/google/android/finsky/download/Download;)V
.end method

.method public abstract addListener(Lcom/google/android/finsky/download/DownloadQueueListener;)V
.end method

.method public abstract addRecoveredDownload(Lcom/google/android/finsky/download/Download;)V
.end method

.method public abstract cancel(Lcom/google/android/finsky/download/Download;)V
.end method

.method public abstract getByPackageName(Ljava/lang/String;)Lcom/google/android/finsky/download/Download;
.end method

.method public abstract getDownloadByContentUri(Landroid/net/Uri;)Lcom/google/android/finsky/download/Download;
.end method

.method public abstract getDownloadManager()Lcom/google/android/finsky/download/DownloadManagerFacade;
.end method

.method public abstract getRunningDownloads()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;"
        }
    .end annotation
.end method

.method public abstract notifyClicked(Lcom/google/android/finsky/download/Download;)V
.end method

.method public abstract notifyProgress(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V
.end method

.method public abstract release(Landroid/net/Uri;)V
.end method

.method public abstract setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V
.end method
