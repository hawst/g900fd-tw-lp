.class public Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;
.super Landroid/app/Service;
.source "FirstPartyInAppBillingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$1;,
        Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;
    }
.end annotation


# instance fields
.field private final mBinder:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 28
    new-instance v0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;-><init>(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->mBinder:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;

    .line 76
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->checkAccountAndPackageName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;
    .param p1, "x1"    # Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private checkAccountAndPackageName(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 5
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 36
    sget-object v1, Lcom/google/android/finsky/config/G;->iabV3FirstPartyApiEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 37
    const-string v1, "This API is disabled."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .line 62
    :cond_0
    :goto_0
    return-object v0

    .line 40
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 41
    const-string v1, "Input Error: Non empty/null argument expected for accountName."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 44
    :cond_2
    invoke-static {p1, p0}, Lcom/google/android/finsky/api/AccountHandler;->hasAccount(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 45
    const-string v1, "Unable to locate specified accountName: %s"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 50
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-static {p2, v1, v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->validatePackageName(Ljava/lang/String;Landroid/content/pm/PackageManager;I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 52
    .local v0, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-ne v0, v1, :cond_0

    .line 57
    sget-object v1, Lcom/google/android/finsky/config/G;->iabV3FirstPartyApiUnrestrictedAccess:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-static {p0, p2}, Lcom/google/android/finsky/utils/SignatureUtils;->isFirstPartyPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 59
    const-string v1, "The calling package is not authorized to use this API: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p2, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 60
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 62
    :cond_4
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0
.end method

.method private createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    .locals 3
    .param p1, "responseCode"    # Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .prologue
    .line 71
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 72
    .local v0, "response":Landroid/os/Bundle;
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {p1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 73
    return-object v0
.end method

.method private getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;-><init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/library/LibraryReplicators;Lcom/google/android/finsky/api/DfeApi;)V

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService;->mBinder:Lcom/google/android/finsky/billing/iab/FirstPartyInAppBillingService$Stub;

    return-object v0
.end method
