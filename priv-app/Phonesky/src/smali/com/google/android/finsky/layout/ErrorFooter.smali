.class public Lcom/google/android/finsky/layout/ErrorFooter;
.super Lcom/google/android/finsky/layout/IdentifiableViewGroup;
.source "ErrorFooter.java"


# instance fields
.field private mErrorMessage:Landroid/widget/TextView;

.field private mRetryButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/ErrorFooter;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method


# virtual methods
.method public configure(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "errorText"    # Ljava/lang/String;
    .param p2, "retryListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mErrorMessage:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    iget-object v0, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mRetryButton:Landroid/widget/Button;

    const v1, 0x7f0c01d7

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 36
    iget-object v0, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mRetryButton:Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Lcom/google/android/finsky/layout/IdentifiableViewGroup;->onFinishInflate()V

    .line 29
    const v0, 0x7f0a01cd

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ErrorFooter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mErrorMessage:Landroid/widget/TextView;

    .line 30
    const v0, 0x7f0a01ce

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ErrorFooter;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mRetryButton:Landroid/widget/Button;

    .line 31
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 21
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 63
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getWidth()I

    move-result v16

    .line 64
    .local v16, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getHeight()I

    move-result v8

    .line 65
    .local v8, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getPaddingLeft()I

    move-result v10

    .line 66
    .local v10, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getPaddingRight()I

    move-result v11

    .line 67
    .local v11, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getPaddingTop()I

    move-result v12

    .line 68
    .local v12, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getPaddingBottom()I

    move-result v9

    .line 70
    .local v9, "paddingBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/ErrorFooter;->mRetryButton:Landroid/widget/Button;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v15

    .line 71
    .local v15, "retryButtonWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/ErrorFooter;->mRetryButton:Landroid/widget/Button;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v13

    .line 72
    .local v13, "retryButtonHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/ErrorFooter;->mErrorMessage:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    .line 73
    .local v7, "errorMessageWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/ErrorFooter;->mErrorMessage:Landroid/widget/TextView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    .line 75
    .local v4, "errorMessageHeight":I
    sub-int v17, v8, v12

    sub-int v17, v17, v9

    sub-int v17, v17, v13

    div-int/lit8 v17, v17, 0x2

    add-int v14, v12, v17

    .line 77
    .local v14, "retryButtonTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/ErrorFooter;->mRetryButton:Landroid/widget/Button;

    move-object/from16 v17, v0

    sub-int v18, v16, v11

    sub-int v18, v18, v15

    sub-int v19, v16, v11

    add-int v20, v14, v13

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v14, v2, v3}, Landroid/widget/Button;->layout(IIII)V

    .line 80
    sub-int v17, v8, v12

    sub-int v17, v17, v9

    sub-int v17, v17, v4

    div-int/lit8 v17, v17, 0x2

    add-int v6, v12, v17

    .line 82
    .local v6, "errorMessageTop":I
    sub-int v17, v16, v10

    sub-int v17, v17, v11

    sub-int v17, v17, v15

    sub-int v17, v17, v7

    div-int/lit8 v17, v17, 0x2

    add-int v5, v10, v17

    .line 84
    .local v5, "errorMessageLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/ErrorFooter;->mErrorMessage:Landroid/widget/TextView;

    move-object/from16 v17, v0

    add-int v18, v5, v7

    add-int v19, v6, v4

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 86
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v8, 0x0

    const/high16 v7, -0x80000000

    .line 41
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 42
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getPaddingLeft()I

    move-result v2

    .line 43
    .local v2, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getPaddingRight()I

    move-result v3

    .line 44
    .local v3, "paddingRight":I
    sub-int v5, v4, v2

    sub-int v0, v5, v3

    .line 48
    .local v0, "contentWidth":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mRetryButton:Landroid/widget/Button;

    div-int/lit8 v6, v0, 0x2

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v6, v8}, Landroid/widget/Button;->measure(II)V

    .line 52
    iget-object v5, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mErrorMessage:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mRetryButton:Landroid/widget/Button;

    invoke-virtual {v6}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v6

    sub-int v6, v0, v6

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v6, v8}, Landroid/widget/TextView;->measure(II)V

    .line 56
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getPaddingTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ErrorFooter;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mErrorMessage:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/finsky/layout/ErrorFooter;->mRetryButton:Landroid/widget/Button;

    invoke-virtual {v7}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    add-int v1, v5, v6

    .line 58
    .local v1, "height":I
    invoke-virtual {p0, v4, v1}, Lcom/google/android/finsky/layout/ErrorFooter;->setMeasuredDimension(II)V

    .line 59
    return-void
.end method
