.class public Lcom/google/android/finsky/layout/play/PlayCirclesIcon;
.super Landroid/widget/ImageView;
.source "PlayCirclesIcon.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->setWillNotDraw(Z)V

    .line 30
    return-void
.end method


# virtual methods
.method public configure(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "isInCircles"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 36
    if-eqz p2, :cond_0

    .line 37
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c03b5

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 39
    const v0, 0x7f02004e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->setImageResource(I)V

    .line 45
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c03b4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 43
    const v0, 0x7f02004d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->setImageResource(I)V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->invalidate()V

    .line 51
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->getWidth()I

    move-result v1

    .line 58
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->getHeight()I

    move-result v0

    .line 60
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->isPressed()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->isClickable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v2

    invoke-virtual {v2, p1, v1, v0}, Lcom/google/android/play/image/AvatarCropTransformation;->drawPressedOverlay(Landroid/graphics/Canvas;II)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCirclesIcon;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v2

    invoke-virtual {v2, p1, v1, v0}, Lcom/google/android/play/image/AvatarCropTransformation;->drawFocusedOverlay(Landroid/graphics/Canvas;II)V

    goto :goto_0
.end method
