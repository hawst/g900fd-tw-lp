.class Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;
.super Ljava/lang/Object;
.source "InAppBillingManager.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/iab/InAppBillingManager;->fetchSkuDetails(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/InAppBillingManager;

.field final synthetic val$response:Landroid/os/Bundle;

.field final synthetic val$semaphore:Ljava/util/concurrent/Semaphore;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/iab/InAppBillingManager;Landroid/os/Bundle;Ljava/util/concurrent/Semaphore;)V
    .locals 0

    .prologue
    .line 507
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    iput-object p2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;->val$response:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;)V
    .locals 9
    .param p1, "detailsResponse"    # Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    .prologue
    .line 511
    iget-object v1, p1, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;->entry:[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    .line 512
    .local v1, "bulkDetailsEntry":[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 513
    .local v2, "detailsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v0, v1

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v3, v0, v4

    .line 514
    .local v3, "entry":Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
    iget-object v6, v3, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v6, :cond_0

    .line 515
    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingManager;->buildDetailsJson(Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;)Ljava/lang/String;
    invoke-static {v6, v3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->access$000(Lcom/google/android/finsky/billing/iab/InAppBillingManager;Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 513
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 518
    .end local v3    # "entry":Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;->val$response:Landroid/os/Bundle;

    const-string v7, "DETAILS_LIST"

    invoke-virtual {v6, v7, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 519
    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;->val$response:Landroid/os/Bundle;

    const-string v7, "RESPONSE_CODE"

    sget-object v8, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v8}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 520
    iget-object v6, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;->val$semaphore:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v6}, Ljava/util/concurrent/Semaphore;->release()V

    .line 521
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 507
    check-cast p1, Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;->onResponse(Lcom/google/android/finsky/protos/Details$BulkDetailsResponse;)V

    return-void
.end method
