.class Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowErrorListener;
.super Ljava/lang/Object;
.source "AddCreditCardSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EscrowErrorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowErrorListener;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$1;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowErrorListener;-><init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;)V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 4
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v3, 0x1

    .line 100
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowErrorListener;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    const/4 v1, 0x2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->logError(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->access$200(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;ILjava/lang/String;)V

    .line 101
    const-string v0, "Error during escrowing: %s"

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar$EscrowErrorListener;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;

    # invokes: Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->setVolleyError(Lcom/android/volley/VolleyError;I)V
    invoke-static {v0, p1, v3}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;->access$300(Lcom/google/android/finsky/billing/creditcard/AddCreditCardSidecar;Lcom/android/volley/VolleyError;I)V

    .line 103
    return-void
.end method
