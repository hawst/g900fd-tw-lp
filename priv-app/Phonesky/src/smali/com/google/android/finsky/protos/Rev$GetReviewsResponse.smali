.class public final Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Rev.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Rev;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GetReviewsResponse"
.end annotation


# instance fields
.field public hasMatchingCount:Z

.field public matchingCount:J

.field public review:[Lcom/google/android/finsky/protos/DocumentV2$Review;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->clear()Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    .line 96
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;
    .locals 2

    .prologue
    .line 99
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$Review;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 100
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->matchingCount:J

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->hasMatchingCount:Z

    .line 102
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->cachedSize:I

    .line 103
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 125
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 126
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 127
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 128
    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    aget-object v0, v3, v1

    .line 129
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-eqz v0, :cond_0

    .line 130
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 127
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 135
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$Review;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->hasMatchingCount:Z

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->matchingCount:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 136
    :cond_2
    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->matchingCount:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 139
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 147
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 148
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 152
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 153
    :sswitch_0
    return-object p0

    .line 158
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 160
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-nez v5, :cond_2

    move v1, v4

    .line 161
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 163
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-eqz v1, :cond_1

    .line 164
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 166
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 167
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Review;-><init>()V

    aput-object v5, v2, v1

    .line 168
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 169
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 160
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Review;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v1, v5

    goto :goto_1

    .line 172
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Review;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Review;-><init>()V

    aput-object v5, v2, v1

    .line 173
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 174
    iput-object v2, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    goto :goto_0

    .line 178
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Review;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->matchingCount:J

    .line 179
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->hasMatchingCount:Z

    goto :goto_0

    .line 148
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 109
    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 110
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 111
    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    aget-object v0, v2, v1

    .line 112
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-eqz v0, :cond_0

    .line 113
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 110
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 117
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$Review;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->hasMatchingCount:Z

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->matchingCount:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 118
    :cond_2
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->matchingCount:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 120
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 121
    return-void
.end method
