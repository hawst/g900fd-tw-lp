.class public Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;
.super Landroid/widget/LinearLayout;
.source "SecondaryActionsModuleLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;
    }
.end annotation


# static fields
.field private static final sCountFormatter:Ljava/text/NumberFormat;


# instance fields
.field private mPlusOneButton:Landroid/view/View;

.field private mPlusOneIcon:Landroid/widget/TextView;

.field private mSecondaryActionsClickListener:Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;

.field private mShareButton:Landroid/view/View;

.field private mWishlistButton:Landroid/view/View;

.field private mWishlistButtonIcon:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->sCountFormatter:Ljava/text/NumberFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mSecondaryActionsClickListener:Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;

    if-nez v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mWishlistButton:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mSecondaryActionsClickListener:Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;->onWishlistClick(Landroid/view/View;)V

    .line 137
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mShareButton:Landroid/view/View;

    if-ne p1, v0, :cond_3

    .line 138
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mSecondaryActionsClickListener:Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;->onShareClick(Landroid/view/View;)V

    .line 140
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mPlusOneButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mSecondaryActionsClickListener:Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;->onPlusOneClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 49
    const v0, 0x7f0a015e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mWishlistButton:Landroid/view/View;

    .line 50
    const v0, 0x7f0a0160

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mWishlistButtonIcon:Landroid/widget/ImageView;

    .line 51
    const v0, 0x7f0a0161

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mShareButton:Landroid/view/View;

    .line 52
    const v0, 0x7f0a0162

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mPlusOneButton:Landroid/view/View;

    .line 53
    const v0, 0x7f0a0163

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mPlusOneIcon:Landroid/widget/TextView;

    .line 55
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mWishlistButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mShareButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;->mPlusOneButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    return-void
.end method
