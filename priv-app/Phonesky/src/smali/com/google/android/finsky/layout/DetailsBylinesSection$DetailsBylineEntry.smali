.class public Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
.super Ljava/lang/Object;
.source "DetailsBylinesSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/DetailsBylinesSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DetailsBylineEntry"
.end annotation


# instance fields
.field public iconResourceId:I

.field public onClickListener:Landroid/view/View$OnClickListener;

.field public text:Ljava/lang/CharSequence;

.field public textResourceId:I


# direct methods
.method public constructor <init>(IILandroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "textResourceId"    # I
    .param p2, "iconResourceId"    # I
    .param p3, "onClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->text:Ljava/lang/CharSequence;

    .line 67
    iput p1, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->textResourceId:I

    .line 68
    iput p2, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->iconResourceId:I

    .line 69
    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->onClickListener:Landroid/view/View$OnClickListener;

    .line 70
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v0, -0x1

    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->text:Ljava/lang/CharSequence;

    .line 74
    iput v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->textResourceId:I

    .line 75
    iput v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->iconResourceId:I

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->onClickListener:Landroid/view/View$OnClickListener;

    .line 77
    return-void
.end method
