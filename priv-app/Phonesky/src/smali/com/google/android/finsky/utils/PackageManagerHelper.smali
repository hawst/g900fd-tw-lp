.class public Lcom/google/android/finsky/utils/PackageManagerHelper;
.super Ljava/lang/Object;
.source "PackageManagerHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/PackageManagerHelper$1;,
        Lcom/google/android/finsky/utils/PackageManagerHelper$OnCompleteListenerNotifier;,
        Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;
    }
.end annotation


# direct methods
.method public static installPackage(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;ZLjava/lang/String;)V
    .locals 12
    .param p0, "contentUri"    # Landroid/net/Uri;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "expectedSize"    # J
    .param p4, "expectedSignature"    # Ljava/lang/String;
    .param p5, "postInstallCallback"    # Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;
    .param p6, "isForwardLocked"    # Z
    .param p7, "packageName"    # Ljava/lang/String;

    .prologue
    .line 85
    new-instance v1, Lcom/google/android/finsky/utils/PackageManagerHelper$OnCompleteListenerNotifier;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v11}, Lcom/google/android/finsky/utils/PackageManagerHelper$OnCompleteListenerNotifier;-><init>(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;ZLjava/lang/String;ZLcom/google/android/finsky/utils/PackageManagerHelper$1;)V

    .line 88
    .local v1, "notifier":Lcom/google/android/finsky/utils/PackageManagerHelper$OnCompleteListenerNotifier;
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-static {v1, v0}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 89
    return-void
.end method

.method public static installPackageWithDowngrade(Landroid/net/Uri;Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;ZLjava/lang/String;Z)V
    .locals 12
    .param p0, "contentUri"    # Landroid/net/Uri;
    .param p1, "postInstallCallback"    # Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;
    .param p2, "isUpdate"    # Z
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "allowDowngrade"    # Z

    .prologue
    .line 105
    new-instance v1, Lcom/google/android/finsky/utils/PackageManagerHelper$OnCompleteListenerNotifier;

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    move-object v2, p0

    move-object v7, p1

    move-object v9, p3

    move/from16 v10, p4

    invoke-direct/range {v1 .. v11}, Lcom/google/android/finsky/utils/PackageManagerHelper$OnCompleteListenerNotifier;-><init>(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;ZLjava/lang/String;ZLcom/google/android/finsky/utils/PackageManagerHelper$1;)V

    .line 108
    .local v1, "notifier":Lcom/google/android/finsky/utils/PackageManagerHelper$OnCompleteListenerNotifier;
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-static {v1, v0}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 109
    return-void
.end method

.method public static uninstallPackage(Ljava/lang/String;)V
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    .line 285
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/finsky/utils/PackageManagerUtils;->uninstallPackage(Landroid/content/Context;Ljava/lang/String;)V

    .line 286
    return-void
.end method

.method public static verificationExceptionToError(Ljava/io/IOException;Ljava/lang/String;)I
    .locals 10
    .param p0, "mVerificationException"    # Ljava/io/IOException;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 138
    instance-of v3, p0, Lcom/google/android/finsky/utils/Sha1Util$FileSizeVerificationError;

    if-eqz v3, :cond_0

    move-object v2, p0

    .line 139
    check-cast v2, Lcom/google/android/finsky/utils/Sha1Util$FileSizeVerificationError;

    .line 141
    .local v2, "fsve":Lcom/google/android/finsky/utils/Sha1Util$FileSizeVerificationError;
    const-string v3, "Signature check of %s failed, size expected=%d actual=%d"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    iget-wide v6, v2, Lcom/google/android/finsky/utils/Sha1Util$FileSizeVerificationError;->expected:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    iget-wide v6, v2, Lcom/google/android/finsky/utils/Sha1Util$FileSizeVerificationError;->actual:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    const/16 v0, 0x397

    .line 155
    .end local v2    # "fsve":Lcom/google/android/finsky/utils/Sha1Util$FileSizeVerificationError;
    .local v0, "errorCode":I
    :goto_0
    return v0

    .line 144
    .end local v0    # "errorCode":I
    :cond_0
    instance-of v3, p0, Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;

    if-eqz v3, :cond_1

    move-object v1, p0

    .line 145
    check-cast v1, Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;

    .line 147
    .local v1, "fshe":Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;
    const-string v3, "Signature check of %s failed, hash expected=%s actual=%s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    iget-object v5, v1, Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;->expected:Ljava/lang/String;

    aput-object v5, v4, v8

    iget-object v5, v1, Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;->actual:Ljava/lang/String;

    aput-object v5, v4, v9

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    const/16 v0, 0x3c0

    .line 150
    .restart local v0    # "errorCode":I
    goto :goto_0

    .line 151
    .end local v0    # "errorCode":I
    .end local v1    # "fshe":Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;
    :cond_1
    const-string v3, "Verification check of %s failed, exception=%s"

    new-array v4, v9, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 153
    const/16 v0, 0x3c1

    .restart local v0    # "errorCode":I
    goto :goto_0
.end method

.method public static verifyApk(Ljava/io/InputStream;JLjava/lang/String;)V
    .locals 3
    .param p0, "input"    # Ljava/io/InputStream;
    .param p1, "expectSize"    # J
    .param p3, "expectSignature"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/finsky/utils/Sha1Util$FileSizeVerificationError;,
            Lcom/google/android/finsky/utils/Sha1Util$FileHashVerificationError;
        }
    .end annotation

    .prologue
    .line 120
    :try_start_0
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 121
    .local v0, "bufferedInput":Ljava/io/BufferedInputStream;
    invoke-static {v0, p3, p1, p2}, Lcom/google/android/finsky/utils/Sha1Util;->verify(Ljava/io/InputStream;Ljava/lang/String;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    invoke-static {p0}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 125
    return-void

    .line 123
    .end local v0    # "bufferedInput":Ljava/io/BufferedInputStream;
    :catchall_0
    move-exception v1

    invoke-static {p0}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    throw v1
.end method
