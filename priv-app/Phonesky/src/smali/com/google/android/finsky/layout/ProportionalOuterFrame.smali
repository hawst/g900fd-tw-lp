.class public Lcom/google/android/finsky/layout/ProportionalOuterFrame;
.super Landroid/view/ViewGroup;
.source "ProportionalOuterFrame.java"


# instance fields
.field private mProportion:F

.field private mTitleHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    sget-object v1, Lcom/android/vending/R$styleable;->ProportionalOuterFrame:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 29
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->mProportion:F

    .line 31
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 32
    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->mTitleHeight:I

    .line 56
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v4, 0x0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->getChildCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 45
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 46
    .local v0, "child":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v1, v2, 0x2

    .line 47
    .local v1, "leftMargin":I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->getHeight()I

    move-result v3

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 49
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "leftMargin":I
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v6, 0x1

    .line 60
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 61
    .local v3, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 62
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0119

    invoke-virtual {v4, v5, v6, v6}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v2

    .line 66
    .local v2, "titleHeightFraction":F
    iget v4, p0, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->mTitleHeight:I

    int-to-float v5, v0

    mul-float/2addr v5, v2

    float-to-int v5, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->mTitleHeight:I

    .line 69
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->getChildCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 70
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    int-to-float v5, v3

    iget v6, p0, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->mProportion:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {p0, v4, v5, p2}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->measureChild(Landroid/view/View;II)V

    .line 75
    :cond_0
    const v4, 0x7f0a009c

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 76
    .local v1, "title":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 77
    iget v4, p0, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->mTitleHeight:I

    invoke-virtual {v1, v4}, Landroid/view/View;->setMinimumHeight(I)V

    .line 80
    :cond_1
    invoke-virtual {p0, v3, v0}, Lcom/google/android/finsky/layout/ProportionalOuterFrame;->setMeasuredDimension(II)V

    .line 81
    return-void
.end method
