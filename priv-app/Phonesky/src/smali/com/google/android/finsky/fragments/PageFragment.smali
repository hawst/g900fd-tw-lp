.class public abstract Lcom/google/android/finsky/fragments/PageFragment;
.super Landroid/support/v4/app/Fragment;
.source "PageFragment.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;
.implements Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;
.implements Lcom/google/android/finsky/layout/play/RootUiElementNode;


# instance fields
.field protected mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

.field protected mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field protected mContext:Landroid/content/Context;

.field protected mDataView:Landroid/view/ViewGroup;

.field private mDfeAccount:Ljava/lang/String;

.field protected mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mImpressionHandler:Landroid/os/Handler;

.field private mImpressionId:J

.field private mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

.field protected mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field protected mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

.field protected mRefreshRequired:Z

.field protected mSaveInstanceStateCalled:Z

.field private mTheme:I


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 83
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 76
    const v0, 0x7f0d01ac

    iput v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mTheme:I

    .line 80
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->getNextImpressionId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mImpressionId:J

    .line 85
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/fragments/PageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 87
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-static {v0}, Landroid/transition/TransitionInflater;->from(Landroid/content/Context;)Landroid/transition/TransitionInflater;

    move-result-object v0

    const v1, 0x7f060001

    invoke-virtual {v0, v1}, Landroid/transition/TransitionInflater;->inflateTransition(I)Landroid/transition/Transition;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/fragments/PageFragment;->setExitTransition(Ljava/lang/Object;)V

    .line 91
    :cond_0
    return-void
.end method


# virtual methods
.method public canChangeFragmentManagerState()Z
    .locals 2

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 275
    .local v0, "activity":Landroid/app/Activity;
    iget-boolean v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mSaveInstanceStateCalled:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/finsky/activities/AuthenticatedActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/finsky/activities/AuthenticatedActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->isStateSaved()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mImpressionHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/finsky/fragments/PageFragment;->mImpressionId:J

    invoke-static {v0, v2, v3, p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->rootImpression(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 417
    return-void
.end method

.method protected createLayoutSwitcher(Lcom/google/android/finsky/layout/ContentFrame;)Lcom/google/android/finsky/layout/LayoutSwitcher;
    .locals 7
    .param p1, "frame"    # Lcom/google/android/finsky/layout/ContentFrame;

    .prologue
    .line 154
    new-instance v0, Lcom/google/android/finsky/layout/LayoutSwitcher;

    const v2, 0x7f0a0219

    const v3, 0x7f0a02a3

    const v4, 0x7f0a0109

    const/4 v6, 0x2

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/layout/LayoutSwitcher;-><init>(Landroid/view/View;IIILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;I)V

    return-object v0
.end method

.method public flushImpression()V
    .locals 4

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mImpressionHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/finsky/fragments/PageFragment;->mImpressionId:J

    invoke-static {v0, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->flushImpression(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/RootUiElementNode;)V

    .line 430
    return-void
.end method

.method protected abstract getLayoutRes()I
.end method

.method public final getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 411
    const/4 v0, 0x0

    return-object v0
.end method

.method public getToc()Lcom/google/android/finsky/api/model/DfeToc;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    return-object v0
.end method

.method public abstract isDataReady()Z
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 181
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/fragments/PageFragmentHost;

    .line 183
    .local v0, "host":Lcom/google/android/finsky/fragments/PageFragmentHost;
    iget-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    if-eq v0, v1, :cond_0

    .line 184
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/fragments/PageFragmentHost;

    iput-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    .line 185
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mContext:Landroid/content/Context;

    .line 186
    iget-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->getNavigationManager()Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 187
    iget-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->getActionBarController()Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    .line 188
    iget-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/PageFragment;->mDfeAccount:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/finsky/fragments/PageFragmentHost;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 189
    iget-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 190
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onInitViewBinders()V

    .line 192
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/finsky/fragments/PageFragment;->mSaveInstanceStateCalled:Z

    .line 193
    const-string v1, "Views bound"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->logTiming(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 168
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mImpressionHandler:Landroid/os/Handler;

    .line 169
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 170
    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 198
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 200
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "finsky.PageFragment.dfeAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mDfeAccount:Ljava/lang/String;

    .line 201
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "finsky.PageFragment.toc"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/DfeToc;

    iput-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mSaveInstanceStateCalled:Z

    .line 203
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 131
    const v1, 0x7f0400ac

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ContentFrame;

    .line 133
    .local v0, "frame":Lcom/google/android/finsky/layout/ContentFrame;
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getLayoutRes()I

    move-result v1

    const v2, 0x7f0a0219

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/finsky/layout/ContentFrame;->inflateDataLayout(Landroid/view/LayoutInflater;II)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mDataView:Landroid/view/ViewGroup;

    .line 134
    iget-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mDataView:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 136
    iget-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mDataView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/ContentFrame;->addView(Landroid/view/View;)V

    .line 139
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/finsky/fragments/PageFragment;->mSaveInstanceStateCalled:Z

    .line 142
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/fragments/PageFragment;->createLayoutSwitcher(Lcom/google/android/finsky/layout/ContentFrame;)Lcom/google/android/finsky/layout/LayoutSwitcher;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    .line 143
    const-string v1, "Views inflated"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->logTiming(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    return-object v0
.end method

.method public onDataChanged()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 293
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    iput-boolean v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mRefreshRequired:Z

    .line 295
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->switchToData()V

    .line 296
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->rebindViews()V

    .line 297
    const-string v0, "Views rebound"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->logTiming(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 301
    :goto_0
    return-void

    .line 299
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mRefreshRequired:Z

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 161
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 162
    iput-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mDataView:Landroid/view/ViewGroup;

    .line 163
    iput-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    .line 164
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->canChangeFragmentManagerState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 356
    iget-object v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    .line 357
    .local v0, "errorMessage":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/fragments/PageFragment;->switchToError(Ljava/lang/String;)V

    .line 359
    .end local v0    # "errorMessage":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected abstract onInitViewBinders()V
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 246
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 247
    invoke-static {p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mSaveInstanceStateCalled:Z

    .line 249
    iget-boolean v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mRefreshRequired:Z

    if-eqz v0, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->refresh()V

    .line 252
    :cond_0
    return-void
.end method

.method public onRetry()V
    .locals 0

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->refresh()V

    .line 257
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 207
    const-string v0, "finsky.PageFragment.theme"

    iget v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mTheme:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mSaveInstanceStateCalled:Z

    .line 209
    return-void
.end method

.method public rebindActionBar()V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method protected abstract rebindViews()V
.end method

.method public refresh()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 230
    iput-boolean v1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mRefreshRequired:Z

    .line 231
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->requestData()V

    .line 232
    const-string v0, "requestData called"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->logTiming(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    return-void
.end method

.method public refreshOnResume()V
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mRefreshRequired:Z

    .line 242
    return-void
.end method

.method protected abstract requestData()V
.end method

.method protected setArgument(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 385
    return-void
.end method

.method protected setArgument(Ljava/lang/String;Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Landroid/os/Parcelable;

    .prologue
    .line 392
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 393
    return-void
.end method

.method protected setArgument(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    return-void
.end method

.method protected setArgument(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 401
    return-void
.end method

.method protected setDfeAccount(Ljava/lang/String;)V
    .locals 1
    .param p1, "dfeAccount"    # Ljava/lang/String;

    .prologue
    .line 362
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 363
    const-string v0, "finsky.PageFragment.dfeAccount"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->setArgument(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_0
    return-void
.end method

.method protected setDfeToc(Lcom/google/android/finsky/api/model/DfeToc;)V
    .locals 1
    .param p1, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    .line 368
    const-string v0, "finsky.PageFragment.toc"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->setArgument(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 369
    return-void
.end method

.method protected setTheme(I)V
    .locals 0
    .param p1, "themeResId"    # I

    .prologue
    .line 98
    iput p1, p0, Lcom/google/android/finsky/fragments/PageFragment;->mTheme:I

    .line 99
    return-void
.end method

.method public startNewImpression()V
    .locals 2

    .prologue
    .line 421
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->getNextImpressionId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mImpressionId:J

    .line 422
    return-void
.end method

.method protected switchToBlank()V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToBlankMode()V

    .line 317
    return-void
.end method

.method protected switchToData()V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToDataMode()V

    .line 351
    return-void
.end method

.method protected switchToError(Ljava/lang/String;)V
    .locals 9
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 330
    iget-object v6, p0, Lcom/google/android/finsky/fragments/PageFragment;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    if-nez v6, :cond_2

    .line 331
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/PageFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 332
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_1

    move v1, v4

    .line 333
    .local v1, "isActivityNull":Z
    :goto_0
    const/4 v2, 0x0

    .line 334
    .local v2, "isAuth":Z
    const/4 v3, 0x0

    .line 335
    .local v3, "isStateSaved":Z
    if-nez v1, :cond_0

    .line 336
    instance-of v2, v0, Lcom/google/android/finsky/activities/AuthenticatedActivity;

    .line 337
    if-eqz v2, :cond_0

    .line 338
    check-cast v0, Lcom/google/android/finsky/activities/AuthenticatedActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->isStateSaved()Z

    move-result v3

    .line 341
    :cond_0
    const-string v6, "mSaveInstanceStateCalled=[%s], activityNull=[%s], isAuthenticatedActivity=[%s], isStateSaved=[%s]"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    iget-boolean v8, p0, Lcom/google/android/finsky/fragments/PageFragment;->mSaveInstanceStateCalled:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v5

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v7, v4

    const/4 v4, 0x2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v7, v4

    const/4 v4, 0x3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v7, v4

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 347
    .end local v1    # "isActivityNull":Z
    .end local v2    # "isAuth":Z
    .end local v3    # "isStateSaved":Z
    :goto_1
    return-void

    .restart local v0    # "activity":Landroid/app/Activity;
    :cond_1
    move v1, v5

    .line 332
    goto :goto_0

    .line 346
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/fragments/PageFragment;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v4, p1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToErrorMode(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected switchToLoading()V
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    const/16 v1, 0x15e

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToLoadingDelayed(I)V

    .line 323
    return-void
.end method

.method protected switchToLoadingImmediately()V
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/finsky/fragments/PageFragment;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToLoadingMode()V

    .line 327
    return-void
.end method
