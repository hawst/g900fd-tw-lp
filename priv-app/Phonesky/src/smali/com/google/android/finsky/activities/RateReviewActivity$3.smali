.class Lcom/google/android/finsky/activities/RateReviewActivity$3;
.super Ljava/lang/Object;
.source "RateReviewActivity.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/RateReviewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/RateReviewActivity;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/RateReviewActivity;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$3;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRatingChanged(Lcom/google/android/finsky/layout/play/PlayRatingBar;I)V
    .locals 6
    .param p1, "ratingBar"    # Lcom/google/android/finsky/layout/play/PlayRatingBar;
    .param p2, "rating"    # I

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$3;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # invokes: Lcom/google/android/finsky/activities/RateReviewActivity;->syncButtonEnabledState()V
    invoke-static {v0}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$000(Lcom/google/android/finsky/activities/RateReviewActivity;)V

    .line 349
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$3;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # invokes: Lcom/google/android/finsky/activities/RateReviewActivity;->syncRatingDescription(I)V
    invoke-static {v0, p2}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$1100(Lcom/google/android/finsky/activities/RateReviewActivity;I)V

    .line 350
    if-lez p2, :cond_0

    .line 351
    iget-object v0, p0, Lcom/google/android/finsky/activities/RateReviewActivity$3;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    iget-object v1, p0, Lcom/google/android/finsky/activities/RateReviewActivity$3;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/RateReviewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100004

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/RateReviewActivity$3;->this$0:Lcom/google/android/finsky/activities/RateReviewActivity;

    # getter for: Lcom/google/android/finsky/activities/RateReviewActivity;->mRatingDescription:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/finsky/activities/RateReviewActivity;->access$1200(Lcom/google/android/finsky/activities/RateReviewActivity;)Landroid/widget/TextView;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 357
    :cond_0
    return-void
.end method
