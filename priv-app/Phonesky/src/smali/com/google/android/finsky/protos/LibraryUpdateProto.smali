.class public interface abstract Lcom/google/android/finsky/protos/LibraryUpdateProto;
.super Ljava/lang/Object;
.source "LibraryUpdateProto.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;,
        Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;,
        Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;,
        Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;,
        Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;,
        Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    }
.end annotation
