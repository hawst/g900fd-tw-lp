.class public final Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TextFormInput"
.end annotation


# instance fields
.field public hasPlaintextResponse:Z

.field public paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

.field public plaintextResponse:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1723
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1724
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->clear()Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    .line 1725
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;
    .locals 1

    .prologue
    .line 1728
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->plaintextResponse:Ljava/lang/String;

    .line 1729
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->hasPlaintextResponse:Z

    .line 1730
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    .line 1731
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->cachedSize:I

    .line 1732
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1749
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1750
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->hasPlaintextResponse:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->plaintextResponse:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1751
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->plaintextResponse:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1754
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    if-eqz v1, :cond_2

    .line 1755
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1758
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1766
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1767
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1771
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1772
    :sswitch_0
    return-object p0

    .line 1777
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->plaintextResponse:Ljava/lang/String;

    .line 1778
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->hasPlaintextResponse:Z

    goto :goto_0

    .line 1782
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    if-nez v1, :cond_1

    .line 1783
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    .line 1785
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1767
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1699
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1738
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->hasPlaintextResponse:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->plaintextResponse:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1739
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->plaintextResponse:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1741
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    if-eqz v0, :cond_2

    .line 1742
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;->paypalAuthResponse:Lcom/google/android/finsky/protos/CreateInstrument$PaypalAuthResponse;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1744
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1745
    return-void
.end method
