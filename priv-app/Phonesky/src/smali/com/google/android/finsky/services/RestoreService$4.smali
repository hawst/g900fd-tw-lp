.class Lcom/google/android/finsky/services/RestoreService$4;
.super Ljava/lang/Object;
.source "RestoreService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/RestoreService;->registerAndNotifyListener(Lcom/google/android/finsky/services/SetupHoldListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/RestoreService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/RestoreService;)V
    .locals 0

    .prologue
    .line 1901
    iput-object p1, p0, Lcom/google/android/finsky/services/RestoreService$4;->this$0:Lcom/google/android/finsky/services/RestoreService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1905
    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$4;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->shouldHold(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1907
    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$4;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v2

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mInstallerRunningPackage:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$3400(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1908
    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$4;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v2

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mInstallerRunningPackage:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$3400(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/lang/String;

    move-result-object v0

    .line 1909
    .local v0, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$4;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v2

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 1910
    .local v1, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->visible:Z

    if-eqz v2, :cond_0

    .line 1911
    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$4;->this$0:Lcom/google/android/finsky/services/RestoreService;

    const/4 v3, 0x3

    iget-object v4, v1, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->title:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v2, v3, v0, v4, v6}, Lcom/google/android/finsky/services/RestoreService;->access$1000(Lcom/google/android/finsky/services/RestoreService;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 1920
    .end local v0    # "packageName":Ljava/lang/String;
    .end local v1    # "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    :goto_0
    return-void

    .line 1916
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$4;->this$0:Lcom/google/android/finsky/services/RestoreService;

    const/4 v3, 0x2

    # invokes: Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v2, v3, v4, v4, v5}, Lcom/google/android/finsky/services/RestoreService;->access$1000(Lcom/google/android/finsky/services/RestoreService;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1918
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$4;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V
    invoke-static {v2, v6, v4, v4, v5}, Lcom/google/android/finsky/services/RestoreService;->access$1000(Lcom/google/android/finsky/services/RestoreService;ILjava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method
