.class public Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterView;
.source "PlayCardMerchClusterView.java"


# instance fields
.field private final mContentVerticalMargin:I

.field private final mContentVerticalPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 32
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b0153

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalMargin:I

    .line 33
    const v1, 0x7f0b0152

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalPadding:I

    .line 34
    return-void
.end method


# virtual methods
.method public configureMerch(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 7
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "merchImagePosition"    # I
    .param p3, "merchImage"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p4, "clusterTitle"    # Ljava/lang/String;
    .param p5, "merchClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;

    .line 48
    .local v0, "merchContent":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 50
    .local v6, "contentLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalMargin:I

    iput v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 51
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalMargin:I

    iput v1, v6, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 52
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContentVerticalPadding:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->setCardContentVerticalPadding(I)V

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 54
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->configureMerch(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 56
    return-void
.end method

.method public configureNoMerch()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;

    .line 60
    .local v1, "merchContent":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 62
    .local v0, "contentLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 63
    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 64
    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->setCardContentVerticalPadding(I)V

    .line 65
    return-void
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0x197

    return v0
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 69
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->onRecycle()V

    .line 70
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;

    .line 71
    .local v0, "merchContent":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterViewContent;->onRecycle()V

    .line 72
    return-void
.end method
