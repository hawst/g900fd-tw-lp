.class Lcom/google/android/finsky/adapters/CardListAdapter$3;
.super Ljava/lang/Object;
.source "CardListAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/adapters/CardListAdapter;->getWarmWelcomeCardView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$finalDismissAction:Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

.field final synthetic val$finalWarmWelcomeCard:Lcom/google/android/finsky/layout/play/WarmWelcomeCard;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/adapters/CardListAdapter;Lcom/google/android/finsky/layout/play/WarmWelcomeCard;Lcom/google/android/finsky/protos/DocumentV2$CallToAction;Lcom/google/android/finsky/api/model/Document;)V
    .locals 0

    .prologue
    .line 1518
    iput-object p1, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iput-object p2, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->val$finalWarmWelcomeCard:Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

    iput-object p3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->val$finalDismissAction:Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    iput-object p4, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1526
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x4cf

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->val$finalWarmWelcomeCard:Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1531
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v0, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->val$finalDismissAction:Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;
    invoke-static {v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->access$400(Lcom/google/android/finsky/adapters/CardListAdapter;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;
    invoke-static {v3}, Lcom/google/android/finsky/adapters/CardListAdapter;->access$500(Lcom/google/android/finsky/adapters/CardListAdapter;)Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->resolveCallToAction(Lcom/google/android/finsky/protos/DocumentV2$CallToAction;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;)V

    .line 1535
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;
    invoke-static {v0}, Lcom/google/android/finsky/adapters/CardListAdapter;->access$600(Lcom/google/android/finsky/adapters/CardListAdapter;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->dismissRecommendation(Ljava/lang/String;)V

    .line 1542
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->val$finalWarmWelcomeCard:Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter$3;->this$0:Lcom/google/android/finsky/adapters/CardListAdapter;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->access$700(Lcom/google/android/finsky/adapters/CardListAdapter;)Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/UiUtils;->fadeOutCluster(Landroid/view/View;Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;J)V

    .line 1543
    return-void
.end method
