.class public Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;
.source "SetupWizardAddCreditCardActivity.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;


# instance fields
.field private mAccountName:Ljava/lang/String;

.field protected mFragmentContainer:Landroid/view/ViewGroup;

.field protected mMainView:Landroid/view/View;

.field private mNeedsHideProgress:Z

.field private mSaveInstanceStateCalled:Z

.field private mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

.field protected mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Ljava/lang/String;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/content/Intent;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "params"    # Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .prologue
    .line 85
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 86
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    const-string v1, "setup_wizard_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 88
    return-object v0
.end method

.method private startOrResumeFlow()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "billing_flow_fragment"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    .line 148
    .local v0, "addCreditCardFragment":Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;
    if-eqz v0, :cond_1

    .line 149
    sget-boolean v1, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 150
    const-string v1, "Re-attached to billing flow fragment."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mAccountName:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v2}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->getCardholderName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v4}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isLightTheme()Z

    move-result v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    move-result-object v0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->getContainerId()I

    move-result v2

    const-string v3, "billing_flow_fragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 159
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 160
    const v1, 0x7f0a0109

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 215
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->finish()V

    .line 219
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/SetupWizardUtils;->animateSliding(Landroid/app/Activity;Z)V

    .line 220
    return-void
.end method

.method public getContainerId()I
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 288
    const/16 v0, 0x37b

    return v0
.end method

.method public hideProgress()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 245
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mNeedsHideProgress:Z

    .line 246
    const v0, 0x7f0a0388

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 247
    const v0, 0x7f0a0121

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 248
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 93
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 94
    .local v6, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mAccountName:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/google/android/finsky/api/AccountHandler;->hasAccount(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    const-string v0, "Invalid account supplied in the intent: %s"

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->finish()V

    .line 121
    :goto_0
    return-void

    .line 102
    :cond_0
    const-string v0, "setup_wizard_params"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .line 105
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isLightTheme()Z

    move-result v0

    if-eqz v0, :cond_1

    const v7, 0x7f0d01bf

    .line 107
    .local v7, "themeId":I
    :goto_1
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->setTheme(I)V

    .line 109
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 111
    const v0, 0x7f0401a5

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mMainView:Landroid/view/View;

    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mMainView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->setContentView(Landroid/view/View;)V

    .line 113
    const v0, 0x7f0a00c4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    .line 114
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mMainView:Landroid/view/View;

    const v1, 0x7f0a009c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mTitleView:Landroid/widget/TextView;

    .line 115
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mTitleView:Landroid/widget/TextView;

    const v1, 0x7f0c00ba

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 117
    iget-object v1, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/SetupWizardUtils;->configureBasicUi(Landroid/app/Activity;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;IZZZ)V

    .line 120
    invoke-direct {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->startOrResumeFlow()V

    goto :goto_0

    .line 105
    .end local v7    # "themeId":I
    :cond_1
    const v7, 0x7f0d01be

    goto :goto_1
.end method

.method public onFinished(ZLandroid/os/Bundle;)V
    .locals 11
    .param p1, "canceled"    # Z
    .param p2, "flowResult"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 170
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 171
    .local v10, "result":Landroid/content/Intent;
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    const-string v0, "instrument_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    const-string v0, "instrument_id"

    const-string v1, "instrument_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    :cond_0
    if-nez p1, :cond_1

    if-eqz p2, :cond_1

    const-string v0, "redeemed_offer_message_html"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    const-string v0, "redeemed_offer_message_html"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 180
    .local v9, "redeemedOfferHtml":Ljava/lang/String;
    const-string v0, "redeemed_offer_message_html"

    invoke-virtual {v10, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->hasSetupCompleteScreen()Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    new-instance v7, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 186
    .local v7, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v5

    .line 187
    .local v5, "account":Landroid/accounts/Account;
    invoke-virtual {v7, v9}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c02a0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x4c4

    const/16 v3, 0x4c5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCanceledOnTouchOutside(Z)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 194
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 195
    .local v6, "args":Landroid/os/Bundle;
    const-string v0, "result_intent"

    invoke-virtual {v6, v0, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 196
    const/4 v0, 0x1

    invoke-virtual {v7, v2, v0, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 197
    invoke-virtual {v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v8

    .line 198
    .local v8, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "redeemed_promo_offer"

    invoke-virtual {v8, v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 211
    .end local v5    # "account":Landroid/accounts/Account;
    .end local v6    # "args":Landroid/os/Bundle;
    .end local v7    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v8    # "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .end local v9    # "redeemedOfferHtml":Ljava/lang/String;
    :goto_0
    return-void

    .line 203
    :cond_1
    if-nez p1, :cond_2

    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/PromptForFopHelper;->expireHasNoFop(Ljava/lang/String;)V

    .line 209
    :cond_2
    invoke-virtual {p0, v4, v10}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->setResult(ILandroid/content/Intent;)V

    .line 210
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->finish()V

    goto :goto_0
.end method

.method public onFlowCanceled(Lcom/google/android/finsky/billing/BillingFlowFragment;)V
    .locals 2
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlowFragment;

    .prologue
    .line 257
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->onFinished(ZLandroid/os/Bundle;)V

    .line 258
    return-void
.end method

.method public onFlowError(Lcom/google/android/finsky/billing/BillingFlowFragment;Ljava/lang/String;)V
    .locals 2
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlowFragment;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 262
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 264
    .local v0, "result":Landroid/content/Intent;
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->setResult(ILandroid/content/Intent;)V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->finish()V

    .line 266
    return-void
.end method

.method public onFlowFinished(Lcom/google/android/finsky/billing/BillingFlowFragment;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlowFragment;
    .param p2, "result"    # Landroid/os/Bundle;

    .prologue
    .line 252
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->onFinished(ZLandroid/os/Bundle;)V

    .line 253
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 282
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 125
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 130
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 127
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->onBackPressed()V

    .line 128
    const/4 v0, 0x1

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 272
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 273
    const-string v1, "result_intent"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 275
    .local v0, "result":Landroid/content/Intent;
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->setResult(ILandroid/content/Intent;)V

    .line 276
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->finish()V

    .line 278
    .end local v0    # "result":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 136
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onResume()V

    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mSaveInstanceStateCalled:Z

    .line 139
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mNeedsHideProgress:Z

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->hideProgress()V

    .line 142
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mSaveInstanceStateCalled:Z

    .line 166
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 167
    return-void
.end method

.method public setHostTitle(I)V
    .locals 4
    .param p1, "resId"    # I

    .prologue
    .line 231
    const-string v0, "Swallowing title: resId=%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 232
    return-void
.end method

.method public showProgress(I)V
    .locals 2
    .param p1, "messageId"    # I

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->mSaveInstanceStateCalled:Z

    if-eqz v0, :cond_0

    .line 241
    :goto_0
    return-void

    .line 239
    :cond_0
    const v0, 0x7f0a0388

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 240
    const v0, 0x7f0a0121

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/creditcard/SetupWizardAddCreditCardActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
