.class public Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;
.super Lcom/google/android/finsky/adapters/PaginatedListAdapter;
.source "MyAppsLibraryAdapter.java"

# interfaces
.implements Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter$1;
    }
.end annotation


# static fields
.field private static sEnableRemoveAppsFromLibrary:Z


# instance fields
.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mHasAccountSwitcher:Z

.field private mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private mIsMultiChoiceMode:Z

.field private final mLeadingSpacerHeight:I

.field private mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private mList:Lcom/google/android/finsky/api/model/DfeList;

.field private final mOnArchiveActionListener:Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;

.field private final mOnClickListener:Landroid/view/View$OnClickListener;

.field private final mOnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mToc:Lcom/google/android/finsky/api/model/DfeToc;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/finsky/config/G;->enableRemoveAppsFromLibrary:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->sEnableRemoveAppsFromLibrary:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/PackageStateRepository;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/play/image/BitmapLoader;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;Landroid/view/View$OnLongClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "authenticatedActivity"    # Lcom/google/android/finsky/activities/AuthenticatedActivity;
    .param p2, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p5, "packageStateRepository"    # Lcom/google/android/finsky/appstate/PackageStateRepository;
    .param p6, "installer"    # Lcom/google/android/finsky/receivers/Installer;
    .param p7, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p8, "onClickListener"    # Landroid/view/View$OnClickListener;
    .param p9, "onArchiveActionListener"    # Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;
    .param p10, "onLongClickListener"    # Landroid/view/View$OnLongClickListener;
    .param p11, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v1, 0x0

    .line 94
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v1, v0}, Lcom/google/android/finsky/adapters/PaginatedListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZZ)V

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 96
    iput-object p7, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 97
    iput-object p8, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 98
    iput-object p9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mOnArchiveActionListener:Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;

    .line 99
    iput-object p10, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 100
    iput-object p11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 101
    iput-boolean v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mIsMultiChoiceMode:Z

    .line 102
    iput-object p3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 103
    iput-object p4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 104
    iput-object p5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    .line 105
    iput-object p6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    .line 107
    invoke-static {p1, v1, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mLeadingSpacerHeight:I

    .line 109
    return-void
.end method

.method private getAccountSwitcherView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 277
    if-nez p1, :cond_0

    .line 278
    const v1, 0x7f0400de

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 280
    :cond_0
    const v1, 0x7f0a00b9

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/AccountSelectorView;

    .line 282
    .local v0, "accountNameView":Lcom/google/android/finsky/layout/AccountSelectorView;
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AccountSelectorView;->configure()V

    .line 283
    const-string v1, "account_switcher"

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/AccountSelectorView;->setIdentifier(Ljava/lang/String;)V

    .line 284
    return-object p1
.end method

.method private getDocView(ILcom/google/android/finsky/api/model/Document;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .param p1, "position"    # I
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 214
    if-nez p3, :cond_0

    .line 215
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f04012a

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    :cond_0
    move-object/from16 v1, p3

    .line 218
    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;

    .line 219
    .local v1, "entry":Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;
    if-nez p2, :cond_1

    .line 220
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->bindLoading()V

    .line 250
    :goto_0
    invoke-virtual {v1, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setTag(Ljava/lang/Object;)V

    .line 251
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setIdentifier(Ljava/lang/String;)V

    .line 253
    return-object p3

    .line 222
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->isDocAvailableForInstall(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v11

    .line 225
    .local v11, "availableForInstall":Z
    iget-boolean v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mIsMultiChoiceMode:Z

    if-eqz v2, :cond_3

    move v12, v11

    .line 226
    .local v12, "clickable":Z
    :goto_1
    const-string v3, "my_apps:library"

    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v5, 0x0

    if-nez v12, :cond_4

    const/4 v6, 0x1

    :goto_2
    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v9, 0x1

    const/4 v10, -0x1

    move-object v2, p2

    invoke-static/range {v1 .. v10}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZI)V

    .line 228
    if-eqz v12, :cond_5

    .line 229
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    :goto_3
    sget-boolean v2, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->sEnableRemoveAppsFromLibrary:Z

    if-eqz v2, :cond_2

    .line 237
    iget-boolean v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mIsMultiChoiceMode:Z

    if-nez v2, :cond_6

    invoke-static {p2}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->canRemoveFromLibrary(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 238
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mOnArchiveActionListener:Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setArchivable(ZLcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;)V

    .line 243
    :cond_2
    :goto_4
    iget-boolean v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mIsMultiChoiceMode:Z

    if-nez v2, :cond_7

    if-eqz v11, :cond_7

    .line 244
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mOnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_0

    .line 225
    .end local v12    # "clickable":Z
    :cond_3
    const/4 v12, 0x1

    goto :goto_1

    .line 226
    .restart local v12    # "clickable":Z
    :cond_4
    const/4 v6, 0x0

    goto :goto_2

    .line 231
    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 240
    :cond_6
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setArchivable(ZLcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;)V

    goto :goto_4

    .line 246
    :cond_7
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    goto :goto_0
.end method

.method private getLeadingSpacerView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 267
    if-nez p1, :cond_0

    .line 268
    const v0, 0x7f04014d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 271
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mLeadingSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 272
    const v0, 0x7f0a0029

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 273
    return-object p1
.end method

.method public static getViewDoc(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 308
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method private isDocAvailableForInstall(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 7
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 257
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v5

    iget-object v2, v5, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 258
    .local v2, "packageName":Ljava/lang/String;
    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mPackageStateRepository:Lcom/google/android/finsky/appstate/PackageStateRepository;

    invoke-interface {v5, v2}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v5

    if-eqz v5, :cond_0

    move v1, v3

    .line 259
    .local v1, "isInstalled":Z
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v5, v2}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v0

    .line 261
    .local v0, "installerState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->isDownloadingOrInstalling()Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {p1, v5, v6}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    return v3

    .end local v0    # "installerState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    .end local v1    # "isInstalled":Z
    :cond_0
    move v1, v4

    .line 258
    goto :goto_0

    .restart local v0    # "installerState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    .restart local v1    # "isInstalled":Z
    :cond_1
    move v3, v4

    .line 261
    goto :goto_1
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 129
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mList:Lcom/google/android/finsky/api/model/DfeList;

    if-nez v2, :cond_0

    move v0, v1

    .line 146
    :goto_0
    return v0

    .line 132
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v0

    .line 133
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->isMoreDataAvailable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 134
    add-int/lit8 v0, v0, 0x1

    .line 136
    :cond_1
    if-nez v0, :cond_2

    move v0, v1

    .line 139
    goto :goto_0

    .line 141
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mHasAccountSwitcher:Z

    if-eqz v1, :cond_3

    .line 142
    add-int/lit8 v0, v0, 0x1

    .line 145
    :cond_3
    add-int/lit8 v0, v0, 0x1

    .line 146
    goto :goto_0
.end method

.method public getDocument(I)Lcom/google/android/finsky/api/model/Document;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 124
    .local v0, "item":Ljava/lang/Object;
    instance-of v1, v0, Lcom/google/android/finsky/api/model/Document;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .end local v0    # "item":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "item":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x0

    .line 181
    if-nez p1, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-object v0

    .line 184
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 185
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mHasAccountSwitcher:Z

    if-eqz v1, :cond_2

    if-eqz p1, :cond_0

    .line 188
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mHasAccountSwitcher:Z

    if-eqz v0, :cond_3

    .line 189
    add-int/lit8 p1, p1, -0x1

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/api/model/DfeList;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 156
    if-nez p1, :cond_0

    .line 157
    const/4 v0, 0x4

    .line 175
    :goto_0
    :pswitch_0
    return v0

    .line 160
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_1

    .line 161
    sget-object v2, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter$1;->$SwitchMap$com$google$android$finsky$adapters$PaginatedListAdapter$FooterMode:[I

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 169
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No footer or item at row "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 167
    goto :goto_0

    .line 172
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mHasAccountSwitcher:Z

    if-eqz v2, :cond_2

    if-ne p1, v0, :cond_2

    .line 173
    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    move v0, v1

    .line 175
    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected getLastRequestError()Ljava/lang/String;
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 196
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 208
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type for getView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getLeadingSpacerView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 206
    :goto_0
    return-object v0

    .line 200
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getAccountSwitcherView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 202
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getDocument(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getDocView(ILcom/google/android/finsky/api/model/Document;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 204
    :pswitch_3
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getLoadingFooterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 206
    :pswitch_4
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getErrorFooterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x5

    return v0
.end method

.method protected isMoreDataAvailable()Z
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->isMoreAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected retryLoadingItems()V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->retryLoadItems()V

    .line 292
    :cond_0
    return-void
.end method

.method public setDfeList(Lcom/google/android/finsky/api/model/DfeList;)V
    .locals 0
    .param p1, "dfeList"    # Lcom/google/android/finsky/api/model/DfeList;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mList:Lcom/google/android/finsky/api/model/DfeList;

    .line 113
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->notifyDataSetChanged()V

    .line 114
    return-void
.end method

.method public setMultiChoiceMode(Z)V
    .locals 0
    .param p1, "isMultiChoiceMode"    # Z

    .prologue
    .line 312
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mIsMultiChoiceMode:Z

    .line 313
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->notifyDataSetChanged()V

    .line 314
    return-void
.end method

.method public showAccountSwitcher()V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->mHasAccountSwitcher:Z

    .line 118
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->notifyDataSetChanged()V

    .line 119
    return-void
.end method
