.class public final Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlusOneData"
.end annotation


# instance fields
.field public circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public circlesTotal:J

.field public hasCirclesTotal:Z

.field public hasSetByUser:Z

.field public hasTotal:Z

.field public oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

.field public setByUser:Z

.field public total:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1634
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1635
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->clear()Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 1636
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1639
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    .line 1640
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasSetByUser:Z

    .line 1641
    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    .line 1642
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasTotal:Z

    .line 1643
    iput-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    .line 1644
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasCirclesTotal:Z

    .line 1645
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1646
    invoke-static {}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;->emptyArray()[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 1647
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->cachedSize:I

    .line 1648
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 1684
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1685
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasSetByUser:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    if-eqz v3, :cond_1

    .line 1686
    :cond_0
    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1689
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasTotal:Z

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 1690
    :cond_2
    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1693
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasCirclesTotal:Z

    if-nez v3, :cond_4

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_5

    .line 1694
    :cond_4
    const/4 v3, 0x3

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1697
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 1698
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 1699
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    aget-object v0, v3, v1

    .line 1700
    .local v0, "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v0, :cond_6

    .line 1701
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1698
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1706
    .end local v0    # "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    .end local v1    # "i":I
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 1707
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 1708
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v3, v1

    .line 1709
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_8

    .line 1710
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1707
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1715
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_9
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 1723
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1724
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1728
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1729
    :sswitch_0
    return-object p0

    .line 1734
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    .line 1735
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasSetByUser:Z

    goto :goto_0

    .line 1739
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    .line 1740
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasTotal:Z

    goto :goto_0

    .line 1744
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    .line 1745
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasCirclesTotal:Z

    goto :goto_0

    .line 1749
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1751
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-nez v5, :cond_2

    move v1, v4

    .line 1752
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    .line 1754
    .local v2, "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v1, :cond_1

    .line 1755
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1757
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1758
    new-instance v5, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    aput-object v5, v2, v1

    .line 1759
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1760
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1757
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1751
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v1, v5

    goto :goto_1

    .line 1763
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;-><init>()V

    aput-object v5, v2, v1

    .line 1764
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1765
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    goto :goto_0

    .line 1769
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    :sswitch_5
    const/16 v5, 0x2a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1771
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_5

    move v1, v4

    .line 1772
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1774
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v1, :cond_4

    .line 1775
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1777
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 1778
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1779
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1780
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1777
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1771
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v1, v5

    goto :goto_3

    .line 1783
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    aput-object v5, v2, v1

    .line 1784
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1785
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    goto/16 :goto_0

    .line 1724
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1599
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 8
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v6, 0x0

    .line 1654
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasSetByUser:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    if-eqz v2, :cond_1

    .line 1655
    :cond_0
    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->setByUser:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1657
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasTotal:Z

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_3

    .line 1658
    :cond_2
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->total:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1660
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->hasCirclesTotal:Z

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_5

    .line 1661
    :cond_4
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlesTotal:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1663
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 1664
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 1665
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->oBSOLETECirclesProfiles:[Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;

    aget-object v0, v2, v1

    .line 1666
    .local v0, "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    if-eqz v0, :cond_6

    .line 1667
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1664
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1671
    .end local v0    # "element":Lcom/google/android/finsky/protos/PlusData$OBSOLETE_PlusProfile;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 1672
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 1673
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;->circlePerson:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v0, v2, v1

    .line 1674
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    if-eqz v0, :cond_8

    .line 1675
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1672
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1679
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1680
    return-void
.end method
