.class Lcom/google/android/finsky/utils/SelfUpdateScheduler$2$1;
.super Ljava/lang/Object;
.source "SelfUpdateScheduler.java"

# interfaces
.implements Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2$1;->this$1:Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public installBeginning()V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method public installFailed(ILjava/lang/String;)V
    .locals 7
    .param p1, "errorCode"    # I
    .param p2, "exceptionType"    # Ljava/lang/String;

    .prologue
    .line 285
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x6f

    iget-object v2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2$1;->this$1:Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;

    iget-object v2, v2, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->val$packageName:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2$1;->this$1:Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;

    iget-object v4, v4, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->this$0:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    # getter for: Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-static {v4}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->access$100(Lcom/google/android/finsky/utils/SelfUpdateScheduler;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    move v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 289
    return-void
.end method

.method public installSucceeded()V
    .locals 4

    .prologue
    .line 280
    const-string v0, "Unexpected install success for %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2$1;->this$1:Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;

    iget-object v3, v3, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->val$packageName:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    return-void
.end method
