.class public Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;
.super Lcom/google/android/play/layout/ForegroundRelativeLayout;
.source "PlayQuickLinkBase.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field protected mLinkIcon:Lcom/google/android/play/image/FifeImageView;

.field protected mLinkText:Landroid/widget/TextView;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v0

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->initialize(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/Browse$QuickLink;ILcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 12
    .param p1, "quickLink"    # Lcom/google/android/finsky/protos/Browse$QuickLink;
    .param p2, "backendId"    # I
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 70
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 71
    iget-object v10, p1, Lcom/google/android/finsky/protos/Browse$QuickLink;->name:Ljava/lang/String;

    .line 73
    .local v10, "name":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    :cond_0
    iget-object v8, p1, Lcom/google/android/finsky/protos/Browse$QuickLink;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 77
    .local v8, "image":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v8, :cond_1

    .line 78
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    iget-object v2, v8, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v3, v8, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    move-object/from16 v0, p5

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 79
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 80
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 86
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 87
    .local v11, "res":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;

    move-result-object v7

    .line 88
    .local v7, "cardViewGroupDelegate":Lcom/google/android/play/cardview/CardViewGroupDelegate;
    iget-boolean v1, p1, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasPrismStyle:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p1, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle:Z

    if-eqz v1, :cond_2

    .line 89
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p1, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId:I

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v1

    invoke-interface {v7, p0, v1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundColor(Landroid/view/View;I)V

    .line 91
    const/4 v1, 0x0

    invoke-interface {v7, p0, v1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setCardElevation(Landroid/view/View;F)V

    .line 92
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    const v2, 0x7f09009c

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 93
    const v1, 0x7f020183

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 102
    :goto_1
    const v1, 0x7f0b0092

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 103
    .local v9, "linkInset":I
    invoke-virtual {p0, v9, v9, v9, v9}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setForegroundPadding(IIII)V

    .line 105
    iget-object v1, p1, Lcom/google/android/finsky/protos/Browse$QuickLink;->link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-eqz v1, :cond_3

    .line 106
    new-instance v1, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase$1;

    move-object v2, p0

    move-object v3, p3

    move-object v4, p1

    move v5, p2

    move-object/from16 v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase$1;-><init>(Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/protos/Browse$QuickLink;ILcom/google/android/finsky/api/model/DfeToc;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    const/16 v1, 0x65

    invoke-static {v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 118
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 119
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 122
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v1

    invoke-interface {v1, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 127
    :goto_2
    invoke-virtual {p0, v10}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 128
    return-void

    .line 82
    .end local v7    # "cardViewGroupDelegate":Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .end local v9    # "linkInset":I
    .end local v11    # "res":Landroid/content/res/Resources;
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 83
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0

    .line 95
    .restart local v7    # "cardViewGroupDelegate":Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .restart local v11    # "res":Landroid/content/res/Resources;
    :cond_2
    const v1, 0x7f090088

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-interface {v7, p0, v1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setBackgroundColor(Landroid/view/View;I)V

    .line 97
    const v1, 0x7f0b0091

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-interface {v7, p0, v1}, Lcom/google/android/play/cardview/CardViewGroupDelegate;->setCardElevation(Landroid/view/View;F)V

    .line 99
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    const v2, 0x7f09005b

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 100
    const v1, 0x7f020184

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 124
    .restart local v9    # "linkInset":I
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setClickable(Z)V

    goto :goto_2
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 195
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCardViewGroupDelegate()Lcom/google/android/play/cardview/CardViewGroupDelegate;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/play/cardview/CardViewGroupDelegates;->IMPL:Lcom/google/android/play/cardview/CardViewGroupDelegate;

    return-object v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onFinishInflate()V

    .line 57
    const v0, 0x7f0a0321

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    .line 58
    const v0, 0x7f0a01b6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    .line 59
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getPaddingLeft()I

    move-result v4

    .line 162
    .local v4, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getPaddingTop()I

    move-result v5

    .line 164
    .local v5, "paddingTop":I
    move v6, v4

    .line 165
    .local v6, "textLeft":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v7}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v7

    if-nez v7, :cond_1

    const/4 v0, 0x1

    .line 166
    .local v0, "hasIcon":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 167
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v7}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 169
    .local v2, "iconLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v7, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int v1, v4, v7

    .line 170
    .local v1, "iconLeft":I
    iget v7, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int v3, v5, v7

    .line 171
    .local v3, "iconTop":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v8}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v1

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v9}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v3

    invoke-virtual {v7, v1, v3, v8, v9}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 174
    iget v7, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v8}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v7, v8

    add-int/2addr v6, v7

    .line 177
    .end local v1    # "iconLeft":I
    .end local v2    # "iconLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v3    # "iconTop":I
    :cond_0
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v6

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v5

    invoke-virtual {v7, v6, v5, v8, v9}, Landroid/widget/TextView;->layout(IIII)V

    .line 179
    return-void

    .line 165
    .end local v0    # "hasIcon":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 13
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    .line 132
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    .line 133
    .local v9, "width":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 135
    .local v4, "height":I
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v10}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v10

    if-nez v10, :cond_1

    const/4 v3, 0x1

    .line 136
    .local v3, "hasIcon":Z
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getPaddingLeft()I

    move-result v10

    sub-int v10, v9, v10

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getPaddingRight()I

    move-result v11

    sub-int v2, v10, v11

    .line 137
    .local v2, "contentWidth":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getPaddingTop()I

    move-result v10

    sub-int v10, v4, v10

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->getPaddingBottom()I

    move-result v11

    sub-int v0, v10, v11

    .line 138
    .local v0, "contentHeight":I
    move v8, v2

    .line 140
    .local v8, "textWidth":I
    if-eqz v3, :cond_0

    .line 142
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v10}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 144
    .local v5, "iconLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v10, v0, v10

    iget v11, v5, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v6, v10, v11

    .line 145
    .local v6, "iconSize":I
    invoke-static {v6, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 146
    .local v7, "iconSpec":I
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkIcon:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v10, v7, v7}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 148
    iget v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v10, v6

    iget v11, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v10, v11

    sub-int/2addr v8, v10

    .line 151
    .end local v5    # "iconLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v6    # "iconSize":I
    .end local v7    # "iconSpec":I
    :cond_0
    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 152
    .local v1, "contentHeightSpec":I
    iget-object v10, p0, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->mLinkText:Landroid/widget/TextView;

    invoke-static {v8, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v10, v11, v1}, Landroid/widget/TextView;->measure(II)V

    .line 156
    invoke-virtual {p0, v9, v4}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setMeasuredDimension(II)V

    .line 157
    return-void

    .line 135
    .end local v0    # "contentHeight":I
    .end local v1    # "contentHeightSpec":I
    .end local v2    # "contentWidth":I
    .end local v3    # "hasIcon":Z
    .end local v8    # "textWidth":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method
