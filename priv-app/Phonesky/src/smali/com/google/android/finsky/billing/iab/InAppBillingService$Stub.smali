.class Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;
.super Lcom/android/vending/billing/IInAppBillingService$Stub;
.source "InAppBillingService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/iab/InAppBillingService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Stub"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    invoke-direct {p0}, Lcom/android/vending/billing/IInAppBillingService$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/billing/iab/InAppBillingService;
    .param p2, "x1"    # Lcom/google/android/finsky/billing/iab/InAppBillingService$1;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;)V

    return-void
.end method


# virtual methods
.method public consumePurchase(ILjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "purchaseToken"    # Ljava/lang/String;

    .prologue
    .line 112
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->validatePackageName(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v1

    .line 113
    .local v1, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v1, v2, :cond_0

    .line 114
    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    .line 120
    :goto_0
    return v2

    .line 116
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getAccountName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 117
    .local v0, "accountName":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 118
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    goto :goto_0

    .line 120
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v2, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->consumePurchase(ILjava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 8
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "sku"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "developerPayload"    # Ljava/lang/String;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->validatePackageName(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v0, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v7

    .line 84
    .local v7, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v7, v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v0, v7}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$400(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v0

    .line 91
    :goto_0
    return-object v0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getAccountName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 88
    .local v6, "accountName":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$400(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v0, v6}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 9
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p4, "newSku"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;
    .param p6, "developerPayload"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 141
    .local p3, "oldSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->validatePackageName(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v0, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v8

    .line 142
    .local v8, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v8, v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v0, v8}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$400(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getAccountName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 146
    .local v7, "accountName":Ljava/lang/String;
    if-nez v7, :cond_1

    .line 147
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$400(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v0, v7}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "continuationToken"    # Ljava/lang/String;

    .prologue
    .line 98
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->validatePackageName(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v1

    .line 99
    .local v1, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v1, v2, :cond_0

    .line 100
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v2, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$400(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    .line 106
    :goto_0
    return-object v2

    .line 102
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getAccountName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "accountName":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 104
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    sget-object v3, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$400(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto :goto_0

    .line 106
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v2, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    goto :goto_0
.end method

.method public getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "skusBundle"    # Landroid/os/Bundle;

    .prologue
    .line 69
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->validatePackageName(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v1

    .line 70
    .local v1, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v1, v2, :cond_0

    .line 71
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v2, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$400(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    .line 77
    :goto_0
    return-object v2

    .line 73
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getAccountName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "accountName":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 75
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    sget-object v3, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$400(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v2

    goto :goto_0

    .line 77
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v2, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    goto :goto_0
.end method

.method public isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->validatePackageName(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v1

    .line 56
    .local v1, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v1, v2, :cond_0

    .line 57
    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    .line 63
    :goto_0
    return v2

    .line 59
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getAccountName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "accountName":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 61
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    goto :goto_0

    .line 63
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v2, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public isPromoEligible(ILjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 126
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->validatePackageName(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$100(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v1

    .line 127
    .local v1, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v1, v2, :cond_0

    .line 128
    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    .line 134
    :goto_0
    return v2

    .line 130
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getAccountName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$200(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "accountName":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 132
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    goto :goto_0

    .line 134
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;->this$0:Lcom/google/android/finsky/billing/iab/InAppBillingService;

    # invokes: Lcom/google/android/finsky/billing/iab/InAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    invoke-static {v2, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->access$300(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v2

    invoke-virtual {v2, p1, p2, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->isPromoEligible(ILjava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method
