.class public Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "BillingProfileActivity.java"

# interfaces
.implements Lcom/google/android/finsky/billing/BillingProfileFragment$Listener;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mPurchaseContextToken:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;I)Landroid/content/Intent;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "purchaseContextToken"    # Ljava/lang/String;
    .param p2, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;
    .param p3, "offerType"    # I

    .prologue
    .line 65
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "BillingProfileActivity.account"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 67
    const-string v1, "BillingProfileActivity.purchaseContextToken"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    const-string v1, "BillingProfileActivity.docid"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 69
    const-string v1, "BillingProfileActivity.offerType"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 70
    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 5

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "BillingProfileActivity.fragment"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    move-object v1, v2

    check-cast v1, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;

    .line 128
    .local v1, "fragment":Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->mAccount:Landroid/accounts/Account;

    if-eqz v2, :cond_0

    .line 129
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    .line 130
    .local v0, "eventLogger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    const-wide/16 v2, 0x0

    const/16 v4, 0x25b

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 132
    .end local v0    # "eventLogger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 133
    return-void
.end method

.method public onCancel()V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->setResult(I)V

    .line 121
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->finish()V

    .line 122
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 77
    const v5, 0x7f040037

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->setContentView(I)V

    .line 79
    const v5, 0x7f0a009c

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 80
    .local v4, "titleView":Landroid/widget/TextView;
    const v5, 0x7f0c00d3

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 81
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "BillingProfileActivity.account"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/accounts/Account;

    iput-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->mAccount:Landroid/accounts/Account;

    .line 82
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "BillingProfileActivity.purchaseContextToken"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->mPurchaseContextToken:Ljava/lang/String;

    .line 84
    const v5, 0x7f0a022b

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 85
    .local v0, "accountView":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    const-string v6, "BillingProfileActivity.fragment"

    invoke-virtual {v5, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 89
    .local v2, "fragment":Landroid/support/v4/app/Fragment;
    if-nez v2, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 91
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "BillingProfileActivity.docid"

    invoke-static {v3, v5}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Common$Docid;

    .line 92
    .local v1, "docid":Lcom/google/android/finsky/protos/Common$Docid;
    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->mAccount:Landroid/accounts/Account;

    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->mPurchaseContextToken:Ljava/lang/String;

    const-string v7, "BillingProfileActivity.offerType"

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v5, v6, v1, v7}, Lcom/google/android/finsky/billing/BillingProfileFragment;->newInstance(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;I)Lcom/google/android/finsky/billing/BillingProfileFragment;

    move-result-object v2

    .line 94
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    const v6, 0x7f0a00c4

    const-string v7, "BillingProfileActivity.fragment"

    invoke-virtual {v5, v6, v2, v7}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 98
    .end local v1    # "docid":Lcom/google/android/finsky/protos/Common$Docid;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public onInstrumentSelected(Ljava/lang/String;)V
    .locals 2
    .param p1, "instrumentId"    # Ljava/lang/String;

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 105
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "BillingProfileActivity.selectedInstrumentId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->setResult(ILandroid/content/Intent;)V

    .line 107
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->finish()V

    .line 108
    return-void
.end method

.method public onPromoCodeRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)V
    .locals 2
    .param p1, "redeemCodeResult"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .prologue
    .line 112
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 113
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "BillingProfileActivity.redeemPromoCodeResult"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 114
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->setResult(ILandroid/content/Intent;)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileActivity;->finish()V

    .line 116
    return-void
.end method
