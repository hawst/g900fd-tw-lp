.class public Lcom/google/android/finsky/layout/EditorialDescriptionSection;
.super Lcom/google/android/finsky/layout/DetailsTextSection;
.source "EditorialDescriptionSection.java"


# instance fields
.field private mIsExpanded:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/EditorialDescriptionSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/DetailsTextSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method private collapseContent()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    iput-boolean v2, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mIsExpanded:Z

    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    iget v1, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mMaxCollapsedLines:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setBodyMaxLines(I)V

    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mFooterLabel:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 53
    return-void
.end method

.method private expandContent()V
    .locals 2

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mIsExpanded:Z

    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setBodyMaxLines(I)V

    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mFooterLabel:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 47
    return-void
.end method


# virtual methods
.method protected handleClick()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-virtual {p0, v1, v1}, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->scrollTo(II)V

    .line 30
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mUrlSpanClicked:Z

    if-eqz v0, :cond_0

    .line 32
    iput-boolean v1, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mUrlSpanClicked:Z

    .line 41
    :goto_0
    return-void

    .line 36
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->mIsExpanded:Z

    if-eqz v0, :cond_1

    .line 37
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->collapseContent()V

    goto :goto_0

    .line 39
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/EditorialDescriptionSection;->expandContent()V

    goto :goto_0
.end method
