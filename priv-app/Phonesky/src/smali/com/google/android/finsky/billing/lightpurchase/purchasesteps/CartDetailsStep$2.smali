.class Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$2;
.super Ljava/lang/Object;
.source "CartDetailsStep.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->populateVoucherViews(Landroid/view/LayoutInflater;Landroid/view/View;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

.field final synthetic val$newVoucherId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$2;->val$newVoucherId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->access$000(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x2ce

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 260
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;
    invoke-static {v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->access$200(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;)Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$2;->val$newVoucherId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->switchVoucher(Ljava/lang/String;)V

    .line 261
    return-void
.end method
