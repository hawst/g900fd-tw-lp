.class public Lcom/google/android/finsky/experiments/FinskyExperiments;
.super Ljava/lang/Object;
.source "FinskyExperiments.java"

# interfaces
.implements Lcom/google/android/finsky/experiments/Experiments;


# static fields
.field private static final sRecognizedExperiments:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

.field private final mEnabledExperiments:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mEnabledHeaderValue:Ljava/lang/String;

.field private mIsDetailsAlbumAllAccessEnabled:Ljava/lang/Boolean;

.field private mIsDetailsAlbumStartCoverExpandedEnabled:Ljava/lang/Boolean;

.field private mIsHideSalePricesExperimentEnabled:Ljava/lang/Boolean;

.field private mIsPostPurchaseXsellEnabledForAllCorpora:Ljava/lang/Boolean;

.field private mShouldEnableTransitionAnimations:Ljava/lang/Boolean;

.field private mShouldHideDownloadCountInDetailsTitle:Ljava/lang/Boolean;

.field private mUnsupportedHeaderValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 243
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    .line 246
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.show_buy_verb_in_button"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 247
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.purchase_button_show_wallet_3d_icon"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 248
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.hide_sale_prices"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 249
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.prompt_for_auth_choice_once"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 250
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.cleanup_auth_settings"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 251
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.prompt_for_fop"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 252
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.prompt_for_fop_ui_mode_radio_button"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 253
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.prompt_for_fop_ui_mode_billing_profile"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 254
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.prompt_for_fop_ui_mode_billing_profile_nested"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 255
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.prompt_for_fop_ui_mode_billing_profile_more_details"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 256
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.prompt_for_fop_ui_mode_billing_profile_not_now"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 257
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.setupwizard_prompt_for_fop_ui_mode_radio_button"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 258
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.setupwizard_prompt_for_fop_ui_mode_billing_profile"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 259
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.setupwizard_prompt_for_fop_ui_mode_billing_profile_more_details"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 260
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.cart_details_old_expander"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 261
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.hide_my_account"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 262
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.hide_edit_payment_my_account"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 263
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:ui.enable_post_purchase_xsell_for_all_corpora"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 264
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:details.album_start_cover_expanded"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 265
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:installer.force_through_sdcard"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 266
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:details.album_all_access_enabled"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 267
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:details.hide_download_count_in_title"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 268
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:auth.use_gms_core_based_auth"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 269
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:search.cap_local_suggestions_2"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 270
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:search.cap_local_suggestions_3"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 271
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:animations.transitions_enabled"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 272
    sget-object v0, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    const-string v1, "cl:search.use_dfe_for_music_search_suggestions"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 273
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    new-instance v0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 278
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    .line 290
    iput-object v1, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsHideSalePricesExperimentEnabled:Ljava/lang/Boolean;

    .line 293
    iput-object v1, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsPostPurchaseXsellEnabledForAllCorpora:Ljava/lang/Boolean;

    .line 296
    iput-object v1, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumStartCoverExpandedEnabled:Ljava/lang/Boolean;

    .line 302
    iput-object v1, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumAllAccessEnabled:Ljava/lang/Boolean;

    .line 305
    iput-object v1, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldHideDownloadCountInDetailsTitle:Ljava/lang/Boolean;

    .line 308
    iput-object v1, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldEnableTransitionAnimations:Ljava/lang/Boolean;

    .line 311
    iput-object p1, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mAccountName:Ljava/lang/String;

    .line 312
    invoke-direct {p0}, Lcom/google/android/finsky/experiments/FinskyExperiments;->loadExperimentsFromDisk()V

    .line 313
    return-void
.end method

.method private loadExperimentsFromDisk()V
    .locals 4

    .prologue
    .line 516
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->experimentList:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v3, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mAccountName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 517
    .local v0, "experiments":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/utils/Utils;->commaUnpackStrings(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 518
    .local v1, "splitExperiments":[Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/google/android/finsky/experiments/FinskyExperiments;->setExperimentsInternal([Ljava/lang/String;)V

    .line 519
    return-void
.end method

.method private setExperimentsInternal([Ljava/lang/String;)V
    .locals 10
    .param p1, "experiments"    # [Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 459
    iget-object v8, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-interface {v8}, Ljava/util/Set;->clear()V

    .line 460
    iput-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledHeaderValue:Ljava/lang/String;

    .line 461
    iput-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mUnsupportedHeaderValue:Ljava/lang/String;

    .line 464
    iput-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsHideSalePricesExperimentEnabled:Ljava/lang/Boolean;

    .line 465
    iput-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsPostPurchaseXsellEnabledForAllCorpora:Ljava/lang/Boolean;

    .line 466
    iput-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumStartCoverExpandedEnabled:Ljava/lang/Boolean;

    .line 467
    iput-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumAllAccessEnabled:Ljava/lang/Boolean;

    .line 468
    iput-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldHideDownloadCountInDetailsTitle:Ljava/lang/Boolean;

    .line 469
    iput-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldEnableTransitionAnimations:Ljava/lang/Boolean;

    .line 471
    new-instance v8, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-direct {v8}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 472
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 473
    .local v2, "clientAlteringExperiments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .line 475
    .local v6, "otherExperiments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v8, Lcom/google/android/finsky/config/G;->additionalExperiments:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/finsky/utils/Utils;->commaUnpackStrings(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 476
    .local v0, "additionalExperiments":[Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ArrayUtils;->concatenate([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "experiments":[Ljava/lang/String;
    check-cast p1, [Ljava/lang/String;

    .line 479
    .restart local p1    # "experiments":[Ljava/lang/String;
    sget-object v8, Lcom/google/android/finsky/api/DfeApiConfig;->showStagingData:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 480
    iget-object v8, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    const-string v9, "android_group:eng.finsky.merchandising.staging"

    invoke-interface {v8, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 481
    const-string v8, "android_group:eng.finsky.merchandising.staging"

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 484
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 487
    .local v7, "unsupportedExperiments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v1, p1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v3, v1, v4

    .line 488
    .local v3, "experiment":Ljava/lang/String;
    sget-object v8, Lcom/google/android/finsky/experiments/FinskyExperiments;->sRecognizedExperiments:Ljava/util/Set;

    invoke-interface {v8, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 489
    iget-object v8, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-interface {v8, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 490
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 487
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 492
    :cond_1
    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 493
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 498
    .end local v3    # "experiment":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_3

    .line 499
    iget-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    iput-object v8, v9, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    .line 502
    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_4

    .line 503
    iget-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v8, v8, [Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    iput-object v8, v9, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->otherExperiment:[Ljava/lang/String;

    .line 508
    :cond_4
    const-string v8, ","

    iget-object v9, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-static {v8, v9}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledHeaderValue:Ljava/lang/String;

    .line 509
    const-string v8, ","

    invoke-static {v8, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mUnsupportedHeaderValue:Ljava/lang/String;

    .line 510
    return-void
.end method


# virtual methods
.method public declared-synchronized getActiveExperiments()Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    .locals 1

    .prologue
    .line 327
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mActiveExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getEnabledExperimentsHeaderValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 423
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledHeaderValue:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getUnsupportedExperimentsHeaderValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 431
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mUnsupportedHeaderValue:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasEnabledExperiments()Z
    .locals 1

    .prologue
    .line 407
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized hasUnsupportedExperiments()Z
    .locals 1

    .prologue
    .line 415
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mUnsupportedHeaderValue:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isDetailsAlbumAllAccessEnabled()Z
    .locals 2

    .prologue
    .line 373
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumAllAccessEnabled:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    const-string v1, "cl:details.album_all_access_enabled"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumAllAccessEnabled:Ljava/lang/Boolean;

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumAllAccessEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isDetailsAlbumStartCoverExpandedEnabled()Z
    .locals 2

    .prologue
    .line 362
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumStartCoverExpandedEnabled:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    const-string v1, "cl:details.album_start_cover_expanded"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumStartCoverExpandedEnabled:Ljava/lang/Boolean;

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsDetailsAlbumStartCoverExpandedEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isEnabled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "experimentId"    # Ljava/lang/String;

    .prologue
    .line 332
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isHideSalesPricesExperimentEnabled()Z
    .locals 2

    .prologue
    .line 340
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsHideSalePricesExperimentEnabled:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    const-string v1, "cl:billing.hide_sale_prices"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsHideSalePricesExperimentEnabled:Ljava/lang/Boolean;

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsHideSalePricesExperimentEnabled:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isPostPurchaseXsellEnabledForAllCorpora()Z
    .locals 2

    .prologue
    .line 351
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsPostPurchaseXsellEnabledForAllCorpora:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    const-string v1, "cl:ui.enable_post_purchase_xsell_for_all_corpora"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsPostPurchaseXsellEnabledForAllCorpora:Ljava/lang/Boolean;

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mIsPostPurchaseXsellEnabledForAllCorpora:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 351
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setExperiments([Ljava/lang/String;)V
    .locals 4
    .param p1, "experiments"    # [Ljava/lang/String;

    .prologue
    .line 436
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->experimentList:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v3, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mAccountName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v1

    .line 437
    .local v1, "preference":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/String;>;"
    invoke-static {p1}, Lcom/google/android/finsky/utils/Utils;->commaPackStrings([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 442
    .local v0, "joinedExperiments":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 450
    :goto_0
    monitor-exit p0

    return-void

    .line 448
    :cond_0
    :try_start_1
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 449
    invoke-direct {p0, p1}, Lcom/google/android/finsky/experiments/FinskyExperiments;->setExperimentsInternal([Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 436
    .end local v0    # "joinedExperiments":Ljava/lang/String;
    .end local v1    # "preference":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/String;>;"
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public declared-synchronized shouldEnableTransitionAnimations()Z
    .locals 2

    .prologue
    .line 395
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldEnableTransitionAnimations:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    const-string v1, "cl:animations.transitions_enabled"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldEnableTransitionAnimations:Ljava/lang/Boolean;

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldEnableTransitionAnimations:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 395
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized shouldHideDownloadCountInDetailsTitle()Z
    .locals 2

    .prologue
    .line 384
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldHideDownloadCountInDetailsTitle:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 385
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mEnabledExperiments:Ljava/util/Set;

    const-string v1, "cl:details.hide_download_count_in_title"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldHideDownloadCountInDetailsTitle:Ljava/lang/Boolean;

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/experiments/FinskyExperiments;->mShouldHideDownloadCountInDetailsTitle:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
