.class public Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;
.super Landroid/view/View;
.source "PlayCardViewSuggestionOverlay.java"


# instance fields
.field private final mOverlayDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 28
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0200d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    .line 29
    const v1, 0x7f020061

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;->setBackgroundResource(I)V

    .line 30
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;->setWillNotDraw(Z)V

    .line 31
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 35
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;->getWidth()I

    move-result v10

    .line 38
    .local v10, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;->getHeight()I

    move-result v0

    .line 40
    .local v0, "height":I
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 41
    .local v3, "overlayIntrinsicWidth":I
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 43
    .local v2, "overlayIntrinsicHeight":I
    if-gt v3, v10, :cond_0

    move v8, v9

    .line 45
    .local v8, "scaleX":F
    :goto_0
    if-gt v2, v0, :cond_1

    .line 47
    .local v9, "scaleY":F
    :goto_1
    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v7

    .line 49
    .local v7, "scale":F
    int-to-float v11, v3

    mul-float/2addr v11, v7

    float-to-int v4, v11

    .line 50
    .local v4, "overlayWidth":I
    int-to-float v11, v2

    mul-float/2addr v11, v7

    float-to-int v1, v11

    .line 52
    .local v1, "overlayHeight":I
    sub-int v11, v10, v4

    div-int/lit8 v5, v11, 0x2

    .line 53
    .local v5, "overlayX":I
    sub-int v11, v0, v1

    div-int/lit8 v6, v11, 0x2

    .line 55
    .local v6, "overlayY":I
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    add-int v12, v5, v4

    add-int v13, v6, v1

    invoke-virtual {v11, v5, v6, v12, v13}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 57
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayCardViewSuggestionOverlay;->mOverlayDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 58
    return-void

    .line 43
    .end local v1    # "overlayHeight":I
    .end local v4    # "overlayWidth":I
    .end local v5    # "overlayX":I
    .end local v6    # "overlayY":I
    .end local v7    # "scale":F
    .end local v8    # "scaleX":F
    .end local v9    # "scaleY":F
    :cond_0
    int-to-float v11, v10

    int-to-float v12, v3

    div-float v8, v11, v12

    goto :goto_0

    .line 45
    .restart local v8    # "scaleX":F
    :cond_1
    int-to-float v11, v0

    int-to-float v12, v2

    div-float v9, v11, v12

    goto :goto_1
.end method
