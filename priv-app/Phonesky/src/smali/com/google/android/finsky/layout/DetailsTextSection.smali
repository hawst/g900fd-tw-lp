.class public Lcom/google/android/finsky/layout/DetailsTextSection;
.super Lcom/google/android/finsky/layout/ForegroundLinearLayout;
.source "DetailsTextSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;,
        Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;,
        Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraCredits;
    }
.end annotation


# instance fields
.field protected mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

.field private mCalloutView:Lcom/google/android/play/layout/PlayTextView;

.field private mExpandedBackend:I

.field private mExpandedBody:Ljava/lang/CharSequence;

.field private mExpandedBodyHeader:Ljava/lang/String;

.field private mExpandedCallout:Ljava/lang/CharSequence;

.field private mExpandedCalloutGravity:I

.field private mExpandedPromoteWhatsNew:Z

.field private mExpandedTranslatedBody:Ljava/lang/CharSequence;

.field private mExpandedWhatsNew:Ljava/lang/CharSequence;

.field private mExpandedWhatsNewHeader:Ljava/lang/CharSequence;

.field private mExtraAttributions:Ljava/lang/String;

.field private mExtraCreditsHeader:Ljava/lang/String;

.field private mExtraCreditsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraCredits;",
            ">;"
        }
    .end annotation
.end field

.field private mExtraPrimaryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;",
            ">;"
        }
    .end annotation
.end field

.field private mExtraSecondaryList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;",
            ">;"
        }
    .end annotation
.end field

.field protected mFooterLabel:Landroid/widget/TextView;

.field private mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

.field protected final mMaxCollapsedLines:I

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;

.field private mSpacerView:Landroid/view/View;

.field private final mTopDeveloperColor:I

.field private mTopDeveloperLabel:Lcom/google/android/finsky/layout/DecoratedTextView;

.field protected mUrlSpanClicked:Z

.field private final mWhatsNewVerticalMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 151
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 156
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 157
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0e0019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mMaxCollapsedLines:I

    .line 158
    const v1, 0x7f0900ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mTopDeveloperColor:I

    .line 159
    const v1, 0x7f0b00fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mWhatsNewVerticalMargin:I

    .line 161
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/DetailsTextSection;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsTextSection;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method private addProductDetails(Lcom/google/android/finsky/api/model/Document;)V
    .locals 14
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 313
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasProductDetails()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 314
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getProductDetails()Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    move-result-object v10

    .line 315
    .local v10, "productDetails":Lcom/google/android/finsky/protos/DocDetails$ProductDetails;
    iget-object v0, v10, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    array-length v11, v0

    .line 316
    .local v11, "productDetailsSectionCount":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v11, :cond_2

    .line 317
    iget-object v0, v10, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    aget-object v12, v0, v8

    .line 318
    .local v12, "section":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    iget-object v0, v12, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    array-length v7, v0

    .line 319
    .local v7, "descriptionCount":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_1
    if-ge v9, v7, :cond_1

    .line 320
    iget-object v0, v12, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    aget-object v6, v0, v9

    .line 321
    .local v6, "description":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    iget-object v0, v6, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_0

    .line 322
    iget-object v13, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    new-instance v0, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    iget-object v1, v12, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->title:Ljava/lang/String;

    iget-object v2, v6, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;->description:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, v6, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;->image:Lcom/google/android/finsky/protos/Common$Image;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Z)V

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 319
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    iget-object v2, v12, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->title:Ljava/lang/String;

    iget-object v3, v6, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;->description:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 316
    .end local v6    # "description":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 331
    .end local v7    # "descriptionCount":I
    .end local v8    # "i":I
    .end local v9    # "j":I
    .end local v10    # "productDetails":Lcom/google/android/finsky/protos/DocDetails$ProductDetails;
    .end local v11    # "productDetailsSectionCount":I
    .end local v12    # "section":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    :cond_2
    return-void
.end method

.method private bind(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Ljava/lang/CharSequence;IILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILcom/google/android/finsky/activities/TextSectionStateListener;)V
    .locals 12
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "callout"    # Ljava/lang/CharSequence;
    .param p4, "calloutGravity"    # I
    .param p5, "calloutMaxLines"    # I
    .param p6, "bodyHeader"    # Ljava/lang/String;
    .param p7, "body"    # Ljava/lang/CharSequence;
    .param p8, "translatedBody"    # Ljava/lang/CharSequence;
    .param p9, "whatsNew"    # Ljava/lang/CharSequence;
    .param p10, "hideWhatsNewInCollapsed"    # Z
    .param p11, "collapsedMaxLines"    # I
    .param p12, "sectionStateListener"    # Lcom/google/android/finsky/activities/TextSectionStateListener;

    .prologue
    .line 542
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 543
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;

    .line 545
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 546
    .local v3, "context":Landroid/content/Context;
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 548
    .local v7, "res":Landroid/content/res/Resources;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->syncCollapsedExtraIcons()V

    .line 550
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mFooterLabel:Landroid/widget/TextView;

    if-eqz v8, :cond_0

    .line 551
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mFooterLabel:Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v9

    invoke-static {v3, v9}, Lcom/google/android/play/utils/PlayCorpusUtils;->getPrimaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 556
    :cond_0
    invoke-direct {p0, p3}, Lcom/google/android/finsky/layout/DetailsTextSection;->selfishifyUrlSpans(Ljava/lang/CharSequence;)V

    .line 557
    move-object/from16 v0, p7

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->selfishifyUrlSpans(Ljava/lang/CharSequence;)V

    .line 558
    move-object/from16 v0, p8

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->selfishifyUrlSpans(Ljava/lang/CharSequence;)V

    .line 559
    move-object/from16 v0, p9

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->selfishifyUrlSpans(Ljava/lang/CharSequence;)V

    .line 561
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    .line 562
    .local v4, "docType":I
    const/4 v8, 0x1

    if-ne v4, v8, :cond_1

    const/4 v6, 0x1

    .line 563
    .local v6, "isApp":Z
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    const/4 v5, 0x1

    .line 565
    .local v5, "hasCallout":Z
    :goto_1
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedBackend:I

    .line 566
    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedCallout:Ljava/lang/CharSequence;

    .line 567
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedCalloutGravity:I

    .line 568
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedBodyHeader:Ljava/lang/String;

    .line 569
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedBody:Ljava/lang/CharSequence;

    .line 570
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedTranslatedBody:Ljava/lang/CharSequence;

    .line 571
    const v8, 0x7f0c0216

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedWhatsNewHeader:Ljava/lang/CharSequence;

    .line 572
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedWhatsNew:Ljava/lang/CharSequence;

    .line 574
    if-eqz v5, :cond_4

    .line 575
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mCalloutView:Lcom/google/android/play/layout/PlayTextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    .line 576
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mCalloutView:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v8, p3}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 577
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mCalloutView:Lcom/google/android/play/layout/PlayTextView;

    move/from16 v0, p5

    invoke-virtual {v8, v0}, Lcom/google/android/play/layout/PlayTextView;->setMaxLines(I)V

    .line 578
    iget-object v9, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mCalloutView:Lcom/google/android/play/layout/PlayTextView;

    if-eqz v6, :cond_3

    const/4 v8, 0x1

    :goto_2
    invoke-virtual {v9, v8}, Lcom/google/android/play/layout/PlayTextView;->setGravity(I)V

    .line 583
    :goto_3
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mSpacerView:Landroid/view/View;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 586
    if-nez p10, :cond_5

    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 588
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    iget-object v9, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedWhatsNewHeader:Ljava/lang/CharSequence;

    move-object/from16 v0, p9

    move/from16 v1, p11

    invoke-virtual {v8, v9, v0, v1}, Lcom/google/android/finsky/layout/DetailsTextBlock;->bind(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 589
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v9

    iget v10, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mWhatsNewVerticalMargin:I

    iget v11, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mWhatsNewVerticalMargin:I

    invoke-virtual {v8, v9, v10, v11}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setCorpusStyle(III)V

    .line 591
    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedPromoteWhatsNew:Z

    .line 606
    :goto_4
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->configureContentClicks()V

    .line 608
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/DetailsTextSection;->setVisibility(I)V

    .line 609
    return-void

    .line 562
    .end local v5    # "hasCallout":Z
    .end local v6    # "isApp":Z
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 563
    .restart local v6    # "isApp":Z
    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 578
    .restart local v5    # "hasCallout":Z
    :cond_3
    const/4 v8, 0x3

    goto :goto_2

    .line 580
    :cond_4
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mCalloutView:Lcom/google/android/play/layout/PlayTextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto :goto_3

    .line 592
    :cond_5
    if-eqz v6, :cond_6

    if-eqz v5, :cond_6

    .line 594
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setVisibility(I)V

    goto :goto_4

    .line 596
    :cond_6
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    move/from16 v2, p11

    invoke-virtual {v8, v0, v1, v2}, Lcom/google/android/finsky/layout/DetailsTextBlock;->bind(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 597
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/DetailsTextBlock;->removeCorpusStyle()V

    .line 598
    if-nez v5, :cond_7

    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 601
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mSpacerView:Landroid/view/View;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    .line 603
    :cond_7
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedPromoteWhatsNew:Z

    goto :goto_4
.end method

.method private configureContentClicks()V
    .locals 2

    .prologue
    .line 657
    new-instance v0, Lcom/google/android/finsky/layout/DetailsTextSection$2;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/DetailsTextSection$2;-><init>(Lcom/google/android/finsky/layout/DetailsTextSection;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 666
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    new-instance v1, Lcom/google/android/finsky/layout/DetailsTextSection$3;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/DetailsTextSection$3;-><init>(Lcom/google/android/finsky/layout/DetailsTextSection;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setBodyClickListener(Landroid/view/View$OnClickListener;)V

    .line 672
    return-void
.end method

.method private expandContent()V
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;

    if-eqz v0, :cond_0

    .line 686
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/activities/TextSectionStateListener;->onSectionExpanded(I)V

    .line 688
    :cond_0
    return-void
.end method

.method private fillContentRatingDetails(Lcom/google/android/finsky/api/model/Document;)V
    .locals 11
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x1

    .line 288
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v8

    .line 289
    .local v8, "docType":I
    if-eq v8, v5, :cond_0

    .line 305
    :goto_0
    return-void

    .line 292
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBadgeForContentRating()Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v6

    .line 293
    .local v6, "contentRatingBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    if-nez v6, :cond_1

    .line 294
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 295
    .local v7, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v2, 0x7f0c03d1

    invoke-virtual {v7, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getNormalizedContentRating()I

    move-result v3

    invoke-static {v7, v3}, Lcom/google/android/finsky/activities/ContentFilterActivity;->getLabel(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 302
    .end local v7    # "context":Landroid/content/Context;
    :cond_1
    iget-object v9, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    new-instance v0, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    iget-object v1, v6, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->title:Ljava/lang/String;

    iget-object v2, v6, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->description:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, v6, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->image:[Lcom/google/android/finsky/protos/Common$Image;

    const/4 v10, 0x0

    aget-object v4, v4, v10

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Z)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private fillExtraAttributionsString(Lcom/google/android/finsky/api/model/Document;)V
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 516
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAttributions()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraAttributions:Ljava/lang/String;

    .line 517
    return-void
.end method

.method private fillExtraCreditsList(Lcom/google/android/finsky/api/model/Document;)V
    .locals 10
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 264
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getVideoCredits()[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    move-result-object v4

    .line 265
    .local v4, "videoCredits":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    if-eqz v4, :cond_0

    array-length v5, v4

    if-nez v5, :cond_1

    .line 275
    :cond_0
    return-void

    .line 269
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0c0217

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraCreditsHeader:Ljava/lang/String;

    .line 270
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraCreditsList:Ljava/util/List;

    .line 271
    move-object v0, v4

    .local v0, "arr$":[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 272
    .local v3, "videoCredit":Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraCreditsList:Ljava/util/List;

    new-instance v6, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraCredits;

    iget-object v7, v3, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    const-string v8, ", "

    iget-object v9, v3, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    invoke-static {v8, v9}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraCredits;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private fillExtraPrimaryList(Lcom/google/android/finsky/api/model/Document;)V
    .locals 17
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 335
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasCreatorBadges()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 336
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCreatorBadges()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v7

    .local v7, "arr$":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    array-length v15, v7

    .local v15, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_0
    if-ge v13, v15, :cond_0

    aget-object v11, v7, v13

    .line 337
    .local v11, "creatorBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    const/4 v1, 0x6

    invoke-static {v11, v1}, Lcom/google/android/finsky/utils/BadgeUtils;->getImage(Lcom/google/android/finsky/protos/DocAnnotations$Badge;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v5

    .line 338
    .local v5, "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    iget-object v2, v11, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->title:Ljava/lang/String;

    iget-object v3, v11, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->description:Ljava/lang/String;

    iget-object v4, v11, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->browseUrl:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Z)V

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 343
    .end local v5    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v7    # "arr$":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v11    # "creatorBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v13    # "i$":I
    .end local v15    # "len$":I
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasItemBadges()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 344
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getItemBadges()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v7

    .restart local v7    # "arr$":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    array-length v15, v7

    .restart local v15    # "len$":I
    const/4 v13, 0x0

    .restart local v13    # "i$":I
    :goto_1
    if-ge v13, v15, :cond_1

    aget-object v14, v7, v13

    .line 345
    .local v14, "itemBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    const/4 v1, 0x6

    invoke-static {v14, v1}, Lcom/google/android/finsky/utils/BadgeUtils;->getImage(Lcom/google/android/finsky/protos/DocAnnotations$Badge;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v5

    .line 346
    .restart local v5    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    iget-object v2, v14, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->title:Ljava/lang/String;

    iget-object v3, v14, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->description:Ljava/lang/String;

    iget-object v4, v14, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->browseUrl:Ljava/lang/String;

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Z)V

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 351
    .end local v5    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v7    # "arr$":[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v13    # "i$":I
    .end local v14    # "itemBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v15    # "len$":I
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasBadgeContainer()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 352
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBadgeContainer()Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    move-result-object v9

    .line 353
    .local v9, "badgeContainer":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    iget-object v1, v9, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->badge:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v10, v1

    .line 354
    .local v10, "badgeCount":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    if-ge v12, v10, :cond_2

    .line 355
    iget-object v1, v9, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->badge:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v8, v1, v12

    .line 356
    .local v8, "badge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    const/4 v1, 0x6

    invoke-static {v8, v1}, Lcom/google/android/finsky/utils/BadgeUtils;->getImage(Lcom/google/android/finsky/protos/DocAnnotations$Badge;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v5

    .line 357
    .restart local v5    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    move-object/from16 v16, v0

    new-instance v1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    iget-object v2, v8, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->title:Ljava/lang/String;

    iget-object v3, v8, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->description:Ljava/lang/String;

    iget-object v4, v8, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->browseUrl:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Z)V

    move-object/from16 v0, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 354
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 361
    .end local v5    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v8    # "badge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v9    # "badgeContainer":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    .end local v10    # "badgeCount":I
    .end local v12    # "i":I
    :cond_2
    return-void
.end method

.method private fillExtraSecondaryList(Lcom/google/android/finsky/api/model/Document;)V
    .locals 20
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 364
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 365
    .local v6, "context":Landroid/content/Context;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v9

    .line 366
    .local v9, "docType":I
    sparse-switch v9, :sswitch_data_0

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 368
    :sswitch_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v4

    .line 369
    .local v4, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    iget-object v15, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_1

    .line 370
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03cc

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    :cond_1
    iget-object v15, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->uploadDate:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_2

    .line 374
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03cd

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->uploadDate:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    :cond_2
    iget-object v15, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_3

    .line 378
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03ce

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 381
    :cond_3
    iget-boolean v15, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasInstallationSize:Z

    if-eqz v15, :cond_0

    iget-wide v0, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-lez v15, :cond_0

    .line 382
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03cf

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-wide v0, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-static {v6, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 388
    .end local v4    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBookDetails()Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    move-result-object v5

    .line 389
    .local v5, "bookDetails":Lcom/google/android/finsky/protos/DocDetails$BookDetails;
    iget-object v15, v5, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->publisher:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_4

    .line 390
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03d2

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v5, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->publisher:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_4
    iget-boolean v15, v5, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->hasNumberOfPages:Z

    if-eqz v15, :cond_5

    .line 394
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03d3

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget v0, v5, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->numberOfPages:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 398
    :cond_5
    iget-object v15, v5, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->publicationDate:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_6

    .line 400
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03d4

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v5, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->publicationDate:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/DateUtils;->formatIso8601Date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 407
    :cond_6
    :goto_1
    iget-object v15, v5, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->isbn:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 408
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03d5

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v5, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->isbn:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 403
    :catch_0
    move-exception v10

    .line 404
    .local v10, "e":Ljava/text/ParseException;
    const-string v15, "Cannot parse ISO 8601 date"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v10, v15, v0}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 413
    .end local v5    # "bookDetails":Lcom/google/android/finsky/protos/DocDetails$BookDetails;
    .end local v10    # "e":Ljava/text/ParseException;
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getVideoDetails()Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    move-result-object v13

    .line 414
    .local v13, "videoDetails":Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    iget-object v15, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_a

    .line 415
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03d6

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 422
    :goto_2
    iget-object v15, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_7

    .line 423
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03d8

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    :cond_7
    iget-object v15, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_8

    .line 428
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03d9

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 431
    :cond_8
    iget-object v15, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    array-length v15, v15

    if-lez v15, :cond_9

    .line 432
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03da

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    const-string v18, ","

    iget-object v0, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->audioLanguage:[Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v18 .. v19}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 436
    :cond_9
    iget-object v15, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    array-length v15, v15

    if-lez v15, :cond_0

    .line 437
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03db

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    const-string v18, ","

    iget-object v0, v13, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->captionLanguage:[Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v18 .. v19}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 418
    :cond_a
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03d6

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    const v18, 0x7f0c03d7

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 446
    .end local v13    # "videoDetails":Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getMagazineDetails()Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    move-result-object v12

    .line 447
    .local v12, "magazineDetails":Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;
    if-eqz v12, :cond_c

    .line 448
    iget-object v15, v12, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deliveryFrequencyDescription:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_b

    .line 449
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03dc

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v12, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deliveryFrequencyDescription:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 453
    :cond_b
    iget-object v15, v12, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->psvDescription:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_c

    .line 454
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03dd

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v12, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->psvDescription:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 460
    :cond_c
    move-object/from16 v8, p1

    .line 461
    .local v8, "currentIssue":Lcom/google/android/finsky/api/model/Document;
    const/16 v15, 0x10

    if-eq v9, v15, :cond_d

    const/16 v15, 0x18

    if-ne v9, v15, :cond_e

    .line 462
    :cond_d
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/utils/DocUtils;->getMagazineCurrentIssueDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v8

    .line 464
    :cond_e
    if-eqz v8, :cond_0

    .line 465
    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getMagazineDetails()Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    move-result-object v11

    .line 466
    .local v11, "issueDetails":Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;
    if-eqz v11, :cond_0

    iget-object v15, v11, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deviceAvailabilityDescriptionHtml:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_0

    .line 468
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03de

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v11, Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;->deviceAvailabilityDescriptionHtml:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 475
    .end local v8    # "currentIssue":Lcom/google/android/finsky/api/model/Document;
    .end local v11    # "issueDetails":Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;
    .end local v12    # "magazineDetails":Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;
    :sswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    move-result-object v2

    .line 476
    .local v2, "albumDetailWrapper":Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;
    if-eqz v2, :cond_0

    .line 477
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    .line 478
    .local v3, "albumDetails":Lcom/google/android/finsky/protos/DocDetails$MusicDetails;
    iget-object v15, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->originalReleaseDate:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_f

    .line 480
    :try_start_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03df

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    iget-object v0, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->originalReleaseDate:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/DateUtils;->formatIso8601Date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    .line 487
    :cond_f
    :goto_3
    iget-object v15, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_10

    .line 491
    iget-object v15, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v15

    if-nez v15, :cond_11

    iget-object v15, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x4

    move/from16 v0, v16

    if-lt v15, v0, :cond_11

    .line 493
    iget-object v15, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseDate:Ljava/lang/String;

    const/16 v16, 0x0

    const/16 v17, 0x4

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 494
    .local v14, "year":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getContext()Landroid/content/Context;

    move-result-object v15

    const v16, 0x7f0c03e3

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v14, v17, v18

    const/16 v18, 0x1

    iget-object v0, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    move-object/from16 v19, v0

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 501
    .end local v14    # "year":Ljava/lang/String;
    .local v7, "copyrightText":Ljava/lang/String;
    :goto_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03e0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v0, v1, v7}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 504
    .end local v7    # "copyrightText":Ljava/lang/String;
    :cond_10
    iget-object v15, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    array-length v15, v15

    if-lez v15, :cond_0

    .line 505
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    new-instance v16, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    const v17, 0x7f0c03e1

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    const-string v18, ","

    iget-object v0, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->genre:[Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v18 .. v19}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v16 .. v18}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 483
    :catch_1
    move-exception v10

    .line 484
    .restart local v10    # "e":Ljava/text/ParseException;
    const-string v15, "Cannot parse ISO 8601 date"

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v10, v15, v0}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 498
    .end local v10    # "e":Ljava/text/ParseException;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getContext()Landroid/content/Context;

    move-result-object v15

    const v16, 0x7f0c03e2

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    iget-object v0, v3, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->label:Ljava/lang/String;

    move-object/from16 v19, v0

    aput-object v19, v17, v18

    invoke-virtual/range {v15 .. v17}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "copyrightText":Ljava/lang/String;
    goto :goto_4

    .line 366
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_4
        0x5 -> :sswitch_1
        0x6 -> :sswitch_2
        0x10 -> :sswitch_3
        0x11 -> :sswitch_3
        0x18 -> :sswitch_3
        0x19 -> :sswitch_3
    .end sparse-switch
.end method

.method private selfishifyUrlSpans(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "string"    # Ljava/lang/CharSequence;

    .prologue
    .line 626
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 654
    :goto_0
    return-void

    .line 630
    :cond_0
    new-instance v0, Lcom/google/android/finsky/layout/DetailsTextSection$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/DetailsTextSection$1;-><init>(Lcom/google/android/finsky/layout/DetailsTextSection;)V

    invoke-static {p1, v0}, Lcom/google/android/play/utils/UrlSpanUtils;->selfishifyUrlSpans(Ljava/lang/CharSequence;Lcom/google/android/play/utils/UrlSpanUtils$Listener;)V

    goto :goto_0
.end method


# virtual methods
.method public bindAboutAuthor(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;ZLandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/activities/TextSectionStateListener;)V
    .locals 13
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "hasDetailsDataLoaded"    # Z
    .param p4, "savedState"    # Landroid/os/Bundle;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p6, "sectionStateListener"    # Lcom/google/android/finsky/activities/TextSectionStateListener;

    .prologue
    .line 523
    if-eqz p3, :cond_0

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBookDetails()Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBookDetails()Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->hasAboutTheAuthor:Z

    if-nez v0, :cond_1

    .line 525
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->setVisibility(I)V

    .line 535
    :goto_0
    return-void

    .line 528
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBookDetails()Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->aboutTheAuthor:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/utils/FastHtmlParser;->fromHtml(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 530
    .local v7, "aboutAuthorText":Ljava/lang/CharSequence;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c035c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    .line 532
    .local v6, "header":Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x3

    const v5, 0x7fffffff

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    iget v11, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mMaxCollapsedLines:I

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v12, p6

    invoke-direct/range {v0 .. v12}, Lcom/google/android/finsky/layout/DetailsTextSection;->bind(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Ljava/lang/CharSequence;IILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILcom/google/android/finsky/activities/TextSectionStateListener;)V

    goto :goto_0
.end method

.method public bindDescription(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;ZZLandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/activities/TextSectionStateListener;)V
    .locals 17
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "hasDetailsDataLoaded"    # Z
    .param p4, "isOwned"    # Z
    .param p5, "savedState"    # Landroid/os/Bundle;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p7, "sectionStateListener"    # Lcom/google/android/finsky/activities/TextSectionStateListener;

    .prologue
    .line 181
    if-nez p3, :cond_0

    .line 182
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/DetailsTextSection;->setVisibility(I)V

    .line 220
    :goto_0
    return-void

    .line 185
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getRawDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    .line 186
    .local v9, "fullDesc":Ljava/lang/CharSequence;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getRawTranslatedDescription()Ljava/lang/String;

    move-result-object v16

    .line 187
    .local v16, "translated":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v10, 0x0

    .line 189
    .local v10, "translatedDesc":Ljava/lang/CharSequence;
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getPromotionalDescription()Ljava/lang/String;

    move-result-object v5

    .line 190
    .local v5, "callout":Ljava/lang/CharSequence;
    const/4 v6, 0x1

    .line 191
    .local v6, "calloutGravity":I
    const v7, 0x7fffffff

    .line 193
    .local v7, "calloutMaxLines":I
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->hasCreatorBadges()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 194
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getCreatorBadges()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v15, v2, v3

    .line 195
    .local v15, "creatorBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mTopDeveloperLabel:Lcom/google/android/finsky/layout/DecoratedTextView;

    iget-object v3, v15, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mTopDeveloperLabel:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mTopDeveloperColor:I

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->setTextColor(I)V

    .line 197
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mTopDeveloperLabel:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/utils/BadgeUtils;->configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mTopDeveloperLabel:Lcom/google/android/finsky/layout/DecoratedTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    .line 205
    .end local v15    # "creatorBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsTextSection;->updateExtras(Lcom/google/android/finsky/api/model/Document;)V

    .line 208
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 209
    move-object v5, v9

    .line 210
    const/4 v6, 0x3

    .line 211
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mMaxCollapsedLines:I

    .line 212
    const/4 v9, 0x0

    .line 215
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->hasWhatsNew()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getWhatsNew()Ljava/lang/CharSequence;

    move-result-object v11

    .line 217
    .local v11, "whatsNew":Ljava/lang/CharSequence;
    :goto_3
    const/4 v8, 0x0

    if-nez p4, :cond_5

    const/4 v12, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mMaxCollapsedLines:I

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v14, p7

    invoke-direct/range {v2 .. v14}, Lcom/google/android/finsky/layout/DetailsTextSection;->bind(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Ljava/lang/CharSequence;IILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILcom/google/android/finsky/activities/TextSectionStateListener;)V

    goto/16 :goto_0

    .line 187
    .end local v5    # "callout":Ljava/lang/CharSequence;
    .end local v6    # "calloutGravity":I
    .end local v7    # "calloutMaxLines":I
    .end local v10    # "translatedDesc":Ljava/lang/CharSequence;
    .end local v11    # "whatsNew":Ljava/lang/CharSequence;
    :cond_2
    invoke-static/range {v16 .. v16}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    goto/16 :goto_1

    .line 201
    .restart local v5    # "callout":Ljava/lang/CharSequence;
    .restart local v6    # "calloutGravity":I
    .restart local v7    # "calloutMaxLines":I
    .restart local v10    # "translatedDesc":Ljava/lang/CharSequence;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mTopDeveloperLabel:Lcom/google/android/finsky/layout/DecoratedTextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    goto :goto_2

    .line 215
    :cond_4
    const/4 v11, 0x0

    goto :goto_3

    .line 217
    .restart local v11    # "whatsNew":Ljava/lang/CharSequence;
    :cond_5
    const/4 v12, 0x0

    goto :goto_4
.end method

.method public bindEditorialDescription(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;ZLandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ILcom/google/android/finsky/activities/TextSectionStateListener;)V
    .locals 17
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "hasDetailsDataLoaded"    # Z
    .param p4, "savedState"    # Landroid/os/Bundle;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p6, "backgroundFillColor"    # I
    .param p7, "sectionStateListener"    # Lcom/google/android/finsky/activities/TextSectionStateListener;

    .prologue
    .line 226
    if-nez p3, :cond_0

    .line 227
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/DetailsTextSection;->setVisibility(I)V

    .line 247
    :goto_0
    return-void

    .line 230
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getRawDescription()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    .line 232
    .local v9, "fullDesc":Ljava/lang/CharSequence;
    const/4 v5, 0x0

    const/4 v6, 0x3

    const v7, 0x7fffffff

    const/4 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mMaxCollapsedLines:I

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v14, p7

    invoke-direct/range {v2 .. v14}, Lcom/google/android/finsky/layout/DetailsTextSection;->bind(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Ljava/lang/CharSequence;IILjava/lang/String;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILcom/google/android/finsky/activities/TextSectionStateListener;)V

    .line 238
    invoke-static/range {p6 .. p6}, Lcom/google/android/finsky/utils/UiUtils;->isColorBright(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const v16, 0x7f09005a

    .line 240
    .local v16, "textColorResourceId":I
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v15

    .line 243
    .local v15, "textColor":I
    move-object/from16 v0, p0

    move/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsTextSection;->setBackgroundColor(I)V

    .line 244
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    move/from16 v0, p6

    invoke-virtual {v2, v0, v15}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setEditorialStyle(II)V

    .line 246
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsTextSection;->mFooterLabel:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 238
    .end local v15    # "textColor":I
    .end local v16    # "textColorResourceId":I
    :cond_1
    const v16, 0x7f09009c

    goto :goto_1
.end method

.method public getExpandedBackend()I
    .locals 1

    .prologue
    .line 691
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedBackend:I

    return v0
.end method

.method public getExpandedBody()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedBody:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getExpandedBodyHeader()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedBodyHeader:Ljava/lang/String;

    return-object v0
.end method

.method public getExpandedCallout()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedCallout:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getExpandedCalloutGravity()I
    .locals 1

    .prologue
    .line 699
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedCalloutGravity:I

    return v0
.end method

.method public getExpandedPromoteWhatsNew()Z
    .locals 1

    .prologue
    .line 723
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedPromoteWhatsNew:Z

    return v0
.end method

.method public getExpandedTranslatedBody()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedTranslatedBody:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getExpandedWhatsNew()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedWhatsNew:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getExpandedWhatsNewHeader()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 715
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExpandedWhatsNewHeader:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getExtraAttributionString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraAttributions:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraCreditsHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraCreditsHeader:Ljava/lang/String;

    return-object v0
.end method

.method public getExtraCreditsList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraCredits;",
            ">;"
        }
    .end annotation

    .prologue
    .line 731
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraCreditsList:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraCreditsList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getExtraPrimaryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 736
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getExtraSecondaryList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;",
            ">;"
        }
    .end annotation

    .prologue
    .line 741
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method protected handleClick()V
    .locals 1

    .prologue
    .line 675
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mUrlSpanClicked:Z

    if-eqz v0, :cond_0

    .line 677
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mUrlSpanClicked:Z

    .line 682
    :goto_0
    return-void

    .line 681
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->expandContent()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 165
    invoke-super {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->onFinishInflate()V

    .line 167
    const v0, 0x7f0a018e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mCalloutView:Lcom/google/android/play/layout/PlayTextView;

    .line 168
    const v0, 0x7f0a018f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mSpacerView:Landroid/view/View;

    .line 170
    const v0, 0x7f0a0190

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsTextBlock;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mBodyContainerView:Lcom/google/android/finsky/layout/DetailsTextBlock;

    .line 171
    const v0, 0x7f0a0191

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    .line 172
    const v0, 0x7f0a0192

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DecoratedTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mTopDeveloperLabel:Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 173
    const v0, 0x7f0a0193

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mFooterLabel:Landroid/widget/TextView;

    .line 174
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mFooterLabel:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0c0383

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    return-void
.end method

.method public syncCollapsedExtraIcons()V
    .locals 6

    .prologue
    .line 613
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 614
    .local v0, "extraIconPairs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Lcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;>;>;"
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 615
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    .line 616
    .local v2, "primary":Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;
    iget-boolean v3, v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->skipInCollapsedMode:Z

    if-nez v3, :cond_0

    iget-object v3, v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_0

    .line 617
    new-instance v3, Landroid/util/Pair;

    iget-object v4, v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->image:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v5, v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->title:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 622
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "primary":Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->populate(Ljava/util/List;Lcom/google/android/play/image/BitmapLoader;)V

    .line 623
    return-void
.end method

.method public updateExtras(Lcom/google/android/finsky/api/model/Document;)V
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 250
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraPrimaryList:Ljava/util/List;

    .line 251
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTextSection;->mExtraSecondaryList:Ljava/util/List;

    .line 255
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->fillContentRatingDetails(Lcom/google/android/finsky/api/model/Document;)V

    .line 256
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->fillExtraCreditsList(Lcom/google/android/finsky/api/model/Document;)V

    .line 257
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->fillExtraPrimaryList(Lcom/google/android/finsky/api/model/Document;)V

    .line 258
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->fillExtraSecondaryList(Lcom/google/android/finsky/api/model/Document;)V

    .line 259
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->fillExtraAttributionsString(Lcom/google/android/finsky/api/model/Document;)V

    .line 260
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->addProductDetails(Lcom/google/android/finsky/api/model/Document;)V

    .line 261
    return-void
.end method
