.class public Lcom/google/android/finsky/activities/ReviewsActivity;
.super Lcom/google/android/finsky/activities/AuthenticatedActivity;
.source "ReviewsActivity.java"

# interfaces
.implements Lcom/google/android/finsky/fragments/PageFragmentHost;


# instance fields
.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field private mIsRottenTomatoesReviews:Z

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mReviewsUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;-><init>()V

    return-void
.end method

.method public static show(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Z)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "reviewsUrl"    # Ljava/lang/String;
    .param p3, "isRottenTomatoesReviews"    # Z

    .prologue
    .line 46
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/ReviewsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "finsky.ReviewsActivity.document"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 48
    const-string v1, "finsky.ReviewsActivity.reviewsUrl"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 49
    const-string v1, "finsky.ReviewsActivity.isRottenTomatoesReviews"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 50
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 51
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 52
    return-void
.end method


# virtual methods
.method public getActionBarController()Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;
    .locals 1

    .prologue
    .line 133
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v0

    return-object v0
.end method

.method public getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p1, "dfeAccount"    # Ljava/lang/String;

    .prologue
    .line 128
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationManager()Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method public getPeopleClient()Lcom/google/android/gms/people/PeopleClient;
    .locals 1

    .prologue
    .line 142
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public goBack()V
    .locals 0

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsActivity;->finish()V

    .line 124
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v7, 0x7f0a00c4

    const/4 v6, 0x0

    .line 56
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 58
    const v4, 0x7f040083

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/activities/ReviewsActivity;->setContentView(I)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 61
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "finsky.ReviewsActivity.document"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/api/model/Document;

    iput-object v4, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 62
    const-string v4, "finsky.ReviewsActivity.reviewsUrl"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mReviewsUrl:Ljava/lang/String;

    .line 63
    const-string v4, "finsky.ReviewsActivity.isRottenTomatoesReviews"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mIsRottenTomatoesReviews:Z

    .line 65
    new-instance v4, Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/FakeNavigationManager;-><init>(Landroid/app/Activity;)V

    iput-object v4, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 67
    new-instance v4, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v5, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-direct {v4, v5, p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/support/v7/app/ActionBarActivity;)V

    iput-object v4, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .line 68
    iget-object v4, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v5, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateDefaultTitle(Ljava/lang/String;)V

    .line 69
    iget-object v4, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v5, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    invoke-virtual {v4, v5, v6}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateCurrentBackendId(IZ)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 73
    .local v3, "manager":Landroid/support/v4/app/FragmentManager;
    invoke-virtual {v3, v7}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_0

    .line 74
    iget-object v4, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v5, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mReviewsUrl:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mIsRottenTomatoesReviews:Z

    invoke-static {v4, v5, v6}, Lcom/google/android/finsky/activities/ReviewsFragment;->newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ReviewsFragment;

    move-result-object v0

    .line 77
    .local v0, "fragment":Lcom/google/android/finsky/activities/ReviewsFragment;
    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 78
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {v1, v7, v0}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 79
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 81
    .end local v0    # "fragment":Lcom/google/android/finsky/activities/ReviewsFragment;
    .end local v1    # "ft":Landroid/support/v4/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 90
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 97
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 92
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goUp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    invoke-super {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onBackPressed()V

    .line 95
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 90
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onReady(Z)V
    .locals 0
    .param p1, "shouldHandleIntent"    # Z

    .prologue
    .line 86
    return-void
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "goBack"    # Z

    .prologue
    .line 138
    return-void
.end method

.method public updateBreadcrumb(Ljava/lang/String;)V
    .locals 1
    .param p1, "breadcrumb"    # Ljava/lang/String;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateDefaultTitle(Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public updateCurrentBackendId(IZ)V
    .locals 1
    .param p1, "backend"    # I
    .param p2, "ignoreActionBarBackground"    # Z

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsActivity;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateCurrentBackendId(IZ)V

    .line 119
    return-void
.end method
