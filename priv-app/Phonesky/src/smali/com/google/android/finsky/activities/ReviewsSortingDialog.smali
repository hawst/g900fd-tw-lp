.class public Lcom/google/android/finsky/activities/ReviewsSortingDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ReviewsSortingDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/ReviewsSortingDialog$Listener;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/ReviewsSortingDialog;)Lcom/google/android/finsky/activities/ReviewsSortingDialog$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/ReviewsSortingDialog;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewsSortingDialog;->getListener()Lcom/google/android/finsky/activities/ReviewsSortingDialog$Listener;

    move-result-object v0

    return-object v0
.end method

.method private getListener()Lcom/google/android/finsky/activities/ReviewsSortingDialog$Listener;
    .locals 3

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsSortingDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 65
    .local v1, "f":Landroid/support/v4/app/Fragment;
    instance-of v2, v1, Lcom/google/android/finsky/activities/ReviewsSortingDialog$Listener;

    if-eqz v2, :cond_0

    .line 66
    check-cast v1, Lcom/google/android/finsky/activities/ReviewsSortingDialog$Listener;

    .line 72
    .end local v1    # "f":Landroid/support/v4/app/Fragment;
    :goto_0
    return-object v1

    .line 68
    .restart local v1    # "f":Landroid/support/v4/app/Fragment;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsSortingDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 69
    .local v0, "a":Landroid/app/Activity;
    instance-of v2, v0, Lcom/google/android/finsky/activities/ReviewsSortingDialog$Listener;

    if-eqz v2, :cond_1

    .line 70
    check-cast v0, Lcom/google/android/finsky/activities/ReviewsSortingDialog$Listener;

    .end local v0    # "a":Landroid/app/Activity;
    move-object v1, v0

    goto :goto_0

    .line 72
    .restart local v0    # "a":Landroid/app/Activity;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newInstance(Lcom/google/android/finsky/api/model/DfeReviews;)Lcom/google/android/finsky/activities/ReviewsSortingDialog;
    .locals 4
    .param p0, "data"    # Lcom/google/android/finsky/api/model/DfeReviews;

    .prologue
    .line 22
    new-instance v1, Lcom/google/android/finsky/activities/ReviewsSortingDialog;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/ReviewsSortingDialog;-><init>()V

    .line 23
    .local v1, "filterOptionsDialog":Lcom/google/android/finsky/activities/ReviewsSortingDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 24
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "sorting_type"

    invoke-static {p0}, Lcom/google/android/finsky/utils/ReviewsSortingUtils;->convertDataSortTypeToDisplayIndex(Lcom/google/android/finsky/api/model/DfeReviews;)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 25
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/ReviewsSortingDialog;->setArguments(Landroid/os/Bundle;)V

    .line 26
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsSortingDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    .line 32
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsSortingDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 33
    .local v0, "arguments":Landroid/os/Bundle;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 34
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0c023b

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 35
    invoke-static {v2}, Lcom/google/android/finsky/utils/ReviewsSortingUtils;->getAllDisplayStrings(Landroid/content/Context;)[Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "sorting_type"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Lcom/google/android/finsky/activities/ReviewsSortingDialog$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/activities/ReviewsSortingDialog$1;-><init>(Lcom/google/android/finsky/activities/ReviewsSortingDialog;)V

    invoke-virtual {v1, v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 46
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method
