.class public final Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Restore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Restore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GetBackupDocumentChoicesResponse"
.end annotation


# instance fields
.field public backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 456
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 457
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->clear()Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    .line 458
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;
    .locals 1

    .prologue
    .line 461
    invoke-static {}, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->emptyArray()[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    .line 462
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->cachedSize:I

    .line 463
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 482
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 483
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 484
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 485
    iget-object v3, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    aget-object v0, v3, v1

    .line 486
    .local v0, "element":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    if-eqz v0, :cond_0

    .line 487
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 484
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 492
    .end local v0    # "element":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    .end local v1    # "i":I
    :cond_1
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 500
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 501
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 505
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 506
    :sswitch_0
    return-object p0

    .line 511
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 513
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    if-nez v5, :cond_2

    move v1, v4

    .line 514
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    .line 516
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    if-eqz v1, :cond_1

    .line 517
    iget-object v5, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 519
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 520
    new-instance v5, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;-><init>()V

    aput-object v5, v2, v1

    .line 521
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 522
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 519
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 513
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    array-length v1, v5

    goto :goto_1

    .line 525
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;-><init>()V

    aput-object v5, v2, v1

    .line 526
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 527
    iput-object v2, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    goto :goto_0

    .line 501
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 436
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 469
    iget-object v2, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 470
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 471
    iget-object v2, p0, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    aget-object v0, v2, v1

    .line 472
    .local v0, "element":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    if-eqz v0, :cond_0

    .line 473
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 470
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 477
    .end local v0    # "element":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 478
    return-void
.end method
