.class Lcom/google/android/finsky/layout/SetupWizardIllustration$1;
.super Ljava/lang/Object;
.source "SetupWizardIllustration.java"

# interfaces
.implements Lcom/google/android/play/image/FifeImageView$OnLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/SetupWizardIllustration;->configure(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/SetupWizardIllustration;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/SetupWizardIllustration;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration$1;->this$0:Lcom/google/android/finsky/layout/SetupWizardIllustration;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
    .locals 13
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v12, 0x0

    .line 100
    if-eqz p2, :cond_0

    .line 101
    iget-object v10, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration$1;->this$0:Lcom/google/android/finsky/layout/SetupWizardIllustration;

    # getter for: Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v10}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->access$000(Lcom/google/android/finsky/layout/SetupWizardIllustration;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v10

    sget-object v11, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v10, v11}, Lcom/google/android/play/image/FifeImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 102
    iget-object v10, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration$1;->this$0:Lcom/google/android/finsky/layout/SetupWizardIllustration;

    # getter for: Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v10}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->access$000(Lcom/google/android/finsky/layout/SetupWizardIllustration;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v4

    .line 103
    .local v4, "layoutWidth":I
    iget-object v10, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration$1;->this$0:Lcom/google/android/finsky/layout/SetupWizardIllustration;

    # getter for: Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v10}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->access$000(Lcom/google/android/finsky/layout/SetupWizardIllustration;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v3

    .line 105
    .local v3, "layoutHeight":I
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    .line 106
    .local v9, "srcWidth":I
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 107
    .local v7, "srcHeight":I
    int-to-float v10, v9

    int-to-float v11, v7

    div-float v6, v10, v11

    .line 110
    .local v6, "srcAspectRatio":F
    iget-object v10, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration$1;->this$0:Lcom/google/android/finsky/layout/SetupWizardIllustration;

    # getter for: Lcom/google/android/finsky/layout/SetupWizardIllustration;->mUseTabletGraphic:Z
    invoke-static {v10}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->access$100(Lcom/google/android/finsky/layout/SetupWizardIllustration;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 114
    iget-object v10, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration$1;->this$0:Lcom/google/android/finsky/layout/SetupWizardIllustration;

    # getter for: Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v10}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->access$000(Lcom/google/android/finsky/layout/SetupWizardIllustration;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v10

    const v11, 0x7f0201b1

    invoke-virtual {v10, v11}, Lcom/google/android/play/image/FifeImageView;->setBackgroundResource(I)V

    .line 116
    int-to-float v10, v3

    mul-float/2addr v10, v6

    float-to-int v2, v10

    .line 117
    .local v2, "dstWidth":I
    move v0, v3

    .line 127
    .local v0, "dstHeight":I
    :goto_0
    new-instance v8, Landroid/graphics/RectF;

    int-to-float v10, v9

    int-to-float v11, v7

    invoke-direct {v8, v12, v12, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 128
    .local v8, "srcRect":Landroid/graphics/RectF;
    new-instance v1, Landroid/graphics/RectF;

    int-to-float v10, v2

    int-to-float v11, v0

    invoke-direct {v1, v12, v12, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 129
    .local v1, "dstRect":Landroid/graphics/RectF;
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 130
    .local v5, "matrix":Landroid/graphics/Matrix;
    sget-object v10, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v5, v8, v1, v10}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 131
    iget-object v10, p0, Lcom/google/android/finsky/layout/SetupWizardIllustration$1;->this$0:Lcom/google/android/finsky/layout/SetupWizardIllustration;

    # getter for: Lcom/google/android/finsky/layout/SetupWizardIllustration;->mImageView:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v10}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->access$000(Lcom/google/android/finsky/layout/SetupWizardIllustration;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v10

    invoke-virtual {v10, v5}, Lcom/google/android/play/image/FifeImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 133
    .end local v0    # "dstHeight":I
    .end local v1    # "dstRect":Landroid/graphics/RectF;
    .end local v2    # "dstWidth":I
    .end local v3    # "layoutHeight":I
    .end local v4    # "layoutWidth":I
    .end local v5    # "matrix":Landroid/graphics/Matrix;
    .end local v6    # "srcAspectRatio":F
    .end local v7    # "srcHeight":I
    .end local v8    # "srcRect":Landroid/graphics/RectF;
    .end local v9    # "srcWidth":I
    :cond_0
    return-void

    .line 121
    .restart local v3    # "layoutHeight":I
    .restart local v4    # "layoutWidth":I
    .restart local v6    # "srcAspectRatio":F
    .restart local v7    # "srcHeight":I
    .restart local v9    # "srcWidth":I
    :cond_1
    int-to-float v10, v3

    mul-float/2addr v10, v6

    float-to-int v10, v10

    invoke-static {v4, v10}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 123
    .restart local v2    # "dstWidth":I
    int-to-float v10, v4

    div-float/2addr v10, v6

    float-to-int v10, v10

    invoke-static {v3, v10}, Ljava/lang/Math;->max(II)I

    move-result v0

    .restart local v0    # "dstHeight":I
    goto :goto_0
.end method

.method public onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V
    .locals 0
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    .line 137
    return-void
.end method
