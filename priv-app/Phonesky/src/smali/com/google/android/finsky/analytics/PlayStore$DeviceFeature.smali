.class public final Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceFeature"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    }
.end annotation


# instance fields
.field public deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3140
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3141
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->clear()Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    .line 3142
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;
    .locals 1

    .prologue
    .line 3145
    invoke-static {}, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->emptyArray()[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    .line 3146
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->cachedSize:I

    .line 3147
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 3166
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 3167
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 3168
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 3169
    iget-object v3, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    aget-object v0, v3, v1

    .line 3170
    .local v0, "element":Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    if-eqz v0, :cond_0

    .line 3171
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3168
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3176
    .end local v0    # "element":Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    .end local v1    # "i":I
    :cond_1
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 3184
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3185
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3189
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3190
    :sswitch_0
    return-object p0

    .line 3195
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3197
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    if-nez v5, :cond_2

    move v1, v4

    .line 3198
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    .line 3200
    .local v2, "newArray":[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    if-eqz v1, :cond_1

    .line 3201
    iget-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3203
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 3204
    new-instance v5, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;-><init>()V

    aput-object v5, v2, v1

    .line 3205
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3206
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3203
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3197
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    array-length v1, v5

    goto :goto_1

    .line 3209
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;-><init>()V

    aput-object v5, v2, v1

    .line 3210
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 3211
    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    goto :goto_0

    .line 3185
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3015
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3153
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 3154
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 3155
    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;->deviceFeatureInfo:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    aget-object v0, v2, v1

    .line 3156
    .local v0, "element":Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    if-eqz v0, :cond_0

    .line 3157
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3154
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3161
    .end local v0    # "element":Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3162
    return-void
.end method
