.class public Lcom/google/android/finsky/adapters/ReviewsAdapter;
.super Lcom/google/android/finsky/adapters/PaginatedListAdapter;
.source "ReviewsAdapter.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;,
        Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;,
        Lcom/google/android/finsky/adapters/ReviewsAdapter$ReviewFeedbackHandler;
    }
.end annotation


# instance fields
.field private final mData:Lcom/google/android/finsky/api/model/DfeReviews;

.field private final mDoc:Lcom/google/android/finsky/api/model/Document;

.field private final mIsRottenTomatoesReviews:Z

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mListingOptionsHandler:Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;

.field private final mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private final mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mReviewFeedbackHandler:Lcom/google/android/finsky/adapters/ReviewsAdapter$ReviewFeedbackHandler;

.field private final mReviewTextMaxLines:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeReviews;ZILcom/google/android/finsky/adapters/ReviewsAdapter$ReviewFeedbackHandler;Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "reviews"    # Lcom/google/android/finsky/api/model/DfeReviews;
    .param p4, "isRottenTomatoesReviews"    # Z
    .param p5, "reviewTextMaxLines"    # I
    .param p6, "ratingHandler"    # Lcom/google/android/finsky/adapters/ReviewsAdapter$ReviewFeedbackHandler;
    .param p7, "listingOptionsHandler"    # Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;
    .param p8, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p9, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-virtual {p3}, Lcom/google/android/finsky/api/model/DfeReviews;->inErrorState()Z

    move-result v1

    invoke-virtual {p3}, Lcom/google/android/finsky/api/model/DfeReviews;->isMoreAvailable()Z

    move-result v2

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/finsky/adapters/PaginatedListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZZ)V

    .line 74
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 75
    iput-object p2, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 76
    iput-object p3, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    .line 77
    iput-boolean p4, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mIsRottenTomatoesReviews:Z

    .line 78
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 80
    iput p5, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mReviewTextMaxLines:I

    .line 81
    iput-object p6, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mReviewFeedbackHandler:Lcom/google/android/finsky/adapters/ReviewsAdapter$ReviewFeedbackHandler;

    .line 82
    iput-object p7, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mListingOptionsHandler:Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;

    .line 83
    iput-object p8, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 84
    iput-object p9, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/adapters/ReviewsAdapter;)Lcom/google/android/finsky/adapters/ReviewsAdapter$ReviewFeedbackHandler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/ReviewsAdapter;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mReviewFeedbackHandler:Lcom/google/android/finsky/adapters/ReviewsAdapter$ReviewFeedbackHandler;

    return-object v0
.end method

.method private bindReviewItem(Landroid/view/View;Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;I)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "holder"    # Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;
    .param p3, "position"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 267
    invoke-virtual {p0, p3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getItem(I)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v2

    .line 269
    .local v2, "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    iget-object v0, v2, Lcom/google/android/finsky/protos/DocumentV2$Review;->commentId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v7, 0x1

    .line 270
    .local v7, "canGiveFeedback":Z
    :goto_0
    iget-object v0, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewItemLayout:Lcom/google/android/finsky/layout/ReviewItemLayout;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget v3, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mReviewTextMaxLines:I

    iget-object v4, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v6, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/ReviewItemLayout;->setReviewInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;ILcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 272
    if-eqz v7, :cond_3

    .line 273
    iget-object v0, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewItemLayout:Lcom/google/android/finsky/layout/ReviewItemLayout;

    new-instance v1, Lcom/google/android/finsky/adapters/ReviewsAdapter$1;

    invoke-direct {v1, p0, v2}, Lcom/google/android/finsky/adapters/ReviewsAdapter$1;-><init>(Lcom/google/android/finsky/adapters/ReviewsAdapter;Lcom/google/android/finsky/protos/DocumentV2$Review;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/ReviewItemLayout;->setActionClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    :goto_1
    iget-boolean v0, v2, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyText:Z

    if-eqz v0, :cond_4

    .line 284
    iget-object v0, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    if-nez v0, :cond_0

    .line 285
    iget-object v0, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewReplyStub:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ReviewReplyLayout;

    iput-object v0, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    .line 286
    iput-object v8, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewReplyStub:Landroid/view/ViewStub;

    .line 288
    :cond_0
    iget-object v0, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->setReviewInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;)V

    .line 294
    :cond_1
    :goto_2
    return-void

    .end local v7    # "canGiveFeedback":Z
    :cond_2
    move v7, v5

    .line 269
    goto :goto_0

    .line 280
    .restart local v7    # "canGiveFeedback":Z
    :cond_3
    iget-object v0, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewItemLayout:Lcom/google/android/finsky/layout/ReviewItemLayout;

    invoke-virtual {v0, v8}, Lcom/google/android/finsky/layout/ReviewItemLayout;->setActionClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 290
    :cond_4
    iget-object v0, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    if-eqz v0, :cond_1

    .line 291
    iget-object v0, p2, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;->reviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method private filtersVisible()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 353
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mIsRottenTomatoesReviews:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getFiltersView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 226
    if-nez p1, :cond_0

    const v1, 0x7f04018e

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/ReviewsControlContainer;

    move-object v0, v1

    .line 229
    .end local p1    # "convertView":Landroid/view/View;
    .local v0, "filtersView":Lcom/google/android/finsky/layout/ReviewsControlContainer;
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    iget-object v2, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mListingOptionsHandler:Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->configure(Lcom/google/android/finsky/api/model/DfeReviews;Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;)V

    .line 230
    return-object v0

    .line 226
    .end local v0    # "filtersView":Lcom/google/android/finsky/layout/ReviewsControlContainer;
    .restart local p1    # "convertView":Landroid/view/View;
    :cond_0
    check-cast p1, Lcom/google/android/finsky/layout/ReviewsControlContainer;

    move-object v0, p1

    goto :goto_0
.end method

.method private getNoMatchingView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 245
    if-nez p1, :cond_0

    const v1, 0x7f04018f

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 247
    .local v0, "result":Landroid/view/View;
    :goto_0
    return-object v0

    .end local v0    # "result":Landroid/view/View;
    :cond_0
    move-object v0, p1

    .line 245
    goto :goto_0
.end method

.method private getReviewItemView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 252
    if-nez p2, :cond_0

    .line 253
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f04018b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 256
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;

    .line 258
    .local v0, "holder":Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;
    if-nez v0, :cond_1

    .line 259
    new-instance v0, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;

    .end local v0    # "holder":Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;
    invoke-direct {v0, p2}, Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 262
    .restart local v0    # "holder":Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;
    :cond_1
    invoke-direct {p0, p2, v0, p1}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->bindReviewItem(Landroid/view/View;Lcom/google/android/finsky/adapters/ReviewsAdapter$ViewHolder;I)V

    .line 263
    return-object p2
.end method

.method private getRottenTomatoesHeader(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 234
    if-nez p1, :cond_0

    const v2, 0x7f040193

    const/4 v3, 0x0

    invoke-virtual {p0, v2, p2, v3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;

    move-object v1, v2

    .line 238
    .end local p1    # "convertView":Landroid/view/View;
    .local v1, "headerView":Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;
    :goto_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    .line 239
    .local v0, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeReviews;->getRottenTomatoesData()Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->bind(Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/play/image/BitmapLoader;)V

    .line 241
    return-object v1

    .line 234
    .end local v0    # "finskyApp":Lcom/google/android/finsky/FinskyApp;
    .end local v1    # "headerView":Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;
    .restart local p1    # "convertView":Landroid/view/View;
    :cond_0
    check-cast p1, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;

    move-object v1, p1

    goto :goto_0
.end method

.method private getRottenTomatoesReview(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 297
    if-nez p2, :cond_0

    const v2, 0x7f040192

    const/4 v3, 0x0

    invoke-virtual {p0, v2, p3, v3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;

    move-object v1, v2

    .line 302
    .end local p2    # "convertView":Landroid/view/View;
    .local v1, "reviewView":Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getItem(I)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v0

    .line 303
    .local v0, "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;->bind(Lcom/google/android/finsky/protos/DocumentV2$Review;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;)V

    .line 304
    return-object v1

    .line 297
    .end local v0    # "review":Lcom/google/android/finsky/protos/DocumentV2$Review;
    .end local v1    # "reviewView":Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    check-cast p2, Lcom/google/android/finsky/layout/RottenTomatoesReviewItem;

    move-object v1, p2

    goto :goto_0
.end method

.method private getStatisticsView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 218
    if-nez p1, :cond_0

    const v1, 0x7f040191

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/HistogramView;

    move-object v0, v1

    .line 221
    .end local p1    # "convertView":Landroid/view/View;
    .local v0, "statsView":Lcom/google/android/finsky/layout/HistogramView;
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/HistogramView;->bind(Lcom/google/android/finsky/api/model/Document;)V

    .line 222
    return-object v0

    .line 218
    .end local v0    # "statsView":Lcom/google/android/finsky/layout/HistogramView;
    .restart local p1    # "convertView":Landroid/view/View;
    :cond_0
    check-cast p1, Lcom/google/android/finsky/layout/HistogramView;

    move-object v0, p1

    goto :goto_0
.end method

.method private rottenTomatoesHeaderVisible()Z
    .locals 1

    .prologue
    .line 358
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mIsRottenTomatoesReviews:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeReviews;->getRottenTomatoesData()Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private statsVisible()Z
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasReviewHistogramData()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mIsRottenTomatoesReviews:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 3

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeReviews;->getCount()I

    move-result v0

    .line 90
    .local v0, "result":I
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->statsVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 91
    add-int/lit8 v0, v0, 0x1

    .line 93
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->filtersVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 94
    add-int/lit8 v0, v0, 0x1

    .line 96
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->rottenTomatoesHeaderVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 97
    add-int/lit8 v0, v0, 0x1

    .line 99
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-eq v1, v2, :cond_4

    .line 100
    add-int/lit8 v0, v0, 0x1

    .line 105
    :cond_3
    :goto_0
    return v0

    .line 101
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeReviews;->getCount()I

    move-result v1

    if-nez v1, :cond_3

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getFirstReviewViewIndex()I
    .locals 2

    .prologue
    .line 201
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeReviews;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 202
    const/4 v0, -0x1

    .line 214
    :cond_0
    :goto_0
    return v0

    .line 204
    :cond_1
    const/4 v0, 0x0

    .line 205
    .local v0, "firstReviewView":I
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->statsVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 206
    add-int/lit8 v0, v0, 0x1

    .line 208
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->filtersVisible()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 209
    add-int/lit8 v0, v0, 0x1

    .line 211
    :cond_3
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->rottenTomatoesHeaderVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getItem(I)Lcom/google/android/finsky/protos/DocumentV2$Review;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getFirstReviewViewIndex()I

    move-result v0

    .line 164
    .local v0, "offset":I
    sub-int v1, p1, v0

    .line 165
    .local v1, "reviewPosition":I
    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeReviews;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 166
    iget-object v2, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/api/model/DfeReviews;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 168
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getItem(I)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v0

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x6

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne p1, v6, :cond_1

    move v1, v2

    .line 117
    .local v1, "isLastRow":Z
    :goto_0
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->statsVisible()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 118
    if-nez p1, :cond_2

    move v2, v3

    .line 157
    :cond_0
    :goto_1
    return v2

    .end local v1    # "isLastRow":Z
    :cond_1
    move v1, v3

    .line 115
    goto :goto_0

    .line 121
    .restart local v1    # "isLastRow":Z
    :cond_2
    add-int/lit8 p1, p1, -0x1

    .line 124
    :cond_3
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->filtersVisible()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 125
    if-eqz p1, :cond_0

    .line 128
    add-int/lit8 p1, p1, -0x1

    .line 131
    :cond_4
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->rottenTomatoesHeaderVisible()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 132
    if-nez p1, :cond_5

    .line 133
    const/4 v2, 0x2

    goto :goto_1

    .line 135
    :cond_5
    add-int/lit8 p1, p1, -0x1

    .line 138
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeReviews;->getCount()I

    move-result v2

    if-nez v2, :cond_8

    .line 139
    iget-object v2, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeReviews;->isMoreAvailable()Z

    move-result v2

    if-eqz v2, :cond_7

    move v2, v4

    goto :goto_1

    :cond_7
    const/4 v2, 0x3

    goto :goto_1

    .line 142
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    move-result-object v0

    .line 143
    .local v0, "footerMode":Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;
    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-eq v0, v2, :cond_b

    if-eqz v1, :cond_b

    .line 144
    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->LOADING:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-ne v0, v2, :cond_9

    move v2, v4

    .line 145
    goto :goto_1

    .line 146
    :cond_9
    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->ERROR:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-ne v0, v2, :cond_a

    move v2, v5

    .line 147
    goto :goto_1

    .line 149
    :cond_a
    const-string v2, "No footer or item in last row"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v5

    .line 150
    goto :goto_1

    .line 154
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mIsRottenTomatoesReviews:Z

    if-eqz v2, :cond_c

    .line 155
    const/4 v2, 0x5

    goto :goto_1

    .line 157
    :cond_c
    const/4 v2, 0x4

    goto :goto_1
.end method

.method protected getLastRequestError()Ljava/lang/String;
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeReviews;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 191
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type for getView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getItemViewType(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getStatisticsView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 189
    :goto_0
    return-object v0

    .line 177
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getFiltersView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 179
    :pswitch_2
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getRottenTomatoesHeader(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 181
    :pswitch_3
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getNoMatchingView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 183
    :pswitch_4
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getReviewItemView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 185
    :pswitch_5
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getRottenTomatoesReview(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 187
    :pswitch_6
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getLoadingFooterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 189
    :pswitch_7
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getErrorFooterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 110
    const/16 v0, 0x8

    return v0
.end method

.method protected isMoreDataAvailable()Z
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeReviews;->isMoreAvailable()Z

    move-result v0

    return v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 340
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 341
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 0
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->triggerFooterErrorMode()V

    .line 346
    return-void
.end method

.method protected retryLoadingItems()V
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ReviewsAdapter;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeReviews;->retryLoadItems()V

    .line 321
    return-void
.end method
