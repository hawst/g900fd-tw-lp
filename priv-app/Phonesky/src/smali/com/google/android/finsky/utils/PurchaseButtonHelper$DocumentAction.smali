.class public Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
.super Ljava/lang/Object;
.source "PurchaseButtonHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/PurchaseButtonHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DocumentAction"
.end annotation


# instance fields
.field public account:Landroid/accounts/Account;

.field public actionType:I

.field public document:Lcom/google/android/finsky/api/model/Document;

.field public offerFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

.field public offerFullText:Ljava/lang/String;

.field public offerText:Ljava/lang/String;

.field public offerType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->reset()V

    .line 102
    return-void
.end method


# virtual methods
.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    .line 106
    iput-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerText:Ljava/lang/String;

    .line 107
    iput-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFullText:Ljava/lang/String;

    .line 108
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerType:I

    .line 109
    iput-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->offerFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    .line 110
    iput-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->document:Lcom/google/android/finsky/api/model/Document;

    .line 111
    iput-object v1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->account:Landroid/accounts/Account;

    .line 112
    return-void
.end method
