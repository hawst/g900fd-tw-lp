.class Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;
.super Ljava/lang/Object;
.source "DetailsTitleCreatorBlock.java"

# interfaces
.implements Lcom/google/android/play/image/BitmapTransformation;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArtistAvatarTransformation"
.end annotation


# static fields
.field private static INSTANCE:Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;

    invoke-direct {v0}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;-><init>()V

    sput-object v0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;->INSTANCE:Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;->INSTANCE:Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;

    return-object v0
.end method


# virtual methods
.method public drawFocusedOverlay(Landroid/graphics/Canvas;II)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 80
    return-void
.end method

.method public drawPressedOverlay(Landroid/graphics/Canvas;II)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 85
    return-void
.end method

.method public getTransformationInset(II)I
    .locals 2
    .param p1, "viewWidth"    # I
    .param p2, "viewHeight"    # I

    .prologue
    .line 55
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v0

    .line 57
    .local v0, "avatarCropTransformation":Lcom/google/android/play/image/AvatarCropTransformation;
    invoke-virtual {v0, p1, p2}, Lcom/google/android/play/image/AvatarCropTransformation;->getTransformationInset(II)I

    move-result v1

    return v1
.end method

.method public transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "source"    # Landroid/graphics/Bitmap;
    .param p2, "viewWidth"    # I
    .param p3, "viewHeight"    # I

    .prologue
    .line 62
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 63
    .local v2, "sourceWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 66
    .local v1, "sourceHeight":I
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 67
    .local v4, "squareCrop":Landroid/graphics/Bitmap;
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 68
    .local v3, "squareCanvas":Landroid/graphics/Canvas;
    sub-int v5, v2, v1

    neg-int v5, v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, p1, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 72
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v0

    .line 74
    .local v0, "avatarCropTransformation":Lcom/google/android/play/image/AvatarCropTransformation;
    invoke-virtual {v0, v4, p2, p3}, Lcom/google/android/play/image/AvatarCropTransformation;->transform(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v5

    return-object v5
.end method
