.class Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;
.super Ljava/lang/Object;
.source "InstallerImpl.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/receivers/InstallerImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RemoteServiceConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/receivers/InstallerImpl;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerImpl;)V
    .locals 0

    .prologue
    .line 1246
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "boundService"    # Landroid/os/IBinder;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1250
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 1251
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    invoke-static {p2}, Lcom/google/android/finsky/installer/IMultiUserCoordinatorService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    move-result-object v3

    # setter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;
    invoke-static {v2, v3}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$1102(Lcom/google/android/finsky/receivers/InstallerImpl;Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;)Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    .line 1255
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;
    invoke-static {v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$1100(Lcom/google/android/finsky/receivers/InstallerImpl;)Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    iget-object v3, v3, Lcom/google/android/finsky/receivers/InstallerImpl;->mListener:Lcom/google/android/finsky/installer/IMultiUserCoordinatorServiceListener;

    invoke-interface {v2, v3}, Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;->registerListener(Lcom/google/android/finsky/installer/IMultiUserCoordinatorServiceListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1276
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnectionCallbacks:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$1400(Lcom/google/android/finsky/receivers/InstallerImpl;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1277
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnectionCallbacks:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$1400(Lcom/google/android/finsky/receivers/InstallerImpl;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 1276
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1256
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 1257
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "Couldn\'t register listener *** received %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1262
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$1300(Lcom/google/android/finsky/receivers/InstallerImpl;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnection:Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;
    invoke-static {v3}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$1200(Lcom/google/android/finsky/receivers/InstallerImpl;)Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1263
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    const/4 v3, 0x0

    # setter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mCoordinatorService:Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;
    invoke-static {v2, v3}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$1102(Lcom/google/android/finsky/receivers/InstallerImpl;Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;)Lcom/google/android/finsky/installer/IMultiUserCoordinatorService;

    .line 1265
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnectionCallbacks:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$1400(Lcom/google/android/finsky/receivers/InstallerImpl;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 1269
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mRunning:Z
    invoke-static {v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$700(Lcom/google/android/finsky/receivers/InstallerImpl;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1270
    const-string v2, "Force-starting the installer after connection failure"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1271
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # setter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mRunning:Z
    invoke-static {v2, v4}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$702(Lcom/google/android/finsky/receivers/InstallerImpl;Z)Z

    .line 1272
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->kick(Z)V
    invoke-static {v2, v4}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$800(Lcom/google/android/finsky/receivers/InstallerImpl;Z)V

    .line 1280
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    return-void

    .line 1279
    .restart local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$RemoteServiceConnection;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mServiceConnectionCallbacks:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$1400(Lcom/google/android/finsky/receivers/InstallerImpl;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 1284
    return-void
.end method
