.class public final Lcom/google/android/finsky/protos/Toc$BillingConfig;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Toc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Toc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillingConfig"
.end annotation


# instance fields
.field public carrierBillingConfig:Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

.field public hasMaxIabApiVersion:Z

.field public maxIabApiVersion:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 908
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 909
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Toc$BillingConfig;->clear()Lcom/google/android/finsky/protos/Toc$BillingConfig;

    .line 910
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Toc$BillingConfig;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 913
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->carrierBillingConfig:Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    .line 914
    iput v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->maxIabApiVersion:I

    .line 915
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->hasMaxIabApiVersion:Z

    .line 916
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->cachedSize:I

    .line 917
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 934
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 935
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->carrierBillingConfig:Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    if-eqz v1, :cond_0

    .line 936
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->carrierBillingConfig:Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 939
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->hasMaxIabApiVersion:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->maxIabApiVersion:I

    if-eqz v1, :cond_2

    .line 940
    :cond_1
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->maxIabApiVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 943
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$BillingConfig;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 951
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 952
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 956
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 957
    :sswitch_0
    return-object p0

    .line 962
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->carrierBillingConfig:Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    if-nez v1, :cond_1

    .line 963
    new-instance v1, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->carrierBillingConfig:Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    .line 965
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->carrierBillingConfig:Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 969
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->maxIabApiVersion:I

    .line 970
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->hasMaxIabApiVersion:Z

    goto :goto_0

    .line 952
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 884
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Toc$BillingConfig;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$BillingConfig;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 923
    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->carrierBillingConfig:Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    if-eqz v0, :cond_0

    .line 924
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->carrierBillingConfig:Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 926
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->hasMaxIabApiVersion:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->maxIabApiVersion:I

    if-eqz v0, :cond_2

    .line 927
    :cond_1
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/Toc$BillingConfig;->maxIabApiVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 929
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 930
    return-void
.end method
