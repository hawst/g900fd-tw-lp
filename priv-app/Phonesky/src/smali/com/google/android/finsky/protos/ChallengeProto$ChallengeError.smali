.class public final Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChallengeError"
.end annotation


# instance fields
.field public cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

.field public descriptionHtml:Ljava/lang/String;

.field public hasDescriptionHtml:Z

.field public hasTitle:Z

.field public submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2268
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2269
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->clear()Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    .line 2270
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2273
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->title:Ljava/lang/String;

    .line 2274
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->hasTitle:Z

    .line 2275
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->descriptionHtml:Ljava/lang/String;

    .line 2276
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->hasDescriptionHtml:Z

    .line 2277
    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 2278
    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 2279
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cachedSize:I

    .line 2280
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2303
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2304
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->hasTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2305
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2308
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->hasDescriptionHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->descriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2309
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->descriptionHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2312
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v1, :cond_4

    .line 2313
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2316
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v1, :cond_5

    .line 2317
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2320
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2328
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2329
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2333
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2334
    :sswitch_0
    return-object p0

    .line 2339
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->title:Ljava/lang/String;

    .line 2340
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->hasTitle:Z

    goto :goto_0

    .line 2344
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->descriptionHtml:Ljava/lang/String;

    .line 2345
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->hasDescriptionHtml:Z

    goto :goto_0

    .line 2349
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-nez v1, :cond_1

    .line 2350
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 2352
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2356
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-nez v1, :cond_2

    .line 2357
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    .line 2359
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2329
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2237
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2286
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->hasTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2287
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2289
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->hasDescriptionHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->descriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2290
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2292
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v0, :cond_4

    .line 2293
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2295
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v0, :cond_5

    .line 2296
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2298
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2299
    return-void
.end method
