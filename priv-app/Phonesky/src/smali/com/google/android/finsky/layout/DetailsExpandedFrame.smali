.class public Lcom/google/android/finsky/layout/DetailsExpandedFrame;
.super Landroid/view/ViewGroup;
.source "DetailsExpandedFrame.java"


# instance fields
.field private mContentScroller:Landroid/view/View;

.field private mLeftBar:Landroid/view/View;

.field private mRightBar:Landroid/view/View;

.field private mScrollerWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method


# virtual methods
.method public fadeInSideBarsPreIcs()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const-wide/16 v2, 0x96

    .line 103
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 104
    .local v1, "context":Landroid/content/Context;
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setSideBarProportion(F)V

    .line 105
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mLeftBar:Landroid/view/View;

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mRightBar:Landroid/view/View;

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 111
    return-void
.end method

.method public fadeOutSideBarsPreIcs()V
    .locals 7

    .prologue
    const-wide/16 v4, 0x96

    const-wide/16 v2, 0x0

    .line 118
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 119
    .local v1, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mLeftBar:Landroid/view/View;

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mRightBar:Landroid/view/View;

    new-instance v6, Lcom/google/android/finsky/layout/DetailsExpandedFrame$1;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame$1;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedFrame;)V

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 129
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 65
    const v0, 0x7f0a0211

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mLeftBar:Landroid/view/View;

    .line 66
    const v0, 0x7f0a0218

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mRightBar:Landroid/view/View;

    .line 67
    const v0, 0x7f0a0212

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mContentScroller:Landroid/view/View;

    .line 68
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v8, 0x0

    .line 162
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->getWidth()I

    move-result v3

    .line 163
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->getHeight()I

    move-result v0

    .line 166
    .local v0, "height":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mContentScroller:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 167
    .local v1, "scrollerWidth":I
    sub-int v4, v3, v1

    div-int/lit8 v2, v4, 0x2

    .line 168
    .local v2, "scrollerX":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mContentScroller:Landroid/view/View;

    add-int v5, v2, v1

    invoke-virtual {v4, v2, v8, v5, v0}, Landroid/view/View;->layout(IIII)V

    .line 170
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mLeftBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 172
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mLeftBar:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mLeftBar:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    sub-int v5, v2, v5

    invoke-virtual {v4, v5, v8, v2, v0}, Landroid/view/View;->layout(IIII)V

    .line 175
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mRightBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 177
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mRightBar:Landroid/view/View;

    add-int v5, v2, v1

    add-int v6, v2, v1

    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mRightBar:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v4, v5, v8, v6, v0}, Landroid/view/View;->layout(IIII)V

    .line 180
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 133
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 136
    .local v4, "width":I
    iget v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mScrollerWidth:I

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 137
    .local v3, "scrollerWidth":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mContentScroller:Landroid/view/View;

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v6, p2}, Landroid/view/View;->measure(II)V

    .line 140
    sub-int v5, v4, v3

    div-int/lit8 v0, v5, 0x2

    .line 141
    .local v0, "barWidth":I
    if-nez v0, :cond_0

    .line 142
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mLeftBar:Landroid/view/View;

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 143
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mRightBar:Landroid/view/View;

    invoke-virtual {v5, v9}, Landroid/view/View;->setVisibility(I)V

    .line 157
    :goto_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setMeasuredDimension(II)V

    .line 158
    return-void

    .line 145
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mLeftBar:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 146
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mRightBar:Landroid/view/View;

    invoke-virtual {v5, v8}, Landroid/view/View;->setVisibility(I)V

    .line 148
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->getPaddingLeft()I

    move-result v5

    sub-int v1, v0, v5

    .line 149
    .local v1, "leftBarWidth":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mLeftBar:Landroid/view/View;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v6, p2}, Landroid/view/View;->measure(II)V

    .line 152
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->getPaddingRight()I

    move-result v5

    sub-int v2, v0, v5

    .line 153
    .local v2, "rightBarWidth":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mRightBar:Landroid/view/View;

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v6, p2}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method public setScrollerWidth(I)V
    .locals 0
    .param p1, "scrollerWidth"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mScrollerWidth:I

    .line 76
    return-void
.end method

.method public setSideBarProportion(F)V
    .locals 6
    .param p1, "sideBarProportion"    # F

    .prologue
    const/4 v5, 0x0

    .line 89
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->mScrollerWidth:I

    sub-int/2addr v3, v4

    div-int/lit8 v0, v3, 0x2

    .line 91
    .local v0, "fullBarWidth":I
    int-to-float v3, v0

    mul-float/2addr v3, p1

    float-to-int v1, v3

    .line 93
    .local v1, "requestedBarWidth":I
    sub-int v2, v0, v1

    .line 95
    .local v2, "sidePadding":I
    invoke-virtual {p0, v2, v5, v2, v5}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setPadding(IIII)V

    .line 96
    return-void
.end method
