.class Lcom/google/android/finsky/layout/DetailsColumnLayout$12;
.super Ljava/lang/Object;
.source "DetailsColumnLayout.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsColumnLayout;->collapseCurrentlyExpandedSectionIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

.field final synthetic val$initialScrollerPaddingTop:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;I)V
    .locals 0

    .prologue
    .line 582
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iput p2, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;->val$initialScrollerPaddingTop:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 9
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    const/4 v8, 0x0

    .line 585
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    .line 586
    .local v2, "fraction":F
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mExpandedContainerTopPadding:I
    invoke-static {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$300(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I
    invoke-static {v6}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$200(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mExpandedContainerTopPadding:I
    invoke-static {v7}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$300(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    mul-float/2addr v6, v2

    float-to-int v6, v6

    add-int v1, v5, v6

    .line 591
    .local v1, "expanderPaddingTop":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v5, v5, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v5, v1}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->setTopPaddingOnTopView(I)V

    .line 594
    iget v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;->val$initialScrollerPaddingTop:I

    int-to-float v5, v5

    mul-float/2addr v5, v2

    float-to-int v4, v5

    .line 595
    .local v4, "scrollerPaddingTop":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceBottom:I
    invoke-static {v5}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$600(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v5

    invoke-static {v5, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 596
    .local v3, "maxPaddingBottom":I
    int-to-float v5, v3

    mul-float/2addr v5, v2

    float-to-int v0, v5

    .line 597
    .local v0, "expanderPaddingBottom":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v5, v5, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedScroller:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    invoke-virtual {v5, v8, v4, v8, v0}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->setPadding(IIII)V

    .line 599
    return-void
.end method
