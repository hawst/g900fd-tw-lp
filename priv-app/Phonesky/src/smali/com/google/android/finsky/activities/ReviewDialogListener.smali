.class public Lcom/google/android/finsky/activities/ReviewDialogListener;
.super Ljava/lang/Object;
.source "ReviewDialogListener.java"

# interfaces
.implements Lcom/google/android/finsky/activities/ReviewFeedbackDialog$Listener;


# instance fields
.field private final mAccountName:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mDoc:Lcom/google/android/finsky/api/model/Document;

.field private final mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

.field private final mReviewSamplesSection:Lcom/google/android/finsky/layout/ReviewSamplesSection;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeDetails;Lcom/google/android/finsky/layout/ReviewSamplesSection;Lcom/google/android/finsky/layout/RateReviewSection;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parentFragment"    # Landroid/support/v4/app/Fragment;
    .param p3, "accountName"    # Ljava/lang/String;
    .param p4, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "detailsData"    # Lcom/google/android/finsky/api/model/DfeDetails;
    .param p6, "reviewSamplesSection"    # Lcom/google/android/finsky/layout/ReviewSamplesSection;
    .param p7, "rateReviewSection"    # Lcom/google/android/finsky/layout/RateReviewSection;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mContext:Landroid/content/Context;

    .line 39
    iput-object p3, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mAccountName:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 41
    iput-object p6, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mReviewSamplesSection:Lcom/google/android/finsky/layout/ReviewSamplesSection;

    .line 42
    iput-object p7, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/ReviewDialogListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/ReviewDialogListener;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialogListener;->refreshUserReview()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/ReviewDialogListener;)Lcom/google/android/finsky/layout/ReviewSamplesSection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/ReviewDialogListener;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mReviewSamplesSection:Lcom/google/android/finsky/layout/ReviewSamplesSection;

    return-object v0
.end method

.method private refreshUserReview()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mReviewSamplesSection:Lcom/google/android/finsky/layout/ReviewSamplesSection;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->invalidateCurrentReviewUrl()V

    .line 101
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mReviewSamplesSection:Lcom/google/android/finsky/layout/ReviewSamplesSection;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->refresh()V

    .line 102
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mRateReviewSection:Lcom/google/android/finsky/layout/RateReviewSection;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/RateReviewSection;->refresh()V

    .line 106
    :cond_0
    return-void
.end method


# virtual methods
.method public onCancelReview()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewDialogListener;->refreshUserReview()V

    .line 92
    return-void
.end method

.method public onDeleteReview(Ljava/lang/String;)V
    .locals 5
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mAccountName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mContext:Landroid/content/Context;

    new-instance v4, Lcom/google/android/finsky/activities/ReviewDialogListener$2;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/ReviewDialogListener$2;-><init>(Lcom/google/android/finsky/activities/ReviewDialogListener;)V

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/utils/RateReviewHelper;->deleteReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V

    .line 85
    return-void
.end method

.method public onReviewFeedback(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V
    .locals 3
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "reviewId"    # Ljava/lang/String;
    .param p3, "newRating"    # Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/finsky/api/model/DfeRateReview;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;->getRpcId()I

    move-result v2

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/google/android/finsky/api/model/DfeRateReview;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Ljava/lang/String;I)V

    .line 117
    .local v0, "request":Lcom/google/android/finsky/api/model/DfeRateReview;
    new-instance v1, Lcom/google/android/finsky/activities/ReviewDialogListener$3;

    invoke-direct {v1, p0, p3}, Lcom/google/android/finsky/activities/ReviewDialogListener$3;-><init>(Lcom/google/android/finsky/activities/ReviewDialogListener;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeRateReview;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 126
    new-instance v1, Lcom/google/android/finsky/activities/ReviewDialogListener$4;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/ReviewDialogListener$4;-><init>(Lcom/google/android/finsky/activities/ReviewDialogListener;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeRateReview;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 132
    return-void
.end method

.method public onSaveReview(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;)V
    .locals 10
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "rating"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "comment"    # Ljava/lang/String;
    .param p5, "author"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mAccountName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mContext:Landroid/content/Context;

    new-instance v8, Lcom/google/android/finsky/activities/ReviewDialogListener$1;

    invoke-direct {v8, p0}, Lcom/google/android/finsky/activities/ReviewDialogListener$1;-><init>(Lcom/google/android/finsky/activities/ReviewDialogListener;)V

    const/16 v9, 0x4b3

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/utils/RateReviewHelper;->updateReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;I)V

    .line 65
    return-void
.end method

.method protected toast(II)V
    .locals 1
    .param p1, "resourceId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 139
    return-void
.end method
