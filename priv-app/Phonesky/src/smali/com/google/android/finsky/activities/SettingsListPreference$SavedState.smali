.class Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;
.super Landroid/preference/Preference$BaseSavedState;
.source "SettingsListPreference.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/SettingsListPreference;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private entries:[Ljava/lang/CharSequence;

.field private entryValues:[Ljava/lang/CharSequence;

.field private summary:Ljava/lang/CharSequence;

.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 138
    new-instance v0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState$1;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Landroid/preference/Preference$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entries:[Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->readCharSequenceArray(Landroid/os/Parcel;[Ljava/lang/CharSequence;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entryValues:[Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->readCharSequenceArray(Landroid/os/Parcel;[Ljava/lang/CharSequence;)V

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->value:Ljava/lang/String;

    .line 98
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->summary:Ljava/lang/CharSequence;

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 102
    invoke-direct {p0, p1}, Landroid/preference/Preference$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;)[Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entries:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;[Ljava/lang/CharSequence;)[Ljava/lang/CharSequence;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;
    .param p1, "x1"    # [Ljava/lang/CharSequence;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entries:[Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;)[Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entryValues:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;[Ljava/lang/CharSequence;)[Ljava/lang/CharSequence;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;
    .param p1, "x1"    # [Ljava/lang/CharSequence;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entryValues:[Ljava/lang/CharSequence;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->value:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->value:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->summary:Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->summary:Ljava/lang/CharSequence;

    return-object p1
.end method

.method private static readCharSequenceArray(Landroid/os/Parcel;[Ljava/lang/CharSequence;)V
    .locals 4
    .param p0, "source"    # Landroid/os/Parcel;
    .param p1, "val"    # [Ljava/lang/CharSequence;

    .prologue
    .line 127
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 128
    .local v0, "N":I
    array-length v2, p1

    if-ne v0, v2, :cond_0

    .line 129
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 130
    sget-object v2, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, p0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    aput-object v2, p1, v1

    .line 129
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 133
    .end local v1    # "i":I
    :cond_0
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "bad array lengths"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 135
    .restart local v1    # "i":I
    :cond_1
    return-void
.end method

.method private static writeCharSequenceArray(Landroid/os/Parcel;[Ljava/lang/CharSequence;)V
    .locals 4
    .param p0, "source"    # Landroid/os/Parcel;
    .param p1, "val"    # [Ljava/lang/CharSequence;

    .prologue
    .line 115
    if-eqz p1, :cond_0

    .line 116
    array-length v0, p1

    .line 117
    .local v0, "N":I
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 118
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 119
    aget-object v2, p1, v1

    const/4 v3, 0x0

    invoke-static {v2, p0, v3}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 118
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 122
    .end local v0    # "N":I
    .end local v1    # "i":I
    :cond_0
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    :cond_1
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 107
    invoke-super {p0, p1, p2}, Landroid/preference/Preference$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entries:[Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->writeCharSequenceArray(Landroid/os/Parcel;[Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->entryValues:[Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->writeCharSequenceArray(Landroid/os/Parcel;[Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsListPreference$SavedState;->summary:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 112
    return-void
.end method
