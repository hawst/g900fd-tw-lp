.class public Lcom/google/android/finsky/widget/recommendation/Recommendation;
.super Ljava/lang/Object;
.source "Recommendation.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field private static final APP_IMAGE_TYPES:[I

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/widget/recommendation/Recommendation;",
            ">;"
        }
    .end annotation
.end field

.field private static final NONAPP_IMAGE_TYPES:[I


# instance fields
.field private final mDocument:Lcom/google/android/finsky/api/model/Document;

.field private final mExpirationTimeMs:J

.field private final mImage:Lcom/google/android/finsky/protos/Common$Image;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->NONAPP_IMAGE_TYPES:[I

    .line 24
    sget-object v0, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->PROMO_IMAGE_TYPES:[I

    sput-object v0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->APP_IMAGE_TYPES:[I

    .line 104
    new-instance v0, Lcom/google/android/finsky/widget/recommendation/Recommendation$1;

    invoke-direct {v0}, Lcom/google/android/finsky/widget/recommendation/Recommendation$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void

    .line 21
    :array_0
    .array-data 4
        0x4
        0x2
        0x0
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/finsky/api/model/Document;)V
    .locals 4
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 31
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-object v0, Lcom/google/android/finsky/config/G;->recommendationTtlMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, v2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/finsky/widget/recommendation/Recommendation;-><init>(Lcom/google/android/finsky/api/model/Document;J)V

    .line 32
    return-void
.end method

.method private constructor <init>(Lcom/google/android/finsky/api/model/Document;J)V
    .locals 2
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "expirationTimeMs"    # J

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 36
    invoke-direct {p0}, Lcom/google/android/finsky/widget/recommendation/Recommendation;->fetchImage()Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 37
    iput-wide p2, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mExpirationTimeMs:J

    .line 38
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/api/model/Document;JLcom/google/android/finsky/widget/recommendation/Recommendation$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "x1"    # J
    .param p4, "x2"    # Lcom/google/android/finsky/widget/recommendation/Recommendation$1;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/widget/recommendation/Recommendation;-><init>(Lcom/google/android/finsky/api/model/Document;J)V

    return-void
.end method

.method private fetchImage()Lcom/google/android/finsky/protos/Common$Image;
    .locals 8

    .prologue
    .line 61
    iget-object v6, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_0

    sget-object v5, Lcom/google/android/finsky/widget/recommendation/Recommendation;->APP_IMAGE_TYPES:[I

    .line 63
    .local v5, "preference":[I
    :goto_0
    move-object v0, v5

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v4, :cond_2

    aget v2, v0, v1

    .line 64
    .local v2, "imageType":I
    iget-object v6, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6, v2}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v3

    .line 65
    .local v3, "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 66
    const/4 v6, 0x0

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/protos/Common$Image;

    .line 69
    .end local v2    # "imageType":I
    .end local v3    # "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    :goto_2
    return-object v6

    .line 61
    .end local v0    # "arr$":[I
    .end local v1    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "preference":[I
    :cond_0
    sget-object v5, Lcom/google/android/finsky/widget/recommendation/Recommendation;->NONAPP_IMAGE_TYPES:[I

    goto :goto_0

    .line 63
    .restart local v0    # "arr$":[I
    .restart local v1    # "i$":I
    .restart local v2    # "imageType":I
    .restart local v3    # "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    .restart local v4    # "len$":I
    .restart local v5    # "preference":[I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 69
    .end local v2    # "imageType":I
    .end local v3    # "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    :cond_2
    const/4 v6, 0x0

    goto :goto_2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 81
    instance-of v0, p1, Lcom/google/android/finsky/widget/recommendation/Recommendation;

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    .line 84
    .end local p1    # "o":Ljava/lang/Object;
    :goto_0
    return v0

    .restart local p1    # "o":Ljava/lang/Object;
    :cond_0
    check-cast p1, Lcom/google/android/finsky/widget/recommendation/Recommendation;

    .end local p1    # "o":Ljava/lang/Object;
    iget-object v0, p1, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public getBackend()I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    return v0
.end method

.method public getDocument()Lcom/google/android/finsky/api/model/Document;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method public getImage()Lcom/google/android/finsky/protos/Common$Image;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mImage:Lcom/google/android/finsky/protos/Common$Image;

    return-object v0
.end method

.method public getImageType()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mImage:Lcom/google/android/finsky/protos/Common$Image;

    iget v0, v0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 74
    iget-object v1, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 75
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mImage:Lcom/google/android/finsky/protos/Common$Image;

    iget v2, v2, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    add-int v0, v1, v2

    .line 76
    return v0
.end method

.method public isExpired()Z
    .locals 4

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mExpirationTimeMs:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasNeutralDismissal()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getNeutralDismissal()Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Dismissal;->url:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 99
    const-wide/16 v0, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 100
    iget-object v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mDocument:Lcom/google/android/finsky/api/model/Document;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 101
    iget-wide v0, p0, Lcom/google/android/finsky/widget/recommendation/Recommendation;->mExpirationTimeMs:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 102
    return-void
.end method
