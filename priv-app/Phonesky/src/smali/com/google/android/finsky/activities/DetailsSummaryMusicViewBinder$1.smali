.class Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;
.super Ljava/lang/Object;
.source "DetailsSummaryMusicViewBinder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->displayActionButtonsIfNecessary(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

.field final synthetic val$account:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iput-object p2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x2

    .line 80
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/16 v2, 0x11e

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v4, v4, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 82
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v1, v1, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/IntentUtils;->isMusicAppWithAllAccessFlowInstalled(Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v1, v1, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v1, v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showAppNeededDialog(I)V

    .line 93
    :goto_0
    return-void

    .line 89
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v2, v1, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/google/android/finsky/config/G;->musicAllAccessSignUpIntentUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;->val$account:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v5, v1, v3}, Lcom/google/android/finsky/utils/IntentUtils;->buildConsumptionAppUrlIntent(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 92
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$1;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v1, v1, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
