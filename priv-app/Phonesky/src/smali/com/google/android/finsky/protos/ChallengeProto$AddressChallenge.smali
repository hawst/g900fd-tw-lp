.class public final Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddressChallenge"
.end annotation


# instance fields
.field public address:Lcom/google/android/finsky/protos/BillingAddress$Address;

.field public checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

.field public descriptionHtml:Ljava/lang/String;

.field public errorHtml:Ljava/lang/String;

.field public errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

.field public hasDescriptionHtml:Z

.field public hasErrorHtml:Z

.field public hasResponseAddressParam:Z

.field public hasResponseCheckboxesParam:Z

.field public hasTitle:Z

.field public requiredField:[I

.field public responseAddressParam:Ljava/lang/String;

.field public responseCheckboxesParam:Ljava/lang/String;

.field public supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 402
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 403
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->clear()Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 404
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 407
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseAddressParam:Ljava/lang/String;

    .line 408
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasResponseAddressParam:Z

    .line 409
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseCheckboxesParam:Ljava/lang/String;

    .line 410
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasResponseCheckboxesParam:Z

    .line 411
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    .line 412
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasTitle:Z

    .line 413
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    .line 414
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasDescriptionHtml:Z

    .line 415
    invoke-static {}, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    .line 416
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 417
    invoke-static {}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .line 418
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorHtml:Ljava/lang/String;

    .line 419
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasErrorHtml:Z

    .line 420
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    .line 421
    invoke-static {}, Lcom/google/android/finsky/protos/ChallengeProto$Country;->emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    .line 422
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->cachedSize:I

    .line 423
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 481
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v3

    .line 482
    .local v3, "size":I
    iget-boolean v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasResponseAddressParam:Z

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseAddressParam:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 483
    :cond_0
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseAddressParam:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 486
    :cond_1
    iget-boolean v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasResponseCheckboxesParam:Z

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseCheckboxesParam:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 487
    :cond_2
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseCheckboxesParam:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 490
    :cond_3
    iget-boolean v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasTitle:Z

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 491
    :cond_4
    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 494
    :cond_5
    iget-boolean v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasDescriptionHtml:Z

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    .line 495
    :cond_6
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 498
    :cond_7
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v4, v4

    if-lez v4, :cond_9

    .line 499
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v4, v4

    if-ge v2, v4, :cond_9

    .line 500
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    aget-object v1, v4, v2

    .line 501
    .local v1, "element":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    if-eqz v1, :cond_8

    .line 502
    const/4 v4, 0x5

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 499
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 507
    .end local v1    # "element":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    .end local v2    # "i":I
    :cond_9
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v4, :cond_a

    .line 508
    const/4 v4, 0x6

    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 511
    :cond_a
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v4, v4

    if-lez v4, :cond_c

    .line 512
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v4, v4

    if-ge v2, v4, :cond_c

    .line 513
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    aget-object v1, v4, v2

    .line 514
    .local v1, "element":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    if-eqz v1, :cond_b

    .line 515
    const/4 v4, 0x7

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 512
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 520
    .end local v1    # "element":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .end local v2    # "i":I
    :cond_c
    iget-boolean v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasErrorHtml:Z

    if-nez v4, :cond_d

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorHtml:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_e

    .line 521
    :cond_d
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorHtml:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 524
    :cond_e
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    if-eqz v4, :cond_10

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    array-length v4, v4

    if-lez v4, :cond_10

    .line 525
    const/4 v0, 0x0

    .line 526
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    array-length v4, v4

    if-ge v2, v4, :cond_f

    .line 527
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    aget v1, v4, v2

    .line 528
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 526
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 531
    .end local v1    # "element":I
    :cond_f
    add-int/2addr v3, v0

    .line 532
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    .line 534
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_10
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    if-eqz v4, :cond_12

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    array-length v4, v4

    if-lez v4, :cond_12

    .line 535
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    array-length v4, v4

    if-ge v2, v4, :cond_12

    .line 536
    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    aget-object v1, v4, v2

    .line 537
    .local v1, "element":Lcom/google/android/finsky/protos/ChallengeProto$Country;
    if-eqz v1, :cond_11

    .line 538
    const/16 v4, 0xa

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 535
    :cond_11
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 543
    .end local v1    # "element":Lcom/google/android/finsky/protos/ChallengeProto$Country;
    .end local v2    # "i":I
    :cond_12
    return v3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    .locals 17
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 551
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v9

    .line 552
    .local v9, "tag":I
    sparse-switch v9, :sswitch_data_0

    .line 556
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v14

    if-nez v14, :cond_0

    .line 557
    :sswitch_0
    return-object p0

    .line 562
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseAddressParam:Ljava/lang/String;

    .line 563
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasResponseAddressParam:Z

    goto :goto_0

    .line 567
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseCheckboxesParam:Ljava/lang/String;

    .line 568
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasResponseCheckboxesParam:Z

    goto :goto_0

    .line 572
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    .line 573
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasTitle:Z

    goto :goto_0

    .line 577
    :sswitch_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    .line 578
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasDescriptionHtml:Z

    goto :goto_0

    .line 582
    :sswitch_5
    const/16 v14, 0x2a

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 584
    .local v1, "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-nez v14, :cond_2

    const/4 v3, 0x0

    .line 585
    .local v3, "i":I
    :goto_1
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    .line 587
    .local v7, "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    if-eqz v3, :cond_1

    .line 588
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 590
    :cond_1
    :goto_2
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_3

    .line 591
    new-instance v14, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;-><init>()V

    aput-object v14, v7, v3

    .line 592
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 593
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 590
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 584
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v3, v14

    goto :goto_1

    .line 596
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    :cond_3
    new-instance v14, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;-><init>()V

    aput-object v14, v7, v3

    .line 597
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 598
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    goto/16 :goto_0

    .line 602
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-nez v14, :cond_4

    .line 603
    new-instance v14, Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/BillingAddress$Address;-><init>()V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 605
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 609
    :sswitch_7
    const/16 v14, 0x3a

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 611
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    if-nez v14, :cond_6

    const/4 v3, 0x0

    .line 612
    .restart local v3    # "i":I
    :goto_3
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .line 614
    .local v7, "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    if-eqz v3, :cond_5

    .line 615
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 617
    :cond_5
    :goto_4
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_7

    .line 618
    new-instance v14, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;-><init>()V

    aput-object v14, v7, v3

    .line 619
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 620
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 617
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 611
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v3, v14

    goto :goto_3

    .line 623
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    :cond_7
    new-instance v14, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;-><init>()V

    aput-object v14, v7, v3

    .line 624
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 625
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    goto/16 :goto_0

    .line 629
    .end local v1    # "arrayLength":I
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    :sswitch_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorHtml:Ljava/lang/String;

    .line 630
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasErrorHtml:Z

    goto/16 :goto_0

    .line 634
    :sswitch_9
    const/16 v14, 0x48

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v5

    .line 636
    .local v5, "length":I
    new-array v12, v5, [I

    .line 637
    .local v12, "validValues":[I
    const/4 v10, 0x0

    .line 638
    .local v10, "validCount":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    move v11, v10

    .end local v10    # "validCount":I
    .local v11, "validCount":I
    :goto_5
    if-ge v3, v5, :cond_9

    .line 639
    if-eqz v3, :cond_8

    .line 640
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 642
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 643
    .local v13, "value":I
    packed-switch v13, :pswitch_data_0

    move v10, v11

    .line 638
    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    :goto_6
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    goto :goto_5

    .line 664
    :pswitch_0
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    aput v13, v12, v11

    goto :goto_6

    .line 668
    .end local v10    # "validCount":I
    .end local v13    # "value":I
    .restart local v11    # "validCount":I
    :cond_9
    if-eqz v11, :cond_0

    .line 669
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    if-nez v14, :cond_a

    const/4 v3, 0x0

    .line 670
    :goto_7
    if-nez v3, :cond_b

    array-length v14, v12

    if-ne v11, v14, :cond_b

    .line 671
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    goto/16 :goto_0

    .line 669
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    array-length v3, v14

    goto :goto_7

    .line 673
    :cond_b
    add-int v14, v3, v11

    new-array v7, v14, [I

    .line 674
    .local v7, "newArray":[I
    if-eqz v3, :cond_c

    .line 675
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 677
    :cond_c
    const/4 v14, 0x0

    invoke-static {v12, v14, v7, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 678
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    goto/16 :goto_0

    .line 684
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v7    # "newArray":[I
    .end local v11    # "validCount":I
    .end local v12    # "validValues":[I
    :sswitch_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 685
    .local v2, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 687
    .local v6, "limit":I
    const/4 v1, 0x0

    .line 688
    .restart local v1    # "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 689
    .local v8, "startPos":I
    :goto_8
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_d

    .line 690
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    packed-switch v14, :pswitch_data_1

    goto :goto_8

    .line 711
    :pswitch_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 715
    :cond_d
    if-eqz v1, :cond_11

    .line 716
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 717
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    if-nez v14, :cond_f

    const/4 v3, 0x0

    .line 718
    .restart local v3    # "i":I
    :goto_9
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 719
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_e

    .line 720
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 722
    :cond_e
    :goto_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_10

    .line 723
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 724
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_2

    goto :goto_a

    .line 745
    :pswitch_2
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput v13, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_a

    .line 717
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    .end local v13    # "value":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    array-length v3, v14

    goto :goto_9

    .line 749
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_10
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    .line 751
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_11
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 755
    .end local v1    # "arrayLength":I
    .end local v2    # "bytes":I
    .end local v6    # "limit":I
    .end local v8    # "startPos":I
    :sswitch_b
    const/16 v14, 0x52

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v1

    .line 757
    .restart local v1    # "arrayLength":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    if-nez v14, :cond_13

    const/4 v3, 0x0

    .line 758
    .restart local v3    # "i":I
    :goto_b
    add-int v14, v3, v1

    new-array v7, v14, [Lcom/google/android/finsky/protos/ChallengeProto$Country;

    .line 760
    .local v7, "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$Country;
    if-eqz v3, :cond_12

    .line 761
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 763
    :cond_12
    :goto_c
    array-length v14, v7

    add-int/lit8 v14, v14, -0x1

    if-ge v3, v14, :cond_14

    .line 764
    new-instance v14, Lcom/google/android/finsky/protos/ChallengeProto$Country;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/ChallengeProto$Country;-><init>()V

    aput-object v14, v7, v3

    .line 765
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 766
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 763
    add-int/lit8 v3, v3, 0x1

    goto :goto_c

    .line 757
    .end local v3    # "i":I
    .end local v7    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$Country;
    :cond_13
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    array-length v3, v14

    goto :goto_b

    .line 769
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$Country;
    :cond_14
    new-instance v14, Lcom/google/android/finsky/protos/ChallengeProto$Country;

    invoke-direct {v14}, Lcom/google/android/finsky/protos/ChallengeProto$Country;-><init>()V

    aput-object v14, v7, v3

    .line 770
    aget-object v14, v7, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 771
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    goto/16 :goto_0

    .line 552
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
    .end sparse-switch

    .line 643
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 690
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 724
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 350
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 429
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasResponseAddressParam:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseAddressParam:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 430
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseAddressParam:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 432
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasResponseCheckboxesParam:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseCheckboxesParam:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 433
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->responseCheckboxesParam:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 435
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasTitle:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 436
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 438
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasDescriptionHtml:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 439
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 441
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 442
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 443
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    aget-object v0, v2, v1

    .line 444
    .local v0, "element":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    if-eqz v0, :cond_8

    .line 445
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 442
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 449
    .end local v0    # "element":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    .end local v1    # "i":I
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v2, :cond_a

    .line 450
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 452
    :cond_a
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 453
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 454
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    aget-object v0, v2, v1

    .line 455
    .local v0, "element":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    if-eqz v0, :cond_b

    .line 456
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 453
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 460
    .end local v0    # "element":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .end local v1    # "i":I
    :cond_c
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->hasErrorHtml:Z

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 461
    :cond_d
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 463
    :cond_e
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    array-length v2, v2

    if-lez v2, :cond_f

    .line 464
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    array-length v2, v2

    if-ge v1, v2, :cond_f

    .line 465
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    aget v3, v3, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 464
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 468
    .end local v1    # "i":I
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 469
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 470
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    aget-object v0, v2, v1

    .line 471
    .local v0, "element":Lcom/google/android/finsky/protos/ChallengeProto$Country;
    if-eqz v0, :cond_10

    .line 472
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 469
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 476
    .end local v0    # "element":Lcom/google/android/finsky/protos/ChallengeProto$Country;
    .end local v1    # "i":I
    :cond_11
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 477
    return-void
.end method
