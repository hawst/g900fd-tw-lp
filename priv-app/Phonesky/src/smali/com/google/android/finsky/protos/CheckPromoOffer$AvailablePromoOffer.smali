.class public final Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CheckPromoOffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CheckPromoOffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AvailablePromoOffer"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;


# instance fields
.field public addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 524
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 525
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->clear()Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    .line 526
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    .locals 2

    .prologue
    .line 510
    sget-object v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->_emptyArray:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    if-nez v0, :cond_1

    .line 511
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 513
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->_emptyArray:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    if-nez v0, :cond_0

    .line 514
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    sput-object v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->_emptyArray:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    .line 516
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->_emptyArray:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    return-object v0

    .line 516
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    .line 530
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->cachedSize:I

    .line 531
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 545
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 546
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    if-eqz v1, :cond_0

    .line 547
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 550
    :cond_0
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 558
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 559
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 563
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 564
    :sswitch_0
    return-object p0

    .line 569
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    if-nez v1, :cond_1

    .line 570
    new-instance v1, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    .line 572
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 559
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 504
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 537
    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    if-eqz v0, :cond_0

    .line 538
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->addCreditCardOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 540
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 541
    return-void
.end method
