.class public final Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WifiAutoUpdateAttempt"
.end annotation


# instance fields
.field public autoUpdateSuccess:Z

.field public hasAutoUpdateSuccess:Z

.field public hasNumFailedAttempts:Z

.field public hasSkippedDueToProjection:Z

.field public hasTimeSinceFirstFailMs:Z

.field public hasWifiCheckIntervalMs:Z

.field public numFailedAttempts:I

.field public skippedDueToProjection:Z

.field public timeSinceFirstFailMs:J

.field public wifiCheckIntervalMs:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4917
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4918
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->clear()Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    .line 4919
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 4922
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->autoUpdateSuccess:Z

    .line 4923
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasAutoUpdateSuccess:Z

    .line 4924
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->numFailedAttempts:I

    .line 4925
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasNumFailedAttempts:Z

    .line 4926
    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->timeSinceFirstFailMs:J

    .line 4927
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasTimeSinceFirstFailMs:Z

    .line 4928
    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->wifiCheckIntervalMs:J

    .line 4929
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasWifiCheckIntervalMs:Z

    .line 4930
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->skippedDueToProjection:Z

    .line 4931
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasSkippedDueToProjection:Z

    .line 4932
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->cachedSize:I

    .line 4933
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 4959
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4960
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasAutoUpdateSuccess:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->autoUpdateSuccess:Z

    if-eqz v1, :cond_1

    .line 4961
    :cond_0
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->autoUpdateSuccess:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4964
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasNumFailedAttempts:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->numFailedAttempts:I

    if-eqz v1, :cond_3

    .line 4965
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->numFailedAttempts:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4968
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasTimeSinceFirstFailMs:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->timeSinceFirstFailMs:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 4969
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->timeSinceFirstFailMs:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4972
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasWifiCheckIntervalMs:Z

    if-nez v1, :cond_6

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->wifiCheckIntervalMs:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 4973
    :cond_6
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->wifiCheckIntervalMs:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4976
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasSkippedDueToProjection:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->skippedDueToProjection:Z

    if-eqz v1, :cond_9

    .line 4977
    :cond_8
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->skippedDueToProjection:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4980
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 4988
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4989
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4993
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4994
    :sswitch_0
    return-object p0

    .line 4999
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->autoUpdateSuccess:Z

    .line 5000
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasAutoUpdateSuccess:Z

    goto :goto_0

    .line 5004
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->numFailedAttempts:I

    .line 5005
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasNumFailedAttempts:Z

    goto :goto_0

    .line 5009
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->timeSinceFirstFailMs:J

    .line 5010
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasTimeSinceFirstFailMs:Z

    goto :goto_0

    .line 5014
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->wifiCheckIntervalMs:J

    .line 5015
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasWifiCheckIntervalMs:Z

    goto :goto_0

    .line 5019
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->skippedDueToProjection:Z

    .line 5020
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasSkippedDueToProjection:Z

    goto :goto_0

    .line 4989
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4880
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 4939
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasAutoUpdateSuccess:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->autoUpdateSuccess:Z

    if-eqz v0, :cond_1

    .line 4940
    :cond_0
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->autoUpdateSuccess:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4942
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasNumFailedAttempts:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->numFailedAttempts:I

    if-eqz v0, :cond_3

    .line 4943
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->numFailedAttempts:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4945
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasTimeSinceFirstFailMs:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->timeSinceFirstFailMs:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 4946
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->timeSinceFirstFailMs:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4948
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasWifiCheckIntervalMs:Z

    if-nez v0, :cond_6

    iget-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->wifiCheckIntervalMs:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 4949
    :cond_6
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->wifiCheckIntervalMs:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4951
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->hasSkippedDueToProjection:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->skippedDueToProjection:Z

    if-eqz v0, :cond_9

    .line 4952
    :cond_8
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$WifiAutoUpdateAttempt;->skippedDueToProjection:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 4954
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4955
    return-void
.end method
