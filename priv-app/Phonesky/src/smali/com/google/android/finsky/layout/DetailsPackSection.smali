.class public Lcom/google/android/finsky/layout/DetailsPackSection;
.super Landroid/widget/LinearLayout;
.source "DetailsPackSection.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoBottomSeparator;
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoTopSeparator;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field protected mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field protected mDoc:Lcom/google/android/finsky/api/model/Document;

.field protected mHeader:Ljava/lang/String;

.field protected mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

.field protected mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

.field private mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

.field protected mListingContent:Landroid/widget/FrameLayout;

.field protected mListingLayout:Landroid/widget/LinearLayout;

.field private mMaxItemsCount:I

.field private mMaxItemsPerRow:I

.field private mMoreUrl:Ljava/lang/String;

.field protected mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mNoDataHeader:Ljava/lang/String;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field protected mSubheader:Ljava/lang/String;

.field protected mToc:Lcom/google/android/finsky/api/model/DfeToc;

.field protected mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsPackSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 106
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 108
    const/16 v0, 0x190

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 111
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 112
    .local v11, "res":Landroid/content/res/Resources;
    const v0, 0x7f090054

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 118
    .local v6, "mainBgColor":I
    const v0, 0x7f0f0007

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->getPaddingLeft()I

    move-result v8

    .line 120
    .local v8, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->getPaddingTop()I

    move-result v10

    .line 121
    .local v10, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->getPaddingRight()I

    move-result v9

    .line 122
    .local v9, "paddingRight":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->getPaddingBottom()I

    move-result v7

    .line 127
    .local v7, "paddingBottom":I
    const v0, 0x7f0b0056

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 128
    .local v2, "extraInset":I
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    new-instance v1, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v1, v6}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsPackSection;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 131
    invoke-virtual {p0, v8, v10, v9, v7}, Lcom/google/android/finsky/layout/DetailsPackSection;->setPadding(IIII)V

    .line 135
    .end local v2    # "extraInset":I
    .end local v7    # "paddingBottom":I
    .end local v8    # "paddingLeft":I
    .end local v9    # "paddingRight":I
    .end local v10    # "paddingTop":I
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/layout/DetailsPackSection;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/DetailsPackSection;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsPackSection;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mMoreUrl:Ljava/lang/String;

    return-object v0
.end method

.method private attachToInternalRequest()V
    .locals 3

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    if-nez v0, :cond_0

    .line 277
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot attach when no request is held internally"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 285
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    .line 287
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToDataMode()V

    .line 288
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->prepareAndPopulateContent()V

    .line 300
    :cond_1
    :goto_0
    return-void

    .line 290
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->isMoreAvailable()Z

    move-result v0

    if-nez v0, :cond_3

    .line 291
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->handleEmptyData()V

    goto :goto_0

    .line 292
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->inErrorState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->getVolleyError()Lcom/android/volley/VolleyError;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToErrorMode(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private detachListeners()V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 203
    :cond_0
    return-void
.end method

.method private prepareAndPopulateContent()V
    .locals 2

    .prologue
    .line 325
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v0

    .line 326
    .local v0, "cookie":[B
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 329
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->populateHeader()V

    .line 330
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->populateContent()V

    .line 331
    return-void
.end method

.method private setupItemListRequest()V
    .locals 4

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->detachListeners()V

    .line 313
    new-instance v0, Lcom/google/android/finsky/api/model/DfeList;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mUrl:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    .line 315
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->attachToInternalRequest()V

    .line 316
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 317
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 318
    return-void
.end method

.method private setupView()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->clearTransientState()V

    .line 268
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 269
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->setupItemListRequest()V

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->handleEmptyData()V

    goto :goto_0
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "header"    # Ljava/lang/String;
    .param p3, "subheader"    # Ljava/lang/String;
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "moreUrl"    # Ljava/lang/String;
    .param p6, "maxItemsPerRow"    # I
    .param p7, "maxRowCount"    # I
    .param p8, "hasDetailsLoaded"    # Z
    .param p9, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 170
    if-nez p8, :cond_0

    .line 171
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    .line 196
    :goto_0
    return-void

    .line 175
    :cond_0
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 176
    iput-object p4, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mUrl:Ljava/lang/String;

    .line 177
    iput-object p2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mHeader:Ljava/lang/String;

    .line 178
    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mSubheader:Ljava/lang/String;

    .line 179
    iput-object p5, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mMoreUrl:Ljava/lang/String;

    .line 180
    iput p6, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mMaxItemsPerRow:I

    .line 181
    mul-int v0, p7, p6

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mMaxItemsCount:I

    .line 182
    iput-object p9, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    if-nez v0, :cond_1

    .line 186
    new-instance v0, Lcom/google/android/finsky/layout/LayoutSwitcher;

    const v1, 0x7f0a0147

    new-instance v2, Lcom/google/android/finsky/layout/DetailsPackSection$1;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/layout/DetailsPackSection$1;-><init>(Lcom/google/android/finsky/layout/DetailsPackSection;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/finsky/layout/LayoutSwitcher;-><init>(Landroid/view/View;ILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    .line 195
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->setupView()V

    goto :goto_0
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 402
    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 403
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected handleEmptyData()V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/16 v6, 0x8

    .line 303
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mNoDataHeader:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    .line 309
    :goto_0
    return-void

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mHeader:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mNoDataHeader:Ljava/lang/String;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 307
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingContent:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;)V
    .locals 0
    .param p1, "api"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 149
    iput-object p2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 150
    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 151
    iput-object p4, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 152
    return-void
.end method

.method public onDataChanged()V
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToDataMode()V

    .line 361
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 362
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->handleEmptyData()V

    .line 367
    :goto_0
    return-void

    .line 364
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsPackSection;->setVisibility(I)V

    .line 365
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->prepareAndPopulateContent()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 378
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->detachListeners()V

    .line 381
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 385
    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 386
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    if-eqz v0, :cond_0

    .line 372
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToErrorMode(Ljava/lang/String;)V

    .line 374
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 141
    const v0, 0x7f0a013f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsPackSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    .line 142
    const v0, 0x7f0a0146

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsPackSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingContent:Landroid/widget/FrameLayout;

    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingContent:Landroid/widget/FrameLayout;

    const v1, 0x7f0a0147

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingLayout:Landroid/widget/LinearLayout;

    .line 144
    return-void
.end method

.method protected populateContent()V
    .locals 15

    .prologue
    .line 209
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 215
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v2

    iget v3, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mMaxItemsCount:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 216
    .local v10, "itemCount":I
    iget v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mMaxItemsPerRow:I

    add-int/2addr v2, v10

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mMaxItemsPerRow:I

    div-int v12, v2, v3

    .line 217
    .local v12, "rowCount":I
    const/4 v11, 0x0

    .line 219
    .local v11, "itemIndex":I
    if-nez v12, :cond_1

    .line 220
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->handleEmptyData()V

    .line 257
    :cond_0
    return-void

    .line 224
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingContent:Landroid/widget/FrameLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 226
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v9

    .line 227
    .local v9, "inflater":Landroid/view/LayoutInflater;
    const/4 v13, 0x0

    .local v13, "rowNum":I
    :goto_0
    if-ge v13, v12, :cond_0

    .line 228
    const v2, 0x7f040062

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingLayout:Landroid/widget/LinearLayout;

    const/4 v4, 0x0

    invoke-virtual {v9, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/finsky/layout/BucketRow;

    .line 231
    .local v14, "rowView":Lcom/google/android/finsky/layout/BucketRow;
    if-ge v11, v10, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2, v11}, Lcom/google/android/finsky/api/model/DfeList;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    move-object v8, v2

    .line 233
    .local v8, "firstEntryInRowData":Lcom/google/android/finsky/api/model/Document;
    :goto_1
    if-eqz v8, :cond_0

    .line 237
    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getArtistDetails()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    move-result-object v2

    if-eqz v2, :cond_3

    const v6, 0x7f040117

    .line 240
    .local v6, "cardLayoutId":I
    :goto_2
    const/4 v7, 0x0

    .local v7, "colNum":I
    :goto_3
    iget v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mMaxItemsPerRow:I

    if-ge v7, v2, :cond_6

    .line 241
    if-ge v11, v10, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2, v11}, Lcom/google/android/finsky/api/model/DfeList;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    move-object v1, v2

    .line 243
    .local v1, "data":Lcom/google/android/finsky/api/model/Document;
    :goto_4
    const/4 v2, 0x0

    invoke-virtual {v9, v6, v14, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 245
    .local v0, "childCard":Lcom/google/android/play/layout/PlayCardViewBase;
    if-eqz v1, :cond_5

    .line 246
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mItemListRequest:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->getInitialUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 252
    :goto_5
    invoke-virtual {v14, v0}, Lcom/google/android/finsky/layout/BucketRow;->addView(Landroid/view/View;)V

    .line 253
    add-int/lit8 v11, v11, 0x1

    .line 240
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 231
    .end local v0    # "childCard":Lcom/google/android/play/layout/PlayCardViewBase;
    .end local v1    # "data":Lcom/google/android/finsky/api/model/Document;
    .end local v6    # "cardLayoutId":I
    .end local v7    # "colNum":I
    .end local v8    # "firstEntryInRowData":Lcom/google/android/finsky/api/model/Document;
    :cond_2
    const/4 v8, 0x0

    goto :goto_1

    .line 237
    .restart local v8    # "firstEntryInRowData":Lcom/google/android/finsky/api/model/Document;
    :cond_3
    const v6, 0x7f0401d9

    goto :goto_2

    .line 241
    .restart local v6    # "cardLayoutId":I
    .restart local v7    # "colNum":I
    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    .line 249
    .restart local v0    # "childCard":Lcom/google/android/play/layout/PlayCardViewBase;
    .restart local v1    # "data":Lcom/google/android/finsky/api/model/Document;
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->clearCardState()V

    goto :goto_5

    .line 255
    .end local v0    # "childCard":Lcom/google/android/play/layout/PlayCardViewBase;
    .end local v1    # "data":Lcom/google/android/finsky/api/model/Document;
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mListingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 227
    add-int/lit8 v13, v13, 0x1

    goto :goto_0
.end method

.method protected populateHeader()V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 337
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mMoreUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mHeader:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mSubheader:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsPackSection;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c02ea

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/finsky/layout/DetailsPackSection$2;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/layout/DetailsPackSection$2;-><init>(Lcom/google/android/finsky/layout/DetailsPackSection;)V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 351
    :goto_0
    return-void

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mHeader:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mSubheader:Ljava/lang/String;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setNoDataHeader(Ljava/lang/String;)V
    .locals 0
    .param p1, "noDataHeader"    # Ljava/lang/String;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsPackSection;->mNoDataHeader:Ljava/lang/String;

    .line 156
    return-void
.end method
