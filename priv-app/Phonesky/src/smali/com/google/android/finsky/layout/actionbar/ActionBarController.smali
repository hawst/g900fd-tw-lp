.class public interface abstract Lcom/google/android/finsky/layout/actionbar/ActionBarController;
.super Ljava/lang/Object;
.source "ActionBarController.java"


# virtual methods
.method public abstract disableActionBarOverlay()V
.end method

.method public abstract enableActionBarOverlay()V
.end method

.method public abstract enterActionBarSearchMode()V
.end method

.method public abstract enterActionBarSectionExpandedMode(Ljava/lang/CharSequence;Lcom/google/android/finsky/activities/TextSectionTranslatable;)V
.end method

.method public abstract exitActionBarSearchMode()V
.end method

.method public abstract exitActionBarSectionExpandedMode()V
.end method

.method public abstract setActionBarAlpha(IZ)V
.end method
