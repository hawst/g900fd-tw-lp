.class public Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
.super Ljava/lang/Object;
.source "CardRecyclerViewAdapter.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ItemEntry"
.end annotation


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mIsLooseItemRow:Z

.field private mTrueEndIndex:I

.field private final mTrueStartIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1914
    new-instance v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry$1;

    invoke-direct {v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IIZ)V
    .locals 1
    .param p1, "startIndex"    # I
    .param p2, "endIndex"    # I
    .param p3, "isLooseItemRow"    # Z

    .prologue
    .line 1892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I

    .line 1893
    iput p1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I

    .line 1894
    iput p2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I

    .line 1895
    iput-boolean p3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mIsLooseItemRow:Z

    .line 1896
    return-void
.end method

.method synthetic constructor <init>(IIZLcom/google/android/finsky/adapters/CardRecyclerViewAdapter$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$1;

    .prologue
    .line 1877
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;-><init>(IIZ)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    .prologue
    .line 1877
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mIsLooseItemRow:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    .prologue
    .line 1877
    iget v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;
    .param p1, "x1"    # I

    .prologue
    .line 1877
    iput p1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;

    .prologue
    .line 1877
    iget v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I

    return v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1904
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1909
    iget v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueStartIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1910
    iget v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mTrueEndIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1911
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ItemEntry;->mIsLooseItemRow:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1912
    return-void

    .line 1911
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
