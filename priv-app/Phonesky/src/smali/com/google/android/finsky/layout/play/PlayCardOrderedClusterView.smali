.class public Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;
.super Lcom/google/android/finsky/layout/IdentifiableLinearLayout;
.source "PlayCardOrderedClusterView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mCardContentHorizontalPadding:I

.field private mColumnCount:I

.field protected mContent:Landroid/widget/LinearLayout;

.field protected mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mRowCount:I

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    const/16 v0, 0x190

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 49
    return-void
.end method

.method private bindEntries(Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
    .locals 12
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "children"    # [Lcom/google/android/finsky/api/model/Document;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .prologue
    .line 138
    array-length v9, p2

    .line 139
    .local v9, "numChildren":I
    const/4 v10, 0x0

    .local v10, "row":I
    :goto_0
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mRowCount:I

    if-ge v10, v2, :cond_3

    .line 140
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 141
    .local v11, "rowOfDocuments":Landroid/view/ViewGroup;
    invoke-direct {p0, v9}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->getNumDisplayedRows(I)I

    move-result v2

    if-lt v10, v2, :cond_1

    .line 142
    const/16 v2, 0x8

    invoke-virtual {v11, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 139
    :cond_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 144
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 145
    invoke-direct {p0, v9, v10}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->getNumDisplayedColumns(II)I

    move-result v7

    .line 146
    .local v7, "displayedColumns":I
    const/4 v6, 0x0

    .local v6, "column":I
    :goto_1
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mColumnCount:I

    if-ge v6, v2, :cond_0

    .line 147
    invoke-virtual {v11, v6}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 149
    .local v0, "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    if-ge v6, v7, :cond_2

    .line 150
    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mColumnCount:I

    mul-int/2addr v2, v10

    add-int v8, v2, v6

    .line 151
    .local v8, "itemIndex":I
    aget-object v1, p2, v8

    .line 152
    .local v1, "cardDocument":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object v5, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 154
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/play/layout/PlayCardViewBase;->setVisibility(I)V

    .line 146
    .end local v1    # "cardDocument":Lcom/google/android/finsky/api/model/Document;
    .end local v8    # "itemIndex":I
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 156
    :cond_2
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/google/android/play/layout/PlayCardViewBase;->setVisibility(I)V

    goto :goto_2

    .line 161
    .end local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    .end local v6    # "column":I
    .end local v7    # "displayedColumns":I
    .end local v11    # "rowOfDocuments":Landroid/view/ViewGroup;
    :cond_3
    return-void
.end method

.method private bindHeader(IILjava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 8
    .param p1, "estimatedResults"    # I
    .param p2, "backendId"    # I
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "headerClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 165
    const/4 v4, 0x0

    .line 166
    .local v4, "more":Ljava/lang/String;
    if-lez p1, :cond_1

    .line 167
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c02e9

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 168
    .local v6, "template":Ljava/lang/String;
    new-array v0, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v6, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 173
    .end local v6    # "template":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    const/4 v3, 0x0

    move v1, p2

    move-object v2, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mCardContentHorizontalPadding:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setExtraHorizontalPadding(I)V

    .line 175
    return-void

    .line 169
    :cond_1
    if-eqz p4, :cond_0

    .line 171
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c02ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private getNumDisplayedColumns(II)I
    .locals 2
    .param p1, "numChildren"    # I
    .param p2, "row"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->getNumDisplayedRows(I)I

    move-result v0

    if-lt p2, v0, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mColumnCount:I

    mul-int/2addr v0, p2

    sub-int v0, p1, v0

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mColumnCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private getNumDisplayedRows(I)I
    .locals 4
    .param p1, "numChildren"    # I

    .prologue
    .line 83
    int-to-double v0, p1

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mColumnCount:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mRowCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "children"    # [Lcom/google/android/finsky/api/model/Document;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "headerClickListener"    # Landroid/view/View$OnClickListener;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 129
    iput-object p6, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 131
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->bindEntries(Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 132
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults:J

    long-to-int v0, v0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2, p5}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->bindHeader(IILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 134
    return-void
.end method

.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 191
    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 192
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public inflateGrid(III)V
    .locals 8
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I
    .param p3, "cardContentHorizontalPadding"    # I

    .prologue
    const/4 v7, 0x0

    .line 60
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mRowCount:I

    .line 61
    iput p2, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mColumnCount:I

    .line 62
    iput p3, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mCardContentHorizontalPadding:I

    .line 64
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 65
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v4, 0x0

    .local v4, "row":I
    :goto_0
    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mRowCount:I

    if-ge v4, v5, :cond_1

    .line 66
    const v5, 0x7f040044

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/BucketRow;

    .line 68
    .local v3, "listingCardsRow":Lcom/google/android/finsky/layout/BucketRow;
    invoke-virtual {v3, p3}, Lcom/google/android/finsky/layout/BucketRow;->setContentHorizontalPadding(I)V

    .line 69
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mColumnCount:I

    if-ge v0, v5, :cond_0

    .line 70
    const v5, 0x7f040124

    invoke-virtual {v1, v5, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 72
    .local v2, "listingCard":Landroid/view/View;
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 73
    invoke-virtual {v3, v2}, Lcom/google/android/finsky/layout/BucketRow;->addView(Landroid/view/View;)V

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 75
    .end local v2    # "listingCard":Landroid/view/View;
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 65
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 77
    .end local v0    # "i":I
    .end local v3    # "listingCardsRow":Lcom/google/android/finsky/layout/BucketRow;
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;->onFinishInflate()V

    .line 55
    const v0, 0x7f0a02b7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mContent:Landroid/widget/LinearLayout;

    .line 56
    const v0, 0x7f0a013f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    .line 57
    return-void
.end method
