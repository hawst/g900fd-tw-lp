.class public Lcom/google/android/finsky/activities/InlineAppDetailsDialog;
.super Lcom/google/android/finsky/activities/AuthenticatedActivity;
.source "InlineAppDetailsDialog.java"

# interfaces
.implements Lcom/google/android/finsky/fragments/PageFragmentHost;


# instance fields
.field private mDialog:Landroid/view/View;

.field private mFragment:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

.field private mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/FakeNavigationManager;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    return-void
.end method


# virtual methods
.method public getActionBarController()Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v0

    return-object v0
.end method

.method public getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p1, "dfeAccount"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationManager()Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    return-object v0
.end method

.method public getPeopleClient()Lcom/google/android/gms/people/PeopleClient;
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    return-object v0
.end method

.method public goBack()V
    .locals 0

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->finish()V

    .line 127
    return-void
.end method

.method protected handleAuthenticationError(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mFragment:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->showError(Lcom/android/volley/VolleyError;)V

    .line 87
    return-void
.end method

.method public hideDialog()V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mDialog:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mDialog:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 153
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, -0x1

    .line 98
    const/16 v0, 0x21

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 99
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->setResult(I)V

    .line 101
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->finish()V

    .line 102
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v8, 0x7f0a00c4

    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 48
    iput-boolean v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mAllowCallingOnReadyOnResume:Z

    .line 49
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-static {p0}, Lcom/google/android/finsky/utils/SignatureUtils;->isCalledByFirstPartyPackage(Landroid/app/Activity;)Z

    move-result v5

    if-nez v5, :cond_0

    sget-object v5, Lcom/google/android/finsky/config/G;->enableThirdPartyInlineAppInstalls:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 53
    .local v0, "allowedToProceed":Z
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 54
    const-string v5, "Called from untrusted package."

    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v5, v7}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->finish()V

    .line 58
    :cond_1
    const v5, 0x7f0400b6

    const/4 v7, 0x0

    invoke-static {p0, v5, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mDialog:Landroid/view/View;

    .line 59
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mDialog:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->setContentView(Landroid/view/View;)V

    .line 60
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/activities/FakeNavigationManager;->setFragmentManager(Landroid/support/v4/app/FragmentManager;)V

    .line 62
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    iput-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mFragment:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    .line 64
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mFragment:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    if-eqz v5, :cond_3

    .line 82
    :goto_1
    return-void

    .end local v0    # "allowedToProceed":Z
    :cond_2
    move v0, v6

    .line 51
    goto :goto_0

    .line 69
    .restart local v0    # "allowedToProceed":Z
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 70
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "docid"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "docId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 72
    const-string v5, "Missing docid."

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->finish()V

    goto :goto_1

    .line 76
    :cond_4
    const-string v5, "referrer"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 78
    .local v4, "referrer":Ljava/lang/String;
    invoke-static {v1, v4}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mFragment:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    .line 79
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    .line 80
    .local v2, "ft":Landroid/support/v4/app/FragmentTransaction;
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mFragment:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    invoke-virtual {v2, v8, v5}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 81
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_1
.end method

.method protected onReady(Z)V
    .locals 1
    .param p1, "shouldHandleIntent"    # Z

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mFragment:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->setHasBeenAuthenticated()V

    .line 92
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsDialog;->mFragment:Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->refresh()V

    .line 93
    return-void
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "goBack"    # Z

    .prologue
    .line 142
    return-void
.end method

.method public updateBreadcrumb(Ljava/lang/String;)V
    .locals 0
    .param p1, "breadcrumb"    # Ljava/lang/String;

    .prologue
    .line 117
    return-void
.end method

.method public updateCurrentBackendId(IZ)V
    .locals 0
    .param p1, "backend"    # I
    .param p2, "ignoreActionBarBackground"    # Z

    .prologue
    .line 122
    return-void
.end method
