.class public Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;
.super Landroid/widget/LinearLayout;
.source "SetupWizardRestoreAppsDialogView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;
    }
.end annotation


# instance fields
.field private mBackupDocumentInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mListView:Landroid/widget/ListView;

.field private mSelectedBackupDocs:[Z

.field private mSelectedBackupDocsCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method private configureListView(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 98
    const v1, 0x7f0a0391

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mListView:Landroid/widget/ListView;

    .line 99
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mListView:Landroid/widget/ListView;

    new-instance v4, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;

    iget-object v5, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mBackupDocumentInfos:Ljava/util/ArrayList;

    invoke-direct {v4, p0, v5}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;-><init>(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    const/4 v0, 0x0

    .local v0, "pos":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 101
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mListView:Landroid/widget/ListView;

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {v1, v4, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 100
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mListView:Landroid/widget/ListView;

    iget v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocsCount:I

    iget-object v5, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    array-length v5, v5

    if-ne v1, v5, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v4, v3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 107
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 108
    return-void

    :cond_2
    move v1, v3

    .line 105
    goto :goto_1
.end method

.method private getSelectedBackupDocsCount()I
    .locals 6

    .prologue
    .line 111
    const/4 v1, 0x0

    .line 112
    .local v1, "count":I
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    .local v0, "arr$":[Z
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-boolean v5, v0, v2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 113
    .local v4, "selected":Ljava/lang/Boolean;
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 114
    add-int/lit8 v1, v1, 0x1

    .line 112
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 117
    .end local v4    # "selected":Ljava/lang/Boolean;
    :cond_1
    return v1
.end method


# virtual methods
.method public configureView(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 53
    .local v0, "context":Landroid/content/Context;
    const-string v6, "SetupWizardAppListDialog.backupDocs"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 55
    .local v4, "parcelableBackupDocumentInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/utils/ParcelableProto<Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;>;>;"
    const-string v6, "SetupWizardAppListDialog.selectedBackupDocs"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v5

    .line 57
    .local v5, "selectedBackupDocs":[Z
    array-length v6, v5

    invoke-static {v5, v6}, Ljava/util/Arrays;->copyOf([ZI)[Z

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    .line 60
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->getSelectedBackupDocsCount()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocsCount:I

    .line 61
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v6, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mBackupDocumentInfos:Ljava/util/ArrayList;

    .line 63
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/utils/ParcelableProto;

    .line 64
    .local v1, "doc":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;>;"
    iget-object v6, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mBackupDocumentInfos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getPayload()Lcom/google/protobuf/nano/MessageNano;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    .end local v1    # "doc":Lcom/google/android/finsky/utils/ParcelableProto;, "Lcom/google/android/finsky/utils/ParcelableProto<Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;>;"
    :cond_0
    const v6, 0x7f0a0391

    invoke-virtual {p0, v6}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 68
    .local v3, "listView":Landroid/widget/ListView;
    invoke-direct {p0, v3}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->configureListView(Landroid/view/View;)V

    .line 69
    return-void
.end method

.method public getResult()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 92
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 93
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "SetupWizardAppListDialog.selectedBackupDocs"

    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 94
    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 164
    move-object v0, p2

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 165
    .local v0, "checkedTextView":Landroid/widget/CheckedTextView;
    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v1

    .line 168
    .local v1, "isChecked":Z
    if-ge p3, v5, :cond_2

    .line 169
    const/4 v2, 0x0

    .local v2, "pos":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    array-length v4, v4

    if-ge v2, v4, :cond_0

    .line 170
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mListView:Landroid/widget/ListView;

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v4, v5, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 172
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    aput-boolean v1, v4, v2

    .line 169
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 174
    :cond_0
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    array-length v3, v3

    :cond_1
    iput v3, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocsCount:I

    .line 182
    .end local v2    # "pos":I
    :goto_1
    return-void

    .line 179
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    add-int/lit8 v6, p3, -0x1

    aput-boolean v1, v4, v6

    .line 180
    iget v6, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocsCount:I

    if-eqz v1, :cond_3

    move v4, v5

    :goto_2
    add-int/2addr v4, v6

    iput v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocsCount:I

    .line 181
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mListView:Landroid/widget/ListView;

    iget v6, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocsCount:I

    iget-object v7, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    array-length v7, v7

    if-ne v6, v7, :cond_4

    :goto_3
    invoke-virtual {v4, v3, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_1

    .line 180
    :cond_3
    const/4 v4, -0x1

    goto :goto_2

    :cond_4
    move v5, v3

    .line 181
    goto :goto_3
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 81
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 82
    check-cast v0, Landroid/os/Bundle;

    .line 83
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "SetupWizardAppListDialog.selectedBackupDocs"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    .line 84
    const-string v1, "SetupWizardRestoreAppsDialogView.parent_instance_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 88
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 73
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 74
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "SetupWizardRestoreAppsDialogView.parent_instance_state"

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 75
    const-string v1, "SetupWizardAppListDialog.selectedBackupDocs"

    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->mSelectedBackupDocs:[Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 76
    return-object v0
.end method
