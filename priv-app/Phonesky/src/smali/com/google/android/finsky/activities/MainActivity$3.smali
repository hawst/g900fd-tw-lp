.class Lcom/google/android/finsky/activities/MainActivity$3;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/MainActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private initialCleanupDone:Z

.field final synthetic this$0:Lcom/google/android/finsky/activities/MainActivity;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/MainActivity;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/google/android/finsky/activities/MainActivity$3;->this$0:Lcom/google/android/finsky/activities/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 4

    .prologue
    .line 304
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/MainActivity$3;->initialCleanupDone:Z

    if-nez v1, :cond_0

    .line 306
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity$3;->this$0:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/MainActivity;->hideLoadingIndicator()V

    .line 307
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity$3;->this$0:Lcom/google/android/finsky/activities/MainActivity;

    # invokes: Lcom/google/android/finsky/activities/MainActivity;->hideErrorMessage()V
    invoke-static {v1}, Lcom/google/android/finsky/activities/MainActivity;->access$200(Lcom/google/android/finsky/activities/MainActivity;)V

    .line 308
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/MainActivity$3;->initialCleanupDone:Z

    .line 312
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity$3;->this$0:Lcom/google/android/finsky/activities/MainActivity;

    # getter for: Lcom/google/android/finsky/activities/MainActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;
    invoke-static {v1}, Lcom/google/android/finsky/activities/MainActivity;->access$000(Lcom/google/android/finsky/activities/MainActivity;)Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->isActionBarOverlayEnabledForCurrent()Z

    move-result v0

    .line 314
    .local v0, "isActionBarOverlayEnabled":Z
    if-eqz v0, :cond_1

    .line 315
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity$3;->this$0:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/MainActivity;->enableActionBarOverlay()V

    .line 320
    :goto_0
    return-void

    .line 317
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity$3;->this$0:Lcom/google/android/finsky/activities/MainActivity;

    const/16 v2, 0xff

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/MainActivity;->setActionBarAlpha(IZ)V

    .line 318
    iget-object v1, p0, Lcom/google/android/finsky/activities/MainActivity$3;->this$0:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/MainActivity;->disableActionBarOverlay()V

    goto :goto_0
.end method
