.class public Lcom/google/android/finsky/utils/IntentUtils;
.super Ljava/lang/Object;
.source "IntentUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;
    }
.end annotation


# static fields
.field private static final BOOKS_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

.field private static final GPLUS_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

.field private static final MAGAZINES_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

.field private static final MUSIC_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

.field private static final MY_APPS:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

.field private static final VIDEOS_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/google/android/finsky/utils/IntentUtils$1;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/IntentUtils$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/IntentUtils;->BOOKS_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    .line 216
    new-instance v0, Lcom/google/android/finsky/utils/IntentUtils$2;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/IntentUtils$2;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/IntentUtils;->MY_APPS:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    .line 276
    new-instance v0, Lcom/google/android/finsky/utils/IntentUtils$3;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/IntentUtils$3;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/IntentUtils;->VIDEOS_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    .line 317
    new-instance v0, Lcom/google/android/finsky/utils/IntentUtils$4;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/IntentUtils$4;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/IntentUtils;->MAGAZINES_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    .line 373
    new-instance v0, Lcom/google/android/finsky/utils/IntentUtils$5;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/IntentUtils$5;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/IntentUtils;->MUSIC_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    .line 399
    new-instance v0, Lcom/google/android/finsky/utils/IntentUtils$6;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/IntentUtils$6;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/IntentUtils;->GPLUS_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    return-void
.end method

.method static synthetic access$100(Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 36
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/utils/IntentUtils;->createLaunchIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static buildConsumptionAppLaunchIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    .line 563
    invoke-static {p1}, Lcom/google/android/finsky/utils/IntentUtils;->getConsumptionApp(I)Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;->buildViewCollectionIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static buildConsumptionAppManageItemIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    .line 591
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/IntentUtils;->getConsumptionApp(I)Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;->buildManageItemIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static buildConsumptionAppUrlIntent(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "accountName"    # Ljava/lang/String;

    .prologue
    .line 573
    invoke-static {p1}, Lcom/google/android/finsky/utils/IntentUtils;->getConsumptionApp(I)Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    move-result-object v0

    invoke-virtual {v0, p0, p2, p3}, Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;->buildUrlIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static buildConsumptionAppViewItemIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    .line 582
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/IntentUtils;->getConsumptionApp(I)Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;->buildViewItemIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static buildShareIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;)Landroid/content/Intent;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 719
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 720
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "text/plain"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 721
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 722
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getShareUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 723
    const-string v1, "android.intent.extra.SUBJECT"

    const v2, 0x7f0c0261

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 725
    return-object v0
.end method

.method public static canResolveIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z
    .locals 3
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 502
    invoke-virtual {p0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 503
    .local v0, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static canResolveUrl(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 795
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 796
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 797
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 798
    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/IntentUtils;->canResolveIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method public static createAccountSpecificIntent(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "accountKey"    # Ljava/lang/String;
    .param p3, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 705
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 706
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 707
    return-object v0
.end method

.method public static createAggregatedHomeIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 845
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public static createBrowseIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "browseUrl"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "backendId"    # I
    .param p4, "clearBackStack"    # Z

    .prologue
    .line 857
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 858
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 859
    const-string v1, "com.google.android.finsky.VIEW_BROWSE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 860
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 861
    const-string v1, "backend_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 862
    const-string v1, "clear_back_stack"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 863
    return-object v0
.end method

.method public static createContinueUrlIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 809
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 810
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 811
    .local v1, "uri":Landroid/net/Uri$Builder;
    const-string v2, "market"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 812
    const-string v2, "details"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 813
    const-string v2, "id"

    invoke-virtual {v1, v2, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 814
    const-string v2, "url"

    invoke-static {p1}, Lcom/google/android/finsky/utils/Utils;->urlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 815
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 816
    const-string v2, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 817
    const-string v2, "com.android.vending"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 818
    return-object v0
.end method

.method public static createCorpusIntent(Landroid/content/Context;ILcom/google/android/finsky/api/model/DfeToc;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I
    .param p2, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    .line 834
    invoke-virtual {p2, p1}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    .line 836
    .local v0, "corpusMetadata":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/finsky/activities/MainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 837
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 838
    const-string v2, "com.google.android.finsky.CORPUS_HOME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 839
    const-string v2, "title"

    iget-object v3, v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 840
    const-string v2, "backend_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 841
    return-object v1
.end method

.method public static createForwardToMainActivityIntent(Landroid/content/Context;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "originalIntent"    # Landroid/content/Intent;

    .prologue
    .line 736
    const-string v2, "authAccount"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 737
    .local v0, "accountName":Ljava/lang/String;
    const-class v2, Lcom/google/android/finsky/activities/MainActivity;

    const-string v3, "authAccount"

    invoke-static {p0, v2, v3, v0}, Lcom/google/android/finsky/utils/IntentUtils;->createAccountSpecificIntent(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 739
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 740
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 741
    return-object v1
.end method

.method public static createIntentForReceiver(Landroid/content/pm/PackageManager;Ljava/lang/String;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 6
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    .line 753
    invoke-virtual {p0, p2, v5}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 754
    .local v1, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v3, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v3, :cond_0

    iget-object v3, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 756
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, p2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 757
    .local v2, "result":Landroid/content/Intent;
    iget-object v3, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 763
    .end local v1    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v2    # "result":Landroid/content/Intent;
    :goto_0
    return-object v2

    .line 762
    :cond_1
    const-string v3, "Could not find receiver for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 763
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static createLaunchIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageManager;)Landroid/content/Intent;
    .locals 4
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "continueUrl"    # Ljava/lang/String;
    .param p2, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v3, 0x0

    .line 768
    const/4 v0, 0x0

    .line 771
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 772
    invoke-static {p0, p1}, Lcom/google/android/finsky/utils/IntentUtils;->createContinueUrlIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/google/android/finsky/utils/IntentUtils;->makeResolvableIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 775
    :cond_0
    if-nez v0, :cond_1

    .line 777
    invoke-virtual {p2, p0}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 778
    if-nez v0, :cond_1

    .line 779
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p0, v3, v3, v2}, Lcom/google/android/finsky/utils/NotificationManager;->createDefaultClickIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 784
    :cond_1
    return-object v0
.end method

.method public static createMovieTrailerIntentForUrl(Landroid/content/pm/PackageManager;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    .line 670
    invoke-static {p0}, Lcom/google/android/finsky/utils/IntentUtils;->isVideosAppTrailerPlaybackSupported(Landroid/content/pm/PackageManager;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 671
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.videos.intent.action.trailers.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 672
    .local v0, "trailerIntent":Landroid/content/Intent;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 673
    const-string v1, "com.google.android.videos"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 674
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 675
    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/IntentUtils;->makeResolvableIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    .line 677
    .end local v0    # "trailerIntent":Landroid/content/Intent;
    :goto_0
    return-object v1

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/utils/IntentUtils;->createYouTubeIntentForUrl(Landroid/content/pm/PackageManager;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0
.end method

.method public static createSendIntentForUrl(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 685
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SENDTO"

    invoke-direct {v0, v1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 690
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 691
    return-object v0
.end method

.method public static createViewDocumentIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 822
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/finsky/utils/IntentUtils;->createViewDocumentUrlIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 823
    .local v0, "intent":Landroid/content/Intent;
    return-object v0
.end method

.method public static createViewDocumentUrlIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "documentUrl"    # Ljava/lang/String;

    .prologue
    .line 827
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 828
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.finsky.DETAILS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 829
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 830
    return-object v0
.end method

.method public static createViewIntentForUrl(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 599
    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 600
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 603
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 608
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 609
    return-object v0
.end method

.method public static createYouTubeIntentForUrl(Landroid/content/pm/PackageManager;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 637
    if-eqz p1, :cond_0

    .line 638
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 640
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 643
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.google.android.youtube"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 644
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 645
    const-string v1, "force_fullscreen"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 646
    const-string v1, "finish_on_ended"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 651
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 653
    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/IntentUtils;->makeResolvableIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static getBackendId(Ljava/lang/String;)I
    .locals 1
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    .line 109
    const-string v0, "com.google.android.apps.books"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x1

    .line 122
    :goto_0
    return v0

    .line 111
    :cond_0
    const-string v0, "com.google.android.videos"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    const/4 v0, 0x4

    goto :goto_0

    .line 113
    :cond_1
    const-string v0, "com.google.android.music"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    const/4 v0, 0x2

    goto :goto_0

    .line 115
    :cond_2
    const-string v0, "com.google.android.apps.magazines"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 116
    const/4 v0, 0x6

    goto :goto_0

    .line 117
    :cond_3
    const-string v0, "com.android.vending"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 118
    const/4 v0, 0x3

    goto :goto_0

    .line 119
    :cond_4
    const-string v0, "com.google.android.apps.plus"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 120
    const/16 v0, 0x9

    goto :goto_0

    .line 122
    :cond_5
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private static getConsumptionApp(I)Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;
    .locals 3
    .param p0, "backendId"    # I

    .prologue
    .line 538
    packed-switch p0, :pswitch_data_0

    .line 552
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown backend "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 540
    :pswitch_1
    sget-object v0, Lcom/google/android/finsky/utils/IntentUtils;->BOOKS_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    .line 550
    :goto_0
    return-object v0

    .line 542
    :pswitch_2
    sget-object v0, Lcom/google/android/finsky/utils/IntentUtils;->MY_APPS:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    goto :goto_0

    .line 544
    :pswitch_3
    sget-object v0, Lcom/google/android/finsky/utils/IntentUtils;->MUSIC_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    goto :goto_0

    .line 546
    :pswitch_4
    sget-object v0, Lcom/google/android/finsky/utils/IntentUtils;->VIDEOS_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    goto :goto_0

    .line 548
    :pswitch_5
    sget-object v0, Lcom/google/android/finsky/utils/IntentUtils;->MAGAZINES_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    goto :goto_0

    .line 550
    :pswitch_6
    sget-object v0, Lcom/google/android/finsky/utils/IntentUtils;->GPLUS_APP:Lcom/google/android/finsky/utils/IntentUtils$ConsumptionApp;

    goto :goto_0

    .line 538
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public static getMinimumRequiredVideosAppVersion()I
    .locals 2

    .prologue
    .line 870
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    sget-object v0, Lcom/google/android/finsky/config/G;->minimumVideosVersionCodePreHoneycomb:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    :cond_0
    sget-object v0, Lcom/google/android/finsky/config/G;->minimumVideosVersionCodeHoneycombPlus:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    goto :goto_0
.end method

.method public static getPackageName(I)Ljava/lang/String;
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 86
    packed-switch p0, :pswitch_data_0

    .line 100
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 88
    :pswitch_1
    const-string v0, "com.google.android.music"

    goto :goto_0

    .line 90
    :pswitch_2
    const-string v0, "com.google.android.videos"

    goto :goto_0

    .line 92
    :pswitch_3
    const-string v0, "com.google.android.apps.books"

    goto :goto_0

    .line 94
    :pswitch_4
    const-string v0, "com.google.android.apps.magazines"

    goto :goto_0

    .line 96
    :pswitch_5
    const-string v0, "com.android.vending"

    goto :goto_0

    .line 98
    :pswitch_6
    const-string v0, "com.google.android.apps.plus"

    goto :goto_0

    .line 86
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method private static isAppInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 4
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 492
    invoke-virtual {p0, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 493
    .local v0, "intent":Landroid/content/Intent;
    if-nez v0, :cond_1

    .line 498
    :cond_0
    :goto_0
    return v2

    .line 496
    :cond_1
    const/high16 v3, 0x10000

    invoke-virtual {p0, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 498
    .local v1, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private static isBooksAppInstalled(Landroid/content/pm/PackageManager;)Z
    .locals 1
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 433
    const-string v0, "com.google.android.apps.books"

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/IntentUtils;->isAppInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isConsumptionAppDisabled(Lcom/google/android/finsky/appstate/PackageStateRepository;I)Z
    .locals 3
    .param p0, "packageStateRepository"    # Lcom/google/android/finsky/appstate/PackageStateRepository;
    .param p1, "backendId"    # I

    .prologue
    .line 532
    invoke-static {p1}, Lcom/google/android/finsky/utils/IntentUtils;->getPackageName(I)Ljava/lang/String;

    move-result-object v0

    .line 533
    .local v0, "packageName":Ljava/lang/String;
    invoke-interface {p0, v0}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v1

    .line 534
    .local v1, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-eqz v1, :cond_0

    iget-boolean v2, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabled:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isConsumptionAppInstalled(Landroid/content/pm/PackageManager;I)Z
    .locals 1
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "backendId"    # I

    .prologue
    .line 511
    packed-switch p1, :pswitch_data_0

    .line 523
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 513
    :pswitch_1
    invoke-static {p0}, Lcom/google/android/finsky/utils/IntentUtils;->isBooksAppInstalled(Landroid/content/pm/PackageManager;)Z

    move-result v0

    goto :goto_0

    .line 515
    :pswitch_2
    invoke-static {p0}, Lcom/google/android/finsky/utils/IntentUtils;->isVideosAppInstalled(Landroid/content/pm/PackageManager;)Z

    move-result v0

    goto :goto_0

    .line 517
    :pswitch_3
    invoke-static {p0}, Lcom/google/android/finsky/utils/IntentUtils;->isMusicAppInstalled(Landroid/content/pm/PackageManager;)Z

    move-result v0

    goto :goto_0

    .line 519
    :pswitch_4
    invoke-static {p0}, Lcom/google/android/finsky/utils/IntentUtils;->isMagazinesAppInstalled(Landroid/content/pm/PackageManager;)Z

    move-result v0

    goto :goto_0

    .line 521
    :pswitch_5
    invoke-static {p0}, Lcom/google/android/finsky/utils/IntentUtils;->isGPlusAppInstalled(Landroid/content/pm/PackageManager;)Z

    move-result v0

    goto :goto_0

    .line 511
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private static isGPlusAppInstalled(Landroid/content/pm/PackageManager;)Z
    .locals 1
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 488
    const-string v0, "com.google.android.apps.plus"

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/IntentUtils;->isAppInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isMagazinesAppInstalled(Landroid/content/pm/PackageManager;)Z
    .locals 6
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 455
    const-string v2, "com.google.android.apps.magazines"

    invoke-static {p0, v2}, Lcom/google/android/finsky/utils/IntentUtils;->isAppInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 456
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v2

    const-string v5, "com.google.android.apps.magazines"

    invoke-interface {v2, v5}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v1

    .line 459
    .local v1, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    iget v5, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    sget-object v2, Lcom/google/android/finsky/config/G;->firstNewsstandAppVersion:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v5, v2, :cond_0

    move v0, v3

    .line 461
    .local v0, "needsToUpgradeToNewsstand":Z
    :goto_0
    if-nez v0, :cond_1

    move v2, v3

    .line 463
    .end local v0    # "needsToUpgradeToNewsstand":Z
    .end local v1    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :goto_1
    return v2

    .restart local v1    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_0
    move v0, v4

    .line 459
    goto :goto_0

    .restart local v0    # "needsToUpgradeToNewsstand":Z
    :cond_1
    move v2, v4

    .line 461
    goto :goto_1

    .end local v0    # "needsToUpgradeToNewsstand":Z
    .end local v1    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_2
    move v2, v4

    .line 463
    goto :goto_1
.end method

.method private static isMusicAppInstalled(Landroid/content/pm/PackageManager;)Z
    .locals 3
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 467
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.music.PLAY"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 468
    .local v0, "musicIntent":Landroid/content/Intent;
    const/high16 v2, 0x10000

    invoke-virtual {p0, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 470
    .local v1, "resolveInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static isMusicAppWithAllAccessFlowInstalled(Landroid/content/pm/PackageManager;)Z
    .locals 6
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 477
    const-string v2, "com.google.android.music"

    invoke-static {p0, v2}, Lcom/google/android/finsky/utils/IntentUtils;->isAppInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 478
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v2

    const-string v5, "com.google.android.music"

    invoke-interface {v2, v5}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v1

    .line 480
    .local v1, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    iget v5, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    sget-object v2, Lcom/google/android/finsky/config/G;->minimumMusicVersionCode:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v5, v2, :cond_0

    move v0, v3

    .line 482
    .local v0, "needsToUpgrade":Z
    :goto_0
    if-nez v0, :cond_1

    move v2, v3

    .line 484
    .end local v0    # "needsToUpgrade":Z
    .end local v1    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :goto_1
    return v2

    .restart local v1    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_0
    move v0, v4

    .line 480
    goto :goto_0

    .restart local v0    # "needsToUpgrade":Z
    :cond_1
    move v2, v4

    .line 482
    goto :goto_1

    .end local v0    # "needsToUpgrade":Z
    .end local v1    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_2
    move v2, v4

    .line 484
    goto :goto_1
.end method

.method private static isVideosAppInstalled(Landroid/content/pm/PackageManager;)Z
    .locals 4
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v1, 0x0

    .line 437
    const-string v2, "com.google.android.videos"

    invoke-static {p0, v2}, Lcom/google/android/finsky/utils/IntentUtils;->isAppInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 438
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v2

    const-string v3, "com.google.android.videos"

    invoke-interface {v2, v3}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v2

    iget v0, v2, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 440
    .local v0, "videosVersion":I
    invoke-static {}, Lcom/google/android/finsky/utils/IntentUtils;->getMinimumRequiredVideosAppVersion()I

    move-result v2

    if-lt v0, v2, :cond_0

    const/4 v1, 0x1

    .line 442
    .end local v0    # "videosVersion":I
    :cond_0
    return v1
.end method

.method private static isVideosAppTrailerPlaybackSupported(Landroid/content/pm/PackageManager;)Z
    .locals 4
    .param p0, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    const/4 v2, 0x0

    .line 446
    const-string v1, "com.google.android.videos"

    invoke-static {p0, v1}, Lcom/google/android/finsky/utils/IntentUtils;->isAppInstalled(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 447
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v1

    const-string v3, "com.google.android.videos"

    invoke-interface {v1, v3}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v1

    iget v0, v1, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 449
    .local v0, "videosVersion":I
    sget-object v1, Lcom/google/android/finsky/config/G;->videoAppTrailerPlaybackMinVersion:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v1, 0x1

    .line 451
    .end local v0    # "videosVersion":I
    :goto_0
    return v1

    .restart local v0    # "videosVersion":I
    :cond_0
    move v1, v2

    .line 449
    goto :goto_0

    .end local v0    # "videosVersion":I
    :cond_1
    move v1, v2

    .line 451
    goto :goto_0
.end method

.method private static makeResolvableIntent(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3
    .param p0, "pm"    # Landroid/content/pm/PackageManager;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 619
    const/high16 v1, 0x10000

    invoke-virtual {p0, p1, v1}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 620
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 621
    .local v0, "fallback":Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 624
    .end local v0    # "fallback":Landroid/content/Intent;
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p1

    goto :goto_0
.end method
