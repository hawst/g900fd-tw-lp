.class Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$1;
.super Ljava/lang/Object;
.source "PaginatedRecyclerViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;)V
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$1;->this$0:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$1;->this$0:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;

    # getter for: Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->mFooterMode:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;
    invoke-static {v0}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->access$000(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;)Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    move-result-object v0

    sget-object v1, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->ERROR:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-ne v0, v1, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$1;->this$0:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->retryLoadingItems()V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$1;->this$0:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;

    sget-object v1, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->LOADING:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    # invokes: Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->setFooterMode(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;->access$100(Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter;Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;)V

    .line 54
    return-void
.end method
