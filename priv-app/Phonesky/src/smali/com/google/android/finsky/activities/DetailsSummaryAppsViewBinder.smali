.class public Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;
.super Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
.source "DetailsSummaryAppsViewBinder.java"

# interfaces
.implements Lcom/google/android/finsky/installer/InstallerListener;
.implements Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$8;
    }
.end annotation


# instance fields
.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private mListenersAdded:Z

.field private final mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

.field private mTrackPackageStatus:Z


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;Lcom/google/android/finsky/receivers/PackageMonitorReceiver;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V
    .locals 0
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "currentAccount"    # Landroid/accounts/Account;
    .param p3, "packageMonitorReceiver"    # Lcom/google/android/finsky/receivers/PackageMonitorReceiver;
    .param p4, "installer"    # Lcom/google/android/finsky/receivers/Installer;
    .param p5, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p6, "libraries"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;-><init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V

    .line 72
    iput-object p3, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    .line 73
    iput-object p4, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    .line 74
    iput-object p5, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 75
    iput-object p6, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;)Lcom/google/android/finsky/receivers/Installer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;Ljava/lang/String;ZLjava/lang/String;ZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z
    .param p5, "x5"    # Z
    .param p6, "x6"    # Z

    .prologue
    .line 49
    invoke-direct/range {p0 .. p6}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refundAndUninstallAsset(Ljava/lang/String;ZLjava/lang/String;ZZZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->confirmRefundApp(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method private attachListeners()V
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mTrackPackageStatus:Z

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver;->detach(Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver;->attach(Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;)V

    .line 92
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mListenersAdded:Z

    if-nez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->addListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mListenersAdded:Z

    .line 97
    :cond_0
    return-void
.end method

.method private confirmRefundApp(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appRefundAccount"    # Ljava/lang/String;
    .param p3, "tryUninstall"    # Z

    .prologue
    .line 417
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v4}, Lcom/google/android/finsky/fragments/PageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 418
    .local v3, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v4, "refund_confirm"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 434
    :goto_0
    return-void

    .line 422
    :cond_0
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 423
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v4, 0x7f0c02a7

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0c01b1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0c01b2

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 426
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 427
    .local v2, "extraArguments":Landroid/os/Bundle;
    const-string v4, "package_name"

    invoke-virtual {v2, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    const-string v4, "account_name"

    invoke-virtual {v2, v4, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    const-string v4, "try_uninstall"

    invoke-virtual {v2, v4, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 430
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    const/4 v5, 0x4

    invoke-virtual {v0, v4, v5, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 432
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 433
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v4, "refund_confirm"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private hideNonDynamicViews()V
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->setupActionButtons(Z)V

    .line 187
    return-void
.end method

.method private listenerRefresh()V
    .locals 1

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refresh()V

    .line 570
    :cond_0
    return-void
.end method

.method private refreshByPackageName(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->syncDynamicSection()V

    .line 490
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->onDataChanged()V

    .line 492
    :cond_0
    return-void
.end method

.method private refundAndUninstallAsset(Ljava/lang/String;ZLjava/lang/String;ZZZ)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "isRefundable"    # Z
    .param p3, "appRefundAccount"    # Ljava/lang/String;
    .param p4, "isSystemPackage"    # Z
    .param p5, "isOwned"    # Z
    .param p6, "hasSubscriptions"    # Z

    .prologue
    const/4 v2, 0x1

    .line 399
    if-nez p2, :cond_0

    move-object v0, p0

    move-object v1, p1

    move v3, p4

    move v4, p5

    move v5, p6

    .line 400
    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->uninstallAsset(Ljava/lang/String;ZZZZ)V

    .line 405
    :goto_0
    return-void

    .line 404
    :cond_0
    invoke-direct {p0, p1, p3, v2}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->confirmRefundApp(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private refundApp(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 440
    const-string v3, "package_name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 441
    .local v1, "packageName":Ljava/lang/String;
    const-string v3, "account_name"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 442
    .local v0, "appRefundAccount":Ljava/lang/String;
    const-string v3, "try_uninstall"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 443
    .local v2, "tryUninstall":Z
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v3}, Lcom/google/android/finsky/fragments/PageFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    new-instance v4, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$7;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$7;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;)V

    invoke-static {v3, v1, v0, v2, v4}, Lcom/google/android/finsky/utils/AppSupport;->silentRefund(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/finsky/utils/AppSupport$RefundListener;)V

    .line 458
    return-void
.end method

.method private uninstallAsset(Ljava/lang/String;ZZZZ)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "showConfirmationDialog"    # Z
    .param p3, "isSystemPackage"    # Z
    .param p4, "isOwned"    # Z
    .param p5, "hasActiveSubscriptions"    # Z

    .prologue
    .line 473
    if-eqz p2, :cond_0

    .line 474
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-static {p1, v0, p3, p4, p5}, Lcom/google/android/finsky/utils/AppSupport;->showUninstallConfirmationDialog(Ljava/lang/String;Landroid/support/v4/app/Fragment;ZZZ)V

    .line 480
    :goto_0
    return-void

    .line 477
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/receivers/Installer;->uninstallAssetSilently(Ljava/lang/String;)V

    .line 478
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refresh()V

    goto :goto_0
.end method

.method private updateContainerLayouts()V
    .locals 3

    .prologue
    .line 387
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->syncButtonSectionVisibility()V

    .line 389
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDynamicSection:Lcom/google/android/finsky/layout/DetailsSummaryDynamic;

    const v2, 0x7f0a0182

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 392
    .local v0, "dynamicStatus":Landroid/widget/TextView;
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 394
    .end local v0    # "dynamicStatus":Landroid/widget/TextView;
    :cond_0
    return-void
.end method


# virtual methods
.method public varargs bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V
    .locals 0
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bindDynamicSection"    # Z
    .param p3, "views"    # [Landroid/view/View;

    .prologue
    .line 101
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V

    .line 102
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->attachListeners()V

    .line 103
    return-void
.end method

.method public init(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/fragments/PageFragment;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "fragment"    # Lcom/google/android/finsky/fragments/PageFragment;
    .param p5, "trackPackageStatus"    # Z
    .param p6, "continueUrl"    # Ljava/lang/String;
    .param p7, "revealTransitionCoverName"    # Ljava/lang/String;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 82
    invoke-super/range {p0 .. p8}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/fragments/PageFragment;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 84
    iput-boolean p5, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mTrackPackageStatus:Z

    .line 85
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->attachListeners()V

    .line 86
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mPackageMonitorReceiver:Lcom/google/android/finsky/receivers/PackageMonitorReceiver;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver;->detach(Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;)V

    .line 108
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mListenersAdded:Z

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->removeListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 110
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mListenersAdded:Z

    .line 112
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->onDestroyView()V

    .line 113
    return-void
.end method

.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3, "statusCode"    # I

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->listenerRefresh()V

    .line 561
    :cond_0
    return-void
.end method

.method public onPackageAdded(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 532
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refreshByPackageName(Ljava/lang/String;)V

    .line 533
    return-void
.end method

.method public onPackageAvailabilityChanged([Ljava/lang/String;Z)V
    .locals 0
    .param p1, "packageNames"    # [Ljava/lang/String;
    .param p2, "available"    # Z

    .prologue
    .line 543
    return-void
.end method

.method public onPackageChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 537
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refreshByPackageName(Ljava/lang/String;)V

    .line 538
    return-void
.end method

.method public onPackageFirstLaunch(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 552
    return-void
.end method

.method public onPackageRemoved(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "replacing"    # Z

    .prologue
    .line 547
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->onDataChanged()V

    .line 548
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 500
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->onPositiveClick(ILandroid/os/Bundle;)V

    .line 501
    packed-switch p1, :pswitch_data_0

    .line 517
    :pswitch_0
    const-string v1, "Unexpected requestCode %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 520
    :cond_0
    :goto_0
    :pswitch_1
    return-void

    .line 504
    :pswitch_2
    const-string v1, "package_name"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 505
    .local v0, "packageName":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    if-eqz v1, :cond_0

    .line 506
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v1, v0}, Lcom/google/android/finsky/receivers/Installer;->uninstallAssetSilently(Ljava/lang/String;)V

    .line 507
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refresh()V

    goto :goto_0

    .line 514
    .end local v0    # "packageName":Ljava/lang/String;
    :pswitch_3
    invoke-direct {p0, p2}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->refundApp(Landroid/os/Bundle;)V

    goto :goto_0

    .line 501
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected setupActionButtons(Z)V
    .locals 26
    .param p1, "isInTransientState"    # Z

    .prologue
    .line 213
    const v3, 0x7f0a0138

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/play/layout/PlayActionButton;

    .line 214
    .local v14, "buyButton":Lcom/google/android/play/layout/PlayActionButton;
    const v3, 0x7f0a013b

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Lcom/google/android/play/layout/PlayActionButton;

    .line 215
    .local v18, "launchButton":Lcom/google/android/play/layout/PlayActionButton;
    const v3, 0x7f0a013d

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Lcom/google/android/play/layout/PlayActionButton;

    .line 216
    .local v22, "uninstallButton":Lcom/google/android/play/layout/PlayActionButton;
    const v3, 0x7f0a013c

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Lcom/google/android/play/layout/PlayActionButton;

    .line 219
    .local v23, "updateButton":Lcom/google/android/play/layout/PlayActionButton;
    const/16 v3, 0x8

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 220
    const/16 v3, 0x8

    invoke-virtual {v14, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 221
    const/16 v3, 0x8

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 222
    const/16 v3, 0x8

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 225
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mHideDynamicFeatures:Z

    if-nez v3, :cond_0

    if-eqz p1, :cond_1

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v3

    iget-object v13, v3, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 231
    .local v13, "appPackageName":Ljava/lang/String;
    new-instance v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-direct {v11, v13, v3, v5}, Lcom/google/android/finsky/activities/AppActionAnalyzer;-><init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V

    .line 236
    .local v11, "actions":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    const/16 v20, 0x0

    .line 237
    .local v20, "numButtons":I
    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isUninstallable:Z

    if-eqz v3, :cond_8

    .line 239
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {v3, v13}, Lcom/google/android/finsky/utils/DocUtils;->hasAutoRenewingSubscriptions(Lcom/google/android/finsky/library/Libraries;Ljava/lang/String;)Z

    move-result v12

    .line 241
    .local v12, "appHasSubscriptions":Z
    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 242
    add-int/lit8 v20, v20, 0x1

    .line 243
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isRefundable:Z

    if-eqz v3, :cond_7

    const v3, 0x7f0c02a3

    :goto_1
    new-instance v6, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$2;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v13, v11, v12}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$2;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;Ljava/lang/String;Lcom/google/android/finsky/activities/AppActionAnalyzer;Z)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v3, v6}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 297
    .end local v12    # "appHasSubscriptions":Z
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {v13, v3, v5, v6}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->getInstallAccount(Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)Landroid/accounts/Account;

    move-result-object v4

    .line 299
    .local v4, "installAccount":Landroid/accounts/Account;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v16

    .line 306
    .local v16, "installAccountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v11, v3}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->hasUpdateAvailable(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v11, v3}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->hasConversionUpdateAvailable(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    move-object/from16 v0, v16

    invoke-static {v3, v5, v0}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabled:Z

    if-nez v3, :cond_4

    .line 309
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 310
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v24

    const v25, 0x7f0c01f0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContinueUrl:Ljava/lang/String;

    const/16 v9, 0xd9

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v3

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 314
    add-int/lit8 v20, v20, 0x1

    .line 319
    :cond_4
    const/4 v3, 0x2

    move/from16 v0, v20

    if-ge v0, v3, :cond_5

    .line 320
    const/4 v15, 0x0

    .line 323
    .local v15, "clickHandler":Landroid/view/View$OnClickListener;
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 324
    const/16 v19, -0x1

    .line 326
    .local v19, "launchButtonTextId":I
    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isLaunchable:Z

    if-eqz v3, :cond_b

    .line 329
    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isContinueLaunch:Z

    if-eqz v3, :cond_a

    .line 331
    const v19, 0x7f0c020d

    .line 333
    new-instance v15, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$5;

    .end local v15    # "clickHandler":Landroid/view/View$OnClickListener;
    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$5;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;)V

    .line 366
    .restart local v15    # "clickHandler":Landroid/view/View$OnClickListener;
    :goto_3
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/layout/PlayActionButton;->getVisibility()I

    move-result v3

    if-nez v3, :cond_5

    .line 367
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1, v15}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 371
    .end local v15    # "clickHandler":Landroid/view/View$OnClickListener;
    .end local v19    # "launchButtonTextId":I
    :cond_5
    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    if-nez v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {v3, v5, v6}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 372
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-static {v3, v5, v6}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v21

    .line 375
    .local v21, "owner":Landroid/accounts/Account;
    const/4 v3, 0x0

    invoke-virtual {v14, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 376
    if-eqz v21, :cond_d

    const/16 v17, 0x1

    .line 377
    .local v17, "isOwned":Z
    :goto_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->getBuyButtonLoggingElementType(ZI)I

    move-result v9

    .line 378
    .local v9, "logElementType":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v24

    const/4 v3, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v3}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->getBuyButtonString(ZI)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContinueUrl:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v3

    move/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v14, v0, v1, v3}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 383
    .end local v9    # "logElementType":I
    .end local v17    # "isOwned":Z
    .end local v21    # "owner":Landroid/accounts/Account;
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->updateContainerLayouts()V

    goto/16 :goto_0

    .line 243
    .end local v4    # "installAccount":Landroid/accounts/Account;
    .end local v16    # "installAccountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .restart local v12    # "appHasSubscriptions":Z
    :cond_7
    const v3, 0x7f0c01e7

    goto/16 :goto_1

    .line 255
    .end local v12    # "appHasSubscriptions":Z
    :cond_8
    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isUninstallable:Z

    if-nez v3, :cond_9

    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isActiveDeviceAdmin:Z

    if-eqz v3, :cond_9

    .line 258
    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 259
    add-int/lit8 v20, v20, 0x1

    .line 260
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    const v5, 0x7f0c01e8

    new-instance v6, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$3;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$3;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5, v6}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 279
    :cond_9
    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isRefundable:Z

    if-eqz v3, :cond_2

    .line 281
    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 282
    add-int/lit8 v20, v20, 0x1

    .line 283
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    const v5, 0x7f0c02a3

    new-instance v6, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$4;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v13, v11}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$4;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;Ljava/lang/String;Lcom/google/android/finsky/activities/AppActionAnalyzer;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5, v6}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 345
    .restart local v4    # "installAccount":Landroid/accounts/Account;
    .restart local v15    # "clickHandler":Landroid/view/View$OnClickListener;
    .restart local v16    # "installAccountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .restart local v19    # "launchButtonTextId":I
    :cond_a
    const v19, 0x7f0c020b

    .line 346
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v3, v5, v6, v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getOpenClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v15

    goto/16 :goto_3

    .line 349
    :cond_b
    iget-boolean v3, v11, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabled:Z

    if-eqz v3, :cond_c

    .line 350
    new-instance v15, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$6;

    .end local v15    # "clickHandler":Landroid/view/View$OnClickListener;
    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$6;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;)V

    .line 362
    .restart local v15    # "clickHandler":Landroid/view/View$OnClickListener;
    const v19, 0x7f0c020f

    goto/16 :goto_3

    .line 364
    :cond_c
    const/16 v3, 0x8

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    goto/16 :goto_3

    .line 376
    .end local v15    # "clickHandler":Landroid/view/View$OnClickListener;
    .end local v19    # "launchButtonTextId":I
    .restart local v21    # "owner":Landroid/accounts/Account;
    :cond_d
    const/16 v17, 0x0

    goto/16 :goto_4
.end method

.method protected showDynamicStatus(I)V
    .locals 2
    .param p1, "statusStringId"    # I

    .prologue
    .line 180
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->showDynamicStatus(I)V

    .line 181
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDynamicSection:Lcom/google/android/finsky/layout/DetailsSummaryDynamic;

    const v1, 0x7f0a0181

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 182
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->hideNonDynamicViews()V

    .line 183
    return-void
.end method

.method protected syncDynamicSection()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 118
    iget-object v8, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v8

    const/4 v9, 0x3

    if-eq v8, v9, :cond_1

    .line 119
    const-string v8, "Unexpected doc backend %s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    invoke-super {p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->syncDynamicSection()V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v8

    iget-object v4, v8, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 127
    .local v4, "packageName":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 131
    iget-object v8, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mDynamicSection:Lcom/google/android/finsky/layout/DetailsSummaryDynamic;

    const v9, 0x7f0a0181

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 133
    .local v1, "downloadSection":Landroid/view/ViewGroup;
    iget-object v8, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v8, v4}, Lcom/google/android/finsky/receivers/Installer;->getProgress(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    move-result-object v6

    .line 135
    .local v6, "progressReport":Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;
    sget-object v8, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$8;->$SwitchMap$com$google$android$finsky$receivers$Installer$InstallerState:[I

    iget-object v9, v6, Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;->installerState:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    invoke-virtual {v9}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 153
    invoke-virtual {v1, v11}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 155
    const v8, 0x7f0a014d

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 156
    .local v2, "downloadingBytes":Landroid/widget/TextView;
    const v8, 0x7f0a014c

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 158
    .local v3, "downloadingPercentage":Landroid/widget/TextView;
    const v8, 0x7f0a010c

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ProgressBar;

    .line 159
    .local v5, "progressBar":Landroid/widget/ProgressBar;
    iget-object v8, p0, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->mContext:Landroid/content/Context;

    invoke-static {v8, v6, v2, v3, v5}, Lcom/google/android/finsky/adapters/DownloadProgressHelper;->configureDownloadProgressUi(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ProgressBar;)V

    .line 162
    const v8, 0x7f0a014b

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 163
    .local v0, "cancel":Landroid/widget/ImageView;
    new-instance v8, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$1;

    invoke-direct {v8, p0, v4, v1}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder$1;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;Ljava/lang/String;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 173
    const v8, 0x7f0a0178

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 174
    .local v7, "title":Landroid/widget/TextView;
    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setSelected(Z)V

    .line 175
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->hideNonDynamicViews()V

    goto :goto_0

    .line 137
    .end local v0    # "cancel":Landroid/widget/ImageView;
    .end local v2    # "downloadingBytes":Landroid/widget/TextView;
    .end local v3    # "downloadingPercentage":Landroid/widget/TextView;
    .end local v5    # "progressBar":Landroid/widget/ProgressBar;
    .end local v7    # "title":Landroid/widget/TextView;
    :pswitch_0
    const v8, 0x7f0c029d

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->showDynamicStatus(I)V

    goto :goto_0

    .line 140
    :pswitch_1
    const v8, 0x7f0c029f

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/activities/DetailsSummaryAppsViewBinder;->showDynamicStatus(I)V

    goto :goto_0

    .line 144
    :pswitch_2
    const/4 v8, 0x4

    invoke-virtual {v1, v8}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 145
    invoke-super {p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->syncDynamicSection()V

    goto/16 :goto_0

    .line 135
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
