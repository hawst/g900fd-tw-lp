.class Lcom/google/android/finsky/download/DownloadProgressManager;
.super Ljava/lang/Object;
.source "DownloadProgressManager.java"

# interfaces
.implements Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;
    }
.end annotation


# instance fields
.field private final mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

.field private mDownloadProgressMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;"
        }
    .end annotation
.end field

.field private final mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

.field private final mHandler:Landroid/os/Handler;

.field private final mHandlerThread:Landroid/os/HandlerThread;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/download/DownloadQueueImpl;)V
    .locals 2
    .param p1, "downloadQueue"    # Lcom/google/android/finsky/download/DownloadQueueImpl;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadProgressMap:Ljava/util/Map;

    .line 50
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    .line 51
    invoke-virtual {p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->getDownloadManager()Lcom/google/android/finsky/download/DownloadManagerFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    .line 54
    const-string v0, "Download progress manager runner"

    invoke-static {v0}, Lcom/google/android/finsky/utils/BackgroundThreadFactory;->createHandlerThread(Ljava/lang/String;)Landroid/os/HandlerThread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mHandlerThread:Landroid/os/HandlerThread;

    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 57
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mHandler:Landroid/os/Handler;

    .line 60
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/download/DownloadProgressManager$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/download/DownloadProgressManager$1;-><init>(Lcom/google/android/finsky/download/DownloadProgressManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 66
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/download/DownloadProgressManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadProgressManager;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/finsky/download/DownloadProgressManager;->onDownloadProgress()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/download/DownloadProgressManager;)Lcom/google/android/finsky/download/DownloadManagerFacade;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadProgressManager;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/download/DownloadProgressManager;)Landroid/os/HandlerThread;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadProgressManager;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mHandlerThread:Landroid/os/HandlerThread;

    return-object v0
.end method

.method private onDownloadProgress()V
    .locals 10

    .prologue
    .line 174
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 177
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v5

    .line 178
    .local v5, "oldUris":Ljava/util/Set;, "Ljava/util/Set<Landroid/net/Uri;>;"
    iget-object v8, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadProgressMap:Ljava/util/Map;

    if-eqz v8, :cond_0

    .line 179
    iget-object v8, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadProgressMap:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 184
    :cond_0
    iget-object v8, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    const/4 v9, 0x0

    invoke-interface {v8, v9, p0}, Lcom/google/android/finsky/download/DownloadManagerFacade;->query(Landroid/net/Uri;Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;)Ljava/util/List;

    move-result-object v6

    .line 187
    .local v6, "progress":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/download/DownloadProgress;>;"
    invoke-static {}, Lcom/google/android/play/utils/collections/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v3

    .line 188
    .local v3, "newMap":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/Uri;Lcom/google/android/finsky/download/DownloadProgress;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/download/DownloadProgress;

    .line 189
    .local v1, "entry":Lcom/google/android/finsky/download/DownloadProgress;
    iget-object v8, v1, Lcom/google/android/finsky/download/DownloadProgress;->contentUri:Landroid/net/Uri;

    invoke-interface {v3, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 191
    .end local v1    # "entry":Lcom/google/android/finsky/download/DownloadProgress;
    :cond_1
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 195
    .local v0, "downloadProgressMap":Ljava/util/Map;, "Ljava/util/Map<Landroid/net/Uri;Lcom/google/android/finsky/download/DownloadProgress;>;"
    iget-object v8, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadProgressMap:Ljava/util/Map;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadProgressMap:Ljava/util/Map;

    invoke-interface {v8, v0}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 196
    :cond_2
    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadProgressMap:Ljava/util/Map;

    .line 198
    invoke-static {}, Lcom/google/android/play/utils/collections/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v4

    .line 199
    .local v4, "newUris":Ljava/util/Set;, "Ljava/util/Set<Landroid/net/Uri;>;"
    iget-object v8, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadProgressMap:Ljava/util/Map;

    if-eqz v8, :cond_3

    .line 200
    iget-object v8, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadProgressMap:Ljava/util/Map;

    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 203
    :cond_3
    invoke-interface {v5, v4}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 206
    new-instance v7, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;

    iget-object v8, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-direct {v7, v8, v5, v4, v0}, Lcom/google/android/finsky/download/DownloadProgressManager$ProgressRunnable;-><init>(Lcom/google/android/finsky/download/DownloadQueue;Ljava/util/Set;Ljava/util/Set;Ljava/util/Map;)V

    .line 208
    .local v7, "r":Ljava/lang/Runnable;
    new-instance v8, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v8, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 210
    .end local v4    # "newUris":Ljava/util/Set;, "Ljava/util/Set<Landroid/net/Uri;>;"
    .end local v7    # "r":Ljava/lang/Runnable;
    :cond_4
    return-void
.end method


# virtual methods
.method public cleanup()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/download/DownloadProgressManager$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/download/DownloadProgressManager$2;-><init>(Lcom/google/android/finsky/download/DownloadProgressManager;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 79
    return-void
.end method

.method public onChange()V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/finsky/download/DownloadProgressManager;->onDownloadProgress()V

    .line 167
    return-void
.end method
