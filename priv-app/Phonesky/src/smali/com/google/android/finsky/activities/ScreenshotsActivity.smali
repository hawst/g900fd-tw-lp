.class public Lcom/google/android/finsky/activities/ScreenshotsActivity;
.super Landroid/app/Activity;
.source "ScreenshotsActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/ScreenshotsActivity$ImagePagerAdapter;
    }
.end annotation


# instance fields
.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field private mPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 62
    return-void
.end method

.method public static show(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;II)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "index"    # I
    .param p3, "imageType"    # I

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/ScreenshotsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "document"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 33
    const-string v1, "index"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 34
    const-string v1, "imageType"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 35
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 36
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 44
    const v2, 0x7f040196

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/ScreenshotsActivity;->setContentView(I)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ScreenshotsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "document"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    iput-object v2, p0, Lcom/google/android/finsky/activities/ScreenshotsActivity;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 47
    const v2, 0x7f0a0374

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/ScreenshotsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    iput-object v2, p0, Lcom/google/android/finsky/activities/ScreenshotsActivity;->mPager:Landroid/support/v4/view/ViewPager;

    .line 49
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ScreenshotsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "imageType"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 50
    .local v0, "imageType":I
    iget-object v2, p0, Lcom/google/android/finsky/activities/ScreenshotsActivity;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v1

    .line 51
    .local v1, "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    iget-object v2, p0, Lcom/google/android/finsky/activities/ScreenshotsActivity;->mPager:Landroid/support/v4/view/ViewPager;

    new-instance v3, Lcom/google/android/finsky/activities/ScreenshotsActivity$ImagePagerAdapter;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v4

    invoke-direct {v3, v1, p0, v4}, Lcom/google/android/finsky/activities/ScreenshotsActivity$ImagePagerAdapter;-><init>(Ljava/util/List;Landroid/content/Context;Lcom/google/android/play/image/BitmapLoader;)V

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 53
    if-nez p1, :cond_0

    .line 54
    iget-object v2, p0, Lcom/google/android/finsky/activities/ScreenshotsActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ScreenshotsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "index"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 57
    :cond_0
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 58
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ScreenshotsActivity;->finish()V

    .line 60
    :cond_1
    return-void
.end method
