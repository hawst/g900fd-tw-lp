.class public Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "StoredValueTopUpActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/fragments/SidecarFragment$Listener;
.implements Lcom/google/android/finsky/layout/ButtonBar$ClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

.field private mDocuments:[Lcom/google/android/finsky/api/model/Document;

.field private mLastShownErrorId:I

.field private mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

.field private mListView:Landroid/widget/ListView;

.field private mLoadingIndicator:Landroid/view/View;

.field private mSelectedDocumentFormattedAmount:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 141
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mLastShownErrorId:I

    return-void
.end method

.method public static createIntent(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;)Landroid/content/Intent;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "topupInfo"    # Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .prologue
    .line 148
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 149
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 150
    const-string v1, "list_url"

    iget-object v2, p1, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;->optionsListUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    return-object v0
.end method

.method private syncPositiveButton()V
    .locals 3

    .prologue
    .line 271
    iget-object v1, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    .line 272
    .local v0, "hasChecked":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonEnabled(Z)V

    .line 273
    return-void

    .line 271
    .end local v0    # "hasChecked":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 277
    if-ne p1, v3, :cond_1

    .line 278
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 279
    .local v0, "returnedData":Landroid/content/Intent;
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    .line 280
    const v2, 0x7f0c0102

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mSelectedDocumentFormattedAmount:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 282
    .local v1, "successMessage":Ljava/lang/String;
    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 284
    .end local v1    # "successMessage":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0, p2, v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->setResult(ILandroid/content/Intent;)V

    .line 285
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->finish()V

    .line 289
    .end local v0    # "returnedData":Landroid/content/Intent;
    :goto_0
    return-void

    .line 288
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 156
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 157
    const v0, 0x7f04003b

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->setContentView(I)V

    .line 158
    const v0, 0x7f0a0109

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mLoadingIndicator:Landroid/view/View;

    .line 159
    const v0, 0x7f0a010d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListView:Landroid/widget/ListView;

    .line 161
    const v0, 0x7f0a0114

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ButtonBar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    .line 162
    iget-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const v1, 0x7f0c02a0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonTitle(I)V

    .line 163
    iget-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const v1, 0x7f0c0134

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonTitle(I)V

    .line 164
    iget-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/ButtonBar;->setClickListener(Lcom/google/android/finsky/layout/ButtonBar$ClickListener;)V

    .line 165
    invoke-direct {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->syncPositiveButton()V

    .line 167
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mAccountName:Ljava/lang/String;

    .line 168
    if-nez p1, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mAccountName:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "list_url"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;->newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;->access$000(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    .line 172
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    const-string v2, "list_sidecar"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 183
    :goto_0
    return-void

    .line 177
    :cond_0
    const-string v0, "selected_document_formatted_amount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mSelectedDocumentFormattedAmount:Ljava/lang/String;

    .line 179
    const-string v0, "last_shown_error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mLastShownErrorId:I

    .line 180
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "list_sidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 267
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-direct {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->syncPositiveButton()V

    .line 268
    return-void
.end method

.method public onNegativeButtonClick()V
    .locals 1

    .prologue
    .line 315
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->setResult(I)V

    .line 316
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->finish()V

    .line 317
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 332
    return-void
.end method

.method public onPositiveButtonClick()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 295
    iget-object v5, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    .line 296
    .local v0, "checkedIndex":I
    iget-object v5, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mDocuments:[Lcom/google/android/finsky/api/model/Document;

    aget-object v1, v5, v0

    .line 297
    .local v1, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v1, v8}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v3

    .line 298
    .local v3, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    if-nez v3, :cond_0

    .line 299
    const-string v5, "Document selected without PURCHASE offer. Ignoring."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 311
    :goto_0
    return-void

    .line 302
    :cond_0
    iget-object v5, v3, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mSelectedDocumentFormattedAmount:Ljava/lang/String;

    .line 303
    invoke-static {}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->builder()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setOfferType(I)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->build()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    move-result-object v4

    .line 307
    .local v4, "params":Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    iget-object v5, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v5, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v5, v4, v6, v7}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;[BLandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    .line 310
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2, v8}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 323
    if-nez p1, :cond_0

    .line 324
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->setResult(I)V

    .line 325
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->finish()V

    .line 327
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 195
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 196
    invoke-direct {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->syncPositiveButton()V

    .line 197
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 187
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 188
    const-string v0, "last_shown_error"

    iget v1, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mLastShownErrorId:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 189
    const-string v0, "selected_document_formatted_amount"

    iget-object v1, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mSelectedDocumentFormattedAmount:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 201
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;->getState()I

    move-result v0

    if-nez v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;->load()V

    .line 206
    :cond_0
    return-void
.end method

.method public onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V
    .locals 14
    .param p1, "fragment"    # Lcom/google/android/finsky/fragments/SidecarFragment;

    .prologue
    .line 216
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mLoadingIndicator:Landroid/view/View;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    .line 217
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListView:Landroid/widget/ListView;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setVisibility(I)V

    .line 218
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Lcom/google/android/finsky/layout/ButtonBar;->setVisibility(I)V

    .line 220
    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getState()I

    move-result v8

    .line 221
    .local v8, "state":I
    const/4 v10, 0x2

    if-ne v8, v10, :cond_2

    .line 222
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    invoke-virtual {v10}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;->getDocuments()[Lcom/google/android/finsky/api/model/Document;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mDocuments:[Lcom/google/android/finsky/api/model/Document;

    .line 223
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mDocuments:[Lcom/google/android/finsky/api/model/Document;

    array-length v10, v10

    invoke-static {v10}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v9

    .line 224
    .local v9, "titles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mDocuments:[Lcom/google/android/finsky/api/model/Document;

    .local v1, "arr$":[Lcom/google/android/finsky/api/model/Document;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v3, v1, v6

    .line 225
    .local v3, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 227
    .end local v3    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v10, 0x109000f

    invoke-direct {v0, p0, v10, v9}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 229
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v10, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 230
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListView:Landroid/widget/ListView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 231
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListView:Landroid/widget/ListView;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 232
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListView:Landroid/widget/ListView;

    invoke-virtual {v10, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 233
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListView:Landroid/widget/ListView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setVisibility(I)V

    .line 234
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/google/android/finsky/layout/ButtonBar;->setVisibility(I)V

    .line 235
    invoke-direct {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->syncPositiveButton()V

    .line 263
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    .end local v1    # "arr$":[Lcom/google/android/finsky/api/model/Document;
    .end local v6    # "i$":I
    .end local v7    # "len$":I
    .end local v9    # "titles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    return-void

    .line 236
    :cond_2
    const/4 v10, 0x1

    if-ne v8, v10, :cond_3

    .line 237
    iget-object v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mLoadingIndicator:Landroid/view/View;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 238
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getState()I

    move-result v10

    const/4 v11, 0x3

    if-ne v10, v11, :cond_1

    .line 239
    iget v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mLastShownErrorId:I

    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getStateInstance()I

    move-result v11

    if-ne v10, v11, :cond_4

    .line 240
    const-string v10, "Already showed error %d, ignoring."

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget v13, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mLastShownErrorId:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 243
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getStateInstance()I

    move-result v10

    iput v10, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mLastShownErrorId:I

    .line 244
    const/4 v5, 0x0

    .line 245
    .local v5, "errorMessage":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getSubstate()I

    move-result v10

    if-nez v10, :cond_6

    .line 246
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    invoke-virtual {v11}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;->getLastError()Lcom/android/volley/VolleyError;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v5

    .line 250
    :cond_5
    :goto_2
    if-eqz v5, :cond_7

    .line 251
    new-instance v2, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 253
    .local v2, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v2, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v10

    const v11, 0x7f0c02a0

    invoke-virtual {v10, v11}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v10, v11, v12, v13}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 255
    invoke-virtual {v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v4

    .line 256
    .local v4, "errorDialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v10

    const-string v11, "error_dialog"

    invoke-virtual {v4, v10, v11}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 247
    .end local v2    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v4    # "errorDialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->getSubstate()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_5

    .line 248
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v10

    const v11, 0x7f0c0100

    invoke-virtual {v10, v11}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    .line 258
    :cond_7
    const-string v10, "Received error without error message."

    const/4 v11, 0x0

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->setResult(I)V

    .line 260
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->finish()V

    goto/16 :goto_1
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity;->mListSidecar:Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/storedvalue/StoredValueTopUpActivity$ListSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 211
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 212
    return-void
.end method
