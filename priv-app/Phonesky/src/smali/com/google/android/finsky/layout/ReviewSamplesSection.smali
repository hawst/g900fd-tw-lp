.class public Lcom/google/android/finsky/layout/ReviewSamplesSection;
.super Lcom/google/android/finsky/layout/ForegroundLinearLayout;
.source "ReviewSamplesSection.java"

# interfaces
.implements Lcom/google/android/finsky/adapters/ReviewsAdapter$ReviewFeedbackHandler;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# instance fields
.field private mAdapter:Lcom/google/android/finsky/adapters/ReviewsAdapter;

.field private mAllReviewsFooter:Landroid/widget/TextView;

.field private mContainerFragment:Landroid/support/v4/app/Fragment;

.field private mData:Lcom/google/android/finsky/api/model/DfeReviews;

.field protected mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field protected mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mReviewHolder:Landroid/widget/LinearLayout;

.field private mReviewStats:Lcom/google/android/finsky/layout/HistogramView;

.field private final mReviewsMaxRows:I

.field private final mReviewsPerRow:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 70
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0e001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewsPerRow:I

    .line 71
    const v1, 0x7f0e0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewsMaxRows:I

    .line 72
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/ReviewSamplesSection;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/ReviewSamplesSection;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/Document;ZLandroid/support/v4/app/Fragment;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "hasDetailsLoaded"    # Z
    .param p4, "containerFragment"    # Landroid/support/v4/app/Fragment;
    .param p5, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 87
    if-nez p3, :cond_1

    .line 88
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->setVisibility(I)V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getReviewsUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    iput-object p4, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mContainerFragment:Landroid/support/v4/app/Fragment;

    .line 97
    iput-object p6, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 98
    iput-object p5, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 99
    iput-object p2, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 100
    iput-object p1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 103
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewStats:Lcom/google/android/finsky/layout/HistogramView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/HistogramView;->bind(Lcom/google/android/finsky/api/model/Document;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewStats:Lcom/google/android/finsky/layout/HistogramView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/HistogramView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 105
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewStats:Lcom/google/android/finsky/layout/HistogramView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/HistogramView;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->setNextFocusDownId(I)V

    .line 106
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewStats:Lcom/google/android/finsky/layout/HistogramView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/HistogramView;->setNextFocusUpId(I)V

    .line 109
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->refresh()V

    goto :goto_0
.end method

.method public invalidateCurrentReviewUrl()V
    .locals 7

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    if-nez v0, :cond_0

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getReviewsUrl()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeReviews;->shouldFilterByDevice()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeReviews;->getVersionFilter()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/DfeReviews;->getRatingFilter()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/DfeReviews;->getSortType()I

    move-result v5

    const/4 v6, 0x1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/finsky/api/DfeApi;->invalidateReviewsCache(Ljava/lang/String;ZIIIZ)V

    goto :goto_0
.end method

.method public onDataChanged()V
    .locals 17

    .prologue
    .line 143
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/DfeReviews;->getCount()I

    move-result v7

    .line 144
    .local v7, "reviewCount":I
    if-nez v7, :cond_0

    .line 145
    const/16 v14, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->setVisibility(I)V

    .line 195
    :goto_0
    return-void

    .line 148
    :cond_0
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->setVisibility(I)V

    .line 151
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v14}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 153
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 155
    .local v5, "inflater":Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAdapter:Lcom/google/android/finsky/adapters/ReviewsAdapter;

    invoke-virtual {v14}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getFirstReviewViewIndex()I

    move-result v4

    .line 156
    .local v4, "firstReviewViewIndex":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewsPerRow:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewsMaxRows:I

    mul-int v6, v14, v15

    .line 157
    .local v6, "maxReviewsToShow":I
    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 158
    .local v11, "reviewsToShow":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewsPerRow:I

    invoke-static {v11, v14}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v9

    .line 160
    .local v9, "reviewRows":I
    const/4 v13, 0x0

    .local v13, "rowIndex":I
    :goto_1
    if-ge v13, v9, :cond_3

    .line 161
    const v14, 0x7f04018c

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewHolder:Landroid/widget/LinearLayout;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v5, v14, v15, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/layout/BucketRow;

    .line 163
    .local v12, "row":Lcom/google/android/finsky/layout/BucketRow;
    const/4 v14, 0x1

    invoke-virtual {v12, v14}, Lcom/google/android/finsky/layout/BucketRow;->setSameChildHeight(Z)V

    .line 164
    const/4 v1, 0x0

    .local v1, "colIndex":I
    :goto_2
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewsPerRow:I

    if-ge v1, v14, :cond_2

    .line 165
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewsPerRow:I

    mul-int/2addr v14, v13

    add-int v8, v14, v1

    .line 166
    .local v8, "reviewIndex":I
    if-ge v8, v11, :cond_1

    .line 167
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAdapter:Lcom/google/android/finsky/adapters/ReviewsAdapter;

    add-int v15, v8, v4

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v14, v15, v0, v12}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 169
    .local v10, "reviewViewToBeAdded":Landroid/view/View;
    invoke-virtual {v12, v10}, Lcom/google/android/finsky/layout/BucketRow;->addView(Landroid/view/View;)V

    .line 164
    .end local v10    # "reviewViewToBeAdded":Landroid/view/View;
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 172
    :cond_1
    new-instance v3, Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-direct {v3, v14}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 173
    .local v3, "dummy":Landroid/view/View;
    const/4 v14, 0x4

    invoke-virtual {v3, v14}, Landroid/view/View;->setVisibility(I)V

    .line 174
    invoke-virtual {v12, v3}, Lcom/google/android/finsky/layout/BucketRow;->addView(Landroid/view/View;)V

    goto :goto_3

    .line 177
    .end local v3    # "dummy":Landroid/view/View;
    .end local v8    # "reviewIndex":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewHolder:Landroid/widget/LinearLayout;

    invoke-virtual {v14, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 160
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 180
    .end local v1    # "colIndex":I
    .end local v12    # "row":Lcom/google/android/finsky/layout/BucketRow;
    :cond_3
    if-le v7, v6, :cond_4

    .line 181
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 182
    .local v2, "context":Landroid/content/Context;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAllReviewsFooter:Landroid/widget/TextView;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 183
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAllReviewsFooter:Landroid/widget/TextView;

    const v15, 0x7f0c01f6

    invoke-virtual {v2, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAllReviewsFooter:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v15

    invoke-static {v2, v15}, Lcom/google/android/play/utils/PlayCorpusUtils;->getPrimaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 189
    .end local v2    # "context":Landroid/content/Context;
    :goto_4
    new-instance v14, Lcom/google/android/finsky/layout/ReviewSamplesSection$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection$1;-><init>(Lcom/google/android/finsky/layout/ReviewSamplesSection;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 187
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAllReviewsFooter:Landroid/widget/TextView;

    const/16 v15, 0x8

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 199
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAdapter:Lcom/google/android/finsky/adapters/ReviewsAdapter;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAdapter:Lcom/google/android/finsky/adapters/ReviewsAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/ReviewsAdapter;->onDestroyView()V

    .line 201
    iput-object v1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAdapter:Lcom/google/android/finsky/adapters/ReviewsAdapter;

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    if-eqz v0, :cond_1

    .line 204
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 205
    iput-object v1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    .line 208
    :cond_1
    invoke-super {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->onDetachedFromWindow()V

    .line 209
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/android/finsky/layout/ForegroundLinearLayout;->onFinishInflate()V

    .line 78
    const v0, 0x7f0a0368

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/HistogramView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewStats:Lcom/google/android/finsky/layout/HistogramView;

    .line 79
    const v0, 0x7f0a015a

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mReviewHolder:Landroid/widget/LinearLayout;

    .line 80
    const v0, 0x7f0a015b

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAllReviewsFooter:Landroid/widget/TextView;

    .line 81
    return-void
.end method

.method public onReviewFeedback(Lcom/google/android/finsky/protos/DocumentV2$Review;)V
    .locals 5
    .param p1, "review"    # Lcom/google/android/finsky/protos/DocumentV2$Review;

    .prologue
    .line 214
    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mContainerFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    .line 215
    .local v1, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v2, "rate_review_dialog"

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 223
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/finsky/protos/DocumentV2$Review;->commentId:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/activities/ReviewFeedbackDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)Lcom/google/android/finsky/activities/ReviewFeedbackDialog;

    move-result-object v0

    .line 221
    .local v0, "dialog":Lcom/google/android/finsky/activities/ReviewFeedbackDialog;
    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mContainerFragment:Landroid/support/v4/app/Fragment;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/activities/ReviewFeedbackDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 222
    const-string v2, "rate_review_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/activities/ReviewFeedbackDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public refresh()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v0, :cond_0

    .line 131
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    if-eqz v0, :cond_1

    .line 122
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 124
    :cond_1
    new-instance v0, Lcom/google/android/finsky/api/model/DfeReviews;

    iget-object v1, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getReviewsUrl()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getVersionCode()I

    move-result v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/api/model/DfeReviews;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeReviews;->setSortType(I)V

    .line 126
    new-instance v0, Lcom/google/android/finsky/adapters/ReviewsAdapter;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    const/4 v5, 0x3

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v9, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object v6, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/finsky/adapters/ReviewsAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeReviews;ZILcom/google/android/finsky/adapters/ReviewsAdapter$ReviewFeedbackHandler;Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mAdapter:Lcom/google/android/finsky/adapters/ReviewsAdapter;

    .line 129
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewSamplesSection;->mData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeReviews;->startLoadItems()V

    goto :goto_0
.end method
