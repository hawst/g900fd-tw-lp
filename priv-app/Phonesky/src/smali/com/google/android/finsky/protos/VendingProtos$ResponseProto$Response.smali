.class public final Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos$ResponseProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Response"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;


# instance fields
.field public ackNotificationsResponse:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

.field public billingEventResponse:Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

.field public checkLicenseResponse:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

.field public contentSyncResponse:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

.field public getAssetResponse:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

.field public getMarketMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

.field public inAppPurchaseInformationResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

.field public inAppRestoreTransactionsResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

.field public modifyCommentResponse:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

.field public purchaseMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

.field public responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

.field public restoreApplicationResponse:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5405
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5406
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->clear()Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    .line 5407
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    .locals 2

    .prologue
    .line 5358
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    if-nez v0, :cond_1

    .line 5359
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 5361
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    if-nez v0, :cond_0

    .line 5362
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    sput-object v0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    .line 5364
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5366
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    return-object v0

    .line 5364
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5410
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    .line 5411
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->modifyCommentResponse:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    .line 5412
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->contentSyncResponse:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    .line 5413
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getAssetResponse:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    .line 5414
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->purchaseMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    .line 5415
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->checkLicenseResponse:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    .line 5416
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getMarketMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    .line 5417
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->restoreApplicationResponse:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;

    .line 5418
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->billingEventResponse:Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    .line 5419
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppRestoreTransactionsResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    .line 5420
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppPurchaseInformationResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    .line 5421
    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->ackNotificationsResponse:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    .line 5422
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->cachedSize:I

    .line 5423
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 5470
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 5471
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    if-eqz v1, :cond_0

    .line 5472
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5475
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->modifyCommentResponse:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    if-eqz v1, :cond_1

    .line 5476
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->modifyCommentResponse:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5479
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->contentSyncResponse:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    if-eqz v1, :cond_2

    .line 5480
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->contentSyncResponse:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5483
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getAssetResponse:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    if-eqz v1, :cond_3

    .line 5484
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getAssetResponse:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5487
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->purchaseMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    if-eqz v1, :cond_4

    .line 5488
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->purchaseMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5491
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->checkLicenseResponse:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    if-eqz v1, :cond_5

    .line 5492
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->checkLicenseResponse:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5495
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getMarketMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    if-eqz v1, :cond_6

    .line 5496
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getMarketMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5499
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->restoreApplicationResponse:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;

    if-eqz v1, :cond_7

    .line 5500
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->restoreApplicationResponse:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5503
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->billingEventResponse:Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    if-eqz v1, :cond_8

    .line 5504
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->billingEventResponse:Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5507
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppRestoreTransactionsResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    if-eqz v1, :cond_9

    .line 5508
    const/16 v1, 0x1e

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppRestoreTransactionsResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5511
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppPurchaseInformationResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    if-eqz v1, :cond_a

    .line 5512
    const/16 v1, 0x1f

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppPurchaseInformationResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5515
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->ackNotificationsResponse:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    if-eqz v1, :cond_b

    .line 5516
    const/16 v1, 0x21

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->ackNotificationsResponse:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5519
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5527
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5528
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5532
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5533
    :sswitch_0
    return-object p0

    .line 5538
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    if-nez v1, :cond_1

    .line 5539
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    .line 5541
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5545
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->modifyCommentResponse:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    if-nez v1, :cond_2

    .line 5546
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->modifyCommentResponse:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    .line 5548
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->modifyCommentResponse:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5552
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->contentSyncResponse:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    if-nez v1, :cond_3

    .line 5553
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->contentSyncResponse:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    .line 5555
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->contentSyncResponse:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5559
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getAssetResponse:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    if-nez v1, :cond_4

    .line 5560
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getAssetResponse:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    .line 5562
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getAssetResponse:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5566
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->purchaseMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    if-nez v1, :cond_5

    .line 5567
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->purchaseMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    .line 5569
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->purchaseMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5573
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->checkLicenseResponse:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    if-nez v1, :cond_6

    .line 5574
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->checkLicenseResponse:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    .line 5576
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->checkLicenseResponse:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 5580
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getMarketMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    if-nez v1, :cond_7

    .line 5581
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getMarketMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    .line 5583
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getMarketMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5587
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->restoreApplicationResponse:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;

    if-nez v1, :cond_8

    .line 5588
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->restoreApplicationResponse:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;

    .line 5590
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->restoreApplicationResponse:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5594
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->billingEventResponse:Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    if-nez v1, :cond_9

    .line 5595
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->billingEventResponse:Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    .line 5597
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->billingEventResponse:Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5601
    :sswitch_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppRestoreTransactionsResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    if-nez v1, :cond_a

    .line 5602
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppRestoreTransactionsResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    .line 5604
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppRestoreTransactionsResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5608
    :sswitch_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppPurchaseInformationResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    if-nez v1, :cond_b

    .line 5609
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppPurchaseInformationResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    .line 5611
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppPurchaseInformationResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5615
    :sswitch_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->ackNotificationsResponse:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    if-nez v1, :cond_c

    .line 5616
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->ackNotificationsResponse:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    .line 5618
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->ackNotificationsResponse:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 5528
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x2a -> :sswitch_2
        0x42 -> :sswitch_3
        0x4a -> :sswitch_4
        0x62 -> :sswitch_5
        0x8a -> :sswitch_6
        0x92 -> :sswitch_7
        0xba -> :sswitch_8
        0xca -> :sswitch_9
        0xf2 -> :sswitch_a
        0xfa -> :sswitch_b
        0x10a -> :sswitch_c
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5352
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5429
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    if-eqz v0, :cond_0

    .line 5430
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->responseProperties:Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5432
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->modifyCommentResponse:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    if-eqz v0, :cond_1

    .line 5433
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->modifyCommentResponse:Lcom/google/android/finsky/protos/VendingProtos$ModifyCommentResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5435
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->contentSyncResponse:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    if-eqz v0, :cond_2

    .line 5436
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->contentSyncResponse:Lcom/google/android/finsky/protos/VendingProtos$ContentSyncResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5438
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getAssetResponse:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    if-eqz v0, :cond_3

    .line 5439
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getAssetResponse:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5441
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->purchaseMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    if-eqz v0, :cond_4

    .line 5442
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->purchaseMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5444
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->checkLicenseResponse:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    if-eqz v0, :cond_5

    .line 5445
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->checkLicenseResponse:Lcom/google/android/finsky/protos/VendingProtos$CheckLicenseResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5447
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getMarketMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    if-eqz v0, :cond_6

    .line 5448
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->getMarketMetadataResponse:Lcom/google/android/finsky/protos/VendingProtos$GetMarketMetadataResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5450
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->restoreApplicationResponse:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;

    if-eqz v0, :cond_7

    .line 5451
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->restoreApplicationResponse:Lcom/google/android/finsky/protos/VendingProtos$RestoreApplicationsResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5453
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->billingEventResponse:Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    if-eqz v0, :cond_8

    .line 5454
    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->billingEventResponse:Lcom/google/android/finsky/protos/VendingProtos$BillingEventResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5456
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppRestoreTransactionsResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    if-eqz v0, :cond_9

    .line 5457
    const/16 v0, 0x1e

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppRestoreTransactionsResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppRestoreTransactionsResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5459
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppPurchaseInformationResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    if-eqz v0, :cond_a

    .line 5460
    const/16 v0, 0x1f

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->inAppPurchaseInformationResponse:Lcom/google/android/finsky/protos/VendingProtos$InAppPurchaseInformationResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5462
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->ackNotificationsResponse:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    if-eqz v0, :cond_b

    .line 5463
    const/16 v0, 0x21

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponseProto$Response;->ackNotificationsResponse:Lcom/google/android/finsky/protos/VendingProtos$AckNotificationsResponseProto;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 5465
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5466
    return-void
.end method
