.class public Lcom/google/android/finsky/layout/BillingAddress;
.super Landroid/widget/LinearLayout;
.source "BillingAddress.java"

# interfaces
.implements Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/BillingAddress$2;,
        Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;,
        Lcom/google/android/finsky/layout/BillingAddress$AddressSuggestionProviderImpl;,
        Lcom/google/android/finsky/layout/BillingAddress$CountrySpinnerItem;
    }
.end annotation


# static fields
.field private static KEY_ADDRESS_SPEC:Ljava/lang/String;

.field private static KEY_SELECTED_COUNTRY:Ljava/lang/String;


# instance fields
.field private mAddressPlaceholder:Lcom/google/android/finsky/layout/AddressFieldsLayout;

.field private mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

.field private mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

.field private mCountries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;",
            ">;"
        }
    .end annotation
.end field

.field private mCountryChangeListener:Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;

.field private mCountrySpinner:Landroid/widget/Spinner;

.field private mCountrySpinnerSelectionSet:Z

.field private mEmailAddress:Landroid/widget/EditText;

.field private mFirstName:Landroid/widget/EditText;

.field private mLastName:Landroid/widget/EditText;

.field private mNameEntry:Landroid/widget/EditText;

.field private mParentListener:Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

.field private mPhoneNumber:Landroid/widget/EditText;

.field private mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

.field private mSuggestionProvider:Lcom/google/android/finsky/layout/BillingAddress$AddressSuggestionProviderImpl;

.field private mWhitelistedCountries:Lcom/google/android/finsky/placesapi/WhitelistedCountriesFlagParser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-string v0, "address_spec"

    sput-object v0, Lcom/google/android/finsky/layout/BillingAddress;->KEY_ADDRESS_SPEC:Ljava/lang/String;

    .line 66
    const-string v0, "selected_country"

    sput-object v0, Lcom/google/android/finsky/layout/BillingAddress;->KEY_SELECTED_COUNTRY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 93
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinnerSelectionSet:Z

    .line 103
    const-string v1, "layout_inflater"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 105
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f04002c

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/BillingAddress;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/BillingAddress;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountries:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/BillingAddress;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/BillingAddress;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/BillingAddress;)Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/BillingAddress;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountryChangeListener:Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/BillingAddress;)Lcom/android/i18n/addressinput/AddressWidget;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/BillingAddress;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    return-object v0
.end method

.method private static addressFieldToAddressEnum(Lcom/android/i18n/addressinput/AddressField;)I
    .locals 2
    .param p0, "addressField"    # Lcom/android/i18n/addressinput/AddressField;

    .prologue
    .line 414
    sget-object v0, Lcom/google/android/finsky/layout/BillingAddress$2;->$SwitchMap$com$android$i18n$addressinput$AddressField:[I

    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressField;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 431
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 416
    :pswitch_0
    const/16 v0, 0x8

    goto :goto_0

    .line 418
    :pswitch_1
    const/4 v0, 0x7

    goto :goto_0

    .line 421
    :pswitch_2
    const/4 v0, 0x5

    goto :goto_0

    .line 423
    :pswitch_3
    const/4 v0, 0x6

    goto :goto_0

    .line 425
    :pswitch_4
    const/16 v0, 0xb

    goto :goto_0

    .line 427
    :pswitch_5
    const/16 v0, 0x9

    goto :goto_0

    .line 429
    :pswitch_6
    const/16 v0, 0xa

    goto :goto_0

    .line 414
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private addressProblemsToInputValidationErrors(Lcom/android/i18n/addressinput/AddressProblems;Ljava/util/List;)V
    .locals 7
    .param p1, "addressProblems"    # Lcom/android/i18n/addressinput/AddressProblems;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/i18n/addressinput/AddressProblems;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 530
    .local p2, "validationErrors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;>;"
    invoke-virtual {p1}, Lcom/android/i18n/addressinput/AddressProblems;->getProblems()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 532
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/i18n/addressinput/AddressField;Lcom/android/i18n/addressinput/AddressProblemType;>;"
    sget-object v4, Lcom/google/android/finsky/layout/BillingAddress$2;->$SwitchMap$com$android$i18n$addressinput$AddressField:[I

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v3}, Lcom/android/i18n/addressinput/AddressField;->ordinal()I

    move-result v3

    aget v3, v4, v3

    packed-switch v3, :pswitch_data_0

    .line 556
    const/16 v1, 0xd

    .line 557
    .local v1, "field":I
    const-string v3, "No equivalent for address widget field: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 559
    :goto_1
    invoke-static {v1}, Lcom/google/android/finsky/layout/BillingAddress;->createValidationError(I)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 534
    .end local v1    # "field":I
    :pswitch_0
    const/16 v1, 0x8

    .line 535
    .restart local v1    # "field":I
    goto :goto_1

    .line 537
    .end local v1    # "field":I
    :pswitch_1
    const/4 v1, 0x7

    .line 538
    .restart local v1    # "field":I
    goto :goto_1

    .line 541
    .end local v1    # "field":I
    :pswitch_2
    const/4 v1, 0x5

    .line 542
    .restart local v1    # "field":I
    goto :goto_1

    .line 544
    .end local v1    # "field":I
    :pswitch_3
    const/4 v1, 0x6

    .line 545
    .restart local v1    # "field":I
    goto :goto_1

    .line 547
    .end local v1    # "field":I
    :pswitch_4
    const/16 v1, 0xb

    .line 548
    .restart local v1    # "field":I
    goto :goto_1

    .line 550
    .end local v1    # "field":I
    :pswitch_5
    const/16 v1, 0x9

    .line 551
    .restart local v1    # "field":I
    goto :goto_1

    .line 553
    .end local v1    # "field":I
    :pswitch_6
    const/16 v1, 0xa

    .line 554
    .restart local v1    # "field":I
    goto :goto_1

    .line 561
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/android/i18n/addressinput/AddressField;Lcom/android/i18n/addressinput/AddressProblemType;>;"
    .end local v1    # "field":I
    :cond_0
    return-void

    .line 532
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static createValidationError(I)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .locals 2
    .param p0, "inputField"    # I

    .prologue
    .line 604
    new-instance v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;-><init>()V

    .line 605
    .local v0, "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    iput p0, v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    .line 606
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasInputField:Z

    .line 607
    return-object v0
.end method

.method private static createValidationErrorWithMessage(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .locals 2
    .param p0, "inputField"    # I
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    .line 612
    invoke-static {p0}, Lcom/google/android/finsky/layout/BillingAddress;->createValidationError(I)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v0

    .line 613
    .local v0, "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    iput-object p1, v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    .line 614
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasErrorMessage:Z

    .line 615
    return-object v0
.end method

.method private isInReducedAddressMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 658
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    iget v1, v1, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->billingAddressType:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onHeightOffsetChangedHoneycomb(F)V
    .locals 1
    .param p1, "heightOffset"    # F

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTranslationY(F)V

    .line 678
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTranslationY(F)V

    .line 679
    return-void
.end method

.method private static optionsFromInputFieldList([I)Lcom/android/i18n/addressinput/FormOptions;
    .locals 10
    .param p0, "requiredFields"    # [I

    .prologue
    .line 391
    new-instance v2, Lcom/android/i18n/addressinput/FormOptions$Builder;

    invoke-direct {v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;-><init>()V

    .line 393
    .local v2, "b":Lcom/android/i18n/addressinput/FormOptions$Builder;
    sget-object v8, Lcom/android/i18n/addressinput/AddressField;->COUNTRY:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v2, v8}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    move-result-object v8

    sget-object v9, Lcom/android/i18n/addressinput/AddressField;->RECIPIENT:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v8, v9}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    move-result-object v8

    sget-object v9, Lcom/android/i18n/addressinput/AddressField;->ORGANIZATION:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v8, v9}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    .line 395
    invoke-static {}, Lcom/android/i18n/addressinput/AddressField;->values()[Lcom/android/i18n/addressinput/AddressField;

    move-result-object v1

    .local v1, "arr$":[Lcom/android/i18n/addressinput/AddressField;
    array-length v7, v1

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v7, :cond_3

    aget-object v3, v1, v6

    .line 396
    .local v3, "field":Lcom/android/i18n/addressinput/AddressField;
    const/4 v4, 0x1

    .line 397
    .local v4, "hideField":Z
    invoke-static {v3}, Lcom/google/android/finsky/layout/BillingAddress;->addressFieldToAddressEnum(Lcom/android/i18n/addressinput/AddressField;)I

    move-result v0

    .line 398
    .local v0, "addressEnumValue":I
    const/4 v8, -0x1

    if-eq v0, v8, :cond_0

    .line 399
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    array-length v8, p0

    if-ge v5, v8, :cond_0

    .line 400
    aget v8, p0, v5

    if-ne v8, v0, :cond_2

    .line 401
    const/4 v4, 0x0

    .line 406
    .end local v5    # "i":I
    :cond_0
    if-eqz v4, :cond_1

    .line 407
    invoke-virtual {v2, v3}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    .line 395
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 399
    .restart local v5    # "i":I
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 410
    .end local v0    # "addressEnumValue":I
    .end local v3    # "field":Lcom/android/i18n/addressinput/AddressField;
    .end local v4    # "hideField":Z
    .end local v5    # "i":I
    :cond_3
    invoke-virtual {v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;->build()Lcom/android/i18n/addressinput/FormOptions;

    move-result-object v8

    return-object v8
.end method

.method private static populatedRequiredFieldsFromAddressType(I)[I
    .locals 8
    .param p0, "billingAddressType"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x1

    const/4 v3, 0x4

    .line 364
    if-ne p0, v5, :cond_1

    .line 365
    const/16 v2, 0x8

    new-array v0, v2, [I

    .line 367
    .local v0, "fields":[I
    aput v3, v0, v6

    .line 368
    const/16 v2, 0xa

    aput v2, v0, v5

    .line 369
    const/16 v2, 0x9

    aput v2, v0, v7

    .line 370
    const/4 v2, 0x5

    aput v2, v0, v4

    .line 371
    const/4 v2, 0x6

    aput v2, v0, v3

    .line 372
    const/4 v2, 0x5

    const/16 v3, 0x8

    aput v3, v0, v2

    .line 373
    const/4 v2, 0x6

    const/4 v3, 0x7

    aput v3, v0, v2

    .line 374
    const/4 v2, 0x7

    const/16 v3, 0xc

    aput v3, v0, v2

    .line 387
    :cond_0
    :goto_0
    return-object v0

    .line 376
    .end local v0    # "fields":[I
    :cond_1
    sget-object v2, Lcom/google/android/finsky/config/G;->reducedBillingAddressRequiresPhonenumber:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 377
    .local v1, "phoneNumRequired":Z
    if-eqz v1, :cond_2

    move v2, v3

    :goto_1
    new-array v0, v2, [I

    .line 380
    .restart local v0    # "fields":[I
    aput v3, v0, v6

    .line 381
    const/16 v2, 0xa

    aput v2, v0, v5

    .line 382
    const/16 v2, 0x9

    aput v2, v0, v7

    .line 383
    if-eqz v1, :cond_0

    .line 384
    const/16 v2, 0xc

    aput v2, v0, v4

    goto :goto_0

    .end local v0    # "fields":[I
    :cond_2
    move v2, v4

    .line 377
    goto :goto_1
.end method

.method private static validateEmailAddress(Ljava/lang/CharSequence;)Z
    .locals 1
    .param p0, "emailAddress"    # Ljava/lang/CharSequence;

    .prologue
    .line 654
    sget-object v0, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public clearErrorMessage()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 439
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 441
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 443
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 444
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressWidget;->clearErrorMessage()V

    .line 445
    return-void
.end method

.method public displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;
    .locals 9
    .param p1, "error"    # Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .prologue
    const v8, 0x7f0c00b5

    const/4 v7, 0x0

    .line 451
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 452
    .local v1, "context":Landroid/content/Context;
    iget-object v2, p1, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    .line 453
    .local v2, "errorMessage":Ljava/lang/String;
    const/4 v3, 0x0

    .line 454
    .local v3, "textView":Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 455
    .local v0, "addressField":Lcom/android/i18n/addressinput/AddressField;
    iget v4, p1, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    packed-switch v4, :pswitch_data_0

    .line 506
    :pswitch_0
    const-string v4, "InputValidationError that can\'t be displayed: type=%d, message=%s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, p1, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    iget-object v7, p1, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 511
    :goto_0
    if-eqz v0, :cond_0

    .line 512
    iget-object v4, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v4, v0}, Lcom/android/i18n/addressinput/AddressWidget;->getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 513
    iget-object v4, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v4, v0}, Lcom/android/i18n/addressinput/AddressWidget;->displayErrorMessageForInvalidEntryIn(Lcom/android/i18n/addressinput/AddressField;)Landroid/widget/TextView;

    .line 524
    :cond_0
    :goto_1
    return-object v3

    .line 457
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    .line 458
    iget-object v4, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 462
    :pswitch_2
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    .line 463
    iget-object v4, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    const v5, 0x7f0c00b6

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 467
    :pswitch_3
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    .line 468
    iget-object v4, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    const v5, 0x7f0c00b7

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 472
    :pswitch_4
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    .line 473
    iget-object v4, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    const v5, 0x7f0c00b4

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 477
    :pswitch_5
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    .line 478
    iget-object v4, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    const v5, 0x7f0c00b8

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 482
    :pswitch_6
    sget-object v0, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    .line 483
    goto :goto_0

    .line 485
    :pswitch_7
    sget-object v0, Lcom/android/i18n/addressinput/AddressField;->LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    .line 486
    goto :goto_0

    .line 488
    :pswitch_8
    const-string v4, "Input error ADDR_WHOLE_ADDRESS. Displaying at ADDRESS_LINE_1."

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 491
    :pswitch_9
    sget-object v0, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_1:Lcom/android/i18n/addressinput/AddressField;

    .line 492
    goto :goto_0

    .line 494
    :pswitch_a
    sget-object v0, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_2:Lcom/android/i18n/addressinput/AddressField;

    .line 495
    goto :goto_0

    .line 497
    :pswitch_b
    sget-object v0, Lcom/android/i18n/addressinput/AddressField;->DEPENDENT_LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    .line 498
    goto :goto_0

    .line 500
    :pswitch_c
    sget-object v0, Lcom/android/i18n/addressinput/AddressField;->POSTAL_CODE:Lcom/android/i18n/addressinput/AddressField;

    .line 501
    goto :goto_0

    .line 503
    :pswitch_d
    sget-object v0, Lcom/android/i18n/addressinput/AddressField;->COUNTRY:Lcom/android/i18n/addressinput/AddressField;

    .line 504
    goto :goto_0

    .line 518
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    .line 519
    iget-object v4, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 455
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_9
        :pswitch_a
        :pswitch_7
        :pswitch_6
        :pswitch_c
        :pswitch_d
        :pswitch_b
        :pswitch_4
        :pswitch_8
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public getAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 624
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v2}, Lcom/android/i18n/addressinput/AddressWidget;->getAddressData()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v0

    .line 626
    .local v0, "address":Lcom/android/i18n/addressinput/AddressData;
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    iget-object v2, v2, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    invoke-static {v0, v2}, Lcom/google/android/finsky/billing/BillingUtils;->instrumentAddressFromAddressData(Lcom/android/i18n/addressinput/AddressData;[I)Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v1

    .line 628
    .local v1, "billingAddress":Lcom/google/android/finsky/protos/BillingAddress$Address;
    invoke-direct {p0}, Lcom/google/android/finsky/layout/BillingAddress;->isInReducedAddressMode()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->deprecatedIsReduced:Z

    .line 629
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDeprecatedIsReduced:Z

    .line 630
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 631
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    .line 632
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPhoneNumber:Z

    .line 634
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 635
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    .line 636
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasName:Z

    .line 638
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    .line 639
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->firstName:Ljava/lang/String;

    .line 640
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasFirstName:Z

    .line 642
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    .line 643
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->lastName:Ljava/lang/String;

    .line 644
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLastName:Z

    .line 646
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    .line 647
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->email:Ljava/lang/String;

    .line 648
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasEmail:Z

    .line 650
    :cond_4
    return-object v1
.end method

.method public getAddressValidationErrors()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;",
            ">;"
        }
    .end annotation

    .prologue
    const v4, 0x7f0c00c0

    .line 571
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 572
    .local v1, "validationErrors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;>;"
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v2}, Lcom/android/i18n/addressinput/AddressWidget;->getAddressProblems()Lcom/android/i18n/addressinput/AddressProblems;

    move-result-object v0

    .line 573
    .local v0, "addressProblems":Lcom/android/i18n/addressinput/AddressProblems;
    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/layout/BillingAddress;->addressProblemsToInputValidationErrors(Lcom/android/i18n/addressinput/AddressProblems;Ljava/util/List;)V

    .line 575
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/Utils;->isEmptyOrSpaces(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 577
    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/BillingAddress;->createValidationErrorWithMessage(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 580
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/Utils;->isEmptyOrSpaces(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 582
    const/16 v2, 0xf

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/BillingAddress;->createValidationErrorWithMessage(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 585
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/Utils;->isEmptyOrSpaces(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 587
    const/16 v2, 0x10

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/BillingAddress;->createValidationErrorWithMessage(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 590
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/Utils;->isEmptyOrSpaces(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 592
    const/16 v2, 0xc

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c00c3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/BillingAddress;->createValidationErrorWithMessage(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 595
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getVisibility()I

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/layout/BillingAddress;->validateEmailAddress(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 597
    const/16 v2, 0x11

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c00c4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/BillingAddress;->createValidationErrorWithMessage(ILjava/lang/String;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 600
    :cond_4
    return-object v1
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    .line 110
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 111
    const v0, 0x7f0a00bc

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/BillingAddress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    .line 112
    const v0, 0x7f0a00eb

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/BillingAddress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    .line 113
    const v0, 0x7f0a00ec

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/BillingAddress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    .line 114
    const v0, 0x7f0a00ef

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/BillingAddress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    .line 115
    const v0, 0x7f0a00ed

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/BillingAddress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    .line 116
    const v0, 0x7f0a00bf

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/BillingAddress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    .line 117
    const v0, 0x7f0a00ee

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/BillingAddress;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/AddressFieldsLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressPlaceholder:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    .line 118
    new-instance v1, Lcom/google/android/finsky/layout/BillingAddress$AddressSuggestionProviderImpl;

    new-instance v2, Lcom/google/android/finsky/placesapi/PlacesService;

    sget-object v0, Lcom/google/android/finsky/config/G;->placesApiKey:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/finsky/placesapi/AdrMicroformatParser;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/finsky/placesapi/AdrMicroformatParser;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/finsky/placesapi/PlacesService;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/placesapi/AdrMicroformatParser;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v0

    new-instance v3, Lcom/google/android/finsky/utils/CachedLocationAccess;

    invoke-direct {v3}, Lcom/google/android/finsky/utils/CachedLocationAccess;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/utils/CachedLocationAccess;->getCachedLocation(Landroid/content/Context;)Landroid/location/Location;

    move-result-object v3

    invoke-direct {v1, p0, v2, v0, v3}, Lcom/google/android/finsky/layout/BillingAddress$AddressSuggestionProviderImpl;-><init>(Lcom/google/android/finsky/layout/BillingAddress;Lcom/google/android/finsky/placesapi/PlacesService;Lcom/android/volley/RequestQueue;Landroid/location/Location;)V

    iput-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSuggestionProvider:Lcom/google/android/finsky/layout/BillingAddress$AddressSuggestionProviderImpl;

    .line 125
    new-instance v0, Lcom/google/android/finsky/placesapi/WhitelistedCountriesFlagParser;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/finsky/placesapi/WhitelistedCountriesFlagParser;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mWhitelistedCountries:Lcom/google/android/finsky/placesapi/WhitelistedCountriesFlagParser;

    .line 126
    return-void
.end method

.method public onHeightOffsetChanged(F)V
    .locals 2
    .param p1, "heightOffset"    # F

    .prologue
    .line 667
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 668
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/BillingAddress;->onHeightOffsetChangedHoneycomb(F)V

    .line 670
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mParentListener:Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

    if-eqz v0, :cond_1

    .line 671
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mParentListener:Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;->onHeightOffsetChanged(F)V

    .line 673
    :cond_1
    return-void
.end method

.method public restoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "inState"    # Landroid/os/Bundle;

    .prologue
    .line 162
    sget-object v1, Lcom/google/android/finsky/layout/BillingAddress;->KEY_ADDRESS_SPEC:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    .line 164
    .local v0, "addressSpec":Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    if-eqz v0, :cond_0

    .line 165
    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    .line 166
    sget-object v1, Lcom/google/android/finsky/layout/BillingAddress;->KEY_SELECTED_COUNTRY:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    iput-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .line 167
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/layout/BillingAddress;->setAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;)V

    .line 168
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v1, p1}, Lcom/android/i18n/addressinput/AddressWidget;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 170
    :cond_0
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 154
    sget-object v0, Lcom/google/android/finsky/layout/BillingAddress;->KEY_ADDRESS_SPEC:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 155
    sget-object v0, Lcom/google/android/finsky/layout/BillingAddress;->KEY_SELECTED_COUNTRY:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v0, p1}, Lcom/android/i18n/addressinput/AddressWidget;->saveInstanceState(Landroid/os/Bundle;)V

    .line 159
    :cond_0
    return-void
.end method

.method public setAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;)V
    .locals 1
    .param p1, "selectedCountry"    # Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .param p2, "spec"    # Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    .prologue
    .line 250
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/BillingAddress;->setAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;Lcom/google/android/finsky/protos/BillingAddress$Address;)V

    .line 251
    return-void
.end method

.method public setAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;Lcom/google/android/finsky/protos/BillingAddress$Address;)V
    .locals 14
    .param p1, "selectedCountry"    # Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .param p2, "spec"    # Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    .param p3, "savedAddress"    # Lcom/google/android/finsky/protos/BillingAddress$Address;

    .prologue
    .line 263
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinnerSelectionSet:Z

    if-nez v1, :cond_3

    .line 266
    const/4 v13, -0x1

    .line 267
    .local v13, "selectedPosition":I
    const/4 v12, 0x0

    .line 268
    .local v12, "position":I
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .line 269
    .local v7, "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    iget-object v1, p1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryCode:Ljava/lang/String;

    iget-object v2, v7, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 270
    move v13, v12

    .line 272
    :cond_0
    add-int/lit8 v12, v12, 0x1

    .line 273
    goto :goto_0

    .line 274
    .end local v7    # "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    :cond_1
    if-ltz v13, :cond_2

    .line 275
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    invoke-virtual {v1, v13}, Landroid/widget/Spinner;->setSelection(I)V

    .line 276
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinnerSelectionSet:Z

    .line 278
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 281
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v12    # "position":I
    .end local v13    # "selectedPosition":I
    :cond_3
    iput-object p1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .line 282
    move-object/from16 v0, p2

    iput-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    .line 283
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    array-length v1, v1

    if-nez v1, :cond_4

    .line 284
    move-object/from16 v0, p2

    iget v1, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->billingAddressType:I

    invoke-static {v1}, Lcom/google/android/finsky/layout/BillingAddress;->populatedRequiredFieldsFromAddressType(I)[I

    move-result-object v1

    move-object/from16 v0, p2

    iput-object v1, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    .line 286
    :cond_4
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    invoke-static {v1}, Lcom/google/android/finsky/layout/BillingAddress;->optionsFromInputFieldList([I)Lcom/android/i18n/addressinput/FormOptions;

    move-result-object v4

    .line 287
    .local v4, "addressFormOptions":Lcom/android/i18n/addressinput/FormOptions;
    const/4 v8, 0x1

    .line 288
    .local v8, "hideAddrPhone":Z
    const/4 v9, 0x1

    .line 289
    .local v9, "hideAddrPostalCountry":Z
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    array-length v1, v1

    if-ge v10, v1, :cond_5

    .line 290
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    aget v1, v1, v10

    packed-switch v1, :pswitch_data_0

    .line 289
    :goto_2
    :pswitch_0
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 292
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_2

    .line 295
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 296
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_2

    .line 299
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 300
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_2

    .line 303
    :pswitch_4
    const/4 v8, 0x0

    .line 304
    goto :goto_2

    .line 306
    :pswitch_5
    const/4 v9, 0x0

    .line 307
    goto :goto_2

    .line 313
    :cond_5
    if-eqz v8, :cond_6

    .line 314
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 316
    :cond_6
    if-eqz v9, :cond_7

    .line 317
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 320
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    if-nez v1, :cond_8

    .line 321
    new-instance v1, Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressPlaceholder:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    new-instance v5, Lcom/google/android/finsky/billing/AddressMetadataCacheManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCache()Lcom/android/volley/Cache;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/finsky/billing/AddressMetadataCacheManager;-><init>(Lcom/android/volley/Cache;)V

    iget-object v6, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    iget-object v6, v6, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryCode:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lcom/android/i18n/addressinput/AddressWidget;-><init>(Landroid/content/Context;Lcom/google/android/finsky/layout/AddressFieldsLayout;Lcom/android/i18n/addressinput/FormOptions;Lcom/android/i18n/addressinput/ClientCacheManager;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    .line 326
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v1}, Lcom/android/i18n/addressinput/AddressWidget;->renderForm()V

    .line 328
    :cond_8
    if-eqz p3, :cond_e

    .line 329
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 330
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 332
    :cond_9
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->firstName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 333
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->firstName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 335
    :cond_a
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->lastName:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 336
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->lastName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 338
    :cond_b
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->email:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 339
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->email:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 341
    :cond_c
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 342
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 344
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-static/range {p3 .. p3}, Lcom/google/android/finsky/billing/BillingUtils;->addressDataFromInstrumentAddress(Lcom/google/android/finsky/protos/BillingAddress$Address;)Lcom/android/i18n/addressinput/AddressData;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/AddressWidget;->renderFormWithSavedAddress(Lcom/android/i18n/addressinput/AddressData;)V

    .line 347
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v1, v4}, Lcom/android/i18n/addressinput/AddressWidget;->setFormOptions(Lcom/android/i18n/addressinput/FormOptions;)V

    .line 348
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    iget-object v2, v2, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/AddressWidget;->updateWidgetOnCountryChange(Ljava/lang/String;)V

    .line 349
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressPlaceholder:Lcom/google/android/finsky/layout/AddressFieldsLayout;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->setOnHeightOffsetChangedListener(Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;)V

    .line 350
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mWhitelistedCountries:Lcom/google/android/finsky/placesapi/WhitelistedCountriesFlagParser;

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    iget-object v2, v2, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/placesapi/WhitelistedCountriesFlagParser;->isCountryEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 351
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSuggestionProvider:Lcom/google/android/finsky/layout/BillingAddress$AddressSuggestionProviderImpl;

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSelectedCountry:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    iget-object v2, v2, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/BillingAddress$AddressSuggestionProviderImpl;->setCountry(Ljava/lang/String;)V

    .line 352
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    iget-object v2, p0, Lcom/google/android/finsky/layout/BillingAddress;->mSuggestionProvider:Lcom/google/android/finsky/layout/BillingAddress$AddressSuggestionProviderImpl;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/AddressWidget;->setSuggestionProvider(Lcom/google/android/finsky/layout/AddressSuggestionProvider;)V

    .line 356
    :goto_3
    return-void

    .line 354
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/AddressWidget;->setSuggestionProvider(Lcom/google/android/finsky/layout/AddressSuggestionProvider;)V

    goto :goto_3

    .line 290
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public setBillingCountries(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p1, "countries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;>;"
    iput-object p1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountries:Ljava/util/List;

    .line 211
    const v3, 0x7f0a00ed

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/BillingAddress;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    .line 212
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f0c0000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 213
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 214
    new-instance v1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/BillingAddress;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x1090008

    invoke-direct {v1, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 217
    .local v1, "countrySpinnerAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/google/android/finsky/layout/BillingAddress$CountrySpinnerItem;>;"
    const v3, 0x1090009

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 219
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountries:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .line 220
    .local v0, "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    new-instance v3, Lcom/google/android/finsky/layout/BillingAddress$CountrySpinnerItem;

    invoke-direct {v3, v0}, Lcom/google/android/finsky/layout/BillingAddress$CountrySpinnerItem;-><init>(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;)V

    invoke-virtual {v1, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_0

    .line 222
    .end local v0    # "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    invoke-virtual {v3, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 223
    iget-object v3, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/google/android/finsky/layout/BillingAddress$1;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/layout/BillingAddress$1;-><init>(Lcom/google/android/finsky/layout/BillingAddress;)V

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 241
    return-void
.end method

.method public setBillingCountryChangeListener(Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountryChangeListener:Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;

    .line 178
    return-void
.end method

.method public setDefaultName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 131
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mFirstName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mLastName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mEmailAddress:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mCountrySpinner:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v0, p1}, Lcom/android/i18n/addressinput/AddressWidget;->setEnabled(Z)V

    .line 136
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 137
    return-void
.end method

.method public setNameInputHint(I)V
    .locals 1
    .param p1, "nameHintId"    # I

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mNameEntry:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(I)V

    .line 191
    return-void
.end method

.method public setOnHeightOffsetChangedListener(Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

    .prologue
    .line 662
    iput-object p1, p0, Lcom/google/android/finsky/layout/BillingAddress;->mParentListener:Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

    .line 663
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 2
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 199
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/layout/BillingAddress;->mPhoneNumber:Landroid/widget/EditText;

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 202
    :cond_0
    return-void
.end method
