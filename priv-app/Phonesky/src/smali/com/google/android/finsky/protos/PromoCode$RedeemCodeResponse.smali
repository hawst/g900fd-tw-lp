.class public final Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PromoCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PromoCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RedeemCodeResponse"
.end annotation


# instance fields
.field public addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

.field public consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

.field public doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public errorMessageHtml:Ljava/lang/String;

.field public hasErrorMessageHtml:Z

.field public hasResult:Z

.field public hasServerLogsCookie:Z

.field public hasStoredValueInstrumentId:Z

.field public hasToken:Z

.field public paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

.field public redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

.field public redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

.field public result:I

.field public serverLogsCookie:[B

.field public storedValueInstrumentId:Ljava/lang/String;

.field public token:Ljava/lang/String;

.field public userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 397
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 398
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->clear()Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    .line 399
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 402
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->result:I

    .line 403
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasResult:Z

    .line 404
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->token:Ljava/lang/String;

    .line 405
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasToken:Z

    .line 406
    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    .line 407
    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 408
    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    .line 409
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->errorMessageHtml:Ljava/lang/String;

    .line 410
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasErrorMessageHtml:Z

    .line 411
    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    .line 412
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->storedValueInstrumentId:Ljava/lang/String;

    .line 413
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasStoredValueInstrumentId:Z

    .line 414
    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 415
    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 416
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->serverLogsCookie:[B

    .line 417
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasServerLogsCookie:Z

    .line 418
    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    .line 419
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->cachedSize:I

    .line 420
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 467
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 468
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->result:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasResult:Z

    if-eqz v1, :cond_1

    .line 469
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->result:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 472
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasToken:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->token:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 473
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->token:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 476
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    if-eqz v1, :cond_4

    .line 477
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 480
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-eqz v1, :cond_5

    .line 481
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 484
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    if-eqz v1, :cond_6

    .line 485
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 488
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasErrorMessageHtml:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->errorMessageHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 489
    :cond_7
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->errorMessageHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 492
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v1, :cond_9

    .line 493
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 496
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasStoredValueInstrumentId:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->storedValueInstrumentId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 497
    :cond_a
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->storedValueInstrumentId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 500
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_c

    .line 501
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 504
    :cond_c
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v1, :cond_d

    .line 505
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 508
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasServerLogsCookie:Z

    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_f

    .line 509
    :cond_e
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 512
    :cond_f
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    if-eqz v1, :cond_10

    .line 513
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 516
    :cond_10
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 524
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 525
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 529
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 530
    :sswitch_0
    return-object p0

    .line 535
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 536
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 542
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->result:I

    .line 543
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasResult:Z

    goto :goto_0

    .line 549
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->token:Ljava/lang/String;

    .line 550
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasToken:Z

    goto :goto_0

    .line 554
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    if-nez v2, :cond_1

    .line 555
    new-instance v2, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    .line 557
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 561
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-nez v2, :cond_2

    .line 562
    new-instance v2, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 564
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 568
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    if-nez v2, :cond_3

    .line 569
    new-instance v2, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    .line 571
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 575
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->errorMessageHtml:Ljava/lang/String;

    .line 576
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasErrorMessageHtml:Z

    goto :goto_0

    .line 580
    :sswitch_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-nez v2, :cond_4

    .line 581
    new-instance v2, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    .line 583
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 587
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->storedValueInstrumentId:Ljava/lang/String;

    .line 588
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasStoredValueInstrumentId:Z

    goto :goto_0

    .line 592
    :sswitch_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v2, :cond_5

    .line 593
    new-instance v2, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 595
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 599
    :sswitch_a
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v2, :cond_6

    .line 600
    new-instance v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 602
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 606
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->serverLogsCookie:[B

    .line 607
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasServerLogsCookie:Z

    goto/16 :goto_0

    .line 611
    :sswitch_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    if-nez v2, :cond_7

    .line 612
    new-instance v2, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    .line 614
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 525
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
    .end sparse-switch

    .line 536
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 426
    iget v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->result:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasResult:Z

    if-eqz v0, :cond_1

    .line 427
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->result:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 429
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasToken:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->token:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 430
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->token:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 432
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    if-eqz v0, :cond_4

    .line 433
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->userConfirmationChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 435
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-eqz v0, :cond_5

    .line 436
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 438
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    if-eqz v0, :cond_6

    .line 439
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redemptionSuccess:Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 441
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasErrorMessageHtml:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->errorMessageHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 442
    :cond_7
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->errorMessageHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 444
    :cond_8
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v0, :cond_9

    .line 445
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 447
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasStoredValueInstrumentId:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->storedValueInstrumentId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 448
    :cond_a
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->storedValueInstrumentId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 450
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_c

    .line 451
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->consumptionAppDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 453
    :cond_c
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_d

    .line 454
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 456
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->hasServerLogsCookie:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_f

    .line 457
    :cond_e
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 459
    :cond_f
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    if-eqz v0, :cond_10

    .line 460
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedeemCodeResponse;->paymentsIntegratorContext:Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 462
    :cond_10
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 463
    return-void
.end method
