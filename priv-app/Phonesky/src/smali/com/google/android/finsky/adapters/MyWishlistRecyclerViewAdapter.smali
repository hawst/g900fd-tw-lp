.class public Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;
.super Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;
.source "MyWishlistRecyclerViewAdapter.java"

# interfaces
.implements Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;


# instance fields
.field private mDismissedDocIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/DfeList;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "loader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p5, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p7, "dfeList"    # Lcom/google/android/finsky/api/model/DfeList;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p9, "isRestoring"    # Z

    .prologue
    .line 32
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v10, p9

    move-object/from16 v13, p8

    invoke-direct/range {v0 .. v13}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/ContainerList;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 36
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mDismissedDocIds:Ljava/util/Set;

    .line 37
    return-void
.end method


# virtual methods
.method protected isDismissed(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 2
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mDismissedDocIds:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public onDataChanged()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mDismissedDocIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 42
    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->onDataChanged()V

    .line 43
    return-void
.end method

.method public onWishlistStatusChanged(Ljava/lang/String;ZZ)V
    .locals 2
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "isInWishlist"    # Z
    .param p3, "isCommitted"    # Z

    .prologue
    .line 47
    if-nez p3, :cond_2

    .line 51
    if-eqz p2, :cond_1

    .line 52
    iget-object v1, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mDismissedDocIds:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 56
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->notifyDataSetChanged()V

    .line 72
    :cond_0
    :goto_1
    return-void

    .line 54
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mDismissedDocIds:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 61
    :cond_2
    if-nez p2, :cond_0

    .line 62
    iget-object v1, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mDismissedDocIds:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 63
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v1, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 64
    iget-object v1, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 65
    iget-object v1, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/api/model/ContainerList;->removeItem(I)V

    .line 69
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V

    goto :goto_1

    .line 63
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
