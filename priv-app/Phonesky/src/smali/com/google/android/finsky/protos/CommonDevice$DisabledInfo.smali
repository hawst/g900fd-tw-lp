.class public final Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DisabledInfo"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;


# instance fields
.field public disabledMessageHtml:Ljava/lang/String;

.field public disabledReason:I

.field public errorMessage:Ljava/lang/String;

.field public hasDisabledMessageHtml:Z

.field public hasDisabledReason:Z

.field public hasErrorMessage:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 519
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 520
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->clear()Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    .line 521
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    .locals 2

    .prologue
    .line 496
    sget-object v0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    if-nez v0, :cond_1

    .line 497
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 499
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    if-nez v0, :cond_0

    .line 500
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    sput-object v0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    .line 502
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    return-object v0

    .line 502
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 524
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledReason:I

    .line 525
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasDisabledReason:Z

    .line 526
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledMessageHtml:Ljava/lang/String;

    .line 527
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasDisabledMessageHtml:Z

    .line 528
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->errorMessage:Ljava/lang/String;

    .line 529
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasErrorMessage:Z

    .line 530
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->cachedSize:I

    .line 531
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 551
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 552
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledReason:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasDisabledReason:Z

    if-eqz v1, :cond_1

    .line 553
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledReason:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 556
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasDisabledMessageHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledMessageHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 557
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledMessageHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 560
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasErrorMessage:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->errorMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 561
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->errorMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 564
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 572
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 573
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 577
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 578
    :sswitch_0
    return-object p0

    .line 583
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 584
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 599
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledReason:I

    .line 600
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasDisabledReason:Z

    goto :goto_0

    .line 606
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledMessageHtml:Ljava/lang/String;

    .line 607
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasDisabledMessageHtml:Z

    goto :goto_0

    .line 611
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->errorMessage:Ljava/lang/String;

    .line 612
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasErrorMessage:Z

    goto :goto_0

    .line 573
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    .line 584
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 490
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 537
    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledReason:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasDisabledReason:Z

    if-eqz v0, :cond_1

    .line 538
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledReason:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 540
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasDisabledMessageHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledMessageHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 541
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledMessageHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 543
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->hasErrorMessage:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->errorMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 544
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->errorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 546
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 547
    return-void
.end method
