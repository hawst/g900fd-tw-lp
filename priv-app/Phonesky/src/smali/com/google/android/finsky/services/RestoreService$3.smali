.class Lcom/google/android/finsky/services/RestoreService$3;
.super Ljava/lang/Object;
.source "RestoreService.java"

# interfaces
.implements Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/RestoreService;->startBitmapDownload(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/RestoreService;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1798
    iput-object p1, p0, Lcom/google/android/finsky/services/RestoreService$3;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iput-object p2, p0, Lcom/google/android/finsky/services/RestoreService$3;->val$packageName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 6
    .param p1, "container"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1801
    invoke-virtual {p1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1802
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 1803
    const-string v1, "Received appIcon for %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/services/RestoreService$3;->val$packageName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1805
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$3;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mBitmapContainers:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/finsky/services/RestoreService;->access$3200(Lcom/google/android/finsky/services/RestoreService;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$3;->val$packageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1806
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$3;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$3;->val$packageName:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->deliverBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    invoke-static {v1, v2, v0}, Lcom/google/android/finsky/services/RestoreService;->access$3300(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 1818
    :goto_0
    return-void

    .line 1809
    :cond_0
    const-string v1, "Unable to download appIcon for %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/services/RestoreService$3;->val$packageName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1810
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$3;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v1}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$3;->val$packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->tryAgainBitmap(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1812
    const-string v1, "Unable to download appIcon for %s"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/services/RestoreService$3;->val$packageName:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1813
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$3;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$3;->val$packageName:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->cancelBitmapDownload(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/finsky/services/RestoreService;->access$1800(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;)V

    goto :goto_0

    .line 1815
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$3;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$3;->val$packageName:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->cancelBitmapDownload(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/google/android/finsky/services/RestoreService;->access$1800(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1798
    check-cast p1, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/services/RestoreService$3;->onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    return-void
.end method
