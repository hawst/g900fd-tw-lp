.class Lcom/google/android/finsky/services/DailyHygiene$1;
.super Ljava/lang/Object;
.source "DailyHygiene.java"

# interfaces
.implements Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/DailyHygiene;->beginSelfUpdateCheck()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/DailyHygiene;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

.field final synthetic val$eventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field final synthetic val$installedVersion:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/DailyHygiene;Lcom/google/android/finsky/analytics/PlayStore$AppData;Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->this$0:Lcom/google/android/finsky/services/DailyHygiene;

    iput-object p2, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-object p3, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$eventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iput-object p4, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$accountName:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$installedVersion:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 7
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$eventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x77

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->this$0:Lcom/google/android/finsky/services/DailyHygiene;

    # invokes: Lcom/google/android/finsky/services/DailyHygiene;->loadAndReplicateAndContinue()V
    invoke-static {v0}, Lcom/google/android/finsky/services/DailyHygiene;->access$200(Lcom/google/android/finsky/services/DailyHygiene;)V

    .line 186
    return-void
.end method

.method public onResponse(Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;)V
    .locals 10
    .param p1, "response"    # Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 157
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getSelfUpdateScheduler()Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    move-result-object v8

    .line 158
    .local v8, "selfUpdateScheduler":Lcom/google/android/finsky/utils/SelfUpdateScheduler;
    invoke-virtual {v8, p1}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->getNewVersion(Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;)I

    move-result v7

    .line 159
    .local v7, "newVersion":I
    if-lez v7, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput v7, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 161
    iget-object v0, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v9, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$eventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x77

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$appData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$accountName:Ljava/lang/String;

    invoke-virtual {v8, v7, v0}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->checkForSelfUpdate(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 167
    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneHoldoffDuringSelfUpdate:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->val$installedVersion:I

    # invokes: Lcom/google/android/finsky/services/DailyHygiene;->emergencyDailyHygiene(I)Z
    invoke-static {v0}, Lcom/google/android/finsky/services/DailyHygiene;->access$000(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 170
    :cond_1
    const-string v0, "Self-update started or running - defer daily hygiene"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->this$0:Lcom/google/android/finsky/services/DailyHygiene;

    # invokes: Lcom/google/android/finsky/services/DailyHygiene;->reschedule(Z)V
    invoke-static {v0, v9}, Lcom/google/android/finsky/services/DailyHygiene;->access$100(Lcom/google/android/finsky/services/DailyHygiene;Z)V

    .line 177
    :goto_0
    return-void

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/services/DailyHygiene$1;->this$0:Lcom/google/android/finsky/services/DailyHygiene;

    # invokes: Lcom/google/android/finsky/services/DailyHygiene;->loadAndReplicateAndContinue()V
    invoke-static {v0}, Lcom/google/android/finsky/services/DailyHygiene;->access$200(Lcom/google/android/finsky/services/DailyHygiene;)V

    goto :goto_0
.end method
