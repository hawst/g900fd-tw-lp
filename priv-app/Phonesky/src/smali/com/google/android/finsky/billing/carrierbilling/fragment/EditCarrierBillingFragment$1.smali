.class Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$1;
.super Ljava/lang/Object;
.source "EditCarrierBillingFragment.java"

# interfaces
.implements Lcom/android/i18n/addressinput/AddressWidget$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->showAddressEditView(Landroid/view/View;Lcom/android/i18n/addressinput/AddressData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;)V
    .locals 0

    .prologue
    .line 251
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$1;->this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onInitialized()V
    .locals 3

    .prologue
    .line 254
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$1;->this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "highlight_address"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 256
    .local v0, "displayErrorList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    .line 257
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$1;->this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$1;->this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    # invokes: Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getFormErrors(Ljava/util/ArrayList;)Ljava/util/Collection;
    invoke-static {v2, v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->access$000(Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;Ljava/util/ArrayList;)Ljava/util/Collection;

    move-result-object v2

    # invokes: Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->displayErrors(Ljava/util/Collection;)V
    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->access$100(Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;Ljava/util/Collection;)V

    .line 259
    :cond_0
    return-void
.end method
