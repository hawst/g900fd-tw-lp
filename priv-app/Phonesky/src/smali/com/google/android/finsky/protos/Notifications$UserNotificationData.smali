.class public final Lcom/google/android/finsky/protos/Notifications$UserNotificationData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Notifications.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Notifications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserNotificationData"
.end annotation


# instance fields
.field public dialogText:Ljava/lang/String;

.field public dialogTitle:Ljava/lang/String;

.field public hasDialogText:Z

.field public hasDialogTitle:Z

.field public hasNotificationText:Z

.field public hasNotificationTitle:Z

.field public hasTickerText:Z

.field public notificationText:Ljava/lang/String;

.field public notificationTitle:Ljava/lang/String;

.field public tickerText:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 358
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 359
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->clear()Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    .line 360
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Notifications$UserNotificationData;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 363
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationTitle:Ljava/lang/String;

    .line 364
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasNotificationTitle:Z

    .line 365
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationText:Ljava/lang/String;

    .line 366
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasNotificationText:Z

    .line 367
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->tickerText:Ljava/lang/String;

    .line 368
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasTickerText:Z

    .line 369
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogTitle:Ljava/lang/String;

    .line 370
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasDialogTitle:Z

    .line 371
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogText:Ljava/lang/String;

    .line 372
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasDialogText:Z

    .line 373
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->cachedSize:I

    .line 374
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 400
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 401
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasNotificationTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 402
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 405
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasNotificationText:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 406
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 409
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasTickerText:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->tickerText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 410
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->tickerText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 413
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasDialogTitle:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 414
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 417
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasDialogText:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 418
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 421
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Notifications$UserNotificationData;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 429
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 430
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 434
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 435
    :sswitch_0
    return-object p0

    .line 440
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationTitle:Ljava/lang/String;

    .line 441
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasNotificationTitle:Z

    goto :goto_0

    .line 445
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationText:Ljava/lang/String;

    .line 446
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasNotificationText:Z

    goto :goto_0

    .line 450
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->tickerText:Ljava/lang/String;

    .line 451
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasTickerText:Z

    goto :goto_0

    .line 455
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogTitle:Ljava/lang/String;

    .line 456
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasDialogTitle:Z

    goto :goto_0

    .line 460
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogText:Ljava/lang/String;

    .line 461
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasDialogText:Z

    goto :goto_0

    .line 430
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 321
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Notifications$UserNotificationData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 380
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasNotificationTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 383
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasNotificationText:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 384
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->notificationText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 386
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasTickerText:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->tickerText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 387
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->tickerText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 389
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasDialogTitle:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 390
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 392
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->hasDialogText:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 393
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Notifications$UserNotificationData;->dialogText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 395
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 396
    return-void
.end method
