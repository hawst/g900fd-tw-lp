.class Lcom/google/android/finsky/services/RestoreService$1;
.super Ljava/lang/Object;
.source "RestoreService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/RestoreService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mLoaded:I

.field final synthetic this$0:Lcom/google/android/finsky/services/RestoreService;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/RestoreService;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1159
    iput-object p1, p0, Lcom/google/android/finsky/services/RestoreService$1;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iput-object p2, p0, Lcom/google/android/finsky/services/RestoreService$1;->val$intent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1163
    iget v1, p0, Lcom/google/android/finsky/services/RestoreService$1;->mLoaded:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/finsky/services/RestoreService$1;->mLoaded:I

    .line 1164
    iget v1, p0, Lcom/google/android/finsky/services/RestoreService$1;->mLoaded:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 1165
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$1;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v1}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v1

    # operator-- for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mStartupRefCount:I
    invoke-static {v1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$1910(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)I

    .line 1166
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$1;->this$0:Lcom/google/android/finsky/services/RestoreService;

    iget-object v2, p0, Lcom/google/android/finsky/services/RestoreService$1;->val$intent:Landroid/content/Intent;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->handleIntent(Landroid/content/Intent;)Z
    invoke-static {v1, v2}, Lcom/google/android/finsky/services/RestoreService;->access$2000(Lcom/google/android/finsky/services/RestoreService;Landroid/content/Intent;)Z

    move-result v0

    .line 1167
    .local v0, "keepRunning":Z
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$1;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->addInstallerListener()V
    invoke-static {v1}, Lcom/google/android/finsky/services/RestoreService;->access$2100(Lcom/google/android/finsky/services/RestoreService;)V

    .line 1168
    if-nez v0, :cond_0

    .line 1170
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService$1;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v1}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->stopServiceIfDone()V

    .line 1173
    .end local v0    # "keepRunning":Z
    :cond_0
    return-void
.end method
