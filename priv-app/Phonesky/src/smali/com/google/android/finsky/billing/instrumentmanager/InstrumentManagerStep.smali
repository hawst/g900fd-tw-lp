.class public abstract Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "InstrumentManagerStep.java"

# interfaces
.implements Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;",
        ">",
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<TT;>;",
        "Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment$ResultListener;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    .local p0, "this":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;, "Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep<TT;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    return-void
.end method

.method protected static createArgs(Ljava/lang/String;Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;)Landroid/os/Bundle;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "tokens"    # Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    .prologue
    .line 34
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 35
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string v1, "InstrumentManagerStep.tokens"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 37
    return-object v0
.end method


# virtual methods
.method public allowButtonBar()Z
    .locals 1

    .prologue
    .line 97
    .local p0, "this":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;, "Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 102
    .local p0, "this":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;, "Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep<TT;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected isSuccess(I)Z
    .locals 3
    .param p1, "resultCode"    # I

    .prologue
    .line 81
    .local p0, "this":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;, "Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep<TT;>;"
    packed-switch p1, :pswitch_data_0

    .line 89
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected InstrumentManager resultCode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :pswitch_0
    const/4 v0, 0x1

    .line 87
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onContinueButtonClicked()V
    .locals 0

    .prologue
    .line 108
    .local p0, "this":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;, "Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep<TT;>;"
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    .local p0, "this":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;, "Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep<TT;>;"
    const v0, 0x7f0400b9

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 75
    .local p0, "this":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;, "Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep<TT;>;"
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->setEventListener(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;)V

    .line 77
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onDestroy()V

    .line 78
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .local p0, "this":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;, "Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep<TT;>;"
    const v8, 0x7f0a022a

    .line 48
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 52
    .local v0, "args":Landroid/os/Bundle;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    const-string v7, "authAccount"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    .line 54
    .local v1, "eventLogger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    new-instance v3, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;

    invoke-direct {v3, p0, v1}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;-><init>(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/analytics/FinskyEventLog;)V

    .line 55
    .local v3, "logger":Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerLogger;
    invoke-static {v3}, Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventDispatcher;->setEventListener(Lcom/google/android/wallet/instrumentmanager/pub/analytics/InstrumentManagerAnalyticsEventListener;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v2

    .line 59
    .local v2, "fragment":Landroid/support/v4/app/Fragment;
    if-nez v2, :cond_0

    .line 60
    const-string v6, "InstrumentManagerStep.tokens"

    invoke-static {v0, v6}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    .line 63
    .local v5, "tokens":Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;
    const v4, 0x7f0d01bd

    .line 64
    .local v4, "themeResourceId":I
    iget-object v6, v5, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->commonToken:[B

    iget-object v7, v5, Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;->instrumentToken:[B

    invoke-static {v6, v7, v4}, Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;->newInstance([B[BI)Lcom/google/android/wallet/instrumentmanager/pub/InstrumentManagerFragment;

    move-result-object v2

    .line 66
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v6

    invoke-virtual {v6, v8, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 70
    .end local v4    # "themeResourceId":I
    .end local v5    # "tokens":Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;
    :cond_0
    return-void
.end method
