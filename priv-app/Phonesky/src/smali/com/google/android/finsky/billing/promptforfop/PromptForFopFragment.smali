.class public Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;
.super Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
.source "PromptForFopFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;
    }
.end annotation


# instance fields
.field private mActionEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mActionsView:Landroid/view/ViewGroup;

.field private mContinueButton:Landroid/view/View;

.field private mHasLoggedScreen:Z

.field private mMainView:Landroid/view/ViewGroup;

.field private mMoreActionsView:Landroid/view/ViewGroup;

.field private mMoreToggler:Lcom/google/android/finsky/layout/SeparatorLinearLayout;

.field private mMoreTogglerTitle:Landroid/widget/TextView;

.field private mMoreVisible:Z

.field private final mNoneOnClickListener:Landroid/view/View$OnClickListener;

.field private mSelectedIndex:I

.field private mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mUiMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;-><init>()V

    .line 134
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mSelectedIndex:I

    .line 146
    new-instance v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$1;-><init>(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mNoneOnClickListener:Landroid/view/View$OnClickListener;

    .line 683
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->notifyListenerOnNoneSelected()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->toggleMore()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->showMoreDetailsDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;
    .param p1, "x1"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->selectItem(I)V

    return-void
.end method

.method private addActionEntry(Landroid/view/ViewGroup;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;IZ)V
    .locals 11
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "actionEntry"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .param p3, "i"    # I
    .param p4, "isLast"    # Z

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    .line 505
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 506
    .local v2, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getActionEntryLayout()I

    move-result v6

    invoke-virtual {v2, v6, p1, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 507
    .local v5, "view":Landroid/view/View;
    const v6, 0x7f0a009c

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 508
    .local v4, "titleView":Landroid/widget/TextView;
    iget-object v6, p2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->displayTitle:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 510
    const v6, 0x7f0a00f9

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/image/FifeImageView;

    .line 511
    .local v1, "imageView":Lcom/google/android/play/image/FifeImageView;
    iget-object v0, p2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 512
    .local v0, "image":Lcom/google/android/finsky/protos/Common$Image;
    if-nez v0, :cond_2

    .line 513
    invoke-virtual {v1, v10}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 521
    :goto_0
    const v6, 0x7f0a00a0

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 522
    .local v3, "radio":Landroid/widget/RadioButton;
    iget v6, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    const/4 v7, 0x1

    if-eq v6, v7, :cond_0

    iget v6, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    const/4 v7, 0x6

    if-ne v6, v7, :cond_3

    .line 524
    :cond_0
    invoke-virtual {v3, p3}, Landroid/widget/RadioButton;->setId(I)V

    .line 525
    invoke-virtual {v3, v9}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 526
    invoke-virtual {v3, v9}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 528
    new-instance v6, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$4;

    invoke-direct {v6, p0, p3}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$4;-><init>(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;I)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 540
    :goto_1
    if-eqz p4, :cond_1

    instance-of v6, v5, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    if-eqz v6, :cond_1

    move-object v6, v5

    .line 541
    check-cast v6, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->hideSeparator()V

    .line 543
    :cond_1
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 544
    return-void

    .line 515
    .end local v3    # "radio":Landroid/widget/RadioButton;
    :cond_2
    iget-object v6, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v7, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v8

    invoke-virtual {v1, v6, v7, v8}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    goto :goto_0

    .line 535
    .restart local v3    # "radio":Landroid/widget/RadioButton;
    :cond_3
    invoke-virtual {v3, v10}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 537
    iget-object v6, p2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->action:Landroid/view/View$OnClickListener;

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public static buildArgumentsBundle(Landroid/accounts/Account;[B)Landroid/os/Bundle;
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "serverLogsCookie"    # [B

    .prologue
    .line 163
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 164
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "BillingProfileFragment.account"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 165
    const-string v1, "PromptForFopFragment.server_logs_cookie"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 166
    return-object v0
.end method

.method private createNoneEntry()Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .locals 4

    .prologue
    .line 437
    new-instance v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    const v1, 0x7f0c011e

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iget-object v2, v2, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v3, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mNoneOnClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private getListener()Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;
    .locals 2

    .prologue
    .line 571
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;

    if-eqz v0, :cond_0

    .line 572
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;

    .line 575
    :goto_0
    return-object v0

    .line 574
    :cond_0
    const-string v0, "No listener registered."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 575
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Landroid/accounts/Account;[B)Landroid/support/v4/app/Fragment;
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "serverLogsCookie"    # [B

    .prologue
    .line 156
    invoke-static {p0, p1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->buildArgumentsBundle(Landroid/accounts/Account;[B)Landroid/os/Bundle;

    move-result-object v0

    .line 157
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;-><init>()V

    .line 158
    .local v1, "result":Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->setArguments(Landroid/os/Bundle;)V

    .line 159
    return-object v1
.end method

.method private notifyListenerOnAlreadySetup()V
    .locals 1

    .prologue
    .line 601
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getListener()Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;

    move-result-object v0

    .line 602
    .local v0, "listener":Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;
    if-eqz v0, :cond_0

    .line 603
    invoke-interface {v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;->onAlreadySetup()V

    .line 605
    :cond_0
    return-void
.end method

.method private notifyListenerOnFatalError(Ljava/lang/String;)V
    .locals 1
    .param p1, "errorMessageHtml"    # Ljava/lang/String;

    .prologue
    .line 587
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getListener()Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;

    move-result-object v0

    .line 588
    .local v0, "listener":Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;
    if-eqz v0, :cond_0

    .line 589
    invoke-interface {v0, p1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;->onFatalError(Ljava/lang/String;)V

    .line 591
    :cond_0
    return-void
.end method

.method private notifyListenerOnInstrumentCreated(Ljava/lang/String;)V
    .locals 1
    .param p1, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 580
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getListener()Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;

    move-result-object v0

    .line 581
    .local v0, "listener":Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;
    if-eqz v0, :cond_0

    .line 582
    invoke-interface {v0, p1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;->onInstrumentCreated(Ljava/lang/String;)V

    .line 584
    :cond_0
    return-void
.end method

.method private notifyListenerOnNoneSelected()V
    .locals 1

    .prologue
    .line 594
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getListener()Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;

    move-result-object v0

    .line 595
    .local v0, "listener":Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;
    if-eqz v0, :cond_0

    .line 596
    invoke-interface {v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$Listener;->onNoneSelected()V

    .line 598
    :cond_0
    return-void
.end method

.method private selectItem(I)V
    .locals 4
    .param p1, "selected"    # I

    .prologue
    const/4 v3, 0x1

    .line 554
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mActionEntries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 555
    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 556
    .local v1, "radio":Landroid/widget/RadioButton;
    if-ne p1, v0, :cond_0

    move v2, v3

    :goto_1
    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 554
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 556
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 558
    .end local v1    # "radio":Landroid/widget/RadioButton;
    :cond_1
    iput p1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mSelectedIndex:I

    .line 559
    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mContinueButton:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 560
    return-void
.end method

.method private showMoreDetailsDialog()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 327
    new-instance v6, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 328
    .local v6, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v0, 0x7f0c0121

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c02a0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c00a5

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x3f2

    iget-object v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mAccount:Landroid/accounts/Account;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 334
    invoke-virtual {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v7

    .line 340
    .local v7, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCancelable(Z)V

    .line 341
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "PromptForFopFragment.more_details_dialog"

    invoke-virtual {v7, v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 342
    return-void
.end method

.method private syncMoreActionsVisibility()V
    .locals 3

    .prologue
    const/16 v0, 0x8

    .line 426
    iget v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 427
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreActionsView:Landroid/view/ViewGroup;

    iget-boolean v2, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreVisible:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 431
    :goto_0
    return-void

    .line 429
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private toggleMore()V
    .locals 4

    .prologue
    const v3, 0x7f020103

    const/4 v1, 0x0

    .line 402
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreVisible:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreVisible:Z

    .line 403
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreVisible:Z

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreTogglerTitle:Landroid/widget/TextView;

    const v2, 0x7f0200e0

    invoke-virtual {v0, v3, v1, v2, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 409
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreTogglerTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 411
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreToggler:Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->showSeparator()V

    .line 422
    :goto_1
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->syncMoreActionsVisibility()V

    .line 423
    return-void

    :cond_0
    move v0, v1

    .line 402
    goto :goto_0

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreTogglerTitle:Landroid/widget/TextView;

    const v2, 0x7f0200e2

    invoke-virtual {v0, v3, v1, v2, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 418
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreTogglerTitle:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 420
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreToggler:Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->hideSeparator()V

    goto :goto_1
.end method


# virtual methods
.method protected configureContinueButtonStyle(Landroid/view/View;)V
    .locals 5
    .param p1, "button"    # Landroid/view/View;

    .prologue
    .line 312
    instance-of v1, p1, Lcom/google/android/finsky/layout/IconButtonGroup;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 313
    check-cast v0, Lcom/google/android/finsky/layout/IconButtonGroup;

    .line 314
    .local v0, "iconButton":Lcom/google/android/finsky/layout/IconButtonGroup;
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/IconButtonGroup;->setBackendForLabel(I)V

    .line 315
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0201a2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/IconButtonGroup;->setIconDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 316
    const v1, 0x7f0c020d

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/IconButtonGroup;->setText(Ljava/lang/String;)V

    .line 320
    .end local v0    # "iconButton":Lcom/google/android/finsky/layout/IconButtonGroup;
    :goto_0
    return-void

    .line 318
    :cond_0
    const-string v1, "Unexpected continue button type: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected determineUiMode()I
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 187
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getExperiments(Ljava/lang/String;)Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v0

    .line 188
    .local v0, "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    const-string v2, "cl:billing.prompt_for_fop_ui_mode_radio_button"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 189
    const/4 v1, 0x1

    .line 204
    :cond_0
    :goto_0
    return v1

    .line 190
    :cond_1
    const-string v2, "cl:billing.prompt_for_fop_ui_mode_billing_profile_nested"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 192
    const/4 v1, 0x3

    goto :goto_0

    .line 193
    :cond_2
    const-string v2, "cl:billing.prompt_for_fop_ui_mode_billing_profile_more_details"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 195
    const/4 v1, 0x4

    goto :goto_0

    .line 196
    :cond_3
    const-string v2, "cl:billing.prompt_for_fop_ui_mode_billing_profile_not_now"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 198
    const/4 v1, 0x5

    goto :goto_0

    .line 199
    :cond_4
    const-string v2, "cl:billing.prompt_for_fop_ui_mode_billing_profile"

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 203
    const-string v2, "No UI mode specified, defaulting to UI_MODE_BILLING_PROFILE"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected getActionEntryLayout()I
    .locals 1

    .prologue
    .line 547
    const v0, 0x7f040168

    return v0
.end method

.method protected getBackgroundEventServerLogsCookie()[B
    .locals 2

    .prologue
    .line 665
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "PromptForFopFragment.server_logs_cookie"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method protected getBillingProfileRequestEnum()I
    .locals 1

    .prologue
    .line 634
    const/4 v0, 0x2

    return v0
.end method

.method protected getCreditCardEventType()I
    .locals 1

    .prologue
    .line 609
    const/16 v0, 0x15e

    return v0
.end method

.method protected getDcbEventType()I
    .locals 1

    .prologue
    .line 614
    const/16 v0, 0x15f

    return v0
.end method

.method protected getGenericInstrumentEventType()I
    .locals 1

    .prologue
    .line 629
    const/16 v0, 0x165

    return v0
.end method

.method protected getMainLayout()I
    .locals 1

    .prologue
    .line 308
    const v0, 0x7f040166

    return v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 677
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 672
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 183
    const/16 v0, 0x3ea

    return v0
.end method

.method protected getPrimerStringId()I
    .locals 1

    .prologue
    .line 323
    const v0, 0x7f0c011c

    return v0
.end method

.method protected getRedeemEventType()I
    .locals 1

    .prologue
    .line 619
    const/16 v0, 0x160

    return v0
.end method

.method protected getTopupEventType()I
    .locals 1

    .prologue
    .line 624
    const/16 v0, 0x161

    return v0
.end method

.method protected logLoading()V
    .locals 5

    .prologue
    .line 640
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v4, 0xd5

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 642
    return-void
.end method

.method protected logScreen()V
    .locals 4

    .prologue
    .line 648
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mHasLoggedScreen:Z

    if-eqz v0, :cond_0

    .line 654
    :goto_0
    return-void

    .line 651
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mHasLoggedScreen:Z

    .line 653
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 565
    iget v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mSelectedIndex:I

    if-ltz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mActionEntries:Ljava/util/List;

    iget v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mSelectedIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    iget-object v0, v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->action:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 568
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 171
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->determineUiMode()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    .line 173
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getPlayStoreUiElementType()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 174
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "PromptForFopFragment.server_logs_cookie"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 175
    .local v0, "serverLogsCookie":[B
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 176
    if-eqz p1, :cond_0

    .line 177
    const-string v1, "PromptForFopFragment.more_visible"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreVisible:Z

    .line 178
    const-string v1, "PromptForFopFragment.has_logged_screen"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mHasLoggedScreen:Z

    .line 180
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 211
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getMainLayout()I

    move-result v13

    const/4 v14, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v13, v1, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    .line 213
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a02a4

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 214
    .local v4, "actionsContainer":Landroid/view/View;
    const v13, 0x7f0a0108

    invoke-virtual {v4, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mActionsView:Landroid/view/ViewGroup;

    .line 216
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a009c

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 217
    .local v12, "titleView":Landroid/widget/TextView;
    if-eqz v12, :cond_0

    .line 218
    const v13, 0x7f0c011b

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 221
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a022b

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 222
    .local v2, "accountView":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 223
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mAccount:Landroid/accounts/Account;

    iget-object v13, v13, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 227
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a031d

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 228
    .local v10, "primerView":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getPrimerStringId()I

    move-result v13

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v13

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a0109

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mProgressIndicator:Landroid/view/View;

    .line 231
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a0104

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mProfileView:Landroid/view/View;

    .line 233
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v13

    invoke-static {v13}, Lcom/google/android/finsky/utils/SetupWizardUtils;->getNavBarIfPossible(Landroid/app/Activity;)Lcom/google/android/finsky/setup/SetupWizardNavBar;

    move-result-object v11

    .line 234
    .local v11, "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    if-eqz v11, :cond_3

    .line 235
    invoke-virtual {v11}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mContinueButton:Landroid/view/View;

    .line 239
    :goto_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mContinueButton:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mContinueButton:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->configureContinueButtonStyle(Landroid/view/View;)V

    .line 241
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mContinueButton:Landroid/view/View;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setEnabled(Z)V

    .line 243
    const v13, 0x7f0a031f

    invoke-virtual {v4, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreToggler:Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    .line 244
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreToggler:Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->hideSeparator()V

    .line 245
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreToggler:Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    const v14, 0x7f0a0320

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreTogglerTitle:Landroid/widget/TextView;

    .line 246
    const v13, 0x7f0a031e

    invoke-virtual {v4, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreActionsView:Landroid/view/ViewGroup;

    .line 247
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreToggler:Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    new-instance v14, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$2;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$2;-><init>(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;)V

    invoke-virtual {v13, v14}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 257
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a031b

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 258
    .local v9, "notNowButton":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a031c

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 259
    .local v6, "moreDetailsButton":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    packed-switch v13, :pswitch_data_0

    .line 295
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a0230

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 296
    .local v5, "continueButtonBar":Landroid/view/View;
    if-eqz v5, :cond_2

    .line 297
    const/16 v13, 0x8

    invoke-virtual {v5, v13}, Landroid/view/View;->setVisibility(I)V

    .line 299
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0b015d

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 301
    .local v3, "actionsBottomPadding":I
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v4, v13, v14, v15, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 304
    .end local v3    # "actionsBottomPadding":I
    .end local v5    # "continueButtonBar":Landroid/view/View;
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    return-object v13

    .line 237
    .end local v6    # "moreDetailsButton":Landroid/widget/TextView;
    .end local v9    # "notNowButton":Landroid/widget/TextView;
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMainView:Landroid/view/ViewGroup;

    const v14, 0x7f0a0231

    invoke-virtual {v13, v14}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mContinueButton:Landroid/view/View;

    goto/16 :goto_0

    .line 261
    .restart local v6    # "moreDetailsButton":Landroid/widget/TextView;
    .restart local v9    # "notNowButton":Landroid/widget/TextView;
    :pswitch_1
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 262
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mNoneOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    const v13, 0x7f0c011d

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 267
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mContinueButton:Landroid/view/View;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 268
    const/4 v13, 0x0

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 269
    new-instance v13, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$3;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment$3;-><init>(Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;)V

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 280
    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    const/4 v14, 0x4

    if-ne v13, v14, :cond_4

    .line 281
    const v8, 0x7f0c0120

    .line 282
    .local v8, "moreDetailsButtonLabel":I
    const v7, 0x7f020102

    .line 287
    .local v7, "moreDetailsButtonIcon":I
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-virtual {v6, v7, v13, v14, v15}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1

    .line 284
    .end local v7    # "moreDetailsButtonIcon":I
    .end local v8    # "moreDetailsButtonLabel":I
    :cond_4
    const v8, 0x7f0c00a5

    .line 285
    .restart local v8    # "moreDetailsButtonLabel":I
    const v7, 0x7f02010c

    .restart local v7    # "moreDetailsButtonIcon":I
    goto :goto_2

    .line 259
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method protected onFatalError(Ljava/lang/String;)V
    .locals 0
    .param p1, "errorMessageHtml"    # Ljava/lang/String;

    .prologue
    .line 475
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->notifyListenerOnFatalError(Ljava/lang/String;)V

    .line 476
    return-void
.end method

.method protected onInstrumentCreated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "instrumentId"    # Ljava/lang/String;
    .param p2, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 459
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->notifyListenerOnInstrumentCreated(Ljava/lang/String;)V

    .line 460
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 489
    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mNoneOnClickListener:Landroid/view/View$OnClickListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 495
    :goto_0
    return-void

    .line 494
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onNegativeClick(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 480
    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    .line 485
    :goto_0
    return-void

    .line 484
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onPositiveClick(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected onPromoCodeRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)V
    .locals 2
    .param p1, "redeemCodeResult"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .prologue
    .line 470
    const-string v0, "Unexpected promo code redemption."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 471
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 357
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 358
    const-string v0, "PromptForFopFragment.selected_index"

    iget v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mSelectedIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 359
    const-string v0, "PromptForFopFragment.more_visible"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreVisible:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 360
    const-string v0, "PromptForFopFragment.has_logged_screen"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mHasLoggedScreen:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 361
    return-void
.end method

.method protected onStoredValueAdded(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "existingStoredValueInstrumentId"    # Ljava/lang/String;
    .param p2, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 465
    invoke-direct {p0, p2}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->notifyListenerOnInstrumentCreated(Ljava/lang/String;)V

    .line 466
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 346
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 347
    if-eqz p2, :cond_0

    .line 348
    const-string v0, "PromptForFopFragment.selected_index"

    iget v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mSelectedIndex:I

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mSelectedIndex:I

    .line 350
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mSelectedIndex:I

    if-ltz v0, :cond_1

    .line 351
    iget v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mSelectedIndex:I

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->selectItem(I)V

    .line 353
    :cond_1
    return-void
.end method

.method protected redeemCheckoutCode()V
    .locals 3

    .prologue
    .line 658
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->createIntent(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 660
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 661
    return-void
.end method

.method protected renderActions(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "actionEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;>;"
    const/4 v11, 0x6

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 365
    iput-object p1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mActionEntries:Ljava/util/List;

    .line 366
    iget-object v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 367
    iget-object v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreActionsView:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 368
    iget-object v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreToggler:Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    const/16 v8, 0x8

    invoke-virtual {v5, v8}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->setVisibility(I)V

    .line 371
    iget v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    if-eq v5, v9, :cond_0

    iget v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    if-eq v5, v10, :cond_0

    iget v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    if-ne v5, v11, :cond_1

    .line 373
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->createNoneEntry()Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    move-result-object v5

    invoke-interface {p1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    :cond_1
    iget v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    if-ne v5, v10, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    const/4 v8, 0x4

    if-lt v5, v8, :cond_4

    move v1, v6

    .line 380
    .local v1, "enableMoreSection":Z
    :goto_0
    if-eqz v1, :cond_2

    .line 381
    iget-object v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreToggler:Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    invoke-virtual {v5, v7}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->setVisibility(I)V

    .line 384
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mActionsView:Landroid/view/ViewGroup;

    .line 385
    .local v4, "parent":Landroid/view/ViewGroup;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 386
    .local v0, "actionEntryCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_6

    .line 387
    if-eqz v1, :cond_3

    if-lt v2, v9, :cond_3

    .line 389
    iget-object v4, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mMoreActionsView:Landroid/view/ViewGroup;

    .line 391
    :cond_3
    add-int/lit8 v5, v0, -0x1

    if-ne v2, v5, :cond_5

    move v3, v6

    .line 392
    .local v3, "isLast":Z
    :goto_2
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    invoke-direct {p0, v4, v5, v2, v3}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->addActionEntry(Landroid/view/ViewGroup;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;IZ)V

    .line 386
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .end local v0    # "actionEntryCount":I
    .end local v1    # "enableMoreSection":Z
    .end local v2    # "i":I
    .end local v3    # "isLast":Z
    .end local v4    # "parent":Landroid/view/ViewGroup;
    :cond_4
    move v1, v7

    .line 377
    goto :goto_0

    .restart local v0    # "actionEntryCount":I
    .restart local v1    # "enableMoreSection":Z
    .restart local v2    # "i":I
    .restart local v4    # "parent":Landroid/view/ViewGroup;
    :cond_5
    move v3, v7

    .line 391
    goto :goto_2

    .line 394
    :cond_6
    iget v5, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->mUiMode:I

    if-ne v5, v11, :cond_7

    .line 396
    invoke-direct {p0, v7}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->selectItem(I)V

    .line 398
    :cond_7
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->syncMoreActionsVisibility()V

    .line 399
    return-void
.end method

.method protected renderInstruments([Lcom/google/android/finsky/protos/CommonDevice$Instrument;)V
    .locals 0
    .param p1, "instruments"    # [Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .prologue
    .line 455
    return-void
.end method

.method protected shouldRender([Lcom/google/android/finsky/protos/CommonDevice$Instrument;)Z
    .locals 3
    .param p1, "instruments"    # [Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .prologue
    const/4 v0, 0x0

    .line 445
    array-length v1, p1

    if-lez v1, :cond_0

    .line 446
    const-string v1, "Unexpected instruments found."

    new-array v2, v0, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 447
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->notifyListenerOnAlreadySetup()V

    .line 450
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
