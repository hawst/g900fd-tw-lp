.class public Lcom/google/android/finsky/layout/RottenTomatoesMeter;
.super Landroid/view/View;
.source "RottenTomatoesMeter.java"


# instance fields
.field private final mAccentFillColor:I

.field private final mBackgroundFillColor:I

.field private mCurrentPercentValue:I

.field private final mFillPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/RottenTomatoesMeter;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 34
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f090054

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mBackgroundFillColor:I

    .line 35
    const v1, 0x7f090062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mAccentFillColor:I

    .line 37
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mFillPaint:Landroid/graphics/Paint;

    .line 38
    iget-object v1, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mFillPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 39
    iget-object v1, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mFillPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 41
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->setWillNotDraw(Z)V

    .line 42
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->getWidth()I

    move-result v11

    .line 54
    .local v11, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->getHeight()I

    move-result v10

    .line 56
    .local v10, "height":I
    iget v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mCurrentPercentValue:I

    mul-int/2addr v0, v11

    div-int/lit8 v9, v0, 0x64

    .line 57
    .local v9, "filledWidth":I
    if-lez v9, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mFillPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mAccentFillColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 59
    int-to-float v3, v9

    int-to-float v4, v10

    iget-object v5, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mFillPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 62
    :cond_0
    sub-int v8, v11, v9

    .line 63
    .local v8, "backgroundWidth":I
    if-lez v8, :cond_1

    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mFillPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mBackgroundFillColor:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 65
    sub-int v0, v11, v8

    int-to-float v3, v0

    int-to-float v5, v11

    int-to-float v6, v10

    iget-object v7, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mFillPaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 67
    :cond_1
    return-void
.end method

.method public setPercentValue(I)V
    .locals 1
    .param p1, "percentValue"    # I

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mCurrentPercentValue:I

    if-eq v0, p1, :cond_0

    .line 46
    iput p1, p0, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->mCurrentPercentValue:I

    .line 47
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/RottenTomatoesMeter;->invalidate()V

    .line 49
    :cond_0
    return-void
.end method
