.class public Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
.super Lcom/google/android/finsky/library/LibraryEntry;
.source "LibrarySubscriptionEntry.java"


# instance fields
.field public final initiationTimestampMs:J

.field public final isAutoRenewing:Z

.field public final trialUntilTimestampMs:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJLjava/lang/Long;JJZ)V
    .locals 15
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "libraryId"    # Ljava/lang/String;
    .param p3, "backend"    # I
    .param p4, "docId"    # Ljava/lang/String;
    .param p5, "offerType"    # I
    .param p6, "documentHash"    # J
    .param p8, "validUntilTimestampMs"    # Ljava/lang/Long;
    .param p9, "initiationTimestampMs"    # J
    .param p11, "trialUntilTimestampMs"    # J
    .param p13, "autoRenewing"    # Z

    .prologue
    .line 23
    const/16 v8, 0xf

    invoke-virtual/range {p8 .. p8}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v9, p5

    move-wide/from16 v10, p6

    invoke-direct/range {v3 .. v13}, Lcom/google/android/finsky/library/LibraryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IIJJ)V

    .line 25
    move-wide/from16 v0, p9

    iput-wide v0, p0, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->initiationTimestampMs:J

    .line 26
    move-wide/from16 v0, p11

    iput-wide v0, p0, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->trialUntilTimestampMs:J

    .line 27
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->isAutoRenewing:Z

    .line 28
    return-void
.end method
