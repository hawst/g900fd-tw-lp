.class public Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;
.super Lcom/google/android/finsky/fragments/LoggingFragment;
.source "ChallengeErrorFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;
    }
.end annotation


# instance fields
.field private mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

.field private mMainView:Landroid/view/View;

.field private mSubmitButton:Lcom/google/android/play/layout/PlayActionButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;-><init>()V

    .line 45
    return-void
.end method

.method private getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;
    .locals 2

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;

    .line 132
    :goto_0
    return-object v0

    .line 128
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;

    if-eqz v0, :cond_1

    .line 129
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;

    goto :goto_0

    .line 131
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;

    if-eqz v0, :cond_2

    .line 132
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;

    goto :goto_0

    .line 134
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No listener registered."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static newInstance(Ljava/lang/String;ILcom/google/android/finsky/protos/ChallengeProto$ChallengeError;)Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "backend"    # I
    .param p2, "challengeError"    # Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    .prologue
    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 61
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    const-string v2, "ChallengeErrorFragment.backend"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 63
    const-string v2, "ChallengeErrorFragment.challenge"

    invoke-static {p2}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 64
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;-><init>()V

    .line 65
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->setArguments(Landroid/os/Bundle;)V

    .line 66
    return-object v1
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 139
    const/16 v0, 0x57e

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 144
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mSubmitButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v1, :cond_3

    .line 145
    const/16 v1, 0x57f

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->logClickEvent(I)V

    .line 146
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-boolean v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionFailChallenge:Z

    if-eqz v1, :cond_1

    .line 147
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;->onFail()V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    array-length v1, v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 151
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionChallenge:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    const/4 v2, 0x0

    aget-object v0, v1, v2

    .line 152
    .local v0, "challenge":Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;->onStartChallenge(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    goto :goto_0

    .line 154
    .end local v0    # "challenge":Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected submit button action."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 156
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v1, :cond_0

    .line 157
    const/16 v1, 0x581

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->logClickEvent(I)V

    .line 158
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-boolean v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->actionFailChallenge:Z

    if-eqz v1, :cond_4

    .line 159
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getListener()Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment$Listener;->onFail()V

    goto :goto_0

    .line 161
    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unexpected cancel button action."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/LoggingFragment;->onCreate(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ChallengeErrorFragment.challenge"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    .line 73
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v5, 0x8

    .line 78
    const v3, 0x7f04001e

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mMainView:Landroid/view/View;

    .line 81
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mMainView:Landroid/view/View;

    const v4, 0x7f0a009c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 82
    .local v2, "title":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mMainView:Landroid/view/View;

    const v4, 0x7f0a00c3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 89
    .local v1, "message":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->descriptionHtml:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 90
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->descriptionHtml:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "ChallengeErrorFragment.backend"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 96
    .local v0, "backend":I
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mMainView:Landroid/view/View;

    const v4, 0x7f0a00f8

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mSubmitButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 97
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 99
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mSubmitButton:Lcom/google/android/play/layout/PlayActionButton;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->submitButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-virtual {v3, v0, v4, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mMainView:Landroid/view/View;

    const v4, 0x7f0a0115

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 104
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 106
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->cancelButton:Lcom/google/android/finsky/protos/ChallengeProto$FormButton;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$FormButton;->label:Ljava/lang/String;

    invoke-virtual {v3, v0, v4, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 111
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mMainView:Landroid/view/View;

    return-object v3

    .line 85
    .end local v0    # "backend":I
    .end local v1    # "message":Landroid/widget/TextView;
    :cond_0
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "No title returned."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 92
    .restart local v1    # "message":Landroid/widget/TextView;
    :cond_1
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 101
    .restart local v0    # "backend":I
    :cond_2
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "No submit button returned."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 108
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mCancelButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v3, v5}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    goto :goto_1
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 116
    invoke-super {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;->onResume()V

    .line 120
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mMainView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 121
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mChallengeError:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    iget-object v1, v1, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;->title:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/ageverification/ChallengeErrorFragment;->mMainView:Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 122
    return-void
.end method
