.class public Lcom/google/android/finsky/utils/SetupWizardUtils;
.super Ljava/lang/Object;
.source "SetupWizardUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;,
        Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;
    }
.end annotation


# static fields
.field public static final ILLUSTRATION_URL_PAYMENT:Ljava/lang/String;

.field public static final ILLUSTRATION_URL_PAYMENT_WIDE:Ljava/lang/String;

.field public static final ILLUSTRATION_URL_RESTORE:Ljava/lang/String;

.field public static final ILLUSTRATION_URL_RESTORE_WIDE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/finsky/config/G;->setupWizardPaymentHeaderGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/utils/SetupWizardUtils;->ILLUSTRATION_URL_PAYMENT:Ljava/lang/String;

    .line 44
    sget-object v0, Lcom/google/android/finsky/config/G;->setupWizardPaymentHeaderWideGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/utils/SetupWizardUtils;->ILLUSTRATION_URL_PAYMENT_WIDE:Ljava/lang/String;

    .line 46
    sget-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreHeaderGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/utils/SetupWizardUtils;->ILLUSTRATION_URL_RESTORE:Ljava/lang/String;

    .line 48
    sget-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreWideHeaderGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/utils/SetupWizardUtils;->ILLUSTRATION_URL_RESTORE_WIDE:Ljava/lang/String;

    return-void
.end method

.method public static animateSliding(Landroid/app/Activity;Z)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "slideBackwards"    # Z

    .prologue
    .line 101
    invoke-static {}, Lcom/google/android/finsky/utils/SetupWizardUtils;->shouldUseMaterialTheme()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 104
    :cond_0
    if-eqz p1, :cond_1

    .line 105
    const v0, 0x7f050015

    const v1, 0x7f050016

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0

    .line 107
    :cond_1
    const v0, 0x7f050018

    const v1, 0x7f050019

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method private static configureBasicMaterialUi(Landroid/app/Activity;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;IZZZ)V
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "params"    # Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;
    .param p2, "context"    # I
    .param p3, "backAllowed"    # Z
    .param p4, "collapseHeaderIfPossible"    # Z
    .param p5, "hasKeyboard"    # Z

    .prologue
    .line 155
    invoke-static {p0}, Lcom/google/android/finsky/utils/SetupWizardUtils;->getNavBarIfPossible(Landroid/app/Activity;)Lcom/google/android/finsky/setup/SetupWizardNavBar;

    move-result-object v1

    .line 156
    .local v1, "navigationBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    invoke-virtual {p1}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->useImmersiveMode()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->setUseImmersiveMode(Z)V

    .line 158
    invoke-virtual {v1}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getBackButton()Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 160
    if-eqz p5, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->useImmersiveMode()Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/finsky/config/G;->setupWizardForceResizeForKeyboardInFullscreen:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 163
    invoke-static {p0}, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->forceSizeChanges(Landroid/app/Activity;)V

    .line 166
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0010

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 169
    .local v2, "useWideImage":Z
    if-nez p2, :cond_2

    .line 170
    if-eqz v2, :cond_1

    sget-object v0, Lcom/google/android/finsky/utils/SetupWizardUtils;->ILLUSTRATION_URL_PAYMENT_WIDE:Ljava/lang/String;

    .line 176
    .local v0, "imageUrl":Ljava/lang/String;
    :goto_0
    const v3, 0x7f0a038e

    invoke-virtual {p0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/SetupWizardIllustration;

    invoke-virtual {v3, v0, p4}, Lcom/google/android/finsky/layout/SetupWizardIllustration;->configure(Ljava/lang/String;Z)V

    .line 178
    return-void

    .line 170
    .end local v0    # "imageUrl":Ljava/lang/String;
    :cond_1
    sget-object v0, Lcom/google/android/finsky/utils/SetupWizardUtils;->ILLUSTRATION_URL_PAYMENT:Ljava/lang/String;

    goto :goto_0

    .line 171
    :cond_2
    const/4 v3, 0x1

    if-ne p2, v3, :cond_4

    .line 172
    if-eqz v2, :cond_3

    sget-object v0, Lcom/google/android/finsky/utils/SetupWizardUtils;->ILLUSTRATION_URL_RESTORE_WIDE:Ljava/lang/String;

    .restart local v0    # "imageUrl":Ljava/lang/String;
    :goto_1
    goto :goto_0

    .end local v0    # "imageUrl":Ljava/lang/String;
    :cond_3
    sget-object v0, Lcom/google/android/finsky/utils/SetupWizardUtils;->ILLUSTRATION_URL_RESTORE:Ljava/lang/String;

    goto :goto_1

    .line 174
    :cond_4
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown context: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static configureBasicUi(Landroid/app/Activity;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;IZZZ)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "params"    # Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;
    .param p2, "context"    # I
    .param p3, "backAllowed"    # Z
    .param p4, "collapseHeaderIfPossible"    # Z
    .param p5, "hasKeyboard"    # Z

    .prologue
    .line 126
    invoke-static {}, Lcom/google/android/finsky/utils/SetupWizardUtils;->shouldUseMaterialTheme()Z

    move-result v1

    .line 128
    .local v1, "useMaterialTheme":Z
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 129
    .local v0, "decorView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    .line 130
    .local v2, "visibility":I
    if-nez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isInitialSetup()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 131
    const/high16 v3, 0x2370000

    or-int/2addr v2, v3

    .line 137
    const/4 p3, 0x0

    .line 139
    :cond_0
    if-eqz p3, :cond_2

    .line 140
    const v3, -0x400001

    and-int/2addr v2, v3

    .line 144
    :goto_0
    invoke-virtual {v0, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 146
    if-eqz v1, :cond_1

    .line 147
    invoke-static/range {p0 .. p5}, Lcom/google/android/finsky/utils/SetupWizardUtils;->configureBasicMaterialUi(Landroid/app/Activity;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;IZZZ)V

    .line 150
    :cond_1
    return-void

    .line 142
    :cond_2
    const/high16 v3, 0x400000

    or-int/2addr v2, v3

    goto :goto_0
.end method

.method public static getNavBarIfPossible(Landroid/app/Activity;)Lcom/google/android/finsky/setup/SetupWizardNavBar;
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 81
    invoke-static {}, Lcom/google/android/finsky/utils/SetupWizardUtils;->shouldUseMaterialTheme()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a038d

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/setup/SetupWizardNavBar;

    goto :goto_0
.end method

.method public static shouldUseMaterialTheme()Z
    .locals 2

    .prologue
    .line 76
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
