.class public final Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PromoCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PromoCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserConfirmationChallenge"
.end annotation


# instance fields
.field public buttonLabel:Ljava/lang/String;

.field public footerHtml:Ljava/lang/String;

.field public formattedPrice:Ljava/lang/String;

.field public hasButtonLabel:Z

.field public hasFooterHtml:Z

.field public hasFormattedPrice:Z

.field public hasMessageHtml:Z

.field public hasPriceBylineHtml:Z

.field public hasTitle:Z

.field public hasTitleBylineHtml:Z

.field public messageHtml:Ljava/lang/String;

.field public priceBylineHtml:Ljava/lang/String;

.field public thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

.field public title:Ljava/lang/String;

.field public titleBylineHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 681
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 682
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->clear()Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    .line 683
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 686
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->title:Ljava/lang/String;

    .line 687
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasTitle:Z

    .line 688
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->titleBylineHtml:Ljava/lang/String;

    .line 689
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasTitleBylineHtml:Z

    .line 690
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->formattedPrice:Ljava/lang/String;

    .line 691
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasFormattedPrice:Z

    .line 692
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->priceBylineHtml:Ljava/lang/String;

    .line 693
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasPriceBylineHtml:Z

    .line 694
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->buttonLabel:Ljava/lang/String;

    .line 695
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasButtonLabel:Z

    .line 696
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 697
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->messageHtml:Ljava/lang/String;

    .line 698
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasMessageHtml:Z

    .line 699
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->footerHtml:Ljava/lang/String;

    .line 700
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasFooterHtml:Z

    .line 701
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->cachedSize:I

    .line 702
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 737
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 738
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasTitle:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 739
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 742
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasTitleBylineHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->titleBylineHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 743
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->titleBylineHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 746
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasFormattedPrice:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->formattedPrice:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 747
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->formattedPrice:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 750
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasPriceBylineHtml:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->priceBylineHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 751
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->priceBylineHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 754
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasButtonLabel:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->buttonLabel:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 755
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->buttonLabel:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 758
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_a

    .line 759
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 762
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasMessageHtml:Z

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->messageHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 763
    :cond_b
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->messageHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 766
    :cond_c
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasFooterHtml:Z

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->footerHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 767
    :cond_d
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->footerHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 770
    :cond_e
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 778
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 779
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 783
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 784
    :sswitch_0
    return-object p0

    .line 789
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->title:Ljava/lang/String;

    .line 790
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasTitle:Z

    goto :goto_0

    .line 794
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->titleBylineHtml:Ljava/lang/String;

    .line 795
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasTitleBylineHtml:Z

    goto :goto_0

    .line 799
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->formattedPrice:Ljava/lang/String;

    .line 800
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasFormattedPrice:Z

    goto :goto_0

    .line 804
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->priceBylineHtml:Ljava/lang/String;

    .line 805
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasPriceBylineHtml:Z

    goto :goto_0

    .line 809
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->buttonLabel:Ljava/lang/String;

    .line 810
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasButtonLabel:Z

    goto :goto_0

    .line 814
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_1

    .line 815
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 817
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 821
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->messageHtml:Ljava/lang/String;

    .line 822
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasMessageHtml:Z

    goto :goto_0

    .line 826
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->footerHtml:Ljava/lang/String;

    .line 827
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasFooterHtml:Z

    goto :goto_0

    .line 779
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 633
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 708
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasTitle:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 709
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 711
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasTitleBylineHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->titleBylineHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 712
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->titleBylineHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 714
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasFormattedPrice:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->formattedPrice:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 715
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->formattedPrice:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 717
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasPriceBylineHtml:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->priceBylineHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 718
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->priceBylineHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 720
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasButtonLabel:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->buttonLabel:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 721
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->buttonLabel:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 723
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_a

    .line 724
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 726
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasMessageHtml:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->messageHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 727
    :cond_b
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->messageHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 729
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->hasFooterHtml:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->footerHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 730
    :cond_d
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->footerHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 732
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 733
    return-void
.end method
