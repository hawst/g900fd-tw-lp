.class public Lcom/google/android/finsky/activities/FlagItemDialog;
.super Lcom/google/android/finsky/activities/AuthenticatedActivity;
.source "FlagItemDialog.java"

# interfaces
.implements Lcom/google/android/finsky/fragments/PageFragmentHost;


# instance fields
.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;-><init>()V

    .line 28
    new-instance v0, Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/FakeNavigationManager;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/FlagItemDialog;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-void
.end method

.method public static show(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/FlagItemDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    const/high16 v1, 0x20010000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 34
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 35
    return-void
.end method


# virtual methods
.method public getActionBarController()Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v0

    return-object v0
.end method

.method public getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p1, "dfeAccount"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    return-object v0
.end method

.method public getNavigationManager()Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPeopleClient()Lcom/google/android/gms/people/PeopleClient;
    .locals 1

    .prologue
    .line 113
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public goBack()V
    .locals 0

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemDialog;->finish()V

    .line 85
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f0a00c4

    .line 39
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const v4, 0x7f040083

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/activities/FlagItemDialog;->setContentView(I)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemDialog;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 44
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "url"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 46
    .local v3, "url":Ljava/lang/String;
    new-instance v4, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v5, p0, Lcom/google/android/finsky/activities/FlagItemDialog;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-direct {v4, v5, p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/support/v7/app/ActionBarActivity;)V

    iput-object v4, p0, Lcom/google/android/finsky/activities/FlagItemDialog;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .line 48
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemDialog;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 55
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-static {v3}, Lcom/google/android/finsky/activities/FlagItemFragment;->newInstance(Ljava/lang/String;)Lcom/google/android/finsky/activities/FlagItemFragment;

    move-result-object v0

    .line 52
    .local v0, "fragment":Lcom/google/android/finsky/activities/FlagItemFragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemDialog;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 53
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {v1, v6, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 54
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 104
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemDialog;->finish()V

    .line 106
    const/4 v0, 0x1

    .line 108
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onReady(Z)V
    .locals 0
    .param p1, "shouldHandleIntent"    # Z

    .prologue
    .line 60
    return-void
.end method

.method public showErrorDialog(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .param p3, "goBack"    # Z

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FlagItemDialog;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/finsky/activities/ErrorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ErrorDialog;

    .line 90
    return-void
.end method

.method public updateBreadcrumb(Ljava/lang/String;)V
    .locals 1
    .param p1, "breadcrumb"    # Ljava/lang/String;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemDialog;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateDefaultTitle(Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method public updateCurrentBackendId(IZ)V
    .locals 1
    .param p1, "backend"    # I
    .param p2, "ignoreActionBarBackground"    # Z

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/finsky/activities/FlagItemDialog;->mActionBarHelper:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateCurrentBackendId(IZ)V

    .line 100
    return-void
.end method
