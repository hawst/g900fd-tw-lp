.class public Lcom/google/android/finsky/billing/iab/InAppBillingManager;
.super Ljava/lang/Object;
.source "InAppBillingManager.java"


# static fields
.field private static final DETAILS_REQUEST_SKU_LIST_MAX_SIZE:I

.field private static final MAX_PURCHASES_IN_RESPONSE:I

.field private static final TIMEOUT_MS:J


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private final mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/finsky/config/G;->iabV3SkuDetailsMaxSize:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->DETAILS_REQUEST_SKU_LIST_MAX_SIZE:I

    .line 147
    sget-object v0, Lcom/google/android/finsky/config/G;->iabV3NetworkTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->TIMEOUT_MS:J

    .line 152
    sget-object v0, Lcom/google/android/finsky/config/G;->iabV3MaxPurchasesInResponse:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->MAX_PURCHASES_IN_RESPONSE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/library/LibraryReplicators;Lcom/google/android/finsky/api/DfeApi;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p3, "libraryReplicators"    # Lcom/google/android/finsky/library/LibraryReplicators;
    .param p4, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 172
    if-nez p1, :cond_0

    .line 173
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_0
    if-nez p2, :cond_1

    .line 176
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "libraries must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_1
    if-nez p3, :cond_2

    .line 179
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "libraryReplicators must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 181
    :cond_2
    invoke-interface {p4}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_3

    .line 182
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "dfeApi must specify an account"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_3
    iput-object p1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mContext:Landroid/content/Context;

    .line 185
    iput-object p2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 186
    iput-object p3, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    .line 187
    iput-object p4, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 188
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/iab/InAppBillingManager;Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->buildDetailsJson(Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/iab/InAppBillingManager;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/iab/InAppBillingManager;)Lcom/google/android/finsky/library/LibraryReplicators;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mLibraryReplicators:Lcom/google/android/finsky/library/LibraryReplicators;

    return-object v0
.end method

.method private buildDetailsJson(Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;)Ljava/lang/String;
    .locals 9
    .param p1, "entry"    # Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;

    .prologue
    const/4 v8, 0x0

    .line 540
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 542
    .local v0, "details":Lorg/json/JSONObject;
    :try_start_0
    iget-object v1, p1, Lcom/google/android/finsky/protos/Details$BulkDetailsEntry;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 543
    .local v1, "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v5, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    const/4 v6, 0x0

    aget-object v4, v5, v6

    .line 545
    .local v4, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget-object v2, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendDocid:Ljava/lang/String;

    .line 546
    .local v2, "docId":Ljava/lang/String;
    const-string v5, "productId"

    invoke-static {v2}, Lcom/google/android/finsky/utils/DocUtils;->extractSkuForInApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 547
    const-string v5, "type"

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getInAppType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 548
    const-string v5, "price"

    iget-object v6, v4, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 549
    iget-object v5, v4, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    array-length v5, v5

    if-lez v5, :cond_0

    .line 551
    const-string v5, "price_amount_micros"

    iget-object v6, v4, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    iget-wide v6, v6, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    invoke-virtual {v0, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 552
    const-string v5, "price_currency_code"

    iget-object v6, v4, Lcom/google/android/finsky/protos/Common$Offer;->convertedPrice:[Lcom/google/android/finsky/protos/Common$Offer;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 559
    :goto_0
    const-string v5, "title"

    iget-object v6, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 560
    const-string v5, "description"

    iget-object v6, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->descriptionHtml:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 564
    .end local v1    # "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v2    # "docId":Ljava/lang/String;
    .end local v4    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 556
    .restart local v1    # "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .restart local v2    # "docId":Ljava/lang/String;
    .restart local v4    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_0
    :try_start_1
    const-string v5, "price_amount_micros"

    iget-wide v6, v4, Lcom/google/android/finsky/protos/Common$Offer;->micros:J

    invoke-virtual {v0, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 557
    const-string v5, "price_currency_code"

    iget-object v6, v4, Lcom/google/android/finsky/protos/Common$Offer;->currencyCode:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 561
    .end local v1    # "doc":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v2    # "docId":Ljava/lang/String;
    .end local v4    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :catch_0
    move-exception v3

    .line 562
    .local v3, "e":Lorg/json/JSONException;
    const-string v5, "Exception when creating json: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private checkBillingApiVersionSupport(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 4
    .param p1, "version"    # I

    .prologue
    .line 425
    const/4 v0, 0x3

    if-lt p1, v0, :cond_0

    const/4 v0, 0x5

    if-le p1, v0, :cond_1

    .line 426
    :cond_0
    const-string v0, "Unsupported billing API version: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 427
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .line 429
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0
.end method

.method private checkBillingEnabled(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 3
    .param p1, "billingApiVersion"    # I

    .prologue
    .line 463
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkBillingApiVersionSupport(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 464
    .local v0, "response":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_0

    .line 472
    .end local v0    # "response":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    :goto_0
    return-object v0

    .line 468
    .restart local v0    # "response":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v1}, Lcom/google/android/finsky/api/DfeApi;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/InAppBillingSetting;->isEnabled(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 469
    const-string v1, "Billing unavailable for this package and user."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 470
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 472
    :cond_1
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0
.end method

.method private checkTypeSupported(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 3
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 438
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 439
    const-string v0, "Input Error: Non empty/null argument expected for type."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 440
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .line 452
    :goto_0
    return-object v0

    .line 442
    :cond_0
    const-string v0, "inapp"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "subs"

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 444
    const-string v0, "Unknown item type specified %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 445
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 447
    :cond_1
    const-string v0, "subs"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/finsky/config/G;->iabV3SubscriptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 449
    const-string v0, "Subscriptions are not supported"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_BILLING_UNAVAILABLE:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 452
    :cond_2
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0
.end method

.method private computeSignatureHash(Landroid/content/pm/PackageInfo;)Ljava/lang/String;
    .locals 3
    .param p1, "packageInfo"    # Landroid/content/pm/PackageInfo;

    .prologue
    .line 628
    iget-object v1, p1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v0

    .line 629
    .local v0, "bytes":[B
    invoke-static {v0}, Lcom/google/android/finsky/utils/Md5Util;->secureHash([B)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private consumeIabPurchase(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "purchaseToken"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v9, 0x0

    .line 728
    new-instance v8, Ljava/util/concurrent/Semaphore;

    invoke-direct {v8, v9}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 729
    .local v8, "semaphore":Ljava/util/concurrent/Semaphore;
    new-array v7, v2, [Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    aput-object v0, v7, v9

    .line 730
    .local v7, "responseCode":[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    new-instance v4, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;

    invoke-direct {v4, p0, v7, v8}, Lcom/google/android/finsky/billing/iab/InAppBillingManager$3;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingManager;[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;Ljava/util/concurrent/Semaphore;)V

    new-instance v5, Lcom/google/android/finsky/billing/iab/InAppBillingManager$4;

    invoke-direct {v5, p0, v7, v8}, Lcom/google/android/finsky/billing/iab/InAppBillingManager$4;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingManager;[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;Ljava/util/concurrent/Semaphore;)V

    move-object v1, p2

    move-object v3, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi;->consumePurchase(Ljava/lang/String;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 766
    :try_start_0
    sget-wide v0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->TIMEOUT_MS:J

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v0, v1, v2}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 767
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 772
    :goto_0
    return-object v0

    .line 769
    :catch_0
    move-exception v6

    .line 770
    .local v6, "e":Ljava/lang/InterruptedException;
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0

    .line 772
    .end local v6    # "e":Ljava/lang/InterruptedException;
    :cond_0
    aget-object v0, v7, v9

    goto :goto_0
.end method

.method private fetchSkuDetails(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "response"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 502
    .local p2, "skuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 503
    .local v1, "docIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eq v7, v0, :cond_0

    .line 504
    invoke-virtual {p2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, p3, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->buildDocidStr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 503
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 506
    :cond_0
    new-instance v8, Ljava/util/concurrent/Semaphore;

    const/4 v0, 0x0

    invoke-direct {v8, v0}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 507
    .local v8, "semaphore":Ljava/util/concurrent/Semaphore;
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    const/4 v3, 0x1

    new-instance v4, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;

    invoke-direct {v4, p0, p4, v8}, Lcom/google/android/finsky/billing/iab/InAppBillingManager$1;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingManager;Landroid/os/Bundle;Ljava/util/concurrent/Semaphore;)V

    new-instance v5, Lcom/google/android/finsky/billing/iab/InAppBillingManager$2;

    invoke-direct {v5, p0, p4, v8}, Lcom/google/android/finsky/billing/iab/InAppBillingManager$2;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingManager;Landroid/os/Bundle;Ljava/util/concurrent/Semaphore;)V

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi;->getDetails(Ljava/util/Collection;Ljava/lang/String;ZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 531
    :try_start_0
    sget-wide v2, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->TIMEOUT_MS:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v2, v3, v0}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 532
    const-string v0, "RESPONSE_CODE"

    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    invoke-virtual {p4, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 537
    :cond_1
    :goto_1
    return-void

    .line 534
    :catch_0
    move-exception v6

    .line 535
    .local v6, "e":Ljava/lang/InterruptedException;
    const-string v0, "RESPONSE_CODE"

    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    invoke-virtual {p4, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method private getInAppType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "docId"    # Ljava/lang/String;

    .prologue
    .line 575
    const-string v0, "inapp:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    const-string v0, "inapp"

    .line 580
    :goto_0
    return-object v0

    .line 577
    :cond_0
    const-string v0, "subs:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 578
    const-string v0, "subs"

    goto :goto_0

    .line 580
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getListAndContinuationToken(Ljava/util/List;Ljava/lang/String;)Landroid/util/Pair;
    .locals 4
    .param p1, "continuationToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 685
    .local p0, "docIdList":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    sget v3, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->MAX_PURCHASES_IN_RESPONSE:I

    if-gt v2, v3, :cond_0

    .line 686
    const/4 v2, 0x0

    invoke-static {p0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    .line 705
    :goto_0
    return-object v2

    .line 688
    :cond_0
    const/4 v1, 0x0

    .line 689
    .local v1, "start":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 691
    invoke-static {p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->tokenToPosition(Ljava/lang/String;)I

    move-result v1

    .line 692
    if-ltz v1, :cond_1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 693
    :cond_1
    const/4 v1, 0x0

    .line 696
    :cond_2
    sget v2, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->MAX_PURCHASES_IN_RESPONSE:I

    add-int v0, v1, v2

    .line 697
    .local v0, "end":I
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 699
    invoke-static {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->positionToToken(I)Ljava/lang/String;

    move-result-object p1

    .line 705
    :goto_1
    invoke-interface {p0, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-static {v2, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    goto :goto_0

    .line 702
    :cond_3
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    .line 703
    const/4 p1, 0x0

    goto :goto_1
.end method

.method private getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 483
    :try_start_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 487
    :goto_0
    return-object v1

    .line 485
    :catch_0
    move-exception v0

    .line 486
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v1, "cannot find package name: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 487
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isDocumentInLibrary(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "itemType"    # Ljava/lang/String;
    .param p2, "itemId"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 568
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v2}, Lcom/google/android/finsky/library/Libraries;->blockingLoad()V

    .line 569
    iget-object v2, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v3, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v3}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v1

    .line 570
    .local v1, "library":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-static {p2, p1, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->buildDocidStr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 571
    .local v0, "docidStr":Ljava/lang/String;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/AccountLibrary;->getInAppEntry(Ljava/lang/String;)Lcom/google/android/finsky/library/LibraryInAppEntry;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private makePurchaseIntent(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 16
    .param p1, "billingApiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p4, "newSku"    # Ljava/lang/String;
    .param p5, "itemType"    # Ljava/lang/String;
    .param p6, "developerPayload"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/app/PendingIntent;"
        }
    .end annotation

    .prologue
    .line 602
    .local p3, "oldSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getPackageInfo(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v14

    .line 603
    .local v14, "packageInfo":Landroid/content/pm/PackageInfo;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->computeSignatureHash(Landroid/content/pm/PackageInfo;)Ljava/lang/String;

    move-result-object v7

    .line 604
    .local v7, "signatureHash":Ljava/lang/String;
    const/4 v9, 0x0

    .line 605
    .local v9, "oldSkusAsDocidStrings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p3, :cond_0

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 606
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "oldSkusAsDocidStrings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v9, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 607
    .restart local v9    # "oldSkusAsDocidStrings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 608
    .local v13, "oldSku":Ljava/lang/String;
    move-object/from16 v0, p5

    move-object/from16 v1, p2

    invoke-static {v13, v0, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->buildDocidStr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v9, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 612
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v13    # "oldSku":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p4

    move-object/from16 v1, p5

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->buildDocidStr(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 613
    .local v12, "newSkuAsDocidStr":Ljava/lang/String;
    new-instance v3, Lcom/google/android/finsky/billing/IabParameters;

    iget v6, v14, Landroid/content/pm/PackageInfo;->versionCode:I

    move/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v8, p6

    invoke-direct/range {v3 .. v9}, Lcom/google/android/finsky/billing/IabParameters;-><init>(ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 615
    .local v3, "iabParameters":Lcom/google/android/finsky/billing/IabParameters;
    invoke-static {}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->builder()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-static {v12, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->buildDocid(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocid(Lcom/google/android/finsky/protos/Common$Docid;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v4

    invoke-virtual {v4, v12}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocidStr(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setOfferType(I)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setIabParameters(Lcom/google/android/finsky/billing/IabParameters;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->build()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    move-result-object v15

    .line 623
    .local v15, "params":Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v4}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    invoke-static {v4, v15}, Lcom/google/android/finsky/billing/lightpurchase/IabV3Activity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;)Landroid/content/Intent;

    move-result-object v11

    .line 624
    .local v11, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v4, v5, v11, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    return-object v4
.end method

.method private performIabPromoCheck(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 776
    new-instance v8, Ljava/util/concurrent/Semaphore;

    invoke-direct {v8, v9}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 777
    .local v8, "semaphore":Ljava/util/concurrent/Semaphore;
    new-array v7, v10, [Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    aput-object v0, v7, v9

    .line 778
    .local v7, "responseCode":[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    const/16 v1, 0xb

    .line 779
    .local v1, "docType":I
    const-string v0, "subs"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 780
    const/16 v1, 0xf

    .line 782
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->getSimIdentifier()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/finsky/billing/iab/InAppBillingManager$5;

    invoke-direct {v4, p0, v7, v8}, Lcom/google/android/finsky/billing/iab/InAppBillingManager$5;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingManager;[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;Ljava/util/concurrent/Semaphore;)V

    new-instance v5, Lcom/google/android/finsky/billing/iab/InAppBillingManager$6;

    invoke-direct {v5, p0, v7, v8}, Lcom/google/android/finsky/billing/iab/InAppBillingManager$6;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingManager;[Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;Ljava/util/concurrent/Semaphore;)V

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi;->checkIabPromo(ILjava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 804
    :try_start_0
    sget-wide v2, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->TIMEOUT_MS:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v2, v3, v0}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 805
    const/4 v0, 0x0

    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    aput-object v2, v7, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 811
    :cond_1
    aget-object v0, v7, v9

    :goto_0
    return-object v0

    .line 807
    :catch_0
    move-exception v6

    .line 808
    .local v6, "e":Ljava/lang/InterruptedException;
    const-string v0, "Interrupted: %s"

    new-array v2, v10, [Ljava/lang/Object;

    invoke-virtual {v6}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 809
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    goto :goto_0
.end method

.method private populatePurchasesForPackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 14
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;
    .param p3, "continuationToken"    # Ljava/lang/String;
    .param p4, "currentPurchases"    # Landroid/os/Bundle;

    .prologue
    .line 638
    iget-object v12, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v13, p0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v13}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v6

    .line 639
    .local v6, "library":Lcom/google/android/finsky/library/AccountLibrary;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 640
    .local v2, "docIdList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 641
    .local v11, "transactionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 642
    .local v9, "signatureList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v12, "inapp"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 643
    invoke-virtual {v6, p1}, Lcom/google/android/finsky/library/AccountLibrary;->getInAppPurchasesForPackage(Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 644
    .local v5, "inAppList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppEntry;>;"
    move-object/from16 v0, p3

    invoke-static {v5, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getListAndContinuationToken(Ljava/util/List;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v7

    .line 646
    .local v7, "listAndToken":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppEntry;>;Ljava/lang/String;>;"
    iget-object v5, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v5    # "inAppList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppEntry;>;"
    check-cast v5, Ljava/util/List;

    .line 647
    .restart local v5    # "inAppList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppEntry;>;"
    iget-object v0, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 p3, v0

    .end local p3    # "continuationToken":Ljava/lang/String;
    check-cast p3, Ljava/lang/String;

    .line 648
    .restart local p3    # "continuationToken":Ljava/lang/String;
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/library/LibraryInAppEntry;

    .line 649
    .local v3, "entry":Lcom/google/android/finsky/library/LibraryInAppEntry;
    invoke-virtual {v3}, Lcom/google/android/finsky/library/LibraryInAppEntry;->getDocId()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/google/android/finsky/utils/DocUtils;->extractSkuForInApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 650
    iget-object v12, v3, Lcom/google/android/finsky/library/LibraryInAppEntry;->signedPurchaseData:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 651
    iget-object v12, v3, Lcom/google/android/finsky/library/LibraryInAppEntry;->signature:Ljava/lang/String;

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 653
    .end local v3    # "entry":Lcom/google/android/finsky/library/LibraryInAppEntry;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "inAppList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppEntry;>;"
    .end local v7    # "listAndToken":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppEntry;>;Ljava/lang/String;>;"
    :cond_0
    const-string v12, "subs"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 654
    invoke-virtual {v6, p1}, Lcom/google/android/finsky/library/AccountLibrary;->getSubscriptionPurchasesForPackage(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 656
    .local v10, "subsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    move-object/from16 v0, p3

    invoke-static {v10, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->getListAndContinuationToken(Ljava/util/List;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v8

    .line 658
    .local v8, "listAndToken":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;Ljava/lang/String;>;"
    iget-object v10, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v10    # "subsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    check-cast v10, Ljava/util/List;

    .line 659
    .restart local v10    # "subsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    iget-object v0, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 p3, v0

    .end local p3    # "continuationToken":Ljava/lang/String;
    check-cast p3, Ljava/lang/String;

    .line 660
    .restart local p3    # "continuationToken":Ljava/lang/String;
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;

    .line 661
    .local v3, "entry":Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    invoke-virtual {v3}, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->getDocId()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/google/android/finsky/utils/DocUtils;->extractSkuForInApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 662
    iget-object v12, v3, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->signedPurchaseData:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 663
    iget-object v12, v3, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->signature:Ljava/lang/String;

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 666
    .end local v3    # "entry":Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v8    # "listAndToken":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;Ljava/lang/String;>;"
    .end local v10    # "subsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    :cond_1
    const-string v12, "INAPP_PURCHASE_ITEM_LIST"

    move-object/from16 v0, p4

    invoke-virtual {v0, v12, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 667
    const-string v12, "INAPP_PURCHASE_DATA_LIST"

    move-object/from16 v0, p4

    invoke-virtual {v0, v12, v11}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 668
    const-string v12, "INAPP_DATA_SIGNATURE_LIST"

    move-object/from16 v0, p4

    invoke-virtual {v0, v12, v9}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 669
    if-eqz p3, :cond_2

    .line 670
    const-string v12, "INAPP_CONTINUATION_TOKEN"

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-virtual {v0, v12, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    :cond_2
    return-void
.end method

.method private static positionToToken(I)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # I

    .prologue
    .line 710
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CONT-TOKEN-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 711
    .local v0, "tokenToEncode":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static tokenToPosition(Ljava/lang/String;)I
    .locals 3
    .param p0, "token"    # Ljava/lang/String;

    .prologue
    .line 716
    new-instance v1, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p0, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 717
    .local v1, "tokenDecoded":Ljava/lang/String;
    const-string v2, "CONT-TOKEN-"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 718
    const/4 v2, -0x1

    .line 721
    :goto_0
    return v2

    .line 720
    :cond_0
    const-string v2, "CONT-TOKEN-"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 721
    .local v0, "position":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method


# virtual methods
.method public consumePurchase(ILjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "purchaseToken"    # Ljava/lang/String;

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkBillingEnabled(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 340
    .local v0, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_0

    .line 341
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    .line 348
    :goto_0
    return v1

    .line 343
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 344
    const-string v1, "Input Error: Non empty/null argument expected for purchaseToken."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 345
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    goto :goto_0

    .line 347
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->consumeIabPurchase(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 348
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    goto :goto_0
.end method

.method public getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 10
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "sku"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "developerPayload"    # Ljava/lang/String;

    .prologue
    .line 275
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 276
    .local v8, "response":Landroid/os/Bundle;
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkBillingEnabled(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v9

    .line 277
    .local v9, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v9, v0, :cond_0

    .line 278
    const-string v0, "RESPONSE_CODE"

    invoke-virtual {v9}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 302
    :goto_0
    return-object v8

    .line 281
    :cond_0
    invoke-direct {p0, p4}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkTypeSupported(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v9

    .line 282
    sget-object v0, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v9, v0, :cond_1

    .line 283
    const-string v0, "RESPONSE_CODE"

    invoke-virtual {v9}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 286
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    const-string v0, "Input Error: Non empty/null argument expected for sku."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    const-string v0, "RESPONSE_CODE"

    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 293
    :cond_2
    invoke-direct {p0, p4, p3, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->isDocumentInLibrary(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 294
    const-string v0, "RESPONSE_CODE"

    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ITEM_ALREADY_OWNED:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 298
    :cond_3
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->makePurchaseIntent(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v7

    .line 300
    .local v7, "purchaseIntent":Landroid/app/PendingIntent;
    const-string v0, "BUY_INTENT"

    invoke-virtual {v8, v0, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 301
    const-string v0, "RESPONSE_CODE"

    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public getBuyIntentToReplaceSkus(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p4, "newSku"    # Ljava/lang/String;
    .param p5, "type"    # Ljava/lang/String;
    .param p6, "developerPayload"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .local p3, "oldSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x0

    .line 384
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 385
    .local v1, "response":Landroid/os/Bundle;
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkBillingEnabled(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v2

    .line 386
    .local v2, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v3, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v2, v3, :cond_0

    .line 387
    const-string v3, "RESPONSE_CODE"

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 418
    :goto_0
    return-object v1

    .line 390
    :cond_0
    const/4 v3, 0x5

    if-ge p1, v3, :cond_1

    .line 391
    const-string v3, "Input Error: getBuyIntentToReplaceSkus was introduced in API version 5."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    const-string v3, "RESPONSE_CODE"

    sget-object v4, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 397
    :cond_1
    invoke-direct {p0, p5}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkTypeSupported(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v2

    .line 398
    sget-object v3, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v2, v3, :cond_2

    .line 399
    const-string v3, "RESPONSE_CODE"

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 402
    :cond_2
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 403
    const-string v3, "Input Error: Non empty/null argument expected for newSku."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 404
    const-string v3, "RESPONSE_CODE"

    sget-object v4, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 409
    :cond_3
    invoke-direct {p0, p5, p4, p2}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->isDocumentInLibrary(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 410
    const-string v3, "RESPONSE_CODE"

    sget-object v4, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_ITEM_ALREADY_OWNED:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 414
    :cond_4
    invoke-direct/range {p0 .. p6}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->makePurchaseIntent(ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 416
    .local v0, "purchaseIntent":Landroid/app/PendingIntent;
    const-string v3, "BUY_INTENT"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 417
    const-string v3, "RESPONSE_CODE"

    sget-object v4, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "continuationToken"    # Ljava/lang/String;

    .prologue
    .line 315
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 316
    .local v0, "response":Landroid/os/Bundle;
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkBillingEnabled(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v1

    .line 317
    .local v1, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v1, v2, :cond_0

    .line 318
    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 328
    :goto_0
    return-object v0

    .line 321
    :cond_0
    invoke-direct {p0, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkTypeSupported(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v1

    .line 322
    sget-object v2, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v1, v2, :cond_1

    .line 323
    const-string v2, "RESPONSE_CODE"

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 326
    :cond_1
    invoke-direct {p0, p2, p3, p4, v0}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->populatePurchasesForPackage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 327
    const-string v2, "RESPONSE_CODE"

    sget-object v3, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 9
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "skusBundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 217
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 218
    .local v1, "response":Landroid/os/Bundle;
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkBillingEnabled(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v2

    .line 219
    .local v2, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v4, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v2, v4, :cond_0

    .line 220
    const-string v4, "RESPONSE_CODE"

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 264
    :goto_0
    return-object v1

    .line 223
    :cond_0
    invoke-direct {p0, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkTypeSupported(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v2

    .line 224
    sget-object v4, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v2, v4, :cond_1

    .line 225
    const-string v4, "RESPONSE_CODE"

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 228
    :cond_1
    if-nez p4, :cond_2

    .line 229
    const-string v4, "Input Error: Non-null argument expected for skusBundle."

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 230
    const-string v4, "RESPONSE_CODE"

    sget-object v5, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 233
    :cond_2
    const-string v4, "ITEM_ID_LIST"

    invoke-virtual {p4, v4}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 234
    .local v3, "skuList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v3, :cond_3

    .line 235
    const-string v4, "Input Error: skusBundle must contain an array associated with key %s."

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "ITEM_ID_LIST"

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    const-string v4, "RESPONSE_CODE"

    sget-object v5, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 240
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 241
    const-string v4, "Input Error: skusBundle array associated with key %s cannot be empty."

    new-array v5, v8, [Ljava/lang/Object;

    const-string v6, "ITEM_ID_LIST"

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    const-string v4, "RESPONSE_CODE"

    sget-object v5, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    .line 246
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    sget v5, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->DETAILS_REQUEST_SKU_LIST_MAX_SIZE:I

    if-le v4, v5, :cond_5

    .line 247
    const-string v4, "Input Error: skusBundle array associated with key %s cannot contain more than %d items."

    new-array v5, v6, [Ljava/lang/Object;

    const-string v6, "ITEM_ID_LIST"

    aput-object v6, v5, v7

    sget v6, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->DETAILS_REQUEST_SKU_LIST_MAX_SIZE:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 250
    const-string v4, "RESPONSE_CODE"

    sget-object v5, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 253
    :cond_5
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_7

    .line 254
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 255
    const-string v4, "Input Error: skusBundle array associated with key %s contains an empty/null sku at index %d."

    new-array v5, v6, [Ljava/lang/Object;

    const-string v6, "ITEM_ID_LIST"

    aput-object v6, v5, v7

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 258
    const-string v4, "RESPONSE_CODE"

    sget-object v5, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v5}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 253
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 263
    :cond_7
    invoke-direct {p0, p2, v3, p3, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->fetchSkuDetails(Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method

.method public isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkBillingEnabled(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 198
    .local v0, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_0

    .line 199
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    .line 205
    :goto_0
    return v1

    .line 201
    :cond_0
    invoke-direct {p0, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkTypeSupported(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 202
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_1

    .line 203
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    goto :goto_0

    .line 205
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    goto :goto_0
.end method

.method public isPromoEligible(ILjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "apiVersion"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 359
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkBillingEnabled(I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 360
    .local v0, "responseCode":Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_0

    .line 361
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    .line 372
    :goto_0
    return v1

    .line 363
    :cond_0
    invoke-direct {p0, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->checkTypeSupported(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 364
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_OK:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    if-eq v0, v1, :cond_1

    .line 365
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    goto :goto_0

    .line 367
    :cond_1
    const/4 v1, 0x4

    if-ge p1, v1, :cond_2

    .line 368
    const-string v1, "Input Error: isPromoEligible was introduced in API version 4."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 369
    sget-object v1, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->RESULT_DEVELOPER_ERROR:Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    goto :goto_0

    .line 371
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;->performIabPromoCheck(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    .line 372
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v1

    goto :goto_0
.end method
