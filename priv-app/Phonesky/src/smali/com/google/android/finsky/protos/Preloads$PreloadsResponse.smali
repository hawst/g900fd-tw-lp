.class public final Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Preloads.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Preloads;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PreloadsResponse"
.end annotation


# instance fields
.field public appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

.field public configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->clear()Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    .line 33
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;

    .line 37
    invoke-static {}, Lcom/google/android/finsky/protos/Preloads$Preload;->emptyArray()[Lcom/google/android/finsky/protos/Preloads$Preload;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->cachedSize:I

    .line 39
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 61
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 62
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;

    if-eqz v3, :cond_0

    .line 63
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 66
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 67
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 68
    iget-object v3, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    aget-object v0, v3, v1

    .line 69
    .local v0, "element":Lcom/google/android/finsky/protos/Preloads$Preload;
    if-eqz v0, :cond_1

    .line 70
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 67
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    .end local v0    # "element":Lcom/google/android/finsky/protos/Preloads$Preload;
    .end local v1    # "i":I
    :cond_2
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 83
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 84
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 88
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 89
    :sswitch_0
    return-object p0

    .line 94
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;

    if-nez v5, :cond_1

    .line 95
    new-instance v5, Lcom/google/android/finsky/protos/Preloads$Preload;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Preloads$Preload;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;

    .line 97
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 101
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 103
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    if-nez v5, :cond_3

    move v1, v4

    .line 104
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Preloads$Preload;

    .line 106
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Preloads$Preload;
    if-eqz v1, :cond_2

    .line 107
    iget-object v5, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 109
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 110
    new-instance v5, Lcom/google/android/finsky/protos/Preloads$Preload;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Preloads$Preload;-><init>()V

    aput-object v5, v2, v1

    .line 111
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 112
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 109
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 103
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Preloads$Preload;
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    array-length v1, v5

    goto :goto_1

    .line 115
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Preloads$Preload;
    :cond_4
    new-instance v5, Lcom/google/android/finsky/protos/Preloads$Preload;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Preloads$Preload;-><init>()V

    aput-object v5, v2, v1

    .line 116
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 117
    iput-object v2, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    goto :goto_0

    .line 84
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 45
    iget-object v2, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;

    if-eqz v2, :cond_0

    .line 46
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 48
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 49
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 50
    iget-object v2, p0, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    aget-object v0, v2, v1

    .line 51
    .local v0, "element":Lcom/google/android/finsky/protos/Preloads$Preload;
    if-eqz v0, :cond_1

    .line 52
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 49
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 56
    .end local v0    # "element":Lcom/google/android/finsky/protos/Preloads$Preload;
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 57
    return-void
.end method
