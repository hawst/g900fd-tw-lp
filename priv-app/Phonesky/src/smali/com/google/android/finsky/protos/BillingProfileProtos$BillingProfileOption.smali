.class public final Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BillingProfileProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BillingProfileProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillingProfileOption"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;


# instance fields
.field public carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

.field public displayTitle:Ljava/lang/String;

.field public externalInstrumentId:Ljava/lang/String;

.field public genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

.field public hasDisplayTitle:Z

.field public hasExternalInstrumentId:Z

.field public hasPaymentsIntegratorInstrumentToken:Z

.field public hasServerLogsCookie:Z

.field public hasType:Z

.field public hasTypeName:Z

.field public iconImage:Lcom/google/android/finsky/protos/Common$Image;

.field public paymentsIntegratorInstrumentToken:[B

.field public serverLogsCookie:[B

.field public topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

.field public type:I

.field public typeName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 276
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->clear()Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    .line 277
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    .locals 2

    .prologue
    .line 228
    sget-object v0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->_emptyArray:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    if-nez v0, :cond_1

    .line 229
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 231
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->_emptyArray:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    if-nez v0, :cond_0

    .line 232
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    sput-object v0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->_emptyArray:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    .line 234
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 236
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->_emptyArray:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    return-object v0

    .line 234
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 280
    iput v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->type:I

    .line 281
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasType:Z

    .line 282
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->displayTitle:Ljava/lang/String;

    .line 283
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasDisplayTitle:Z

    .line 284
    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 285
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->serverLogsCookie:[B

    .line 286
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasServerLogsCookie:Z

    .line 287
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->typeName:Ljava/lang/String;

    .line 288
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasTypeName:Z

    .line 289
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->externalInstrumentId:Ljava/lang/String;

    .line 290
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasExternalInstrumentId:Z

    .line 291
    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    .line 292
    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .line 293
    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .line 294
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->paymentsIntegratorInstrumentToken:[B

    .line 295
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasPaymentsIntegratorInstrumentToken:Z

    .line 296
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->cachedSize:I

    .line 297
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 338
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 339
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->type:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasType:Z

    if-eqz v1, :cond_1

    .line 340
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->type:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 343
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasDisplayTitle:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->displayTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 344
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->displayTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 347
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasExternalInstrumentId:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->externalInstrumentId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 348
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->externalInstrumentId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 351
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-eqz v1, :cond_6

    .line 352
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-eqz v1, :cond_7

    .line 356
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    if-eqz v1, :cond_8

    .line 360
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasPaymentsIntegratorInstrumentToken:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->paymentsIntegratorInstrumentToken:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_a

    .line 364
    :cond_9
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->paymentsIntegratorInstrumentToken:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_b

    .line 368
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasServerLogsCookie:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_d

    .line 372
    :cond_c
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasTypeName:Z

    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->typeName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 376
    :cond_e
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->typeName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 379
    :cond_f
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 387
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 388
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 392
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 393
    :sswitch_0
    return-object p0

    .line 398
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 399
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 407
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->type:I

    .line 408
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasType:Z

    goto :goto_0

    .line 414
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->displayTitle:Ljava/lang/String;

    .line 415
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasDisplayTitle:Z

    goto :goto_0

    .line 419
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->externalInstrumentId:Ljava/lang/String;

    .line 420
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasExternalInstrumentId:Z

    goto :goto_0

    .line 424
    :sswitch_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-nez v2, :cond_1

    .line 425
    new-instance v2, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    .line 427
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 431
    :sswitch_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-nez v2, :cond_2

    .line 432
    new-instance v2, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    .line 434
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 438
    :sswitch_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    if-nez v2, :cond_3

    .line 439
    new-instance v2, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .line 441
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 445
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->paymentsIntegratorInstrumentToken:[B

    .line 446
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasPaymentsIntegratorInstrumentToken:Z

    goto :goto_0

    .line 450
    :sswitch_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v2, :cond_4

    .line 451
    new-instance v2, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 453
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 457
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->serverLogsCookie:[B

    .line 458
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasServerLogsCookie:Z

    goto/16 :goto_0

    .line 462
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->typeName:Ljava/lang/String;

    .line 463
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasTypeName:Z

    goto/16 :goto_0

    .line 388
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch

    .line 399
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 213
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    iget v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->type:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasType:Z

    if-eqz v0, :cond_1

    .line 304
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->type:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 306
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasDisplayTitle:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->displayTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 307
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->displayTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 309
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasExternalInstrumentId:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->externalInstrumentId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 310
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->externalInstrumentId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 312
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    if-eqz v0, :cond_6

    .line 313
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->topupInfo:Lcom/google/android/finsky/protos/CommonDevice$TopupInfo;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 315
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-eqz v0, :cond_7

    .line 316
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->carrierBillingInstrumentStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 318
    :cond_7
    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    if-eqz v0, :cond_8

    .line 319
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->genericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 321
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasPaymentsIntegratorInstrumentToken:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->paymentsIntegratorInstrumentToken:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_a

    .line 322
    :cond_9
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->paymentsIntegratorInstrumentToken:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 324
    :cond_a
    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_b

    .line 325
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 327
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasServerLogsCookie:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_d

    .line 328
    :cond_c
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 330
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->hasTypeName:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->typeName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 331
    :cond_e
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->typeName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 333
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 334
    return-void
.end method
