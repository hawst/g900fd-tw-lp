.class Lcom/google/android/finsky/installer/PackageInstallerImpl$1;
.super Landroid/os/AsyncTask;
.source "PackageInstallerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/installer/PackageInstallerImpl;->install(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field mException:Ljava/io/IOException;

.field final synthetic this$0:Lcom/google/android/finsky/installer/PackageInstallerImpl;

.field final synthetic val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

.field final synthetic val$contentUri:Landroid/net/Uri;

.field final synthetic val$expectedSignature:Ljava/lang/String;

.field final synthetic val$expectedSize:J

.field final synthetic val$finalSession:Landroid/content/pm/PackageInstaller$Session;

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$sessionId:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/installer/PackageInstallerImpl;JLandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/PackageInstaller$Session;Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;I)V
    .locals 2

    .prologue
    .line 316
    iput-object p1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->this$0:Lcom/google/android/finsky/installer/PackageInstallerImpl;

    iput-wide p2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$expectedSize:J

    iput-object p4, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$contentUri:Landroid/net/Uri;

    iput-object p5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$expectedSignature:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$packageName:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$finalSession:Landroid/content/pm/PackageInstaller$Session;

    iput-object p8, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    iput p9, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$sessionId:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 317
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->mException:Ljava/io/IOException;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 11
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    const-wide/16 v2, 0x0

    .line 322
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 323
    .local v7, "contentResolver":Landroid/content/ContentResolver;
    iget-wide v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$expectedSize:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$contentUri:Landroid/net/Uri;

    invoke-virtual {v7, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v6

    .line 326
    .local v6, "apkStream":Ljava/io/InputStream;
    iget-wide v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$expectedSize:J

    iget-object v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$expectedSignature:Ljava/lang/String;

    invoke-static {v6, v0, v1, v2}, Lcom/google/android/finsky/utils/PackageManagerHelper;->verifyApk(Ljava/io/InputStream;JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    .end local v6    # "apkStream":Ljava/io/InputStream;
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$finalSession:Landroid/content/pm/PackageInstaller$Session;

    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$packageName:Ljava/lang/String;

    const-wide/16 v2, 0x0

    iget-wide v4, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$expectedSize:J

    invoke-virtual/range {v0 .. v5}, Landroid/content/pm/PackageInstaller$Session;->openWrite(Ljava/lang/String;JJ)Ljava/io/OutputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v10

    .line 349
    .local v10, "sessionStream":Ljava/io/OutputStream;
    :try_start_2
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$contentUri:Landroid/net/Uri;

    invoke-virtual {v7, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v6

    .line 350
    .restart local v6    # "apkStream":Ljava/io/InputStream;
    invoke-static {v6, v10}, Lcom/google/android/finsky/utils/Utils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 351
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$finalSession:Landroid/content/pm/PackageInstaller$Session;

    invoke-virtual {v0, v10}, Landroid/content/pm/PackageInstaller$Session;->fsync(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 359
    :try_start_3
    invoke-virtual {v10}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 366
    const/4 v0, 0x0

    .end local v6    # "apkStream":Ljava/io/InputStream;
    .end local v10    # "sessionStream":Ljava/io/OutputStream;
    :goto_0
    return-object v0

    .line 328
    :catch_0
    move-exception v9

    .line 329
    .local v9, "ioe":Ljava/io/IOException;
    iput-object v9, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->mException:Ljava/io/IOException;

    .line 330
    iget-object v0, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->mException:Ljava/io/IOException;

    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$packageName:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/PackageManagerHelper;->verificationExceptionToError(Ljava/io/IOException;Ljava/lang/String;)I

    move-result v8

    .line 332
    .local v8, "errorCode":I
    const-string v0, "Signature check failed, aborting installation."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 333
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 341
    .end local v8    # "errorCode":I
    .end local v9    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v9

    .line 342
    .restart local v9    # "ioe":Ljava/io/IOException;
    iput-object v9, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->mException:Ljava/io/IOException;

    .line 343
    const/16 v0, 0x3cc

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 352
    .end local v9    # "ioe":Ljava/io/IOException;
    .restart local v10    # "sessionStream":Ljava/io/OutputStream;
    :catch_2
    move-exception v9

    .line 353
    .restart local v9    # "ioe":Ljava/io/IOException;
    iput-object v9, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->mException:Ljava/io/IOException;

    .line 354
    const/16 v0, 0x3cd

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 360
    .end local v9    # "ioe":Ljava/io/IOException;
    .restart local v6    # "apkStream":Ljava/io/InputStream;
    :catch_3
    move-exception v9

    .line 361
    .restart local v9    # "ioe":Ljava/io/IOException;
    iput-object v9, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->mException:Ljava/io/IOException;

    .line 362
    const/16 v0, 0x3ce

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 316
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 7
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 371
    if-eqz p1, :cond_0

    .line 373
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 374
    .local v0, "errorCode":I
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->mException:Ljava/io/IOException;

    const-string v2, "Exception while streaming %s: %d"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$packageName:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 376
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$finalSession:Landroid/content/pm/PackageInstaller$Session;

    invoke-virtual {v1}, Landroid/content/pm/PackageInstaller$Session;->close()V

    .line 377
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->this$0:Lcom/google/android/finsky/installer/PackageInstallerImpl;

    # getter for: Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->access$000(Lcom/google/android/finsky/installer/PackageInstallerImpl;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$packageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    iget-object v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->mException:Ljava/io/IOException;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installFailed(ILjava/lang/String;)V

    .line 390
    .end local v0    # "errorCode":I
    :goto_0
    return-void

    .line 384
    :cond_0
    const-string v1, "Committing session %d for %s"

    new-array v2, v3, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$sessionId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget-object v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$packageName:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 386
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    invoke-interface {v1}, Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;->installBeginning()V

    .line 387
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$finalSession:Landroid/content/pm/PackageInstaller$Session;

    iget-object v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->this$0:Lcom/google/android/finsky/installer/PackageInstallerImpl;

    iget-object v3, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$packageName:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$sessionId:I

    iget-object v5, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$callback:Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;

    # invokes: Lcom/google/android/finsky/installer/PackageInstallerImpl;->getCommitCallback(Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)Landroid/content/IntentSender;
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->access$100(Lcom/google/android/finsky/installer/PackageInstallerImpl;Ljava/lang/String;ILcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;)Landroid/content/IntentSender;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageInstaller$Session;->commit(Landroid/content/IntentSender;)V

    .line 388
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$finalSession:Landroid/content/pm/PackageInstaller$Session;

    invoke-virtual {v1}, Landroid/content/pm/PackageInstaller$Session;->close()V

    .line 389
    iget-object v1, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->this$0:Lcom/google/android/finsky/installer/PackageInstallerImpl;

    # getter for: Lcom/google/android/finsky/installer/PackageInstallerImpl;->mOpenSessionMap:Ljava/util/Map;
    invoke-static {v1}, Lcom/google/android/finsky/installer/PackageInstallerImpl;->access$000(Lcom/google/android/finsky/installer/PackageInstallerImpl;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->val$packageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 316
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/installer/PackageInstallerImpl$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
