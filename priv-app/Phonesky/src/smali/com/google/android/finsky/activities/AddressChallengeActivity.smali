.class public Lcom/google/android/finsky/activities/AddressChallengeActivity;
.super Lcom/google/android/finsky/activities/ChallengeActivity;
.source "AddressChallengeActivity.java"


# instance fields
.field private mFlow:Lcom/google/android/finsky/billing/BillingFlow;

.field private final mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ChallengeActivity;-><init>()V

    .line 27
    new-instance v0, Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/FakeNavigationManager;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/AddressChallengeActivity;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    return-void
.end method

.method public static getIntent(ILcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 3
    .param p0, "backend"    # I
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    .param p2, "extraParameters"    # Landroid/os/Bundle;

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/activities/AddressChallengeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "backend"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 43
    const-string v1, "challenge"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 44
    const-string v1, "extra_parameters"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 45
    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 50
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/ChallengeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v4, 0x7f040030

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/activities/AddressChallengeActivity;->setContentView(I)V

    .line 54
    new-instance v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iget-object v4, p0, Lcom/google/android/finsky/activities/AddressChallengeActivity;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-direct {v0, v4, p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/support/v7/app/ActionBarActivity;)V

    .line 55
    .local v0, "actionBarHelper":Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AddressChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "backend"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 56
    .local v2, "backendId":I
    invoke-virtual {v0, v2, v6}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateCurrentBackendId(IZ)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AddressChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "challenge"

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 60
    .local v1, "addressChallenge":Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AddressChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "extra_parameters"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    .line 61
    .local v3, "extraParameters":Landroid/os/Bundle;
    new-instance v4, Lcom/google/android/finsky/billing/challenge/AddressChallengeFlow;

    invoke-direct {v4, p0, p0, v1, v3}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFlow;-><init>(Lcom/google/android/finsky/billing/BillingFlowContext;Lcom/google/android/finsky/billing/BillingFlowListener;Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;Landroid/os/Bundle;)V

    iput-object v4, p0, Lcom/google/android/finsky/activities/AddressChallengeActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    .line 62
    if-eqz p1, :cond_0

    .line 63
    iget-object v4, p0, Lcom/google/android/finsky/activities/AddressChallengeActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v4, p1}, Lcom/google/android/finsky/billing/BillingFlow;->resumeFromSavedState(Landroid/os/Bundle;)V

    .line 67
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/activities/AddressChallengeActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v4}, Lcom/google/android/finsky/billing/BillingFlow;->start()V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 79
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/finsky/activities/AddressChallengeActivity;->mNavigationManager:Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/FakeNavigationManager;->goUp()Z

    .line 81
    const/4 v0, 0x1

    .line 83
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/ChallengeActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/ChallengeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/activities/AddressChallengeActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/android/finsky/activities/AddressChallengeActivity;->mFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/BillingFlow;->saveState(Landroid/os/Bundle;)V

    .line 75
    :cond_0
    return-void
.end method
