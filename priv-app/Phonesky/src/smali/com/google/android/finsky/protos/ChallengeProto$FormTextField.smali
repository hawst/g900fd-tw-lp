.class public final Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FormTextField"
.end annotation


# instance fields
.field public defaultValue:Ljava/lang/String;

.field public error:Ljava/lang/String;

.field public hasDefaultValue:Z

.field public hasError:Z

.field public hasHint:Z

.field public hasLabel:Z

.field public hasPostParam:Z

.field public hint:Ljava/lang/String;

.field public label:Ljava/lang/String;

.field public postParam:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2579
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2580
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->clear()Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    .line 2581
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2584
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->label:Ljava/lang/String;

    .line 2585
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasLabel:Z

    .line 2586
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    .line 2587
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasDefaultValue:Z

    .line 2588
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    .line 2589
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasHint:Z

    .line 2590
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->error:Ljava/lang/String;

    .line 2591
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasError:Z

    .line 2592
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    .line 2593
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasPostParam:Z

    .line 2594
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->cachedSize:I

    .line 2595
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2621
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2622
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasLabel:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->label:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2623
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->label:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2626
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasDefaultValue:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2627
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2630
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasHint:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2631
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2634
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasError:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->error:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2635
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->error:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2638
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasPostParam:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2639
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2642
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2650
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2651
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2655
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2656
    :sswitch_0
    return-object p0

    .line 2661
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->label:Ljava/lang/String;

    .line 2662
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasLabel:Z

    goto :goto_0

    .line 2666
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    .line 2667
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasDefaultValue:Z

    goto :goto_0

    .line 2671
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    .line 2672
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasHint:Z

    goto :goto_0

    .line 2676
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->error:Ljava/lang/String;

    .line 2677
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasError:Z

    goto :goto_0

    .line 2681
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    .line 2682
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasPostParam:Z

    goto :goto_0

    .line 2651
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2542
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2601
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasLabel:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->label:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2602
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->label:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2604
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasDefaultValue:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2605
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->defaultValue:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2607
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasHint:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2608
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hint:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2610
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasError:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->error:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2611
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->error:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2613
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->hasPostParam:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2614
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormTextField;->postParam:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2616
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2617
    return-void
.end method
