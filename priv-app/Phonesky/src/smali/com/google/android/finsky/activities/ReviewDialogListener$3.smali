.class Lcom/google/android/finsky/activities/ReviewDialogListener$3;
.super Ljava/lang/Object;
.source "ReviewDialogListener.java"

# interfaces
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/ReviewDialogListener;->onReviewFeedback(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/ReviewDialogListener;

.field final synthetic val$newRating:Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/ReviewDialogListener;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewDialogListener$3;->this$0:Lcom/google/android/finsky/activities/ReviewDialogListener;

    iput-object p2, p0, Lcom/google/android/finsky/activities/ReviewDialogListener$3;->val$newRating:Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged()V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener$3;->val$newRating:Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;

    sget-object v1, Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;->SPAM:Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;

    if-ne v0, v1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener$3;->this$0:Lcom/google/android/finsky/activities/ReviewDialogListener;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialogListener;->mReviewSamplesSection:Lcom/google/android/finsky/layout/ReviewSamplesSection;
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewDialogListener;->access$100(Lcom/google/android/finsky/activities/ReviewDialogListener;)Lcom/google/android/finsky/layout/ReviewSamplesSection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->invalidateCurrentReviewUrl()V

    .line 122
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener$3;->this$0:Lcom/google/android/finsky/activities/ReviewDialogListener;

    # getter for: Lcom/google/android/finsky/activities/ReviewDialogListener;->mReviewSamplesSection:Lcom/google/android/finsky/layout/ReviewSamplesSection;
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewDialogListener;->access$100(Lcom/google/android/finsky/activities/ReviewDialogListener;)Lcom/google/android/finsky/layout/ReviewSamplesSection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/ReviewSamplesSection;->refresh()V

    .line 124
    :cond_0
    return-void
.end method
