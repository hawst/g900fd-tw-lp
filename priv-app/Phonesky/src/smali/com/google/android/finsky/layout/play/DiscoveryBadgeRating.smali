.class public Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;
.super Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
.source "DiscoveryBadgeRating.java"


# instance fields
.field private mAverageValue:Landroid/widget/TextView;

.field private mCount:Landroid/widget/TextView;

.field private mFloatFormatter:Ljava/text/NumberFormat;

.field private mFocusedOutlineColor:I

.field private mOctagonPath:Landroid/graphics/Path;

.field private mOutlineStrokeWidth:F

.field private mPaint:Landroid/graphics/Paint;

.field private mPressedFillColor:I

.field private mPressedOutlineColor:I

.field private mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

.field private mVertices:[Landroid/graphics/PointF;

.field private mWhiteOctagonRadius:F

.field private mWhiteOctagonStrokeWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    .line 57
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 62
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0b017a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mBadgeRadius:I

    .line 64
    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mFloatFormatter:Ljava/text/NumberFormat;

    .line 65
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mFloatFormatter:Ljava/text/NumberFormat;

    invoke-virtual {v2, v3}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 66
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mFloatFormatter:Ljava/text/NumberFormat;

    invoke-virtual {v2, v3}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 68
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    .line 69
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    .line 70
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v2, v3}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 71
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->setWillNotDraw(Z)V

    .line 72
    new-array v2, v4, [Landroid/graphics/PointF;

    iput-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    .line 73
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_0

    .line 74
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    aput-object v3, v2, v0

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    const v2, 0x7f0b0179

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mWhiteOctagonStrokeWidth:I

    .line 82
    iget v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mBadgeRadius:I

    iget v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mWhiteOctagonStrokeWidth:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mWhiteOctagonStrokeWidth:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mWhiteOctagonRadius:F

    .line 85
    const v2, 0x7f09007f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPressedFillColor:I

    .line 86
    const v2, 0x7f090080

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPressedOutlineColor:I

    .line 87
    const v2, 0x7f090081

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mFocusedOutlineColor:I

    .line 88
    const/high16 v2, 0x3f000000    # 0.5f

    const v3, 0x7f0b0078

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOutlineStrokeWidth:F

    .line 89
    return-void
.end method

.method private setOctagonPath(FFF)V
    .locals 5
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "radius"    # F

    .prologue
    const/4 v4, 0x0

    .line 176
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    invoke-direct {p0, p1, p2, p3, v1}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->updateOctagonCoordinates(FFF[Landroid/graphics/PointF;)V

    .line 177
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 179
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    aget-object v2, v2, v4

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    aget-object v3, v3, v4

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 181
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 181
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 184
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 185
    return-void
.end method

.method private updateOctagonCoordinates(FFF[Landroid/graphics/PointF;)V
    .locals 10
    .param p1, "centerX"    # F
    .param p2, "centerY"    # F
    .param p3, "radius"    # F
    .param p4, "vertices"    # [Landroid/graphics/PointF;

    .prologue
    .line 189
    const-wide v6, 0x3fe921fb54442d18L    # 0.7853981633974483

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    .line 190
    .local v4, "offset":D
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    const/high16 v7, -0x40800000    # -1.0f

    mul-float/2addr v7, p3

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 191
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    const/4 v7, 0x0

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 193
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    float-to-double v8, p3

    mul-double/2addr v8, v4

    double-to-int v7, v8

    neg-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 194
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    float-to-double v8, p3

    mul-double/2addr v8, v4

    double-to-int v7, v8

    neg-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 196
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x2

    aget-object v6, v6, v7

    const/4 v7, 0x0

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 197
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x2

    aget-object v6, v6, v7

    neg-float v7, p3

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 199
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    float-to-double v8, p3

    mul-double/2addr v8, v4

    double-to-int v7, v8

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 200
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x3

    aget-object v6, v6, v7

    float-to-double v8, p3

    mul-double/2addr v8, v4

    double-to-int v7, v8

    neg-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 202
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x4

    aget-object v6, v6, v7

    iput p3, v6, Landroid/graphics/PointF;->x:F

    .line 203
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x4

    aget-object v6, v6, v7

    const/4 v7, 0x0

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 205
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x5

    aget-object v6, v6, v7

    float-to-double v8, p3

    mul-double/2addr v8, v4

    double-to-int v7, v8

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 206
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x5

    aget-object v6, v6, v7

    float-to-double v8, p3

    mul-double/2addr v8, v4

    double-to-int v7, v8

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 208
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    const/4 v7, 0x0

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 209
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x6

    aget-object v6, v6, v7

    iput p3, v6, Landroid/graphics/PointF;->y:F

    .line 211
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x7

    aget-object v6, v6, v7

    float-to-double v8, p3

    mul-double/2addr v8, v4

    double-to-int v7, v8

    neg-int v7, v7

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->x:F

    .line 212
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    const/4 v7, 0x7

    aget-object v6, v6, v7

    float-to-double v8, p3

    mul-double/2addr v8, v4

    double-to-int v7, v8

    int-to-float v7, v7

    iput v7, v6, Landroid/graphics/PointF;->y:F

    .line 214
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mVertices:[Landroid/graphics/PointF;

    .local v0, "arr$":[Landroid/graphics/PointF;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 215
    .local v3, "p":Landroid/graphics/PointF;
    iget v6, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, p1

    iput v6, v3, Landroid/graphics/PointF;->x:F

    .line 216
    iget v6, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v6, p2

    iput v6, v3, Landroid/graphics/PointF;->y:F

    .line 214
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    .end local v3    # "p":Landroid/graphics/PointF;
    :cond_0
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 6
    .param p1, "badge"    # Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 109
    invoke-super/range {p0 .. p7}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 110
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mFloatFormatter:Ljava/text/NumberFormat;

    iget v3, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->aggregateRating:F

    float-to-double v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 111
    .local v1, "formattedRating":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mAverageValue:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mFloatFormatter:Ljava/text/NumberFormat;

    invoke-virtual {v3, v1}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mCount:Landroid/widget/TextView;

    iget-object v3, p1, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mAverageValue:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 122
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Ljava/text/ParseException;
    const-string v2, "Cannot parse %s. Exception %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 0

    .prologue
    .line 126
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->drawableStateChanged()V

    .line 127
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->invalidate()V

    .line 128
    return-void
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 222
    const/16 v0, 0x70a

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->getWidth()I

    move-result v3

    div-int/lit8 v1, v3, 0x2

    .line 133
    .local v1, "x":I
    iget v2, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mBadgeRadius:I

    .line 136
    .local v2, "y":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mCurrentFillColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 137
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 138
    int-to-float v3, v1

    int-to-float v4, v2

    iget v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mBadgeRadius:I

    int-to-float v5, v5

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->setOctagonPath(FFF)V

    .line 139
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 142
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mWhiteOctagonStrokeWidth:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 143
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 144
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 145
    int-to-float v3, v1

    int-to-float v4, v2

    iget v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mWhiteOctagonRadius:F

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->setOctagonPath(FFF)V

    .line 146
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 148
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->isDuplicateParentStateEnabled()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->isClickable()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 149
    .local v0, "drawPressed":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 150
    int-to-float v3, v1

    int-to-float v4, v2

    iget v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mBadgeRadius:I

    int-to-float v5, v5

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->setOctagonPath(FFF)V

    .line 153
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPressedFillColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 154
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 155
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 158
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPressedOutlineColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 159
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 160
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOutlineStrokeWidth:F

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 161
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 173
    :cond_1
    :goto_1
    return-void

    .line 148
    .end local v0    # "drawPressed":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 165
    .restart local v0    # "drawPressed":Z
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->isFocused()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 167
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mFocusedOutlineColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 168
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 169
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOutlineStrokeWidth:F

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 170
    int-to-float v3, v1

    int-to-float v4, v2

    iget v5, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mBadgeRadius:I

    int-to-float v5, v5

    invoke-direct {p0, v3, v4, v5}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->setOctagonPath(FFF)V

    .line 171
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mOctagonPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->onFinishInflate()V

    .line 95
    const v0, 0x7f0a01aa

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mAverageValue:Landroid/widget/TextView;

    .line 96
    const v0, 0x7f0a01ab

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 97
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeRating;->mCount:Landroid/widget/TextView;

    .line 98
    return-void
.end method
