.class public final Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseMetadataResponseProto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;
    }
.end annotation


# instance fields
.field public countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1455
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1456
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    .line 1457
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;
    .locals 1

    .prologue
    .line 1460
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    .line 1461
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->cachedSize:I

    .line 1462
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1476
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1477
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    if-eqz v1, :cond_0

    .line 1478
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1481
    :cond_0
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1489
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1490
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1494
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1495
    :sswitch_0
    return-object p0

    .line 1500
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    if-nez v1, :cond_1

    .line 1501
    new-instance v1, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    .line 1503
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    goto :goto_0

    .line 1490
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1041
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1468
    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    if-eqz v0, :cond_0

    .line 1469
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto;->countries:Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1471
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1472
    return-void
.end method
