.class Lcom/google/android/finsky/services/VpaService$5;
.super Ljava/lang/Object;
.source "VpaService.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/VpaService;->vpaGetPreloads(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/VpaService;

.field final synthetic val$accountName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/finsky/services/VpaService$5;->this$0:Lcom/google/android/finsky/services/VpaService;

    iput-object p2, p0, Lcom/google/android/finsky/services/VpaService$5;->val$accountName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;)V
    .locals 7
    .param p1, "preloadsResponse"    # Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    .prologue
    const/4 v6, 0x0

    .line 246
    const-string v1, "VPA response received"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    iget-object v0, p1, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->configPreload:Lcom/google/android/finsky/protos/Preloads$Preload;

    .line 249
    .local v0, "configPreload":Lcom/google/android/finsky/protos/Preloads$Preload;
    if-eqz v0, :cond_0

    .line 250
    iget-object v1, p0, Lcom/google/android/finsky/services/VpaService$5;->this$0:Lcom/google/android/finsky/services/VpaService;

    iget-object v2, p0, Lcom/google/android/finsky/services/VpaService$5;->val$accountName:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/services/VpaService;->downloadVpaConfig(Ljava/lang/String;Lcom/google/android/finsky/protos/Preloads$Preload;)V
    invoke-static {v1, v2, v0}, Lcom/google/android/finsky/services/VpaService;->access$700(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;Lcom/google/android/finsky/protos/Preloads$Preload;)V

    .line 256
    :cond_0
    iget-object v1, p1, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 257
    iget-object v1, p0, Lcom/google/android/finsky/services/VpaService$5;->this$0:Lcom/google/android/finsky/services/VpaService;

    iget-object v2, p0, Lcom/google/android/finsky/services/VpaService$5;->val$accountName:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;->appPreload:[Lcom/google/android/finsky/protos/Preloads$Preload;

    const/4 v4, 0x2

    const/4 v5, 0x1

    # invokes: Lcom/google/android/finsky/services/VpaService;->downloadPackages(Ljava/lang/String;[Lcom/google/android/finsky/protos/Preloads$Preload;IZ)V
    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/services/VpaService;->access$800(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;[Lcom/google/android/finsky/protos/Preloads$Preload;IZ)V

    .line 260
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/services/VpaService$5;->this$0:Lcom/google/android/finsky/services/VpaService;

    # setter for: Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z
    invoke-static {v1, v6}, Lcom/google/android/finsky/services/VpaService;->access$302(Lcom/google/android/finsky/services/VpaService;Z)Z

    .line 261
    iget-object v1, p0, Lcom/google/android/finsky/services/VpaService$5;->this$0:Lcom/google/android/finsky/services/VpaService;

    # invokes: Lcom/google/android/finsky/services/VpaService;->stopServiceIfDone()V
    invoke-static {v1}, Lcom/google/android/finsky/services/VpaService;->access$200(Lcom/google/android/finsky/services/VpaService;)V

    .line 262
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 243
    check-cast p1, Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/services/VpaService$5;->onResponse(Lcom/google/android/finsky/protos/Preloads$PreloadsResponse;)V

    return-void
.end method
