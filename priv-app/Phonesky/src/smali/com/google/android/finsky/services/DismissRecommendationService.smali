.class public Lcom/google/android/finsky/services/DismissRecommendationService;
.super Landroid/app/IntentService;
.source "DismissRecommendationService.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/google/android/finsky/services/DismissRecommendationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public static getDismissPendingIntent(Landroid/content/Context;ILcom/google/android/finsky/api/model/Document;II)Landroid/app/PendingIntent;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I
    .param p2, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "backendId"    # I
    .param p4, "position"    # I

    .prologue
    const/4 v5, 0x0

    .line 118
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->hasNeutralDismissal()Z

    move-result v3

    if-nez v3, :cond_0

    .line 119
    const/4 v3, 0x0

    .line 137
    :goto_0
    return-object v3

    .line 121
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getNeutralDismissal()Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    move-result-object v3

    iget-object v1, v3, Lcom/google/android/finsky/protos/DocumentV2$Dismissal;->url:Ljava/lang/String;

    .line 123
    .local v1, "dismissalUrl":Ljava/lang/String;
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    .line 124
    .local v2, "dummyUriForDeDuping":Landroid/net/Uri$Builder;
    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 125
    const-string v3, "dismiss"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 126
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 127
    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 128
    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 130
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/finsky/services/DismissRecommendationService;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 131
    .local v0, "dismiss":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 132
    const-string v3, "appWidgetId"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 133
    const-string v3, "documentId"

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    const-string v3, "dismissalUrl"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string v3, "backendId"

    invoke-virtual {v0, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 137
    invoke-static {p0, v5, v0, v5}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 18
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 55
    const-string v13, "appWidgetId"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 58
    .local v2, "appWidgetId":I
    const-string v13, "documentId"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 59
    .local v6, "documentId":Ljava/lang/String;
    const-string v13, "dismissalUrl"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 60
    .local v5, "dismissalUrl":Ljava/lang/String;
    const-string v13, "backendId"

    const/4 v14, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 61
    .local v3, "backendId":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v11

    .line 63
    .local v11, "library":Lcom/google/android/finsky/library/Library;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v4

    .line 65
    .local v4, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    if-nez v4, :cond_0

    .line 66
    const-class v13, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;

    const/4 v14, 0x1

    new-array v14, v14, [I

    const/4 v15, 0x0

    aput v2, v14, v15

    move-object/from16 v0, p0

    invoke-static {v0, v13, v14}, Lcom/google/android/finsky/widget/BaseWidgetProvider;->update(Landroid/content/Context;Ljava/lang/Class;[I)V

    .line 114
    :goto_0
    return-void

    .line 70
    :cond_0
    const/4 v8, 0x1

    .line 71
    .local v8, "flushCache":Z
    const/4 v12, 0x0

    .line 73
    .local v12, "recList":Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    :try_start_0
    move-object/from16 v0, p0

    invoke-static {v0, v4, v3, v11}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecommendations(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;ILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    move-result-object v12

    .line 75
    invoke-virtual {v12, v6}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->remove(Ljava/lang/String;)Z

    .line 76
    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->writeToDisk(Landroid/content/Context;)V

    .line 77
    const/4 v13, 0x1

    new-array v13, v13, [I

    const/4 v14, 0x0

    aput v2, v13, v14

    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->notifyDataSetChanged(Landroid/content/Context;[I)V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    const/4 v8, 0x0

    .line 86
    if-eqz v8, :cond_1

    .line 87
    const-string v13, "Invalidating cached recs for %d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    invoke-static {v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecsWidgetUrl(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-interface {v4, v13, v14}, Lcom/google/android/finsky/api/DfeApi;->invalidateListCache(Ljava/lang/String;Z)V

    .line 89
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->deleteCachedRecommendations(Landroid/content/Context;I)V

    .line 93
    :cond_1
    :goto_1
    const-string v13, "Dismissing id=[%s], url=[%s]"

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v6, v14, v15

    const/4 v15, 0x1

    aput-object v5, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v9

    .line 95
    .local v9, "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/RateSuggestedContentResponse;>;"
    invoke-interface {v4, v5, v9, v9}, Lcom/google/android/finsky/api/DfeApi;->rateSuggestedContent(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    move-result-object v10

    .line 96
    .local v10, "gtfoRequest":Lcom/android/volley/Request;, "Lcom/android/volley/Request<*>;"
    invoke-virtual {v9, v10}, Lcom/android/volley/toolbox/RequestFuture;->setRequest(Lcom/android/volley/Request;)V

    .line 99
    :try_start_1
    sget-object v13, Lcom/google/android/finsky/config/G;->recommendationsFetchTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v13}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Long;

    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    sget-object v13, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v9, v14, v15, v13}, Lcom/android/volley/toolbox/RequestFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    .line 101
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->needsBackfill()Z

    move-result v13

    if-eqz v13, :cond_2

    .line 102
    invoke-static {v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecsWidgetUrl(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-interface {v4, v13, v14}, Lcom/google/android/finsky/api/DfeApi;->invalidateListCache(Ljava/lang/String;Z)V

    .line 103
    move-object/from16 v0, p0

    invoke-static {v4, v0, v12, v11, v2}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->performBackFill(Lcom/google/android/finsky/api/DfeApi;Landroid/content/Context;Lcom/google/android/finsky/widget/recommendation/RecommendationList;Lcom/google/android/finsky/library/Library;I)V

    .line 106
    :cond_2
    const-string v13, "Dismissed %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v5, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_5

    goto/16 :goto_0

    .line 107
    :catch_0
    move-exception v7

    .line 108
    .local v7, "e":Ljava/lang/InterruptedException;
    const-string v13, "Interrupted while dismissing"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 79
    .end local v7    # "e":Ljava/lang/InterruptedException;
    .end local v9    # "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/RateSuggestedContentResponse;>;"
    .end local v10    # "gtfoRequest":Lcom/android/volley/Request;, "Lcom/android/volley/Request<*>;"
    :catch_1
    move-exception v7

    .line 80
    .local v7, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_2
    const-string v13, "Execution exception while fetching: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 86
    if-eqz v8, :cond_1

    .line 87
    const-string v13, "Invalidating cached recs for %d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    invoke-static {v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecsWidgetUrl(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-interface {v4, v13, v14}, Lcom/google/android/finsky/api/DfeApi;->invalidateListCache(Ljava/lang/String;Z)V

    .line 89
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->deleteCachedRecommendations(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 81
    .end local v7    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v7

    .line 82
    .local v7, "e":Ljava/lang/InterruptedException;
    :try_start_3
    const-string v13, "Interrupted while fetching: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 86
    if-eqz v8, :cond_1

    .line 87
    const-string v13, "Invalidating cached recs for %d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    invoke-static {v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecsWidgetUrl(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-interface {v4, v13, v14}, Lcom/google/android/finsky/api/DfeApi;->invalidateListCache(Ljava/lang/String;Z)V

    .line 89
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->deleteCachedRecommendations(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 83
    .end local v7    # "e":Ljava/lang/InterruptedException;
    :catch_3
    move-exception v7

    .line 84
    .local v7, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_4
    const-string v13, "Timed out while fetching: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 86
    if-eqz v8, :cond_1

    .line 87
    const-string v13, "Invalidating cached recs for %d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    invoke-static {v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecsWidgetUrl(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    invoke-interface {v4, v13, v14}, Lcom/google/android/finsky/api/DfeApi;->invalidateListCache(Ljava/lang/String;Z)V

    .line 89
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->deleteCachedRecommendations(Landroid/content/Context;I)V

    goto/16 :goto_1

    .line 86
    .end local v7    # "e":Ljava/util/concurrent/TimeoutException;
    :catchall_0
    move-exception v13

    if-eqz v8, :cond_3

    .line 87
    const-string v14, "Invalidating cached recs for %d"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    invoke-static {v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecsWidgetUrl(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    invoke-interface {v4, v14, v15}, Lcom/google/android/finsky/api/DfeApi;->invalidateListCache(Ljava/lang/String;Z)V

    .line 89
    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->deleteCachedRecommendations(Landroid/content/Context;I)V

    :cond_3
    throw v13

    .line 109
    .restart local v9    # "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/RateSuggestedContentResponse;>;"
    .restart local v10    # "gtfoRequest":Lcom/android/volley/Request;, "Lcom/android/volley/Request<*>;"
    :catch_4
    move-exception v7

    .line 110
    .local v7, "e":Ljava/util/concurrent/ExecutionException;
    const-string v13, "Exception while dismissing: %s"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v7, v14, v15

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 111
    .end local v7    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_5
    move-exception v7

    .line 112
    .local v7, "e":Ljava/util/concurrent/TimeoutException;
    const-string v13, "Timed out while dismissing"

    const/4 v14, 0x0

    new-array v14, v14, [Ljava/lang/Object;

    invoke-static {v13, v14}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
