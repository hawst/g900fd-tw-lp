.class public Lcom/google/android/finsky/layout/play/PlayCardClusterView;
.super Lcom/google/android/finsky/layout/play/PlayClusterView;
.source "PlayCardClusterView.java"


# instance fields
.field protected mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field protected mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

.field protected mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field protected mStreamDismissListener:Lcom/google/android/finsky/layout/play/PlayCardDismissListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method


# virtual methods
.method public createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p1, "metadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p2, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p3, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .param p7, "cardHeap"    # Lcom/google/android/finsky/layout/play/PlayCardHeap;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 92
    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 93
    iput-object p3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 94
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mStreamDismissListener:Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    .line 96
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setMetadata(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;)V

    .line 101
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getClusterLoggingDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v8

    .line 102
    .local v8, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    if-nez v8, :cond_0

    const/4 v9, 0x0

    .line 103
    .local v9, "serverLogsCookie":[B
    :goto_0
    move-object/from16 v0, p8

    invoke-virtual {p0, v9, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->configureLogging([BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 105
    move-object/from16 v0, p7

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/play/PlayCardHeap;->recycle(Lcom/google/android/finsky/layout/play/PlayCardClusterView;)V

    .line 106
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getParentOfChildren()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v7

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->createContent(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 108
    return-void

    .line 102
    .end local v9    # "serverLogsCookie":[B
    :cond_0
    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v9

    goto :goto_0
.end method

.method public getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;
    .locals 1
    .param p1, "cardIndex"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v0

    return-object v0
.end method

.method public getCardChildCount()I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildCount()I

    move-result v0

    return v0
.end method

.method public getMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v0

    return-object v0
.end method

.method public hasCards()Z
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getCardChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayClusterView;->onFinishInflate()V

    .line 44
    const v0, 0x7f0a02b7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    .line 45
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v5, 0x0

    .line 129
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getWidth()I

    move-result v1

    .line 130
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getPaddingTop()I

    move-result v2

    .line 132
    .local v2, "y":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 133
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v3, v5, v2, v1, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->layout(IIII)V

    .line 134
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v2, v3

    .line 137
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 139
    .local v0, "contentLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v3

    .line 140
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {v3, v5, v2, v1, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->layout(IIII)V

    .line 141
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v5, 0x0

    .line 111
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 112
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->getPaddingBottom()I

    move-result v4

    add-int v1, v3, v4

    .line 114
    .local v1, "height":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 115
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v3, p1, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->measure(II)V

    .line 116
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v1, v3

    .line 119
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 121
    .local v0, "contentLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v3, p1, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->measure(II)V

    .line 122
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 124
    invoke-virtual {p0, v2, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->setMeasuredDimension(II)V

    .line 125
    return-void
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayClusterView;->onRecycle()V

    .line 80
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onRecycle()V

    .line 81
    return-void
.end method

.method public removeAllCards()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->removeAllCards()V

    .line 57
    return-void
.end method

.method public setCardContentHorizontalPadding(I)V
    .locals 1
    .param p1, "cardContentHorizontalPadding"    # I

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setCardContentHorizontalPadding(I)V

    .line 85
    return-void
.end method

.method public withClusterDocumentData(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    .locals 1
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setClusterDocumentData(Lcom/google/android/finsky/api/model/Document;)V

    .line 69
    return-object p0
.end method

.method public withLooseDocumentsData(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    .locals 1
    .param p2, "parentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/finsky/layout/play/PlayCardClusterView;"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "looseDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setLooseDocumentsData(Ljava/util/List;Ljava/lang/String;)V

    .line 74
    return-object p0
.end method
