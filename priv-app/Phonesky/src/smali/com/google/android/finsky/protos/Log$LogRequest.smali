.class public final Lcom/google/android/finsky/protos/Log$LogRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Log.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Log;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LogRequest"
.end annotation


# instance fields
.field public clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 188
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Log$LogRequest;->clear()Lcom/google/android/finsky/protos/Log$LogRequest;

    .line 189
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Log$LogRequest;
    .locals 1

    .prologue
    .line 192
    invoke-static {}, Lcom/google/android/finsky/protos/Log$ClickLogEvent;->emptyArray()[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    .line 193
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->cachedSize:I

    .line 194
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 4

    .prologue
    .line 213
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 214
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 215
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 216
    iget-object v3, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    aget-object v0, v3, v1

    .line 217
    .local v0, "element":Lcom/google/android/finsky/protos/Log$ClickLogEvent;
    if-eqz v0, :cond_0

    .line 218
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 215
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 223
    .end local v0    # "element":Lcom/google/android/finsky/protos/Log$ClickLogEvent;
    .end local v1    # "i":I
    :cond_1
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Log$LogRequest;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 231
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 232
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 236
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 237
    :sswitch_0
    return-object p0

    .line 242
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 244
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    if-nez v5, :cond_2

    move v1, v4

    .line 245
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    .line 247
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Log$ClickLogEvent;
    if-eqz v1, :cond_1

    .line 248
    iget-object v5, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 250
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 251
    new-instance v5, Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Log$ClickLogEvent;-><init>()V

    aput-object v5, v2, v1

    .line 252
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 253
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 250
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 244
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Log$ClickLogEvent;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    array-length v1, v5

    goto :goto_1

    .line 256
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Log$ClickLogEvent;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Log$ClickLogEvent;-><init>()V

    aput-object v5, v2, v1

    .line 257
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 258
    iput-object v2, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    goto :goto_0

    .line 232
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 167
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Log$LogRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Log$LogRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 200
    iget-object v2, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 201
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 202
    iget-object v2, p0, Lcom/google/android/finsky/protos/Log$LogRequest;->clickEvent:[Lcom/google/android/finsky/protos/Log$ClickLogEvent;

    aget-object v0, v2, v1

    .line 203
    .local v0, "element":Lcom/google/android/finsky/protos/Log$ClickLogEvent;
    if-eqz v0, :cond_0

    .line 204
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 201
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    .end local v0    # "element":Lcom/google/android/finsky/protos/Log$ClickLogEvent;
    .end local v1    # "i":I
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 209
    return-void
.end method
