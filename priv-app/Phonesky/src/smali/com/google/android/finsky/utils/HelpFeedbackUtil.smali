.class public Lcom/google/android/finsky/utils/HelpFeedbackUtil;
.super Ljava/lang/Object;
.source "HelpFeedbackUtil.java"


# static fields
.field private static final FALLBACK_SUPPORT_URL:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/finsky/config/G;->supportUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/HelpFeedbackUtil;->FALLBACK_SUPPORT_URL:Landroid/net/Uri;

    return-void
.end method

.method private static getHelpContextForCorpus(Lcom/google/android/finsky/fragments/PageFragment;)Ljava/lang/String;
    .locals 2
    .param p0, "fragment"    # Lcom/google/android/finsky/fragments/PageFragment;

    .prologue
    .line 94
    instance-of v1, p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    if-nez v1, :cond_0

    .line 95
    const-string v1, "mobile_store_default"

    .line 111
    .end local p0    # "fragment":Lcom/google/android/finsky/fragments/PageFragment;
    .local v0, "backendId":I
    :goto_0
    return-object v1

    .line 97
    .end local v0    # "backendId":I
    .restart local p0    # "fragment":Lcom/google/android/finsky/fragments/PageFragment;
    :cond_0
    check-cast p0, Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    .end local p0    # "fragment":Lcom/google/android/finsky/fragments/PageFragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->getBackendId()I

    move-result v0

    .line 98
    .restart local v0    # "backendId":I
    packed-switch v0, :pswitch_data_0

    .line 111
    :pswitch_0
    const-string v1, "mobile_store_default"

    goto :goto_0

    .line 100
    :pswitch_1
    const-string v1, "mobile_books"

    goto :goto_0

    .line 102
    :pswitch_2
    const-string v1, "mobile_music"

    goto :goto_0

    .line 104
    :pswitch_3
    const-string v1, "mobile_apps"

    goto :goto_0

    .line 107
    :pswitch_4
    const-string v1, "mobile_movies"

    goto :goto_0

    .line 109
    :pswitch_5
    const-string v1, "mobile_newsstand"

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method private static getHelpContextForObject(Lcom/google/android/finsky/api/model/Document;)Ljava/lang/String;
    .locals 2
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 116
    if-nez p0, :cond_0

    .line 117
    const-string v1, "mobile_store_default"

    .line 133
    :goto_0
    return-object v1

    .line 119
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    .line 120
    .local v0, "backendId":I
    packed-switch v0, :pswitch_data_0

    .line 133
    :pswitch_0
    const-string v1, "mobile_store_default"

    goto :goto_0

    .line 122
    :pswitch_1
    const-string v1, "mobile_books_object"

    goto :goto_0

    .line 124
    :pswitch_2
    const-string v1, "mobile_music_object"

    goto :goto_0

    .line 126
    :pswitch_3
    const-string v1, "mobile_apps_object"

    goto :goto_0

    .line 129
    :pswitch_4
    const-string v1, "mobile_movies_object"

    goto :goto_0

    .line 131
    :pswitch_5
    const-string v1, "mobile_newsstand_object"

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public static launchHelpFeedback(Lcom/google/android/finsky/activities/MainActivity;)V
    .locals 6
    .param p0, "activity"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MainActivity;->getNavigationManager()Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-result-object v3

    .line 57
    .local v3, "navigationManager":Lcom/google/android/finsky/navigationmanager/NavigationManager;
    invoke-virtual {v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentPageType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 80
    :pswitch_0
    const-string v0, "mobile_store_default"

    .line 83
    .local v0, "helpContext":Ljava/lang/String;
    :goto_0
    new-instance v4, Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-direct {v4, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setGoogleAccount(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v4

    sget-object v5, Lcom/google/android/finsky/utils/HelpFeedbackUtil;->FALLBACK_SUPPORT_URL:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setFallbackSupportUri(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v4

    invoke-static {p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->getScreenshot(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setScreenshot(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v1

    .line 88
    .local v1, "helpInstance":Lcom/google/android/gms/googlehelp/GoogleHelp;
    invoke-virtual {v1, p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->buildHelpIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    .line 89
    .local v2, "helpIntent":Landroid/content/Intent;
    new-instance v4, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;

    invoke-direct {v4, p0}, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v4, v2}, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;->launch(Landroid/content/Intent;)V

    .line 90
    return-void

    .line 59
    .end local v0    # "helpContext":Ljava/lang/String;
    .end local v1    # "helpInstance":Lcom/google/android/gms/googlehelp/GoogleHelp;
    .end local v2    # "helpIntent":Landroid/content/Intent;
    :pswitch_1
    const-string v0, "mobile_home"

    .line 60
    .restart local v0    # "helpContext":Ljava/lang/String;
    goto :goto_0

    .line 62
    .end local v0    # "helpContext":Ljava/lang/String;
    :pswitch_2
    const-string v0, "mobile_search"

    .line 63
    .restart local v0    # "helpContext":Ljava/lang/String;
    goto :goto_0

    .line 65
    .end local v0    # "helpContext":Ljava/lang/String;
    :pswitch_3
    const-string v0, "mobile_my_apps"

    .line 66
    .restart local v0    # "helpContext":Ljava/lang/String;
    goto :goto_0

    .line 68
    .end local v0    # "helpContext":Ljava/lang/String;
    :pswitch_4
    const-string v0, "mobile_wishlist"

    .line 69
    .restart local v0    # "helpContext":Ljava/lang/String;
    goto :goto_0

    .line 71
    .end local v0    # "helpContext":Ljava/lang/String;
    :pswitch_5
    const-string v0, "mobile_people"

    .line 72
    .restart local v0    # "helpContext":Ljava/lang/String;
    goto :goto_0

    .line 74
    .end local v0    # "helpContext":Ljava/lang/String;
    :pswitch_6
    invoke-virtual {v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/utils/HelpFeedbackUtil;->getHelpContextForCorpus(Lcom/google/android/finsky/fragments/PageFragment;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .restart local v0    # "helpContext":Ljava/lang/String;
    goto :goto_0

    .line 77
    .end local v0    # "helpContext":Ljava/lang/String;
    :pswitch_7
    invoke-virtual {v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/utils/HelpFeedbackUtil;->getHelpContextForObject(Lcom/google/android/finsky/api/model/Document;)Ljava/lang/String;

    move-result-object v0

    .line 78
    .restart local v0    # "helpContext":Ljava/lang/String;
    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_6
        :pswitch_3
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
