.class public final Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CheckPromoOffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CheckPromoOffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AddCreditCardPromoOffer"
.end annotation


# instance fields
.field public descriptionHtml:Ljava/lang/String;

.field public hasDescriptionHtml:Z

.field public hasHeaderText:Z

.field public hasIntroductoryTextHtml:Z

.field public hasNoActionDescription:Z

.field public hasOfferTitle:Z

.field public hasTermsAndConditionsHtml:Z

.field public headerText:Ljava/lang/String;

.field public image:Lcom/google/android/finsky/protos/Common$Image;

.field public introductoryTextHtml:Ljava/lang/String;

.field public noActionDescription:Ljava/lang/String;

.field public offerTitle:Ljava/lang/String;

.field public termsAndConditionsHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 230
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 231
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->clear()Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    .line 232
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 235
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText:Ljava/lang/String;

    .line 236
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasHeaderText:Z

    .line 237
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml:Ljava/lang/String;

    .line 238
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasIntroductoryTextHtml:Z

    .line 239
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->offerTitle:Ljava/lang/String;

    .line 240
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasOfferTitle:Z

    .line 241
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml:Ljava/lang/String;

    .line 242
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasDescriptionHtml:Z

    .line 243
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml:Ljava/lang/String;

    .line 244
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasTermsAndConditionsHtml:Z

    .line 245
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 246
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription:Ljava/lang/String;

    .line 247
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasNoActionDescription:Z

    .line 248
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->cachedSize:I

    .line 249
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 281
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 282
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasHeaderText:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 283
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 286
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasDescriptionHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 287
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 290
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_4

    .line 291
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasIntroductoryTextHtml:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 295
    :cond_5
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasOfferTitle:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->offerTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 299
    :cond_7
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->offerTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 302
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasNoActionDescription:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 303
    :cond_9
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 306
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasTermsAndConditionsHtml:Z

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 307
    :cond_b
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 310
    :cond_c
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 318
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 319
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 323
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 324
    :sswitch_0
    return-object p0

    .line 329
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText:Ljava/lang/String;

    .line 330
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasHeaderText:Z

    goto :goto_0

    .line 334
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml:Ljava/lang/String;

    .line 335
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasDescriptionHtml:Z

    goto :goto_0

    .line 339
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_1

    .line 340
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 342
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 346
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml:Ljava/lang/String;

    .line 347
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasIntroductoryTextHtml:Z

    goto :goto_0

    .line 351
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->offerTitle:Ljava/lang/String;

    .line 352
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasOfferTitle:Z

    goto :goto_0

    .line 356
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription:Ljava/lang/String;

    .line 357
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasNoActionDescription:Z

    goto :goto_0

    .line 361
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml:Ljava/lang/String;

    .line 362
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasTermsAndConditionsHtml:Z

    goto :goto_0

    .line 319
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 186
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 255
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasHeaderText:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 256
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->headerText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 258
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasDescriptionHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 259
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 261
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_4

    .line 262
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 264
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasIntroductoryTextHtml:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 265
    :cond_5
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->introductoryTextHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 267
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasOfferTitle:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->offerTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 268
    :cond_7
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->offerTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 270
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasNoActionDescription:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 271
    :cond_9
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->noActionDescription:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 273
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->hasTermsAndConditionsHtml:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 274
    :cond_b
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$AddCreditCardPromoOffer;->termsAndConditionsHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 276
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 277
    return-void
.end method
