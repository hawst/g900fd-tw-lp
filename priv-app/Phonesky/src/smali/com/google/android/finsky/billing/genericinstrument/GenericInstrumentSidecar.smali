.class public Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
.super Lcom/google/android/finsky/fragments/SidecarFragment;
.source "GenericInstrumentSidecar.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$1;,
        Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;,
        Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$GetInitialInstrumentFlowStateListener;
    }
.end annotation


# instance fields
.field private final mCreateInstrumentListener:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;

.field private mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mErrorHtml:Ljava/lang/String;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mGenericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

.field private final mInitialFlowStateListener:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$GetInitialInstrumentFlowStateListener;

.field private mInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;-><init>()V

    .line 87
    new-instance v0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$GetInitialInstrumentFlowStateListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$GetInitialInstrumentFlowStateListener;-><init>(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialFlowStateListener:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$GetInitialInstrumentFlowStateListener;

    .line 89
    new-instance v0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;-><init>(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentListener:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;

    .line 325
    return-void
.end method

.method static synthetic access$1002(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;)Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$1500(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$202(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;)Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    return-object p1
.end method

.method static synthetic access$302(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mErrorHtml:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;IILcom/android/volley/VolleyError;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(IILcom/android/volley/VolleyError;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getInstrumentType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    return-void
.end method

.method private getInstrumentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mGenericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    iget-object v0, v0, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentMetadata:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;

    iget-object v0, v0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentMetadata;->instrumentType:Ljava/lang/String;

    return-object v0
.end method

.method private logBackgroundEvent(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 272
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(IILcom/android/volley/VolleyError;)V

    .line 273
    return-void
.end method

.method private logBackgroundEvent(IILcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "type"    # I
    .param p2, "errorCode"    # I
    .param p3, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 277
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setExceptionType(Ljava/lang/Throwable;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setErrorCode(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 280
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 281
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;)Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "genericInstrument"    # Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .prologue
    .line 128
    new-instance v1, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;-><init>()V

    .line 129
    .local v1, "result":Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 130
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v2, "GenericInstrumentSidecar.generic_instrument"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 132
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setArguments(Landroid/os/Bundle;)V

    .line 133
    return-object v1
.end method


# virtual methods
.method public createInstrument(Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 193
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getState()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 194
    const-string v0, "%s createInstrument() called while RUNNING."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getInstrumentType()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    :cond_0
    const/16 v0, 0x2c6

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(I)V

    .line 198
    iput-object v5, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .line 199
    iput-object v5, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mErrorHtml:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentListener:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;

    iget-object v2, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentListener:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$CreateInstrumentListener;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->createInstrument(Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 201
    invoke-virtual {p0, v3, v4}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    .line 202
    return-void
.end method

.method public getErrorHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mErrorHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getInstrumentFlowState()Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    .line 246
    :goto_0
    return-object v0

    .line 243
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->createInstrumentFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    goto :goto_0

    .line 246
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInstrumentId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 231
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Instrument requested while state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getState()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;->instrumentId:Ljava/lang/String;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 139
    .local v0, "args":Landroid/os/Bundle;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 140
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v2}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 141
    const-string v1, "GenericInstrumentSidecar.generic_instrument"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    iput-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mGenericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    .line 142
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onCreate(Landroid/os/Bundle;)V

    .line 143
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 147
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 148
    const-string v0, "GenericInstrumentSidecar.initial_instrument_flow_state_response"

    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 150
    const-string v0, "GenericInstrumentSidecar.create_instrument_response"

    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 152
    const-string v0, "GenericInstrumentSidecar.error_html"

    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mErrorHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method public requestInitialInstrumentFlowState()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 170
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getState()I

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "%s requestInitialInstrumentFlowState() called while state=%d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getInstrumentType()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getState()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 174
    :cond_0
    const/16 v0, 0x2bc

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->logBackgroundEvent(I)V

    .line 176
    iput-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    .line 177
    iput-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .line 178
    iput-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mErrorHtml:Ljava/lang/String;

    .line 180
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mGenericInstrument:Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;

    iget-object v1, v1, Lcom/google/android/finsky/protos/CommonDevice$GenericInstrument;->createInstrumentFlowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    iget-object v2, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialFlowStateListener:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$GetInitialInstrumentFlowStateListener;

    iget-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialFlowStateListener:Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar$GetInitialInstrumentFlowStateListener;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/api/DfeApi;->getInitialInstrumentFlowState(Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 182
    invoke-virtual {p0, v5, v4}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    .line 183
    return-void
.end method

.method protected restoreFromSavedInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->restoreFromSavedInstanceState(Landroid/os/Bundle;)V

    .line 158
    const-string v0, "GenericInstrumentSidecar.initial_instrument_flow_state_response"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    iput-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    .line 160
    const-string v0, "GenericInstrumentSidecar.create_instrument_response"

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    iput-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mCreateInstrumentResponse:Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentResponse;

    .line 162
    const-string v0, "GenericInstrumentSidecar.error_html"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mErrorHtml:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public retryAfterError()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 209
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getState()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 210
    const-string v1, "%s retryAfterError() called while state=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getInstrumentType()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->getState()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->mInitialInstrumentFlowStateResponse:Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;

    .line 215
    .local v0, "response":Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;
    if-eqz v0, :cond_1

    iget v1, v0, Lcom/google/android/finsky/protos/BuyInstruments$GetInitialInstrumentFlowStateResponse;->result:I

    if-eq v1, v5, :cond_2

    .line 217
    :cond_1
    invoke-virtual {p0, v4, v4}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    .line 222
    :goto_0
    return-void

    .line 220
    :cond_2
    const/4 v1, 0x4

    invoke-virtual {p0, v1, v4}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentSidecar;->setState(II)V

    goto :goto_0
.end method
