.class final Lcom/google/android/finsky/utils/PurchaseInitiator$2;
.super Ljava/lang/Object;
.source "PurchaseInitiator.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/PurchaseInitiator;->processPurchaseStatusResponse(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;ZLjava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$continueUrl:Ljava/lang/String;

.field final synthetic val$deliveryToken:Ljava/lang/String;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$initiateAppDownload:Z

.field final synthetic val$response:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;ZLcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$response:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    iput-boolean p2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$initiateAppDownload:Z

    iput-object p3, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p4, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$continueUrl:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$deliveryToken:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 158
    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$response:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    iget v0, v1, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->status:I

    .line 159
    .local v0, "status":I
    if-ne v0, v4, :cond_1

    .line 160
    iget-boolean v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$initiateAppDownload:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    if-ne v1, v4, :cond_1

    .line 162
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$continueUrl:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setContinueUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$response:Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Buy$PurchaseStatusResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-eqz v1, :cond_2

    .line 166
    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$deliveryToken:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 167
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$deliveryToken:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/google/android/finsky/receivers/Installer;->setDeliveryToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$account:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/PurchaseInitiator;->initiateDownload(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V

    .line 176
    :cond_1
    :goto_0
    return-void

    .line 172
    :cond_2
    const-string v1, "missing delivery data for %s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$2;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
