.class public Lcom/google/android/finsky/adapters/QuickLinkHelper;
.super Ljava/lang/Object;
.source "QuickLinkHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    }
.end annotation


# direct methods
.method public static getQuickLinksForStream(Landroid/content/Context;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "quickLinks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;>;"
    .local p2, "primaryStreamOutput":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;>;"
    .local p3, "fallbackStreamOutput":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;>;"
    const/4 v3, 0x0

    .line 52
    .local v3, "numRequiredQuickLinks":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 53
    .local v1, "link":Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mQuickLink:Lcom/google/android/finsky/protos/Browse$QuickLink;
    invoke-static {v1}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$000(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v6

    iget-boolean v6, v6, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired:Z

    if-eqz v6, :cond_0

    .line 54
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 59
    .end local v1    # "link":Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    sub-int/2addr v7, v3

    invoke-static {v6, v3, v7}, Lcom/google/android/finsky/utils/UiUtils;->getStreamQuickLinkColumnCount(Landroid/content/res/Resources;II)I

    move-result v2

    .line 62
    .local v2, "numQuickLinksPerRow":I
    int-to-double v6, v3

    int-to-double v8, v2

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v4, v6

    .line 63
    .local v4, "numRowsRequired":I
    mul-int v6, v4, v2

    sub-int v5, v6, v3

    .line 65
    .local v5, "remainder":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 66
    .restart local v1    # "link":Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mQuickLink:Lcom/google/android/finsky/protos/Browse$QuickLink;
    invoke-static {v1}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$000(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v6

    iget-boolean v6, v6, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired:Z

    if-nez v6, :cond_2

    .line 67
    if-lez v5, :cond_3

    .line 68
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v5, v5, -0x1

    goto :goto_1

    .line 71
    :cond_3
    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 75
    .end local v1    # "link":Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    :cond_4
    return-void
.end method

.method public static getQuickLinksRow(Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/view/LayoutInflater;Lcom/google/android/play/image/BitmapLoader;Landroid/view/ViewGroup;Landroid/view/ViewGroup;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;IILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View;
    .locals 21
    .param p0, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p1, "navMgr"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "convertView"    # Landroid/view/ViewGroup;
    .param p5, "parent"    # Landroid/view/ViewGroup;
    .param p6, "quickLinks"    # [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    .param p7, "quickLinkRowNum"    # I
    .param p8, "numQuickLinksPerRow"    # I
    .param p9, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 84
    move-object/from16 v0, p6

    array-length v3, v0

    move/from16 v0, p8

    invoke-static {v3, v0}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v20

    .line 85
    .local v20, "totalRowCount":I
    add-int/lit8 v3, v20, -0x1

    move/from16 v0, p7

    if-ne v0, v3, :cond_1

    const/4 v14, 0x1

    .line 86
    .local v14, "isLastRow":Z
    :goto_0
    move-object/from16 v0, p6

    array-length v0, v0

    move/from16 v16, v0

    .line 87
    .local v16, "numQuickLinks":I
    mul-int v17, p7, p8

    .line 95
    .local v17, "offset":I
    const/4 v15, 0x0

    .line 96
    .local v15, "needToInflateChildren":Z
    if-nez p4, :cond_0

    .line 97
    const v3, 0x7f04016b

    const/4 v4, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .end local p4    # "convertView":Landroid/view/ViewGroup;
    check-cast p4, Landroid/view/ViewGroup;

    .line 99
    .restart local p4    # "convertView":Landroid/view/ViewGroup;
    const/4 v15, 0x1

    .line 102
    :cond_0
    if-eqz v15, :cond_2

    .line 103
    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 104
    const/4 v12, 0x0

    .local v12, "columnNum":I
    :goto_1
    move/from16 v0, p8

    if-ge v12, v0, :cond_2

    .line 105
    const v11, 0x7f04016a

    .line 106
    .local v11, "childLayoutId":I
    const/4 v3, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v0, v11, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 104
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 85
    .end local v11    # "childLayoutId":I
    .end local v12    # "columnNum":I
    .end local v14    # "isLastRow":Z
    .end local v15    # "needToInflateChildren":Z
    .end local v16    # "numQuickLinks":I
    .end local v17    # "offset":I
    :cond_1
    const/4 v14, 0x0

    goto :goto_0

    .line 111
    .restart local v14    # "isLastRow":Z
    .restart local v15    # "needToInflateChildren":Z
    .restart local v16    # "numQuickLinks":I
    .restart local v17    # "offset":I
    :cond_2
    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 112
    .local v19, "res":Landroid/content/res/Resources;
    const v3, 0x7f0b014a

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 116
    .local v9, "blockPaddingBottom":I
    const/4 v12, 0x0

    .restart local v12    # "columnNum":I
    :goto_2
    invoke-virtual/range {p4 .. p4}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v12, v3, :cond_4

    .line 117
    move-object/from16 v0, p4

    invoke-virtual {v0, v12}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;

    .line 118
    .local v2, "cell":Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;
    add-int v13, v17, v12

    .line 119
    .local v13, "index":I
    move/from16 v0, v16

    if-lt v13, v0, :cond_3

    .line 120
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setVisibility(I)V

    .line 116
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 123
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->setVisibility(I)V

    .line 124
    aget-object v18, p6, v13

    .line 125
    .local v18, "quickLinkInfo":Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mQuickLink:Lcom/google/android/finsky/protos/Browse$QuickLink;
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$000(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v3

    # getter for: Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->mBackendId:I
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;->access$100(Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;)I

    move-result v4

    move-object/from16 v5, p1

    move-object/from16 v6, p0

    move-object/from16 v7, p3

    move-object/from16 v8, p9

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;->bind(Lcom/google/android/finsky/protos/Browse$QuickLink;ILcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_3

    .line 129
    .end local v2    # "cell":Lcom/google/android/finsky/layout/play/PlayQuickLinkBase;
    .end local v13    # "index":I
    .end local v18    # "quickLinkInfo":Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    :cond_4
    if-eqz v14, :cond_5

    move v10, v9

    .line 130
    .local v10, "bottomPadding":I
    :goto_4
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v4, v5, v10}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 131
    return-object p4

    .line 129
    .end local v10    # "bottomPadding":I
    :cond_5
    const/4 v10, 0x0

    goto :goto_4
.end method
