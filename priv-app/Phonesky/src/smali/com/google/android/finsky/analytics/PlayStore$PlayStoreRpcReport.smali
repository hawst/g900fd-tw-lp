.class public final Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreRpcReport"
.end annotation


# instance fields
.field public backoffMultiplier:F

.field public clientLatencyMs:J

.field public endConnectionType:I

.field public hasBackoffMultiplier:Z

.field public hasClientLatencyMs:Z

.field public hasEndConnectionType:Z

.field public hasNumAttempts:Z

.field public hasResponseBodySizeBytes:Z

.field public hasServerLatencyMs:Z

.field public hasStartConnectionType:Z

.field public hasTimeoutMs:Z

.field public hasUrl:Z

.field public hasVolleyErrorType:Z

.field public hasWasSuccessful:Z

.field public numAttempts:I

.field public responseBodySizeBytes:I

.field public serverLatencyMs:J

.field public startConnectionType:I

.field public timeoutMs:I

.field public url:Ljava/lang/String;

.field public volleyErrorType:I

.field public wasSuccessful:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1594
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1595
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    .line 1596
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1599
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->url:Ljava/lang/String;

    .line 1600
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasUrl:Z

    .line 1601
    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->clientLatencyMs:J

    .line 1602
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasClientLatencyMs:Z

    .line 1603
    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->serverLatencyMs:J

    .line 1604
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasServerLatencyMs:Z

    .line 1605
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->numAttempts:I

    .line 1606
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasNumAttempts:Z

    .line 1607
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->timeoutMs:I

    .line 1608
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasTimeoutMs:Z

    .line 1609
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->backoffMultiplier:F

    .line 1610
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasBackoffMultiplier:Z

    .line 1611
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->wasSuccessful:Z

    .line 1612
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasWasSuccessful:Z

    .line 1613
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->startConnectionType:I

    .line 1614
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasStartConnectionType:Z

    .line 1615
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->endConnectionType:I

    .line 1616
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasEndConnectionType:Z

    .line 1617
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->responseBodySizeBytes:I

    .line 1618
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasResponseBodySizeBytes:Z

    .line 1619
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->volleyErrorType:I

    .line 1620
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasVolleyErrorType:Z

    .line 1621
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->cachedSize:I

    .line 1622
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1667
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1668
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->url:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1669
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1672
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasClientLatencyMs:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->clientLatencyMs:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 1673
    :cond_2
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->clientLatencyMs:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1676
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasServerLatencyMs:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->serverLatencyMs:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 1677
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->serverLatencyMs:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1680
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasNumAttempts:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->numAttempts:I

    if-eqz v1, :cond_7

    .line 1681
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->numAttempts:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1684
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasTimeoutMs:Z

    if-nez v1, :cond_8

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->timeoutMs:I

    if-eqz v1, :cond_9

    .line 1685
    :cond_8
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->timeoutMs:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1688
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasBackoffMultiplier:Z

    if-nez v1, :cond_a

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->backoffMultiplier:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_b

    .line 1690
    :cond_a
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->backoffMultiplier:F

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeFloatSize(IF)I

    move-result v1

    add-int/2addr v0, v1

    .line 1693
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasWasSuccessful:Z

    if-nez v1, :cond_c

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->wasSuccessful:Z

    if-eqz v1, :cond_d

    .line 1694
    :cond_c
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->wasSuccessful:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1697
    :cond_d
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->startConnectionType:I

    if-nez v1, :cond_e

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasStartConnectionType:Z

    if-eqz v1, :cond_f

    .line 1698
    :cond_e
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->startConnectionType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1701
    :cond_f
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->endConnectionType:I

    if-nez v1, :cond_10

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasEndConnectionType:Z

    if-eqz v1, :cond_11

    .line 1702
    :cond_10
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->endConnectionType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1705
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasResponseBodySizeBytes:Z

    if-nez v1, :cond_12

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->responseBodySizeBytes:I

    if-eqz v1, :cond_13

    .line 1706
    :cond_12
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->responseBodySizeBytes:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1709
    :cond_13
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->volleyErrorType:I

    if-nez v1, :cond_14

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasVolleyErrorType:Z

    if-eqz v1, :cond_15

    .line 1710
    :cond_14
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->volleyErrorType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1713
    :cond_15
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1721
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1722
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1726
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1727
    :sswitch_0
    return-object p0

    .line 1732
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->url:Ljava/lang/String;

    .line 1733
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasUrl:Z

    goto :goto_0

    .line 1737
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->clientLatencyMs:J

    .line 1738
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasClientLatencyMs:Z

    goto :goto_0

    .line 1742
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->serverLatencyMs:J

    .line 1743
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasServerLatencyMs:Z

    goto :goto_0

    .line 1747
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->numAttempts:I

    .line 1748
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasNumAttempts:Z

    goto :goto_0

    .line 1752
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->timeoutMs:I

    .line 1753
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasTimeoutMs:Z

    goto :goto_0

    .line 1757
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readFloat()F

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->backoffMultiplier:F

    .line 1758
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasBackoffMultiplier:Z

    goto :goto_0

    .line 1762
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->wasSuccessful:Z

    .line 1763
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasWasSuccessful:Z

    goto :goto_0

    .line 1767
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1768
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1776
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->startConnectionType:I

    .line 1777
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasStartConnectionType:Z

    goto :goto_0

    .line 1783
    .end local v1    # "value":I
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1784
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 1792
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->endConnectionType:I

    .line 1793
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasEndConnectionType:Z

    goto :goto_0

    .line 1799
    .end local v1    # "value":I
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->responseBodySizeBytes:I

    .line 1800
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasResponseBodySizeBytes:Z

    goto :goto_0

    .line 1804
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1805
    .restart local v1    # "value":I
    packed-switch v1, :pswitch_data_2

    goto :goto_0

    .line 1812
    :pswitch_2
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->volleyErrorType:I

    .line 1813
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasVolleyErrorType:Z

    goto :goto_0

    .line 1722
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x35 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch

    .line 1768
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1784
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 1805
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1516
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 1628
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->url:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1629
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->url:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1631
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasClientLatencyMs:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->clientLatencyMs:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 1632
    :cond_2
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->clientLatencyMs:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 1634
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasServerLatencyMs:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->serverLatencyMs:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 1635
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->serverLatencyMs:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt64(IJ)V

    .line 1637
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasNumAttempts:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->numAttempts:I

    if-eqz v0, :cond_7

    .line 1638
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->numAttempts:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 1640
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasTimeoutMs:Z

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->timeoutMs:I

    if-eqz v0, :cond_9

    .line 1641
    :cond_8
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->timeoutMs:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 1643
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasBackoffMultiplier:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->backoffMultiplier:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_b

    .line 1645
    :cond_a
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->backoffMultiplier:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeFloat(IF)V

    .line 1647
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasWasSuccessful:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->wasSuccessful:Z

    if-eqz v0, :cond_d

    .line 1648
    :cond_c
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->wasSuccessful:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1650
    :cond_d
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->startConnectionType:I

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasStartConnectionType:Z

    if-eqz v0, :cond_f

    .line 1651
    :cond_e
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->startConnectionType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1653
    :cond_f
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->endConnectionType:I

    if-nez v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasEndConnectionType:Z

    if-eqz v0, :cond_11

    .line 1654
    :cond_10
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->endConnectionType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1656
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasResponseBodySizeBytes:Z

    if-nez v0, :cond_12

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->responseBodySizeBytes:I

    if-eqz v0, :cond_13

    .line 1657
    :cond_12
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->responseBodySizeBytes:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 1659
    :cond_13
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->volleyErrorType:I

    if-nez v0, :cond_14

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->hasVolleyErrorType:Z

    if-eqz v0, :cond_15

    .line 1660
    :cond_14
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreRpcReport;->volleyErrorType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1662
    :cond_15
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1663
    return-void
.end method
