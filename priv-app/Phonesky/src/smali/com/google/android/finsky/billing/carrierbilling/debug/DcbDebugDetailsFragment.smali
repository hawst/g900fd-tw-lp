.class public Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "DcbDebugDetailsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static create(Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDetailExtractor;Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment;
    .locals 10
    .param p0, "detailExtractor"    # Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDetailExtractor;
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-interface {p0}, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDetailExtractor;->getDetails()Ljava/util/Collection;

    move-result-object v3

    .line 33
    .local v3, "details":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDetail;>;"
    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v8

    new-array v2, v8, [Ljava/lang/String;

    .line 34
    .local v2, "detailArray":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 35
    .local v6, "index":I
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDetail;

    .line 36
    .local v1, "detail":Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDetail;
    add-int/lit8 v7, v6, 0x1

    .end local v6    # "index":I
    .local v7, "index":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v1}, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDetail;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ": "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v1}, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDetail;->getValue()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v2, v6

    move v6, v7

    .line 37
    .end local v7    # "index":I
    .restart local v6    # "index":I
    goto :goto_0

    .line 39
    .end local v1    # "detail":Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDetail;
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 40
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v8, "formatted_details"

    invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 41
    const-string v8, "title"

    invoke-virtual {v0, v8, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    new-instance v4, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment;

    invoke-direct {v4}, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment;-><init>()V

    .line 44
    .local v4, "fragment":Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment;
    invoke-virtual {v4, v0}, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 45
    return-object v4
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    new-instance v2, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const v8, 0x7f0d01b1

    invoke-direct {v2, v7, v8}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 53
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 54
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v7, "formatted_details"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 55
    .local v3, "details":[Ljava/lang/String;
    const-string v7, "title"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 58
    .local v6, "title":Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 59
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v7, 0x7f0c02a0

    new-instance v8, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment$1;

    invoke-direct {v8, p0}, Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment$1;-><init>(Lcom/google/android/finsky/billing/carrierbilling/debug/DcbDebugDetailsFragment;)V

    invoke-virtual {v1, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 65
    const/4 v7, 0x0

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 66
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 67
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    .line 69
    .local v4, "dialog":Landroid/app/AlertDialog;
    new-instance v5, Landroid/widget/ListView;

    invoke-direct {v5, v2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 70
    .local v5, "dialogView":Landroid/widget/ListView;
    new-instance v7, Landroid/widget/ArrayAdapter;

    const v8, 0x7f040051

    invoke-direct {v7, v2, v8, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 73
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 74
    return-object v4
.end method
