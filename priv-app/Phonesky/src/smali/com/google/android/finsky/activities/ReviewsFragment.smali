.class public Lcom/google/android/finsky/activities/ReviewsFragment;
.super Lcom/google/android/finsky/fragments/PageFragment;
.source "ReviewsFragment.java"

# interfaces
.implements Lcom/google/android/finsky/activities/ReviewFeedbackDialog$Listener;
.implements Lcom/google/android/finsky/activities/ReviewsFilterOptionsDialog$Listener;
.implements Lcom/google/android/finsky/activities/ReviewsSortingDialog$Listener;


# instance fields
.field protected mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

.field protected mDocument:Lcom/google/android/finsky/api/model/Document;

.field private mFilterByDevice:Z

.field private mFilterByVersion:Z

.field private mIsRottenTomatoesReviews:Z

.field protected final mReviewsBinder:Lcom/google/android/finsky/activities/ReviewListViewBinder;

.field private mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

.field private mReviewsUrl:Ljava/lang/String;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mSortType:I

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/PageFragment;-><init>()V

    .line 44
    new-instance v0, Lcom/google/android/finsky/activities/ReviewListViewBinder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/ReviewListViewBinder;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsBinder:Lcom/google/android/finsky/activities/ReviewListViewBinder;

    .line 61
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    .line 64
    const/16 v0, 0x12e

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/ReviewsFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/ReviewsFragment;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->reloadReviews()V

    return-void
.end method

.method public static newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ReviewsFragment;
    .locals 3
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "reviewsUrlOverride"    # Ljava/lang/String;
    .param p2, "isRottenTomatoesReviews"    # Z

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/finsky/activities/ReviewsFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/ReviewsFragment;-><init>()V

    .line 77
    .local v0, "fragment":Lcom/google/android/finsky/activities/ReviewsFragment;
    const v2, 0x7f0d01b8

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/ReviewsFragment;->setTheme(I)V

    .line 78
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/ReviewsFragment;->setDfeToc(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 79
    const-string v2, "finsky.ReviewsFragment.document"

    invoke-virtual {v0, v2, p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->setArgument(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 80
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getReviewsUrl()Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "reviewsUrl":Ljava/lang/String;
    :goto_0
    const-string v2, "finsky.ReviewsFragment.reviewsUrl"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/finsky/activities/ReviewsFragment;->setArgument(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v2, "finsky.ReviewsFragment.isRottenTomatoesReviews"

    invoke-virtual {v0, v2, p2}, Lcom/google/android/finsky/activities/ReviewsFragment;->setArgument(Ljava/lang/String;Z)V

    .line 84
    return-object v0

    .end local v1    # "reviewsUrl":Ljava/lang/String;
    :cond_0
    move-object v1, p1

    .line 80
    goto :goto_0
.end method

.method private recordState()V
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v1, "finsky.PageFragment.ReviewsFragment.filterByDevice"

    iget-boolean v2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mFilterByDevice:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v1, "finsky.PageFragment.ReviewsFragment.filterByVersion"

    iget-boolean v2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mFilterByVersion:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v1, "finsky.PageFragment.ReviewsFragment.sortType"

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeReviews;->getSortType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 145
    :cond_0
    return-void
.end method

.method private reloadReviews()V
    .locals 7

    .prologue
    .line 256
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeReviews;->shouldFilterByDevice()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeReviews;->getVersionFilter()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/DfeReviews;->getRatingFilter()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/DfeReviews;->getSortType()I

    move-result v5

    const/4 v6, 0x1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/finsky/api/DfeApi;->invalidateReviewsCache(Ljava/lang/String;ZIIIZ)V

    .line 259
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeReviews;->resetItems()V

    .line 260
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->switchToLoading()V

    .line 261
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->requestData()V

    .line 264
    return-void
.end method


# virtual methods
.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 98
    const v0, 0x7f0400ad

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeReviews;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 103
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 106
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 107
    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    .line 110
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mIsRottenTomatoesReviews:Z

    if-eqz v1, :cond_3

    const/4 v0, -0x1

    .line 112
    .local v0, "defaultSortType":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v2, "finsky.PageFragment.ReviewsFragment.filterByDevice"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mFilterByDevice:Z

    .line 113
    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v2, "finsky.PageFragment.ReviewsFragment.filterByVersion"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mFilterByVersion:Z

    .line 114
    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    const-string v2, "finsky.PageFragment.ReviewsFragment.sortType"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSortType:I

    .line 116
    new-instance v1, Lcom/google/android/finsky/api/model/DfeDetails;

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 117
    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    if-nez v1, :cond_1

    .line 118
    new-instance v1, Lcom/google/android/finsky/api/model/DfeReviews;

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getVersionCode()I

    move-result v4

    const/4 v5, 0x1

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/api/model/DfeReviews;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;IZ)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    .line 119
    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 120
    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 122
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    iget-boolean v2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mFilterByVersion:Z

    iget-boolean v3, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mFilterByDevice:Z

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/api/model/DfeReviews;->setFilters(ZZ)V

    .line 123
    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    iget v2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSortType:I

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/api/model/DfeReviews;->setSortType(I)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->isDataReady()Z

    move-result v1

    if-nez v1, :cond_2

    .line 125
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->switchToLoading()V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->requestData()V

    .line 128
    :cond_2
    return-void

    .line 110
    .end local v0    # "defaultSortType":I
    :cond_3
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 89
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "finsky.ReviewsFragment.document"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 91
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "finsky.ReviewsFragment.reviewsUrl"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsUrl:Ljava/lang/String;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "finsky.ReviewsFragment.isRottenTomatoesReviews"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mIsRottenTomatoesReviews:Z

    .line 93
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/ReviewsFragment;->setRetainInstance(Z)V

    .line 94
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 150
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/PageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 151
    .local v0, "result":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->isDataReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->onDataChanged()V

    .line 158
    :cond_0
    return-object v0
.end method

.method public onDataChanged()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 192
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsBinder:Lcom/google/android/finsky/activities/ReviewListViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/ReviewListViewBinder;->setData(Lcom/google/android/finsky/api/model/DfeReviews;)V

    .line 195
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDataChanged()V

    .line 196
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsBinder:Lcom/google/android/finsky/activities/ReviewListViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/ReviewListViewBinder;->onDestroyView()V

    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeReviews;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 205
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDestroyView()V

    .line 206
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 7

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsBinder:Lcom/google/android/finsky/activities/ReviewListViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v4, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v5, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object v2, p0

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/activities/ReviewListViewBinder;->init(Landroid/content/Context;Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 169
    return-void
.end method

.method public onReviewFeedback(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V
    .locals 3
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "reviewId"    # Ljava/lang/String;
    .param p3, "newRating"    # Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;

    .prologue
    .line 237
    new-instance v0, Lcom/google/android/finsky/api/model/DfeRateReview;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;->getRpcId()I

    move-result v2

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/google/android/finsky/api/model/DfeRateReview;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Ljava/lang/String;I)V

    .line 239
    .local v0, "request":Lcom/google/android/finsky/api/model/DfeRateReview;
    new-instance v1, Lcom/google/android/finsky/activities/ReviewsFragment$1;

    invoke-direct {v1, p0, p3}, Lcom/google/android/finsky/activities/ReviewsFragment$1;-><init>(Lcom/google/android/finsky/activities/ReviewsFragment;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeRateReview;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 247
    new-instance v1, Lcom/google/android/finsky/activities/ReviewsFragment$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/ReviewsFragment$2;-><init>(Lcom/google/android/finsky/activities/ReviewsFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/DfeRateReview;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 253
    return-void
.end method

.method public onReviewFilterChanged(ZZ)V
    .locals 1
    .param p1, "filterByVersion"    # Z
    .param p2, "filterByDevice"    # Z

    .prologue
    .line 214
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mFilterByVersion:Z

    .line 215
    iput-boolean p2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mFilterByDevice:Z

    .line 216
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/api/model/DfeReviews;->setFilters(ZZ)V

    .line 217
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeReviews;->refetchReviews()V

    .line 218
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->recordState()V

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 134
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 135
    return-void
.end method

.method public onSortingChanged(I)V
    .locals 1
    .param p1, "dataSortType"    # I

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/api/model/DfeReviews;->setSortType(I)V

    .line 227
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeReviews;->refetchReviews()V

    .line 228
    return-void
.end method

.method public rebindActionBar()V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 180
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 181
    return-void
.end method

.method protected rebindViews()V
    .locals 4

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ReviewsFragment;->rebindActionBar()V

    .line 174
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsBinder:Lcom/google/android/finsky/activities/ReviewListViewBinder;

    iget-object v1, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDataView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mDocument:Lcom/google/android/finsky/api/model/Document;

    iget-boolean v3, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mIsRottenTomatoesReviews:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/activities/ReviewListViewBinder;->bind(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Z)V

    .line 175
    return-void
.end method

.method protected requestData()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mReviewsData:Lcom/google/android/finsky/api/model/DfeReviews;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeReviews;->startLoadItems()V

    .line 186
    return-void
.end method

.method protected toast(II)V
    .locals 1
    .param p1, "resourceId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 271
    return-void
.end method
