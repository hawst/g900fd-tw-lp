.class Lcom/google/android/finsky/layout/DetailsPackSection$2;
.super Ljava/lang/Object;
.source "DetailsPackSection.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsPackSection;->populateHeader()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsPackSection;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsPackSection;)V
    .locals 0

    .prologue
    .line 340
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsPackSection$2;->this$0:Lcom/google/android/finsky/layout/DetailsPackSection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsPackSection$2;->this$0:Lcom/google/android/finsky/layout/DetailsPackSection;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsPackSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsPackSection$2;->this$0:Lcom/google/android/finsky/layout/DetailsPackSection;

    # getter for: Lcom/google/android/finsky/layout/DetailsPackSection;->mMoreUrl:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/finsky/layout/DetailsPackSection;->access$000(Lcom/google/android/finsky/layout/DetailsPackSection;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsPackSection$2;->this$0:Lcom/google/android/finsky/layout/DetailsPackSection;

    iget-object v3, v3, Lcom/google/android/finsky/layout/DetailsPackSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsPackSection$2;->this$0:Lcom/google/android/finsky/layout/DetailsPackSection;

    iget-object v4, v4, Lcom/google/android/finsky/layout/DetailsPackSection;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsPackSection$2;->this$0:Lcom/google/android/finsky/layout/DetailsPackSection;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBrowse(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 345
    return-void
.end method
