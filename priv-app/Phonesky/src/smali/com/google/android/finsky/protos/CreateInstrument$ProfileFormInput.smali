.class public final Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProfileFormInput"
.end annotation


# instance fields
.field public customerAddress:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

.field public tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1158
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1159
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->clear()Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    .line 1160
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1163
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->customerAddress:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    .line 1164
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    .line 1165
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->cachedSize:I

    .line 1166
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1183
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1184
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->customerAddress:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    if-eqz v1, :cond_0

    .line 1185
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->customerAddress:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1188
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    if-eqz v1, :cond_1

    .line 1189
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1192
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1200
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1201
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1205
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1206
    :sswitch_0
    return-object p0

    .line 1211
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->customerAddress:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    if-nez v1, :cond_1

    .line 1212
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->customerAddress:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    .line 1214
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->customerAddress:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1218
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    if-nez v1, :cond_2

    .line 1219
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    .line 1221
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1201
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1135
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1172
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->customerAddress:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    if-eqz v0, :cond_0

    .line 1173
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->customerAddress:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormInput;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1175
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    if-eqz v0, :cond_1

    .line 1176
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1178
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1179
    return-void
.end method
