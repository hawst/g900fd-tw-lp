.class public Lcom/google/android/finsky/placesapi/PlaceDetailResponse;
.super Ljava/lang/Object;
.source "PlaceDetailResponse.java"


# instance fields
.field private mAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/protos/BillingAddress$Address;)V
    .locals 0
    .param p1, "address"    # Lcom/google/android/finsky/protos/BillingAddress$Address;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/finsky/placesapi/PlaceDetailResponse;->mAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 21
    return-void
.end method

.method public static parseFromJson(Lorg/json/JSONObject;Lcom/google/android/finsky/placesapi/AdrMicroformatParser;)Lcom/google/android/finsky/placesapi/PlaceDetailResponse;
    .locals 5
    .param p0, "json"    # Lorg/json/JSONObject;
    .param p1, "parser"    # Lcom/google/android/finsky/placesapi/AdrMicroformatParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 40
    const-string v4, "adr_address"

    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 43
    .local v2, "formattedAddress":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1, v2}, Lcom/google/android/finsky/placesapi/AdrMicroformatParser;->parse(Ljava/lang/String;)Lcom/google/android/finsky/protos/BillingAddress$Address;
    :try_end_0
    .catch Lcom/google/android/finsky/placesapi/AdrMicroformatParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 49
    .local v0, "address":Lcom/google/android/finsky/protos/BillingAddress$Address;
    new-instance v4, Lcom/google/android/finsky/placesapi/PlaceDetailResponse;

    invoke-direct {v4, v0}, Lcom/google/android/finsky/placesapi/PlaceDetailResponse;-><init>(Lcom/google/android/finsky/protos/BillingAddress$Address;)V

    return-object v4

    .line 44
    .end local v0    # "address":Lcom/google/android/finsky/protos/BillingAddress$Address;
    :catch_0
    move-exception v1

    .line 45
    .local v1, "e":Lcom/google/android/finsky/placesapi/AdrMicroformatParserException;
    new-instance v3, Lorg/json/JSONException;

    invoke-virtual {v1}, Lcom/google/android/finsky/placesapi/AdrMicroformatParserException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    .line 46
    .local v3, "jsonException":Lorg/json/JSONException;
    invoke-virtual {v3, v1}, Lorg/json/JSONException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 47
    throw v3
.end method


# virtual methods
.method public getAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/finsky/placesapi/PlaceDetailResponse;->mAddress:Lcom/google/android/finsky/protos/BillingAddress$Address;

    return-object v0
.end method
