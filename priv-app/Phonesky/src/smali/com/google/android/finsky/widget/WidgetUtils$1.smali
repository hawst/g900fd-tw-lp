.class final Lcom/google/android/finsky/widget/WidgetUtils$1;
.super Ljava/lang/Object;
.source "WidgetUtils.java"

# interfaces
.implements Lcom/google/android/finsky/library/LibraryReplicators$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/widget/WidgetUtils;->registerLibraryMutationsListener(Landroid/content/Context;Lcom/google/android/finsky/library/LibraryReplicators;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/android/finsky/widget/WidgetUtils$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMutationsApplied(Lcom/google/android/finsky/library/AccountLibrary;Ljava/lang/String;)V
    .locals 4
    .param p1, "library"    # Lcom/google/android/finsky/library/AccountLibrary;
    .param p2, "libraryId"    # Ljava/lang/String;

    .prologue
    .line 134
    # invokes: Lcom/google/android/finsky/widget/WidgetUtils;->getWidgetTypeForLibraryId(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {p2}, Lcom/google/android/finsky/widget/WidgetUtils;->access$000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "type":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 136
    iget-object v3, p0, Lcom/google/android/finsky/widget/WidgetUtils$1;->val$context:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/finsky/widget/WidgetTypeMap;->get(Landroid/content/Context;)Lcom/google/android/finsky/widget/WidgetTypeMap;

    move-result-object v2

    .line 137
    .local v2, "typeMap":Lcom/google/android/finsky/widget/WidgetTypeMap;
    const-class v3, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/finsky/widget/WidgetTypeMap;->getWidgets(Ljava/lang/Class;Ljava/lang/String;)[I

    move-result-object v0

    .line 138
    .local v0, "appWidgetIds":[I
    array-length v3, v0

    if-lez v3, :cond_0

    .line 139
    iget-object v3, p0, Lcom/google/android/finsky/widget/WidgetUtils$1;->val$context:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->notifyDataSetChanged(Landroid/content/Context;[I)V

    .line 142
    .end local v0    # "appWidgetIds":[I
    .end local v2    # "typeMap":Lcom/google/android/finsky/widget/WidgetTypeMap;
    :cond_0
    return-void
.end method
