.class public Lcom/google/android/finsky/api/DfeRequest;
.super Lcom/android/volley/Request;
.source "DfeRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Lcom/android/volley/Request",
        "<",
        "Lcom/google/android/finsky/protos/Response$ResponseWrapper;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEBUG:Z

.field private static final PROTO_DEBUG:Z


# instance fields
.field private mAllowMultipleResponses:Z

.field private final mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

.field private mAvoidBulkCancel:Z

.field private mExtraHeaders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/android/volley/Response$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Response$Listener",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final mResponseClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field private mResponseDelivered:Z

.field private mResponseVerifier:Lcom/google/android/play/dfe/api/DfeResponseVerifier;

.field private mServerLatencyMs:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 51
    sget-boolean v0, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    sput-boolean v0, Lcom/google/android/finsky/api/DfeRequest;->DEBUG:Z

    .line 53
    const-string v0, "DfeProto"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/finsky/api/DfeRequest;->PROTO_DEBUG:Z

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 4
    .param p1, "method"    # I
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "apiContext"    # Lcom/google/android/finsky/api/DfeApiContext;
    .param p6, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/api/DfeApiContext;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    .local p4, "responseClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p5, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TT;>;"
    const/4 v1, 0x0

    .line 116
    sget-object v0, Lcom/google/android/finsky/api/DfeApi;->BASE_URI:Landroid/net/Uri;

    invoke-static {v0, p2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p6}, Lcom/android/volley/Request;-><init>(ILjava/lang/String;Lcom/android/volley/Response$ErrorListener;)V

    .line 65
    iput-boolean v1, p0, Lcom/google/android/finsky/api/DfeRequest;->mAllowMultipleResponses:Z

    .line 71
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/finsky/api/DfeRequest;->mServerLatencyMs:J

    .line 77
    iput-boolean v1, p0, Lcom/google/android/finsky/api/DfeRequest;->mAvoidBulkCancel:Z

    .line 117
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    const-string v0, "Empty DFE URL"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    :cond_0
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->skipAllCaches:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/api/DfeRequest;->setShouldCache(Z)Lcom/android/volley/Request;

    .line 121
    new-instance v0, Lcom/google/android/finsky/api/DfeRetryPolicy;

    invoke-direct {v0, p3}, Lcom/google/android/finsky/api/DfeRetryPolicy;-><init>(Lcom/google/android/finsky/api/DfeApiContext;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/api/DfeRequest;->setRetryPolicy(Lcom/android/volley/RetryPolicy;)Lcom/android/volley/Request;

    .line 122
    iput-object p3, p0, Lcom/google/android/finsky/api/DfeRequest;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    .line 123
    iput-object p5, p0, Lcom/google/android/finsky/api/DfeRequest;->mListener:Lcom/android/volley/Response$Listener;

    .line 124
    iput-object p4, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseClass:Ljava/lang/Class;

    .line 125
    return-void

    :cond_1
    move v0, v1

    .line 120
    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V
    .locals 7
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "apiContext"    # Lcom/google/android/finsky/api/DfeApiContext;
    .param p5, "errorListener"    # Lcom/android/volley/Response$ErrorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/api/DfeApiContext;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/android/volley/Response$Listener",
            "<TT;>;",
            "Lcom/android/volley/Response$ErrorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 100
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    .local p3, "responseClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    .local p4, "listener":Lcom/android/volley/Response$Listener;, "Lcom/android/volley/Response$Listener<TT;>;"
    const/4 v1, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/api/DfeRequest;-><init>(ILjava/lang/String;Lcom/google/android/finsky/api/DfeApiContext;Ljava/lang/Class;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 102
    return-void
.end method

.method private getSignatureResponse(Lcom/android/volley/NetworkResponse;)Ljava/lang/String;
    .locals 2
    .param p1, "networkResponse"    # Lcom/android/volley/NetworkResponse;

    .prologue
    .line 409
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    iget-object v0, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    const-string v1, "X-DFE-Signature-Response"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private handleServerCommands(Lcom/google/android/finsky/protos/Response$ResponseWrapper;)Lcom/android/volley/Response;
    .locals 6
    .param p1, "wrapper"    # Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/Response$ResponseWrapper;",
            ")",
            "Lcom/android/volley/Response",
            "<",
            "Lcom/google/android/finsky/protos/Response$ResponseWrapper;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    const/4 v1, 0x0

    .line 465
    iget-object v2, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    if-nez v2, :cond_1

    .line 484
    :cond_0
    :goto_0
    return-object v1

    .line 469
    :cond_1
    iget-object v0, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    .line 472
    .local v0, "commands":Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;
    iget-boolean v2, v0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasLogErrorStacktrace:Z

    if-eqz v2, :cond_2

    .line 473
    const-string v2, "%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->logErrorStacktrace:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 477
    :cond_2
    iget-boolean v2, v0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->clearCache:Z

    if-eqz v2, :cond_3

    .line 478
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeRequest;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/DfeApiContext;->getCache()Lcom/android/volley/Cache;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/volley/Cache;->clear()V

    .line 481
    :cond_3
    iget-boolean v2, v0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->hasDisplayErrorMessage:Z

    if-eqz v2, :cond_0

    .line 482
    new-instance v1, Lcom/google/android/finsky/api/DfeServerError;

    iget-object v2, v0, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;->displayErrorMessage:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/finsky/api/DfeServerError;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v1

    goto :goto_0
.end method

.method private logProtoResponse(Lcom/google/android/finsky/protos/Response$ResponseWrapper;)V
    .locals 10
    .param p1, "wrapper"    # Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    .prologue
    .line 288
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    sget-object v6, Lcom/google/android/finsky/api/DfeApiConfig;->protoLogUrlRegexp:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 289
    .local v5, "regexp":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeRequest;->getUrl()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 290
    const-class v7, Lcom/google/protobuf/nano/MessageNanoPrinter;

    monitor-enter v7

    .line 291
    :try_start_0
    const-string v6, "DfeProto"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Response for "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeRequest;->getUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    invoke-static {p1}, Lcom/google/protobuf/nano/MessageNanoPrinter;->print(Lcom/google/protobuf/nano/MessageNano;)Ljava/lang/String;

    move-result-object v3

    .line 293
    .local v3, "log":Ljava/lang/String;
    const-string v6, "\n"

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 295
    .local v4, "logLine":Ljava/lang/String;
    const-string v6, "DfeProto"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "| "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297
    .end local v4    # "logLine":Ljava/lang/String;
    :cond_0
    monitor-exit v7

    .line 302
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "log":Ljava/lang/String;
    :goto_1
    return-void

    .line 297
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 299
    :cond_1
    const-string v6, "DfeProto"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Url does not match regexp: url="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeRequest;->getUrl()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " / regexp="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private makeCacheKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 542
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/api/DfeRequest;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/DfeApiContext;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static parseCacheHeaders(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Cache$Entry;
    .locals 12
    .param p0, "response"    # Lcom/android/volley/NetworkResponse;

    .prologue
    const-wide/16 v10, 0x0

    .line 415
    invoke-static {p0}, Lcom/android/volley/toolbox/HttpHeaderParser;->parseCacheHeaders(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Cache$Entry;

    move-result-object v1

    .line 416
    .local v1, "entry":Lcom/android/volley/Cache$Entry;
    if-nez v1, :cond_0

    .line 417
    const/4 v1, 0x0

    .line 443
    .end local v1    # "entry":Lcom/android/volley/Cache$Entry;
    :goto_0
    return-object v1

    .line 420
    .restart local v1    # "entry":Lcom/android/volley/Cache$Entry;
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 425
    .local v4, "now":J
    :try_start_0
    iget-object v6, p0, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    const-string v7, "X-DFE-Soft-TTL"

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 426
    .local v3, "softTtlHeader":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 427
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    add-long/2addr v6, v4

    iput-wide v6, v1, Lcom/android/volley/Cache$Entry;->softTtl:J

    .line 430
    :cond_1
    iget-object v6, p0, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    const-string v7, "X-DFE-Hard-TTL"

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 431
    .local v2, "hardTtlHeader":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 432
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    add-long/2addr v6, v4

    iput-wide v6, v1, Lcom/android/volley/Cache$Entry;->ttl:J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 441
    .end local v2    # "hardTtlHeader":Ljava/lang/String;
    .end local v3    # "softTtlHeader":Ljava/lang/String;
    :cond_2
    :goto_1
    iget-wide v6, v1, Lcom/android/volley/Cache$Entry;->ttl:J

    iget-wide v8, v1, Lcom/android/volley/Cache$Entry;->softTtl:J

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    iput-wide v6, v1, Lcom/android/volley/Cache$Entry;->ttl:J

    goto :goto_0

    .line 434
    :catch_0
    move-exception v0

    .line 435
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v6, "Invalid TTL: %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 436
    iput-wide v10, v1, Lcom/android/volley/Cache$Entry;->softTtl:J

    .line 437
    iput-wide v10, v1, Lcom/android/volley/Cache$Entry;->ttl:J

    goto :goto_1
.end method

.method private parseWrapperAndVerifyFromBytes(Lcom/android/volley/NetworkResponse;Ljava/lang/String;)Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    .locals 3
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .param p2, "signatureResponse"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;,
            Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
        }
    .end annotation

    .prologue
    .line 375
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    iget-object v1, p1, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-static {v1}, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->parseFrom([B)Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    move-result-object v0

    .line 379
    .local v0, "parsedResponse":Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseVerifier:Lcom/google/android/play/dfe/api/DfeResponseVerifier;

    if-eqz v1, :cond_0

    .line 380
    iget-object v1, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseVerifier:Lcom/google/android/play/dfe/api/DfeResponseVerifier;

    iget-object v2, p1, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-interface {v1, v2, p2}, Lcom/google/android/play/dfe/api/DfeResponseVerifier;->verify([BLjava/lang/String;)V

    .line 381
    const-string v1, "signature-verification-succeeded"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/api/DfeRequest;->addMarker(Ljava/lang/String;)V

    .line 383
    :cond_0
    return-object v0
.end method

.method private parseWrapperAndVerifySignature(Lcom/android/volley/NetworkResponse;Z)Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    .locals 7
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .param p2, "manuallyUnzip"    # Z

    .prologue
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 341
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/api/DfeRequest;->getSignatureResponse(Lcom/android/volley/NetworkResponse;)Ljava/lang/String;

    move-result-object v2

    .line 342
    .local v2, "signatureResponse":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 343
    new-instance v1, Ljava/util/zip/GZIPInputStream;

    new-instance v3, Ljava/io/ByteArrayInputStream;

    iget-object v4, p1, Lcom/android/volley/NetworkResponse;->data:[B

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v3}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 344
    .local v1, "is":Ljava/io/InputStream;
    invoke-direct {p0, v1, v2}, Lcom/google/android/finsky/api/DfeRequest;->parseWrapperAndVerifySignatureFromIs(Ljava/io/InputStream;Ljava/lang/String;)Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    move-result-object v3

    .line 360
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v2    # "signatureResponse":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 346
    .restart local v2    # "signatureResponse":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, p1, v2}, Lcom/google/android/finsky/api/DfeRequest;->parseWrapperAndVerifyFromBytes(Lcom/android/volley/NetworkResponse;Ljava/lang/String;)Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    goto :goto_0

    .line 348
    .end local v2    # "signatureResponse":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 349
    .local v0, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    if-nez p2, :cond_1

    .line 350
    invoke-direct {p0, p1, v6}, Lcom/google/android/finsky/api/DfeRequest;->parseWrapperAndVerifySignature(Lcom/android/volley/NetworkResponse;Z)Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    move-result-object v3

    goto :goto_0

    .line 352
    :cond_1
    const-string v3, "Cannot parse response as ResponseWrapper proto."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    .end local v0    # "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 354
    :catch_1
    move-exception v0

    .line 355
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "IOException while manually unzipping request."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 356
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 357
    .local v0, "e":Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
    const-string v3, "signature-verification-failed"

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/api/DfeRequest;->addMarker(Ljava/lang/String;)V

    .line 358
    const-string v3, "Could not verify request: %s, exception %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p0, v4, v5

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private parseWrapperAndVerifySignatureFromIs(Ljava/io/InputStream;Ljava/lang/String;)Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "signatureResponse"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
        }
    .end annotation

    .prologue
    .line 400
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    invoke-static {p1}, Lcom/google/android/finsky/utils/Utils;->readBytes(Ljava/io/InputStream;)[B

    move-result-object v1

    .line 401
    .local v1, "uncompressedResponse":[B
    invoke-static {v1}, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->parseFrom([B)Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    move-result-object v0

    .line 402
    .local v0, "result":Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseVerifier:Lcom/google/android/play/dfe/api/DfeResponseVerifier;

    if-eqz v2, :cond_0

    .line 403
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseVerifier:Lcom/google/android/play/dfe/api/DfeResponseVerifier;

    invoke-interface {v2, v1, p2}, Lcom/google/android/play/dfe/api/DfeResponseVerifier;->verify([BLjava/lang/String;)V

    .line 405
    :cond_0
    return-object v0
.end method


# virtual methods
.method public addExtraHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "header"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 198
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/api/DfeRequest;->mExtraHeaders:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 199
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/api/DfeRequest;->mExtraHeaders:Ljava/util/Map;

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/api/DfeRequest;->mExtraHeaders:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    return-void
.end method

.method public deliverError(Lcom/android/volley/VolleyError;)V
    .locals 3
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 530
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    instance-of v0, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/google/android/finsky/api/DfeRequest;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/DfeApiContext;->invalidateAuthToken()V

    .line 533
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseDelivered:Z

    if-nez v0, :cond_1

    .line 534
    invoke-super {p0, p1}, Lcom/android/volley/Request;->deliverError(Lcom/android/volley/VolleyError;)V

    .line 539
    :goto_0
    return-void

    .line 536
    :cond_1
    const-string v0, "Not delivering error response for request=[%s], error=[%s] because response already delivered."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public deliverResponse(Lcom/google/android/finsky/protos/Response$ResponseWrapper;)V
    .locals 7
    .param p1, "responseWrapper"    # Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    .prologue
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 505
    const/4 v1, 0x0

    .line 507
    .local v1, "response":Lcom/google/protobuf/nano/MessageNano;, "TT;"
    :try_start_0
    iget-object v2, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->payload:Lcom/google/android/finsky/protos/Response$Payload;

    const-class v3, Lcom/google/android/finsky/protos/Response$Payload;

    iget-object v4, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseClass:Ljava/lang/Class;

    invoke-static {v2, v3, v4}, Lcom/google/android/play/dfe/utils/NanoProtoHelper;->getParsedResponseFromWrapper(Lcom/google/protobuf/nano/MessageNano;Ljava/lang/Class;Ljava/lang/Class;)Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 514
    if-eqz v1, :cond_2

    .line 515
    iget-boolean v2, p0, Lcom/google/android/finsky/api/DfeRequest;->mAllowMultipleResponses:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseDelivered:Z

    if-nez v2, :cond_1

    .line 516
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeRequest;->mListener:Lcom/android/volley/Response$Listener;

    invoke-interface {v2, v1}, Lcom/android/volley/Response$Listener;->onResponse(Ljava/lang/Object;)V

    .line 517
    iput-boolean v5, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseDelivered:Z

    .line 525
    :goto_0
    return-void

    .line 509
    :catch_0
    move-exception v0

    .line 510
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Null wrapper parsed for request=[%s]"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p0, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 511
    new-instance v2, Lcom/android/volley/ParseError;

    invoke-direct {v2, v0}, Lcom/android/volley/ParseError;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/api/DfeRequest;->deliverError(Lcom/android/volley/VolleyError;)V

    goto :goto_0

    .line 519
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const-string v2, "Not delivering second response for request=[%s]"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p0, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 522
    :cond_2
    const-string v2, "Null parsed response for request=[%s]"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p0, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 523
    new-instance v2, Lcom/android/volley/VolleyError;

    invoke-direct {v2}, Lcom/android/volley/VolleyError;-><init>()V

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/api/DfeRequest;->deliverError(Lcom/android/volley/VolleyError;)V

    goto :goto_0
.end method

.method public bridge synthetic deliverResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 50
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    check-cast p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/DfeRequest;->deliverResponse(Lcom/google/android/finsky/protos/Response$ResponseWrapper;)V

    return-void
.end method

.method public getAvoidBulkCancel()Z
    .locals 1

    .prologue
    .line 139
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    iget-boolean v0, p0, Lcom/google/android/finsky/api/DfeRequest;->mAvoidBulkCancel:Z

    return v0
.end method

.method public getCacheKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 554
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    invoke-super {p0}, Lcom/android/volley/Request;->getUrl()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/api/DfeRequest;->makeCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeaders()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    .prologue
    .line 211
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    iget-object v5, p0, Lcom/google/android/finsky/api/DfeRequest;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/DfeApiContext;->getHeaders()Ljava/util/Map;

    move-result-object v1

    .line 212
    .local v1, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/google/android/finsky/api/DfeRequest;->mExtraHeaders:Ljava/util/Map;

    if-eqz v5, :cond_0

    .line 213
    iget-object v5, p0, Lcom/google/android/finsky/api/DfeRequest;->mExtraHeaders:Ljava/util/Map;

    invoke-interface {v1, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 215
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseVerifier:Lcom/google/android/play/dfe/api/DfeResponseVerifier;

    if-eqz v5, :cond_1

    .line 217
    :try_start_0
    const-string v5, "X-DFE-Signature-Request"

    iget-object v6, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseVerifier:Lcom/google/android/play/dfe/api/DfeResponseVerifier;

    invoke-interface {v6}, Lcom/google/android/play/dfe/api/DfeResponseVerifier;->getSignatureRequest()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeRequest;->getRetryPolicy()Lcom/android/volley/RetryPolicy;

    move-result-object v4

    .line 228
    .local v4, "retryPolicy":Lcom/android/volley/RetryPolicy;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "timeoutMs="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v4}, Lcom/android/volley/RetryPolicy;->getCurrentTimeout()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 229
    .local v2, "requestParamsValue":Ljava/lang/String;
    invoke-interface {v4}, Lcom/android/volley/RetryPolicy;->getCurrentRetryCount()I

    move-result v3

    .line 230
    .local v3, "retryAttempt":I
    if-lez v3, :cond_2

    .line 231
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; retryAttempt="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 233
    :cond_2
    const-string v5, "X-DFE-Request-Params"

    invoke-interface {v1, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    return-object v1

    .line 218
    .end local v2    # "requestParamsValue":Ljava/lang/String;
    .end local v3    # "retryAttempt":I
    .end local v4    # "retryPolicy":Lcom/android/volley/RetryPolicy;
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Lcom/google/android/play/dfe/api/DfeResponseVerifier$DfeResponseVerifierException;
    const-string v5, "Couldn\'t create signature request: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 220
    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeRequest;->cancel()V

    goto :goto_0
.end method

.method public getServerLatencyMs()J
    .locals 2

    .prologue
    .line 563
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    iget-wide v0, p0, Lcom/google/android/finsky/api/DfeRequest;->mServerLatencyMs:J

    return-wide v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 8

    .prologue
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    const/16 v4, 0x26

    const/4 v7, -0x1

    const/16 v5, 0x3f

    .line 168
    invoke-super {p0}, Lcom/android/volley/Request;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 169
    .local v2, "url":Ljava/lang/String;
    sget-object v3, Lcom/google/android/finsky/api/DfeApiConfig;->ipCountryOverride:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 170
    .local v1, "overrideCountry":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 171
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v7, :cond_5

    move v3, v4

    :goto_0
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 172
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "ipCountryOverride="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 174
    :cond_0
    sget-object v3, Lcom/google/android/finsky/api/DfeApiConfig;->mccMncOverride:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 175
    .local v0, "mccmnc":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 176
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v7, :cond_6

    move v3, v4

    :goto_1
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 177
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "mccmncOverride="

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 179
    :cond_1
    sget-object v3, Lcom/google/android/finsky/api/DfeApiConfig;->skipAllCaches:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 180
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v7, :cond_7

    move v3, v4

    :goto_2
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 181
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "skipCache=true"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 183
    :cond_2
    sget-object v3, Lcom/google/android/finsky/api/DfeApiConfig;->showStagingData:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 184
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    if-eq v3, v7, :cond_8

    move v3, v4

    :goto_3
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 185
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "showStagingData=true"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 187
    :cond_3
    sget-object v3, Lcom/google/android/finsky/api/DfeApiConfig;->prexDisabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 188
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v7, :cond_9

    :goto_4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 189
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "p13n=false"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 191
    :cond_4
    return-object v2

    .end local v0    # "mccmnc":Ljava/lang/String;
    :cond_5
    move v3, v5

    .line 171
    goto/16 :goto_0

    .restart local v0    # "mccmnc":Ljava/lang/String;
    :cond_6
    move v3, v5

    .line 176
    goto/16 :goto_1

    :cond_7
    move v3, v5

    .line 180
    goto/16 :goto_2

    :cond_8
    move v3, v5

    .line 184
    goto :goto_3

    :cond_9
    move v4, v5

    .line 188
    goto :goto_4
.end method

.method public handleNotifications(Lcom/google/android/finsky/protos/Response$ResponseWrapper;)V
    .locals 5
    .param p1, "wrapper"    # Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    .prologue
    .line 493
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    iget-object v4, p0, Lcom/google/android/finsky/api/DfeRequest;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/DfeApiContext;->getNotificationManager()Lcom/google/android/finsky/api/DfeNotificationManager;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v4, v4

    if-nez v4, :cond_1

    .line 501
    :cond_0
    return-void

    .line 497
    :cond_1
    iget-object v0, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Notifications$Notification;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 499
    .local v3, "notification":Lcom/google/android/finsky/protos/Notifications$Notification;
    iget-object v4, p0, Lcom/google/android/finsky/api/DfeRequest;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/DfeApiContext;->getNotificationManager()Lcom/google/android/finsky/api/DfeNotificationManager;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/google/android/finsky/api/DfeNotificationManager;->processNotification(Lcom/google/android/finsky/protos/Notifications$Notification;)V

    .line 497
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected parseNetworkError(Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;
    .locals 4
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 448
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    instance-of v2, p1, Lcom/android/volley/ServerError;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    if-eqz v2, :cond_0

    .line 449
    iget-object v2, p1, Lcom/android/volley/VolleyError;->networkResponse:Lcom/android/volley/NetworkResponse;

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/finsky/api/DfeRequest;->parseWrapperAndVerifySignature(Lcom/android/volley/NetworkResponse;Z)Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    move-result-object v1

    .line 451
    .local v1, "wrapper":Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    if-eqz v1, :cond_0

    .line 452
    invoke-direct {p0, v1}, Lcom/google/android/finsky/api/DfeRequest;->handleServerCommands(Lcom/google/android/finsky/protos/Response$ResponseWrapper;)Lcom/android/volley/Response;

    move-result-object v0

    .line 453
    .local v0, "response":Lcom/android/volley/Response;, "Lcom/android/volley/Response<Lcom/google/android/finsky/protos/Response$ResponseWrapper;>;"
    iget-object p1, v0, Lcom/android/volley/Response;->error:Lcom/android/volley/VolleyError;

    .line 456
    .end local v0    # "response":Lcom/android/volley/Response;, "Lcom/android/volley/Response<Lcom/google/android/finsky/protos/Response$ResponseWrapper;>;"
    .end local v1    # "wrapper":Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    .end local p1    # "volleyError":Lcom/android/volley/VolleyError;
    :cond_0
    return-object p1
.end method

.method public parseNetworkResponse(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Response;
    .locals 11
    .param p1, "response"    # Lcom/android/volley/NetworkResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/volley/NetworkResponse;",
            ")",
            "Lcom/android/volley/Response",
            "<",
            "Lcom/google/android/finsky/protos/Response$ResponseWrapper;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 240
    sget-boolean v6, Lcom/google/android/finsky/api/DfeRequest;->DEBUG:Z

    if-eqz v6, :cond_1

    .line 241
    const/4 v1, 0x0

    .line 242
    .local v1, "contentLength":I
    iget-object v6, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    if-eqz v6, :cond_0

    iget-object v6, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    const-string v7, "X-DFE-Content-Length"

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 243
    iget-object v6, p1, Lcom/android/volley/NetworkResponse;->headers:Ljava/util/Map;

    const-string v7, "X-DFE-Content-Length"

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    div-int/lit16 v1, v6, 0x400

    .line 246
    :cond_0
    const-string v6, "Parsed response for url=[%s] contentLength=[%d KB]"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeRequest;->getUrl()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 249
    .end local v1    # "contentLength":I
    :cond_1
    invoke-direct {p0, p1, v9}, Lcom/google/android/finsky/api/DfeRequest;->parseWrapperAndVerifySignature(Lcom/android/volley/NetworkResponse;Z)Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    move-result-object v5

    .line 250
    .local v5, "wrapper":Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    if-nez v5, :cond_3

    .line 251
    new-instance v6, Lcom/android/volley/ParseError;

    invoke-direct {v6, p1}, Lcom/android/volley/ParseError;-><init>(Lcom/android/volley/NetworkResponse;)V

    invoke-static {v6}, Lcom/android/volley/Response;->error(Lcom/android/volley/VolleyError;)Lcom/android/volley/Response;

    move-result-object v2

    .line 284
    :cond_2
    :goto_0
    return-object v2

    .line 252
    :cond_3
    sget-boolean v6, Lcom/google/android/finsky/api/DfeRequest;->PROTO_DEBUG:Z

    if-eqz v6, :cond_4

    .line 253
    invoke-direct {p0, v5}, Lcom/google/android/finsky/api/DfeRequest;->logProtoResponse(Lcom/google/android/finsky/protos/Response$ResponseWrapper;)V

    .line 255
    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/finsky/api/DfeRequest;->handleServerCommands(Lcom/google/android/finsky/protos/Response$ResponseWrapper;)Lcom/android/volley/Response;

    move-result-object v2

    .line 256
    .local v2, "error":Lcom/android/volley/Response;, "Lcom/android/volley/Response<Lcom/google/android/finsky/protos/Response$ResponseWrapper;>;"
    if-nez v2, :cond_2

    .line 259
    iget-object v6, v5, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    if-eqz v6, :cond_5

    .line 260
    iget-object v3, v5, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    .line 261
    .local v3, "metadata":Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;
    iget-boolean v6, v3, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->hasLatencyMillis:Z

    if-eqz v6, :cond_5

    .line 262
    iget-wide v6, v3, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;->latencyMillis:J

    iput-wide v6, p0, Lcom/google/android/finsky/api/DfeRequest;->mServerLatencyMs:J

    .line 266
    .end local v3    # "metadata":Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;
    :cond_5
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/api/DfeRequest;->handleNotifications(Lcom/google/android/finsky/protos/Response$ResponseWrapper;)V

    .line 269
    iget-object v6, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseVerifier:Lcom/google/android/play/dfe/api/DfeResponseVerifier;

    if-eqz v6, :cond_7

    .line 274
    const/4 v0, 0x0

    .line 278
    .local v0, "cacheEntry":Lcom/android/volley/Cache$Entry;
    :goto_1
    if-eqz v0, :cond_6

    .line 279
    invoke-virtual {p0, v5, v0}, Lcom/google/android/finsky/api/DfeRequest;->stripForCache(Lcom/google/android/finsky/protos/Response$ResponseWrapper;Lcom/android/volley/Cache$Entry;)V

    .line 282
    :cond_6
    invoke-static {v5, v0}, Lcom/android/volley/Response;->success(Ljava/lang/Object;Lcom/android/volley/Cache$Entry;)Lcom/android/volley/Response;

    move-result-object v4

    .line 283
    .local v4, "wrappedResponse":Lcom/android/volley/Response;, "Lcom/android/volley/Response<Lcom/google/android/finsky/protos/Response$ResponseWrapper;>;"
    const-string v6, "DFE response %s"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/finsky/api/DfeRequest;->getUrl()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->logTiming(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v2, v4

    .line 284
    goto :goto_0

    .line 276
    .end local v0    # "cacheEntry":Lcom/android/volley/Cache$Entry;
    .end local v4    # "wrappedResponse":Lcom/android/volley/Response;, "Lcom/android/volley/Response<Lcom/google/android/finsky/protos/Response$ResponseWrapper;>;"
    :cond_7
    invoke-static {p1}, Lcom/google/android/finsky/api/DfeRequest;->parseCacheHeaders(Lcom/android/volley/NetworkResponse;)Lcom/android/volley/Cache$Entry;

    move-result-object v0

    .restart local v0    # "cacheEntry":Lcom/android/volley/Cache$Entry;
    goto :goto_1
.end method

.method public setAllowMultipleResponses(Z)V
    .locals 0
    .param p1, "allow"    # Z

    .prologue
    .line 153
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    iput-boolean p1, p0, Lcom/google/android/finsky/api/DfeRequest;->mAllowMultipleResponses:Z

    .line 154
    return-void
.end method

.method public setAvoidBulkCancel()V
    .locals 1

    .prologue
    .line 132
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/api/DfeRequest;->mAvoidBulkCancel:Z

    .line 133
    return-void
.end method

.method public setRequireAuthenticatedResponse(Lcom/google/android/play/dfe/api/DfeResponseVerifier;)V
    .locals 0
    .param p1, "responseVerifier"    # Lcom/google/android/play/dfe/api/DfeResponseVerifier;

    .prologue
    .line 160
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    iput-object p1, p0, Lcom/google/android/finsky/api/DfeRequest;->mResponseVerifier:Lcom/google/android/play/dfe/api/DfeResponseVerifier;

    .line 161
    return-void
.end method

.method stripForCache(Lcom/google/android/finsky/protos/Response$ResponseWrapper;Lcom/android/volley/Cache$Entry;)V
    .locals 12
    .param p1, "wrapper"    # Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    .param p2, "rootEntry"    # Lcom/android/volley/Cache$Entry;

    .prologue
    .local p0, "this":Lcom/google/android/finsky/api/DfeRequest;, "Lcom/google/android/finsky/api/DfeRequest<TT;>;"
    const/4 v10, 0x1

    .line 308
    iget-object v9, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v9, v9

    if-ge v9, v10, :cond_0

    iget-object v9, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    if-nez v9, :cond_0

    iget-object v9, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v9, v9

    if-ge v9, v10, :cond_0

    .line 336
    :goto_0
    return-void

    .line 314
    :cond_0
    iget-object v9, p0, Lcom/google/android/finsky/api/DfeRequest;->mApiContext:Lcom/google/android/finsky/api/DfeApiContext;

    invoke-virtual {v9}, Lcom/google/android/finsky/api/DfeApiContext;->getCache()Lcom/android/volley/Cache;

    move-result-object v1

    .line 315
    .local v1, "cache":Lcom/android/volley/Cache;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 316
    .local v6, "now":J
    iget-object v0, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_1

    aget-object v8, v0, v4

    .line 317
    .local v8, "prefetch":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    new-instance v2, Lcom/android/volley/Cache$Entry;

    invoke-direct {v2}, Lcom/android/volley/Cache$Entry;-><init>()V

    .line 318
    .local v2, "entry":Lcom/android/volley/Cache$Entry;
    iget-object v9, v8, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->response:[B

    iput-object v9, v2, Lcom/android/volley/Cache$Entry;->data:[B

    .line 319
    iget-object v9, v8, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->etag:Ljava/lang/String;

    iput-object v9, v2, Lcom/android/volley/Cache$Entry;->etag:Ljava/lang/String;

    .line 320
    iget-wide v10, p2, Lcom/android/volley/Cache$Entry;->serverDate:J

    iput-wide v10, v2, Lcom/android/volley/Cache$Entry;->serverDate:J

    .line 321
    iget-wide v10, v8, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->ttl:J

    add-long/2addr v10, v6

    iput-wide v10, v2, Lcom/android/volley/Cache$Entry;->ttl:J

    .line 322
    iget-wide v10, v8, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->softTtl:J

    add-long/2addr v10, v6

    iput-wide v10, v2, Lcom/android/volley/Cache$Entry;->softTtl:J

    .line 323
    sget-object v9, Lcom/google/android/finsky/api/DfeApi;->BASE_URI:Landroid/net/Uri;

    iget-object v10, v8, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->url:Ljava/lang/String;

    invoke-static {v9, v10}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 325
    .local v3, "fullCacheUrl":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/google/android/finsky/api/DfeRequest;->makeCacheKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v9, v2}, Lcom/android/volley/Cache;->put(Ljava/lang/String;Lcom/android/volley/Cache$Entry;)V

    .line 316
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 329
    .end local v2    # "entry":Lcom/android/volley/Cache$Entry;
    .end local v3    # "fullCacheUrl":Ljava/lang/String;
    .end local v8    # "prefetch":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->emptyArray()[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    move-result-object v9

    iput-object v9, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    .line 331
    const/4 v9, 0x0

    iput-object v9, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    .line 333
    invoke-static {}, Lcom/google/android/finsky/protos/Notifications$Notification;->emptyArray()[Lcom/google/android/finsky/protos/Notifications$Notification;

    move-result-object v9

    iput-object v9, p1, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    .line 335
    invoke-static {p1}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v9

    iput-object v9, p2, Lcom/android/volley/Cache$Entry;->data:[B

    goto :goto_0
.end method
