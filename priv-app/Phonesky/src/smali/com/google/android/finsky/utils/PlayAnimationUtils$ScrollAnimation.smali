.class public Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
.super Landroid/view/animation/TranslateAnimation;
.source "PlayAnimationUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/PlayAnimationUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScrollAnimation"
.end annotation


# instance fields
.field private mStartedScrolling:Z


# direct methods
.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "fromXDelta"    # F
    .param p2, "toXDelta"    # F
    .param p3, "fromYDelta"    # F
    .param p4, "toYDelta"    # F

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 50
    return-void
.end method


# virtual methods
.method public hasStartedScrolling()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->mStartedScrolling:Z

    return v0
.end method

.method public setStartedScrolling()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;->mStartedScrolling:Z

    .line 54
    return-void
.end method
