.class public Lcom/google/android/finsky/providers/QSBSuggestionsProvider;
.super Landroid/content/SearchRecentSuggestionsProvider;
.source "QSBSuggestionsProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/content/SearchRecentSuggestionsProvider;-><init>()V

    .line 20
    const-string v0, "com.google.android.finsky.QSBSuggestionsProvider2"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/providers/QSBSuggestionsProvider;->setupSuggestions(Ljava/lang/String;I)V

    .line 21
    return-void
.end method


# virtual methods
.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 31
    if-eqz p4, :cond_0

    array-length v3, p4

    if-nez v3, :cond_1

    .line 32
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SelectionArgs must be provided for the Uri: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 36
    :cond_1
    aget-object v3, p4, v4

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "query":Ljava/lang/String;
    new-instance v2, Lcom/google/android/finsky/providers/SuggestionHandler;

    invoke-virtual {p0}, Lcom/google/android/finsky/providers/QSBSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/finsky/providers/SuggestionHandler;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 38
    .local v2, "suggestionHandler":Lcom/google/android/finsky/providers/SuggestionHandler;
    new-instance v1, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;

    invoke-virtual {p0}, Lcom/google/android/finsky/providers/QSBSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3, v4, v0, v2}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/finsky/providers/SuggestionHandler;)V

    .line 40
    .local v1, "suggester":Lcom/google/android/finsky/providers/SearchSuggestionFetcher;
    invoke-virtual {v1}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;->gatherSuggestions()V

    .line 41
    invoke-virtual {v2}, Lcom/google/android/finsky/providers/SuggestionHandler;->getSuggestions()Landroid/database/Cursor;

    move-result-object v3

    return-object v3
.end method
