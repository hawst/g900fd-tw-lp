.class public final Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AuthenticationChallenge"
.end annotation


# instance fields
.field public authenticationType:I

.field public documentTitle:Ljava/lang/String;

.field public formattedPrice:Ljava/lang/String;

.field public gaiaDescriptionTextHtml:Ljava/lang/String;

.field public gaiaFooterTextHtml:Ljava/lang/String;

.field public gaiaHeaderText:Ljava/lang/String;

.field public gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

.field public gaiaOptOutDescriptionTextHtml:Ljava/lang/String;

.field public hasAuthenticationType:Z

.field public hasDocumentTitle:Z

.field public hasFormattedPrice:Z

.field public hasGaiaDescriptionTextHtml:Z

.field public hasGaiaFooterTextHtml:Z

.field public hasGaiaHeaderText:Z

.field public hasGaiaOptOutDescriptionTextHtml:Z

.field public hasInstrumentDisplayTitle:Z

.field public hasResponseAuthenticationTypeParam:Z

.field public hasResponseRetryCountParam:Z

.field public instrumentDisplayTitle:Ljava/lang/String;

.field public responseAuthenticationTypeParam:Ljava/lang/String;

.field public responseRetryCountParam:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 913
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 914
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->clear()Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    .line 915
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 918
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->authenticationType:I

    .line 919
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasAuthenticationType:Z

    .line 920
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseAuthenticationTypeParam:Ljava/lang/String;

    .line 921
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasResponseAuthenticationTypeParam:Z

    .line 922
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseRetryCountParam:Ljava/lang/String;

    .line 923
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasResponseRetryCountParam:Z

    .line 924
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaHeaderText:Ljava/lang/String;

    .line 925
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaHeaderText:Z

    .line 926
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaDescriptionTextHtml:Ljava/lang/String;

    .line 927
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaDescriptionTextHtml:Z

    .line 928
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaFooterTextHtml:Ljava/lang/String;

    .line 929
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaFooterTextHtml:Z

    .line 930
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    .line 931
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutDescriptionTextHtml:Ljava/lang/String;

    .line 932
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaOptOutDescriptionTextHtml:Z

    .line 933
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->documentTitle:Ljava/lang/String;

    .line 934
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasDocumentTitle:Z

    .line 935
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->formattedPrice:Ljava/lang/String;

    .line 936
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasFormattedPrice:Z

    .line 937
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->instrumentDisplayTitle:Ljava/lang/String;

    .line 938
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasInstrumentDisplayTitle:Z

    .line 939
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->cachedSize:I

    .line 940
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 984
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 985
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->authenticationType:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasAuthenticationType:Z

    if-eqz v1, :cond_1

    .line 986
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->authenticationType:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 989
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasResponseAuthenticationTypeParam:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseAuthenticationTypeParam:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 990
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseAuthenticationTypeParam:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 993
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasResponseRetryCountParam:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseRetryCountParam:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 994
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseRetryCountParam:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 997
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaHeaderText:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaHeaderText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 998
    :cond_6
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaHeaderText:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1001
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaDescriptionTextHtml:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaDescriptionTextHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1002
    :cond_8
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaDescriptionTextHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1005
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaFooterTextHtml:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaFooterTextHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1006
    :cond_a
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaFooterTextHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1009
    :cond_b
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-eqz v1, :cond_c

    .line 1010
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1013
    :cond_c
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaOptOutDescriptionTextHtml:Z

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutDescriptionTextHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 1014
    :cond_d
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutDescriptionTextHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1017
    :cond_e
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasDocumentTitle:Z

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->documentTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 1018
    :cond_f
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->documentTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1021
    :cond_10
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasFormattedPrice:Z

    if-nez v1, :cond_11

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->formattedPrice:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    .line 1022
    :cond_11
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->formattedPrice:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1025
    :cond_12
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasInstrumentDisplayTitle:Z

    if-nez v1, :cond_13

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->instrumentDisplayTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    .line 1026
    :cond_13
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->instrumentDisplayTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1029
    :cond_14
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1037
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1038
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1042
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1043
    :sswitch_0
    return-object p0

    .line 1048
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1049
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1054
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->authenticationType:I

    .line 1055
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasAuthenticationType:Z

    goto :goto_0

    .line 1061
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseAuthenticationTypeParam:Ljava/lang/String;

    .line 1062
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasResponseAuthenticationTypeParam:Z

    goto :goto_0

    .line 1066
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseRetryCountParam:Ljava/lang/String;

    .line 1067
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasResponseRetryCountParam:Z

    goto :goto_0

    .line 1071
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaHeaderText:Ljava/lang/String;

    .line 1072
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaHeaderText:Z

    goto :goto_0

    .line 1076
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaDescriptionTextHtml:Ljava/lang/String;

    .line 1077
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaDescriptionTextHtml:Z

    goto :goto_0

    .line 1081
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaFooterTextHtml:Ljava/lang/String;

    .line 1082
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaFooterTextHtml:Z

    goto :goto_0

    .line 1086
    :sswitch_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-nez v2, :cond_1

    .line 1087
    new-instance v2, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    .line 1089
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1093
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutDescriptionTextHtml:Ljava/lang/String;

    .line 1094
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaOptOutDescriptionTextHtml:Z

    goto :goto_0

    .line 1098
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->documentTitle:Ljava/lang/String;

    .line 1099
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasDocumentTitle:Z

    goto :goto_0

    .line 1103
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->formattedPrice:Ljava/lang/String;

    .line 1104
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasFormattedPrice:Z

    goto :goto_0

    .line 1108
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->instrumentDisplayTitle:Ljava/lang/String;

    .line 1109
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasInstrumentDisplayTitle:Z

    goto :goto_0

    .line 1038
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
        0x62 -> :sswitch_a
        0x6a -> :sswitch_b
    .end sparse-switch

    .line 1049
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 853
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 946
    iget v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->authenticationType:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasAuthenticationType:Z

    if-eqz v0, :cond_1

    .line 947
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->authenticationType:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 949
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasResponseAuthenticationTypeParam:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseAuthenticationTypeParam:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 950
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseAuthenticationTypeParam:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 952
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasResponseRetryCountParam:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseRetryCountParam:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 953
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->responseRetryCountParam:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 955
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaHeaderText:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaHeaderText:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 956
    :cond_6
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaHeaderText:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 958
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaDescriptionTextHtml:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaDescriptionTextHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 959
    :cond_8
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaDescriptionTextHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 961
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaFooterTextHtml:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaFooterTextHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 962
    :cond_a
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaFooterTextHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 964
    :cond_b
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    if-eqz v0, :cond_c

    .line 965
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutCheckbox:Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 967
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasGaiaOptOutDescriptionTextHtml:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutDescriptionTextHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 968
    :cond_d
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->gaiaOptOutDescriptionTextHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 970
    :cond_e
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasDocumentTitle:Z

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->documentTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 971
    :cond_f
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->documentTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 973
    :cond_10
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasFormattedPrice:Z

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->formattedPrice:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 974
    :cond_11
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->formattedPrice:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 976
    :cond_12
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->hasInstrumentDisplayTitle:Z

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->instrumentDisplayTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 977
    :cond_13
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;->instrumentDisplayTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 979
    :cond_14
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 980
    return-void
.end method
