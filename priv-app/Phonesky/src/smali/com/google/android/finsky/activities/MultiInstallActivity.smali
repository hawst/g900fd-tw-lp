.class public Lcom/google/android/finsky/activities/MultiInstallActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "MultiInstallActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    }
.end annotation


# instance fields
.field private mCurrentFragment:Lcom/google/android/finsky/activities/InstallApprovalFragment;

.field private mCurrentInstallIndex:I

.field private mCurrentPageType:I

.field private mInstallAccountName:Ljava/lang/String;

.field private mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private mInstallsForApproval:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;",
            ">;"
        }
    .end annotation
.end field

.field private mMode:I

.field private mNegativeButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mPositiveButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mShowPermissionBucketsLearnMoreHeader:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 388
    return-void
.end method

.method private displayPageOrFinish(Z)V
    .locals 5
    .param p1, "sameInstallAsPrevPage"    # Z

    .prologue
    const/4 v3, 0x1

    .line 302
    iget v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    iget-object v2, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallsForApproval:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 303
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->finish()V

    .line 322
    :goto_0
    return-void

    .line 307
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallsForApproval:Ljava/util/ArrayList;

    iget v2, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .line 308
    .local v0, "installDetails":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->needsAutoUpdateWarning()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 309
    iput v3, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    .line 321
    :goto_1
    iget v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    iget v2, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    invoke-direct {p0, v1, v2, p1}, Lcom/google/android/finsky/activities/MultiInstallActivity;->showInstallApprovalPage(IIZ)V

    goto :goto_0

    .line 310
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->needsLargeDownloadWarning()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 311
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    goto :goto_1

    .line 312
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->needsPermissionsWarning()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 313
    const/4 v1, 0x3

    iput v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    goto :goto_1

    .line 315
    :cond_3
    const-string v1, "Failed to determine the next page type when updating %s."

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageName:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$000(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->finish()V

    goto :goto_0
.end method

.method private getApprovalType()I
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 281
    iget v2, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    packed-switch v2, :pswitch_data_0

    .line 289
    const-string v2, "Invalid current page type: %d"

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v3

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 290
    :goto_0
    :pswitch_0
    return v0

    .line 285
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 287
    goto :goto_0

    .line 281
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getInstallIntent(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "documents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/MultiInstallActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "MultiInstallActivity.installs"

    invoke-static {p1}, Lcom/google/android/finsky/utils/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 92
    const-string v1, "MultiInstallActivity.mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 93
    const-string v1, "MultiInstallActivity.install-account-name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    return-object v0
.end method

.method public static getUpdateIntent(Landroid/content/Context;Ljava/util/List;)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "documents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/MultiInstallActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "MultiInstallActivity.installs"

    invoke-static {p1}, Lcom/google/android/finsky/utils/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 108
    const-string v1, "MultiInstallActivity.mode"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 109
    return-object v0
.end method

.method private installAndShowWarnings(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "documents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    const/4 v6, 0x1

    .line 165
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 166
    .local v3, "installsWithNoWarnings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 168
    .local v4, "installsWithWarnings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;>;"
    iget v5, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mMode:I

    invoke-static {p1, v5, v3, v4}, Lcom/google/android/finsky/activities/MultiInstallActivity;->splitDocumentsOnWarnings(Ljava/util/List;ILjava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 170
    invoke-direct {p0, v3}, Lcom/google/android/finsky/activities/MultiInstallActivity;->performBulkInstall(Ljava/util/List;)V

    .line 172
    iput-object v4, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallsForApproval:Ljava/util/ArrayList;

    .line 176
    sget-object v5, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPermissionBucketsLearnMore:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 177
    .local v1, "hasSeenBuckets":Z
    if-nez v1, :cond_1

    .line 178
    iget-object v5, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallsForApproval:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .line 179
    .local v0, "details":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->needsPermissionsWarning()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 180
    iput-boolean v6, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mShowPermissionBucketsLearnMoreHeader:Z

    goto :goto_0

    .line 184
    .end local v0    # "details":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mShowPermissionBucketsLearnMoreHeader:Z

    .line 186
    :cond_2
    invoke-direct {p0, v6}, Lcom/google/android/finsky/activities/MultiInstallActivity;->displayPageOrFinish(Z)V

    .line 187
    return-void
.end method

.method private performBulkInstall(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 325
    .local p1, "installs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .line 326
    .local v1, "install":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/MultiInstallActivity;->performSingleInstall(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)V

    goto :goto_0

    .line 328
    .end local v1    # "install":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    :cond_0
    return-void
.end method

.method private performSingleInstall(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)V
    .locals 8
    .param p1, "install"    # Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .prologue
    .line 333
    iget v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 334
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$000(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->versionCode:I
    invoke-static {p1}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$300(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)I

    move-result v2

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$100(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->reason:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$400(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-interface/range {v0 .. v6}, Lcom/google/android/finsky/receivers/Installer;->updateSingleInstalledApp(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZI)V

    .line 340
    :goto_0
    return-void

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageName:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$000(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->versionCode:I
    invoke-static {p1}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$300(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallAccountName:Ljava/lang/String;

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageTitle:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$100(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->reason:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$400(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x3

    invoke-interface/range {v0 .. v7}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0
.end method

.method private setButtonText(I)V
    .locals 6
    .param p1, "pageType"    # I

    .prologue
    .line 230
    packed-switch p1, :pswitch_data_0

    .line 241
    const-string v2, "Invalid current page type: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 242
    const v1, 0x7f0c0295

    .line 245
    .local v1, "positiveStringId":I
    :goto_0
    const v0, 0x7f0c0298

    .line 247
    .local v0, "negativeStringId":I
    iget-object v2, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mPositiveButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/layout/PlayActionButton;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v2, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mNegativeButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/layout/PlayActionButton;->setText(Ljava/lang/CharSequence;)V

    .line 249
    return-void

    .line 232
    .end local v0    # "negativeStringId":I
    .end local v1    # "positiveStringId":I
    :pswitch_0
    const v1, 0x7f0c0297

    .line 233
    .restart local v1    # "positiveStringId":I
    goto :goto_0

    .line 235
    .end local v1    # "positiveStringId":I
    :pswitch_1
    const v1, 0x7f0c0296

    .line 236
    .restart local v1    # "positiveStringId":I
    goto :goto_0

    .line 238
    .end local v1    # "positiveStringId":I
    :pswitch_2
    const v1, 0x7f0c0295

    .line 239
    .restart local v1    # "positiveStringId":I
    goto :goto_0

    .line 230
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private showInstallApprovalPage(IIZ)V
    .locals 11
    .param p1, "installIndex"    # I
    .param p2, "pageType"    # I
    .param p3, "fadeInAnimation"    # Z

    .prologue
    const/4 v10, 0x1

    .line 252
    invoke-direct {p0, p2}, Lcom/google/android/finsky/activities/MultiInstallActivity;->setButtonText(I)V

    .line 254
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallsForApproval:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .line 255
    .local v7, "installDetails":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    invoke-direct {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getApprovalType()I

    move-result v4

    .line 256
    .local v4, "approvalType":I
    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageName:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$000(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v0

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageTitle:Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$100(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, p1, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallsForApproval:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->permissions:[Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$200(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)[Ljava/lang/String;

    move-result-object v5

    iget-boolean v6, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mShowPermissionBucketsLearnMoreHeader:Z

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/activities/InstallApprovalFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;III[Ljava/lang/String;Z)Lcom/google/android/finsky/activities/InstallApprovalFragment;

    move-result-object v8

    .line 261
    .local v8, "nextFragment":Lcom/google/android/finsky/activities/InstallApprovalFragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v9

    .line 262
    .local v9, "transaction":Landroid/support/v4/app/FragmentTransaction;
    if-eqz p3, :cond_1

    .line 263
    const v0, 0x7f05000e

    const v1, 0x7f05000c

    invoke-virtual {v9, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 268
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentFragment:Lcom/google/android/finsky/activities/InstallApprovalFragment;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentFragment:Lcom/google/android/finsky/activities/InstallApprovalFragment;

    invoke-virtual {v9, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 271
    :cond_0
    const v0, 0x7f0a03af

    invoke-virtual {v9, v0, v8}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 272
    invoke-virtual {v9}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 273
    iput-object v8, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentFragment:Lcom/google/android/finsky/activities/InstallApprovalFragment;

    .line 276
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mPositiveButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v0, v10}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 277
    iget-object v0, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mNegativeButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v0, v10}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 278
    return-void

    .line 265
    :cond_1
    const v0, 0x7f050017

    const v1, 0x7f05001a

    invoke-virtual {v9, v0, v1}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    goto :goto_0
.end method

.method private static splitDocumentsOnWarnings(Ljava/util/List;ILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 10
    .param p1, "mode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p0, "documents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    .local p2, "installsWithNoWarnings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;>;"
    .local p3, "installsWithWarnings":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;>;"
    const/4 v9, 0x2

    .line 202
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 203
    :cond_0
    const-string v7, "The output lists are not initially empty."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v3

    .line 207
    .local v3, "installPolicies":Lcom/google/android/finsky/installer/InstallPolicies;
    if-ne p1, v9, :cond_2

    const-string v5, "bulk_update"

    .line 209
    .local v5, "reason":Ljava/lang/String;
    :goto_0
    sget-object v7, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 210
    .local v6, "treatDisabledUpdatesAsWarnings":Z
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 212
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    if-ne p1, v9, :cond_3

    .line 213
    invoke-virtual {v3, v0, v6}, Lcom/google/android/finsky/installer/InstallPolicies;->getUpdateWarningsForDocument(Lcom/google/android/finsky/api/model/Document;Z)Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;

    move-result-object v4

    .line 219
    .local v4, "installWarnings":Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    :goto_2
    new-instance v2, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    invoke-direct {v2, v0, v4, v5}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;-><init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;Ljava/lang/String;)V

    .line 220
    .local v2, "installDetails":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    invoke-virtual {v2}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->done()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 221
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 207
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "installDetails":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    .end local v4    # "installWarnings":Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    .end local v5    # "reason":Ljava/lang/String;
    .end local v6    # "treatDisabledUpdatesAsWarnings":Z
    :cond_2
    const-string v5, "bulk_install"

    goto :goto_0

    .line 216
    .restart local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v5    # "reason":Ljava/lang/String;
    .restart local v6    # "treatDisabledUpdatesAsWarnings":Z
    :cond_3
    invoke-virtual {v3, v0}, Lcom/google/android/finsky/installer/InstallPolicies;->getInstallWarningsForDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;

    move-result-object v4

    .restart local v4    # "installWarnings":Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    goto :goto_2

    .line 223
    .restart local v2    # "installDetails":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    :cond_4
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 226
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v2    # "installDetails":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    .end local v4    # "installWarnings":Lcom/google/android/finsky/installer/InstallPolicies$InstallWarnings;
    :cond_5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 344
    iget v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    if-nez v1, :cond_1

    .line 345
    const-string v1, "Unexpected click for page type: %d"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mPositiveButton:Lcom/google/android/play/layout/PlayActionButton;

    if-eq p1, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mNegativeButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v1, :cond_3

    .line 351
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mPositiveButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v1, v4}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 352
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mNegativeButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v1, v4}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 354
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mPositiveButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v1, :cond_5

    .line 355
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallsForApproval:Ljava/util/ArrayList;

    iget v2, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;

    .line 356
    .local v0, "installDetails":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    iget v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    packed-switch v1, :pswitch_data_0

    .line 375
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->done()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 376
    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->performSingleInstall(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)V

    .line 377
    iget v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    .line 378
    invoke-direct {p0, v4}, Lcom/google/android/finsky/activities/MultiInstallActivity;->displayPageOrFinish(Z)V

    goto :goto_0

    .line 358
    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->setAutoUpdateDisableWarningAccepted()V

    goto :goto_1

    .line 361
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->setLargeDownloadWarningAccepted()V

    .line 363
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->packageName:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$000(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/finsky/receivers/Installer;->setMobileDataAllowed(Ljava/lang/String;)V

    goto :goto_1

    .line 366
    :pswitch_2
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->setPermissionsWarningAccepted()V

    .line 369
    # getter for: Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->docId:Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;->access$500(Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/PermissionsBucketer;->setAcceptedNewBuckets(Ljava/lang/String;)V

    .line 372
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPermissionBucketsLearnMore:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_1

    .line 381
    :cond_4
    invoke-direct {p0, v3}, Lcom/google/android/finsky/activities/MultiInstallActivity;->displayPageOrFinish(Z)V

    goto :goto_0

    .line 382
    .end local v0    # "installDetails":Lcom/google/android/finsky/activities/MultiInstallActivity$InstallDetails;
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mNegativeButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v1, :cond_0

    .line 383
    iget v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    .line 384
    invoke-direct {p0, v4}, Lcom/google/android/finsky/activities/MultiInstallActivity;->displayPageOrFinish(Z)V

    goto :goto_0

    .line 356
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 114
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 116
    const v1, 0x7f0401be

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/MultiInstallActivity;->setContentView(I)V

    .line 118
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    .line 119
    const v1, 0x7f0a00f8

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/MultiInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mPositiveButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 120
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mPositiveButton:Lcom/google/android/play/layout/PlayActionButton;

    const v2, 0x7f0c02a0

    invoke-virtual {v1, v5, v2, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 121
    const v1, 0x7f0a0115

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/MultiInstallActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mNegativeButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 122
    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mNegativeButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0134

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v5, v2, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "MultiInstallActivity.mode"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mMode:I

    .line 125
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "MultiInstallActivity.install-account-name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallAccountName:Ljava/lang/String;

    .line 127
    if-nez p1, :cond_0

    .line 128
    iput v4, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    .line 129
    iput v4, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    .line 130
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "MultiInstallActivity.installs"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 131
    .local v0, "documents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->installAndShowWarnings(Ljava/util/List;)V

    .line 143
    .end local v0    # "documents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/api/model/Document;>;"
    :goto_0
    return-void

    .line 133
    :cond_0
    const-string v1, "MultiInstallActivity.installs-for-approval"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallsForApproval:Ljava/util/ArrayList;

    .line 135
    const-string v1, "MultiInstallActivity.show-learn-more-header"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mShowPermissionBucketsLearnMoreHeader:Z

    .line 137
    const-string v1, "MultiInstallActivity.current-install-index"

    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    .line 138
    const-string v1, "MultiInstallActivity.current-page-type"

    invoke-virtual {p1, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    .line 139
    iget v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/MultiInstallActivity;->setButtonText(I)V

    .line 140
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0a03af

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/activities/InstallApprovalFragment;

    iput-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentFragment:Lcom/google/android/finsky/activities/InstallApprovalFragment;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 148
    const-string v0, "MultiInstallActivity.installs-for-approval"

    iget-object v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mInstallsForApproval:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 149
    const-string v0, "MultiInstallActivity.show-learn-more-header"

    iget-boolean v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mShowPermissionBucketsLearnMoreHeader:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    const-string v0, "MultiInstallActivity.current-install-index"

    iget v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentInstallIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    const-string v0, "MultiInstallActivity.current-page-type"

    iget v1, p0, Lcom/google/android/finsky/activities/MultiInstallActivity;->mCurrentPageType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 152
    return-void
.end method
