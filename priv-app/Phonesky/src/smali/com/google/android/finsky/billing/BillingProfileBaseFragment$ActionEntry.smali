.class public Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
.super Ljava/lang/Object;
.source "BillingProfileBaseFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ActionEntry"
.end annotation


# instance fields
.field public final action:Landroid/view/View$OnClickListener;

.field public final displayTitle:Ljava/lang/String;

.field public final iconImage:Lcom/google/android/finsky/protos/Common$Image;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "option"    # Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    .param p2, "action"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 901
    iget-object v0, p1, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->displayTitle:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V

    .line 902
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "displayTitle"    # Ljava/lang/String;
    .param p2, "iconImage"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p3, "action"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 905
    iput-object p1, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->displayTitle:Ljava/lang/String;

    .line 906
    iput-object p2, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 907
    iput-object p3, p0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->action:Landroid/view/View$OnClickListener;

    .line 908
    return-void
.end method
