.class public Lcom/google/android/finsky/layout/DetailsColumnLayout;
.super Landroid/widget/FrameLayout;
.source "DetailsColumnLayout.java"


# static fields
.field public static final IS_ICS_OR_GREATER:Z


# instance fields
.field private mCardTransitionTarget:Landroid/view/View;

.field private mCurrentlyExpandedSectionId:I

.field protected mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

.field protected mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

.field protected mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

.field protected mDetailsExpandedScroller:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

.field protected mDetailsScroller:Landroid/widget/ScrollView;

.field private mExpandedContainerTopPadding:I

.field private mHasLeadingGap:Z

.field private final mLeadingHeroSectionId:I

.field private mOriginalDistanceBottom:I

.field private mOriginalDistanceTop:I

.field private mSectionsAbove:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSectionsBelow:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTextSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->IS_ICS_OR_GREATER:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 107
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 110
    sget-object v1, Lcom/android/vending/R$styleable;->DetailsColumnLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 112
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mHasLeadingGap:Z

    .line 114
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mLeadingHeroSectionId:I

    .line 116
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 118
    sget-boolean v1, Lcom/google/android/finsky/layout/DetailsColumnLayout;->IS_ICS_OR_GREATER:Z

    if-eqz v1, :cond_0

    .line 119
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsAbove:Ljava/util/Set;

    .line 120
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsBelow:Ljava/util/Set;

    .line 122
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsColumnLayout;

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mCurrentlyExpandedSectionId:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/DetailsColumnLayout;)Lcom/google/android/finsky/activities/TextSectionStateListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsColumnLayout;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mTextSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsColumnLayout;

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsColumnLayout;

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mExpandedContainerTopPadding:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/DetailsColumnLayout;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsColumnLayout;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsAbove:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/layout/DetailsColumnLayout;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsColumnLayout;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsBelow:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsColumnLayout;

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceBottom:I

    return v0
.end method

.method private collapseCurrentlyExpandedSectionIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
    .locals 25
    .param p1, "section"    # Lcom/google/android/finsky/layout/DetailsTextSection;

    .prologue
    .line 494
    new-instance v13, Landroid/animation/AnimatorSet;

    invoke-direct {v13}, Landroid/animation/AnimatorSet;-><init>()V

    .line 495
    .local v13, "collapseSet":Landroid/animation/AnimatorSet;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 497
    .local v9, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 501
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getTop()I

    move-result v21

    .line 502
    .local v21, "sectionTop":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v19

    .line 503
    .local v19, "scrollTop":I
    sub-int v14, v21, v19

    .line 504
    .local v14, "currentDistanceTop":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I

    sub-int v15, v14, v2

    .line 505
    .local v15, "extraScrollDistance":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v15}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 506
    sub-int v21, v21, v15

    .line 512
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->blockLayoutRequests()V

    .line 514
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getHeroVisibleHeight()I

    move-result v18

    .line 518
    .local v18, "initialScrollerPaddingTop":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsAbove:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 519
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsBelow:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 520
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildCount()I

    move-result v2

    move/from16 v0, v17

    if-ge v0, v2, :cond_2

    .line 521
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 522
    .local v10, "child":Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v11

    .line 523
    .local v11, "childTop":I
    move/from16 v0, v21

    if-ge v11, v0, :cond_0

    .line 524
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsAbove:Ljava/util/Set;

    invoke-virtual {v10}, Landroid/view/View;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 525
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v10, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 527
    :cond_0
    move/from16 v0, v21

    if-le v11, v0, :cond_1

    .line 528
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsBelow:Ljava/util/Set;

    invoke-virtual {v10}, Landroid/view/View;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 529
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceBottom:I

    int-to-float v2, v2

    invoke-virtual {v10, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 520
    :cond_1
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 532
    .end local v10    # "child":Landroid/view/View;
    .end local v11    # "childTop":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHeight()I

    move-result v20

    .line 536
    .local v20, "scrollerHeight":I
    new-instance v24, Landroid/animation/ValueAnimator;

    invoke-direct/range {v24 .. v24}, Landroid/animation/ValueAnimator;-><init>()V

    .line 537
    .local v24, "translateAnimation":Landroid/animation/ValueAnimator;
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    const/4 v3, 0x1

    move/from16 v0, v20

    int-to-float v4, v0

    aput v4, v2, v3

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 538
    const-wide/16 v2, 0x12c

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 539
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 540
    new-instance v2, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout$10;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 558
    new-instance v2, Lcom/google/android/finsky/layout/DetailsColumnLayout$11;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout$11;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 573
    move-object/from16 v0, v24

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 576
    new-instance v12, Landroid/animation/ValueAnimator;

    invoke-direct {v12}, Landroid/animation/ValueAnimator;-><init>()V

    .line 579
    .local v12, "collapsePaddingsAnimation":Landroid/animation/ValueAnimator;
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-virtual {v12, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 580
    const-wide/16 v2, 0x12c

    invoke-virtual {v12, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 581
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v12, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 582
    new-instance v2, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v2, v0, v1}, Lcom/google/android/finsky/layout/DetailsColumnLayout$12;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;I)V

    invoke-virtual {v12, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 601
    invoke-interface {v9, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 603
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    const-string v3, "sideBarProportion"

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v22

    .line 605
    .local v22, "sideBarsAnimator":Landroid/animation/ObjectAnimator;
    const/16 v23, 0x78

    .line 606
    .local v23, "sideBarsAnimatorLength":I
    move/from16 v0, v23

    int-to-long v2, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 607
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 608
    move-object/from16 v0, v22

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 611
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v2, v9}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->addFadeOutAnimatorsIcs(Ljava/util/List;)V

    .line 614
    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/16 v5, 0x4b

    const-wide/16 v6, 0x96

    const/4 v8, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v8}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v16

    .line 616
    .local v16, "fadeInSection":Landroid/animation/Animator;
    move-object/from16 v0, v16

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 619
    invoke-virtual {v13, v9}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 620
    invoke-virtual {v13}, Landroid/animation/AnimatorSet;->start()V

    .line 621
    return-void

    .line 579
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 603
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private collapseCurrentlyExpandedSectionPreIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
    .locals 13
    .param p1, "section"    # Lcom/google/android/finsky/layout/DetailsTextSection;

    .prologue
    const-wide/16 v4, 0x96

    const/4 v8, 0x0

    .line 432
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 435
    .local v1, "context":Landroid/content/Context;
    const-wide/16 v2, 0x4b

    new-instance v6, Lcom/google/android/finsky/layout/DetailsColumnLayout$8;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout$8;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 452
    .local v0, "fadeInMainContent":Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 455
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getHeroVisibleHeight()I

    move-result v12

    .line 458
    .local v12, "initialScrollerPaddingTop":I
    new-instance v6, Lcom/google/android/finsky/layout/DetailsColumnLayout$9;

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHeight()I

    move-result v2

    int-to-float v11, v2

    move-object v7, p0

    move v9, v8

    move v10, v8

    invoke-direct/range {v6 .. v12}, Lcom/google/android/finsky/layout/DetailsColumnLayout$9;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;FFFFI)V

    .line 476
    .local v6, "collapsePaddingsAnimation":Landroid/view/animation/TranslateAnimation;
    invoke-virtual {v6, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 477
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v6, v2}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 480
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->fadeOutContentPreIcs()V

    .line 483
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedScroller:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    invoke-virtual {v2, v6}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 484
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v2, v0}, Landroid/widget/ScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 485
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->fadeOutSideBarsPreIcs()V

    .line 486
    return-void
.end method

.method private expandSectionIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
    .locals 24
    .param p1, "section"    # Lcom/google/android/finsky/layout/DetailsTextSection;

    .prologue
    .line 288
    new-instance v13, Landroid/animation/AnimatorSet;

    invoke-direct {v13}, Landroid/animation/AnimatorSet;-><init>()V

    .line 289
    .local v13, "expandSet":Landroid/animation/AnimatorSet;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 292
    .local v9, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x4b

    const/4 v8, 0x0

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v8}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v14

    .line 294
    .local v14, "fadeOutSection":Landroid/animation/Animator;
    invoke-interface {v9, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getTop()I

    move-result v20

    .line 300
    .local v20, "sectionTop":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getBottom()I

    move-result v19

    .line 301
    .local v19, "sectionBottom":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getHeight()I

    move-result v18

    .line 302
    .local v18, "scrollerHeight":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v17

    .line 304
    .local v17, "scrollTop":I
    sub-int v2, v20, v17

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I

    .line 305
    add-int v2, v17, v18

    sub-int v2, v2, v19

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceBottom:I

    .line 308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsAbove:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsBelow:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 310
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildCount()I

    move-result v2

    if-ge v15, v2, :cond_2

    .line 311
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v2, v15}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 312
    .local v10, "child":Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v11

    .line 313
    .local v11, "childTop":I
    move/from16 v0, v20

    if-ge v11, v0, :cond_0

    .line 314
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsAbove:Ljava/util/Set;

    invoke-virtual {v10}, Landroid/view/View;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 316
    :cond_0
    move/from16 v0, v20

    if-le v11, v0, :cond_1

    .line 317
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mSectionsBelow:Ljava/util/Set;

    invoke-virtual {v10}, Landroid/view/View;->getId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 310
    :cond_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 323
    .end local v10    # "child":Landroid/view/View;
    .end local v11    # "childTop":I
    :cond_2
    new-instance v23, Landroid/animation/ValueAnimator;

    invoke-direct/range {v23 .. v23}, Landroid/animation/ValueAnimator;-><init>()V

    .line 324
    .local v23, "translateAnimation":Landroid/animation/ValueAnimator;
    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    const/4 v3, 0x1

    move/from16 v0, v18

    int-to-float v4, v0

    aput v4, v2, v3

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 325
    const-wide/16 v2, 0x12c

    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 326
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 327
    new-instance v2, Lcom/google/android/finsky/layout/DetailsColumnLayout$4;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout$4;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->blockLayoutRequests()V

    .line 349
    new-instance v2, Lcom/google/android/finsky/layout/DetailsColumnLayout$5;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout$5;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 359
    move-object/from16 v0, v23

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setVisibility(I)V

    .line 363
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setAlpha(F)V

    .line 364
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setSideBarProportion(F)V

    .line 365
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->hideAllViews()V

    .line 367
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getHeroVisibleHeight()I

    move-result v16

    .line 370
    .local v16, "initialScrollerPaddingTop":I
    new-instance v12, Landroid/animation/ValueAnimator;

    invoke-direct {v12}, Landroid/animation/ValueAnimator;-><init>()V

    .line 374
    .local v12, "expandPaddingsAnimation":Landroid/animation/ValueAnimator;
    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-virtual {v12, v2}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 375
    const-wide/16 v2, 0x12c

    invoke-virtual {v12, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 376
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v12, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 377
    new-instance v2, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-direct {v2, v0, v1}, Lcom/google/android/finsky/layout/DetailsColumnLayout$6;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;I)V

    invoke-virtual {v12, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 397
    new-instance v2, Lcom/google/android/finsky/layout/DetailsColumnLayout$7;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout$7;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V

    invoke-virtual {v12, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 407
    invoke-interface {v9, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 409
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    const-string v3, "sideBarProportion"

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_1

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v21

    .line 411
    .local v21, "sideBarsAnimator":Landroid/animation/ObjectAnimator;
    const/16 v22, 0x78

    .line 412
    .local v22, "sideBarsAnimatorDelay":I
    move/from16 v0, v22

    int-to-long v2, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 413
    const/16 v2, 0xb4

    int-to-long v2, v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 414
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 415
    move-object/from16 v0, v21

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 417
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v2, v9}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->addFadeInAnimatorsIcs(Ljava/util/List;)V

    .line 420
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->setSeparatorsVisible(Z)V

    .line 423
    invoke-virtual {v13, v9}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 424
    invoke-virtual {v13}, Landroid/animation/AnimatorSet;->start()V

    .line 425
    return-void

    .line 374
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 409
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private expandSectionPreIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
    .locals 14
    .param p1, "section"    # Lcom/google/android/finsky/layout/DetailsTextSection;

    .prologue
    const/4 v13, 0x0

    const/4 v2, 0x0

    .line 219
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 222
    .local v7, "context":Landroid/content/Context;
    const-wide/16 v4, 0x4b

    new-instance v1, Lcom/google/android/finsky/layout/DetailsColumnLayout$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout$2;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V

    invoke-static {v7, v4, v5, v1}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v8

    .line 229
    .local v8, "fadeOutMainContent":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v8, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 232
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    invoke-virtual {v1, v13}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setVisibility(I)V

    .line 233
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->hideAllViews()V

    .line 238
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getTop()I

    move-result v12

    .line 239
    .local v12, "sectionTop":I
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getBottom()I

    move-result v11

    .line 240
    .local v11, "sectionBottom":I
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v10

    .line 241
    .local v10, "scrollerHeight":I
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v9

    .line 243
    .local v9, "scrollTop":I
    sub-int v1, v12, v9

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I

    .line 244
    add-int v1, v9, v10

    sub-int/2addr v1, v11

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceBottom:I

    .line 247
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getHeroVisibleHeight()I

    move-result v6

    .line 250
    .local v6, "initialScrollerPaddingTop":I
    new-instance v0, Lcom/google/android/finsky/layout/DetailsColumnLayout$3;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    int-to-float v5, v1

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/layout/DetailsColumnLayout$3;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;FFFFI)V

    .line 269
    .local v0, "expandPaddingsAnimation":Landroid/view/animation/TranslateAnimation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 270
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 273
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v1, v13}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->setSeparatorsVisible(Z)V

    .line 276
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v1, v8}, Landroid/widget/ScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 277
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedScroller:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 278
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->fadeInContentPreIcs()V

    .line 279
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->fadeInSideBarsPreIcs()V

    .line 280
    return-void
.end method

.method private getHeroVisibleHeight()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 628
    iget-boolean v4, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mHasLeadingGap:Z

    if-nez v4, :cond_1

    .line 634
    :cond_0
    :goto_0
    return v3

    .line 631
    :cond_1
    iget v4, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mLeadingHeroSectionId:I

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 632
    .local v0, "leadingHero":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    .line 633
    .local v1, "leadingHeroBottom":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    .line 634
    .local v2, "scrollTop":I
    if-le v1, v2, :cond_0

    sub-int v3, v1, v2

    goto :goto_0
.end method


# virtual methods
.method public collapseCurrentlyExpandedSection()V
    .locals 3

    .prologue
    .line 204
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    iget v2, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mCurrentlyExpandedSectionId:I

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsTextSection;

    .line 206
    .local v0, "section":Lcom/google/android/finsky/layout/DetailsTextSection;
    sget-boolean v1, Lcom/google/android/finsky/layout/DetailsColumnLayout;->IS_ICS_OR_GREATER:Z

    if-eqz v1, :cond_0

    .line 207
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->collapseCurrentlyExpandedSectionIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V

    .line 211
    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mCurrentlyExpandedSectionId:I

    .line 212
    return-void

    .line 209
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->collapseCurrentlyExpandedSectionPreIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V

    goto :goto_0
.end method

.method public configure(Lcom/google/android/finsky/activities/TextSectionStateListener;)V
    .locals 2
    .param p1, "textSectionStateListener"    # Lcom/google/android/finsky/activities/TextSectionStateListener;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mTextSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;

    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedScroller:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    new-instance v1, Lcom/google/android/finsky/layout/DetailsColumnLayout$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout$1;-><init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->setFlingToDismissListener(Lcom/google/android/finsky/layout/scroll/GestureScrollView$FlingToDismissListener;)V

    .line 161
    return-void
.end method

.method public disableActionBarOverlayScrolling()V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mExpandedContainerTopPadding:I

    .line 146
    return-void
.end method

.method public enableActionBarOverlayScrolling()V
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/UiUtils;->getActionBarHeight(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mExpandedContainerTopPadding:I

    .line 142
    return-void
.end method

.method public expandSection(ILcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 6
    .param p1, "sectionLayoutId"    # I
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "backendId"    # I
    .param p4, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 184
    iput p1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mCurrentlyExpandedSectionId:I

    .line 188
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DetailsTextSection;

    .line 190
    .local v1, "section":Lcom/google/android/finsky/layout/DetailsTextSection;
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mTextSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->populateFromSection(Lcom/google/android/finsky/layout/DetailsTextSection;Lcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/activities/TextSectionStateListener;)V

    .line 193
    sget-boolean v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->IS_ICS_OR_GREATER:Z

    if-eqz v0, :cond_0

    .line 194
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->expandSectionIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->expandSectionPreIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V

    goto :goto_0
.end method

.method public getCardTransitionTarget()Landroid/view/View;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mCardTransitionTarget:Landroid/view/View;

    return-object v0
.end method

.method public getCurrentlyExpandedSectionId()I
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mCurrentlyExpandedSectionId:I

    return v0
.end method

.method public getLeadingHeroSectionId()I
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mLeadingHeroSectionId:I

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 126
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 128
    const v0, 0x7f0a00c9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    .line 129
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    const v1, 0x7f0a00ca

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    .line 131
    const v0, 0x7f0a0210

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    const v1, 0x7f0a0212

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedScroller:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedScroller:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    const v1, 0x7f0a0213

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    .line 137
    const v0, 0x7f0a020f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mCardTransitionTarget:Landroid/view/View;

    .line 138
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 639
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 642
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setScrollerWidth(I)V

    .line 643
    return-void
.end method
