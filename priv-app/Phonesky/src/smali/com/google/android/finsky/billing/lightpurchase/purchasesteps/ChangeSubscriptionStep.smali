.class public Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "ChangeSubscriptionStep.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;",
        ">;",
        "Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;"
    }
.end annotation


# instance fields
.field private mBackend:I

.field private mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 38
    const/16 v0, 0x500

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method public static newInstance(ILcom/google/android/finsky/protos/Purchase$ChangeSubscription;)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;
    .locals 4
    .param p0, "backend"    # I
    .param p1, "changeSubscription"    # Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    .prologue
    .line 44
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;-><init>()V

    .line 46
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;
    const-string v2, "ChangeSubscriptionStep.backend"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 47
    const-string v2, "ChangeSubscriptionStep.changeSubscription"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 48
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->setArguments(Landroid/os/Bundle;)V

    .line 49
    return-object v1
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 82
    const v0, 0x7f0c020d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onContinueButtonClicked()V
    .locals 1

    .prologue
    .line 87
    const/16 v0, 0x501

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->logClick(I)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->switchFromChangeSubscriptionToCart()V

    .line 89
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 56
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "ChangeSubscriptionStep.backend"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->mBackend:I

    .line 57
    const-string v1, "ChangeSubscriptionStep.changeSubscription"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    .line 58
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    const v4, 0x7f0400c4

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 66
    .local v2, "mMainView":Landroid/view/View;
    const v4, 0x7f0a009c

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 67
    .local v3, "title":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    iget-object v4, v4, Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;->title:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget v5, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->mBackend:I

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 71
    .local v0, "corpusColor":Landroid/content/res/ColorStateList;
    const v4, 0x7f0a00c3

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 72
    .local v1, "description":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/ChangeSubscriptionStep;->mChangeSubscription:Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;

    iget-object v4, v4, Lcom/google/android/finsky/protos/Purchase$ChangeSubscription;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/finsky/billing/BillingUtils;->parseHtmlAndColorizeEm(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 75
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 77
    return-object v2
.end method
