.class public Lcom/google/android/finsky/layout/play/PlayCardViewRate;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayCardViewRate.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;
    }
.end annotation


# instance fields
.field private mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

.field private mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

.field private mRateBarSeparator:Landroid/view/View;

.field private mRateListener:Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;

.field private mState:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mState:I

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/PlayCardViewRate;FZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardViewRate;
    .param p1, "x1"    # F
    .param p2, "x2"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setRating(FZ)V

    return-void
.end method

.method private setRating(FZ)V
    .locals 10
    .param p1, "ratingFloat"    # F
    .param p2, "isCommitted"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 393
    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 395
    .local v3, "rating":I
    if-gtz v3, :cond_0

    .line 423
    :goto_0
    return-void

    .line 399
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateListener:Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;

    if-eqz v4, :cond_1

    .line 400
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateListener:Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;

    invoke-interface {v4, v3, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;->onRate(IZ)V

    .line 403
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    invoke-virtual {v4, v3}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->setRating(I)V

    .line 404
    if-lez v3, :cond_2

    .line 405
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 406
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f100004

    new-array v6, v9, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    invoke-static {v0, v4, v5}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 412
    .end local v0    # "context":Landroid/content/Context;
    :cond_2
    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mState:I

    if-nez v4, :cond_3

    .line 413
    invoke-virtual {p0, v9}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setState(I)V

    .line 414
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getContext()Landroid/content/Context;

    move-result-object v4

    const-wide/16 v6, 0x64

    const/4 v5, 0x0

    invoke-static {v4, v6, v7, v5}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v2

    .line 416
    .local v2, "fadeInAnimation":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    invoke-virtual {v4, v8}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->setVisibility(I)V

    .line 417
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 418
    .local v1, "doc":Lcom/google/android/finsky/api/model/Document;
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->configure(II)V

    .line 419
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    invoke-virtual {v4, v2}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 422
    .end local v1    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v2    # "fadeInAnimation":Landroid/view/animation/Animation;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->invalidate()V

    goto :goto_0
.end method


# virtual methods
.method public bindLoading()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 322
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->bindLoading()V

    .line 323
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 326
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    if-eqz v0, :cond_1

    .line 327
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->setVisibility(I)V

    .line 329
    :cond_1
    return-void
.end method

.method public clearRating()V
    .locals 5

    .prologue
    .line 426
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateListener:Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;

    if-eqz v1, :cond_0

    .line 427
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateListener:Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;

    invoke-interface {v1}, Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;->onRateCleared()V

    .line 429
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->setRating(I)V

    .line 430
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mState:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 431
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getContext()Landroid/content/Context;

    move-result-object v1

    const-wide/16 v2, 0xfa

    new-instance v4, Lcom/google/android/finsky/layout/play/PlayCardViewRate$2;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate$2;-><init>(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 438
    .local v0, "fadeOutAnimation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 440
    .end local v0    # "fadeOutAnimation":Landroid/view/animation/Animation;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->invalidate()V

    .line 441
    return-void
.end method

.method public getCardType()I
    .locals 1

    .prologue
    .line 103
    const/16 v0, 0xd

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    const v1, 0x7f0a032f

    .line 86
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->onFinishInflate()V

    .line 88
    const v0, 0x7f0a02d2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    .line 89
    const v0, 0x7f0a02d3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayRatingBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    .line 90
    const v0, 0x7f0a02d4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    .line 92
    sget-boolean v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->DISABLE_NESTED_FOCUS_TRAVERSAL:Z

    if-nez v0, :cond_0

    .line 96
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setNextFocusDownId(I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0a01b3

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 99
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 48
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getPaddingLeft()I

    move-result v18

    .line 226
    .local v18, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getPaddingRight()I

    move-result v19

    .line 227
    .local v19, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getPaddingTop()I

    move-result v20

    .line 228
    .local v20, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getPaddingBottom()I

    move-result v17

    .line 230
    .local v17, "paddingBottom":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getWidth()I

    move-result v42

    .line 231
    .local v42, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getHeight()I

    move-result v5

    .line 233
    .local v5, "height":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v36

    check-cast v36, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 235
    .local v36, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mTitle:Landroid/widget/TextView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v40

    check-cast v40, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 237
    .local v40, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 239
    .local v14, "overflowLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 241
    .local v7, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardSnippet;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v32

    check-cast v32, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 244
    .local v32, "snippetLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getVisibility()I

    move-result v43

    const/16 v44, 0x8

    move/from16 v0, v43

    move/from16 v1, v44

    if-eq v0, v1, :cond_0

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v27

    check-cast v27, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 247
    .local v27, "rateBarSeparatorLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 250
    .local v23, "rateBarContainerLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getMeasuredWidth()I

    move-result v25

    .line 251
    .local v25, "rateBarContainerWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getMeasuredHeight()I

    move-result v21

    .line 252
    .local v21, "rateBarContainerHeight":I
    sub-int v43, v42, v18

    sub-int v43, v43, v19

    sub-int v43, v43, v25

    div-int/lit8 v43, v43, 0x2

    add-int v22, v18, v43

    .line 254
    .local v22, "rateBarContainerLeft":I
    sub-int v43, v5, v17

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v44, v0

    sub-int v43, v43, v44

    sub-int v24, v43, v21

    .line 256
    .local v24, "rateBarContainerTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v43, v0

    add-int v44, v22, v25

    add-int v45, v24, v21

    move-object/from16 v0, v43

    move/from16 v1, v22

    move/from16 v2, v24

    move/from16 v3, v44

    move/from16 v4, v45

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->layout(IIII)V

    .line 259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/view/View;->getMeasuredHeight()I

    move-result v26

    .line 260
    .local v26, "rateBarSeparatorHeight":I
    sub-int v43, v42, v19

    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v44, v0

    sub-int v28, v43, v44

    .line 261
    .local v28, "rateBarSeparatorRight":I
    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v43, v0

    sub-int v43, v24, v43

    move-object/from16 v0, v27

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v44, v0

    sub-int v43, v43, v44

    sub-int v29, v43, v26

    .line 263
    .local v29, "rateBarSeparatorTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/view/View;->getMeasuredWidth()I

    move-result v44

    sub-int v44, v28, v44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/view/View;->getMeasuredHeight()I

    move-result v45

    add-int v45, v45, v29

    move-object/from16 v0, v43

    move/from16 v1, v44

    move/from16 v2, v29

    move/from16 v3, v28

    move/from16 v4, v45

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 268
    .end local v21    # "rateBarContainerHeight":I
    .end local v22    # "rateBarContainerLeft":I
    .end local v23    # "rateBarContainerLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v24    # "rateBarContainerTop":I
    .end local v25    # "rateBarContainerWidth":I
    .end local v26    # "rateBarSeparatorHeight":I
    .end local v27    # "rateBarSeparatorLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v28    # "rateBarSeparatorRight":I
    .end local v29    # "rateBarSeparatorTop":I
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v35

    .line 269
    .local v35, "thumbnailHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v37

    .line 270
    .local v37, "thumbnailWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v43, v0

    move-object/from16 v0, v36

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v44, v0

    add-int v44, v44, v18

    move-object/from16 v0, v36

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v45, v0

    add-int v45, v45, v20

    move-object/from16 v0, v36

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v46, v0

    add-int v46, v46, v18

    add-int v46, v46, v37

    move-object/from16 v0, v36

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v47, v0

    add-int v47, v47, v20

    add-int v47, v47, v35

    invoke-virtual/range {v43 .. v47}, Lcom/google/android/play/layout/PlayCardThumbnail;->layout(IIII)V

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardThumbnail;->getRight()I

    move-result v43

    move-object/from16 v0, v36

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v44, v0

    add-int v34, v43, v44

    .line 277
    .local v34, "textColumnLeft":I
    move-object/from16 v0, v40

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v43, v0

    add-int v41, v20, v43

    .line 278
    .local v41, "titleTop":I
    move-object/from16 v0, v40

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v43, v0

    add-int v39, v34, v43

    .line 279
    .local v39, "titleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mTitle:Landroid/widget/TextView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v38

    .line 280
    .local v38, "titleHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mTitle:Landroid/widget/TextView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mTitle:Landroid/widget/TextView;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v44

    add-int v44, v44, v39

    add-int v45, v41, v38

    move-object/from16 v0, v43

    move/from16 v1, v39

    move/from16 v2, v41

    move/from16 v3, v44

    move/from16 v4, v45

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 283
    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v43, v0

    add-int v16, v41, v43

    .line 284
    .local v16, "overflowTop":I
    sub-int v43, v42, v19

    iget v0, v14, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v44, v0

    sub-int v15, v43, v44

    .line 285
    .local v15, "overflowRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v44

    sub-int v44, v15, v44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v45

    add-int v45, v45, v16

    move-object/from16 v0, v43

    move/from16 v1, v44

    move/from16 v2, v16

    move/from16 v3, v45

    invoke-virtual {v0, v1, v2, v15, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredHeight()I

    move-result v6

    .line 289
    .local v6, "labelHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mTitle:Landroid/widget/TextView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/widget/TextView;->getBottom()I

    move-result v43

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v44, v0

    add-int v9, v43, v44

    .line 290
    .local v9, "labelTop":I
    sub-int v43, v42, v19

    iget v0, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v44, v0

    sub-int v8, v43, v44

    .line 291
    .local v8, "labelRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v44

    sub-int v44, v8, v44

    add-int v45, v9, v6

    move-object/from16 v0, v43

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-virtual {v0, v1, v9, v8, v2}, Lcom/google/android/play/layout/PlayCardLabelView;->layout(IIII)V

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardLabelView;->getBaseline()I

    move-result v43

    add-int v43, v43, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/play/layout/PlayTextView;->getBaseline()I

    move-result v44

    sub-int v33, v43, v44

    .line 295
    .local v33, "subtitleTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v44

    add-int v44, v44, v39

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v45

    add-int v45, v45, v33

    move-object/from16 v0, v43

    move/from16 v1, v39

    move/from16 v2, v33

    move/from16 v3, v44

    move/from16 v4, v45

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayTextView;->layout(IIII)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v43

    const/16 v44, 0x8

    move/from16 v0, v43

    move/from16 v1, v44

    if-eq v0, v1, :cond_1

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/play/layout/PlayCardThumbnail;->getBottom()I

    move-result v43

    move-object/from16 v0, v32

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v44, v0

    sub-int v30, v43, v44

    .line 300
    .local v30, "snippetBottom":I
    move-object/from16 v0, v32

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v43, v0

    add-int v31, v39, v43

    .line 301
    .local v31, "snippetLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/play/layout/PlayCardSnippet;->getMeasuredHeight()I

    move-result v44

    sub-int v44, v30, v44

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Lcom/google/android/play/layout/PlayCardSnippet;->getMeasuredWidth()I

    move-result v45

    add-int v45, v45, v31

    move-object/from16 v0, v43

    move/from16 v1, v31

    move/from16 v2, v44

    move/from16 v3, v45

    move/from16 v4, v30

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/layout/PlayCardSnippet;->layout(IIII)V

    .line 305
    .end local v30    # "snippetBottom":I
    .end local v31    # "snippetLeft":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->getVisibility()I

    move-result v43

    const/16 v44, 0x8

    move/from16 v0, v43

    move/from16 v1, v44

    if-eq v0, v1, :cond_2

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->getMeasuredWidth()I

    move-result v44

    add-int v44, v44, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->getMeasuredHeight()I

    move-result v45

    add-int v45, v45, v20

    move-object/from16 v0, v43

    move/from16 v1, v18

    move/from16 v2, v20

    move/from16 v3, v44

    move/from16 v4, v45

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->layout(IIII)V

    .line 311
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    .line 312
    .local v13, "loadingWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v43, v0

    invoke-virtual/range {v43 .. v43}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 313
    .local v10, "loadingHeight":I
    sub-int v43, v42, v18

    sub-int v43, v43, v19

    sub-int v43, v43, v13

    div-int/lit8 v43, v43, 0x2

    add-int v11, v18, v43

    .line 314
    .local v11, "loadingLeft":I
    sub-int v43, v5, v20

    sub-int v43, v43, v17

    sub-int v43, v43, v10

    div-int/lit8 v43, v43, 0x2

    add-int v12, v20, v43

    .line 315
    .local v12, "loadingTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v43, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v44, v0

    invoke-virtual/range {v44 .. v44}, Landroid/view/View;->getMeasuredWidth()I

    move-result v44

    add-int v44, v44, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v45, v0

    invoke-virtual/range {v45 .. v45}, Landroid/view/View;->getMeasuredHeight()I

    move-result v45

    add-int v45, v45, v12

    move-object/from16 v0, v43

    move/from16 v1, v44

    move/from16 v2, v45

    invoke-virtual {v0, v11, v12, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 318
    return-void
.end method

.method protected onMeasure(II)V
    .locals 32
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mData:Ljava/lang/Object;

    move-object/from16 v28, v0

    if-eqz v28, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mData:Ljava/lang/Object;

    move-object/from16 v28, v0

    check-cast v28, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v28

    const/16 v29, 0x3

    move/from16 v0, v28

    move/from16 v1, v29

    if-ne v0, v1, :cond_6

    const/4 v4, 0x1

    .line 111
    .local v4, "isApp":Z
    :goto_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v27

    .line 112
    .local v27, "width":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 114
    .local v3, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getPaddingLeft()I

    move-result v9

    .line 115
    .local v9, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getPaddingRight()I

    move-result v10

    .line 116
    .local v10, "paddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getPaddingTop()I

    move-result v11

    .line 117
    .local v11, "paddingTop":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getPaddingBottom()I

    move-result v8

    .line 118
    .local v8, "paddingBottom":I
    sub-int v28, v27, v9

    sub-int v2, v28, v10

    .line 120
    .local v2, "contentWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v23

    check-cast v23, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 122
    .local v23, "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mTitle:Landroid/widget/TextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v25

    check-cast v25, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 124
    .local v25, "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v18

    check-cast v18, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 126
    .local v18, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayCardLabelView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 128
    .local v5, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayCardSnippet;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    check-cast v16, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 131
    .local v16, "snippetLp":Landroid/view/ViewGroup$MarginLayoutParams;
    const/4 v14, 0x0

    .line 133
    .local v14, "rateBlockHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 135
    .local v12, "rateBarLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 138
    .local v13, "rateBarSeparatorLp":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    move/from16 v0, v29

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->measure(II)V

    .line 141
    iget v0, v12, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getMeasuredHeight()I

    move-result v29

    add-int v28, v28, v29

    iget v0, v12, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v29, v0

    add-int v28, v28, v29

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move/from16 v29, v0

    add-int v14, v28, v29

    .line 143
    if-eqz v4, :cond_0

    .line 145
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v28, v0

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v29, v0

    add-int v28, v28, v29

    add-int v14, v14, v28

    .line 151
    :cond_0
    sub-int v28, v3, v11

    sub-int v28, v28, v8

    sub-int v22, v28, v14

    .line 152
    .local v22, "thumbnailHeight":I
    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mThumbnailAspectRatio:F

    move/from16 v29, v0

    div-float v28, v28, v29

    sub-int v29, v27, v9

    sub-int v29, v29, v10

    mul-int/lit8 v29, v29, 0x2

    div-int/lit8 v29, v29, 0x3

    move/from16 v0, v29

    int-to-float v0, v0

    move/from16 v29, v0

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->min(FF)F

    move-result v28

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v24, v0

    .line 155
    .local v24, "thumbnailWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    move-object/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    move/from16 v0, v24

    move/from16 v1, v29

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    const/high16 v30, 0x40000000    # 2.0f

    move/from16 v0, v22

    move/from16 v1, v30

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v30

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/play/layout/PlayCardThumbnail;->measure(II)V

    .line 158
    sub-int v28, v2, v24

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    sub-int v21, v28, v29

    .line 160
    .local v21, "textColumnWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v28, v0

    if-eqz v28, :cond_1

    .line 165
    if-eqz v4, :cond_7

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    sub-int v28, v2, v28

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    sub-int v15, v28, v29

    .line 168
    .local v15, "separatorWidth":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    move-object/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    move/from16 v0, v29

    invoke-static {v15, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    move/from16 v30, v0

    const/high16 v31, 0x40000000    # 2.0f

    invoke-static/range {v30 .. v31}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v30

    invoke-virtual/range {v28 .. v30}, Landroid/view/View;->measure(II)V

    .line 174
    .end local v15    # "separatorWidth":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v28, v0

    const/high16 v29, -0x80000000

    move/from16 v0, v29

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/play/layout/PlayCardLabelView;->measure(II)V

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLabel:Lcom/google/android/play/layout/PlayCardLabelView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayCardLabelView;->getMeasuredWidth()I

    move-result v28

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v29, v0

    add-int v28, v28, v29

    iget v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    add-int v6, v28, v29

    .line 179
    .local v6, "labelWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mOverflow:Landroid/widget/ImageView;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Landroid/widget/ImageView;->measure(II)V

    .line 182
    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    sub-int v28, v21, v28

    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    sub-int v26, v28, v29

    .line 183
    .local v26, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mTitle:Landroid/widget/TextView;

    move-object/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    move/from16 v0, v26

    move/from16 v1, v29

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Landroid/widget/TextView;->measure(II)V

    .line 186
    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    sub-int v28, v21, v28

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    sub-int v28, v28, v29

    sub-int v20, v28, v6

    .line 191
    .local v20, "subtitleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v19

    .line 193
    .local v19, "subtitleMeasuredWidth":I
    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_2

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSubtitle:Lcom/google/android/play/layout/PlayTextView;

    move-object/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    move/from16 v0, v20

    move/from16 v1, v29

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    .line 198
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/play/layout/PlayCardSnippet;->getVisibility()I

    move-result v28

    const/16 v29, 0x8

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_3

    .line 199
    move-object/from16 v0, v16

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v28, v0

    sub-int v28, v21, v28

    move-object/from16 v0, v16

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v29, v0

    sub-int v17, v28, v29

    .line 200
    .local v17, "snippetWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mSnippet1:Lcom/google/android/play/layout/PlayCardSnippet;

    move-object/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    move/from16 v0, v17

    move/from16 v1, v29

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    const/high16 v30, 0x40000000    # 2.0f

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/play/layout/PlayCardSnippet;->measure(II)V

    .line 204
    .end local v17    # "snippetWidth":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->getVisibility()I

    move-result v28

    const/16 v29, 0x8

    move/from16 v0, v28

    move/from16 v1, v29

    if-eq v0, v1, :cond_5

    .line 206
    move/from16 v7, v22

    .line 207
    .local v7, "overlayHeight":I
    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    move-object/from16 v28, v0

    if-eqz v28, :cond_4

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBarSeparator:Landroid/view/View;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Landroid/view/View;->getMeasuredHeight()I

    move-result v28

    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move/from16 v29, v0

    add-int v28, v28, v29

    add-int v7, v7, v28

    .line 214
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    move-object/from16 v28, v0

    const/high16 v29, 0x40000000    # 2.0f

    move/from16 v0, v29

    invoke-static {v2, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v29

    const/high16 v30, 0x40000000    # 2.0f

    move/from16 v0, v30

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v30

    invoke-virtual/range {v28 .. v30}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->measure(II)V

    .line 218
    .end local v7    # "overlayHeight":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mLoadingIndicator:Landroid/view/View;

    move-object/from16 v28, v0

    const/16 v29, 0x0

    const/16 v30, 0x0

    invoke-virtual/range {v28 .. v30}, Landroid/view/View;->measure(II)V

    .line 220
    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-virtual {v0, v1, v3}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setMeasuredDimension(II)V

    .line 221
    return-void

    .line 108
    .end local v2    # "contentWidth":I
    .end local v3    # "height":I
    .end local v4    # "isApp":Z
    .end local v5    # "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v6    # "labelWidth":I
    .end local v8    # "paddingBottom":I
    .end local v9    # "paddingLeft":I
    .end local v10    # "paddingRight":I
    .end local v11    # "paddingTop":I
    .end local v12    # "rateBarLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v13    # "rateBarSeparatorLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v14    # "rateBlockHeight":I
    .end local v16    # "snippetLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v18    # "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v19    # "subtitleMeasuredWidth":I
    .end local v20    # "subtitleWidth":I
    .end local v21    # "textColumnWidth":I
    .end local v22    # "thumbnailHeight":I
    .end local v23    # "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v24    # "thumbnailWidth":I
    .end local v25    # "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v26    # "titleWidth":I
    .end local v27    # "width":I
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 165
    .restart local v2    # "contentWidth":I
    .restart local v3    # "height":I
    .restart local v4    # "isApp":Z
    .restart local v5    # "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v8    # "paddingBottom":I
    .restart local v9    # "paddingLeft":I
    .restart local v10    # "paddingRight":I
    .restart local v11    # "paddingTop":I
    .restart local v12    # "rateBarLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v13    # "rateBarSeparatorLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v14    # "rateBlockHeight":I
    .restart local v16    # "snippetLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v18    # "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v21    # "textColumnWidth":I
    .restart local v22    # "thumbnailHeight":I
    .restart local v23    # "thumbnailLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v24    # "thumbnailWidth":I
    .restart local v25    # "titleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v27    # "width":I
    :cond_7
    iget v0, v13, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move/from16 v28, v0

    sub-int v28, v21, v28

    move-object/from16 v0, v25

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move/from16 v29, v0

    sub-int v15, v28, v29

    goto/16 :goto_1
.end method

.method public onRatingChanged(Lcom/google/android/finsky/layout/play/PlayRatingBar;I)V
    .locals 9
    .param p1, "ratingBar"    # Lcom/google/android/finsky/layout/play/PlayRatingBar;
    .param p2, "rating"    # I

    .prologue
    const/4 v3, 0x0

    .line 348
    int-to-float v0, p2

    invoke-direct {p0, v0, v3}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->setRating(FZ)V

    .line 351
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getLoggingData()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 352
    .local v8, "parentNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x4b8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v8}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 355
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v4/app/FragmentActivity;

    if-nez v0, :cond_0

    .line 356
    const-string v0, "View context is not a fragment activity in Rate Card"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 379
    :goto_0
    return-void

    .line 359
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getContext()Landroid/content/Context;

    move-result-object v4

    check-cast v4, Landroid/support/v4/app/FragmentActivity;

    .line 363
    .local v4, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getData()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/api/model/Document;

    .line 364
    .local v7, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v2

    int-to-float v3, p2

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    new-instance v5, Lcom/google/android/finsky/layout/play/PlayCardViewRate$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    const/16 v6, 0x19d

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/RateReviewHelper;->rateDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;I)V

    goto :goto_0
.end method

.method public setData(Ljava/lang/Object;I)V
    .locals 5
    .param p1, "data"    # Ljava/lang/Object;
    .param p2, "backendId"    # I

    .prologue
    .line 333
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;->setData(Ljava/lang/Object;I)V

    move-object v0, p1

    .line 334
    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 337
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    const v3, 0x7f0b0172

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->setVerticalPadding(I)V

    .line 338
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mBackendId:I

    invoke-virtual {v2, v3, v4, p0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->configure(IILcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;)V

    .line 340
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getRateString(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v1

    .line 342
    .local v1, "rateCardLabel":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 343
    return-void
.end method

.method public setRateListener(Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;)V
    .locals 0
    .param p1, "rateListener"    # Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateListener:Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;

    .line 72
    return-void
.end method

.method public setState(I)V
    .locals 4
    .param p1, "state"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 75
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mState:I

    if-ne v0, p1, :cond_0

    .line 82
    :goto_0
    return-void

    .line 78
    :cond_0
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mState:I

    .line 80
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mContentOverlay:Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;

    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mState:I

    if-ne v0, v2, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewRateOverlay;->setVisibility(I)V

    .line 81
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mRateBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mState:I

    if-eq v3, v2, :cond_1

    move v1, v2

    :cond_1
    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->setEnabled(Z)V

    goto :goto_0

    .line 80
    :cond_2
    const/16 v0, 0x8

    goto :goto_1
.end method

.method public setVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 445
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewRate;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    if-nez p1, :cond_0

    .line 450
    :goto_0
    return-void

    .line 449
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/play/layout/PlayCardViewBase;->setVisibility(I)V

    goto :goto_0
.end method
