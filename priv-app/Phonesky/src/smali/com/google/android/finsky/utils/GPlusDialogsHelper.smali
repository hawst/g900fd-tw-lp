.class public Lcom/google/android/finsky/utils/GPlusDialogsHelper;
.super Ljava/lang/Object;
.source "GPlusDialogsHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/GPlusDialogsHelper$PublicReviewsDialog;,
        Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpAndPublicReviewsDialog;,
        Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;
    }
.end annotation


# direct methods
.method public static showGPlusSignUpAndPublicReviewsDialog(Landroid/support/v4/app/FragmentManager;)V
    .locals 2
    .param p0, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 123
    invoke-static {}, Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpAndPublicReviewsDialog;->newInstance()Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpAndPublicReviewsDialog;

    move-result-object v0

    .line 124
    .local v0, "dialog":Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpAndPublicReviewsDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpAndPublicReviewsDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method public static showGPlusSignUpDialog(Landroid/support/v4/app/FragmentManager;)V
    .locals 2
    .param p0, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 112
    invoke-static {}, Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;->newInstance()Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;

    move-result-object v0

    .line 113
    .local v0, "dialog":Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 114
    return-void
.end method

.method public static showPublicReviewsDialog(Landroid/support/v4/app/FragmentManager;)Z
    .locals 4
    .param p0, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 133
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->acceptedPlusReviews:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 135
    .local v0, "acceptedPlusReviews":Z
    if-nez v0, :cond_0

    .line 136
    invoke-static {}, Lcom/google/android/finsky/utils/GPlusDialogsHelper$PublicReviewsDialog;->newInstance()Lcom/google/android/finsky/utils/GPlusDialogsHelper$PublicReviewsDialog;

    move-result-object v1

    .line 137
    .local v1, "dialog":Lcom/google/android/finsky/utils/GPlusDialogsHelper$PublicReviewsDialog;
    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/google/android/finsky/utils/GPlusDialogsHelper$PublicReviewsDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 138
    const/4 v2, 0x1

    .line 140
    .end local v1    # "dialog":Lcom/google/android/finsky/utils/GPlusDialogsHelper$PublicReviewsDialog;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
