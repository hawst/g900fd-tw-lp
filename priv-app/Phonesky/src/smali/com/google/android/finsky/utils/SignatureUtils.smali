.class public Lcom/google/android/finsky/utils/SignatureUtils;
.super Ljava/lang/Object;
.source "SignatureUtils.java"


# static fields
.field private static sOtherSignatures:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/Signature;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static containsFirstPartyPackage(Landroid/content/Context;[Ljava/lang/String;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageNames"    # [Ljava/lang/String;

    .prologue
    .line 147
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 148
    .local v3, "packageName":Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/google/android/finsky/utils/SignatureUtils;->isFirstPartyPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 149
    const/4 v4, 0x1

    .line 152
    .end local v3    # "packageName":Ljava/lang/String;
    :goto_1
    return v4

    .line 147
    .restart local v3    # "packageName":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 152
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static declared-synchronized faultInOtherSignatures(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 189
    const-class v1, Lcom/google/android/finsky/utils/SignatureUtils;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/finsky/utils/SignatureUtils;->sOtherSignatures:Ljava/util/List;

    if-nez v0, :cond_0

    .line 190
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/SignatureUtils;->sOtherSignatures:Ljava/util/List;

    .line 193
    sget-object v0, Lcom/google/android/finsky/utils/SignatureUtils;->sOtherSignatures:Ljava/util/List;

    const-string v2, "MIICUjCCAbsCBEk0mH4wDQYJKoZIhvcNAQEEBQAwcDELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtHb29nbGUsIEluYzEUMBIGA1UECxMLR29vZ2xlLCBJbmMxEDAOBgNVBAMTB1Vua25vd24wHhcNMDgxMjAyMDIwNzU4WhcNMzYwNDE5MDIwNzU4WjBwMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC0dvb2dsZSwgSW5jMRQwEgYDVQQLEwtHb29nbGUsIEluYzEQMA4GA1UEAxMHVW5rbm93bjCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEAn0gDGZD5sUcmOE4EU9GPjAu/jcd7JQSksSB8TGxEurwArcZhD6a2qy2oDjPy7vFrJqP2uFua+sqQn/u+s/TJT36BIqeY4OunXO090in6c2X0FRZBWqnBYX3Vg84Zuuigu9iF/BeptL0mQIBRIarbk3fetAATOBQYiC7FIoL8WA0CAwEAATANBgkqhkiG9w0BAQQFAAOBgQBAhmae1jHaQ4Td0GHSJuBzuYzEuZ34teS+njy+l1Aeg98cb6lZwM5gXE/SrG0chM7eIEdsurGb6PIgOv93F61lLY/MiQcI0SFtqERXWSZJ4OnTxLtM9Y2hnbHU/EG8uVhPZOZfQQ0FKf1baIOMFB0Km9HbEZHLKg33kOoMsS2zpA=="

    invoke-static {v2}, Lcom/google/android/finsky/utils/SignatureUtils;->makeSignature(Ljava/lang/String;)Landroid/content/pm/Signature;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    invoke-static {p0}, Lcom/google/android/finsky/utils/SignatureUtils;->isFinskyWithDebugKeys(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    const-string v0, "Apps signed by first-party test keys will be allowed"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    sget-object v0, Lcom/google/android/finsky/utils/SignatureUtils;->sOtherSignatures:Ljava/util/List;

    const-string v2, "MIIEqDCCA5CgAwIBAgIJAIR+T/LWtd6OMA0GCSqGSIb3DQEBBQUAMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTAeFw0xMDAxMjAwMTAxMzVaFw0zNzA2MDcwMTAxMzVaMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTCCASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBANgocXw20RcP1E0Kew8HESboW7/fM7A0YABalMz7ZaXboLJD32Cxkb+dBt8dilwKM+LRY/UT3x0iU0HqPDN5IuhcAuw0ztlMuAcjpiP/S6/7tOXv5nc7PqK+uLyyAmfP54VRH4Mu+YerdZT+HinPvE0IOh8SUgB3c5byFltpewCjoME6zDCKk/IhY8FunD1KshSfNkxFwEMUMnA58doJYJPxs/wYtlYQlcYiX8cQK5h8bxOkXSTj4MVOhZ1n41tnCCcT0tbwV900V9GfxP6N3eyMOk8/lyMFGacKKDY0rDWBo0q9oX2EWgoJhfv4BgsDaid4YIFj+gw3uefyoQ52vHcCAQOjgfwwgfkwHQYDVR0OBBYEFLXH+RJveA06+8plc3M/9SJrmxc3MIHJBgNVHSMEgcEwgb6AFLXH+RJveA06+8plc3M/9SJrmxc3oYGapIGXMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbYIJAIR+T/LWtd6OMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAEw+p2V9Jua71xEMjxnfH42hCX0zhg9p3r/K20ajfoflsw+7NHscdVW8uzyZVBSARpZfnHkqAtDb5aZHYbN5R6tr/7C6xqLBoM34Yvh3qWcN/W8GLkBuzhgGDGBJjfw2nycRcZjlb8uhUuYFjc6UzlkfxPSpmCszutgZbXdvVbfQGs8x3dcM7LeJeHYGZRD5SaVSSjExs8tlQc+LNUIOvMRSJVmWP0JmaQVyZmJPs5jP21IXiB0RHG4DRhb4USEY0KKmnRPXkvDNEdvVjiODWlSlSsJR59IsRGo/7hQSEOlER0tAYwe7JoQrT2vTVYIcc5ZR/6JgWwXiJJXXFdh6kfY="

    invoke-static {v2}, Lcom/google/android/finsky/utils/SignatureUtils;->makeSignature(Ljava/lang/String;)Landroid/content/pm/Signature;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 200
    :cond_1
    :try_start_1
    const-string v0, "Will not allow first-party apps signed by test keys"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 189
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getCallingAppIfAuthorized(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/play/utils/config/GservicesValue;Lcom/google/android/finsky/analytics/FinskyEventLog;I)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "docId"    # Ljava/lang/String;
    .param p3, "eventLog"    # Lcom/google/android/finsky/analytics/FinskyEventLog;
    .param p4, "eventType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/finsky/analytics/FinskyEventLog;",
            "I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 242
    .local p2, "guard":Lcom/google/android/play/utils/config/GservicesValue;, "Lcom/google/android/play/utils/config/GservicesValue<Ljava/lang/Boolean;>;"
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v7

    .line 243
    .local v7, "callingUid":I
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 244
    .local v9, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v9, v7}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    .line 245
    .local v6, "callingPackages":[Ljava/lang/String;
    if-eqz v6, :cond_0

    array-length v0, v6

    if-nez v0, :cond_2

    .line 246
    :cond_0
    const-string v0, "getPackagesForUid %d returned 0 packages"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    const-string v3, "no-packages-for-uid"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p3

    move v1, p4

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const/4 v4, 0x0

    .line 270
    :cond_1
    :goto_0
    return-object v4

    .line 251
    :cond_2
    array-length v0, v6

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 252
    const/4 v0, 0x0

    aget-object v4, v6, v0

    .line 264
    .local v4, "callerName":Ljava/lang/String;
    :goto_1
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    invoke-static {p0, v6}, Lcom/google/android/finsky/utils/SignatureUtils;->containsFirstPartyPackage(Landroid/content/Context;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 268
    const-string v0, "Unable to authorize API access for %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v4, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 269
    const-string v3, "auth-rejection"

    const/4 v5, 0x0

    move-object v0, p3

    move v1, p4

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/4 v4, 0x0

    goto :goto_0

    .line 255
    .end local v4    # "callerName":Ljava/lang/String;
    :cond_4
    invoke-virtual {v9, v7}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v8

    .line 256
    .local v8, "nameForUid":Ljava/lang/String;
    if-nez v8, :cond_5

    .line 257
    const-string v0, "getNameForUid %d returned null"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 258
    const-string v3, "no-name-for-uid"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p3

    move v1, p4

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    const/4 v4, 0x0

    goto :goto_0

    .line 261
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sharedUserId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "callerName":Ljava/lang/String;
    goto :goto_1
.end method

.method public static isCalledByFirstPartyPackage(Landroid/app/Activity;)Z
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 124
    invoke-virtual {p0}, Landroid/app/Activity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    .line 125
    .local v0, "callingPackage":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 126
    const-string v2, "Couldn\'t determine caller, did you use startActivityForResult?"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    :goto_0
    return v1

    .line 130
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    const/4 v1, 0x1

    goto :goto_0

    .line 133
    :cond_1
    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/SignatureUtils;->isFirstPartyPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static isFinskyWithDebugKeys(Landroid/content/Context;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x0

    .line 208
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0x40

    invoke-virtual {v9, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    .line 210
    .local v5, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v6, v5, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    .local v6, "phoneskySignatures":[Landroid/content/pm/Signature;
    const-string v9, "MIIEqDCCA5CgAwIBAgIJANWFuGx90071MA0GCSqGSIb3DQEBBAUAMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTAeFw0wODA0MTUyMzM2NTZaFw0zNTA5MDEyMzM2NTZaMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTCCASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBANbOLggKv+IxTdGNs8/TGFy0PTP6DHThvbbR24kT9ixcOd9W+EaBPWW+wPPKQmsHxajtWjmQwWfna8mZuSeJS48LIgAZlKkpFeVyxW0qMBujb8X8ETrWy550NaFtI6t9+u7hZeTfHwqNvacKhp1RbE6dBRGWynwMVX8XW8N1+UjFaq6GCJukT4qmpN2afb8sCjUigq0GuMwYXrFVee74bQgLHWGJwPmvmLHC69EH6kWr22ijx4OKXlSIx2xT1AsSHee70w5iDBiK4aph27yH3TxkXy9V89TDdexAcKk/cVHYNnDBapcavl7y0RiQ4biu8ymM8Ga/nmzhRKya6G0cGw8CAQOjgfwwgfkwHQYDVR0OBBYEFI0cxb6VTEM8YYY6FbBMvAPyT+CyMIHJBgNVHSMEgcEwgb6AFI0cxb6VTEM8YYY6FbBMvAPyT+CyoYGapIGXMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbYIJANWFuGx90071MAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEEBQADggEBABnTDPEF+3iSP0wNfdIjIz1AlnrPzgAIHVvXxunW7SBrDhEglQZBbKJEk5kT0mtKoOD1JMrSu1xuTKEBahWRbqHsXclaXjoBADb0kkjVEJu/Lh5hgYZnOjvlba8Ld7HCKePCVePoTJBdI4fvugnL8TsgK05aIskyY0hKI9L8KfqfGTl1lzOv2KoWD0KWwtAWPoGChZxmQ+nBli+gwYMzM1vAkP+aayLe0a1EQimlOalO762r0GXO0ks+UeXde2Z4e+8S/pf7pITEI/tP+MxJTALw9QUWEv9lKTk+jkbqxbsh8nfBUapfKqYn0eidpwq2AzVp3juYl7//fKnaPhJD9gs="

    invoke-static {v9}, Lcom/google/android/finsky/utils/SignatureUtils;->makeSignature(Ljava/lang/String;)Landroid/content/pm/Signature;

    move-result-object v2

    .line 216
    .local v2, "finskyDebugSignature":Landroid/content/pm/Signature;
    move-object v0, v6

    .local v0, "arr$":[Landroid/content/pm/Signature;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v7, v0, v3

    .line 217
    .local v7, "signature":Landroid/content/pm/Signature;
    invoke-virtual {v2, v7}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 218
    const/4 v8, 0x1

    .line 221
    .end local v0    # "arr$":[Landroid/content/pm/Signature;
    .end local v2    # "finskyDebugSignature":Landroid/content/pm/Signature;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v6    # "phoneskySignatures":[Landroid/content/pm/Signature;
    .end local v7    # "signature":Landroid/content/pm/Signature;
    :cond_0
    :goto_1
    return v8

    .line 211
    :catch_0
    move-exception v1

    .line 212
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v9, "Unable to look up our own PackageInfo"

    new-array v10, v8, [Ljava/lang/Object;

    invoke-static {v1, v9, v10}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 216
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "arr$":[Landroid/content/pm/Signature;
    .restart local v2    # "finskyDebugSignature":Landroid/content/pm/Signature;
    .restart local v3    # "i$":I
    .restart local v4    # "len$":I
    .restart local v5    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v6    # "phoneskySignatures":[Landroid/content/pm/Signature;
    .restart local v7    # "signature":Landroid/content/pm/Signature;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public static isFirstPartyPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 156
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 159
    .local v7, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, p1, v11}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 160
    .local v8, "result":I
    if-nez v8, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v9

    .line 165
    :cond_1
    invoke-static {p0}, Lcom/google/android/finsky/utils/SignatureUtils;->faultInOtherSignatures(Landroid/content/Context;)V

    .line 167
    const/16 v11, 0x40

    :try_start_0
    invoke-virtual {v7, p1, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 171
    .local v6, "pinfo":Landroid/content/pm/PackageInfo;
    iget-object v0, v6, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .local v0, "arr$":[Landroid/content/pm/Signature;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v5, v0, v3

    .line 172
    .local v5, "packageSignature":Landroid/content/pm/Signature;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    sget-object v11, Lcom/google/android/finsky/utils/SignatureUtils;->sOtherSignatures:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v2, v11, :cond_2

    .line 173
    sget-object v11, Lcom/google/android/finsky/utils/SignatureUtils;->sOtherSignatures:Ljava/util/List;

    invoke-interface {v11, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    invoke-virtual {v5, v11}, Landroid/content/pm/Signature;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v11

    if-nez v11, :cond_0

    .line 172
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 171
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 178
    .end local v0    # "arr$":[Landroid/content/pm/Signature;
    .end local v2    # "i":I
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "packageSignature":Landroid/content/pm/Signature;
    .end local v6    # "pinfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v1

    .line 179
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v11, "Couldn\'t get info for package %s"

    new-array v9, v9, [Ljava/lang/Object;

    aput-object p1, v9, v10

    invoke-static {v11, v9}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_3
    move v9, v10

    .line 181
    goto :goto_0
.end method

.method public static logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "eventLog"    # Lcom/google/android/finsky/analytics/FinskyEventLog;
    .param p1, "eventType"    # I
    .param p2, "document"    # Ljava/lang/String;
    .param p3, "reason"    # Ljava/lang/String;
    .param p4, "callingPackage"    # Ljava/lang/String;
    .param p5, "exceptionType"    # Ljava/lang/String;

    .prologue
    .line 275
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setDocument(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setReason(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v2

    if-nez p3, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setOperationSuccess(Z)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setCallingPackage(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setExceptionType(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 281
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 282
    return-void

    .line 275
    .end local v0    # "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static makeSignature(Ljava/lang/String;)Landroid/content/pm/Signature;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 225
    const/4 v1, 0x0

    invoke-static {p0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 226
    .local v0, "bytes":[B
    new-instance v1, Landroid/content/pm/Signature;

    invoke-direct {v1, v0}, Landroid/content/pm/Signature;-><init>([B)V

    return-object v1
.end method
