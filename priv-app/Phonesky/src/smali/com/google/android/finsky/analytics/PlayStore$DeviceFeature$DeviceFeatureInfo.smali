.class public final Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceFeatureInfo"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;


# instance fields
.field public featureName:Ljava/lang/String;

.field public hasFeatureName:Z

.field public hasLastConnectionTimeMs:Z

.field public lastConnectionTimeMs:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3043
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3044
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->clear()Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    .line 3045
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    .locals 2

    .prologue
    .line 3024
    sget-object v0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->_emptyArray:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    if-nez v0, :cond_1

    .line 3025
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 3027
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->_emptyArray:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    if-nez v0, :cond_0

    .line 3028
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    sput-object v0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->_emptyArray:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    .line 3030
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3032
    :cond_1
    sget-object v0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->_emptyArray:[Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    return-object v0

    .line 3030
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3048
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->featureName:Ljava/lang/String;

    .line 3049
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->hasFeatureName:Z

    .line 3050
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->lastConnectionTimeMs:J

    .line 3051
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->hasLastConnectionTimeMs:Z

    .line 3052
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->cachedSize:I

    .line 3053
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 3070
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3071
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->hasFeatureName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->featureName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3072
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->featureName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3075
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->hasLastConnectionTimeMs:Z

    if-nez v1, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->lastConnectionTimeMs:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 3076
    :cond_2
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->lastConnectionTimeMs:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3079
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 3087
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3088
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3092
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3093
    :sswitch_0
    return-object p0

    .line 3098
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->featureName:Ljava/lang/String;

    .line 3099
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->hasFeatureName:Z

    goto :goto_0

    .line 3103
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->lastConnectionTimeMs:J

    .line 3104
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->hasLastConnectionTimeMs:Z

    goto :goto_0

    .line 3088
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3018
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3059
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->hasFeatureName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->featureName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3060
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->featureName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3062
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->hasLastConnectionTimeMs:Z

    if-nez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->lastConnectionTimeMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 3063
    :cond_2
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$DeviceFeature$DeviceFeatureInfo;->lastConnectionTimeMs:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 3065
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3066
    return-void
.end method
