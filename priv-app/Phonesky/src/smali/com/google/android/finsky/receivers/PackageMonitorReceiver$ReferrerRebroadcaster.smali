.class Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;
.super Ljava/lang/Object;
.source "PackageMonitorReceiver.java"

# interfaces
.implements Lcom/google/android/finsky/receivers/PackageMonitorReceiver$PackageStatusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/receivers/PackageMonitorReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ReferrerRebroadcaster"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/receivers/PackageMonitorReceiver$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/receivers/PackageMonitorReceiver$1;

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 214
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;->doBroadcastInstallReferrer(Ljava/lang/String;Z)V

    return-void
.end method

.method private broadcastInstallReferrer(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "packageAdded"    # Z

    .prologue
    .line 251
    new-instance v0, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster$1;-><init>(Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;Ljava/lang/String;Z)V

    .line 261
    .local v0, "continueRunnable":Ljava/lang/Runnable;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/Libraries;->load(Ljava/lang/Runnable;)V

    .line 262
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    .line 263
    return-void
.end method

.method private doBroadcastInstallReferrer(Ljava/lang/String;Z)V
    .locals 28
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "packageAdded"    # Z

    .prologue
    .line 266
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v7

    .line 267
    .local v7, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    move-object/from16 v0, p1

    invoke-virtual {v7, v0}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v6

    .line 268
    .local v6, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v6, :cond_0

    iget-object v0, v6, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-object/from16 v24, v0

    if-nez v24, :cond_1

    .line 384
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    iget-object v15, v6, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 273
    .local v15, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v15}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v24

    if-eqz v24, :cond_7

    invoke-virtual {v15}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getDeliveryData()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v24

    move-object/from16 v0, v24

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->immediateStartNeeded:Z

    move/from16 v24, v0

    if-eqz v24, :cond_7

    const/4 v11, 0x1

    .line 276
    .local v11, "forceLaunch":Z
    :goto_1
    sget v24, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v25, 0xc

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_8

    const/16 v23, 0x1

    .line 285
    .local v23, "usesFrozenApps":Z
    :goto_2
    if-nez v11, :cond_2

    move/from16 v0, v23

    move/from16 v1, p2

    if-eq v0, v1, :cond_9

    :cond_2
    const/16 v21, 0x1

    .line 286
    .local v21, "sendIntent":Z
    :goto_3
    if-eqz v21, :cond_0

    .line 295
    if-eqz v11, :cond_a

    .line 298
    const-string v10, "forced-launch"

    .line 323
    .local v10, "externalReferrer":Ljava/lang/String;
    :cond_3
    :goto_4
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_4

    .line 324
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;)Ljava/util/List;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v24

    if-gtz v24, :cond_4

    .line 325
    const/4 v10, 0x0

    .line 326
    const-string v24, "Dropped referrer for %s because not-owned"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object p1, v25, v26

    invoke-static/range {v24 .. v25}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 327
    const/16 v24, 0x204

    iget-object v0, v6, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    move/from16 v25, v0

    const-string v26, "not-owned"

    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, p1

    move/from16 v3, v25

    move-object/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;->logExternalReferrer(ILjava/lang/String;ILjava/lang/String;)V

    .line 338
    :cond_4
    invoke-virtual {v7}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v16

    .line 339
    .local v16, "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    const/16 v24, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setExternalReferrer(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-wide/16 v24, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    move-wide/from16 v2, v24

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setExternalReferrerTimestampMs(Ljava/lang/String;J)V

    .line 343
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_0

    .line 344
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    .line 345
    .local v9, "context":Landroid/content/Context;
    new-instance v18, Landroid/content/Intent;

    const-string v24, "com.android.vending.INSTALL_REFERRER"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 346
    .local v18, "referrerIntent":Landroid/content/Intent;
    if-eqz v11, :cond_5

    sget v24, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v25, 0xd

    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_5

    .line 347
    const/16 v24, 0x20

    move-object/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 348
    const-string v24, "Forcing %s to wake up"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object p1, v25, v26

    invoke-static/range {v24 .. v25}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    :cond_5
    const-string v24, "referrer"

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    invoke-virtual {v9}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v17

    .line 355
    .local v17, "pm":Landroid/content/pm/PackageManager;
    const/16 v24, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v20

    .line 356
    .local v20, "resolvedReceivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v24

    if-lez v24, :cond_0

    .line 357
    const/16 v22, 0x0

    .line 358
    .local v22, "sent":Z
    # getter for: Lcom/google/android/finsky/receivers/PackageMonitorReceiver;->ICE_CREAM_SANDWICH_OR_GREATER:Z
    invoke-static {}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver;->access$200()Z

    move-result v24

    if-eqz v24, :cond_b

    .line 359
    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 360
    const/16 v22, 0x1

    .line 374
    :cond_6
    if-eqz v22, :cond_d

    .line 375
    const-string v24, "Delivered referrer for %s"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object p1, v25, v26

    invoke-static/range {v24 .. v25}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 376
    const/16 v24, 0x205

    iget-object v0, v6, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    move/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, p1

    move/from16 v3, v25

    move-object/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;->logExternalReferrer(ILjava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 273
    .end local v9    # "context":Landroid/content/Context;
    .end local v10    # "externalReferrer":Ljava/lang/String;
    .end local v11    # "forceLaunch":Z
    .end local v16    # "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    .end local v17    # "pm":Landroid/content/pm/PackageManager;
    .end local v18    # "referrerIntent":Landroid/content/Intent;
    .end local v20    # "resolvedReceivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v21    # "sendIntent":Z
    .end local v22    # "sent":Z
    .end local v23    # "usesFrozenApps":Z
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 276
    .restart local v11    # "forceLaunch":Z
    :cond_8
    const/16 v23, 0x0

    goto/16 :goto_2

    .line 285
    .restart local v23    # "usesFrozenApps":Z
    :cond_9
    const/16 v21, 0x0

    goto/16 :goto_3

    .line 302
    .restart local v21    # "sendIntent":Z
    :cond_a
    invoke-virtual {v15}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getExternalReferrer()Ljava/lang/String;

    move-result-object v10

    .line 303
    .restart local v10    # "externalReferrer":Ljava/lang/String;
    invoke-virtual {v15}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getExternalReferrerTimestampMs()J

    move-result-wide v12

    .line 304
    .local v12, "externalReferrerTimestampMs":J
    const-wide/16 v24, 0x0

    cmp-long v24, v12, v24

    if-lez v24, :cond_3

    .line 305
    sget-object v24, Lcom/google/android/finsky/config/G;->externalReferrerLifespanMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/Long;

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    add-long v24, v24, v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    cmp-long v24, v24, v26

    if-gez v24, :cond_3

    .line 308
    const/4 v10, 0x0

    .line 309
    const-string v24, "Dropped referrer for %s because expired"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object p1, v25, v26

    invoke-static/range {v24 .. v25}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    const/16 v24, 0x204

    iget-object v0, v6, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    move/from16 v25, v0

    const-string v26, "expired"

    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, p1

    move/from16 v3, v25

    move-object/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;->logExternalReferrer(ILjava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_4

    .line 364
    .end local v12    # "externalReferrerTimestampMs":J
    .restart local v9    # "context":Landroid/content/Context;
    .restart local v16    # "installerDataStore":Lcom/google/android/finsky/appstate/InstallerDataStore;
    .restart local v17    # "pm":Landroid/content/pm/PackageManager;
    .restart local v18    # "referrerIntent":Landroid/content/Intent;
    .restart local v20    # "resolvedReceivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v22    # "sent":Z
    :cond_b
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_c
    :goto_5
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_6

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/content/pm/ResolveInfo;

    .line 365
    .local v19, "resolvedReceiver":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 366
    new-instance v8, Landroid/content/Intent;

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 367
    .local v8, "broadcastIntent":Landroid/content/Intent;
    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 369
    invoke-virtual {v9, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 370
    const/16 v22, 0x1

    goto :goto_5

    .line 380
    .end local v8    # "broadcastIntent":Landroid/content/Intent;
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v19    # "resolvedReceiver":Landroid/content/pm/ResolveInfo;
    :cond_d
    const-string v24, "Couldn\'t find referrer receiver for %s"

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object p1, v25, v26

    invoke-static/range {v24 .. v25}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0
.end method

.method private logExternalReferrer(ILjava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "backgroundEventType"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "installedVersion"    # I
    .param p4, "reason"    # Ljava/lang/String;

    .prologue
    .line 391
    new-instance v2, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v2, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v2, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setDocument(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setReason(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    .line 394
    .local v1, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    if-ltz p3, :cond_0

    .line 395
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    .line 396
    .local v0, "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    iput p3, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 397
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 398
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setAppData(Lcom/google/android/finsky/analytics/PlayStore$AppData;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    .line 400
    .end local v0    # "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 401
    return-void
.end method


# virtual methods
.method public onPackageAdded(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 218
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;->broadcastInstallReferrer(Ljava/lang/String;Z)V

    .line 219
    return-void
.end method

.method public onPackageAvailabilityChanged([Ljava/lang/String;Z)V
    .locals 0
    .param p1, "packageNames"    # [Ljava/lang/String;
    .param p2, "available"    # Z

    .prologue
    .line 228
    return-void
.end method

.method public onPackageChanged(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 225
    return-void
.end method

.method public onPackageFirstLaunch(Ljava/lang/String;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 232
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/receivers/PackageMonitorReceiver$ReferrerRebroadcaster;->broadcastInstallReferrer(Ljava/lang/String;Z)V

    .line 233
    return-void
.end method

.method public onPackageRemoved(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "replacing"    # Z

    .prologue
    .line 222
    return-void
.end method
