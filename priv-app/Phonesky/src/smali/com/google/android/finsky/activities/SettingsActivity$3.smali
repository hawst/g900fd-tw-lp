.class Lcom/google/android/finsky/activities/SettingsActivity$3;
.super Landroid/os/AsyncTask;
.source "SettingsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/SettingsActivity;->getReauthSettingsOverNetwork(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/SettingsActivity;

.field final synthetic val$newPurchaseAuth:I

.field final synthetic val$previousPurchaseAuth:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/SettingsActivity;II)V
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Lcom/google/android/finsky/activities/SettingsActivity$3;->this$0:Lcom/google/android/finsky/activities/SettingsActivity;

    iput p2, p0, Lcom/google/android/finsky/activities/SettingsActivity$3;->val$previousPurchaseAuth:I

    iput p3, p0, Lcom/google/android/finsky/activities/SettingsActivity$3;->val$newPurchaseAuth:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 518
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    .line 519
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;-><init>(Landroid/content/Context;)V

    .line 521
    .local v0, "authClient":Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataClient;
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;

    iget-object v3, p0, Lcom/google/android/finsky/activities/SettingsActivity$3;->this$0:Lcom/google/android/finsky/activities/SettingsActivity;

    # getter for: Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/finsky/activities/SettingsActivity;->access$300(Lcom/google/android/finsky/activities/SettingsActivity;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataClient;->getReauthSettings(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 515
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/SettingsActivity$3;->doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity$3;->this$0:Lcom/google/android/finsky/activities/SettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SettingsActivity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 532
    :goto_0
    return-void

    .line 530
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity$3;->this$0:Lcom/google/android/finsky/activities/SettingsActivity;

    iget v1, p0, Lcom/google/android/finsky/activities/SettingsActivity$3;->val$previousPurchaseAuth:I

    iget v2, p0, Lcom/google/android/finsky/activities/SettingsActivity$3;->val$newPurchaseAuth:I

    # invokes: Lcom/google/android/finsky/activities/SettingsActivity;->handleReauthSettingsResponse(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;II)V
    invoke-static {v0, p1, v1, v2}, Lcom/google/android/finsky/activities/SettingsActivity;->access$400(Lcom/google/android/finsky/activities/SettingsActivity;Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;II)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 515
    check-cast p1, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/SettingsActivity$3;->onPostExecute(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;)V

    return-void
.end method
