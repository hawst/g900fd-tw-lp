.class public Lcom/google/android/finsky/utils/NotificationManager;
.super Ljava/lang/Object;
.source "NotificationManager.java"

# interfaces
.implements Lcom/google/android/finsky/utils/Notifier;


# static fields
.field private static final SUPPORTS_RICH_NOTIFICATIONS:Z

.field private static final UNKNOWN_PACKAGE_ID:I


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mListener:Lcom/google/android/finsky/utils/NotificationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    const-string v0, "unknown package"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, Lcom/google/android/finsky/utils/NotificationManager;->UNKNOWN_PACKAGE_ID:I

    .line 61
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/utils/NotificationManager;->SUPPORTS_RICH_NOTIFICATIONS:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    .line 72
    return-void
.end method

.method public static createDefaultClickIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "dialogTitle"    # Ljava/lang/String;
    .param p3, "dialogHtmlMessage"    # Ljava/lang/String;
    .param p4, "detailsUrl"    # Ljava/lang/String;

    .prologue
    .line 808
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 809
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 810
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 811
    invoke-static {p0, p4}, Lcom/google/android/finsky/utils/IntentUtils;->createViewDocumentUrlIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 812
    const-string v1, "error_doc_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 814
    :cond_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 815
    const-string v1, "error_title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 817
    :cond_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 818
    const-string v1, "error_html_message"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 820
    :cond_2
    return-object v0
.end method

.method private declared-synchronized drawableToBitmap(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 14
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p2, "loggingId"    # Ljava/lang/String;

    .prologue
    .line 344
    monitor-enter p0

    :try_start_0
    iget-object v10, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 345
    .local v6, "res":Landroid/content/res/Resources;
    const v10, 0x1050005

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 346
    .local v2, "maxIconWidth":I
    const v10, 0x1050006

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 348
    .local v1, "maxIconHeight":I
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    .line 349
    .local v4, "originalWidth":I
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 351
    .local v3, "originalHeight":I
    if-gt v4, v2, :cond_0

    if-gt v3, v1, :cond_0

    instance-of v10, p1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v10, :cond_0

    .line 353
    check-cast p1, Landroid/graphics/drawable/BitmapDrawable;

    .end local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 373
    :goto_0
    monitor-exit p0

    return-object v7

    .line 356
    .restart local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    :try_start_1
    const-string v10, "Resource for %s is %s of size %d*%d"

    const/4 v11, 0x4

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object p2, v11, v12

    const/4 v12, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 361
    int-to-float v10, v2

    int-to-float v11, v4

    div-float/2addr v10, v11

    int-to-float v11, v1

    int-to-float v12, v3

    div-float/2addr v11, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 363
    .local v5, "ratio":F
    const/high16 v10, 0x3f800000    # 1.0f

    invoke-static {v10, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 364
    int-to-float v10, v4

    mul-float/2addr v10, v5

    float-to-int v9, v10

    .line 365
    .local v9, "scaledWidth":I
    int-to-float v10, v3

    mul-float/2addr v10, v5

    float-to-int v8, v10

    .line 366
    .local v8, "scaledHeight":I
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v8, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 369
    .local v7, "result":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v7}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 370
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11, v9, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 371
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 344
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v1    # "maxIconHeight":I
    .end local v2    # "maxIconWidth":I
    .end local v3    # "originalHeight":I
    .end local v4    # "originalWidth":I
    .end local v5    # "ratio":F
    .end local v6    # "res":Landroid/content/res/Resources;
    .end local v7    # "result":Landroid/graphics/Bitmap;
    .end local v8    # "scaledHeight":I
    .end local v9    # "scaledWidth":I
    .end local p1    # "drawable":Landroid/graphics/drawable/Drawable;
    :catchall_0
    move-exception v10

    monitor-exit p0

    throw v10
.end method

.method private static logNotification(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 6
    .param p0, "notificationShown"    # Z
    .param p1, "notificationId"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "clickIntent"    # Landroid/content/Intent;

    .prologue
    .line 493
    sget-object v2, Lcom/google/android/finsky/config/G;->debugOptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 501
    :goto_0
    return-void

    .line 496
    :cond_0
    const-string v2, "error_return_code"

    const/4 v3, -0x1

    invoke-virtual {p4, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 498
    .local v0, "returnCode":I
    if-eqz p0, :cond_1

    const-string v1, "Showing"

    .line 499
    .local v1, "verb":Ljava/lang/String;
    :goto_1
    const-string v2, "%s notification: [ID=%s, Title=%s, Message=%s, returnCode=%d]"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    const/4 v4, 0x2

    aput-object p2, v3, v4

    const/4 v4, 0x3

    aput-object p3, v3, v4

    const/4 v4, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 498
    .end local v1    # "verb":Ljava/lang/String;
    :cond_1
    const-string v1, "Suppressing"

    goto :goto_1
.end method

.method private logNotificationImpression(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 802
    new-instance v0, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-direct {v0, p1, v1, v1, v1}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 803
    .local v0, "node":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 804
    return-void
.end method

.method private showAppErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "statusBarText"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "icon"    # I

    .prologue
    .line 401
    const-string v6, "err"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/utils/NotificationManager;->showAppMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 403
    return-void
.end method

.method private showAppMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "statusBarText"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "icon"    # I
    .param p6, "category"    # Ljava/lang/String;

    .prologue
    .line 407
    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/NotificationManager;->showAppNotificationOrAlert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 409
    return-void
.end method

.method private showAppNotificationOnly(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "statusBarText"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "icon"    # I
    .param p6, "returnCode"    # I
    .param p7, "category"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 427
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mListener:Lcom/google/android/finsky/utils/NotificationListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mListener:Lcom/google/android/finsky/utils/NotificationListener;

    invoke-interface {v0, p1, p3, p4}, Lcom/google/android/finsky/utils/NotificationListener;->showAppNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 429
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v2, v2, v1}, Lcom/google/android/finsky/utils/NotificationManager;->createDefaultClickIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 432
    .local v6, "detailsIntent":Landroid/content/Intent;
    const-string v0, "error_return_code"

    invoke-virtual {v6, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v7, p7

    .line 433
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/utils/NotificationManager;->showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 436
    .end local v6    # "detailsIntent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method private showAppNotificationOrAlert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "statusBarText"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "icon"    # I
    .param p6, "returnCode"    # I
    .param p7, "category"    # Ljava/lang/String;

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mListener:Lcom/google/android/finsky/utils/NotificationListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mListener:Lcom/google/android/finsky/utils/NotificationListener;

    invoke-interface {v0, p1, p3, p4, p6}, Lcom/google/android/finsky/utils/NotificationListener;->showAppAlert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 416
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, p3, p4, v1}, Lcom/google/android/finsky/utils/NotificationManager;->createDefaultClickIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 419
    .local v6, "detailsIntent":Landroid/content/Intent;
    const-string v0, "error_return_code"

    invoke-virtual {v6, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v7, p7

    .line 420
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/utils/NotificationManager;->showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 423
    .end local v6    # "detailsIntent":Landroid/content/Intent;
    :cond_1
    return-void
.end method

.method private showDocNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "statusBarText"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "message"    # Ljava/lang/String;
    .param p5, "icon"    # I
    .param p6, "detailsUrl"    # Ljava/lang/String;
    .param p7, "category"    # Ljava/lang/String;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mListener:Lcom/google/android/finsky/utils/NotificationListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mListener:Lcom/google/android/finsky/utils/NotificationListener;

    invoke-interface {v0, p1, p3, p4, p6}, Lcom/google/android/finsky/utils/NotificationListener;->showDocAlert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 441
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p3, p4, p6}, Lcom/google/android/finsky/utils/NotificationManager;->createDefaultClickIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/utils/NotificationManager;->showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 445
    :cond_1
    return-void
.end method

.method private showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;)V
    .locals 13
    .param p1, "notificationId"    # Ljava/lang/String;
    .param p2, "tickerText"    # Ljava/lang/String;
    .param p3, "contentTitle"    # Ljava/lang/String;
    .param p4, "contentText"    # Ljava/lang/String;
    .param p5, "extraText"    # Ljava/lang/String;
    .param p6, "icon"    # I
    .param p7, "largeBitmap"    # Landroid/graphics/Bitmap;
    .param p8, "extraCount"    # I
    .param p9, "clickIntent"    # Landroid/content/Intent;
    .param p10, "isClickIntentBroadcast"    # Z
    .param p11, "deleteIntent"    # Landroid/content/Intent;

    .prologue
    .line 686
    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v12}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;Z)V

    .line 688
    return-void
.end method

.method private showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;Z)V
    .locals 14
    .param p1, "notificationId"    # Ljava/lang/String;
    .param p2, "tickerText"    # Ljava/lang/String;
    .param p3, "contentTitle"    # Ljava/lang/String;
    .param p4, "contentText"    # Ljava/lang/String;
    .param p5, "extraText"    # Ljava/lang/String;
    .param p6, "icon"    # I
    .param p7, "largeBitmap"    # Landroid/graphics/Bitmap;
    .param p8, "extraCount"    # I
    .param p9, "clickIntent"    # Landroid/content/Intent;
    .param p10, "isClickIntentBroadcast"    # Z
    .param p11, "deleteIntent"    # Landroid/content/Intent;
    .param p12, "ongoing"    # Z

    .prologue
    .line 694
    const-string v13, "status"

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move-object/from16 v11, p11

    move/from16 v12, p12

    invoke-direct/range {v0 .. v13}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;ZLjava/lang/String;)V

    .line 698
    return-void
.end method

.method private showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;ZLjava/lang/String;)V
    .locals 9
    .param p1, "notificationId"    # Ljava/lang/String;
    .param p2, "tickerText"    # Ljava/lang/String;
    .param p3, "contentTitle"    # Ljava/lang/String;
    .param p4, "contentText"    # Ljava/lang/String;
    .param p5, "extraText"    # Ljava/lang/String;
    .param p6, "icon"    # I
    .param p7, "largeBitmap"    # Landroid/graphics/Bitmap;
    .param p8, "extraCount"    # I
    .param p9, "clickIntent"    # Landroid/content/Intent;
    .param p10, "isClickIntentBroadcast"    # Z
    .param p11, "deleteIntent"    # Landroid/content/Intent;
    .param p12, "ongoing"    # Z
    .param p13, "category"    # Ljava/lang/String;

    .prologue
    .line 705
    new-instance v7, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v8, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    invoke-virtual {v7, p3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    invoke-virtual {v7, p4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    move-object/from16 v0, p13

    invoke-virtual {v7, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/support/v4/app/NotificationCompat$Builder;->setLocalOnly(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 713
    .local v1, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 714
    new-instance v7, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v7}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>()V

    invoke-virtual {v7, p5}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->bigText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setStyle(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 716
    :cond_0
    invoke-virtual {v1, p6}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 717
    if-eqz p7, :cond_1

    .line 718
    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 720
    :cond_1
    if-lez p8, :cond_2

    .line 721
    move/from16 v0, p8

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setNumber(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 723
    :cond_2
    iget-object v7, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090060

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setColor(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 724
    const/4 v7, -0x1

    invoke-virtual {v1, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 725
    if-nez p1, :cond_5

    sget v3, Lcom/google/android/finsky/utils/NotificationManager;->UNKNOWN_PACKAGE_ID:I

    .line 729
    .local v3, "notificationCode":I
    :goto_0
    if-nez p10, :cond_6

    .line 730
    iget-object v7, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const/high16 v8, 0x50000000

    move-object/from16 v0, p9

    invoke-static {v7, v3, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 738
    .local v4, "pendingClickIntent":Landroid/app/PendingIntent;
    :goto_1
    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 739
    if-eqz p11, :cond_3

    .line 740
    iget-object v7, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const/high16 v8, 0x50000000

    move-object/from16 v0, p11

    invoke-static {v7, v3, v0, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 743
    .local v5, "pendingDeleteIntent":Landroid/app/PendingIntent;
    invoke-virtual {v1, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setDeleteIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 746
    .end local v5    # "pendingDeleteIntent":Landroid/app/PendingIntent;
    :cond_3
    if-eqz p12, :cond_7

    .line 747
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setOngoing(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 752
    :goto_2
    iget-object v7, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const-string v8, "notification"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 756
    .local v2, "mgr":Landroid/app/NotificationManager;
    invoke-static {}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->isLimitedOrSchoolEduUser()Z

    move-result v7

    if-nez v7, :cond_8

    const/4 v6, 0x1

    .line 757
    .local v6, "shouldShowNotification":Z
    :goto_3
    if-eqz v6, :cond_4

    .line 758
    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 761
    :cond_4
    move-object/from16 v0, p9

    invoke-static {v6, p1, p3, p4, v0}, Lcom/google/android/finsky/utils/NotificationManager;->logNotification(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 763
    return-void

    .line 725
    .end local v2    # "mgr":Landroid/app/NotificationManager;
    .end local v3    # "notificationCode":I
    .end local v4    # "pendingClickIntent":Landroid/app/PendingIntent;
    .end local v6    # "shouldShowNotification":Z
    :cond_5
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    goto :goto_0

    .line 734
    .restart local v3    # "notificationCode":I
    :cond_6
    iget-object v7, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const/high16 v8, 0x50000000

    move-object/from16 v0, p9

    invoke-static {v7, v3, v0, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .restart local v4    # "pendingClickIntent":Landroid/app/PendingIntent;
    goto :goto_1

    .line 749
    :cond_7
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_2

    .line 756
    .restart local v2    # "mgr":Landroid/app/NotificationManager;
    :cond_8
    const/4 v6, 0x0

    goto :goto_3
.end method

.method private showSuccessfulNewInstallMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 15
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "continueUrl"    # Ljava/lang/String;

    .prologue
    .line 221
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v2, 0x7f0c015b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 223
    .local v3, "tickerText":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v2, 0x7f0c015c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 225
    .local v5, "contentText":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 226
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v2, 0x7f0c015d

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 230
    :cond_0
    const/16 v1, 0x385

    invoke-direct {p0, v1}, Lcom/google/android/finsky/utils/NotificationManager;->logNotificationImpression(I)V

    .line 231
    invoke-static/range {p2 .. p3}, Lcom/google/android/finsky/receivers/NotificationReceiver;->getSuccessfullyInstalledClickedIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 234
    .local v10, "onClickIntent":Landroid/content/Intent;
    const/4 v8, 0x0

    .line 235
    .local v8, "largeBitmap":Landroid/graphics/Bitmap;
    sget-boolean v1, Lcom/google/android/finsky/utils/NotificationManager;->SUPPORTS_RICH_NOTIFICATIONS:Z

    if-eqz v1, :cond_1

    .line 238
    :try_start_0
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 239
    .local v14, "pm":Landroid/content/pm/PackageManager;
    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 240
    .local v13, "appIconDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v13, :cond_1

    .line 241
    move-object/from16 v0, p2

    invoke-direct {p0, v13, v0}, Lcom/google/android/finsky/utils/NotificationManager;->drawableToBitmap(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 248
    .end local v13    # "appIconDrawable":Landroid/graphics/drawable/Drawable;
    .end local v14    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    :goto_0
    const/4 v6, 0x0

    const v7, 0x7f0201d2

    const/4 v9, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    move-object v1, p0

    move-object/from16 v2, p2

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v12}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;)V

    .line 251
    return-void

    .line 243
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private showSuccessfulUpdateMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 18
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 256
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/NotificationManager;->hideAllMessagesForPackage(Ljava/lang/String;)V

    .line 258
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->successfulUpdateNotificationAppList:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 263
    .local v16, "notificationAppListString":Ljava/lang/String;
    const/16 v2, 0xa

    const/16 v3, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object p1

    .line 265
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 266
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v15

    .line 275
    .local v15, "notificationAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v15, v2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 278
    const-string v2, "\n"

    invoke-static {v2, v15}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v16

    .line 279
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->successfulUpdateNotificationAppList:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 281
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 283
    .local v14, "notificationAppCount":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v3, 0x7f0c015e

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p1, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 285
    .local v4, "tickerText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f100000

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v9

    invoke-virtual {v2, v3, v14, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 289
    .local v5, "titleText":Ljava/lang/String;
    const/4 v6, 0x0

    .line 290
    .local v6, "contentText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 291
    .local v17, "res":Landroid/content/res/Resources;
    packed-switch v14, :pswitch_data_0

    .line 319
    const v2, 0x7f0c0364

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x1

    const/4 v9, 0x1

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x2

    const/4 v9, 0x2

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x3

    const/4 v9, 0x3

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x4

    add-int/lit8 v9, v14, -0x4

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v3, v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 326
    :goto_1
    const/4 v2, 0x1

    if-gt v14, v2, :cond_0

    .line 327
    const/16 v2, 0x386

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/finsky/utils/NotificationManager;->logNotificationImpression(I)V

    .line 329
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/receivers/NotificationReceiver;->getSuccessfullyUpdatedClickedIntent()Landroid/content/Intent;

    move-result-object v11

    .line 331
    .local v11, "notificationClickedIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/google/android/finsky/receivers/NotificationReceiver;->getSuccessfullyUpdatedDeletedIntent()Landroid/content/Intent;

    move-result-object v13

    .line 334
    .local v13, "notificationDeleteIntent":Landroid/content/Intent;
    sget-boolean v2, Lcom/google/android/finsky/utils/NotificationManager;->SUPPORTS_RICH_NOTIFICATIONS:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    if-le v14, v2, :cond_2

    const v8, 0x7f0201d3

    .line 338
    .local v8, "iconResourceId":I
    :goto_2
    const-string v3, "successful update"

    const/4 v9, 0x0

    const/4 v10, -0x1

    const/4 v12, 0x1

    move-object/from16 v2, p0

    move-object v7, v6

    invoke-direct/range {v2 .. v13}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;)V

    .line 341
    .end local v8    # "iconResourceId":I
    .end local v11    # "notificationClickedIntent":Landroid/content/Intent;
    .end local v13    # "notificationDeleteIntent":Landroid/content/Intent;
    :goto_3
    return-void

    .line 269
    .end local v4    # "tickerText":Ljava/lang/String;
    .end local v5    # "titleText":Ljava/lang/String;
    .end local v6    # "contentText":Ljava/lang/String;
    .end local v14    # "notificationAppCount":I
    .end local v15    # "notificationAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v17    # "res":Landroid/content/res/Resources;
    :cond_1
    const-string v2, "\n"

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/Lists;->newArrayList([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v15

    .line 272
    .restart local v15    # "notificationAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 293
    .restart local v4    # "tickerText":Ljava/lang/String;
    .restart local v5    # "titleText":Ljava/lang/String;
    .restart local v6    # "contentText":Ljava/lang/String;
    .restart local v14    # "notificationAppCount":I
    .restart local v17    # "res":Landroid/content/res/Resources;
    :pswitch_0
    const-string v2, "App count is 0 in successful update notification"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 296
    :pswitch_1
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "contentText":Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    .line 297
    .restart local v6    # "contentText":Ljava/lang/String;
    goto :goto_1

    .line 299
    :pswitch_2
    const v2, 0x7f0c035f

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x1

    const/4 v9, 0x1

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 301
    goto :goto_1

    .line 303
    :pswitch_3
    const v2, 0x7f0c0360

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x1

    const/4 v9, 0x1

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x2

    const/4 v9, 0x2

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 306
    goto/16 :goto_1

    .line 308
    :pswitch_4
    const v2, 0x7f0c0361

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x1

    const/4 v9, 0x1

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x2

    const/4 v9, 0x2

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x3

    const/4 v9, 0x3

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 311
    goto/16 :goto_1

    .line 313
    :pswitch_5
    const v2, 0x7f0c0362

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x1

    const/4 v9, 0x1

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x2

    const/4 v9, 0x2

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x3

    const/4 v9, 0x3

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    const/4 v7, 0x4

    const/4 v9, 0x4

    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    aput-object v9, v3, v7

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 317
    goto/16 :goto_1

    .line 334
    .restart local v11    # "notificationClickedIntent":Landroid/content/Intent;
    .restart local v13    # "notificationDeleteIntent":Landroid/content/Intent;
    :cond_2
    const v8, 0x7f0201d2

    goto/16 :goto_2

    .line 291
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public hideAllMessagesForPackage(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 768
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 770
    .local v0, "mgr":Landroid/app/NotificationManager;
    if-nez p1, :cond_0

    sget v1, Lcom/google/android/finsky/utils/NotificationManager;->UNKNOWN_PACKAGE_ID:I

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 771
    return-void

    .line 770
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public hideInstallingMessage()V
    .locals 3

    .prologue
    .line 782
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 784
    .local v0, "mgr":Landroid/app/NotificationManager;
    const-string v1, "package installing"

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 785
    return-void
.end method

.method public hidePackageRemoveRequestMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 789
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 791
    .local v0, "mgr":Landroid/app/NotificationManager;
    const-string v1, "package..remove..request.."

    invoke-virtual {v1, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 792
    return-void
.end method

.method public hidePackageRemovedMessage(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 796
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 798
    .local v0, "mgr":Landroid/app/NotificationManager;
    const-string v1, "package..removed.."

    invoke-virtual {v1, p1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 799
    return-void
.end method

.method public hideUpdatesAvailableMessage()V
    .locals 3

    .prologue
    .line 775
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 777
    .local v0, "mgr":Landroid/app/NotificationManager;
    const-string v1, "updates"

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 778
    return-void
.end method

.method public setNotificationListener(Lcom/google/android/finsky/utils/NotificationListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/utils/NotificationListener;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mListener:Lcom/google/android/finsky/utils/NotificationListener;

    .line 77
    return-void
.end method

.method public showDownloadErrorMessage(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 9
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .param p4, "serverMessage"    # Ljava/lang/String;
    .param p5, "isUpdate"    # Z

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 152
    if-nez p5, :cond_1

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c014f

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 154
    .local v2, "barText":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0150

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 155
    .local v3, "titleText":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0152

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p1, v5, v6

    aput-object p4, v5, v7

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 175
    .local v4, "messageText":Ljava/lang/String;
    :goto_0
    const v5, 0x108008a

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/utils/NotificationManager;->showAppErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 177
    return-void

    .line 160
    .end local v4    # "messageText":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0151

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "messageText":Ljava/lang/String;
    goto :goto_0

    .line 164
    .end local v2    # "barText":Ljava/lang/String;
    .end local v3    # "titleText":Ljava/lang/String;
    .end local v4    # "messageText":Ljava/lang/String;
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0153

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 165
    .restart local v2    # "barText":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0154

    new-array v5, v7, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 166
    .restart local v3    # "titleText":Ljava/lang/String;
    if-eqz p4, :cond_2

    .line 167
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0156

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p1, v5, v6

    aput-object p4, v5, v7

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "messageText":Ljava/lang/String;
    goto :goto_0

    .line 171
    .end local v4    # "messageText":Ljava/lang/String;
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0155

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p1, v5, v6

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v0, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "messageText":Ljava/lang/String;
    goto :goto_0
.end method

.method public showExternalStorageFull(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 90
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0143

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0144

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0145

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const v5, 0x108008a

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/utils/NotificationManager;->showAppErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 94
    return-void
.end method

.method public showExternalStorageMissing(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0146

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0147

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0148

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const v5, 0x108008a

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/utils/NotificationManager;->showAppErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 144
    return-void
.end method

.method public showHarmfulAppRemoveRequestMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 13
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;
    .param p4, "responseToken"    # [B

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/vending/verifier/PackageWarningDialog;->NO_ID:I

    const/4 v2, 0x2

    move-object v3, p1

    move-object v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/verifier/PackageWarningDialog;->createIntent(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v9

    .line 129
    .local v9, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0373

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 131
    .local v2, "title":Ljava/lang/String;
    const-string v0, "package..remove..request.."

    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0374

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0201d5

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object v0, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v12}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;Z)V

    .line 134
    return-void
.end method

.method public showHarmfulAppRemovedMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    sget v1, Lcom/google/android/vending/verifier/PackageWarningDialog;->NO_ID:I

    const/4 v2, 0x3

    const/4 v6, 0x0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/vending/verifier/PackageWarningDialog;->createIntent(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v9

    .line 117
    .local v9, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0371

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "title":Ljava/lang/String;
    const-string v0, "package..removed.."

    invoke-virtual {v0, p2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0372

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0201d4

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v3, v2

    invoke-direct/range {v0 .. v11}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;)V

    .line 121
    return-void
.end method

.method public showInstallationFailureMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "returnCode"    # I

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/google/android/finsky/utils/NotificationManager;->hideInstallingMessage()V

    .line 380
    const v5, 0x108008a

    const-string v7, "err"

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p1

    move-object v4, p3

    move v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/NotificationManager;->showAppNotificationOrAlert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 382
    return-void
.end method

.method public showInstallingMessage(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 15
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "isUpdate"    # Z

    .prologue
    .line 190
    iget-object v2, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    if-eqz p3, :cond_0

    const v1, 0x7f0c0159

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 193
    .local v3, "tickerText":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    if-eqz p3, :cond_1

    const v1, 0x7f0c015a

    :goto_1
    invoke-virtual {v2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 196
    .local v5, "contentText":Ljava/lang/String;
    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/finsky/activities/MainActivity;->getMyDownloadsIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v10

    .line 200
    .local v10, "clickIntent":Landroid/content/Intent;
    :goto_2
    const-string v2, "package installing"

    const/4 v6, 0x0

    const v7, 0x1080081

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const-string v14, "progress"

    move-object v1, p0

    move-object/from16 v4, p1

    invoke-direct/range {v1 .. v14}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;ZLjava/lang/String;)V

    .line 203
    return-void

    .line 190
    .end local v3    # "tickerText":Ljava/lang/String;
    .end local v5    # "contentText":Ljava/lang/String;
    .end local v10    # "clickIntent":Landroid/content/Intent;
    :cond_0
    const v1, 0x7f0c0157

    goto :goto_0

    .line 193
    .restart local v3    # "tickerText":Ljava/lang/String;
    :cond_1
    const v1, 0x7f0c0158

    goto :goto_1

    .line 196
    .restart local v5    # "contentText":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static/range {p2 .. p2}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-static {v1, v0, v2, v4, v6}, Lcom/google/android/finsky/utils/NotificationManager;->createDefaultClickIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    goto :goto_2
.end method

.method public showInternalStorageFull(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 82
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0140

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0141

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0142

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const v5, 0x108008a

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/utils/NotificationManager;->showAppErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    return-void
.end method

.method public showMaliciousAssetRemovedMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 107
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0149

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v2, 0x7f0c014a

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v3, 0x7f0c014b

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/finsky/utils/NotificationManager;->showMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public showMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "shortMessage"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 449
    const v5, 0x108008a

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v1, p1, p3, v1}, Lcom/google/android/finsky/utils/NotificationManager;->createDefaultClickIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const-string v7, "status"

    move-object v0, p0

    move-object v2, p1

    move-object v3, p1

    move-object v4, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/utils/NotificationManager;->showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;Ljava/lang/String;)V

    .line 452
    return-void
.end method

.method public showNewUpdatesAvailableMessage(Ljava/util/List;I)V
    .locals 14
    .param p2, "totalUpdatesCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 505
    .local p1, "appDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 506
    .local v13, "res":Landroid/content/res/Resources;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v12

    .line 507
    .local v12, "newUpdatesCount":I
    const v0, 0x7f0c0299

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 508
    .local v2, "tickerText":Ljava/lang/String;
    const v0, 0x7f100003

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v5

    invoke-virtual {v13, v0, v12, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 512
    .local v3, "contentTitle":Ljava/lang/String;
    const/4 v4, 0x0

    .line 514
    .local v4, "contentText":Ljava/lang/String;
    packed-switch v12, :pswitch_data_0

    .line 543
    const v1, 0x7f0c0164

    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v0, 0x4

    add-int/lit8 v7, v12, -0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v0

    invoke-virtual {v13, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 550
    :goto_0
    sget-boolean v0, Lcom/google/android/finsky/utils/NotificationManager;->SUPPORTS_RICH_NOTIFICATIONS:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    if-le v12, v0, :cond_0

    const v6, 0x7f0201d7

    .line 554
    .local v6, "iconResourceId":I
    :goto_1
    const/16 v0, 0x384

    invoke-direct {p0, v0}, Lcom/google/android/finsky/utils/NotificationManager;->logNotificationImpression(I)V

    .line 555
    invoke-static {}, Lcom/google/android/finsky/receivers/NotificationReceiver;->getNewUpdateClickedIntent()Landroid/content/Intent;

    move-result-object v9

    .line 559
    .local v9, "onClickIntent":Landroid/content/Intent;
    const-string v1, "updates"

    const/4 v7, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v0, p0

    move-object v5, v4

    move/from16 v8, p2

    invoke-direct/range {v0 .. v11}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;)V

    .line 561
    .end local v6    # "iconResourceId":I
    .end local v9    # "onClickIntent":Landroid/content/Intent;
    :goto_2
    return-void

    .line 516
    :pswitch_0
    const-string v0, "App count is 0 in new updates notification"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 519
    :pswitch_1
    const v1, 0x7f0c015f

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v13, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 521
    goto :goto_0

    .line 523
    :pswitch_2
    const v1, 0x7f0c0160

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v13, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 525
    goto :goto_0

    .line 527
    :pswitch_3
    const v1, 0x7f0c0161

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v13, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 530
    goto/16 :goto_0

    .line 532
    :pswitch_4
    const v1, 0x7f0c0162

    const/4 v0, 0x4

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v13, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 535
    goto/16 :goto_0

    .line 537
    :pswitch_5
    const v1, 0x7f0c0163

    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x4

    const/4 v0, 0x4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v13, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 541
    goto/16 :goto_0

    .line 550
    :cond_0
    const v6, 0x7f0201d6

    goto/16 :goto_1

    .line 514
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public showNormalAssetRemovedMessage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c014c

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c014d

    new-array v3, v4, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const v1, 0x7f0c014e

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const v5, 0x108008a

    const-string v6, "status"

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/utils/NotificationManager;->showAppMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 102
    return-void
.end method

.method public showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/content/Intent;Ljava/lang/String;)V
    .locals 12
    .param p1, "notificationId"    # Ljava/lang/String;
    .param p2, "statusBarText"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "htmlMessage"    # Ljava/lang/String;
    .param p5, "icon"    # I
    .param p6, "clickIntent"    # Landroid/content/Intent;
    .param p7, "category"    # Ljava/lang/String;

    .prologue
    .line 459
    invoke-static/range {p4 .. p4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 460
    .local v2, "message":Ljava/lang/String;
    if-nez p1, :cond_1

    sget v5, Lcom/google/android/finsky/utils/NotificationManager;->UNKNOWN_PACKAGE_ID:I

    .line 462
    .local v5, "notificationCode":I
    :goto_0
    iget-object v8, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const/high16 v9, 0x50000000

    move-object/from16 v0, p6

    invoke-static {v8, v5, v0, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 466
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v8, Landroid/support/v4/app/NotificationCompat$Builder;

    iget-object v9, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    move/from16 v0, p5

    invoke-virtual {v8, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8, p2}, Landroid/support/v4/app/NotificationCompat$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v8, v10, v11}, Landroid/support/v4/app/NotificationCompat$Builder;->setWhen(J)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    move-object/from16 v0, p7

    invoke-virtual {v8, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->setCategory(Ljava/lang/String;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setVisibility(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, -0x1

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setPriority(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/support/v4/app/NotificationCompat$Builder;->setLocalOnly(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/NotificationCompat$Builder;->build()Landroid/app/Notification;

    move-result-object v4

    .line 477
    .local v4, "notification":Landroid/app/Notification;
    iget-object v8, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8, p3, v2, v6}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 478
    iget-object v8, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    const-string v9, "notification"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    .line 480
    .local v3, "mgr":Landroid/app/NotificationManager;
    iget-object v8, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v8, p3, v2, v6}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 483
    invoke-static {}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->isLimitedOrSchoolEduUser()Z

    move-result v8

    if-nez v8, :cond_2

    const/4 v7, 0x1

    .line 484
    .local v7, "shouldShowNotification":Z
    :goto_1
    if-eqz v7, :cond_0

    .line 485
    invoke-virtual {v3, v5, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 488
    :cond_0
    move-object/from16 v0, p6

    invoke-static {v7, p1, p3, v2, v0}, Lcom/google/android/finsky/utils/NotificationManager;->logNotification(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 489
    return-void

    .line 460
    .end local v3    # "mgr":Landroid/app/NotificationManager;
    .end local v4    # "notification":Landroid/app/Notification;
    .end local v5    # "notificationCode":I
    .end local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v7    # "shouldShowNotification":Z
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v5

    goto :goto_0

    .line 483
    .restart local v3    # "mgr":Landroid/app/NotificationManager;
    .restart local v4    # "notification":Landroid/app/Notification;
    .restart local v5    # "notificationCode":I
    .restart local v6    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_2
    const/4 v7, 0x0

    goto :goto_1
.end method

.method public showOutstandingUpdatesMessage(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 565
    .local p1, "appDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 566
    .local v12, "res":Landroid/content/res/Resources;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v13

    .line 567
    .local v13, "updatesCount":I
    const v0, 0x7f100001

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v5

    invoke-virtual {v12, v0, v13, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 570
    .local v2, "tickerText":Ljava/lang/String;
    const v0, 0x7f100001

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v5

    invoke-virtual {v12, v0, v13, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 574
    .local v3, "contentTitle":Ljava/lang/String;
    const/4 v4, 0x0

    .line 575
    .local v4, "contentText":Ljava/lang/String;
    packed-switch v13, :pswitch_data_0

    .line 604
    const v1, 0x7f0c0164

    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v0, 0x4

    add-int/lit8 v7, v13, -0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v0

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 611
    :goto_0
    sget-boolean v0, Lcom/google/android/finsky/utils/NotificationManager;->SUPPORTS_RICH_NOTIFICATIONS:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    if-le v13, v0, :cond_0

    const v6, 0x7f0201d7

    .line 615
    .local v6, "iconResourceId":I
    :goto_1
    const/16 v0, 0x387

    invoke-direct {p0, v0}, Lcom/google/android/finsky/utils/NotificationManager;->logNotificationImpression(I)V

    .line 616
    invoke-static {}, Lcom/google/android/finsky/receivers/NotificationReceiver;->getOutstandingUpdateClickedIntent()Landroid/content/Intent;

    move-result-object v9

    .line 620
    .local v9, "onClickIntent":Landroid/content/Intent;
    const-string v1, "updates"

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v0, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;)V

    .line 622
    .end local v6    # "iconResourceId":I
    .end local v9    # "onClickIntent":Landroid/content/Intent;
    :goto_2
    return-void

    .line 577
    :pswitch_0
    const-string v0, "App count is 0 in new outstanding updates notification"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 580
    :pswitch_1
    const v1, 0x7f0c015f

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 582
    goto :goto_0

    .line 584
    :pswitch_2
    const v1, 0x7f0c0160

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 586
    goto :goto_0

    .line 588
    :pswitch_3
    const v1, 0x7f0c0161

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 591
    goto/16 :goto_0

    .line 593
    :pswitch_4
    const v1, 0x7f0c0162

    const/4 v0, 0x4

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 596
    goto/16 :goto_0

    .line 598
    :pswitch_5
    const v1, 0x7f0c0163

    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x4

    const/4 v0, 0x4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 602
    goto/16 :goto_0

    .line 611
    :cond_0
    const v6, 0x7f0201d6

    goto/16 :goto_1

    .line 575
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public showPurchaseErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "shortMessage"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "docId"    # Ljava/lang/String;
    .param p5, "detailsUrl"    # Ljava/lang/String;

    .prologue
    .line 387
    const v5, 0x108008a

    const-string v7, "err"

    move-object v0, p0

    move-object v1, p4

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/NotificationManager;->showDocNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 389
    return-void
.end method

.method public showSubscriptionsWarningMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 393
    const v5, 0x108008a

    const/4 v6, 0x2

    const-string v7, "err"

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/NotificationManager;->showAppNotificationOnly(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 397
    return-void
.end method

.method public showSuccessfulInstallMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "continueUrl"    # Ljava/lang/String;
    .param p4, "isUpdate"    # Z

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/finsky/utils/NotificationManager;->hideInstallingMessage()V

    .line 212
    if-eqz p4, :cond_0

    .line 213
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/utils/NotificationManager;->showSuccessfulUpdateMessage(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :goto_0
    return-void

    .line 215
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/utils/NotificationManager;->showSuccessfulNewInstallMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showUpdatesNeedApprovalMessage(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 626
    .local p1, "appDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v0, p0, Lcom/google/android/finsky/utils/NotificationManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 627
    .local v12, "res":Landroid/content/res/Resources;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v13

    .line 628
    .local v13, "updatesCount":I
    const v0, 0x7f100002

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v1, v5

    invoke-virtual {v12, v0, v13, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 630
    .local v3, "contentTitle":Ljava/lang/String;
    move-object v2, v3

    .line 632
    .local v2, "tickerText":Ljava/lang/String;
    const/4 v4, 0x0

    .line 633
    .local v4, "contentText":Ljava/lang/String;
    packed-switch v13, :pswitch_data_0

    .line 662
    const v1, 0x7f0c016a

    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v0, 0x4

    add-int/lit8 v7, v13, -0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v0

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 669
    :goto_0
    sget-boolean v0, Lcom/google/android/finsky/utils/NotificationManager;->SUPPORTS_RICH_NOTIFICATIONS:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    if-le v13, v0, :cond_0

    const v6, 0x7f0201d7

    .line 673
    .local v6, "iconResourceId":I
    :goto_1
    const/16 v0, 0x388

    invoke-direct {p0, v0}, Lcom/google/android/finsky/utils/NotificationManager;->logNotificationImpression(I)V

    .line 674
    invoke-static {}, Lcom/google/android/finsky/receivers/NotificationReceiver;->getNewUpdateNeedApprovalClicked()Landroid/content/Intent;

    move-result-object v9

    .line 678
    .local v9, "onClickIntent":Landroid/content/Intent;
    const-string v1, "updates"

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object v0, p0

    move-object v5, v4

    invoke-direct/range {v0 .. v11}, Lcom/google/android/finsky/utils/NotificationManager;->showNewNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/graphics/Bitmap;ILandroid/content/Intent;ZLandroid/content/Intent;)V

    .line 680
    .end local v6    # "iconResourceId":I
    .end local v9    # "onClickIntent":Landroid/content/Intent;
    :goto_2
    return-void

    .line 635
    :pswitch_0
    const-string v0, "App count is 0 in need approval notification"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 638
    :pswitch_1
    const v1, 0x7f0c0165

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 640
    goto :goto_0

    .line 642
    :pswitch_2
    const v1, 0x7f0c0166

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 644
    goto :goto_0

    .line 646
    :pswitch_3
    const v1, 0x7f0c0167

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 649
    goto/16 :goto_0

    .line 651
    :pswitch_4
    const v1, 0x7f0c0168

    const/4 v0, 0x4

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 654
    goto/16 :goto_0

    .line 656
    :pswitch_5
    const v1, 0x7f0c0169

    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x1

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x2

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x3

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    const/4 v7, 0x4

    const/4 v0, 0x4

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-virtual {v12, v1, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 660
    goto/16 :goto_0

    .line 669
    :cond_0
    const v6, 0x7f0201d6

    goto/16 :goto_1

    .line 633
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
