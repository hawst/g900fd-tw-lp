.class final Lcom/google/android/finsky/gearhead/GearheadStateMonitor$1;
.super Landroid/content/BroadcastReceiver;
.source "GearheadStateMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->initialize(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 88
    const-string v0, "com.google.android.gms.car.CONNECTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    # setter for: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sIsProjecting:Z
    invoke-static {v0}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$002(Z)Z

    .line 90
    # invokes: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->onReady()V
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$100()V

    .line 95
    :goto_0
    return-void

    .line 92
    :cond_0
    const/4 v0, 0x0

    # setter for: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sIsProjecting:Z
    invoke-static {v0}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$002(Z)Z

    .line 93
    # invokes: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->onReady()V
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$100()V

    goto :goto_0
.end method
