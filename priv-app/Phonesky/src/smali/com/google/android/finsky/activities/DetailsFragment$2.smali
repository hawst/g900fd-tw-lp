.class Lcom/google/android/finsky/activities/DetailsFragment$2;
.super Lcom/google/android/play/transition/BaseTransitionListener;
.source "DetailsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsFragment;->configureEnterTransition(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mTitleBackground:Landroid/graphics/drawable/Drawable;

.field private mViewsToFade:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsFragment;)V
    .locals 1

    .prologue
    .line 1339
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    invoke-direct {p0}, Lcom/google/android/play/transition/BaseTransitionListener;-><init>()V

    .line 1341
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment$2;->mViewsToFade:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Landroid/transition/Transition;)V
    .locals 13
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    const/4 v12, 0x2

    const-wide/16 v4, 0x190

    .line 1428
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 1429
    .local v11, "view":Landroid/view/ViewGroup;
    if-eqz v11, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
    invoke-static {v1}, Lcom/google/android/finsky/activities/DetailsFragment;->access$800(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1432
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSummaryViewBinder:Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
    invoke-static {v1}, Lcom/google/android/finsky/activities/DetailsFragment;->access$800(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->bindCoverFromDocument()V

    .line 1434
    new-instance v9, Landroid/animation/AnimatorSet;

    invoke-direct {v9}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1435
    .local v9, "fadeInSet":Landroid/animation/AnimatorSet;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 1440
    .local v8, "fadeInAnimators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    iget-object v1, v1, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/finsky/layout/HeroGraphicView;->unfreezeCorpusFill(J)V

    .line 1443
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment$2;->mViewsToFade:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1444
    .local v0, "viewToFade":Landroid/view/View;
    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1449
    .end local v0    # "viewToFade":Landroid/view/View;
    :cond_0
    const v1, 0x7f0a00ca

    invoke-virtual {v11, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    .line 1451
    .local v7, "detailsContainer":Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;
    const-string v1, "sectionSeparatorAlphaMultiplier"

    new-array v2, v12, [F

    fill-array-data v2, :array_0

    invoke-static {v7, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1455
    invoke-virtual {v9, v8}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1456
    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->start()V

    .line 1460
    .end local v7    # "detailsContainer":Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;
    .end local v8    # "fadeInAnimators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    .end local v9    # "fadeInSet":Landroid/animation/AnimatorSet;
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment$2;->mTitleBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 1461
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsFragment$2;->mTitleBackground:Landroid/graphics/drawable/Drawable;

    const-string v2, "alpha"

    new-array v3, v12, [I

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 1464
    :cond_2
    return-void

    .line 1451
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 1461
    :array_1
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method public onTransitionStart(Landroid/transition/Transition;)V
    .locals 29
    .param p1, "transition"    # Landroid/transition/Transition;

    .prologue
    .line 1345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v20

    .line 1346
    .local v20, "view":Landroid/view/View;
    if-nez v20, :cond_1

    .line 1424
    :cond_0
    :goto_0
    return-void

    .line 1349
    :cond_1
    const v23, 0x7f0a00ca

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    .line 1351
    .local v6, "detailsContainer":Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;
    invoke-virtual {v6}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildCount()I

    move-result v5

    .line 1352
    .local v5, "childCount":I
    if-eqz v5, :cond_0

    .line 1357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mUseWideLayout:Z

    move/from16 v23, v0

    if-eqz v23, :cond_3

    const v16, 0x7f0a019d

    .line 1359
    .local v16, "titleSectionId":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mColumnLayout:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 1360
    .local v15, "title":Landroid/view/View;
    if-eqz v15, :cond_2

    .line 1361
    invoke-virtual {v15}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/DetailsFragment$2;->mTitleBackground:Landroid/graphics/drawable/Drawable;

    .line 1362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->mTitleBackground:Landroid/graphics/drawable/Drawable;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {v23 .. v24}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1367
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v23, v0

    const-wide/16 v24, 0x190

    invoke-virtual/range {v23 .. v25}, Lcom/google/android/finsky/layout/HeroGraphicView;->freezeCorpusFill(J)V

    .line 1369
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    if-ge v11, v5, :cond_6

    .line 1370
    invoke-virtual {v6, v11}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1371
    .local v4, "child":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment;->mPromoHeroView:Lcom/google/android/finsky/layout/HeroGraphicView;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    if-ne v4, v0, :cond_4

    .line 1369
    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1357
    .end local v4    # "child":Landroid/view/View;
    .end local v11    # "i":I
    .end local v15    # "title":Landroid/view/View;
    .end local v16    # "titleSectionId":I
    :cond_3
    const v16, 0x7f0a0175

    goto :goto_1

    .line 1374
    .restart local v4    # "child":Landroid/view/View;
    .restart local v11    # "i":I
    .restart local v15    # "title":Landroid/view/View;
    .restart local v16    # "titleSectionId":I
    :cond_4
    instance-of v0, v4, Lcom/google/android/finsky/layout/DetailsPartialFadeSection;

    move/from16 v23, v0

    if-eqz v23, :cond_5

    move-object v13, v4

    .line 1375
    check-cast v13, Lcom/google/android/finsky/layout/DetailsPartialFadeSection;

    .line 1377
    .local v13, "partialFadeView":Lcom/google/android/finsky/layout/DetailsPartialFadeSection;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->mViewsToFade:Ljava/util/List;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-interface {v13, v0}, Lcom/google/android/finsky/layout/DetailsPartialFadeSection;->addParticipatingChildViews(Ljava/util/List;)V

    goto :goto_3

    .line 1379
    .end local v13    # "partialFadeView":Lcom/google/android/finsky/layout/DetailsPartialFadeSection;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->mViewsToFade:Ljava/util/List;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1382
    .end local v4    # "child":Landroid/view/View;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->mViewsToFade:Ljava/util/List;

    move-object/from16 v23, v0

    invoke-interface/range {v23 .. v23}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/View;

    .line 1383
    .local v21, "viewToFade":Landroid/view/View;
    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_4

    .line 1386
    .end local v21    # "viewToFade":Landroid/view/View;
    :cond_7
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->setSectionSeparatorAlphaMultiplier(F)V

    .line 1388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mRevealTransitionCoverName:Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/google/android/finsky/activities/DetailsFragment;->access$300(Lcom/google/android/finsky/activities/DetailsFragment;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_0

    .line 1391
    const v23, 0x7f0a00c8

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 1392
    .local v8, "hero":Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    .line 1393
    .local v10, "heroWidth":I
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 1396
    .local v9, "heroHeight":I
    const/high16 v23, 0x42c80000    # 100.0f

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/view/View;->setZ(F)V

    .line 1397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSourceLeft:I
    invoke-static/range {v23 .. v23}, Lcom/google/android/finsky/activities/DetailsFragment;->access$400(Lcom/google/android/finsky/activities/DetailsFragment;)I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v24, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSourceWidth:I
    invoke-static/range {v24 .. v24}, Lcom/google/android/finsky/activities/DetailsFragment;->access$500(Lcom/google/android/finsky/activities/DetailsFragment;)I

    move-result v24

    div-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    div-int/lit8 v24, v10, 0x2

    sub-int v18, v23, v24

    .line 1398
    .local v18, "translationX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v23, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSourceTop:I
    invoke-static/range {v23 .. v23}, Lcom/google/android/finsky/activities/DetailsFragment;->access$600(Lcom/google/android/finsky/activities/DetailsFragment;)I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$2;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v24, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSourceHeight:I
    invoke-static/range {v24 .. v24}, Lcom/google/android/finsky/activities/DetailsFragment;->access$700(Lcom/google/android/finsky/activities/DetailsFragment;)I

    move-result v24

    sub-int v24, v9, v24

    div-int/lit8 v24, v24, 0x2

    sub-int v19, v23, v24

    .line 1399
    .local v19, "translationY":I
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 1400
    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1402
    new-instance v17, Landroid/animation/AnimatorSet;

    invoke-direct/range {v17 .. v17}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1403
    .local v17, "translateSet":Landroid/animation/AnimatorSet;
    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const-string v25, "translationX"

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [F

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v28, v0

    aput v28, v26, v27

    const/16 v27, 0x1

    const/16 v28, 0x0

    aput v28, v26, v27

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v8, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    const-string v25, "translationY"

    const/16 v26, 0x2

    move/from16 v0, v26

    new-array v0, v0, [F

    move-object/from16 v26, v0

    const/16 v27, 0x0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v28, v0

    aput v28, v26, v27

    const/16 v27, 0x1

    const/16 v28, 0x0

    aput v28, v26, v27

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-static {v8, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v25

    aput-object v25, v23, v24

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1406
    new-instance v23, Lcom/google/android/finsky/activities/DetailsFragment$2$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v8}, Lcom/google/android/finsky/activities/DetailsFragment$2$1;-><init>(Lcom/google/android/finsky/activities/DetailsFragment$2;Landroid/view/View;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1414
    const-wide/16 v24, 0x190

    move-object/from16 v0, v17

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1415
    invoke-virtual/range {v17 .. v17}, Landroid/animation/AnimatorSet;->start()V

    .line 1417
    mul-int v23, v10, v10

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v22, v0

    .line 1418
    .local v22, "widthSquared":F
    mul-int v23, v9, v9

    move/from16 v0, v23

    int-to-float v7, v0

    .line 1419
    .local v7, "heightSquared":F
    add-float v23, v22, v7

    invoke-static/range {v23 .. v23}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v23

    const/high16 v24, 0x40000000    # 2.0f

    div-float v14, v23, v24

    .line 1421
    .local v14, "radius":F
    div-int/lit8 v23, v10, 0x2

    div-int/lit8 v24, v9, 0x2

    const/16 v25, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-static {v8, v0, v1, v2, v14}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v23

    const-wide/16 v24, 0x190

    invoke-virtual/range {v23 .. v25}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/animation/Animator;->start()V

    goto/16 :goto_0
.end method
