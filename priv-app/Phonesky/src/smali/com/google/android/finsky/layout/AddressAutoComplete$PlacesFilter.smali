.class Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;
.super Landroid/widget/Filter;
.source "AddressAutoComplete.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/AddressAutoComplete;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlacesFilter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/AddressAutoComplete;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/layout/AddressAutoComplete;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;->this$0:Lcom/google/android/finsky/layout/AddressAutoComplete;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/layout/AddressAutoComplete;Lcom/google/android/finsky/layout/AddressAutoComplete$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/layout/AddressAutoComplete;
    .param p2, "x1"    # Lcom/google/android/finsky/layout/AddressAutoComplete$1;

    .prologue
    .line 102
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;-><init>(Lcom/google/android/finsky/layout/AddressAutoComplete;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7
    .param p1, "constraint"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x0

    .line 108
    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;->this$0:Lcom/google/android/finsky/layout/AddressAutoComplete;

    monitor-enter v5

    .line 109
    :try_start_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;->this$0:Lcom/google/android/finsky/layout/AddressAutoComplete;

    # getter for: Lcom/google/android/finsky/layout/AddressAutoComplete;->mBlockNextSuggestion:Z
    invoke-static {v4}, Lcom/google/android/finsky/layout/AddressAutoComplete;->access$200(Lcom/google/android/finsky/layout/AddressAutoComplete;)Z

    move-result v0

    .line 110
    .local v0, "block":Z
    iget-object v4, p0, Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;->this$0:Lcom/google/android/finsky/layout/AddressAutoComplete;

    # getter for: Lcom/google/android/finsky/layout/AddressAutoComplete;->mSuggestionProvider:Lcom/google/android/finsky/layout/AddressSuggestionProvider;
    invoke-static {v4}, Lcom/google/android/finsky/layout/AddressAutoComplete;->access$300(Lcom/google/android/finsky/layout/AddressAutoComplete;)Lcom/google/android/finsky/layout/AddressSuggestionProvider;

    move-result-object v2

    .line 111
    .local v2, "provider":Lcom/google/android/finsky/layout/AddressSuggestionProvider;
    iget-object v4, p0, Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;->this$0:Lcom/google/android/finsky/layout/AddressAutoComplete;

    const/4 v6, 0x0

    # setter for: Lcom/google/android/finsky/layout/AddressAutoComplete;->mBlockNextSuggestion:Z
    invoke-static {v4, v6}, Lcom/google/android/finsky/layout/AddressAutoComplete;->access$202(Lcom/google/android/finsky/layout/AddressAutoComplete;Z)Z

    .line 112
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 114
    invoke-interface {v2, p1}, Lcom/google/android/finsky/layout/AddressSuggestionProvider;->getSuggestions(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v3

    .line 115
    .local v3, "suggestions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/placesapi/PlaceAutocompletePrediction;>;"
    if-eqz v3, :cond_0

    .line 116
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 117
    .local v1, "filterResults":Landroid/widget/Filter$FilterResults;
    iput-object v3, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 118
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    iput v4, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 124
    .end local v1    # "filterResults":Landroid/widget/Filter$FilterResults;
    .end local v3    # "suggestions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/placesapi/PlaceAutocompletePrediction;>;"
    :cond_0
    return-object v1

    .line 112
    .end local v0    # "block":Z
    .end local v2    # "provider":Lcom/google/android/finsky/layout/AddressSuggestionProvider;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2
    .param p1, "constraint"    # Ljava/lang/CharSequence;
    .param p2, "results"    # Landroid/widget/Filter$FilterResults;

    .prologue
    .line 131
    if-eqz p2, :cond_0

    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;->this$0:Lcom/google/android/finsky/layout/AddressAutoComplete;

    # getter for: Lcom/google/android/finsky/layout/AddressAutoComplete;->mAdapter:Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;
    invoke-static {v0}, Lcom/google/android/finsky/layout/AddressAutoComplete;->access$400(Lcom/google/android/finsky/layout/AddressAutoComplete;)Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;

    move-result-object v1

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    # setter for: Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;->mPredictions:Ljava/util/List;
    invoke-static {v1, v0}, Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;->access$002(Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;Ljava/util/List;)Ljava/util/List;

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;->this$0:Lcom/google/android/finsky/layout/AddressAutoComplete;

    # getter for: Lcom/google/android/finsky/layout/AddressAutoComplete;->mAdapter:Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;
    invoke-static {v0}, Lcom/google/android/finsky/layout/AddressAutoComplete;->access$400(Lcom/google/android/finsky/layout/AddressAutoComplete;)Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;->notifyDataSetChanged()V

    .line 138
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressAutoComplete$PlacesFilter;->this$0:Lcom/google/android/finsky/layout/AddressAutoComplete;

    # getter for: Lcom/google/android/finsky/layout/AddressAutoComplete;->mAdapter:Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;
    invoke-static {v0}, Lcom/google/android/finsky/layout/AddressAutoComplete;->access$400(Lcom/google/android/finsky/layout/AddressAutoComplete;)Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/AddressAutoComplete$Adapter;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
