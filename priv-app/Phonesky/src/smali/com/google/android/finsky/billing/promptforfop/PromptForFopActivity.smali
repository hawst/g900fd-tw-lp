.class public Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;
.super Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;
.source "PromptForFopActivity.java"

# interfaces
.implements Lcom/google/android/finsky/billing/promptforfop/PromptForFopMessageFragment$Listener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/accounts/Account;[B)Landroid/content/Intent;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "serverLogsCookie"    # [B

    .prologue
    .line 25
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 26
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, p1, v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;->putIntentExtras(Landroid/accounts/Account;[BLandroid/content/Intent;)V

    .line 27
    return-object v0
.end method


# virtual methods
.method protected createContentFragment()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;->mServerLogsCookie:[B

    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopFragment;->newInstance(Landroid/accounts/Account;[B)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method protected displayMessage(IILjava/lang/String;)V
    .locals 4
    .param p1, "messageId"    # I
    .param p2, "logType"    # I
    .param p3, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopMessageFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/finsky/billing/promptforfop/PromptForFopMessageFragment;

    move-result-object v0

    .line 50
    .local v0, "fragment":Lcom/google/android/finsky/billing/promptforfop/PromptForFopMessageFragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0a00c4

    const-string v3, "PromptForFopBaseActivity.fragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 53
    return-void
.end method

.method protected getActivityLayout()I
    .locals 1

    .prologue
    .line 32
    const v0, 0x7f040164

    return v0
.end method

.method protected getAlreadySetupEventType()I
    .locals 1

    .prologue
    .line 67
    const/16 v0, 0x163

    return v0
.end method

.method protected getBillingProfileErrorEventType()I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0x164

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0x3e9

    return v0
.end method

.method protected getSnoozeEventType()I
    .locals 1

    .prologue
    .line 57
    const/16 v0, 0x162

    return v0
.end method

.method public onContinueClicked()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;->setResult(I)V

    .line 76
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;->finish()V

    .line 77
    return-void
.end method
