.class public Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;
.super Ljava/lang/Object;
.source "PermissionsBucketer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/PermissionsBucketer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PermissionData"
.end annotation


# instance fields
.field public final mExistingPermissionsBucket:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;",
            ">;"
        }
    .end annotation
.end field

.field public final mForcePermissionPrompt:Z

.field public final mNewPermissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 252
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    .line 253
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mExistingPermissionsBucket:Ljava/util/List;

    .line 254
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mForcePermissionPrompt:Z

    .line 255
    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/util/List;Z)V
    .locals 0
    .param p3, "forcePermissionPrompt"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 258
    .local p1, "newPermissions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;>;"
    .local p2, "existingPermissions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 259
    iput-object p1, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    .line 260
    iput-object p2, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mExistingPermissionsBucket:Ljava/util/List;

    .line 261
    iput-boolean p3, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mForcePermissionPrompt:Z

    .line 262
    return-void
.end method
