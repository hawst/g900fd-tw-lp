.class public Lcom/google/android/finsky/layout/FadingEdgeImageView;
.super Lcom/google/android/play/image/FifeImageView;
.source "FadingEdgeImageView.java"


# instance fields
.field private mFadingEdgeBackgroundColor:I

.field private mHasFadingLeftEdge:Z

.field private mHasFadingRightEdge:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/play/image/FifeImageView;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/image/FifeImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method


# virtual methods
.method public clearFadingEdges()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 42
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setFadingEdgeLength(I)V

    .line 43
    iput-boolean v0, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mHasFadingLeftEdge:Z

    .line 44
    iput-boolean v0, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mHasFadingRightEdge:Z

    .line 45
    return-void
.end method

.method public configureFadingEdges(ZZII)V
    .locals 1
    .param p1, "hasFadingLeftEdge"    # Z
    .param p2, "hasFadingRightEdge"    # Z
    .param p3, "fadingEdgeLength"    # I
    .param p4, "fadingEdgeBackgroundColor"    # I

    .prologue
    .line 33
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setHorizontalFadingEdgeEnabled(Z)V

    .line 34
    invoke-virtual {p0, p3}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setFadingEdgeLength(I)V

    .line 35
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mHasFadingLeftEdge:Z

    .line 36
    iput-boolean p2, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mHasFadingRightEdge:Z

    .line 37
    iput p4, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mFadingEdgeBackgroundColor:I

    .line 38
    return-void
.end method

.method protected getLeftFadingEdgeStrength()F
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mHasFadingLeftEdge:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getRightFadingEdgeStrength()F
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mHasFadingRightEdge:Z

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSolidColor()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "drawing"
    .end annotation

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mHasFadingLeftEdge:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mHasFadingRightEdge:Z

    if-eqz v0, :cond_1

    .line 61
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/layout/FadingEdgeImageView;->mFadingEdgeBackgroundColor:I

    .line 63
    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Lcom/google/android/play/image/FifeImageView;->getSolidColor()I

    move-result v0

    goto :goto_0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    return v0
.end method

.method protected onSetAlpha(I)Z
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method
