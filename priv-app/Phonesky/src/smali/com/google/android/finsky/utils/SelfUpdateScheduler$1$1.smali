.class Lcom/google/android/finsky/utils/SelfUpdateScheduler$1$1;
.super Ljava/lang/Object;
.source "SelfUpdateScheduler.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->onTokenReceived(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1$1;->this$1:Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;)V
    .locals 10
    .param p1, "deliveryResponse"    # Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 186
    iget v9, p1, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->status:I

    .line 187
    .local v9, "responseCode":I
    if-eq v9, v1, :cond_0

    .line 188
    const-string v0, "SelfUpdate non-OK response - %d"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 206
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v0, p1, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    if-nez v0, :cond_1

    .line 193
    const-string v0, "SelfUpdate response missing appDeliveryData"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 196
    :cond_1
    iget-object v8, p1, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 198
    .local v8, "deliveryData":Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    iget-object v0, v8, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    aget-object v7, v0, v3

    .line 199
    .local v7, "authCookie":Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    iget-boolean v0, v8, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasDownloadSize:Z

    if-eqz v0, :cond_2

    iget-wide v4, v8, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    .line 201
    .local v4, "downloadSize":J
    :goto_1
    iget-boolean v0, v8, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasSignature:Z

    if-eqz v0, :cond_3

    iget-object v6, v8, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->signature:Ljava/lang/String;

    .line 203
    .local v6, "downloadSignature":Ljava/lang/String;
    :goto_2
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1$1;->this$1:Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;

    iget-object v0, v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->this$0:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    iget-object v1, v8, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadUrl:Ljava/lang/String;

    iget-object v2, v7, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;->name:Ljava/lang/String;

    iget-object v3, v7, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;->value:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/utils/SelfUpdateScheduler;->startSelfUpdateDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->access$000(Lcom/google/android/finsky/utils/SelfUpdateScheduler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    goto :goto_0

    .line 199
    .end local v4    # "downloadSize":J
    .end local v6    # "downloadSignature":Ljava/lang/String;
    :cond_2
    const-wide/16 v4, -0x1

    goto :goto_1

    .line 201
    .restart local v4    # "downloadSize":J
    :cond_3
    const/4 v6, 0x0

    goto :goto_2
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 183
    check-cast p1, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1$1;->onResponse(Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;)V

    return-void
.end method
