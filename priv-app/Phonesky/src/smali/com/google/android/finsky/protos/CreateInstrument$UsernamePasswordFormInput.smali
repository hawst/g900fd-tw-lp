.class public final Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UsernamePasswordFormInput"
.end annotation


# instance fields
.field public password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

.field public tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

.field public username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 933
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 934
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->clear()Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    .line 935
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 938
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    .line 939
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    .line 940
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    .line 941
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->cachedSize:I

    .line 942
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 962
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 963
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    if-eqz v1, :cond_0

    .line 964
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 967
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    if-eqz v1, :cond_1

    .line 968
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 971
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    if-eqz v1, :cond_2

    .line 972
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 975
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 983
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 984
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 988
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 989
    :sswitch_0
    return-object p0

    .line 994
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    if-nez v1, :cond_1

    .line 995
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    .line 997
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1001
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    if-nez v1, :cond_2

    .line 1002
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    .line 1004
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1008
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    if-nez v1, :cond_3

    .line 1009
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    .line 1011
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 984
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 907
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 948
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    if-eqz v0, :cond_0

    .line 949
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 951
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    if-eqz v0, :cond_1

    .line 952
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 954
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    if-eqz v0, :cond_2

    .line 955
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 957
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 958
    return-void
.end method
