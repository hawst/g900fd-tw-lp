.class public Lcom/google/android/finsky/auth/GmsCoreAuthApi;
.super Ljava/lang/Object;
.source "GmsCoreAuthApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/auth/GmsCoreAuthApi$3;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/auth/GmsCoreAuthApi;Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    .param p1, "x1"    # Lcom/google/android/finsky/auth/AuthResponseListener;
    .param p2, "x2"    # Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->handleVerifyPasswordResponse(Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/auth/GmsCoreAuthApi;Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    .param p1, "x1"    # Lcom/google/android/finsky/auth/AuthResponseListener;
    .param p2, "x2"    # Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->handleVerifyPinResponse(Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;)V

    return-void
.end method

.method private handleVerifyPasswordResponse(Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;)V
    .locals 4
    .param p1, "authResponseListener"    # Lcom/google/android/finsky/auth/AuthResponseListener;
    .param p2, "response"    # Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    .prologue
    .line 117
    if-nez p2, :cond_0

    sget-object v1, Lcom/google/android/gms/auth/firstparty/shared/Status;->UNKNOWN_ERROR:Lcom/google/android/gms/auth/firstparty/shared/Status;

    .line 118
    .local v1, "responseStatus":Lcom/google/android/gms/auth/firstparty/shared/Status;
    :goto_0
    sget-object v2, Lcom/google/android/finsky/auth/GmsCoreAuthApi$3;->$SwitchMap$com$google$android$gms$auth$firstparty$shared$Status:[I

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/shared/Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 141
    const/4 v0, 0x1

    .line 144
    .local v0, "errorType":I
    :goto_1
    if-eqz v0, :cond_1

    .line 145
    invoke-interface {p1, v0}, Lcom/google/android/finsky/auth/AuthResponseListener;->onAuthFailure(I)V

    .line 149
    :goto_2
    return-void

    .line 117
    .end local v0    # "errorType":I
    .end local v1    # "responseStatus":Lcom/google/android/gms/auth/firstparty/shared/Status;
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->getStatus()Lcom/google/android/gms/auth/firstparty/shared/Status;

    move-result-object v1

    goto :goto_0

    .line 120
    .restart local v1    # "responseStatus":Lcom/google/android/gms/auth/firstparty/shared/Status;
    :pswitch_0
    const/4 v0, 0x0

    .line 121
    .restart local v0    # "errorType":I
    goto :goto_1

    .line 123
    .end local v0    # "errorType":I
    :pswitch_1
    const/4 v0, 0x6

    .line 124
    .restart local v0    # "errorType":I
    goto :goto_1

    .line 127
    .end local v0    # "errorType":I
    :pswitch_2
    const/4 v0, 0x4

    .line 128
    .restart local v0    # "errorType":I
    goto :goto_1

    .line 130
    .end local v0    # "errorType":I
    :pswitch_3
    const/4 v0, 0x2

    .line 131
    .restart local v0    # "errorType":I
    goto :goto_1

    .line 133
    .end local v0    # "errorType":I
    :pswitch_4
    const/4 v0, 0x3

    .line 134
    .restart local v0    # "errorType":I
    goto :goto_1

    .line 136
    .end local v0    # "errorType":I
    :pswitch_5
    const/4 v0, 0x5

    .line 137
    .restart local v0    # "errorType":I
    goto :goto_1

    .line 148
    :cond_1
    invoke-interface {p1}, Lcom/google/android/finsky/auth/AuthResponseListener;->onAuthSuccess()V

    goto :goto_2

    .line 118
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private handleVerifyPinResponse(Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;)V
    .locals 2
    .param p1, "authResponseListener"    # Lcom/google/android/finsky/auth/AuthResponseListener;
    .param p2, "response"    # Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;

    .prologue
    .line 161
    if-nez p2, :cond_0

    const/4 v1, 0x1

    .line 163
    .local v1, "responseStatus":I
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 176
    :pswitch_0
    const/4 v0, 0x1

    .line 179
    .local v0, "errorType":I
    :goto_1
    if-eqz v0, :cond_1

    .line 180
    invoke-interface {p1, v0}, Lcom/google/android/finsky/auth/AuthResponseListener;->onAuthFailure(I)V

    .line 184
    :goto_2
    return-void

    .line 161
    .end local v0    # "errorType":I
    .end local v1    # "responseStatus":I
    :cond_0
    iget v1, p2, Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;->status:I

    goto :goto_0

    .line 165
    .restart local v1    # "responseStatus":I
    :pswitch_1
    const/4 v0, 0x0

    .line 166
    .restart local v0    # "errorType":I
    goto :goto_1

    .line 168
    .end local v0    # "errorType":I
    :pswitch_2
    const/4 v0, 0x5

    .line 169
    .restart local v0    # "errorType":I
    goto :goto_1

    .line 171
    .end local v0    # "errorType":I
    :pswitch_3
    const/4 v0, 0x4

    .line 172
    .restart local v0    # "errorType":I
    goto :goto_1

    .line 183
    :cond_1
    invoke-interface {p1}, Lcom/google/android/finsky/auth/AuthResponseListener;->onAuthSuccess()V

    goto :goto_2

    .line 163
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static isAvailable()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v2

    const-string v3, "cl:auth.use_gms_core_based_auth"

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 43
    .local v0, "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return v1

    .line 37
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    .line 38
    .restart local v0    # "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_0

    .line 43
    const/4 v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public validateUserPassword(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "gaiaPassword"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/google/android/finsky/auth/AuthResponseListener;

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/finsky/auth/GmsCoreAuthApi$1;-><init>(Lcom/google/android/finsky/auth/GmsCoreAuthApi;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V

    .line 75
    .local v0, "verifyPasswordResponseAsyncTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 76
    return-void
.end method

.method public validateUserPin(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V
    .locals 2
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "gaiaPin"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/google/android/finsky/auth/AuthResponseListener;

    .prologue
    .line 88
    new-instance v0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;-><init>(Lcom/google/android/finsky/auth/GmsCoreAuthApi;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V

    .line 104
    .local v0, "verifyPinResponseAsyncTask":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 105
    return-void
.end method
