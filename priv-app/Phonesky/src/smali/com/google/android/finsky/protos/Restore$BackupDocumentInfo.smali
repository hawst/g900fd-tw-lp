.class public final Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Restore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Restore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BackupDocumentInfo"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;


# instance fields
.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public hasRestorePriority:Z

.field public hasTitle:Z

.field public hasVersionCode:Z

.field public restorePriority:I

.field public thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

.field public title:Ljava/lang/String;

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 312
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 313
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->clear()Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    .line 314
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    .locals 2

    .prologue
    .line 283
    sget-object v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->_emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    if-nez v0, :cond_1

    .line 284
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 286
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->_emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    if-nez v0, :cond_0

    .line 287
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    sput-object v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->_emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    .line 289
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->_emptyArray:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    return-object v0

    .line 289
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 317
    iput-object v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 318
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->title:Ljava/lang/String;

    .line 319
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasTitle:Z

    .line 320
    iput v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->versionCode:I

    .line 321
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasVersionCode:Z

    .line 322
    iput-object v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 323
    iput v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->restorePriority:I

    .line 324
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasRestorePriority:Z

    .line 325
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->cachedSize:I

    .line 326
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 352
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 353
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_0

    .line 354
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 357
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasTitle:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 358
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 361
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasVersionCode:Z

    if-nez v1, :cond_3

    iget v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->versionCode:I

    if-eqz v1, :cond_4

    .line 362
    :cond_3
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_5

    .line 366
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasRestorePriority:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->restorePriority:I

    if-eqz v1, :cond_7

    .line 370
    :cond_6
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->restorePriority:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 373
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 381
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 382
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 386
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 387
    :sswitch_0
    return-object p0

    .line 392
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v1, :cond_1

    .line 393
    new-instance v1, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 395
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 399
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->title:Ljava/lang/String;

    .line 400
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasTitle:Z

    goto :goto_0

    .line 404
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->versionCode:I

    .line 405
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasVersionCode:Z

    goto :goto_0

    .line 409
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_2

    .line 410
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 412
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 416
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->restorePriority:I

    .line 417
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasRestorePriority:Z

    goto :goto_0

    .line 382
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 277
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_0

    .line 333
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 335
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasTitle:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 336
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 338
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasVersionCode:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->versionCode:I

    if-eqz v0, :cond_4

    .line 339
    :cond_3
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 341
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_5

    .line 342
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 344
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasRestorePriority:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->restorePriority:I

    if-eqz v0, :cond_7

    .line 345
    :cond_6
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->restorePriority:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 347
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 348
    return-void
.end method
