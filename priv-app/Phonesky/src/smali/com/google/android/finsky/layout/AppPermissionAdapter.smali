.class public Lcom/google/android/finsky/layout/AppPermissionAdapter;
.super Lcom/google/android/finsky/layout/PermissionAdapter;
.source "AppPermissionAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

.field private final mIsAppInstalled:Z

.field private mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "permissions"    # [Ljava/lang/String;
    .param p4, "hasAcceptedBuckets"    # Z

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/finsky/layout/PermissionAdapter;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    .line 32
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v2, p2}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->getPackageInfo(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 34
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mIsAppInstalled:Z

    .line 35
    invoke-static {v1}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->loadLocalAssetPermissions(Landroid/content/pm/PackageInfo;)Ljava/util/Set;

    move-result-object v0

    .line 37
    .local v0, "allLocalPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-static {p3, v0, p4}, Lcom/google/android/finsky/utils/PermissionsBucketer;->getPermissionBuckets([Ljava/lang/String;Ljava/util/Set;Z)Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    .line 39
    return-void

    .line 34
    .end local v0    # "allLocalPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/AppPermissionAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AppPermissionAdapter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getExistingPermissionsView(Landroid/view/ViewGroup;Ljava/util/List;)Landroid/view/View;
    .locals 26
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 136
    .local p2, "existingBuckets":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;>;"
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v12

    .line 139
    .local v12, "count":I
    if-eqz v12, :cond_0

    const/16 v4, 0x11

    if-ne v12, v4, :cond_1

    .line 140
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "numBuckets=["

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v23, "]"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v23, 0x0

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    :cond_1
    const/16 v18, 0x0

    .line 144
    .local v18, "permissionsStr":Ljava/lang/String;
    if-lez v12, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    iget v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 145
    .local v14, "first":Ljava/lang/String;
    :goto_0
    const/4 v4, 0x1

    if-le v12, v4, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    iget v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 146
    .local v19, "second":Ljava/lang/String;
    :goto_1
    const/4 v4, 0x2

    if-le v12, v4, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const/4 v4, 0x2

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    iget v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 147
    .local v21, "third":Ljava/lang/String;
    :goto_2
    const/4 v4, 0x3

    if-le v12, v4, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const/4 v4, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    iget v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 148
    .local v15, "fourth":Ljava/lang/String;
    :goto_3
    const/4 v4, 0x4

    if-le v12, v4, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const/4 v4, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    iget v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 149
    .local v13, "fifth":Ljava/lang/String;
    :goto_4
    const/4 v4, 0x5

    if-le v12, v4, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const/4 v4, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    iget v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 151
    .local v20, "sixth":Ljava/lang/String;
    :goto_5
    packed-switch v12, :pswitch_data_0

    .line 175
    :goto_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v23, 0x7f040095

    const/16 v24, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p1

    move/from16 v2, v24

    invoke-virtual {v4, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v22

    .line 176
    .local v22, "view":Landroid/view/View;
    const v4, 0x7f0a01d2

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 177
    .local v10, "bucketIcon":Landroid/widget/ImageView;
    const v4, 0x7f020114

    invoke-virtual {v10, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 178
    const v4, 0x7f0a0173

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 179
    .local v16, "header":Landroid/widget/TextView;
    const v4, 0x7f0a01d4

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 180
    .local v8, "shortDescription":Landroid/widget/TextView;
    const v4, 0x7f0a01d3

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 181
    .local v6, "expanderIcon":Landroid/widget/ImageView;
    const v4, 0x7f0a01d5

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 182
    .local v7, "detailedBuckets":Landroid/view/ViewGroup;
    const v4, 0x7f0c0285

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 183
    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    const v4, 0x7f0a01d0

    move-object/from16 v0, v22

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 188
    .local v5, "headerRow":Landroid/view/View;
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 189
    new-instance v3, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/finsky/layout/AppPermissionAdapter$1;-><init>(Lcom/google/android/finsky/layout/AppPermissionAdapter;Landroid/view/View;Landroid/widget/ImageView;Landroid/view/ViewGroup;Landroid/widget/TextView;)V

    .line 210
    .local v3, "headerOnClickListener":Landroid/view/View$OnClickListener;
    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    .local v17, "i$":Ljava/util/Iterator;
    :goto_7
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    .line 214
    .local v9, "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    iget v0, v9, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketDescription:I

    move/from16 v23, v0

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v9, v4}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->getPermissionView(Landroid/view/ViewGroup;Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;Ljava/lang/String;)Landroid/view/View;

    move-result-object v11

    .line 217
    .local v11, "bucketView":Landroid/view/View;
    const v4, 0x7f0a01d3

    invoke-virtual {v11, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 218
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    const/4 v4, 0x0

    invoke-virtual {v11, v4}, Landroid/view/View;->setClickable(Z)V

    .line 220
    const v4, 0x7f0a00b4

    invoke-virtual {v11, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 221
    invoke-virtual {v7, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_7

    .line 144
    .end local v3    # "headerOnClickListener":Landroid/view/View$OnClickListener;
    .end local v5    # "headerRow":Landroid/view/View;
    .end local v6    # "expanderIcon":Landroid/widget/ImageView;
    .end local v7    # "detailedBuckets":Landroid/view/ViewGroup;
    .end local v8    # "shortDescription":Landroid/widget/TextView;
    .end local v9    # "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v10    # "bucketIcon":Landroid/widget/ImageView;
    .end local v11    # "bucketView":Landroid/view/View;
    .end local v13    # "fifth":Ljava/lang/String;
    .end local v14    # "first":Ljava/lang/String;
    .end local v15    # "fourth":Ljava/lang/String;
    .end local v16    # "header":Landroid/widget/TextView;
    .end local v17    # "i$":Ljava/util/Iterator;
    .end local v19    # "second":Ljava/lang/String;
    .end local v20    # "sixth":Ljava/lang/String;
    .end local v21    # "third":Ljava/lang/String;
    .end local v22    # "view":Landroid/view/View;
    :cond_2
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 145
    .restart local v14    # "first":Ljava/lang/String;
    :cond_3
    const/16 v19, 0x0

    goto/16 :goto_1

    .line 146
    .restart local v19    # "second":Ljava/lang/String;
    :cond_4
    const/16 v21, 0x0

    goto/16 :goto_2

    .line 147
    .restart local v21    # "third":Ljava/lang/String;
    :cond_5
    const/4 v15, 0x0

    goto/16 :goto_3

    .line 148
    .restart local v15    # "fourth":Ljava/lang/String;
    :cond_6
    const/4 v13, 0x0

    goto/16 :goto_4

    .line 149
    .restart local v13    # "fifth":Ljava/lang/String;
    :cond_7
    const/16 v20, 0x0

    goto/16 :goto_5

    .line 153
    .restart local v20    # "sixth":Ljava/lang/String;
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    iget v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 154
    goto/16 :goto_6

    .line 156
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    const v23, 0x7f0c035f

    const/16 v24, 0x2

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v14, v24, v25

    const/16 v25, 0x1

    aput-object v19, v24, v25

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 157
    goto/16 :goto_6

    .line 159
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    const v23, 0x7f0c0360

    const/16 v24, 0x3

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v14, v24, v25

    const/16 v25, 0x1

    aput-object v19, v24, v25

    const/16 v25, 0x2

    aput-object v21, v24, v25

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 161
    goto/16 :goto_6

    .line 163
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    const v23, 0x7f0c0361

    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v14, v24, v25

    const/16 v25, 0x1

    aput-object v19, v24, v25

    const/16 v25, 0x2

    aput-object v21, v24, v25

    const/16 v25, 0x3

    aput-object v15, v24, v25

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 165
    goto/16 :goto_6

    .line 167
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    const v23, 0x7f0c0362

    const/16 v24, 0x5

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v14, v24, v25

    const/16 v25, 0x1

    aput-object v19, v24, v25

    const/16 v25, 0x2

    aput-object v21, v24, v25

    const/16 v25, 0x3

    aput-object v15, v24, v25

    const/16 v25, 0x4

    aput-object v13, v24, v25

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 169
    goto/16 :goto_6

    .line 171
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    const v23, 0x7f0c0363

    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v14, v24, v25

    const/16 v25, 0x1

    aput-object v19, v24, v25

    const/16 v25, 0x2

    aput-object v21, v24, v25

    const/16 v25, 0x3

    aput-object v15, v24, v25

    const/16 v25, 0x4

    aput-object v13, v24, v25

    const/16 v25, 0x5

    aput-object v20, v24, v25

    move/from16 v0, v23

    move-object/from16 v1, v24

    invoke-virtual {v4, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    goto/16 :goto_6

    .line 223
    .restart local v3    # "headerOnClickListener":Landroid/view/View$OnClickListener;
    .restart local v5    # "headerRow":Landroid/view/View;
    .restart local v6    # "expanderIcon":Landroid/widget/ImageView;
    .restart local v7    # "detailedBuckets":Landroid/view/ViewGroup;
    .restart local v8    # "shortDescription":Landroid/widget/TextView;
    .restart local v10    # "bucketIcon":Landroid/widget/ImageView;
    .restart local v16    # "header":Landroid/widget/TextView;
    .restart local v17    # "i$":Ljava/util/Iterator;
    .restart local v22    # "view":Landroid/view/View;
    :cond_8
    return-object v22

    .line 151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static getPackageInfo(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 2
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 105
    const/16 v1, 0x1000

    :try_start_0
    invoke-virtual {p0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 108
    :goto_0
    return-object v1

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getPermissionView(Landroid/view/ViewGroup;Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;Ljava/lang/String;)Landroid/view/View;
    .locals 8
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "bucket"    # Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .param p3, "contentViewText"    # Ljava/lang/String;

    .prologue
    .line 228
    iget-object v5, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f04010e

    const/4 v7, 0x0

    invoke-virtual {v5, v6, p1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 229
    .local v4, "view":Landroid/view/View;
    const v5, 0x7f0a0173

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 230
    .local v3, "headerView":Landroid/widget/TextView;
    const v5, 0x7f0a00b4

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 231
    .local v1, "contentView":Landroid/widget/TextView;
    const v5, 0x7f0a01d3

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 232
    .local v2, "expanderIcon":Landroid/widget/ImageView;
    const v5, 0x7f0a01d2

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 233
    .local v0, "bucketIcon":Landroid/widget/ImageView;
    iget v5, p2, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 234
    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    iget v5, p2, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketIcon:I

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 236
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 238
    new-instance v5, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;

    invoke-direct {v5, p0, v1, v2}, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;-><init>(Lcom/google/android/finsky/layout/AppPermissionAdapter;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    return-object v4
.end method

.method public static loadLocalAssetPermissions(Landroid/content/pm/PackageInfo;)Ljava/util/Set;
    .locals 6
    .param p0, "packageInfo"    # Landroid/content/pm/PackageInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/pm/PackageInfo;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    if-nez p0, :cond_1

    .line 92
    const/4 v3, 0x0

    .line 100
    :cond_0
    return-object v3

    .line 94
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v3

    .line 95
    .local v3, "oldPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v5, p0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 96
    iget-object v0, p0, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v4, v0, v1

    .line 97
    .local v4, "permission":Ljava/lang/String;
    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 96
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    iget-object v0, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    iget-object v0, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mExistingPermissionsBucket:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    iget-object v0, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    iget-object v0, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    iget-object v0, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mExistingPermissionsBucket:Ljava/util/List;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 65
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 114
    iget-object v4, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    iget-object v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge p1, v4, :cond_2

    .line 115
    iget-object v4, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    iget-object v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    .line 117
    .local v0, "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    iget v4, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketId:I

    const/16 v5, 0x10

    if-ne v4, v5, :cond_1

    .line 118
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .local v1, "builder":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mPermissionDescriptions:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 120
    iget-object v4, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    const v5, 0x7f0c02d2

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mPermissionDescriptions:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 124
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 128
    .end local v1    # "builder":Ljava/lang/StringBuilder;
    .end local v3    # "i":I
    .local v2, "contentViewText":Ljava/lang/String;
    :goto_1
    invoke-direct {p0, p3, v0, v2}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->getPermissionView(Landroid/view/ViewGroup;Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;Ljava/lang/String;)Landroid/view/View;

    move-result-object v4

    .line 130
    .end local v0    # "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v2    # "contentViewText":Ljava/lang/String;
    :goto_2
    return-object v4

    .line 126
    .restart local v0    # "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;

    iget v5, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketDescription:I

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "contentViewText":Ljava/lang/String;
    goto :goto_1

    .line 130
    .end local v0    # "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v2    # "contentViewText":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    iget-object v4, v4, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mExistingPermissionsBucket:Ljava/util/List;

    invoke-direct {p0, p3, v4}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->getExistingPermissionsView(Landroid/view/ViewGroup;Ljava/util/List;)Landroid/view/View;

    move-result-object v4

    goto :goto_2
.end method

.method public hasNewPermissions()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mData:Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    iget-object v0, v0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAppInstalled()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter;->mIsAppInstalled:Z

    return v0
.end method

.method public showTheNoPermissionMessage()Z
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->hasNewPermissions()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
