.class public abstract Lcom/google/android/finsky/billing/BillingFlowFragment;
.super Landroid/support/v4/app/Fragment;
.source "BillingFlowFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;
    }
.end annotation


# instance fields
.field protected mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 30
    return-void
.end method


# virtual methods
.method public abstract back()V
.end method

.method public abstract canGoBack()Z
.end method

.method public cancel()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;->onFlowCanceled(Lcom/google/android/finsky/billing/BillingFlowFragment;)V

    .line 140
    return-void
.end method

.method public fail(Ljava/lang/String;)V
    .locals 1
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    invoke-interface {v0, p0, p1}, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;->onFlowError(Lcom/google/android/finsky/billing/BillingFlowFragment;Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method public finish(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "result"    # Landroid/os/Bundle;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/finsky/billing/BillingFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    invoke-interface {v0, p0, p1}, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;->onFlowFinished(Lcom/google/android/finsky/billing/BillingFlowFragment;Landroid/os/Bundle;)V

    .line 131
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingFlowFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingFlowFragment;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    .line 101
    :goto_0
    return-void

    .line 96
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingFlowFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    if-eqz v0, :cond_1

    .line 97
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingFlowFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    goto :goto_0

    .line 99
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/BillingFlowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    iput-object v0, p0, Lcom/google/android/finsky/billing/BillingFlowFragment;->mHost:Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;

    goto :goto_0
.end method
