.class public Lcom/google/android/finsky/layout/StarLabel;
.super Landroid/view/View;
.source "StarLabel.java"


# instance fields
.field private mHeight:I

.field private mMaxStars:I

.field private mNumStars:I

.field private final mStarDrawable:Landroid/graphics/drawable/Drawable;

.field private final mStarPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/StarLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/StarLabel;->setNumStars(I)V

    .line 30
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/StarLabel;->setWillNotDraw(Z)V

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 33
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f02013f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/StarLabel;->mStarDrawable:Landroid/graphics/drawable/Drawable;

    .line 34
    const v1, 0x7f0b0170

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/StarLabel;->mStarPadding:I

    .line 35
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 60
    iget v5, p0, Lcom/google/android/finsky/layout/StarLabel;->mMaxStars:I

    if-gtz v5, :cond_1

    .line 73
    :cond_0
    return-void

    .line 63
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/StarLabel;->mStarDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 64
    .local v2, "starSize":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/StarLabel;->getWidth()I

    move-result v3

    .line 65
    .local v3, "x":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/StarLabel;->getHeight()I

    move-result v0

    .line 66
    .local v0, "height":I
    sub-int v5, v0, v2

    div-int/lit8 v4, v5, 0x2

    .line 67
    .local v4, "y":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v5, p0, Lcom/google/android/finsky/layout/StarLabel;->mNumStars:I

    if-ge v1, v5, :cond_0

    .line 68
    iget-object v5, p0, Lcom/google/android/finsky/layout/StarLabel;->mStarDrawable:Landroid/graphics/drawable/Drawable;

    sub-int v6, v3, v2

    add-int v7, v4, v2

    invoke-virtual {v5, v6, v4, v3, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 69
    iget-object v5, p0, Lcom/google/android/finsky/layout/StarLabel;->mStarDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 70
    iget v5, p0, Lcom/google/android/finsky/layout/StarLabel;->mStarPadding:I

    add-int/2addr v5, v2

    sub-int/2addr v3, v5

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 51
    iget-object v1, p0, Lcom/google/android/finsky/layout/StarLabel;->mStarDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 52
    .local v0, "starSize":I
    iget v1, p0, Lcom/google/android/finsky/layout/StarLabel;->mMaxStars:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/google/android/finsky/layout/StarLabel;->mMaxStars:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/google/android/finsky/layout/StarLabel;->mStarPadding:I

    mul-int/2addr v2, v3

    add-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/finsky/layout/StarLabel;->mHeight:I

    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/layout/StarLabel;->setMeasuredDimension(II)V

    .line 54
    return-void
.end method

.method public setMaxStars(I)V
    .locals 0
    .param p1, "maxStars"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/google/android/finsky/layout/StarLabel;->mMaxStars:I

    .line 43
    return-void
.end method

.method public setNumStars(I)V
    .locals 0
    .param p1, "numStars"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/google/android/finsky/layout/StarLabel;->mNumStars:I

    .line 39
    return-void
.end method

.method public setStarHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/google/android/finsky/layout/StarLabel;->mHeight:I

    .line 47
    return-void
.end method
