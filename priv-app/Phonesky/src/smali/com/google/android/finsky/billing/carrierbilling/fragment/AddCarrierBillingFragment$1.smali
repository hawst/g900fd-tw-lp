.class Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$1;
.super Ljava/lang/Object;
.source "AddCarrierBillingFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$1;->this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$1;->this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    # getter for: Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->mSetupWizardTosSelection:Landroid/widget/RadioGroup;
    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->access$000(Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;)Landroid/widget/RadioGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$1;->this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    # getter for: Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->mSetupWizardTosSelection:Landroid/widget/RadioGroup;
    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->access$000(Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;)Landroid/widget/RadioGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f0a0385

    if-ne v0, v1, :cond_1

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$1;->this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    # invokes: Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->handleTosAccepted()V
    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->access$100(Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;)V

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment$1;->this$0:Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;

    # invokes: Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->handleTosDeclined()V
    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;->access$200(Lcom/google/android/finsky/billing/carrierbilling/fragment/AddCarrierBillingFragment;)V

    goto :goto_0
.end method
