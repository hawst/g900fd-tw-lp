.class public Lcom/google/android/finsky/utils/SelfUpdateScheduler;
.super Ljava/lang/Object;
.source "SelfUpdateScheduler.java"

# interfaces
.implements Lcom/google/android/finsky/download/DownloadQueueListener;


# static fields
.field private static sCertificateHash:Ljava/lang/String;

.field private static sCertificateHashSelfUpdateMD5:Ljava/lang/String;


# instance fields
.field private final mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

.field private mDownloadSignature:Ljava/lang/String;

.field private mDownloadSize:J

.field private mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

.field private mMarketVersion:I

.field private mOnAppExitDeferrer:Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

.field private mUpdateDownload:Lcom/google/android/finsky/download/Download;

.field private mUpdateInProgress:Z


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/download/DownloadQueue;I)V
    .locals 3
    .param p1, "downloadQueue"    # Lcom/google/android/finsky/download/DownloadQueue;
    .param p2, "marketVersion"    # I

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateInProgress:Z

    .line 53
    iput-object v2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .line 56
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadSize:J

    .line 59
    iput-object v2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadSignature:Ljava/lang/String;

    .line 64
    iput-object v2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    .line 90
    iput-object p1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    .line 91
    iput p2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    .line 92
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/utils/SelfUpdateScheduler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/utils/SelfUpdateScheduler;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # J
    .param p6, "x5"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct/range {p0 .. p6}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->startSelfUpdateDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/utils/SelfUpdateScheduler;)Lcom/google/android/finsky/analytics/PlayStore$AppData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/utils/SelfUpdateScheduler;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    .prologue
    .line 36
    iget-wide v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadSize:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/utils/SelfUpdateScheduler;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadSignature:Ljava/lang/String;

    return-object v0
.end method

.method private static computeClientHashes(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 344
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 346
    .local v4, "vendingPackageName":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/16 v6, 0x40

    invoke-virtual {v5, v4, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 348
    .local v1, "marketPackageInfo":Landroid/content/pm/PackageInfo;
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 349
    .local v3, "signatures":[Landroid/content/pm/Signature;
    const/4 v5, 0x0

    aget-object v5, v3, v5

    invoke-virtual {v5}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v2

    .line 350
    .local v2, "signature":[B
    invoke-static {v2}, Lcom/google/android/finsky/utils/Md5Util;->secureHash([B)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->sCertificateHashSelfUpdateMD5:Ljava/lang/String;

    .line 351
    invoke-static {v2}, Lcom/google/android/finsky/utils/Sha1Util;->secureHash([B)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->sCertificateHash:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 357
    .end local v1    # "marketPackageInfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "signature":[B
    .end local v3    # "signatures":[Landroid/content/pm/Signature;
    :goto_0
    return-void

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "Unable to find package info for %s - %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v4, v6, v7

    const/4 v7, 0x1

    aput-object v0, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354
    const-string v5, "signature-hash-NameNotFoundException"

    sput-object v5, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->sCertificateHashSelfUpdateMD5:Ljava/lang/String;

    .line 355
    const-string v5, "certificate-hash-NameNotFoundException"

    sput-object v5, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->sCertificateHash:Ljava/lang/String;

    goto :goto_0
.end method

.method public static declared-synchronized getCertificateHash(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 337
    const-class v1, Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->sCertificateHash:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 338
    invoke-static {p0}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->computeClientHashes(Landroid/content/Context;)V

    .line 340
    :cond_0
    sget-object v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->sCertificateHash:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 337
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getCertificateHashSelfUpdateMD5(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 330
    const-class v1, Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->sCertificateHashSelfUpdateMD5:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 331
    invoke-static {p0}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->computeClientHashes(Landroid/content/Context;)V

    .line 333
    :cond_0
    sget-object v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->sCertificateHashSelfUpdateMD5:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private startSelfUpdateDownload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 20
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "cookieName"    # Ljava/lang/String;
    .param p3, "cookieValue"    # Ljava/lang/String;
    .param p4, "downloadSize"    # J
    .param p6, "downloadSignature"    # Ljava/lang/String;

    .prologue
    .line 233
    move-wide/from16 v0, p4

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadSize:J

    .line 234
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadSignature:Ljava/lang/String;

    .line 236
    new-instance v5, Lcom/google/android/finsky/download/DownloadImpl;

    const-string v7, ""

    const/4 v8, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, -0x1

    const-wide/16 v14, -0x1

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x1

    move-object/from16 v6, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    invoke-direct/range {v5 .. v18}, Lcom/google/android/finsky/download/DownloadImpl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;JJLcom/google/android/finsky/download/obb/Obb;ZZ)V

    .line 238
    .local v5, "download":Lcom/google/android/finsky/download/Download;
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    .line 239
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    move-object/from16 v0, p0

    invoke-interface {v4, v0}, Lcom/google/android/finsky/download/DownloadQueue;->addListener(Lcom/google/android/finsky/download/DownloadQueueListener;)V

    .line 240
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;

    invoke-interface {v4, v5}, Lcom/google/android/finsky/download/DownloadQueue;->add(Lcom/google/android/finsky/download/Download;)V

    .line 241
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v6

    const/16 v7, 0x64

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-virtual/range {v6 .. v12}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 244
    return-void
.end method


# virtual methods
.method public checkForSelfUpdate(ILjava/lang/String;)Z
    .locals 9
    .param p1, "serverMarketVersion"    # I
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 140
    iget-boolean v1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateInProgress:Z

    if-eqz v1, :cond_0

    .line 141
    const-string v1, "Skipping DFE self-update check as there is an update already queued."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v0}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v8

    .line 218
    :goto_0
    return v0

    .line 147
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    if-lt v1, p1, :cond_1

    .line 148
    const-string v1, "Skipping DFE self-update. Local Version [%d] >= Server Version [%d]"

    new-array v2, v2, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v8

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 153
    :cond_1
    const-string v1, "Starting DFE self-update from local version [%d] to server version [%d]"

    new-array v2, v2, [Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v8

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 158
    iput-boolean v8, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateInProgress:Z

    .line 161
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    .line 162
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iget v1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mMarketVersion:I

    iput v1, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 163
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v8, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 164
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput p1, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 165
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v8, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 166
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v8, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 167
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    iput-boolean v8, v0, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    .line 170
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    .line 171
    .local v7, "context":Landroid/content/Context;
    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 174
    .local v3, "vendingPackageName":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->getCertificateHashSelfUpdateMD5(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    .line 175
    .local v5, "certificateHashSelfUpdateMD5":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->getCertificateHash(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 177
    .local v6, "certificateHash":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;

    move-object v1, p0

    move-object v2, p2

    move v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;-><init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    invoke-static {v7, v0}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper;->get(Landroid/content/Context;Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;)V

    move v0, v8

    .line 218
    goto :goto_0
.end method

.method public getNewVersion(Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;)I
    .locals 2
    .param p1, "response"    # Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;

    .prologue
    .line 117
    const/4 v0, -0x1

    .line 118
    .local v0, "serverMarketVersion":I
    iget-boolean v1, p1, Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;->hasLatestClientVersionCode:Z

    if-eqz v1, :cond_0

    .line 119
    iget v0, p1, Lcom/google/android/finsky/protos/SelfUpdate$SelfUpdateResponse;->latestClientVersionCode:I

    .line 121
    :cond_0
    return v0
.end method

.method public getNewVersion(Lcom/google/android/finsky/protos/Toc$TocResponse;)I
    .locals 3
    .param p1, "response"    # Lcom/google/android/finsky/protos/Toc$TocResponse;

    .prologue
    .line 103
    const/4 v1, -0x1

    .line 104
    .local v1, "serverMarketVersion":I
    iget-object v2, p1, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    if-eqz v2, :cond_0

    .line 105
    iget-object v0, p1, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    .line 106
    .local v0, "config":Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;
    iget-boolean v2, v0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->hasLatestClientVersionCode:Z

    if-eqz v2, :cond_0

    .line 107
    iget v1, v0, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;->latestClientVersionCode:I

    .line 110
    .end local v0    # "config":Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;
    :cond_0
    return v1
.end method

.method public isSelfUpdateRunning()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateInProgress:Z

    return v0
.end method

.method public onCancel(Lcom/google/android/finsky/download/Download;)V
    .locals 0
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 321
    return-void
.end method

.method public onComplete(Lcom/google/android/finsky/download/Download;)V
    .locals 7
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 251
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    if-eq p1, v0, :cond_0

    .line 253
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Self-update ignoring completed download "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    :goto_0
    return-void

    .line 256
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 257
    .local v2, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x66

    iget-object v6, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 260
    iput-object v3, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    .line 261
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mOnAppExitDeferrer:Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

    if-eqz v0, :cond_1

    .line 262
    const-string v0, "Self-update package Uri was already assigned!"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 266
    :cond_1
    const-string v0, "Self-update ready to be installed, waiting for market to close."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    new-instance v0, Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mOnAppExitDeferrer:Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

    .line 268
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mOnAppExitDeferrer:Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;

    new-instance v1, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;

    invoke-direct {v1, p0, v2, p1}, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;-><init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler;Ljava/lang/String;Lcom/google/android/finsky/download/Download;)V

    const/16 v3, 0x2710

    invoke-virtual {v0, v1, v3}, Lcom/google/android/finsky/utils/ApplicationDismissedDeferrer;->runOnApplicationClose(Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public onError(Lcom/google/android/finsky/download/Download;I)V
    .locals 7
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "httpStatus"    # I

    .prologue
    const/4 v3, 0x0

    .line 302
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    if-ne p1, v0, :cond_0

    .line 303
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x68

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move v4, p2

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 306
    const-string v0, "Self-update failed because of HTTP error code: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    :cond_0
    return-void
.end method

.method public onNotificationClicked(Lcom/google/android/finsky/download/Download;)V
    .locals 0
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 324
    return-void
.end method

.method public onProgress(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V
    .locals 0
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "progress"    # Lcom/google/android/finsky/download/DownloadProgress;

    .prologue
    .line 327
    return-void
.end method

.method public onStart(Lcom/google/android/finsky/download/Download;)V
    .locals 7
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v3, 0x0

    .line 312
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mUpdateDownload:Lcom/google/android/finsky/download/Download;

    if-ne p1, v0, :cond_0

    .line 313
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x65

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 317
    :cond_0
    return-void
.end method
