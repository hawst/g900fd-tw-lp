.class Lcom/google/android/finsky/layout/ScreenshotGallery$3;
.super Ljava/lang/Object;
.source "ScreenshotGallery.java"

# interfaces
.implements Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/ScreenshotGallery;->loadNextImage(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

.field final synthetic val$imageIndex:I

.field final synthetic val$numImagesToLoad:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/ScreenshotGallery;II)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    iput p2, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->val$imageIndex:I

    iput p3, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->val$numImagesToLoad:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 7
    .param p1, "bitmapContainer"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 259
    iget-object v2, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # getter for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mImageStripAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;
    invoke-static {v2}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$000(Lcom/google/android/finsky/layout/ScreenshotGallery;)Lcom/google/android/finsky/adapters/ImageStripAdapter;

    move-result-object v2

    if-nez v2, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 264
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # getter for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mInFlightRequests:Ljava/util/List;
    invoke-static {v2}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$300(Lcom/google/android/finsky/layout/ScreenshotGallery;)Ljava/util/List;

    move-result-object v2

    iget v3, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->val$imageIndex:I

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 266
    invoke-virtual {p1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 267
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    .line 269
    iget-object v2, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # ++operator for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mNumImagesFailed:I
    invoke-static {v2}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$404(Lcom/google/android/finsky/layout/ScreenshotGallery;)I

    move-result v2

    iget v3, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->val$numImagesToLoad:I

    if-ne v2, v3, :cond_0

    .line 271
    iget-object v2, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # getter for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;
    invoke-static {v2}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$500(Lcom/google/android/finsky/layout/ScreenshotGallery;)Lcom/google/android/finsky/layout/LayoutSwitcher;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/ScreenshotGallery;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c02d9

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToErrorMode(Ljava/lang/String;)V

    goto :goto_0

    .line 275
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # getter for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;
    invoke-static {v2}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$500(Lcom/google/android/finsky/layout/ScreenshotGallery;)Lcom/google/android/finsky/layout/LayoutSwitcher;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/LayoutSwitcher;->isDataMode()Z

    move-result v2

    if-nez v2, :cond_3

    .line 276
    iget-object v2, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # getter for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;
    invoke-static {v2}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$500(Lcom/google/android/finsky/layout/ScreenshotGallery;)Lcom/google/android/finsky/layout/LayoutSwitcher;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToDataMode()V

    .line 281
    :cond_3
    new-instance v1, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v5}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v3, v2, v5

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # getter for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mResources:Landroid/content/res/Resources;
    invoke-static {v4}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$600(Lcom/google/android/finsky/layout/ScreenshotGallery;)Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v3, v2, v6

    invoke-direct {v1, v2}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 284
    .local v1, "fadeInDrawable":Landroid/graphics/drawable/TransitionDrawable;
    invoke-virtual {v1, v6}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 285
    const/16 v2, 0xfa

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 288
    iget-object v2, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->this$0:Lcom/google/android/finsky/layout/ScreenshotGallery;

    # getter for: Lcom/google/android/finsky/layout/ScreenshotGallery;->mImageStripAdapter:Lcom/google/android/finsky/adapters/ImageStripAdapter;
    invoke-static {v2}, Lcom/google/android/finsky/layout/ScreenshotGallery;->access$000(Lcom/google/android/finsky/layout/ScreenshotGallery;)Lcom/google/android/finsky/adapters/ImageStripAdapter;

    move-result-object v2

    iget v3, p0, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->val$imageIndex:I

    invoke-virtual {v2, v3, v1}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->setImageAt(ILandroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 256
    check-cast p1, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/ScreenshotGallery$3;->onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    return-void
.end method
