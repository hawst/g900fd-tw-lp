.class public Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
.super Landroid/view/ViewGroup;
.source "PlayCardClusterViewContent.java"

# interfaces
.implements Lcom/google/android/finsky/adapters/Recyclable;


# instance fields
.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field protected mCardContentHorizontalPadding:I

.field protected mCardContentPaddingBottom:I

.field protected mCardContentPaddingTop:I

.field private final mCardInset:I

.field protected mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field private mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

.field private mClusterLoggingDocument:Lcom/google/android/finsky/api/model/Document;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLooseDocumentsData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field protected mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentId:Ljava/lang/String;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mSmallCardContentMinHeight:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 72
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mInflater:Landroid/view/LayoutInflater;

    .line 76
    sget-object v2, Lcom/android/vending/R$styleable;->PlayCardClusterViewContent:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 78
    .local v1, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentPaddingTop:I

    .line 80
    const/4 v2, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentPaddingBottom:I

    .line 82
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 85
    .local v0, "res":Landroid/content/res/Resources;
    const v2, 0x7f0b0063

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mSmallCardContentMinHeight:I

    .line 87
    const v2, 0x7f0b0092

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardInset:I

    .line 88
    return-void
.end method

.method private getCardFromHeap(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;Lcom/google/android/finsky/layout/play/PlayCardHeap;)Lcom/google/android/play/layout/PlayCardViewBase;
    .locals 2
    .param p1, "cardMetadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .param p2, "cardHeap"    # Lcom/google/android/finsky/layout/play/PlayCardHeap;

    .prologue
    .line 177
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p2, p1, v1}, Lcom/google/android/finsky/layout/play/PlayCardHeap;->getCard(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;Landroid/view/LayoutInflater;)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v0

    .line 178
    .local v0, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayCardViewBase;->setThumbnailAspectRatio(F)V

    .line 179
    return-object v0
.end method

.method private getCellHeight(F)F
    .locals 9
    .param p1, "cellSize"    # F

    .prologue
    .line 313
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getCardMetadataForMinCellHeight()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v1

    .line 314
    .local v1, "cardMetadataForMinCellHeight":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    if-nez v1, :cond_0

    .line 330
    .end local p1    # "cellSize":F
    :goto_0
    return p1

    .line 319
    .restart local p1    # "cellSize":F
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v0

    .line 320
    .local v0, "cardHSpan":I
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v3

    .line 321
    .local v3, "cardVSpan":I
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v2

    .line 327
    .local v2, "cardThumbnailAspectRatio":F
    int-to-float v7, v0

    mul-float/2addr v7, p1

    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardInset:I

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    sub-float v5, v7, v8

    .line 328
    .local v5, "contentWidth":F
    mul-float v6, v5, v2

    .line 329
    .local v6, "thumbnailHeight":F
    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mSmallCardContentMinHeight:I

    int-to-float v7, v7

    add-float/2addr v7, v6

    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardInset:I

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float v4, v7, v8

    .line 330
    .local v4, "contentHeight":F
    int-to-float v7, v3

    div-float p1, v4, v7

    goto :goto_0
.end method


# virtual methods
.method public bindCardAt(IILcom/google/android/finsky/layout/play/PlayCardDismissListener;)V
    .locals 1
    .param p1, "tileIndex"    # I
    .param p2, "docIndex"    # I
    .param p3, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    .prologue
    .line 268
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->bindCardContent(Lcom/google/android/play/layout/PlayCardViewBase;IILcom/google/android/finsky/layout/play/PlayCardDismissListener;)V

    .line 269
    return-void
.end method

.method public bindCardContent(Lcom/google/android/play/layout/PlayCardViewBase;IILcom/google/android/finsky/layout/play/PlayCardDismissListener;)V
    .locals 17
    .param p1, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;
    .param p2, "tileIndex"    # I
    .param p3, "docIndex"    # I
    .param p4, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    .prologue
    .line 273
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getDoc(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    .line 274
    .local v3, "doc":Lcom/google/android/finsky/api/model/Document;
    if-nez v3, :cond_0

    .line 277
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/layout/PlayCardViewBase;->clearCardState()V

    .line 295
    :goto_0
    return-void

    .line 279
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->hasReasons()Z

    move-result v15

    .line 280
    .local v15, "docHasReasons":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v14

    .line 281
    .local v14, "cardMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->shouldRespectChildThumbnailAspectRatio(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v12

    .line 284
    .local v12, "aspectRatio":F
    :goto_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lcom/google/android/play/layout/PlayCardViewBase;->setThumbnailAspectRatio(F)V

    .line 285
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/play/layout/PlayCardViewBase;->getThumbnail()Lcom/google/android/play/layout/PlayCardThumbnail;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v13

    check-cast v13, Lcom/google/android/play/image/FifeImageView;

    .line 286
    .local v13, "cardCoverImage":Lcom/google/android/play/image/FifeImageView;
    invoke-virtual {v13}, Lcom/google/android/play/image/FifeImageView;->freezeImage()V

    .line 289
    move/from16 v16, v15

    .line 290
    .local v16, "isDismissable":Z
    if-eqz v16, :cond_2

    if-eqz p4, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/utils/ClientMutationCache;->isDismissedRecommendation(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v7, 0x1

    .line 292
    .local v7, "isDismissed":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mParentId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    if-eqz v16, :cond_3

    move-object/from16 v8, p4

    :goto_3
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v10, 0x0

    const/4 v11, -0x1

    move-object/from16 v2, p1

    invoke-static/range {v2 .. v11}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZI)V

    goto :goto_0

    .line 281
    .end local v7    # "isDismissed":Z
    .end local v12    # "aspectRatio":F
    .end local v13    # "cardCoverImage":Lcom/google/android/play/image/FifeImageView;
    .end local v16    # "isDismissable":Z
    :cond_1
    invoke-virtual {v14}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getThumbnailAspectRatio()F

    move-result v12

    goto :goto_1

    .line 290
    .restart local v12    # "aspectRatio":F
    .restart local v13    # "cardCoverImage":Lcom/google/android/play/image/FifeImageView;
    .restart local v16    # "isDismissable":Z
    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    .line 292
    .restart local v7    # "isDismissed":Z
    :cond_3
    const/4 v8, 0x0

    goto :goto_3
.end method

.method public createContent(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 6
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "dismissListener"    # Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .param p5, "cardHeap"    # Lcom/google/android/finsky/layout/play/PlayCardHeap;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 217
    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 218
    iput-object p3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 219
    iput-object p6, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 221
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getNumberOfTilesToBind()I

    move-result v4

    .line 222
    .local v4, "tilesToBind":I
    const/4 v3, 0x0

    .local v3, "tileIndex":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 223
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v5, v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v1

    .line 225
    .local v1, "cardMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    invoke-direct {p0, v1, p5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardFromHeap(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;Lcom/google/android/finsky/layout/play/PlayCardHeap;)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v0

    .line 228
    .local v0, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    if-ge v3, v4, :cond_0

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->tileIndexToDocumentIndex(I)I

    move-result v2

    .line 229
    .local v2, "docIndex":I
    :goto_1
    invoke-virtual {p0, v0, v3, v2, p4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->bindCardContent(Lcom/google/android/play/layout/PlayCardViewBase;IILcom/google/android/finsky/layout/play/PlayCardDismissListener;)V

    .line 230
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->addView(Landroid/view/View;)V

    .line 222
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 228
    .end local v2    # "docIndex":I
    :cond_0
    const/4 v2, -0x1

    goto :goto_1

    .line 232
    .end local v0    # "card":Lcom/google/android/play/layout/PlayCardViewBase;
    .end local v1    # "cardMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    :cond_1
    return-void
.end method

.method public getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;
    .locals 1
    .param p1, "cardIndex"    # I

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getIndexOfFirstCard()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    return-object v0
.end method

.method public getCardChildCount()I
    .locals 2

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getIndexOfFirstCard()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getCardChildIndex(Landroid/view/View;)I
    .locals 2
    .param p1, "card"    # Landroid/view/View;

    .prologue
    .line 198
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 199
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 200
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getIndexOfFirstCard()I

    move-result v1

    sub-int v1, v0, v1

    .line 203
    :goto_1
    return v1

    .line 198
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getCardContentHorizontalPadding()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentHorizontalPadding:I

    return v0
.end method

.method protected getCellSize(I)F
    .locals 3
    .param p1, "availableWidth"    # I

    .prologue
    .line 308
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getWidth()I

    move-result v0

    .line 309
    .local v0, "columns":I
    int-to-float v1, p1

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentHorizontalPadding:I

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    int-to-float v2, v0

    div-float/2addr v1, v2

    return v1
.end method

.method public getClusterLoggingDocument()Lcom/google/android/finsky/api/model/Document;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterLoggingDocument:Lcom/google/android/finsky/api/model/Document;

    if-nez v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

    .line 117
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterLoggingDocument:Lcom/google/android/finsky/api/model/Document;

    goto :goto_0
.end method

.method protected final getDoc(I)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 162
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getDocCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 163
    :cond_0
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mLooseDocumentsData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    goto :goto_0
.end method

.method protected final getDocCount()I
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mLooseDocumentsData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method protected getExtraColumnOffset()I
    .locals 10

    .prologue
    .line 417
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v5

    .line 418
    .local v5, "tileCount":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getIndexOfFirstCard()I

    move-result v2

    .line 420
    .local v2, "childOffset":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->shouldAlignToParentEndIfNecessary()Z

    move-result v8

    if-nez v8, :cond_0

    .line 421
    const/4 v8, 0x0

    .line 443
    :goto_0
    return v8

    .line 424
    :cond_0
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getWidth()I

    move-result v4

    .line 429
    .local v4, "templateColumnCount":I
    const/4 v3, 0x0

    .line 430
    .local v3, "rightmostOccupiedColumn":I
    const/4 v6, 0x0

    .local v6, "tileIndex":I
    :goto_1
    if-ge v6, v5, :cond_2

    .line 431
    add-int v8, v2, v6

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v8

    const/4 v9, 0x4

    if-ne v8, v9, :cond_1

    .line 430
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 434
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v8, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v7

    .line 435
    .local v7, "tileMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getXStart()I

    move-result v1

    .line 436
    .local v1, "cardXStart":I
    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v0

    .line 437
    .local v0, "cardHSpan":I
    add-int v8, v1, v0

    invoke-static {v3, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    goto :goto_2

    .line 439
    .end local v0    # "cardHSpan":I
    .end local v1    # "cardXStart":I
    .end local v7    # "tileMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_2
    if-nez v3, :cond_3

    .line 441
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getLeadingGap()I

    move-result v8

    sub-int v8, v4, v8

    goto :goto_0

    .line 443
    :cond_3
    sub-int v8, v4, v3

    goto :goto_0
.end method

.method protected getIndexOfFirstCard()I
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    return v0
.end method

.method public getLeadingGap(I)I
    .locals 3
    .param p1, "availableWidth"    # I

    .prologue
    .line 298
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellSize(I)F

    move-result v0

    .line 299
    .local v0, "cellSize":F
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getLeadingGap()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    add-int/2addr v1, v2

    return v1
.end method

.method public getMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    return-object v0
.end method

.method protected getNumberOfTilesToBind()I
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v0

    return v0
.end method

.method public getTrailingGap(I)I
    .locals 3
    .param p1, "availableWidth"    # I

    .prologue
    .line 303
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellSize(I)F

    move-result v0

    .line 304
    .local v0, "cellSize":F
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getPaddingRight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTrailingGap()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    add-int/2addr v1, v2

    return v1
.end method

.method public hideCardAt(I)V
    .locals 6
    .param p1, "tileIndex"    # I

    .prologue
    .line 255
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v0

    .line 256
    .local v0, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getContext()Landroid/content/Context;

    move-result-object v2

    const-wide/16 v4, 0xfa

    new-instance v3, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;Lcom/google/android/play/layout/PlayCardViewBase;)V

    invoke-static {v2, v4, v5, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 264
    .local v1, "fadeOut":Landroid/view/animation/Animation;
    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayCardViewBase;->startAnimation(Landroid/view/animation/Animation;)V

    .line 265
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 21
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 381
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getHeight()I

    move-result v14

    .line 382
    .local v14, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getWidth()I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellSize(I)F

    move-result v10

    .line 383
    .local v10, "cellSize":F
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellHeight(F)F

    move-result v9

    .line 384
    .local v9, "cellHeight":F
    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentHorizontalPadding:I

    .line 386
    .local v15, "paddingLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v16

    .line 387
    .local v16, "tileCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getHeight()I

    move-result v12

    .line 388
    .local v12, "clusterRowCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getIndexOfFirstCard()I

    move-result v11

    .line 389
    .local v11, "childOffset":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getExtraColumnOffset()I

    move-result v13

    .line 391
    .local v13, "extraColumnOffset":I
    const/16 v17, 0x0

    .local v17, "tileIndex":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, v16

    if-ge v0, v1, :cond_1

    .line 392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v18

    .line 393
    .local v18, "tileMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getXStart()I

    move-result v19

    add-int v6, v19, v13

    .line 394
    .local v6, "cardXStart":I
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getYStart()I

    move-result v8

    .line 395
    .local v8, "cardYStart":I
    add-int v19, v11, v17

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 396
    .local v2, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    int-to-float v0, v6

    move/from16 v19, v0

    mul-float v19, v19, v10

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    add-int v5, v15, v19

    .line 397
    .local v5, "cardLeft":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentPaddingBottom:I

    move/from16 v19, v0

    sub-int v3, v14, v19

    .line 398
    .local v3, "cardBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->shouldRespectChildHeight()Z

    move-result v19

    if-nez v19, :cond_0

    .line 399
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v19

    add-int v7, v8, v19

    .line 400
    .local v7, "cardYEnd":I
    sub-int v19, v12, v7

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    mul-float v19, v19, v9

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    sub-int v3, v3, v19

    .line 403
    .end local v7    # "cardYEnd":I
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/play/layout/PlayCardViewBase;->getMeasuredHeight()I

    move-result v19

    sub-int v19, v3, v19

    invoke-virtual {v2}, Lcom/google/android/play/layout/PlayCardViewBase;->getMeasuredWidth()I

    move-result v20

    add-int v20, v20, v5

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v2, v5, v0, v1, v3}, Lcom/google/android/play/layout/PlayCardViewBase;->layout(IIII)V

    .line 405
    invoke-virtual {v2}, Lcom/google/android/play/layout/PlayCardViewBase;->getThumbnail()Lcom/google/android/play/layout/PlayCardThumbnail;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/image/FifeImageView;

    .line 406
    .local v4, "cardCoverImage":Lcom/google/android/play/image/FifeImageView;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/google/android/play/image/FifeImageView;->unfreezeImage(Z)V

    .line 391
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0

    .line 408
    .end local v2    # "card":Lcom/google/android/play/layout/PlayCardViewBase;
    .end local v3    # "cardBottom":I
    .end local v4    # "cardCoverImage":Lcom/google/android/play/image/FifeImageView;
    .end local v5    # "cardLeft":I
    .end local v6    # "cardXStart":I
    .end local v8    # "cardYStart":I
    .end local v18    # "tileMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 22
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 335
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 336
    .local v2, "availableWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getHeight()I

    move-result v14

    .line 337
    .local v14, "rows":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellSize(I)F

    move-result v9

    .line 338
    .local v9, "cellSize":F
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCellHeight(F)F

    move-result v8

    .line 340
    .local v8, "cellHeight":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->shouldRespectChildHeight()Z

    move-result v13

    .line 342
    .local v13, "respectChildHeight":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v16

    .line 343
    .local v16, "tileCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getIndexOfFirstCard()I

    move-result v11

    .line 344
    .local v11, "childOffset":I
    const/4 v10, 0x0

    .line 345
    .local v10, "childMaxHeight":I
    const/16 v19, 0x0

    .line 346
    .local v19, "visibleCardCount":I
    const/16 v17, 0x0

    .local v17, "tileIndex":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, v16

    if-ge v0, v1, :cond_2

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    move-result-object v18

    .line 348
    .local v18, "tileMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getHSpan()I

    move-result v4

    .line 349
    .local v4, "cardHSpan":I
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->getVSpan()I

    move-result v6

    .line 350
    .local v6, "cardVSpan":I
    add-int v20, v11, v17

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 351
    .local v3, "card":Landroid/view/View;
    int-to-float v0, v4

    move/from16 v20, v0

    mul-float v20, v20, v9

    move/from16 v0, v20

    float-to-int v7, v0

    .line 352
    .local v7, "cardWidth":I
    int-to-float v0, v6

    move/from16 v20, v0

    mul-float v20, v20, v8

    move/from16 v0, v20

    float-to-int v5, v0

    .line 353
    .local v5, "cardHeight":I
    const/high16 v20, 0x40000000    # 2.0f

    move/from16 v0, v20

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 354
    .local v12, "childWidthSpec":I
    if-eqz v13, :cond_1

    .line 355
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v12, v0}, Landroid/view/View;->measure(II)V

    .line 356
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v20

    move/from16 v0, v20

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 361
    :goto_1
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v20

    if-nez v20, :cond_0

    .line 362
    add-int/lit8 v19, v19, 0x1

    .line 346
    :cond_0
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 358
    :cond_1
    const/high16 v20, 0x40000000    # 2.0f

    move/from16 v0, v20

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v3, v12, v0}, Landroid/view/View;->measure(II)V

    goto :goto_1

    .line 366
    .end local v3    # "card":Landroid/view/View;
    .end local v4    # "cardHSpan":I
    .end local v5    # "cardHeight":I
    .end local v6    # "cardVSpan":I
    .end local v7    # "cardWidth":I
    .end local v12    # "childWidthSpec":I
    .end local v18    # "tileMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;
    :cond_2
    if-nez v19, :cond_3

    .line 367
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setMeasuredDimension(II)V

    .line 377
    :goto_2
    return-void

    .line 369
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentPaddingTop:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentPaddingBottom:I

    move/from16 v21, v0

    add-int v15, v20, v21

    .line 370
    .local v15, "targetHeight":I
    if-eqz v13, :cond_4

    .line 371
    add-int/2addr v15, v10

    .line 375
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setMeasuredDimension(II)V

    goto :goto_2

    .line 373
    :cond_4
    int-to-float v0, v14

    move/from16 v20, v0

    mul-float v20, v20, v8

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int v15, v15, v20

    goto :goto_3
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 108
    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

    .line 109
    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mLooseDocumentsData:Ljava/util/List;

    .line 110
    return-void
.end method

.method public removeAllCards()V
    .locals 2

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getIndexOfFirstCard()I

    move-result v0

    .line 188
    .local v0, "indexOfFirstCard":I
    if-nez v0, :cond_1

    .line 189
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->removeAllViews()V

    .line 195
    :cond_0
    return-void

    .line 192
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getChildCount()I

    move-result v1

    if-le v1, v0, :cond_0

    .line 193
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->removeViewAt(I)V

    goto :goto_0
.end method

.method public setCardContentHorizontalPadding(I)V
    .locals 1
    .param p1, "cardContentHorizontalPadding"    # I

    .prologue
    .line 136
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentHorizontalPadding:I

    if-eq v0, p1, :cond_0

    .line 137
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentHorizontalPadding:I

    .line 138
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->requestLayout()V

    .line 140
    :cond_0
    return-void
.end method

.method public setCardContentVerticalPadding(I)V
    .locals 0
    .param p1, "mCardContentVerticalPadding"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentPaddingTop:I

    .line 127
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mCardContentPaddingBottom:I

    .line 128
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->requestLayout()V

    .line 129
    return-void
.end method

.method public setClusterDocumentData(Lcom/google/android/finsky/api/model/Document;)V
    .locals 2
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mLooseDocumentsData:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 92
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already initialized with loose documents"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_0
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mParentId:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setClusterLoggingDocument(Lcom/google/android/finsky/api/model/Document;)V
    .locals 0
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterLoggingDocument:Lcom/google/android/finsky/api/model/Document;

    .line 123
    return-void
.end method

.method public setLooseDocumentsData(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .param p2, "parentId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "looseDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClusterDocumentData:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already initialized with cluster document"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mLooseDocumentsData:Ljava/util/List;

    .line 103
    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mParentId:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public setMetadata(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;)V
    .locals 0
    .param p1, "metadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p2, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    .line 153
    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 154
    return-void
.end method

.method protected tileIndexToDocumentIndex(I)I
    .locals 0
    .param p1, "tileIndex"    # I

    .prologue
    .line 248
    return p1
.end method
