.class Lcom/google/android/finsky/layout/OrderHistoryRowView$1;
.super Ljava/lang/Object;
.source "OrderHistoryRowView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/OrderHistoryRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/OrderHistoryRowView;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const-wide/16 v2, 0x96

    const/4 v1, 0x0

    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mHasPurchaseDetails:Z
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$000(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPurchaseDetailsView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$100(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mPurchaseDetailsView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$100(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->animateFadeIn(Landroid/view/View;J)V

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCanOpen:Z
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$200(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$300(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 122
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mOpenButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$300(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->animateFadeIn(Landroid/view/View;J)V

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mCanRefund:Z
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$400(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$500(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$1;->this$0:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    # getter for: Lcom/google/android/finsky/layout/OrderHistoryRowView;->mRefundButton:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->access$500(Lcom/google/android/finsky/layout/OrderHistoryRowView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->animateFadeIn(Landroid/view/View;J)V

    .line 128
    :cond_2
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 132
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 112
    return-void
.end method
