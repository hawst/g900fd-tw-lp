.class public Lcom/google/android/finsky/api/model/Document;
.super Ljava/lang/Object;
.source "Document.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

.field private final mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field private mImageTypeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Common$Image;",
            ">;>;"
        }
    .end annotation
.end field

.field private mSubscriptionsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private mTrustedSourceProfileDocument:Lcom/google/android/finsky/api/model/Document;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1435
    new-instance v0, Lcom/google/android/finsky/api/model/Document$1;

    invoke-direct {v0}, Lcom/google/android/finsky/api/model/Document$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/api/model/Document;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V
    .locals 0
    .param p1, "document"    # Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 94
    return-void
.end method

.method private getCrossSellSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getImageTypeMap()Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Common$Image;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 754
    iget-object v5, p0, Lcom/google/android/finsky/api/model/Document;->mImageTypeMap:Ljava/util/Map;

    if-nez v5, :cond_1

    .line 755
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/api/model/Document;->mImageTypeMap:Ljava/util/Map;

    .line 756
    iget-object v5, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Common$Image;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v2, v0, v1

    .line 757
    .local v2, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget v3, v2, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    .line 758
    .local v3, "imageType":I
    iget-object v5, p0, Lcom/google/android/finsky/api/model/Document;->mImageTypeMap:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 759
    iget-object v5, p0, Lcom/google/android/finsky/api/model/Document;->mImageTypeMap:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 761
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/api/model/Document;->mImageTypeMap:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 756
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 764
    .end local v0    # "arr$":[Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "i$":I
    .end local v2    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v3    # "imageType":I
    .end local v4    # "len$":I
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/api/model/Document;->mImageTypeMap:Ljava/util/Map;

    return-object v5
.end method

.method private getMoreBySectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getRelatedSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelated:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;
    .locals 1

    .prologue
    .line 1305
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z
    .locals 6
    .param p0, "offer"    # Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    const/4 v0, 0x1

    .line 943
    if-eqz p0, :cond_1

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Offer;->hasOnSaleDate:Z

    if-eqz v1, :cond_1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public canUseAsPartialDocument()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1360
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    .line 1376
    :cond_0
    :goto_0
    return v0

    .line 1366
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSongDetails()Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1371
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-boolean v1, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsReusable:Z

    if-eqz v1, :cond_0

    .line 1376
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1427
    const/4 v0, 0x0

    return v0
.end method

.method public getActionBanner()Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
    .locals 1

    .prologue
    .line 1255
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->isActionBanner()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;
    .locals 1

    .prologue
    .line 785
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAntennaInfo()Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
    .locals 1

    .prologue
    .line 1215
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    return-object v0
.end method

.method public getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    .locals 1

    .prologue
    .line 778
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->appDetails:Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getArtistDetails()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .locals 1

    .prologue
    .line 792
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->artistDetails:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAttributions()Ljava/lang/String;
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->attributionHtml:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getAvailabilityRestriction()I
    .locals 1

    .prologue
    .line 954
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    iget v0, v0, Lcom/google/android/finsky/protos/FilterRules$Availability;->restriction:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;
    .locals 1

    .prologue
    .line 919
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    return-object v0
.end method

.method public getBackend()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendId:I

    return v0
.end method

.method public getBackendDocId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendDocid:Ljava/lang/String;

    return-object v0
.end method

.method public getBackingDocV2()Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .locals 1

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    return-object v0
.end method

.method public getBadgeContainer()Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    .locals 2

    .prologue
    .line 1138
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getBadgeForContentRating()Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .locals 1

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForContentRating:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    return-object v0
.end method

.method public getBodyOfWorkBrowseUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 258
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 259
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasBodyOfWork()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    .line 262
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getBodyOfWorkHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 246
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 247
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasBodyOfWork()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    .line 250
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getBodyOfWorkListUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 270
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 271
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasBodyOfWork()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 272
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    .line 274
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getBookDetails()Lcom/google/android/finsky/protos/DocDetails$BookDetails;
    .locals 1

    .prologue
    .line 806
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->bookDetails:Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCensoring()I
    .locals 1

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iget v0, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->censoring:I

    return v0
.end method

.method public getChildAt(I)Lcom/google/android/finsky/api/model/Document;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    if-nez v0, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/finsky/api/model/Document;

    iput-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    new-instance v1, Lcom/google/android/finsky/api/model/Document;

    iget-object v2, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v2, v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v2, v2, p1

    invoke-direct {v1, v2}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    aput-object v1, v0, p1

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getChildCount()I
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v0, v0

    return v0
.end method

.method public getChildren()[Lcom/google/android/finsky/api/model/Document;
    .locals 5

    .prologue
    .line 121
    iget-object v2, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    if-nez v2, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/finsky/api/model/Document;

    iput-object v2, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    .line 124
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v0

    .line 125
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 126
    iget-object v2, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    aget-object v2, v2, v1

    if-nez v2, :cond_1

    .line 127
    iget-object v2, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    new-instance v3, Lcom/google/android/finsky/api/model/Document;

    iget-object v4, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v4, v4, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->child:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v4, v4, v1

    invoke-direct {v3, v4}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    aput-object v3, v2, v1

    .line 125
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 130
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/api/model/Document;->mChildDocuments:[Lcom/google/android/finsky/api/model/Document;

    return-object v2
.end method

.method public getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    return-object v0
.end method

.method public getContainerViews()[Lcom/google/android/finsky/protos/Containers$ContainerView;
    .locals 1

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    return-object v0
.end method

.method public getContainerWithBannerTemplate()Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;
    .locals 1

    .prologue
    .line 1273
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    return-object v0
.end method

.method public getCoreContentHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 281
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 282
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_0

    .line 283
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    .line 285
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getCoreContentListUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 305
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 306
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_0

    .line 307
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCoreContent:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    .line 309
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getCreator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->creator:Ljava/lang/String;

    return-object v0
.end method

.method public getCreatorBadges()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .locals 1

    .prologue
    .line 1113
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    return-object v0
.end method

.method public getCreatorDoc()Lcom/google/android/finsky/api/model/Document;
    .locals 2

    .prologue
    .line 570
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCreatorDoc()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 571
    new-instance v0, Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v0, v1}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 573
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCrossSellBrowseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 514
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCrossSell()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getCrossSellSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getCrossSellHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCrossSell()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getCrossSellSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getCrossSellListUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 507
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasCrossSell()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getCrossSellSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getCrossSellSection()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDealOfTheDayInfo()Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;
    .locals 1

    .prologue
    .line 1219
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    return-object v0
.end method

.method public getDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 617
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getRawDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/FastHtmlParser;->fromHtml(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionReason()Ljava/lang/String;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1296
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v0

    .line 1297
    .local v0, "reasons":Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocumentV2$Reason;->descriptionHtml:Ljava/lang/String;

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getDetailsUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->detailsUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayArtist()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .locals 1

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->displayArtist:Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 342
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDocId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    return-object v0
.end method

.method public getDocumentType()I
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docType:I

    return v0
.end method

.method public getEditorialSeriesContainer()Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
    .locals 1

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    return-object v0
.end method

.method public getEmptyContainer()Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;
    .locals 1

    .prologue
    .line 1247
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    return-object v0
.end method

.method public getFirstCreatorBadge()Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .locals 2

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getFirstItemBadge()Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .locals 2

    .prologue
    .line 1121
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getFormattedPrice(I)Ljava/lang/String;
    .locals 2
    .param p1, "offerType"    # I

    .prologue
    .line 981
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    .line 982
    .local v0, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-eqz v1, :cond_0

    .line 983
    iget-object v1, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    .line 986
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFullDocid()Lcom/google/android/finsky/protos/Common$Docid;
    .locals 3

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/DocUtils;->createDocid(IILjava/lang/String;)Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    return-object v0
.end method

.method public getImages(I)Ljava/util/List;
    .locals 2
    .param p1, "imageType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Common$Image;",
            ">;"
        }
    .end annotation

    .prologue
    .line 741
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getImageTypeMap()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getItemBadges()[Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .locals 1

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    return-object v0
.end method

.method public getLinkAnnotation()Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 363
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMagazineDetails()Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;
    .locals 1

    .prologue
    .line 834
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->magazineDetails:Lcom/google/android/finsky/protos/DocDetails$MagazineDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMoreByBrowseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 450
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasMoreBy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getMoreBySectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getMoreByHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 436
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasMoreBy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getMoreBySectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getMoreByListUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasMoreBy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getMoreBySectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getNeutralDismissal()Lcom/google/android/finsky/protos/DocumentV2$Dismissal;
    .locals 1

    .prologue
    .line 1290
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasNeutralDismissal()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNextBannerInfo()Lcom/google/android/finsky/protos/DocumentV2$NextBanner;
    .locals 1

    .prologue
    .line 1223
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    return-object v0
.end method

.method public getNormalizedContentRating()I
    .locals 2

    .prologue
    .line 893
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    .line 894
    .local v0, "details":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->contentRating:I

    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;
    .locals 5
    .param p1, "offerType"    # I

    .prologue
    .line 927
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 928
    .local v3, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget v4, v3, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    if-ne v4, p1, :cond_0

    .line 932
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :goto_1
    return-object v3

    .line 927
    .restart local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 932
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getOfferNote()Ljava/lang/String;
    .locals 2

    .prologue
    .line 530
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 531
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    .line 532
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->offerNote:Ljava/lang/String;

    .line 534
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getOptimalDeviceClassWarning()Lcom/google/android/finsky/protos/DocAnnotations$Warning;
    .locals 1

    .prologue
    .line 378
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasOptimalDeviceClassWarning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    .line 381
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getOverflowLinks()[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;
    .locals 2

    .prologue
    .line 552
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 553
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    .line 554
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->overflowLink:[Lcom/google/android/finsky/protos/DocumentV2$OverflowLink;

    .line 556
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPersonDetails()Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    .locals 1

    .prologue
    .line 848
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->personDetails:Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPlusOneData()Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;
    .locals 2

    .prologue
    .line 1070
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 1071
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    .line 1072
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 1074
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPostPurchaseCrossSellSection()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionPurchaseCrossSell:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPrivacyPolicyUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 541
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 542
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    .line 543
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->privacyPolicyUrl:Ljava/lang/String;

    .line 545
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProductDetails()Lcom/google/android/finsky/protos/DocDetails$ProductDetails;
    .locals 1

    .prologue
    .line 869
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    return-object v0
.end method

.method public getPromotionalDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->promotionalDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseHistoryDetails()Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;
    .locals 1

    .prologue
    .line 1235
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    if-eqz v0, :cond_0

    .line 1237
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->purchaseHistoryDetails:Lcom/google/android/finsky/protos/DocAnnotations$PurchaseHistoryDetails;

    .line 1239
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRatingCount()J
    .locals 2

    .prologue
    .line 703
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    iget-wide v0, v0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->ratingsCount:J

    return-wide v0
.end method

.method public getRatingHistogram()[I
    .locals 6

    .prologue
    const/4 v3, 0x5

    .line 714
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasRating()Z

    move-result v2

    if-nez v2, :cond_0

    .line 715
    new-array v1, v3, [I

    fill-array-data v1, :array_0

    .line 722
    .local v1, "histogram":[I
    :goto_0
    return-object v1

    .line 718
    .end local v1    # "histogram":[I
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    .line 719
    .local v0, "agg":Lcom/google/android/finsky/protos/Rating$AggregateRating;
    new-array v1, v3, [I

    const/4 v2, 0x0

    iget-wide v4, v0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fiveStarRatings:J

    long-to-int v3, v4

    aput v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, v0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->fourStarRatings:J

    long-to-int v3, v4

    aput v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, v0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->threeStarRatings:J

    long-to-int v3, v4

    aput v3, v1, v2

    const/4 v2, 0x3

    iget-wide v4, v0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->twoStarRatings:J

    long-to-int v3, v4

    aput v3, v1, v2

    const/4 v2, 0x4

    iget-wide v4, v0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->oneStarRatings:J

    long-to-int v3, v4

    aput v3, v1, v2

    .line 722
    .restart local v1    # "histogram":[I
    goto :goto_0

    .line 715
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public getRawDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->descriptionHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getRawTranslatedDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->translatedDescriptionHtml:Ljava/lang/String;

    return-object v0
.end method

.method public getRecommendationsContainerWithHeaderTemplate()Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
    .locals 1

    .prologue
    .line 1206
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    return-object v0
.end method

.method public getRelatedBrowseUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 475
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasRelated()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getRelatedSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->browseUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getRelatedDocTypeHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 216
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 217
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_0

    .line 218
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    .line 220
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getRelatedDocTypeListUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 227
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 228
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_0

    .line 229
    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionRelatedDocType:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    .line 231
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getRelatedHeader()Ljava/lang/String;
    .locals 1

    .prologue
    .line 468
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasRelated()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getRelatedSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getRelatedListUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasRelated()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getRelatedSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getReleaseType()I
    .locals 2

    .prologue
    .line 1331
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->albumDetails:Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getReviewsUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->reviewsUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getServerLogsCookie()[B
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->serverLogsCookie:[B

    return-object v0
.end method

.method public getShareUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->shareUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getSongDetails()Lcom/google/android/finsky/protos/DocDetails$SongDetails;
    .locals 1

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->songDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStarRating()F
    .locals 1

    .prologue
    .line 696
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    iget v0, v0, Lcom/google/android/finsky/protos/Rating$AggregateRating;->starRating:F

    return v0
.end method

.method public getSubscriptionsList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1340
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1341
    const/4 v3, 0x0

    .line 1354
    :goto_0
    return-object v3

    .line 1344
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/api/model/Document;->mSubscriptionsList:Ljava/util/List;

    if-nez v3, :cond_1

    .line 1345
    new-instance v3, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v4, v4, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v4, v4, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v4, v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lcom/google/android/finsky/api/model/Document;->mSubscriptionsList:Ljava/util/List;

    .line 1348
    iget-object v3, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v3, v3, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v2, v3, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1349
    .local v2, "subscriptions":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_1

    .line 1350
    aget-object v0, v2, v1

    .line 1351
    .local v0, "child":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v3, p0, Lcom/google/android/finsky/api/model/Document;->mSubscriptionsList:Ljava/util/List;

    new-instance v4, Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v4, v0}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1349
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1354
    .end local v0    # "child":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v1    # "i":I
    .end local v2    # "subscriptions":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/api/model/Document;->mSubscriptionsList:Ljava/util/List;

    goto :goto_0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 594
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public getSuggestForRatingSection()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;
    .locals 2

    .prologue
    .line 522
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 523
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionSuggestForRating:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;
    .locals 1

    .prologue
    .line 1227
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    if-eqz v0, :cond_0

    .line 1229
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->suggestionReasons:Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    .line 1231
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTalentDetails()Lcom/google/android/finsky/protos/DocDetails$TalentDetails;
    .locals 1

    .prologue
    .line 855
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->talentDetails:Lcom/google/android/finsky/protos/DocDetails$TalentDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 587
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTrustedSourcePersonDoc()Lcom/google/android/finsky/api/model/Document;
    .locals 2

    .prologue
    .line 1175
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mTrustedSourceProfileDocument:Lcom/google/android/finsky/api/model/Document;

    if-nez v0, :cond_0

    .line 1176
    new-instance v0, Lcom/google/android/finsky/api/model/Document;

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;->source:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v0, v1}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    iput-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mTrustedSourceProfileDocument:Lcom/google/android/finsky/api/model/Document;

    .line 1179
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mTrustedSourceProfileDocument:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method public getTvEpisodeDetails()Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;
    .locals 1

    .prologue
    .line 820
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvEpisodeDetails:Lcom/google/android/finsky/protos/DocDetails$TvEpisodeDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvSeasonDetails()Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;
    .locals 1

    .prologue
    .line 827
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->tvSeasonDetails:Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVersionCode()I
    .locals 2

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    iget v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getVideoCredits()[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .locals 2

    .prologue
    .line 901
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getVideoDetails()Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    move-result-object v0

    .line 902
    .local v0, "details":Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->credit:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVideoDetails()Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    .locals 1

    .prologue
    .line 813
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;->videoDetails:Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVoucherIds()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasVoucherIds()Z

    move-result v6

    if-nez v6, :cond_1

    .line 640
    const/4 v2, 0x0

    .line 647
    :cond_0
    return-object v2

    .line 642
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v6, v6, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v5, v6, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    .line 643
    .local v5, "vouchers":[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    new-instance v2, Ljava/util/HashSet;

    array-length v6, v5

    invoke-direct {v2, v6}, Ljava/util/HashSet;-><init>(I)V

    .line 644
    .local v2, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object v0, v5

    .local v0, "arr$":[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 645
    .local v4, "voucher":Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;
    iget-object v6, v4, Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v6, v6, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendDocid:Ljava/lang/String;

    invoke-interface {v2, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 644
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getWarmWelcome()Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;
    .locals 1

    .prologue
    .line 1269
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->isWarmWelcome()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWarningMessage()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 1090
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1091
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 1092
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    iget-object v5, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v4, v5

    .line 1093
    .local v4, "warningCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_1

    .line 1095
    if-eqz v1, :cond_0

    .line 1096
    const-string v5, "<br />"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1098
    :cond_0
    iget-object v5, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->localizedMessage:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1093
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1100
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1101
    .local v3, "sequence":Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/finsky/utils/FastHtmlParser;->fromHtml(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    return-object v5
.end method

.method public getWhatsNew()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 679
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    if-nez v0, :cond_1

    .line 680
    :cond_0
    const-string v0, ""

    .line 682
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->recentChangesHtml:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/utils/FastHtmlParser;->fromHtml(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public getYouTubeWatchUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 909
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 910
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->backendUrl:Ljava/lang/String;

    .line 912
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasAntennaInfo()Z
    .locals 1

    .prologue
    .line 1150
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->seriesAntenna:Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBadgeContainer()Z
    .locals 1

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->docBadgeContainer:[Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasBodyOfWork()Z
    .locals 2

    .prologue
    .line 238
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 239
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionBodyOfWork:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasCensoring()Z
    .locals 2

    .prologue
    .line 1309
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    move-result-object v0

    .line 1310
    .local v0, "albumDetails":Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iget-boolean v1, v1, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->hasCensoring:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasContainerAnnotation()Z
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContainerViews()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 399
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasContainerAnnotation()Z

    move-result v1

    if-nez v1, :cond_1

    .line 402
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->containerView:[Lcom/google/android/finsky/protos/Containers$ContainerView;

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hasContainerWithBannerTemplate()Z
    .locals 1

    .prologue
    .line 1210
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->containerWithBanner:Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCreatorBadges()Z
    .locals 1

    .prologue
    .line 1105
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForCreator:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCreatorDoc()Z
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->creatorDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasCreatorRelatedContent()Z
    .locals 2

    .prologue
    .line 327
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 328
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->sectionMoreBy:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 330
    const/4 v1, 0x1

    .line 332
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasCrossSell()Z
    .locals 1

    .prologue
    .line 493
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getCrossSellSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDealOfTheDayInfo()Z
    .locals 1

    .prologue
    .line 1142
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->dealOfTheDay:Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDetails()Z
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasDocumentType()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDocType:Z

    return v0
.end method

.method public hasEditorialSeriesContainer()Z
    .locals 1

    .prologue
    .line 1187
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->editorialSeriesContainer:Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEmptyContainer()Z
    .locals 1

    .prologue
    .line 1243
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasImages(I)Z
    .locals 2
    .param p1, "imageType"    # I

    .prologue
    .line 745
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getImageTypeMap()Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hasItemBadges()Z
    .locals 1

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->badgeForDoc:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasLinkAnnotation()Z
    .locals 2

    .prologue
    .line 349
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 350
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v1, :cond_0

    .line 351
    const/4 v1, 0x1

    .line 353
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasMoreBy()Z
    .locals 1

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getMoreBySectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasNeutralDismissal()Z
    .locals 2

    .prologue
    .line 1282
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v0

    .line 1283
    .local v0, "suggestionReasons":Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->neutralDismissal:Lcom/google/android/finsky/protos/DocumentV2$Dismissal;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasNextBanner()Z
    .locals 1

    .prologue
    .line 1146
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->nextBanner:Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasOptimalDeviceClassWarning()Z
    .locals 2

    .prologue
    .line 370
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 371
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->optimalDeviceClassWarning:Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasPlusOneData()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1058
    iget-object v2, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    .line 1059
    .local v0, "annotations":Lcom/google/android/finsky/protos/DocumentV2$Annotations;
    if-eqz v0, :cond_0

    .line 1060
    iget-object v2, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v2, v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v2, v2, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 1062
    :cond_0
    return v1
.end method

.method public hasProductDetails()Z
    .locals 1

    .prologue
    .line 862
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->productDetails:Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRating()Z
    .locals 1

    .prologue
    .line 689
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->aggregateRating:Lcom/google/android/finsky/protos/Rating$AggregateRating;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReasons()Z
    .locals 1

    .prologue
    .line 1393
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;->reason:[Lcom/google/android/finsky/protos/DocumentV2$Reason;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationsContainerTemplate()Z
    .locals 1

    .prologue
    .line 1196
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainer:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRecommendationsContainerWithHeaderTemplate()Z
    .locals 1

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->template:Lcom/google/android/finsky/protos/DocumentV2$Template;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->recommendationsContainerWithHeader:Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasRelated()Z
    .locals 1

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getRelatedSectionMetadata()Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasReleaseType()Z
    .locals 2

    .prologue
    .line 1322
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    move-result-object v0

    .line 1323
    .local v0, "albumDetails":Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->releaseType:[I

    array-length v1, v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasReviewHistogramData()Z
    .locals 6

    .prologue
    .line 1380
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getRatingHistogram()[I

    move-result-object v2

    .line 1381
    .local v2, "histogram":[I
    move-object v0, v2

    .local v0, "arr$":[I
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget v1, v0, v3

    .line 1382
    .local v1, "count":I
    if-lez v1, :cond_0

    .line 1383
    const/4 v5, 0x1

    .line 1386
    .end local v1    # "count":I
    :goto_1
    return v5

    .line 1381
    .restart local v1    # "count":I
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1386
    .end local v1    # "count":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public hasSample()Z
    .locals 6

    .prologue
    .line 966
    iget-object v4, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v3, v4, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->offer:[Lcom/google/android/finsky/protos/Common$Offer;

    .line 967
    .local v3, "offers":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v2, v3

    .line 968
    .local v2, "offerCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 969
    aget-object v1, v3, v0

    .line 970
    .local v1, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget v4, v1, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    .line 971
    const/4 v4, 0x1

    .line 974
    .end local v1    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :goto_1
    return v4

    .line 968
    .restart local v1    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 974
    .end local v1    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public hasScreenshots()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1045
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v0

    .line 1047
    .local v0, "imageList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    if-eq v1, v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasSubscriptions()Z
    .locals 1

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->subscription:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasVoucherIds()Z
    .locals 1

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->voucherInfo:[Lcom/google/android/finsky/protos/DocumentV2$VoucherInfo;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWarningMessage()Z
    .locals 1

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->annotations:Lcom/google/android/finsky/protos/DocumentV2$Annotations;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Annotations;->warning:[Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasWhatsNew()Z
    .locals 1

    .prologue
    .line 671
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasDetails()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->recentChangesHtml:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isActionBanner()Z
    .locals 1

    .prologue
    .line 1251
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->actionBanner:Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAddToCirclesContainer()Z
    .locals 1

    .prologue
    .line 1162
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->addToCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$AddToCirclesContainer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAvailableIfOwned()Z
    .locals 1

    .prologue
    .line 959
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->availability:Lcom/google/android/finsky/protos/FilterRules$Availability;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/FilterRules$Availability;->availableIfOwned:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDismissable()Z
    .locals 1

    .prologue
    .line 1401
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasReasons()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasNeutralDismissal()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEmptyContainer()Z
    .locals 1

    .prologue
    .line 1183
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->emptyContainer:Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInProgressSeason()Z
    .locals 3

    .prologue
    .line 883
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getTvSeasonDetails()Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;

    move-result-object v0

    .line 884
    .local v0, "seasonDetails":Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;
    iget-object v1, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget v1, v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docType:I

    const/16 v2, 0x13

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->hasExpectedEpisodeCount:Z

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->episodeCount:I

    iget v2, v0, Lcom/google/android/finsky/protos/DocDetails$TvSeasonDetails;->expectedEpisodeCount:I

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMature()Z
    .locals 1

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->mature:Z

    return v0
.end method

.method public isMyCirclesContainer()Z
    .locals 1

    .prologue
    .line 1166
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->myCirclesContainer:Lcom/google/android/finsky/protos/DocumentV2$MyCirclesContainer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOrdered()Z
    .locals 1

    .prologue
    .line 1412
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    if-nez v0, :cond_0

    .line 1413
    const/4 v0, 0x0

    .line 1415
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->containerMetadata:Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered:Z

    goto :goto_0
.end method

.method public isRateAndSuggestCluster()Z
    .locals 1

    .prologue
    .line 1158
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateAndSuggestContainer:Lcom/google/android/finsky/protos/DocumentV2$RateAndSuggestContainer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRateCluster()Z
    .locals 1

    .prologue
    .line 1154
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->rateContainer:Lcom/google/android/finsky/protos/DocumentV2$RateContainer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSingleCardWithButton()Z
    .locals 1

    .prologue
    .line 1261
    const/4 v0, 0x0

    return v0
.end method

.method public isTrustedSourceContainer()Z
    .locals 1

    .prologue
    .line 1170
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->trustedSourceContainer:Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$TrustedSourceContainer;->source:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWarmWelcome()Z
    .locals 1

    .prologue
    .line 1265
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/api/model/Document;->getTemplate()Lcom/google/android/finsky/protos/DocumentV2$Template;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Template;->warmWelcome:Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public needsCheckoutFlow(I)Z
    .locals 2
    .param p1, "offerType"    # I

    .prologue
    .line 993
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    .line 994
    .local v0, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v0, :cond_0

    .line 995
    iget-boolean v1, v0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    .line 997
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 2
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 621
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iput-object p1, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->descriptionHtml:Ljava/lang/String;

    .line 622
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->hasDescriptionHtml:Z

    .line 623
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1450
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1451
    .local v0, "sb":Ljava/lang/StringBuilder;
    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1452
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1453
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1454
    const-string v1, " v="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v2

    iget v2, v2, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1456
    :cond_0
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1457
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1432
    iget-object v0, p0, Lcom/google/android/finsky/api/model/Document;->mDocument:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v0}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1433
    return-void
.end method
