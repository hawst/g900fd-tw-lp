.class public Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;
.super Lcom/google/android/finsky/layout/play/SingleLineContainer;
.source "AuthChallengeDialogDocumentInfoLayout.java"


# instance fields
.field private mPriceView:Landroid/widget/TextView;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/SingleLineContainer;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attributeSet"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/SingleLineContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/SingleLineContainer;->onFinishInflate()V

    .line 38
    const v0, 0x7f0a0235

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->mTitleView:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f0a0237

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->mPriceView:Landroid/widget/TextView;

    .line 40
    return-void
.end method

.method public setDocumentInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "price"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 43
    const/4 v0, 0x0

    .line 44
    .local v0, "showLayout":Z
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 45
    iget-object v3, p0, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object v3, p0, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 47
    const/4 v0, 0x1

    .line 52
    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 53
    iget-object v3, p0, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v3, p0, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    const/4 v0, 0x1

    .line 60
    :goto_1
    if-eqz v0, :cond_2

    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->setVisibility(I)V

    .line 61
    return-void

    .line 49
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 57
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/AuthChallengeDialogDocumentInfoLayout;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    move v1, v2

    .line 60
    goto :goto_2
.end method
