.class public final Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;
.super Lcom/google/protobuf/nano/MessageNano;
.source "LibraryUpdateProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/LibraryUpdateProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClientLibraryState"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;


# instance fields
.field public corpus:I

.field public hasCorpus:Z

.field public hasHashCodeSum:Z

.field public hasLibraryId:Z

.field public hasLibrarySize:Z

.field public hasServerToken:Z

.field public hashCodeSum:J

.field public libraryId:Ljava/lang/String;

.field public librarySize:I

.field public serverToken:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 949
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 950
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;

    .line 951
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;
    .locals 2

    .prologue
    .line 918
    sget-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;

    if-nez v0, :cond_1

    .line 919
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 921
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;

    if-nez v0, :cond_0

    .line 922
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;

    sput-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;

    .line 924
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 926
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->_emptyArray:[Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;

    return-object v0

    .line 924
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 954
    iput v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->corpus:I

    .line 955
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasCorpus:Z

    .line 956
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->libraryId:Ljava/lang/String;

    .line 957
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibraryId:Z

    .line 958
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->serverToken:[B

    .line 959
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasServerToken:Z

    .line 960
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hashCodeSum:J

    .line 961
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasHashCodeSum:Z

    .line 962
    iput v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->librarySize:I

    .line 963
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibrarySize:Z

    .line 964
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->cachedSize:I

    .line 965
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 991
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 992
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->corpus:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasCorpus:Z

    if-eqz v1, :cond_1

    .line 993
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->corpus:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 996
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasServerToken:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->serverToken:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 997
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->serverToken:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1000
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasHashCodeSum:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hashCodeSum:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 1001
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hashCodeSum:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1004
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibrarySize:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->librarySize:I

    if-eqz v1, :cond_7

    .line 1005
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->librarySize:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1008
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibraryId:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->libraryId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1009
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->libraryId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1012
    :cond_9
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1020
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1021
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1025
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1026
    :sswitch_0
    return-object p0

    .line 1031
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1032
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1044
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->corpus:I

    .line 1045
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasCorpus:Z

    goto :goto_0

    .line 1051
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->serverToken:[B

    .line 1052
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasServerToken:Z

    goto :goto_0

    .line 1056
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hashCodeSum:J

    .line 1057
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasHashCodeSum:Z

    goto :goto_0

    .line 1061
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->librarySize:I

    .line 1062
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibrarySize:Z

    goto :goto_0

    .line 1066
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->libraryId:Ljava/lang/String;

    .line 1067
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibraryId:Z

    goto :goto_0

    .line 1021
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    .line 1032
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 912
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 971
    iget v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->corpus:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasCorpus:Z

    if-eqz v0, :cond_1

    .line 972
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->corpus:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 974
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasServerToken:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->serverToken:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_3

    .line 975
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->serverToken:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 977
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasHashCodeSum:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hashCodeSum:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 978
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hashCodeSum:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 980
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibrarySize:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->librarySize:I

    if-eqz v0, :cond_7

    .line 981
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->librarySize:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 983
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibraryId:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->libraryId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 984
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->libraryId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 986
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 987
    return-void
.end method
