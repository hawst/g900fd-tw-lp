.class public final Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TileTemplate"
.end annotation


# instance fields
.field public colorTextArgb:Ljava/lang/String;

.field public colorThemeArgb:Ljava/lang/String;

.field public hasColorTextArgb:Z

.field public hasColorThemeArgb:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3653
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3654
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->clear()Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    .line 3655
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3658
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorThemeArgb:Ljava/lang/String;

    .line 3659
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->hasColorThemeArgb:Z

    .line 3660
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorTextArgb:Ljava/lang/String;

    .line 3661
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->hasColorTextArgb:Z

    .line 3662
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->cachedSize:I

    .line 3663
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3680
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3681
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->hasColorThemeArgb:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorThemeArgb:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3682
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3685
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->hasColorTextArgb:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorTextArgb:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3686
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorTextArgb:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3689
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 3697
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3698
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3702
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3703
    :sswitch_0
    return-object p0

    .line 3708
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorThemeArgb:Ljava/lang/String;

    .line 3709
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->hasColorThemeArgb:Z

    goto :goto_0

    .line 3713
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorTextArgb:Ljava/lang/String;

    .line 3714
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->hasColorTextArgb:Z

    goto :goto_0

    .line 3698
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3628
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3669
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->hasColorThemeArgb:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorThemeArgb:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3670
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorThemeArgb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3672
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->hasColorTextArgb:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorTextArgb:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3673
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV2$TileTemplate;->colorTextArgb:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3675
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3676
    return-void
.end method
