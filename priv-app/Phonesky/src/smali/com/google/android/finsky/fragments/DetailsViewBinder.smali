.class public abstract Lcom/google/android/finsky/fragments/DetailsViewBinder;
.super Ljava/lang/Object;
.source "DetailsViewBinder.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field protected mDoc:Lcom/google/android/finsky/api/model/Document;

.field protected mHeaderLayoutId:I

.field protected mInflater:Landroid/view/LayoutInflater;

.field protected mLayout:Landroid/view/View;

.field protected mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

.field protected mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bind(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "headerString"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mLayout:Landroid/view/View;

    .line 67
    iput-object p2, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 69
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-virtual {p0, p1, p3, v0}, Lcom/google/android/finsky/fragments/DetailsViewBinder;->bind(Landroid/view/View;Ljava/lang/String;I)V

    .line 70
    return-void
.end method

.method public bind(Landroid/view/View;Ljava/lang/String;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "headerString"    # Ljava/lang/String;
    .param p3, "headerBackendId"    # I

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mLayout:Landroid/view/View;

    .line 75
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/fragments/DetailsViewBinder;->setupHeader(Ljava/lang/String;I)V

    .line 76
    return-void
.end method

.method public init(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "api"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mContext:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 55
    iput-object p3, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mInflater:Landroid/view/LayoutInflater;

    .line 57
    const v0, 0x7f0a0173

    iput v0, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mHeaderLayoutId:I

    .line 58
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mLayoutSwitcher:Lcom/google/android/finsky/layout/LayoutSwitcher;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/LayoutSwitcher;->switchToErrorMode(Ljava/lang/String;)V

    .line 105
    :cond_0
    return-void
.end method

.method protected setupHeader(Ljava/lang/String;I)V
    .locals 3
    .param p1, "headerString"    # Ljava/lang/String;
    .param p2, "headerBackendId"    # I

    .prologue
    .line 82
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mLayout:Landroid/view/View;

    iget v2, p0, Lcom/google/android/finsky/fragments/DetailsViewBinder;->mHeaderLayoutId:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 83
    .local v0, "headerView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    :cond_0
    return-void
.end method
