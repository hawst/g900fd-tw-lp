.class public final Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "UserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/UserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UserActivitySettingsResponse"
.end annotation


# instance fields
.field public currentUser:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public hasOptIn:Z

.field public hasSettingsDescription:Z

.field public hasSettingsTitle:Z

.field public hasSettingsTosHtml:Z

.field public optIn:Z

.field public settingsDescription:Ljava/lang/String;

.field public settingsTitle:Ljava/lang/String;

.field public settingsTosHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->clear()Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    .line 133
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->currentUser:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTitle:Ljava/lang/String;

    .line 138
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsTitle:Z

    .line 139
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsDescription:Ljava/lang/String;

    .line 140
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsDescription:Z

    .line 141
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTosHtml:Ljava/lang/String;

    .line 142
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsTosHtml:Z

    .line 143
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->optIn:Z

    .line 144
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasOptIn:Z

    .line 145
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->cachedSize:I

    .line 146
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 172
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 173
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->currentUser:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v1, :cond_0

    .line 174
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->currentUser:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsTitle:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTitle:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 178
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTitle:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsDescription:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsDescription:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 182
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsDescription:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsTosHtml:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTosHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 186
    :cond_5
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTosHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasOptIn:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->optIn:Z

    if-eqz v1, :cond_8

    .line 190
    :cond_7
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->optIn:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_8
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 201
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 202
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 206
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 207
    :sswitch_0
    return-object p0

    .line 212
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->currentUser:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v1, :cond_1

    .line 213
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->currentUser:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 215
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->currentUser:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 219
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTitle:Ljava/lang/String;

    .line 220
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsTitle:Z

    goto :goto_0

    .line 224
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsDescription:Ljava/lang/String;

    .line 225
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsDescription:Z

    goto :goto_0

    .line 229
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTosHtml:Ljava/lang/String;

    .line 230
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsTosHtml:Z

    goto :goto_0

    .line 234
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->optIn:Z

    .line 235
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasOptIn:Z

    goto :goto_0

    .line 202
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->currentUser:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_0

    .line 153
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->currentUser:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 155
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsTitle:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTitle:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 156
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 158
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsDescription:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsDescription:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 159
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsDescription:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 161
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasSettingsTosHtml:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTosHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 162
    :cond_5
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->settingsTosHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 164
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->hasOptIn:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->optIn:Z

    if-eqz v0, :cond_8

    .line 165
    :cond_7
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/UserActivity$UserActivitySettingsResponse;->optIn:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 167
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 168
    return-void
.end method
