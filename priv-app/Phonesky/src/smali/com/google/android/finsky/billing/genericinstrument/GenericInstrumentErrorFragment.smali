.class public Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;
.super Lcom/google/android/finsky/fragments/LoggingFragment;
.source "GenericInstrumentErrorFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mErrorDescription:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;Z)Landroid/support/v4/app/Fragment;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "errorHtml"    # Ljava/lang/String;
    .param p2, "isTerminalError"    # Z

    .prologue
    .line 34
    new-instance v1, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;-><init>()V

    .line 35
    .local v1, "fragment":Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 36
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    const-string v2, "GenericInstrumentErrorFragment.error_html"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v2, "GenericInstrumentErrorFragment.is_terminal_error"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 39
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->setArguments(Landroid/os/Bundle;)V

    .line 40
    return-object v1
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 93
    const/16 v0, 0x5dd

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 77
    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->mButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v1, :cond_0

    .line 78
    const/16 v1, 0x5de

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->logClickEvent(I)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;

    .line 81
    .local v0, "hostFragment":Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "GenericInstrumentErrorFragment.is_terminal_error"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->acknowledgeTerminalError()V

    .line 87
    .end local v0    # "hostFragment":Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;
    :cond_0
    :goto_0
    return-void

    .line 84
    .restart local v0    # "hostFragment":Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->retryAfterError()V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 46
    const v4, 0x7f040033

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 49
    .local v3, "mainView":Landroid/view/ViewGroup;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 50
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v4, "GenericInstrumentErrorFragment.error_html"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 51
    .local v2, "errorHtml":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 52
    const-string v4, "Missing expected error message."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 53
    const v4, 0x7f0c012e

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 55
    :cond_0
    const v4, 0x7f0a00bb

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->mErrorDescription:Landroid/widget/TextView;

    .line 56
    iget-object v4, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->mErrorDescription:Landroid/widget/TextView;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v4, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->mErrorDescription:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 59
    const-string v4, "GenericInstrumentErrorFragment.is_terminal_error"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const v1, 0x7f0c02a0

    .line 61
    .local v1, "buttonTextResourceId":I
    :goto_0
    const v4, 0x7f0a00f8

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v4, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->mButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 62
    iget-object v4, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->mButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v5, 0x3

    invoke-virtual {v4, v5, v1, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 64
    return-object v3

    .line 59
    .end local v1    # "buttonTextResourceId":I
    :cond_1
    const v1, 0x7f0c012f

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 69
    invoke-super {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;->onResume()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->mErrorDescription:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentErrorFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 73
    return-void
.end method
