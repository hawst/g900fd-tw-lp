.class public Lcom/google/android/finsky/layout/HistogramView;
.super Landroid/widget/RelativeLayout;
.source "HistogramView.java"


# instance fields
.field private mAverageValue:Landroid/widget/TextView;

.field private mFloatFormatter:Ljava/text/NumberFormat;

.field private final mHalfSeparatorThickness:I

.field private mHistogramTable:Lcom/google/android/finsky/layout/HistogramTable;

.field private mIntegerFormatter:Ljava/text/NumberFormat;

.field private final mNeedsHeightCorrection:Z

.field private mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

.field private mRatingCount:Landroid/widget/TextView;

.field private final mSeparatorInset:I

.field private final mSeparatorPaint:Landroid/graphics/Paint;

.field private final mShowsBottomSeparator:Z

.field private mSummaryBox:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/HistogramView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 48
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mFloatFormatter:Ljava/text/NumberFormat;

    .line 51
    iget-object v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mFloatFormatter:Ljava/text/NumberFormat;

    invoke-virtual {v3, v5}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 52
    iget-object v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mFloatFormatter:Ljava/text/NumberFormat;

    invoke-virtual {v3, v5}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 53
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mIntegerFormatter:Ljava/text/NumberFormat;

    .line 55
    sget-object v3, Lcom/android/vending/R$styleable;->HistogramView:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 56
    .local v0, "attrArray":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mNeedsHeightCorrection:Z

    .line 58
    invoke-virtual {v0, v5, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mShowsBottomSeparator:Z

    .line 60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 62
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/HistogramView;->setWillNotDraw(Z)V

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 65
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f0b0052

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 67
    .local v2, "separatorThickness":I
    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mHalfSeparatorThickness:I

    .line 68
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mSeparatorPaint:Landroid/graphics/Paint;

    .line 69
    iget-object v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mSeparatorPaint:Landroid/graphics/Paint;

    const v4, 0x7f090053

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 70
    iget-object v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mSeparatorPaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 71
    const v3, 0x7f0b00ec

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mSeparatorInset:I

    .line 72
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;)V
    .locals 11
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 122
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasReviewHistogramData()Z

    move-result v4

    if-nez v4, :cond_0

    .line 124
    const-string v4, "No histogram data received from server"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    const/16 v4, 0x8

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/HistogramView;->setVisibility(I)V

    .line 146
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-virtual {p0, v9}, Lcom/google/android/finsky/layout/HistogramView;->setVisibility(I)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HistogramView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 133
    .local v1, "res":Landroid/content/res/Resources;
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getRatingCount()J

    move-result-wide v2

    .line 134
    .local v2, "ratingCount":J
    iget-object v4, p0, Lcom/google/android/finsky/layout/HistogramView;->mRatingCount:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/google/android/finsky/layout/HistogramView;->mIntegerFormatter:Ljava/text/NumberFormat;

    invoke-virtual {v5, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v4, p0, Lcom/google/android/finsky/layout/HistogramView;->mRatingCount:Landroid/widget/TextView;

    const v5, 0x7f100011

    long-to-int v6, v2

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v1, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 139
    iget-object v4, p0, Lcom/google/android/finsky/layout/HistogramView;->mFloatFormatter:Ljava/text/NumberFormat;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getStarRating()F

    move-result v5

    float-to-double v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "formattedRating":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/finsky/layout/HistogramView;->mAverageValue:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v4, p0, Lcom/google/android/finsky/layout/HistogramView;->mAverageValue:Landroid/widget/TextView;

    const v5, 0x7f0c0404

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v0, v6, v9

    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 144
    iget-object v4, p0, Lcom/google/android/finsky/layout/HistogramView;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getStarRating()F

    move-result v5

    invoke-virtual {v4, v5}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 145
    iget-object v4, p0, Lcom/google/android/finsky/layout/HistogramView;->mHistogramTable:Lcom/google/android/finsky/layout/HistogramTable;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getRatingHistogram()[I

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/layout/HistogramTable;->setHistogram([I)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 114
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/HistogramView;->mShowsBottomSeparator:Z

    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HistogramView;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/HistogramView;->mHalfSeparatorThickness:I

    sub-int v6, v0, v1

    .line 116
    .local v6, "bottomY":I
    iget v0, p0, Lcom/google/android/finsky/layout/HistogramView;->mSeparatorInset:I

    int-to-float v1, v0

    int-to-float v2, v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HistogramView;->getWidth()I

    move-result v0

    iget v3, p0, Lcom/google/android/finsky/layout/HistogramView;->mSeparatorInset:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/finsky/layout/HistogramView;->mSeparatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 119
    .end local v6    # "bottomY":I
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 78
    const v0, 0x7f0a0332

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/HistogramView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/HistogramView;->mSummaryBox:Landroid/widget/LinearLayout;

    .line 79
    const v0, 0x7f0a0369

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/HistogramView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/HistogramTable;

    iput-object v0, p0, Lcom/google/android/finsky/layout/HistogramView;->mHistogramTable:Lcom/google/android/finsky/layout/HistogramTable;

    .line 80
    const v0, 0x7f0a01aa

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/HistogramView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/HistogramView;->mAverageValue:Landroid/widget/TextView;

    .line 81
    iget-object v0, p0, Lcom/google/android/finsky/layout/HistogramView;->mSummaryBox:Landroid/widget/LinearLayout;

    const v1, 0x7f0a0333

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/HistogramView;->mRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 82
    iget-object v0, p0, Lcom/google/android/finsky/layout/HistogramView;->mSummaryBox:Landroid/widget/LinearLayout;

    const v1, 0x7f0a0334

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/HistogramView;->mRatingCount:Landroid/widget/TextView;

    .line 83
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 92
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 93
    iget-boolean v5, p0, Lcom/google/android/finsky/layout/HistogramView;->mNeedsHeightCorrection:Z

    if-eqz v5, :cond_0

    .line 94
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 95
    .local v4, "width":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/HistogramView;->mAverageValue:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/HistogramView;->mSummaryBox:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v6

    add-int v2, v5, v6

    .line 96
    .local v2, "leftHeight":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/HistogramView;->mHistogramTable:Lcom/google/android/finsky/layout/HistogramTable;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/HistogramTable;->getMeasuredHeight()I

    move-result v3

    .line 97
    .local v3, "rightHeight":I
    const/4 v0, 0x0

    .line 98
    .local v0, "baseHeight":I
    if-le v2, v3, :cond_1

    .line 99
    iget-object v5, p0, Lcom/google/android/finsky/layout/HistogramView;->mHistogramTable:Lcom/google/android/finsky/layout/HistogramTable;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/HistogramTable;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/HistogramView;->mHistogramTable:Lcom/google/android/finsky/layout/HistogramTable;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/HistogramTable;->getBaseline()I

    move-result v6

    sub-int/2addr v5, v6

    add-int v0, v2, v5

    .line 105
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HistogramView;->getPaddingBottom()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/HistogramView;->getPaddingTop()I

    move-result v6

    add-int v1, v5, v6

    .line 106
    .local v1, "height":I
    invoke-virtual {p0, v4, v1}, Lcom/google/android/finsky/layout/HistogramView;->setMeasuredDimension(II)V

    .line 108
    .end local v0    # "baseHeight":I
    .end local v1    # "height":I
    .end local v2    # "leftHeight":I
    .end local v3    # "rightHeight":I
    .end local v4    # "width":I
    :cond_0
    return-void

    .line 102
    .restart local v0    # "baseHeight":I
    .restart local v2    # "leftHeight":I
    .restart local v3    # "rightHeight":I
    .restart local v4    # "width":I
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/HistogramView;->mRatingCount:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/HistogramView;->mRatingCount:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getBaseline()I

    move-result v6

    sub-int/2addr v5, v6

    add-int v0, v3, v5

    goto :goto_0
.end method
