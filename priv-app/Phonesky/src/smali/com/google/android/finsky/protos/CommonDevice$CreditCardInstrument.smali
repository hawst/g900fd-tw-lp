.class public final Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CreditCardInstrument"
.end annotation


# instance fields
.field public escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

.field public escrowHandle:Ljava/lang/String;

.field public expirationMonth:I

.field public expirationYear:I

.field public hasEscrowHandle:Z

.field public hasExpirationMonth:Z

.field public hasExpirationYear:Z

.field public hasLastDigits:Z

.field public hasType:Z

.field public lastDigits:Ljava/lang/String;

.field public type:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 792
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 793
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->clear()Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    .line 794
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 797
    iput v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->type:I

    .line 798
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasType:Z

    .line 799
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowHandle:Ljava/lang/String;

    .line 800
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasEscrowHandle:Z

    .line 801
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->lastDigits:Ljava/lang/String;

    .line 802
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasLastDigits:Z

    .line 803
    iput v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationMonth:I

    .line 804
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationMonth:Z

    .line 805
    iput v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationYear:I

    .line 806
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationYear:Z

    .line 807
    invoke-static {}, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->emptyArray()[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    .line 808
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->cachedSize:I

    .line 809
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 843
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 844
    .local v2, "size":I
    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->type:I

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasType:Z

    if-eqz v3, :cond_1

    .line 845
    :cond_0
    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->type:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 848
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasEscrowHandle:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowHandle:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 849
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowHandle:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 852
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasLastDigits:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->lastDigits:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 853
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->lastDigits:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 856
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationMonth:Z

    if-nez v3, :cond_6

    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationMonth:I

    if-eqz v3, :cond_7

    .line 857
    :cond_6
    const/4 v3, 0x4

    iget v4, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationMonth:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 860
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationYear:Z

    if-nez v3, :cond_8

    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationYear:I

    if-eqz v3, :cond_9

    .line 861
    :cond_8
    const/4 v3, 0x5

    iget v4, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationYear:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 864
    :cond_9
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    array-length v3, v3

    if-lez v3, :cond_b

    .line 865
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    array-length v3, v3

    if-ge v1, v3, :cond_b

    .line 866
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    aget-object v0, v3, v1

    .line 867
    .local v0, "element":Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    if-eqz v0, :cond_a

    .line 868
    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 865
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 873
    .end local v0    # "element":Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    .end local v1    # "i":I
    :cond_b
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 881
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 882
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 886
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 887
    :sswitch_0
    return-object p0

    .line 892
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 893
    .local v4, "value":I
    sparse-switch v4, :sswitch_data_1

    goto :goto_0

    .line 904
    :sswitch_2
    iput v4, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->type:I

    .line 905
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasType:Z

    goto :goto_0

    .line 911
    .end local v4    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowHandle:Ljava/lang/String;

    .line 912
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasEscrowHandle:Z

    goto :goto_0

    .line 916
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->lastDigits:Ljava/lang/String;

    .line 917
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasLastDigits:Z

    goto :goto_0

    .line 921
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationMonth:I

    .line 922
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationMonth:Z

    goto :goto_0

    .line 926
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationYear:I

    .line 927
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationYear:Z

    goto :goto_0

    .line 931
    :sswitch_7
    const/16 v6, 0x32

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 933
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    if-nez v6, :cond_2

    move v1, v5

    .line 934
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    .line 936
    .local v2, "newArray":[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    if-eqz v1, :cond_1

    .line 937
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 939
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 940
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;-><init>()V

    aput-object v6, v2, v1

    .line 941
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 942
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 939
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 933
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    array-length v1, v6

    goto :goto_1

    .line 945
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;-><init>()V

    aput-object v6, v2, v1

    .line 946
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 947
    iput-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    goto :goto_0

    .line 882
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x28 -> :sswitch_6
        0x32 -> :sswitch_7
    .end sparse-switch

    .line 893
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0xa -> :sswitch_2
        0x1b -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 752
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 815
    iget v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->type:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasType:Z

    if-eqz v2, :cond_1

    .line 816
    :cond_0
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->type:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 818
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasEscrowHandle:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowHandle:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 819
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowHandle:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 821
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasLastDigits:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->lastDigits:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 822
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->lastDigits:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 824
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationMonth:Z

    if-nez v2, :cond_6

    iget v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationMonth:I

    if-eqz v2, :cond_7

    .line 825
    :cond_6
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationMonth:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 827
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->hasExpirationYear:Z

    if-nez v2, :cond_8

    iget v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationYear:I

    if-eqz v2, :cond_9

    .line 828
    :cond_8
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->expirationYear:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 830
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 831
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 832
    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CreditCardInstrument;->escrowEfeParam:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    aget-object v0, v2, v1

    .line 833
    .local v0, "element":Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    if-eqz v0, :cond_a

    .line 834
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 831
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 838
    .end local v0    # "element":Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    .end local v1    # "i":I
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 839
    return-void
.end method
