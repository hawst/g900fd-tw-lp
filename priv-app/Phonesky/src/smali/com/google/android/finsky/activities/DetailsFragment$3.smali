.class Lcom/google/android/finsky/activities/DetailsFragment$3;
.super Landroid/support/v4/app/SharedElementCallback;
.source "DetailsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsFragment;->configureEnterSharedElementCallback()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mIsOpeningTarget:Z

.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsFragment;)V
    .locals 0

    .prologue
    .line 1473
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    invoke-direct {p0}, Landroid/support/v4/app/SharedElementCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapSharedElements(Ljava/util/List;Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1577
    .local p1, "names":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Landroid/view/View;>;"
    iget-boolean v7, p0, Lcom/google/android/finsky/activities/DetailsFragment$3;->mIsOpeningTarget:Z

    if-nez v7, :cond_0

    .line 1578
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 1579
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1580
    .local v6, "sharedElementName":Ljava/lang/String;
    const-string v7, "transition_card_details:cover:"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1582
    invoke-interface {p2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 1583
    .local v5, "sharedCover":Landroid/view/View;
    if-nez v5, :cond_1

    .line 1610
    .end local v1    # "i":I
    .end local v5    # "sharedCover":Landroid/view/View;
    .end local v6    # "sharedElementName":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 1588
    .restart local v1    # "i":I
    .restart local v5    # "sharedCover":Landroid/view/View;
    .restart local v6    # "sharedElementName":Ljava/lang/String;
    :cond_1
    const/4 v7, 0x2

    new-array v2, v7, [I

    .line 1589
    .local v2, "location":[I
    invoke-virtual {v5, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1590
    const/4 v7, 0x1

    aget v7, v2, v7

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int v0, v7, v8

    .line 1592
    .local v0, "coverBottom":I
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    invoke-virtual {v7}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0a00c9

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ScrollView;

    .line 1594
    .local v4, "scrollView":Landroid/widget/ScrollView;
    invoke-virtual {v4}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v3

    .line 1596
    .local v3, "scrollPosition":I
    if-gt v0, v3, :cond_0

    .line 1602
    invoke-interface {p2, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1603
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    goto :goto_1

    .line 1578
    .end local v0    # "coverBottom":I
    .end local v2    # "location":[I
    .end local v3    # "scrollPosition":I
    .end local v4    # "scrollView":Landroid/widget/ScrollView;
    .end local v5    # "sharedCover":Landroid/view/View;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onSharedElementEnd(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p3, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v1, 0x0

    .line 1568
    iput-boolean v1, p0, Lcom/google/android/finsky/activities/DetailsFragment$3;->mIsOpeningTarget:Z

    .line 1569
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/DetailsFragment;->access$900(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1570
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/DetailsFragment;->access$900(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->unfreezeImage(Z)V

    .line 1571
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v0, v1}, Lcom/google/android/finsky/activities/DetailsFragment;->access$902(Lcom/google/android/finsky/activities/DetailsFragment;Lcom/google/android/play/image/FifeImageView;)Lcom/google/android/play/image/FifeImageView;

    .line 1573
    :cond_0
    return-void
.end method

.method public onSharedElementStart(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1479
    .local p1, "sharedElementNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "sharedElements":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .local p3, "sharedElementSnapshots":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/activities/DetailsFragment;->getView()Landroid/view/View;

    move-result-object v16

    .line 1480
    .local v16, "view":Landroid/view/View;
    if-nez v16, :cond_1

    const/16 v17, 0x1

    :goto_0
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/finsky/activities/DetailsFragment$3;->mIsOpeningTarget:Z

    .line 1482
    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v17

    if-eqz v17, :cond_2

    .line 1563
    :cond_0
    :goto_1
    return-void

    .line 1480
    :cond_1
    const/16 v17, 0x0

    goto :goto_0

    .line 1486
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->mIsOpeningTarget:Z

    move/from16 v17, v0

    if-eqz v17, :cond_7

    .line 1487
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v5, v0, :cond_0

    .line 1488
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 1489
    .local v10, "sharedElementName":Ljava/lang/String;
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 1490
    .local v9, "sharedElement":Landroid/view/View;
    const-string v17, "transition_card_details:cover:"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v18, v0

    move-object/from16 v17, v9

    check-cast v17, Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    # setter for: Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;
    invoke-static {v0, v1}, Lcom/google/android/finsky/activities/DetailsFragment;->access$902(Lcom/google/android/finsky/activities/DetailsFragment;Lcom/google/android/play/image/FifeImageView;)Lcom/google/android/play/image/FifeImageView;

    .line 1495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;
    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/activities/DetailsFragment;->access$900(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v17

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/play/image/FifeImageView;->setAlpha(F)V

    .line 1499
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v6, v0, [I

    .line 1500
    .local v6, "loc":[I
    const/4 v11, 0x0

    .line 1501
    .local v11, "sourceOwnLeft":I
    const/4 v12, 0x0

    .line 1502
    .local v12, "sourceOwnTop":I
    move-object v7, v9

    .line 1503
    .local v7, "parent":Landroid/view/View;
    :goto_3
    if-eqz v7, :cond_4

    .line 1504
    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v8

    .line 1505
    .local v8, "parentId":I
    const v17, 0x7f0a01b3

    move/from16 v0, v17

    if-ne v8, v0, :cond_3

    .line 1506
    invoke-virtual {v7, v6}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1507
    const/16 v17, 0x0

    aget v11, v6, v17

    .line 1508
    const/16 v17, 0x1

    aget v12, v6, v17

    .line 1509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v18

    # setter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSourceWidth:I
    invoke-static/range {v17 .. v18}, Lcom/google/android/finsky/activities/DetailsFragment;->access$502(Lcom/google/android/finsky/activities/DetailsFragment;I)I

    .line 1510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v18

    # setter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSourceHeight:I
    invoke-static/range {v17 .. v18}, Lcom/google/android/finsky/activities/DetailsFragment;->access$702(Lcom/google/android/finsky/activities/DetailsFragment;I)I

    .line 1512
    :cond_3
    const v17, 0x7f0a025c

    move/from16 v0, v17

    if-ne v8, v0, :cond_6

    .line 1518
    .end local v8    # "parentId":I
    :cond_4
    invoke-virtual {v7, v6}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1519
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aget v18, v6, v18

    sub-int v18, v11, v18

    # setter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSourceLeft:I
    invoke-static/range {v17 .. v18}, Lcom/google/android/finsky/activities/DetailsFragment;->access$402(Lcom/google/android/finsky/activities/DetailsFragment;I)I

    .line 1520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    aget v18, v6, v18

    sub-int v18, v12, v18

    # setter for: Lcom/google/android/finsky/activities/DetailsFragment;->mSourceTop:I
    invoke-static/range {v17 .. v18}, Lcom/google/android/finsky/activities/DetailsFragment;->access$602(Lcom/google/android/finsky/activities/DetailsFragment;I)I

    .line 1487
    .end local v6    # "loc":[I
    .end local v7    # "parent":Landroid/view/View;
    .end local v11    # "sourceOwnLeft":I
    .end local v12    # "sourceOwnTop":I
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 1516
    .restart local v6    # "loc":[I
    .restart local v7    # "parent":Landroid/view/View;
    .restart local v8    # "parentId":I
    .restart local v11    # "sourceOwnLeft":I
    .restart local v12    # "sourceOwnTop":I
    :cond_6
    invoke-virtual {v7}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    .end local v7    # "parent":Landroid/view/View;
    check-cast v7, Landroid/view/View;

    .line 1517
    .restart local v7    # "parent":Landroid/view/View;
    goto :goto_3

    .line 1530
    .end local v5    # "i":I
    .end local v6    # "loc":[I
    .end local v7    # "parent":Landroid/view/View;
    .end local v8    # "parentId":I
    .end local v9    # "sharedElement":Landroid/view/View;
    .end local v10    # "sharedElementName":Ljava/lang/String;
    .end local v11    # "sourceOwnLeft":I
    .end local v12    # "sourceOwnTop":I
    :cond_7
    const/4 v13, 0x0

    .line 1531
    .local v13, "titleCover":Lcom/google/android/play/image/FifeImageView;
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_4
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v5, v0, :cond_8

    .line 1532
    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1533
    .local v4, "elementName":Ljava/lang/String;
    const-string v17, "transition_card_details:cover:"

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 1535
    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "titleCover":Lcom/google/android/play/image/FifeImageView;
    check-cast v13, Lcom/google/android/play/image/FifeImageView;

    .line 1540
    .end local v4    # "elementName":Ljava/lang/String;
    .restart local v13    # "titleCover":Lcom/google/android/play/image/FifeImageView;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mOriginalCoverBitmap:Landroid/graphics/Bitmap;
    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/activities/DetailsFragment;->access$1000(Lcom/google/android/finsky/activities/DetailsFragment;)Landroid/graphics/Bitmap;

    move-result-object v17

    if-eqz v17, :cond_9

    if-eqz v13, :cond_9

    .line 1543
    invoke-virtual {v13}, Lcom/google/android/play/image/FifeImageView;->freezeImage()V

    .line 1544
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mOriginalCoverBitmap:Landroid/graphics/Bitmap;
    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/activities/DetailsFragment;->access$1000(Lcom/google/android/finsky/activities/DetailsFragment;)Landroid/graphics/Bitmap;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1546
    invoke-virtual {v13}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v17

    const/high16 v18, 0x40000000    # 2.0f

    invoke-static/range {v17 .. v18}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 1549
    .local v15, "titleMeasureWidthSpec":I
    invoke-virtual {v13}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v17

    const/high16 v18, 0x40000000    # 2.0f

    invoke-static/range {v17 .. v18}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v14

    .line 1552
    .local v14, "titleMeasureHeightSpec":I
    invoke-virtual {v13, v15, v14}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 1553
    invoke-virtual {v13}, Lcom/google/android/play/image/FifeImageView;->getLeft()I

    move-result v17

    invoke-virtual {v13}, Lcom/google/android/play/image/FifeImageView;->getTop()I

    move-result v18

    invoke-virtual {v13}, Lcom/google/android/play/image/FifeImageView;->getRight()I

    move-result v19

    invoke-virtual {v13}, Lcom/google/android/play/image/FifeImageView;->getBottom()I

    move-result v20

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v13, v0, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 1556
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;
    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/activities/DetailsFragment;->access$900(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v17

    if-eqz v17, :cond_9

    .line 1557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;
    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/activities/DetailsFragment;->access$900(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mOriginalCoverBitmap:Landroid/graphics/Bitmap;
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/activities/DetailsFragment;->access$1000(Lcom/google/android/finsky/activities/DetailsFragment;)Landroid/graphics/Bitmap;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Lcom/google/android/play/image/FifeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1558
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    # getter for: Lcom/google/android/finsky/activities/DetailsFragment;->mCoverInSource:Lcom/google/android/play/image/FifeImageView;
    invoke-static/range {v17 .. v17}, Lcom/google/android/finsky/activities/DetailsFragment;->access$900(Lcom/google/android/finsky/activities/DetailsFragment;)Lcom/google/android/play/image/FifeImageView;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/play/image/FifeImageView;->freezeImage()V

    .line 1562
    .end local v14    # "titleMeasureHeightSpec":I
    .end local v15    # "titleMeasureWidthSpec":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/DetailsFragment$3;->this$0:Lcom/google/android/finsky/activities/DetailsFragment;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    # setter for: Lcom/google/android/finsky/activities/DetailsFragment;->mOriginalCoverBitmap:Landroid/graphics/Bitmap;
    invoke-static/range {v17 .. v18}, Lcom/google/android/finsky/activities/DetailsFragment;->access$1002(Lcom/google/android/finsky/activities/DetailsFragment;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto/16 :goto_1

    .line 1531
    .restart local v4    # "elementName":Ljava/lang/String;
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4
.end method
