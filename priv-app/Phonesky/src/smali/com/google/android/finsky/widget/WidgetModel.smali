.class public Lcom/google/android/finsky/widget/WidgetModel;
.super Ljava/lang/Object;
.source "WidgetModel.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/widget/WidgetModel$PromotionalItem;,
        Lcom/google/android/finsky/widget/WidgetModel$ImageSelector;,
        Lcom/google/android/finsky/widget/WidgetModel$RefreshListener;
    }
.end annotation


# instance fields
.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private final mDocumentTypes:[I

.field private final mImageHeightResource:I

.field private final mImageSelector:Lcom/google/android/finsky/widget/WidgetModel$ImageSelector;

.field private final mItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/widget/WidgetModel$PromotionalItem;",
            ">;"
        }
    .end annotation
.end field

.field private mList:Lcom/google/android/finsky/api/model/DfeList;

.field private mListener:Lcom/google/android/finsky/widget/WidgetModel$RefreshListener;

.field private mLoadedImagesSoFar:I

.field private mMaxHeight:I

.field private final mMaxItems:I

.field private mSize:I

.field private mUpdatePending:Z


# direct methods
.method public constructor <init>(Lcom/google/android/play/image/BitmapLoader;[ILcom/google/android/finsky/widget/WidgetModel$ImageSelector;II)V
    .locals 1
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "validDocumentTypes"    # [I
    .param p3, "imageSelector"    # Lcom/google/android/finsky/widget/WidgetModel$ImageSelector;
    .param p4, "imageHeightResource"    # I
    .param p5, "maxItems"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mLoadedImagesSoFar:I

    .line 43
    iput p5, p0, Lcom/google/android/finsky/widget/WidgetModel;->mMaxItems:I

    .line 44
    invoke-static {p5}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mItems:Ljava/util/List;

    .line 45
    iput-object p1, p0, Lcom/google/android/finsky/widget/WidgetModel;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 46
    iput-object p2, p0, Lcom/google/android/finsky/widget/WidgetModel;->mDocumentTypes:[I

    .line 47
    iput-object p3, p0, Lcom/google/android/finsky/widget/WidgetModel;->mImageSelector:Lcom/google/android/finsky/widget/WidgetModel$ImageSelector;

    .line 48
    iput p4, p0, Lcom/google/android/finsky/widget/WidgetModel;->mImageHeightResource:I

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/widget/WidgetModel;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/widget/WidgetModel;
    .param p1, "x1"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "x2"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .param p3, "x3"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/widget/WidgetModel;->bitmapLoaded(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;I)V

    return-void
.end method

.method private bitmapLoaded(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;I)V
    .locals 7
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "result"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .param p3, "imageType"    # I

    .prologue
    .line 186
    iget v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mLoadedImagesSoFar:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mLoadedImagesSoFar:I

    .line 187
    invoke-virtual {p2}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v6, p0, Lcom/google/android/finsky/widget/WidgetModel;->mItems:Ljava/util/List;

    new-instance v0, Lcom/google/android/finsky/widget/WidgetModel$PromotionalItem;

    invoke-virtual {p2}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v4

    move v2, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/widget/WidgetModel$PromotionalItem;-><init>(Landroid/graphics/Bitmap;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;)V

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/widget/WidgetModel;->loadViewsIfDone()V

    .line 192
    return-void
.end method

.method private isValidDocument(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 7
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x1

    .line 169
    iget-object v6, p0, Lcom/google/android/finsky/widget/WidgetModel;->mDocumentTypes:[I

    if-nez v6, :cond_1

    .line 181
    :cond_0
    :goto_0
    return v5

    .line 173
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasDocumentType()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 174
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    .line 175
    .local v1, "documentType":I
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mDocumentTypes:[I

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget v4, v0, v2

    .line 176
    .local v4, "type":I
    if-eq v4, v1, :cond_0

    .line 175
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 181
    .end local v0    # "arr$":[I
    .end local v1    # "documentType":I
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "type":I
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private loadViewsIfDone()V
    .locals 2

    .prologue
    .line 195
    iget v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mLoadedImagesSoFar:I

    iget v1, p0, Lcom/google/android/finsky/widget/WidgetModel;->mSize:I

    if-ne v0, v1, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mListener:Lcom/google/android/finsky/widget/WidgetModel$RefreshListener;

    invoke-interface {v0}, Lcom/google/android/finsky/widget/WidgetModel$RefreshListener;->onData()V

    .line 198
    :cond_0
    return-void
.end method


# virtual methods
.method public getItems()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/finsky/widget/WidgetModel$PromotionalItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mItems:Ljava/util/List;

    return-object v0
.end method

.method public onDataChanged()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 133
    iget-object v8, p0, Lcom/google/android/finsky/widget/WidgetModel;->mItems:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 134
    iput-boolean v7, p0, Lcom/google/android/finsky/widget/WidgetModel;->mUpdatePending:Z

    .line 135
    iput v7, p0, Lcom/google/android/finsky/widget/WidgetModel;->mLoadedImagesSoFar:I

    .line 136
    iget-object v8, p0, Lcom/google/android/finsky/widget/WidgetModel;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v8

    iget v9, p0, Lcom/google/android/finsky/widget/WidgetModel;->mMaxItems:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 137
    .local v5, "numItems":I
    iput v5, p0, Lcom/google/android/finsky/widget/WidgetModel;->mSize:I

    .line 138
    const/4 v3, 0x0

    .local v3, "item":I
    :goto_0
    if-ge v3, v5, :cond_4

    .line 139
    iget-object v8, p0, Lcom/google/android/finsky/widget/WidgetModel;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v8, v3}, Lcom/google/android/finsky/api/model/DfeList;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 140
    .local v0, "document":Lcom/google/android/finsky/api/model/Document;
    iget-object v8, p0, Lcom/google/android/finsky/widget/WidgetModel;->mImageSelector:Lcom/google/android/finsky/widget/WidgetModel$ImageSelector;

    iget v9, p0, Lcom/google/android/finsky/widget/WidgetModel;->mMaxHeight:I

    invoke-interface {v8, v0, v9}, Lcom/google/android/finsky/widget/WidgetModel$ImageSelector;->getImage(Lcom/google/android/finsky/api/model/Document;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v1

    .line 141
    .local v1, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v8, p0, Lcom/google/android/finsky/widget/WidgetModel;->mImageSelector:Lcom/google/android/finsky/widget/WidgetModel$ImageSelector;

    invoke-interface {v8, v0}, Lcom/google/android/finsky/widget/WidgetModel$ImageSelector;->getImageType(Lcom/google/android/finsky/api/model/Document;)I

    move-result v2

    .line 142
    .local v2, "imageType":I
    invoke-direct {p0, v0}, Lcom/google/android/finsky/widget/WidgetModel;->isValidDocument(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v8

    if-eqz v8, :cond_0

    if-eqz v1, :cond_0

    iget-object v8, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 143
    :cond_0
    iget v8, p0, Lcom/google/android/finsky/widget/WidgetModel;->mSize:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/google/android/finsky/widget/WidgetModel;->mSize:I

    .line 144
    invoke-direct {p0}, Lcom/google/android/finsky/widget/WidgetModel;->loadViewsIfDone()V

    .line 138
    :cond_1
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 147
    :cond_2
    iget-boolean v8, v1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v8, :cond_3

    iget v6, p0, Lcom/google/android/finsky/widget/WidgetModel;->mMaxHeight:I

    .line 148
    .local v6, "requestHeight":I
    :goto_2
    iget-object v8, p0, Lcom/google/android/finsky/widget/WidgetModel;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v9, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    new-instance v10, Lcom/google/android/finsky/widget/WidgetModel$1;

    invoke-direct {v10, p0, v0, v2}, Lcom/google/android/finsky/widget/WidgetModel$1;-><init>(Lcom/google/android/finsky/widget/WidgetModel;Lcom/google/android/finsky/api/model/Document;I)V

    invoke-virtual {v8, v9, v7, v6, v10}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v4

    .line 155
    .local v4, "newContainer":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    invoke-virtual {v4}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 156
    invoke-direct {p0, v0, v4, v2}, Lcom/google/android/finsky/widget/WidgetModel;->bitmapLoaded(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;I)V

    goto :goto_1

    .end local v4    # "newContainer":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .end local v6    # "requestHeight":I
    :cond_3
    move v6, v7

    .line 147
    goto :goto_2

    .line 159
    .end local v0    # "document":Lcom/google/android/finsky/api/model/Document;
    .end local v1    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v2    # "imageType":I
    :cond_4
    invoke-direct {p0}, Lcom/google/android/finsky/widget/WidgetModel;->loadViewsIfDone()V

    .line 160
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 2
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mUpdatePending:Z

    .line 165
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mListener:Lcom/google/android/finsky/widget/WidgetModel$RefreshListener;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/widget/WidgetModel$RefreshListener;->onError(Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public refresh(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Lcom/google/android/finsky/widget/WidgetModel$RefreshListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "dfeUrl"    # Ljava/lang/String;
    .param p4, "listener"    # Lcom/google/android/finsky/widget/WidgetModel$RefreshListener;

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mUpdatePending:Z

    if-eqz v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 83
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mUpdatePending:Z

    .line 85
    iput-object p4, p0, Lcom/google/android/finsky/widget/WidgetModel;->mListener:Lcom/google/android/finsky/widget/WidgetModel$RefreshListener;

    .line 86
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 90
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/widget/WidgetModel;->mImageHeightResource:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mMaxHeight:I

    .line 91
    new-instance v0, Lcom/google/android/finsky/api/model/DfeList;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p3, v1}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mList:Lcom/google/android/finsky/api/model/DfeList;

    .line 92
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mUpdatePending:Z

    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetModel;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 110
    return-void
.end method
