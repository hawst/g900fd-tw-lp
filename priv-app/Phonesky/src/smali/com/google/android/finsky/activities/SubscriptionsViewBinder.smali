.class public Lcom/google/android/finsky/activities/SubscriptionsViewBinder;
.super Lcom/google/android/finsky/fragments/DetailsViewBinder;
.source "SubscriptionsViewBinder.java"

# interfaces
.implements Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;
.implements Lcom/google/android/finsky/library/Libraries$Listener;


# instance fields
.field private mDestroyed:Z

.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field private mFragment:Landroid/support/v4/app/Fragment;

.field private mLibraries:Lcom/google/android/finsky/library/Libraries;

.field mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mSavedState:Landroid/os/Bundle;

.field private mSubscriptionItemLayoutId:I

.field private mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/DetailsViewBinder;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDestroyed:Z

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)Lcom/google/android/finsky/layout/SubscriptionsSection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    .prologue
    .line 59
    iget v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionItemLayoutId:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSavedState:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->rebindViews()V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SubscriptionsViewBinder;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private addAppSubscriptionsToMap(Lcom/google/android/finsky/library/AccountLibrary;Ljava/util/Map;)V
    .locals 4
    .param p1, "library"    # Lcom/google/android/finsky/library/AccountLibrary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/library/AccountLibrary;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/library/LibrarySubscriptionEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 219
    .local p2, "subscriptionEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;>;"
    iget-object v3, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/finsky/library/AccountLibrary;->getSubscriptionPurchasesForPackage(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 221
    .local v2, "subscriptions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;

    .line 222
    .local v0, "entry":Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    invoke-virtual {v0}, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 224
    .end local v0    # "entry":Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    :cond_0
    return-void
.end method

.method private cancelSubscription(Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;)V
    .locals 7
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 410
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    .line 411
    .local v0, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/finsky/library/RevokeListenerWrapper;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    new-instance v6, Lcom/google/android/finsky/activities/SubscriptionsViewBinder$3;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder$3;-><init>(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)V

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/finsky/library/RevokeListenerWrapper;-><init>(Lcom/google/android/finsky/library/LibraryReplicators;Landroid/accounts/Account;Lcom/android/volley/Response$Listener;)V

    new-instance v4, Lcom/google/android/finsky/activities/SubscriptionsViewBinder$4;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder$4;-><init>(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)V

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/api/DfeApi;->revoke(Ljava/lang/String;ILcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 430
    return-void
.end method

.method private fetchSubscriptionDocs(Ljava/util/Collection;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/library/LibrarySubscriptionEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286
    .local p1, "subscriptionDocids":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    .local p2, "subscriptionEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;>;"
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    sget-boolean v0, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 288
    const-string v0, "No active subscriptions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 338
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    new-instance v1, Lcom/google/android/finsky/activities/SubscriptionsViewBinder$1;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder$1;-><init>(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;Ljava/util/Map;)V

    new-instance v2, Lcom/google/android/finsky/activities/SubscriptionsViewBinder$2;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder$2;-><init>(Lcom/google/android/finsky/activities/SubscriptionsViewBinder;)V

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->getDetails(Ljava/util/Collection;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    goto :goto_0
.end method

.method private getLibraryMagazineSubscriptionEntry(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
    .locals 3
    .param p1, "subscriptionDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 438
    iget-object v1, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v1}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 439
    .local v0, "owningAccount":Landroid/accounts/Account;
    iget-object v1, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/library/AccountLibrary;->getMagazinesSubscriptionEntry(Ljava/lang/String;)Lcom/google/android/finsky/library/LibrarySubscriptionEntry;

    move-result-object v1

    return-object v1
.end method

.method private handleAppSubscriptions()V
    .locals 6

    .prologue
    .line 203
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v3

    .line 205
    .local v3, "subscriptionEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;>;"
    iget-object v4, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v5, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v5}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v0

    .line 206
    .local v0, "currentLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    iget-object v4, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v4}, Lcom/google/android/finsky/library/Libraries;->getAccountLibraries()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/library/AccountLibrary;

    .line 207
    .local v2, "library":Lcom/google/android/finsky/library/AccountLibrary;
    if-eq v2, v0, :cond_0

    .line 208
    invoke-direct {p0, v2, v3}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->addAppSubscriptionsToMap(Lcom/google/android/finsky/library/AccountLibrary;Ljava/util/Map;)V

    goto :goto_0

    .line 211
    .end local v2    # "library":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_1
    invoke-direct {p0, v0, v3}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->addAppSubscriptionsToMap(Lcom/google/android/finsky/library/AccountLibrary;Ljava/util/Map;)V

    .line 214
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-direct {p0, v4, v3}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->fetchSubscriptionDocs(Ljava/util/Collection;Ljava/util/Map;)V

    .line 215
    return-void
.end method

.method private handleMusicSubscriptions()V
    .locals 14

    .prologue
    .line 240
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    .line 244
    .local v8, "subscriptionDocids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v9

    .line 247
    .local v9, "subscriptionEntries":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;>;"
    sget-object v11, Lcom/google/android/finsky/config/G;->musicAppSubscriptionBackendDocidBlacklist:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v11}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 248
    .local v3, "commaSeparatedBlacklist":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    .line 249
    .local v1, "blacklistedDocids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Object;>;"
    if-eqz v3, :cond_0

    .line 250
    const-string v11, ","

    invoke-virtual {v3, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 251
    .local v2, "blacklistedDocidsArray":[Ljava/lang/String;
    invoke-static {v1, v2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 257
    .end local v2    # "blacklistedDocidsArray":[Ljava/lang/String;
    :cond_0
    iget-object v11, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v12, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v12}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v0

    .line 258
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    const/4 v11, 0x2

    invoke-static {v11}, Lcom/google/android/finsky/library/AccountLibrary;->getLibraryIdFromBackend(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Lcom/google/android/finsky/library/AccountLibrary;->getLibrary(Ljava/lang/String;)Lcom/google/android/finsky/library/HashingLibrary;

    move-result-object v6

    .line 260
    .local v6, "musicLibrary":Lcom/google/android/finsky/library/Library;
    invoke-interface {v6}, Lcom/google/android/finsky/library/Library;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/library/LibraryEntry;

    .line 261
    .local v5, "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    instance-of v11, v5, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;

    if-eqz v11, :cond_1

    move-object v10, v5

    .line 262
    check-cast v10, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;

    .line 264
    .local v10, "subscriptionEntry":Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
    invoke-virtual {v10}, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->getDocId()Ljava/lang/String;

    move-result-object v7

    .line 265
    .local v7, "subscriptionDocId":Ljava/lang/String;
    invoke-interface {v1, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 266
    sget-boolean v11, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v11, :cond_1

    .line 267
    const-string v11, "Ignoring blacklisted subscription: %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v7, v12, v13

    invoke-static {v11, v12}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 271
    :cond_2
    invoke-interface {v9, v7, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    invoke-static {v7}, Lcom/google/android/finsky/utils/DocUtils;->getMusicSubscriptionDocid(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v8, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 275
    .end local v5    # "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    .end local v7    # "subscriptionDocId":Ljava/lang/String;
    .end local v10    # "subscriptionEntry":Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
    :cond_3
    invoke-direct {p0, v8, v9}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->fetchSubscriptionDocs(Ljava/util/Collection;Ljava/util/Map;)V

    .line 276
    return-void
.end method

.method private rebindViews()V
    .locals 9

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    if-nez v0, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/SubscriptionsSection;->getChildCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/SubscriptionsSection;->setVisibility(I)V

    .line 159
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v7

    .line 160
    .local v7, "documentType":I
    const/4 v0, 0x1

    if-ne v7, v0, :cond_4

    .line 161
    const-string v0, "com.google.android.music"

    iget-object v3, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->handleMusicSubscriptions()V

    goto :goto_0

    .line 165
    :cond_3
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->handleAppSubscriptions()V

    goto :goto_0

    .line 170
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/SubscriptionsSection;->clearSubscriptions()V

    .line 172
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const/4 v3, 0x6

    if-ne v0, v3, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 174
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getSubscriptionsList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 175
    .local v1, "subscriptionDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->getLibraryMagazineSubscriptionEntry(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/library/LibrarySubscriptionEntry;

    move-result-object v2

    .line 177
    .local v2, "subsEntry":Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
    if-eqz v2, :cond_5

    .line 178
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    iget v3, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionItemLayoutId:I

    iget-object v5, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSavedState:Landroid/os/Bundle;

    iget-object v6, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object v4, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/SubscriptionsSection;->addSubscription(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;ILcom/google/android/finsky/layout/SubscriptionView$CancelListener;Landroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_1

    .line 184
    .end local v1    # "subscriptionDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v2    # "subsEntry":Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/SubscriptionsSection;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/SubscriptionsSection;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public bind(Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/layout/SubscriptionsSection;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/Document;ILandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "subscriptionsSection"    # Lcom/google/android/finsky/layout/SubscriptionsSection;
    .param p3, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p4, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "subscriptionItemLayoutId"    # I
    .param p6, "savedState"    # Landroid/os/Bundle;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mFragment:Landroid/support/v4/app/Fragment;

    .line 124
    iput-object p2, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    .line 125
    iput-object p3, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 126
    iput-object p4, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 127
    iput-object p6, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSavedState:Landroid/os/Bundle;

    .line 128
    iput p5, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionItemLayoutId:I

    .line 129
    iput-object p7, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->removeListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->addListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDestroyed:Z

    .line 139
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->rebindViews()V

    .line 140
    return-void
.end method

.method public init(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/library/Libraries;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "api"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "libraries"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 89
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/DetailsViewBinder;->init(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 90
    iput-object p4, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 91
    return-void
.end method

.method public onAllLibrariesLoaded()V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public onCancel(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;)V
    .locals 16
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "libraryEntry"    # Lcom/google/android/finsky/library/LibrarySubscriptionEntry;

    .prologue
    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mFragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v12

    .line 348
    .local v12, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v2, "SubscriptionsViewBinder.confirm_cancel_dialog"

    invoke-virtual {v12, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 374
    :goto_0
    return-void

    .line 352
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 353
    .local v14, "now":J
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->trialUntilTimestampMs:J

    cmp-long v2, v14, v2

    if-gez v2, :cond_1

    const v10, 0x7f0c021f

    .line 356
    .local v10, "confirmRes":I
    :goto_1
    new-instance v9, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v9}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 357
    .local v9, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v10, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c01b1

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c01b2

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const/16 v3, 0x130

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v4

    const/16 v5, 0xf3

    const/16 v6, 0xf4

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 365
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 366
    .local v11, "extraArguments":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->getAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    const-string v2, "subscription_doc"

    move-object/from16 v0, p1

    invoke-virtual {v11, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mFragment:Landroid/support/v4/app/Fragment;

    const/4 v3, 0x3

    invoke-virtual {v9, v2, v3, v11}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 372
    invoke-virtual {v9}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v8

    .line 373
    .local v8, "alert":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v2, "SubscriptionsViewBinder.confirm_cancel_dialog"

    invoke-virtual {v8, v12, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 353
    .end local v8    # "alert":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .end local v9    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v10    # "confirmRes":I
    .end local v11    # "extraArguments":Landroid/os/Bundle;
    :cond_1
    const v10, 0x7f0c021e

    goto :goto_1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mDestroyed:Z

    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->removeListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 99
    :cond_0
    return-void
.end method

.method public onLibraryContentsChanged(Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 0
    .param p1, "library"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->rebindViews()V

    .line 109
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 391
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->rebindViews()V

    .line 392
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 381
    const-string v2, "authAccount"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 382
    .local v0, "accountName":Ljava/lang/String;
    const-string v2, "subscription_doc"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 383
    .local v1, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->cancelSubscription(Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;)V

    .line 384
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    if-nez v0, :cond_0

    .line 400
    :goto_0
    return-void

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/SubscriptionsViewBinder;->mSubscriptionsSection:Lcom/google/android/finsky/layout/SubscriptionsSection;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/SubscriptionsSection;->saveInstanceState(Landroid/os/Bundle;)V

    goto :goto_0
.end method
