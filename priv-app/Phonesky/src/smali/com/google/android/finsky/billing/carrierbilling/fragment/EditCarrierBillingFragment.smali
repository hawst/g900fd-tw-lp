.class public Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;
.super Lcom/google/android/finsky/fragments/LoggingFragment;
.source "EditCarrierBillingFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$2;,
        Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;
    }
.end annotation


# instance fields
.field private mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

.field private mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

.field private mBillingUiMode:I

.field private mCancelButton:Landroid/widget/Button;

.field private mEditSection:Landroid/view/ViewGroup;

.field private mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;

.field private mNameView:Landroid/widget/TextView;

.field private mPhoneNumberEditView:Landroid/widget/EditText;

.field private mSaveButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;-><init>()V

    .line 381
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;Ljava/util/ArrayList;)Ljava/util/Collection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getFormErrors(Ljava/util/ArrayList;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;Ljava/util/Collection;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;
    .param p1, "x1"    # Ljava/util/Collection;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->displayErrors(Ljava/util/Collection;)V

    return-void
.end method

.method private displayErrors(Ljava/util/Collection;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "errorFields":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;>;"
    const v9, 0x7f0c00c1

    .line 276
    const/4 v5, 0x0

    .line 277
    .local v5, "topMostView":Landroid/view/View;
    const/4 v4, 0x0

    .line 278
    .local v4, "topMostTop":I
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;

    .line 279
    .local v2, "errorField":Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;
    const/4 v1, 0x0

    .line 280
    .local v1, "currentView":Landroid/view/View;
    sget-object v6, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$2;->$SwitchMap$com$google$android$finsky$billing$carrierbilling$PhoneCarrierBillingUtils$AddressInputField:[I

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto :goto_0

    .line 283
    :pswitch_0
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v7, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_1:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressWidget;->getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;

    move-result-object v1

    .line 284
    if-eqz v1, :cond_1

    move-object v6, v1

    .line 285
    check-cast v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v8, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_1:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v7, v8}, Lcom/android/i18n/addressinput/AddressWidget;->getNameForField(Lcom/android/i18n/addressinput/AddressField;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7, v9}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setTextViewError(Landroid/widget/TextView;Ljava/lang/String;I)V

    .line 336
    :cond_1
    :goto_1
    if-nez v5, :cond_2

    .line 337
    move-object v5, v1

    .line 338
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mEditSection:Landroid/view/ViewGroup;

    invoke-static {v6, v1}, Lcom/google/android/finsky/billing/BillingUtils;->getViewOffsetToChild(Landroid/view/ViewGroup;Landroid/view/View;)I

    move-result v4

    goto :goto_0

    .line 291
    :pswitch_1
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v7, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_2:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressWidget;->getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;

    move-result-object v1

    .line 292
    if-eqz v1, :cond_1

    move-object v6, v1

    .line 293
    check-cast v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v8, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_2:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v7, v8}, Lcom/android/i18n/addressinput/AddressWidget;->getNameForField(Lcom/android/i18n/addressinput/AddressField;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7, v9}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setTextViewError(Landroid/widget/TextView;Ljava/lang/String;I)V

    goto :goto_1

    .line 299
    :pswitch_2
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v7, Lcom/android/i18n/addressinput/AddressField;->LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressWidget;->getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;

    move-result-object v1

    .line 300
    if-eqz v1, :cond_1

    move-object v6, v1

    .line 301
    check-cast v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v8, Lcom/android/i18n/addressinput/AddressField;->LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v7, v8}, Lcom/android/i18n/addressinput/AddressWidget;->getNameForField(Lcom/android/i18n/addressinput/AddressField;)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0c00c2

    invoke-direct {p0, v6, v7, v8}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setTextViewError(Landroid/widget/TextView;Ljava/lang/String;I)V

    goto :goto_1

    .line 307
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mNameView:Landroid/widget/TextView;

    move-object v6, v1

    .line 308
    check-cast v6, Landroid/widget/TextView;

    const v7, 0x7f0c00c8

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0c00c0

    invoke-direct {p0, v6, v7, v8}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setTextViewError(Landroid/widget/TextView;Ljava/lang/String;I)V

    goto :goto_1

    .line 312
    :pswitch_4
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v7, Lcom/android/i18n/addressinput/AddressField;->POSTAL_CODE:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressWidget;->getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;

    move-result-object v1

    .line 313
    if-eqz v1, :cond_1

    .line 314
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v7, Lcom/android/i18n/addressinput/AddressField;->POSTAL_CODE:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressWidget;->displayErrorMessageForInvalidEntryIn(Lcom/android/i18n/addressinput/AddressField;)Landroid/widget/TextView;

    goto :goto_1

    .line 319
    :pswitch_5
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v7, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressWidget;->getViewForField(Lcom/android/i18n/addressinput/AddressField;)Landroid/view/View;

    move-result-object v1

    .line 320
    if-eqz v1, :cond_1

    .line 321
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    sget-object v7, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v6, v7}, Lcom/android/i18n/addressinput/AddressWidget;->displayErrorMessageForInvalidEntryIn(Lcom/android/i18n/addressinput/AddressField;)Landroid/widget/TextView;

    goto :goto_1

    .line 326
    :pswitch_6
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mPhoneNumberEditView:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    .line 327
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mPhoneNumberEditView:Landroid/widget/EditText;

    move-object v6, v1

    .line 328
    check-cast v6, Landroid/widget/TextView;

    const v7, 0x7f0c00b4

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0c00c3

    invoke-direct {p0, v6, v7, v8}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setTextViewError(Landroid/widget/TextView;Ljava/lang/String;I)V

    goto/16 :goto_1

    .line 339
    :cond_2
    if-eqz v1, :cond_0

    .line 340
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mEditSection:Landroid/view/ViewGroup;

    invoke-static {v6, v1}, Lcom/google/android/finsky/billing/BillingUtils;->getViewOffsetToChild(Landroid/view/ViewGroup;Landroid/view/View;)I

    move-result v0

    .line 341
    .local v0, "currentTop":I
    if-ge v0, v4, :cond_0

    .line 342
    move-object v5, v1

    .line 343
    move v4, v0

    goto/16 :goto_0

    .line 347
    .end local v0    # "currentTop":I
    .end local v1    # "currentView":Landroid/view/View;
    .end local v2    # "errorField":Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;
    :cond_3
    if-eqz v5, :cond_4

    .line 348
    invoke-virtual {v5}, Landroid/view/View;->requestFocus()Z

    .line 350
    :cond_4
    return-void

    .line 280
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private getFormErrors(Ljava/util/ArrayList;)Ljava/util/Collection;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "highlightField":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .line 171
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 172
    .local v1, "errors":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 173
    .local v0, "error":Ljava/lang/Integer;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 202
    :pswitch_0
    const-string v3, "InputValidationError that can\'t be displayed: type=%d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 175
    :pswitch_1
    sget-object v3, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;->PERSON_NAME:Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 178
    :pswitch_2
    sget-object v3, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;->ADDR_PHONE:Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 181
    :pswitch_3
    sget-object v3, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;->ADDR_STATE:Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184
    :pswitch_4
    sget-object v3, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;->ADDR_CITY:Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 187
    :pswitch_5
    const-string v3, "Input error ADDR_WHOLE_ADDRESS. Displaying at ADDRESS_LINE_1."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 190
    :pswitch_6
    sget-object v3, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;->ADDR_ADDRESS1:Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 193
    :pswitch_7
    sget-object v3, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;->ADDR_ADDRESS2:Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 196
    :pswitch_8
    sget-object v3, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;->ADDR_POSTAL_CODE:Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 199
    :pswitch_9
    sget-object v3, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;->ADDR_COUNTRY_CODE:Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;

    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 206
    .end local v0    # "error":Ljava/lang/Integer;
    :cond_0
    return-object v1

    .line 173
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_6
        :pswitch_7
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method private getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mPhoneNumberEditView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getReturnAddress()Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;
    .locals 4

    .prologue
    .line 357
    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v2}, Lcom/android/i18n/addressinput/AddressWidget;->getAddressData()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v0

    .line 358
    .local v0, "addressData":Lcom/android/i18n/addressinput/AddressData;
    new-instance v2, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;

    invoke-direct {v2}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;-><init>()V

    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;->setName(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressData;->getPostalCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;->setPostalCode(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressData;->getPostalCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;->setCountry(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;

    move-result-object v1

    .line 363
    .local v1, "builder":Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;
    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getCarrierBillingStorage()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils;->isPhoneNumberRequired(Lcom/google/android/finsky/billing/BillingUtils$AddressMode;Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 365
    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;->setIdentifier(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;

    .line 368
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    sget-object v3, Lcom/google/android/finsky/billing/BillingUtils$AddressMode;->FULL_ADDRESS:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    if-ne v2, v3, :cond_1

    .line 369
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressData;->getAddressLine1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;->setAddress1(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressData;->getAddressLine2()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;->setAddress2(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressData;->getLocality()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;->setCity(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressData;->getAdministrativeArea()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;->setState(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;

    .line 374
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo$Builder;->build()Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    move-result-object v2

    return-object v2
.end method

.method private initViews(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mEditSection:Landroid/view/ViewGroup;

    .line 159
    const v0, 0x7f0a0101

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mNameView:Landroid/widget/TextView;

    .line 160
    const v0, 0x7f0a0103

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mPhoneNumberEditView:Landroid/widget/EditText;

    .line 161
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Lcom/google/android/finsky/billing/BillingUtils$AddressMode;Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;Ljava/util/ArrayList;I)Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "addressMode"    # Lcom/google/android/finsky/billing/BillingUtils$AddressMode;
    .param p2, "prefillAddress"    # Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;
    .param p4, "mode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/billing/BillingUtils$AddressMode;",
            "Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)",
            "Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;"
        }
    .end annotation

    .prologue
    .line 70
    .local p3, "displayErrorList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    new-instance v1, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;-><init>()V

    .line 71
    .local v1, "fragment":Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 72
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v2, "prefill_address"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 74
    const-string v2, "type"

    invoke-virtual {p1}, Lcom/google/android/finsky/billing/BillingUtils$AddressMode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    const-string v2, "highlight_address"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 76
    const-string v2, "ui_mode"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setArguments(Landroid/os/Bundle;)V

    .line 78
    return-object v1
.end method

.method private setTextViewError(Landroid/widget/TextView;Ljava/lang/String;I)V
    .locals 1
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "textViewName"    # Ljava/lang/String;
    .param p3, "errorMessageResId"    # I

    .prologue
    .line 353
    invoke-virtual {p0, p3}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    return-void
.end method

.method private setupAddressEditView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getCarrierBillingStorage()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils;->isPhoneNumberRequired(Lcom/google/android/finsky/billing/BillingUtils$AddressMode;Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 231
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getLine1NumberFromTelephony()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->showPhoneNumberEditView(Ljava/lang/String;)V

    .line 235
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->showAddressEditView(Landroid/view/View;Lcom/android/i18n/addressinput/AddressData;)V

    .line 236
    return-void
.end method

.method private setupAddressEditView(Landroid/view/View;Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "prefillAddress"    # Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    .prologue
    .line 211
    invoke-virtual {p2}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->showNameView(Ljava/lang/String;)V

    .line 213
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getCarrierBillingStorage()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils;->isPhoneNumberRequired(Lcom/google/android/finsky/billing/BillingUtils$AddressMode;Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 215
    invoke-virtual {p2}, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;->getIdentifier()Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "phoneNumber":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/utils/Utils;->isEmptyOrSpaces(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 217
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getLine1NumberFromTelephony()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 220
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->showPhoneNumberEditView(Ljava/lang/String;)V

    .line 223
    .end local v0    # "phoneNumber":Ljava/lang/String;
    :cond_1
    invoke-static {p2}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils;->subscriberInfoToAddressData(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;)Lcom/android/i18n/addressinput/AddressData;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->showAddressEditView(Landroid/view/View;Lcom/android/i18n/addressinput/AddressData;)V

    .line 225
    return-void
.end method

.method private showAddressEditView(Landroid/view/View;Lcom/android/i18n/addressinput/AddressData;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "addressData"    # Lcom/android/i18n/addressinput/AddressData;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    invoke-static {v0}, Lcom/google/android/finsky/billing/BillingUtils;->getAddressFormOptions(Lcom/google/android/finsky/billing/BillingUtils$AddressMode;)Lcom/android/i18n/addressinput/FormOptions;

    move-result-object v3

    .line 245
    .local v3, "addressFormOptions":Lcom/android/i18n/addressinput/FormOptions;
    const v0, 0x7f0a00ee

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/AddressFieldsLayout;

    .line 246
    .local v2, "rootView":Lcom/google/android/finsky/layout/AddressFieldsLayout;
    if-nez p2, :cond_0

    const/4 v5, 0x0

    .line 248
    .local v5, "postalCountry":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v4, Lcom/google/android/finsky/billing/AddressMetadataCacheManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCache()Lcom/android/volley/Cache;

    move-result-object v6

    invoke-direct {v4, v6}, Lcom/google/android/finsky/billing/AddressMetadataCacheManager;-><init>(Lcom/android/volley/Cache;)V

    invoke-direct/range {v0 .. v5}, Lcom/android/i18n/addressinput/AddressWidget;-><init>(Landroid/content/Context;Lcom/google/android/finsky/layout/AddressFieldsLayout;Lcom/android/i18n/addressinput/FormOptions;Lcom/android/i18n/addressinput/ClientCacheManager;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    .line 251
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    new-instance v1, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$1;-><init>(Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressWidget;->setListener(Lcom/android/i18n/addressinput/AddressWidget$Listener;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v0, p2}, Lcom/android/i18n/addressinput/AddressWidget;->renderFormWithSavedAddress(Lcom/android/i18n/addressinput/AddressData;)V

    .line 262
    return-void

    .line 246
    .end local v5    # "postalCountry":Ljava/lang/String;
    :cond_0
    invoke-virtual {p2}, Lcom/android/i18n/addressinput/AddressData;->getPostalCountry()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private showNameView(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    return-void
.end method

.method private showPhoneNumberEditView(Ljava/lang/String;)V
    .locals 2
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mPhoneNumberEditView:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 269
    invoke-static {p1}, Lcom/google/android/finsky/utils/Utils;->isEmptyOrSpaces(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mPhoneNumberEditView:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 272
    :cond_0
    return-void
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mBillingUiMode:I

    if-nez v0, :cond_0

    const/16 v0, 0x34d

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x380

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 151
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/LoggingFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 152
    if-eqz p1, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v0, p1}, Lcom/android/i18n/addressinput/AddressWidget;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 155
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 391
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mSaveButton:Landroid/widget/Button;

    if-ne p1, v1, :cond_2

    .line 392
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mNameView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v3}, Lcom/android/i18n/addressinput/AddressWidget;->getAddressProblems()Lcom/android/i18n/addressinput/AddressProblems;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils;->getErrors(Ljava/lang/String;Ljava/lang/String;Lcom/android/i18n/addressinput/AddressProblems;Lcom/google/android/finsky/billing/BillingUtils$AddressMode;)Ljava/util/Collection;

    move-result-object v0

    .line 395
    .local v0, "errors":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;>;"
    const/16 v1, 0x34e

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->logClickEvent(I)V

    .line 396
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 397
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mEditSection:Landroid/view/ViewGroup;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->hideKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 398
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getReturnAddress()Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;->onEditCarrierBillingResult(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;)V

    .line 406
    .end local v0    # "errors":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;>;"
    :cond_0
    :goto_0
    return-void

    .line 400
    .restart local v0    # "errors":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;>;"
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->displayErrors(Ljava/util/Collection;)V

    goto :goto_0

    .line 402
    .end local v0    # "errors":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/billing/carrierbilling/PhoneCarrierBillingUtils$AddressInputField;>;"
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v1, :cond_0

    .line 403
    const/16 v1, 0x34f

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->logClickEvent(I)V

    .line 404
    iget-object v1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;->onEditCarrierBillingResult(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v10, 0x7f0c0134

    const v9, 0x7f0c00c5

    const/4 v8, 0x0

    .line 94
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 95
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "ui_mode"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mBillingUiMode:I

    .line 96
    iget v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mBillingUiMode:I

    if-nez v6, :cond_0

    const v1, 0x7f040035

    .line 98
    .local v1, "layoutId":I
    :goto_0
    invoke-virtual {p1, v1, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 102
    .local v4, "v":Landroid/view/View;
    const v6, 0x7f0a0100

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 103
    .local v5, "view":Landroid/view/ViewGroup;
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 104
    invoke-virtual {v5}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 106
    invoke-direct {p0, v5}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->initViews(Landroid/view/ViewGroup;)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;

    invoke-virtual {v6}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSetupWizardNavBar()Lcom/google/android/finsky/setup/SetupWizardNavBar;

    move-result-object v3

    .line 110
    .local v3, "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    if-eqz v3, :cond_1

    .line 111
    invoke-virtual {v3}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mSaveButton:Landroid/widget/Button;

    .line 112
    invoke-virtual {v3}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getBackButton()Landroid/widget/Button;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mCancelButton:Landroid/widget/Button;

    .line 118
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mSaveButton:Landroid/widget/Button;

    instance-of v6, v6, Lcom/google/android/play/layout/PlayActionButton;

    if-eqz v6, :cond_2

    .line 119
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mSaveButton:Landroid/widget/Button;

    check-cast v6, Lcom/google/android/play/layout/PlayActionButton;

    const/4 v7, 0x3

    invoke-virtual {v6, v7, v9, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 125
    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mCancelButton:Landroid/widget/Button;

    instance-of v6, v6, Lcom/google/android/play/layout/PlayActionButton;

    if-eqz v6, :cond_3

    .line 126
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mCancelButton:Landroid/widget/Button;

    check-cast v6, Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v6, v8, v10, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 133
    :goto_3
    const-string v6, "type"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/finsky/billing/BillingUtils$AddressMode;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressMode:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    .line 134
    const-string v6, "prefill_address"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    .line 135
    .local v2, "prefillAddress":Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;
    if-eqz v2, :cond_4

    .line 136
    invoke-direct {p0, v5, v2}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setupAddressEditView(Landroid/view/View;Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;)V

    .line 140
    :goto_4
    return-object v4

    .line 96
    .end local v1    # "layoutId":I
    .end local v2    # "prefillAddress":Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;
    .end local v3    # "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    .end local v4    # "v":Landroid/view/View;
    .end local v5    # "view":Landroid/view/ViewGroup;
    :cond_0
    const v1, 0x7f04019e

    goto :goto_0

    .line 114
    .restart local v1    # "layoutId":I
    .restart local v3    # "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    .restart local v4    # "v":Landroid/view/View;
    .restart local v5    # "view":Landroid/view/ViewGroup;
    :cond_1
    const v6, 0x7f0a00f8

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mSaveButton:Landroid/widget/Button;

    .line 115
    const v6, 0x7f0a0115

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Button;

    iput-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mCancelButton:Landroid/widget/Button;

    goto :goto_1

    .line 122
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mSaveButton:Landroid/widget/Button;

    invoke-virtual {v6, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mSaveButton:Landroid/widget/Button;

    invoke-virtual {v6, v9}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    .line 129
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v6, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v6, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v6, v10}, Landroid/widget/Button;->setText(I)V

    goto :goto_3

    .line 138
    .restart local v2    # "prefillAddress":Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;
    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->setupAddressEditView(Landroid/view/View;)V

    goto :goto_4
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/LoggingFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mAddressWidget:Lcom/android/i18n/addressinput/AddressWidget;

    invoke-virtual {v0, p1}, Lcom/android/i18n/addressinput/AddressWidget;->saveInstanceState(Landroid/os/Bundle;)V

    .line 147
    return-void
.end method

.method public setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment;->mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/EditCarrierBillingFragment$EditCarrierBillingResultListener;

    .line 168
    return-void
.end method
