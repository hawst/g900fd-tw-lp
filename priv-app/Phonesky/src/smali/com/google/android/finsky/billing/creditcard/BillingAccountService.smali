.class public Lcom/google/android/finsky/billing/creditcard/BillingAccountService;
.super Landroid/app/Service;
.source "BillingAccountService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Lcom/google/android/finsky/api/DfeApi;[ILjava/util/concurrent/Semaphore;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/BillingAccountService;
    .param p1, "x1"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "x2"    # [I
    .param p3, "x3"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->checkValidInstrument(Lcom/google/android/finsky/api/DfeApi;[ILjava/util/concurrent/Semaphore;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/BillingAccountService;
    .param p1, "x1"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->checkPromoOffers(Landroid/accounts/Account;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Ljava/lang/Throwable;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/creditcard/BillingAccountService;
    .param p1, "x1"    # Ljava/lang/Throwable;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->convertErrorCode(Ljava/lang/Throwable;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/api/DfeApi;I[ILjava/util/concurrent/Semaphore;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/api/DfeApi;
    .param p1, "x1"    # I
    .param p2, "x2"    # [I
    .param p3, "x3"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 40
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->returnResult(Lcom/google/android/finsky/api/DfeApi;I[ILjava/util/concurrent/Semaphore;)V

    return-void
.end method

.method private checkPromoOffers(Landroid/accounts/Account;)Landroid/os/Bundle;
    .locals 12
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/volley/AuthFailureError;
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 173
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 174
    .local v5, "result":Landroid/os/Bundle;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    iget-object v8, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    .line 176
    .local v1, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v4

    .line 177
    .local v4, "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;>;"
    invoke-interface {v1, v4, v4}, Lcom/google/android/finsky/api/DfeApi;->checkPromoOffers(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 180
    :try_start_0
    invoke-virtual {v4}, Lcom/android/volley/toolbox/RequestFuture;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    .line 181
    .local v0, "checkPromoOfferResponse":Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;
    iget-boolean v7, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->checkoutTokenRequired:Z

    if-eqz v7, :cond_1

    .line 182
    const-string v7, "Unexpected checkout_token_required."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 183
    const/4 v6, -0x3

    .line 202
    .end local v0    # "checkPromoOfferResponse":Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;
    .local v6, "resultCode":I
    :goto_0
    if-ne v6, v11, :cond_0

    .line 203
    invoke-direct {p0, v1, v6}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->fetchToc(Lcom/google/android/finsky/api/DfeApi;I)I

    move-result v6

    .line 206
    :cond_0
    const-string v7, "result_code"

    invoke-virtual {v5, v7, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 207
    invoke-static {v6}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->logOfferResultCode(I)V

    .line 208
    const-string v7, "CheckPromoOffers result: %d"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    return-object v5

    .line 184
    .end local v6    # "resultCode":I
    .restart local v0    # "checkPromoOfferResponse":Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;
    :cond_1
    :try_start_1
    iget-boolean v7, v0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasAvailablePromoOfferStatus:Z

    if-nez v7, :cond_2

    .line 185
    const-string v7, "No available promo offer status returned."

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 186
    const/4 v6, -0x1

    .restart local v6    # "resultCode":I
    goto :goto_0

    .line 188
    .end local v6    # "resultCode":I
    :cond_2
    invoke-direct {p0, v0, p1, v5}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->setResultFromOfferStatus(Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;Landroid/accounts/Account;Landroid/os/Bundle;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    .restart local v6    # "resultCode":I
    goto :goto_0

    .line 190
    .end local v0    # "checkPromoOfferResponse":Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;
    .end local v6    # "resultCode":I
    :catch_0
    move-exception v2

    .line 191
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v7, "Interrupted while requesting /checkPromoOffers: %s"

    new-array v8, v11, [Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 192
    const/4 v6, -0x4

    .line 198
    .restart local v6    # "resultCode":I
    goto :goto_0

    .line 193
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v6    # "resultCode":I
    :catch_1
    move-exception v2

    .line 195
    .local v2, "e":Ljava/util/concurrent/ExecutionException;
    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    .line 196
    .local v3, "error":Ljava/lang/Throwable;
    const-string v7, "Error while requesting /checkPromoOffers: %s"

    new-array v8, v11, [Ljava/lang/Object;

    aput-object v3, v8, v10

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    invoke-direct {p0, v3}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService;->convertErrorCode(Ljava/lang/Throwable;)I

    move-result v6

    .restart local v6    # "resultCode":I
    goto :goto_0
.end method

.method private checkValidInstrument(Lcom/google/android/finsky/api/DfeApi;[ILjava/util/concurrent/Semaphore;)V
    .locals 2
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "resultCodeOut"    # [I
    .param p3, "semaphore"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 310
    new-instance v0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$3;-><init>(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Lcom/google/android/finsky/api/DfeApi;[ILjava/util/concurrent/Semaphore;)V

    new-instance v1, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$4;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$4;-><init>(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Lcom/google/android/finsky/api/DfeApi;[ILjava/util/concurrent/Semaphore;)V

    invoke-interface {p1, v0, v1}, Lcom/google/android/finsky/api/DfeApi;->checkInstrument(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 328
    return-void
.end method

.method private convertErrorCode(Ljava/lang/Throwable;)I
    .locals 1
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 331
    instance-of v0, p1, Lcom/android/volley/ServerError;

    if-eqz v0, :cond_0

    .line 332
    const/4 v0, -0x1

    .line 340
    :goto_0
    return v0

    .line 333
    :cond_0
    instance-of v0, p1, Lcom/android/volley/NetworkError;

    if-eqz v0, :cond_1

    .line 334
    const/4 v0, -0x2

    goto :goto_0

    .line 335
    :cond_1
    instance-of v0, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v0, :cond_2

    .line 336
    const/4 v0, -0x3

    goto :goto_0

    .line 337
    :cond_2
    instance-of v0, p1, Lcom/android/volley/TimeoutError;

    if-eqz v0, :cond_3

    .line 338
    const/4 v0, -0x4

    goto :goto_0

    .line 340
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private fetchToc(Lcom/google/android/finsky/api/DfeApi;I)I
    .locals 8
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "resultCode"    # I

    .prologue
    const/4 v7, -0x4

    const/4 v6, 0x0

    .line 218
    const/4 v3, 0x1

    new-array v1, v3, [I

    aput p2, v1, v6

    .line 219
    .local v1, "resultCodeWrapper":[I
    new-instance v2, Ljava/util/concurrent/Semaphore;

    invoke-direct {v2, v6}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 220
    .local v2, "semaphore":Ljava/util/concurrent/Semaphore;
    new-instance v3, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;

    invoke-direct {v3, p0, v2, v1}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$2;-><init>(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;Ljava/util/concurrent/Semaphore;[I)V

    invoke-static {p1, v6, v3}, Lcom/google/android/finsky/utils/GetTocHelper;->getToc(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    .line 236
    const-wide/16 v4, 0x2d

    :try_start_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 237
    const/4 v3, 0x0

    const/4 v4, -0x4

    aput v4, v1, v3
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :cond_0
    :goto_0
    aget v3, v1, v6

    return v3

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Ljava/lang/InterruptedException;
    aput v7, v1, v6

    goto :goto_0
.end method

.method private static logOfferResultCode(I)V
    .locals 0
    .param p0, "resultCode"    # I

    .prologue
    .line 306
    return-void
.end method

.method private static returnResult(Lcom/google/android/finsky/api/DfeApi;I[ILjava/util/concurrent/Semaphore;)V
    .locals 1
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p1, "result"    # I
    .param p2, "resultOut"    # [I
    .param p3, "semaphore"    # Ljava/util/concurrent/Semaphore;

    .prologue
    .line 347
    const/4 v0, 0x0

    aput p1, p2, v0

    .line 348
    invoke-virtual {p3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 349
    return-void
.end method

.method private setResultFromOfferStatus(Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;Landroid/accounts/Account;Landroid/os/Bundle;)I
    .locals 7
    .param p1, "checkPromoOfferResponse"    # Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "result"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 262
    iget v3, p1, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availablePromoOfferStatus:I

    packed-switch v3, :pswitch_data_0

    .line 290
    iget-object v3, p1, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v3, :cond_1

    .line 291
    const/4 v2, 0x2

    .line 292
    .local v2, "resultCode":I
    const-string v3, "redeemed_offer_message_html"

    iget-object v4, p1, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    iget-object v4, v4, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p3, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    :goto_0
    return v2

    .line 264
    .end local v2    # "resultCode":I
    :pswitch_0
    iget-object v3, p1, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    array-length v3, v3

    if-nez v3, :cond_0

    .line 265
    const-string v3, "No offer returned while offer status is %d."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p1, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availablePromoOfferStatus:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    const/4 v2, -0x1

    .restart local v2    # "resultCode":I
    goto :goto_0

    .line 269
    .end local v2    # "resultCode":I
    :cond_0
    const/4 v2, 0x1

    .line 274
    .restart local v2    # "resultCode":I
    iget-object v3, p1, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    aget-object v3, v3, v6

    invoke-static {p2, v3}, Lcom/google/android/finsky/activities/SetupWizardAvailablePromoOfferActivity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;)Landroid/content/Intent;

    move-result-object v1

    .line 276
    .local v1, "offerIntent":Landroid/content/Intent;
    const-string v3, "available_offer_redemption_intent"

    invoke-virtual {p3, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 282
    .end local v1    # "offerIntent":Landroid/content/Intent;
    .end local v2    # "resultCode":I
    :pswitch_1
    const/4 v2, 0x1

    .line 283
    .restart local v2    # "resultCode":I
    invoke-static {p2}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->createExternalSetupWizardIntent(Landroid/accounts/Account;)Landroid/content/Intent;

    move-result-object v0

    .line 285
    .local v0, "fopIntent":Landroid/content/Intent;
    const-string v3, "available_offer_redemption_intent"

    invoke-virtual {p3, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0

    .line 295
    .end local v0    # "fopIntent":Landroid/content/Intent;
    .end local v2    # "resultCode":I
    :cond_1
    const/4 v2, 0x3

    .restart local v2    # "resultCode":I
    goto :goto_0

    .line 262
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/creditcard/BillingAccountService$1;-><init>(Lcom/google/android/finsky/billing/creditcard/BillingAccountService;)V

    return-object v0
.end method
