.class public Lcom/google/android/finsky/download/DownloadProgress;
.super Ljava/lang/Object;
.source "DownloadProgress.java"


# instance fields
.field public final bytesCompleted:J

.field public final bytesTotal:J

.field public final contentUri:Landroid/net/Uri;

.field public final statusCode:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;JJI)V
    .locals 0
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "bytesCompleted"    # J
    .param p4, "bytesTotal"    # J
    .param p6, "statusId"    # I

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadProgress;->contentUri:Landroid/net/Uri;

    .line 35
    iput-wide p2, p0, Lcom/google/android/finsky/download/DownloadProgress;->bytesCompleted:J

    .line 36
    iput-wide p4, p0, Lcom/google/android/finsky/download/DownloadProgress;->bytesTotal:J

    .line 37
    iput p6, p0, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 47
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/google/android/finsky/download/DownloadProgress;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 48
    check-cast v0, Lcom/google/android/finsky/download/DownloadProgress;

    .line 49
    .local v0, "other":Lcom/google/android/finsky/download/DownloadProgress;
    iget-wide v2, p0, Lcom/google/android/finsky/download/DownloadProgress;->bytesCompleted:J

    iget-wide v4, v0, Lcom/google/android/finsky/download/DownloadProgress;->bytesCompleted:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/download/DownloadProgress;->bytesTotal:J

    iget-wide v4, v0, Lcom/google/android/finsky/download/DownloadProgress;->bytesTotal:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    iget v3, v0, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadProgress;->contentUri:Landroid/net/Uri;

    iget-object v3, v0, Lcom/google/android/finsky/download/DownloadProgress;->contentUri:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 53
    .end local v0    # "other":Lcom/google/android/finsky/download/DownloadProgress;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadProgress;->contentUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Lcom/google/android/finsky/download/DownloadProgress;->bytesCompleted:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/finsky/download/DownloadProgress;->bytesTotal:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
