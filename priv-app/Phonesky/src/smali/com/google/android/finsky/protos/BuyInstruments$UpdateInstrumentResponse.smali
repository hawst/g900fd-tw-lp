.class public final Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BuyInstruments.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BuyInstruments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UpdateInstrumentResponse"
.end annotation


# instance fields
.field public checkoutTokenRequired:Z

.field public errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

.field public hasCheckoutTokenRequired:Z

.field public hasInstrumentId:Z

.field public hasResult:Z

.field public hasUserMessageHtml:Z

.field public instrumentId:Ljava/lang/String;

.field public redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

.field public result:I

.field public userMessageHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->clear()Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    .line 159
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 162
    iput v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    .line 163
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasResult:Z

    .line 164
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->instrumentId:Ljava/lang/String;

    .line 165
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasInstrumentId:Z

    .line 166
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    .line 167
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasUserMessageHtml:Z

    .line 168
    invoke-static {}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .line 169
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->checkoutTokenRequired:Z

    .line 170
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasCheckoutTokenRequired:Z

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    .line 172
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->cachedSize:I

    .line 173
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 207
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 208
    .local v2, "size":I
    iget v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasResult:Z

    if-eqz v3, :cond_1

    .line 209
    :cond_0
    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 212
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasInstrumentId:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->instrumentId:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 213
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->instrumentId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 216
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasUserMessageHtml:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 217
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 220
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 221
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 222
    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    aget-object v0, v3, v1

    .line 223
    .local v0, "element":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    if-eqz v0, :cond_6

    .line 224
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 221
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 229
    .end local v0    # "element":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .end local v1    # "i":I
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasCheckoutTokenRequired:Z

    if-nez v3, :cond_8

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->checkoutTokenRequired:Z

    if-eqz v3, :cond_9

    .line 230
    :cond_8
    const/4 v3, 0x5

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->checkoutTokenRequired:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 233
    :cond_9
    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v3, :cond_a

    .line 234
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 237
    :cond_a
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 245
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 246
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 250
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 251
    :sswitch_0
    return-object p0

    .line 256
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 257
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 261
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    .line 262
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasResult:Z

    goto :goto_0

    .line 268
    .end local v4    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->instrumentId:Ljava/lang/String;

    .line 269
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasInstrumentId:Z

    goto :goto_0

    .line 273
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    .line 274
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasUserMessageHtml:Z

    goto :goto_0

    .line 278
    :sswitch_4
    const/16 v6, 0x22

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 280
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    if-nez v6, :cond_2

    move v1, v5

    .line 281
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .line 283
    .local v2, "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    if-eqz v1, :cond_1

    .line 284
    iget-object v6, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 286
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 287
    new-instance v6, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;-><init>()V

    aput-object v6, v2, v1

    .line 288
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 289
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 286
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 280
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v1, v6

    goto :goto_1

    .line 292
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;-><init>()V

    aput-object v6, v2, v1

    .line 293
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 294
    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    goto :goto_0

    .line 298
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->checkoutTokenRequired:Z

    .line 299
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasCheckoutTokenRequired:Z

    goto :goto_0

    .line 303
    :sswitch_6
    iget-object v6, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-nez v6, :cond_4

    .line 304
    new-instance v6, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    .line 306
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 246
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    .line 257
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 179
    iget v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasResult:Z

    if-eqz v2, :cond_1

    .line 180
    :cond_0
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 182
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasInstrumentId:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->instrumentId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 183
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->instrumentId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 185
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasUserMessageHtml:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 186
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->userMessageHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 188
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 189
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 190
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    aget-object v0, v2, v1

    .line 191
    .local v0, "element":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    if-eqz v0, :cond_6

    .line 192
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 189
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 196
    .end local v0    # "element":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .end local v1    # "i":I
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->hasCheckoutTokenRequired:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->checkoutTokenRequired:Z

    if-eqz v2, :cond_9

    .line 197
    :cond_8
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->checkoutTokenRequired:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 199
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v2, :cond_a

    .line 200
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 202
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 203
    return-void
.end method
