.class Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$4;
.super Ljava/lang/Object;
.source "AddressChallengeStep.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->setupWidgets(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$4;->this$0:Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 238
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$4;->this$0:Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;

    # getter for: Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    invoke-static {v4}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->access$000(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    move-result-object v4

    iget-object v0, v4, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 239
    .local v2, "inputValidationError":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    iget-object v4, p0, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep$4;->this$0:Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;

    # getter for: Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;
    invoke-static {v4}, Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;->access$200(Lcom/google/android/finsky/billing/giftcard/steps/AddressChallengeStep;)Lcom/google/android/finsky/layout/BillingAddress;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/google/android/finsky/layout/BillingAddress;->displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;

    .line 238
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 241
    .end local v2    # "inputValidationError":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    :cond_0
    return-void
.end method
