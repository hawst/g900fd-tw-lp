.class public Lcom/google/android/finsky/utils/RestrictedDeviceHelper;
.super Ljava/lang/Object;
.source "RestrictedDeviceHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;,
        Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;
    }
.end annotation


# static fields
.field private static sIsEduDevice:Ljava/lang/Boolean;

.field private static sIsSchoolOwnedEduDevice:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 31
    sput-object v0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsEduDevice:Ljava/lang/Boolean;

    .line 32
    sput-object v0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsSchoolOwnedEduDevice:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$002(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Boolean;

    .prologue
    .line 24
    sput-object p0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsSchoolOwnedEduDevice:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;

    .prologue
    .line 24
    invoke-static {p0}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->isSchoolOwnedEduDeviceAsync(Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;)V

    return-void
.end method

.method private static isEduDevice()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 65
    sget-object v2, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsEduDevice:Ljava/lang/Boolean;

    if-nez v2, :cond_0

    .line 66
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    const-string v3, "device_policy"

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 69
    .local v0, "dpm":Landroid/app/admin/DevicePolicyManager;
    :try_start_0
    sget-object v2, Lcom/google/android/finsky/config/G;->eduDevicePolicyApp:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/admin/DevicePolicyManager;->isDeviceOwnerApp(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsEduDevice:Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 78
    :cond_0
    :goto_0
    sget-object v2, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsEduDevice:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    return v2

    .line 70
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/NoSuchMethodError;
    const-string v2, "No method named isDeviceOwnerApp exists."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 72
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsEduDevice:Ljava/lang/Boolean;

    goto :goto_0

    .line 73
    .end local v1    # "e":Ljava/lang/NoSuchMethodError;
    :catch_1
    move-exception v1

    .line 74
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v2, "Unable to invoke isDeviceOwnerApp"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 75
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsEduDevice:Ljava/lang/Boolean;

    goto :goto_0
.end method

.method public static isLimitedOrSchoolEduUser()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getUsers()Lcom/google/android/finsky/utils/Users;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/Users;->isLimitedUser()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/config/G;->debugLimitedUserState:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    :cond_0
    const/4 v0, 0x1

    .line 56
    :goto_0
    return v0

    .line 46
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v0, v2, :cond_2

    move v0, v1

    .line 48
    goto :goto_0

    .line 50
    :cond_2
    sget-object v0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsSchoolOwnedEduDevice:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 51
    sget-object v0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsSchoolOwnedEduDevice:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0

    .line 53
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->isEduDevice()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 54
    invoke-static {}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->isSchoolOwnedEduDevice()Z

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 56
    goto :goto_0
.end method

.method private static isSchoolOwnedEduDevice()Z
    .locals 4

    .prologue
    .line 85
    new-instance v1, Ljava/util/concurrent/FutureTask;

    new-instance v2, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$1;

    invoke-direct {v2}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$1;-><init>()V

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 97
    .local v1, "futureTask":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<Ljava/lang/Void;>;"
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->run()V

    .line 99
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    .line 100
    sget-object v2, Lcom/google/android/finsky/utils/RestrictedDeviceHelper;->sIsSchoolOwnedEduDevice:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 102
    :goto_0
    return v2

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/Throwable;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isSchoolOwnedEduDeviceAsync(Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;)V
    .locals 6
    .param p0, "listener"    # Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;

    .prologue
    .line 111
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    .line 112
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;

    const/4 v3, 0x0

    invoke-direct {v0, v1, p0, v3}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;-><init>(Landroid/content/Context;Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;Lcom/google/android/finsky/utils/RestrictedDeviceHelper$1;)V

    .line 113
    .local v0, "conn":Landroid/content/ServiceConnection;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.nfcprovision.IOwnedService.BIND"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 114
    .local v2, "intent":Landroid/content/Intent;
    new-instance v3, Landroid/content/ComponentName;

    const-string v4, "com.google.android.nfcprovision"

    const-string v5, "com.google.android.nfcprovision.SchoolOwnedService"

    invoke-direct {v3, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 115
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 117
    const/4 v3, 0x0

    invoke-interface {p0, v3}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;->onComplete(Z)V

    .line 119
    :cond_0
    return-void
.end method
