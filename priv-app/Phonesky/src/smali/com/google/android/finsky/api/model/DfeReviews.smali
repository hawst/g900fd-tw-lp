.class public Lcom/google/android/finsky/api/model/DfeReviews;
.super Lcom/google/android/finsky/api/model/PaginatedList;
.source "DfeReviews.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/api/model/PaginatedList",
        "<",
        "Lcom/google/android/finsky/protos/Rev$ReviewResponse;",
        "Lcom/google/android/finsky/protos/DocumentV2$Review;",
        ">;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Rev$ReviewResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mFilterByDevice:Z

.field private mFilterByVersion:Z

.field private mRating:I

.field private mRottenTomatoesReviewData:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

.field private mSortType:I

.field private mVersionCode:I


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "reviewsUrl"    # Ljava/lang/String;
    .param p3, "versionCode"    # I
    .param p4, "autoLoadNextPage"    # Z

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p2, p4}, Lcom/google/android/finsky/api/model/PaginatedList;-><init>(Ljava/lang/String;Z)V

    .line 36
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 37
    iput-boolean v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mFilterByVersion:Z

    .line 38
    iput-boolean v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mFilterByDevice:Z

    .line 39
    iput v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mRating:I

    .line 40
    iput p3, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mVersionCode:I

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mSortType:I

    .line 42
    return-void
.end method


# virtual methods
.method protected clearDiskCache()V
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public currentlyFilteringByVersion()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mFilterByVersion:Z

    return v0
.end method

.method protected getItemsFromResponse(Lcom/google/android/finsky/protos/Rev$ReviewResponse;)[Lcom/google/android/finsky/protos/DocumentV2$Review;
    .locals 1
    .param p1, "response"    # Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .prologue
    .line 92
    iget-object v0, p1, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p1, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->criticReviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    iput-object v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mRottenTomatoesReviewData:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mRottenTomatoesReviewData:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 96
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->getResponse:Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Rev$GetReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    goto :goto_0
.end method

.method protected bridge synthetic getItemsFromResponse(Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeReviews;->getItemsFromResponse(Lcom/google/android/finsky/protos/Rev$ReviewResponse;)[Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v0

    return-object v0
.end method

.method protected getNextPageUrl(Lcom/google/android/finsky/protos/Rev$ReviewResponse;)Ljava/lang/String;
    .locals 1
    .param p1, "response"    # Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .prologue
    .line 102
    iget-object v0, p1, Lcom/google/android/finsky/protos/Rev$ReviewResponse;->nextPageUrl:Ljava/lang/String;

    return-object v0
.end method

.method protected bridge synthetic getNextPageUrl(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeReviews;->getNextPageUrl(Lcom/google/android/finsky/protos/Rev$ReviewResponse;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRatingFilter()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mRating:I

    return v0
.end method

.method public getRottenTomatoesData()Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mRottenTomatoesReviewData:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    return-object v0
.end method

.method public getSortType()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mSortType:I

    return v0
.end method

.method public getVersionFilter()I
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mFilterByVersion:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mVersionCode:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method protected makeRequest(Ljava/lang/String;)Lcom/android/volley/Request;
    .locals 8
    .param p1, "url"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-boolean v2, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mFilterByDevice:Z

    iget-boolean v1, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mFilterByVersion:Z

    if-eqz v1, :cond_0

    iget v3, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mVersionCode:I

    :goto_0
    iget v4, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mRating:I

    iget v5, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mSortType:I

    move-object v1, p1

    move-object v6, p0

    move-object v7, p0

    invoke-interface/range {v0 .. v7}, Lcom/google/android/finsky/api/DfeApi;->getReviews(Ljava/lang/String;ZIIILcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public refetchReviews()V
    .locals 0

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeReviews;->resetItems()V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeReviews;->startLoadItems()V

    .line 47
    return-void
.end method

.method public setFilters(ZZ)V
    .locals 0
    .param p1, "filterByVersion"    # Z
    .param p2, "filterByDevice"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mFilterByVersion:Z

    .line 63
    iput-boolean p2, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mFilterByDevice:Z

    .line 64
    return-void
.end method

.method public setSortType(I)V
    .locals 0
    .param p1, "sortType"    # I

    .prologue
    .line 77
    iput p1, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mSortType:I

    .line 78
    return-void
.end method

.method public shouldFilterByDevice()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/finsky/api/model/DfeReviews;->mFilterByDevice:Z

    return v0
.end method
