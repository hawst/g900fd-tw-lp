.class public Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
.super Lcom/google/android/finsky/fragments/PageFragment;
.source "MyAppsTabbedFragment.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;


# instance fields
.field private mBreadcrumb:Ljava/lang/String;

.field private mFragmentObjectMap:Lcom/google/android/finsky/utils/ObjectMap;

.field private mFrameworkBundle:Landroid/os/Bundle;

.field mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

.field private mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mViewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/PageFragment;-><init>()V

    .line 80
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFrameworkBundle:Landroid/os/Bundle;

    .line 87
    new-instance v0, Lcom/google/android/finsky/utils/ObjectMap;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/ObjectMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFragmentObjectMap:Lcom/google/android/finsky/utils/ObjectMap;

    .line 96
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private archiveDocs(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "docids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 477
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    .line 478
    .local v0, "eventLogger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    .line 479
    .local v8, "numDocIds":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v8, :cond_0

    .line 480
    const/16 v1, 0x1fa

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const/4 v4, 0x0

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 479
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 486
    :cond_0
    const v1, 0x7f0c0376

    invoke-static {v1}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->newInstance(I)Lcom/google/android/finsky/billing/ProgressDialogFragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    .line 487
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "progress_dialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 488
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    sget-object v2, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_APPS:Ljava/lang/String;

    new-instance v3, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$2;

    invoke-direct {v3, p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$2;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;Ljava/util/List;)V

    new-instance v4, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$3;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$3;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V

    invoke-interface {v1, p1, v2, v3, v4}, Lcom/google/android/finsky/api/DfeApi;->archiveFromLibrary(Ljava/util/Collection;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 517
    return-void
.end method

.method private cleanupActionModeIfNecessary()Z
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->finishActiveMode()Z

    move-result v0

    return v0
.end method

.method private clearState()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 314
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 315
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 316
    .local v0, "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-virtual {v0, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 318
    .end local v0    # "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_1

    .line 319
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 320
    iput-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 323
    :cond_1
    iput-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    .line 324
    return-void
.end method

.method private configureViewPager()V
    .locals 5

    .prologue
    .line 424
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    const v3, 0x7f0a021c

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    iput-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    .line 425
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-nez v2, :cond_0

    .line 440
    :goto_0
    return-void

    .line 429
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 430
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0110

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 433
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 434
    .local v1, "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->notifyPagerAdapterChanged()V

    .line 435
    invoke-virtual {v1, p0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 437
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v0

    .line 439
    .local v0, "corpusPrimaryColor":I
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private dismissProgressDialog()V
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->dismiss()V

    .line 445
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    .line 447
    :cond_0
    return-void
.end method

.method private fetchPermissionsAndInstall(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 525
    .local p1, "documents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "progress_dialog"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 570
    :goto_0
    return-void

    .line 529
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->cleanupActionModeIfNecessary()Z

    .line 531
    const/4 v3, 0x0

    invoke-static {v3}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/ProgressDialogFragment;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    .line 532
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "progress_dialog"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 534
    invoke-static {p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getDocIdList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v2

    .line 538
    .local v2, "docIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    .line 539
    .local v0, "accountName":Ljava/lang/String;
    new-instance v1, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-direct {v1, v3, v2}, Lcom/google/android/finsky/api/model/DfeBulkDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/util/List;)V

    .line 540
    .local v1, "dfeModel":Lcom/google/android/finsky/api/model/DfeBulkDetails;
    new-instance v3, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;Lcom/google/android/finsky/api/model/DfeBulkDetails;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 559
    new-instance v3, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$5;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$5;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    goto :goto_0
.end method

.method private static getDocIdList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573
    .local p0, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 574
    .local v1, "docids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 575
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 577
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    return-object v1
.end method

.method public static newInstance(Lcom/google/android/finsky/api/model/DfeToc;)Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
    .locals 1
    .param p0, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;-><init>()V

    .line 107
    .local v0, "fragment":Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->setDfeToc(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 109
    return-object v0
.end method

.method private recordState()V
    .locals 4

    .prologue
    .line 338
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFragmentObjectMap:Lcom/google/android/finsky/utils/ObjectMap;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->onSaveInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 347
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v1, "MyAppsTabbedAdapter.CurrentTabType"

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getTabType(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 351
    :cond_1
    return-void
.end method


# virtual methods
.method public confirmArchiveDocs(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 454
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v3, "archive_confirm"

    invoke-virtual {v1, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 474
    :goto_0
    return-void

    .line 458
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v4, :cond_1

    .line 459
    const v3, 0x7f0c0377

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 464
    .local v8, "message":Ljava/lang/String;
    :goto_1
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 465
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v0, v8}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f0c02a0

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v1

    const v3, 0x7f0c0134

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 466
    invoke-static {p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getDocIdList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v7

    .line 467
    .local v7, "docids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 468
    .local v9, "params":Landroid/os/Bundle;
    const-string v1, "docid_list"

    invoke-virtual {v9, v1, v7}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 469
    const/4 v1, 0x6

    invoke-virtual {v0, p0, v1, v9}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 470
    const/16 v1, 0x13d

    const/16 v3, 0x10d

    const/16 v4, 0x10e

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 472
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v6

    .line 473
    .local v6, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "archive_confirm"

    invoke-virtual {v6, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 462
    .end local v0    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v6    # "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .end local v7    # "docids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "message":Ljava/lang/String;
    .end local v9    # "params":Landroid/os/Bundle;
    :cond_1
    const v1, 0x7f0c0378

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "message":Ljava/lang/String;
    goto :goto_1
.end method

.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 251
    const v0, 0x7f0400ae

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method handleMenuItem(Ljava/util/List;Landroid/view/MenuItem;)Z
    .locals 1
    .param p2, "menuItem"    # Landroid/view/MenuItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Landroid/view/MenuItem;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 584
    .local p1, "documents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 589
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 586
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->fetchPermissionsAndInstall(Ljava/util/List;)V

    .line 587
    const/4 v0, 0x1

    goto :goto_0

    .line 584
    :pswitch_data_0
    .packed-switch 0x7f0a03cf
        :pswitch_0
    .end packed-switch
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x1

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 199
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 201
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getCorpusMyCollectionDescription(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "breadcrumbText":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 204
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f0c0260

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 207
    :cond_0
    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mBreadcrumb:Ljava/lang/String;

    .line 210
    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFrameworkBundle:Landroid/os/Bundle;

    invoke-virtual {v2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 211
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFrameworkBundle:Landroid/os/Bundle;

    .line 214
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    .line 217
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v1

    .line 218
    .local v1, "notifier":Lcom/google/android/finsky/utils/Notifier;
    invoke-interface {v1}, Lcom/google/android/finsky/utils/Notifier;->hideUpdatesAvailableMessage()V

    .line 220
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->isDataReady()Z

    move-result v2

    if-nez v2, :cond_2

    .line 221
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->switchToLoading()V

    .line 222
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->requestData()V

    .line 223
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->rebindActionBar()V

    .line 228
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-interface {v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->enableActionBarOverlay()V

    .line 229
    return-void

    .line 225
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->rebindViews()V

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 602
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->cleanupActionModeIfNecessary()Z

    move-result v0

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 114
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 115
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->setRetainInstance(Z)V

    .line 116
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 120
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/PageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/ContentFrame;

    .line 122
    .local v1, "frame":Lcom/google/android/finsky/layout/ContentFrame;
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v2, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;

    .line 123
    .local v2, "headerListLayout":Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;
    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 124
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$1;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 190
    const v3, 0x7f0a021c

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setContentViewId(I)V

    .line 191
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const/4 v5, 0x3

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 194
    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 355
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->cleanupActionModeIfNecessary()Z

    .line 356
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->recordState()V

    .line 357
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->clearState()V

    .line 359
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    instance-of v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->detach()V

    .line 365
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDestroyView()V

    .line 366
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    .prologue
    .line 306
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 414
    packed-switch p1, :pswitch_data_0

    .line 421
    :goto_0
    return-void

    .line 416
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->refresh()V

    goto :goto_0

    .line 414
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "scrollState"    # I

    .prologue
    .line 377
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 381
    return-void
.end method

.method public onPageSelected(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 385
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->onPageSelected(I)V

    .line 388
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getPageTitle(I)Ljava/lang/String;

    move-result-object v0

    .line 389
    .local v0, "selectedTabTitle":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;

    const v3, 0x7f0c03b2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 394
    :cond_0
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 399
    packed-switch p1, :pswitch_data_0

    .line 406
    const-string v1, "Unexpected requestCode %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 409
    :goto_0
    return-void

    .line 401
    :pswitch_0
    const-string v1, "docid_list"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 402
    .local v0, "docids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->cleanupActionModeIfNecessary()Z

    .line 403
    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->archiveDocs(Ljava/util/List;)V

    goto :goto_0

    .line 399
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 328
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->recordState()V

    .line 329
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFrameworkBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 330
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 331
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 233
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onStart()V

    .line 234
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    .line 236
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->dismissProgressDialog()V

    .line 237
    return-void
.end method

.method public rebindActionBar()V
    .locals 3

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 302
    return-void
.end method

.method public rebindViews()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 262
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->switchToData()V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->rebindActionBar()V

    .line 264
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    if-eqz v0, :cond_0

    .line 295
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/library/Libraries;->hasSubscriptions()Z

    move-result v6

    .line 273
    .local v6, "hasSubscriptions":Z
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/activities/AuthenticatedActivity;

    .line 275
    .local v1, "authenticatedActivity":Lcom/google/android/finsky/activities/AuthenticatedActivity;
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v7, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFragmentObjectMap:Lcom/google/android/finsky/utils/ObjectMap;

    move-object v8, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;-><init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/play/image/BitmapLoader;ZLcom/google/android/finsky/utils/ObjectMap;Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    .line 279
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->configureViewPager()V

    .line 281
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v2, "MyAppsTabbedAdapter.CurrentTabType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mFrameworkBundle:Landroid/os/Bundle;

    const-string v2, "MyAppsTabbedAdapter.CurrentTabType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 284
    .local v9, "defaultTabType":I
    :goto_1
    const/4 v11, 0x0

    .line 285
    .local v11, "indexOfSelectedItem":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getCount()I

    move-result v0

    if-ge v10, v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mTabbedAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;

    invoke-virtual {v0, v10}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedAdapter;->getTabType(I)I

    move-result v0

    if-ne v0, v9, :cond_3

    .line 287
    move v11, v10

    .line 292
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v11, v0, v12}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->onPageScrolled(IFI)V

    .line 293
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v11, v12}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    goto :goto_0

    .line 281
    .end local v9    # "defaultTabType":I
    .end local v10    # "i":I
    .end local v11    # "indexOfSelectedItem":I
    :cond_2
    const/4 v9, 0x1

    goto :goto_1

    .line 285
    .restart local v9    # "defaultTabType":I
    .restart local v10    # "i":I
    .restart local v11    # "indexOfSelectedItem":I
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->rebindViews()V

    .line 247
    :goto_0
    return-void

    .line 245
    :cond_0
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->refresh()V

    goto :goto_0
.end method

.method protected requestData()V
    .locals 0

    .prologue
    .line 310
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->clearState()V

    .line 311
    return-void
.end method
