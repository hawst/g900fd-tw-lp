.class public Lcom/google/android/finsky/layout/DetailsSectionStack;
.super Landroid/widget/LinearLayout;
.source "DetailsSectionStack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/DetailsSectionStack$NoBottomSeparator;,
        Lcom/google/android/finsky/layout/DetailsSectionStack$NoTopSeparator;
    }
.end annotation


# instance fields
.field private final mHalfSeparatorThickness:I

.field private final mMaxSectionSeparatorAlpha:I

.field private mSectionSeparatorAlphaMultiplier:F

.field private final mSectionSeparatorColor:I

.field private final mSectionSeparatorInset:I

.field private final mSectionSeparatorPaint:Landroid/graphics/Paint;

.field private mShowSeparators:Z

.field private mShowTrailingSeparator:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsSectionStack;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 71
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 72
    .local v0, "res":Landroid/content/res/Resources;
    sget-object v3, Lcom/android/vending/R$styleable;->DetailsSectionStack:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 74
    .local v2, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v2, v5, v5}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorInset:I

    .line 76
    invoke-virtual {v2, v6, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mShowTrailingSeparator:Z

    .line 78
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 80
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorPaint:Landroid/graphics/Paint;

    .line 81
    const v3, 0x7f090053

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorColor:I

    .line 82
    iget v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorColor:I

    invoke-static {v3}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mMaxSectionSeparatorAlpha:I

    .line 83
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorPaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorColor:I

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 84
    const v3, 0x7f0b0052

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 86
    .local v1, "separatorThickness":I
    const/4 v3, 0x2

    invoke-static {v1, v3}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mHalfSeparatorThickness:I

    .line 87
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorPaint:Landroid/graphics/Paint;

    int-to-float v4, v1

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 89
    iput-boolean v6, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mShowSeparators:Z

    .line 90
    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorAlphaMultiplier:F

    .line 92
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/layout/DetailsSectionStack;->setWillNotDraw(Z)V

    .line 93
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 118
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 120
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mShowSeparators:Z

    if-nez v0, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    const/4 v10, 0x1

    .line 135
    .local v10, "isFirstVisibleChild":Z
    const/4 v12, 0x0

    .line 137
    .local v12, "skipNextVisibleChild":Z
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSectionStack;->getChildCount()I

    move-result v7

    .line 138
    .local v7, "childCount":I
    const/4 v11, 0x0

    .line 139
    .local v11, "lastVisibleChild":Landroid/view/View;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v7, :cond_6

    .line 140
    invoke-virtual {p0, v9}, Lcom/google/android/finsky/layout/DetailsSectionStack;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 141
    .local v8, "currChild":Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 139
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 145
    :cond_2
    move-object v11, v8

    .line 147
    if-nez v12, :cond_3

    instance-of v0, v8, Lcom/google/android/finsky/layout/DetailsSectionStack$NoTopSeparator;

    if-eqz v0, :cond_4

    .line 148
    :cond_3
    instance-of v12, v8, Lcom/google/android/finsky/layout/DetailsSectionStack$NoBottomSeparator;

    .line 149
    const/4 v10, 0x0

    .line 150
    goto :goto_2

    .line 153
    :cond_4
    if-nez v10, :cond_5

    .line 154
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mHalfSeparatorThickness:I

    add-int v13, v0, v1

    .line 155
    .local v13, "topY":I
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorInset:I

    add-int/2addr v0, v1

    int-to-float v1, v0

    int-to-float v2, v13

    invoke-virtual {v8}, Landroid/view/View;->getRight()I

    move-result v0

    iget v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorInset:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v13

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 160
    .end local v13    # "topY":I
    :cond_5
    instance-of v12, v8, Lcom/google/android/finsky/layout/DetailsSectionStack$NoBottomSeparator;

    .line 161
    const/4 v10, 0x0

    goto :goto_2

    .line 164
    .end local v8    # "currChild":Landroid/view/View;
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mShowTrailingSeparator:Z

    if-eqz v0, :cond_0

    if-eqz v11, :cond_0

    .line 165
    invoke-virtual {v11}, Landroid/view/View;->getBottom()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mHalfSeparatorThickness:I

    sub-int v6, v0, v1

    .line 166
    .local v6, "bottomY":I
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorInset:I

    add-int/2addr v0, v1

    int-to-float v1, v0

    int-to-float v2, v6

    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v0

    iget v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorInset:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setSectionSeparatorAlphaMultiplier(F)V
    .locals 5
    .param p1, "sectionSeparatorAlphaMultiplier"    # F

    .prologue
    .line 103
    iget v2, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorAlphaMultiplier:F

    cmpl-float v2, v2, p1

    if-eqz v2, :cond_0

    .line 104
    iput p1, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorAlphaMultiplier:F

    .line 106
    iget v2, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorAlphaMultiplier:F

    iget v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mMaxSectionSeparatorAlpha:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 108
    .local v1, "newSeparatorColorAlpha":I
    shl-int/lit8 v2, v1, 0x18

    iget v3, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorColor:I

    const v4, 0xffffff

    and-int/2addr v3, v4

    or-int v0, v2, v3

    .line 111
    .local v0, "newSeparatorColor":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mSectionSeparatorPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSectionStack;->invalidate()V

    .line 114
    .end local v0    # "newSeparatorColor":I
    .end local v1    # "newSeparatorColorAlpha":I
    :cond_0
    return-void
.end method

.method public setSeparatorsVisible(Z)V
    .locals 1
    .param p1, "showSeparators"    # Z

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mShowSeparators:Z

    if-eq v0, p1, :cond_0

    .line 97
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/DetailsSectionStack;->mShowSeparators:Z

    .line 98
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsSectionStack;->invalidate()V

    .line 100
    :cond_0
    return-void
.end method
