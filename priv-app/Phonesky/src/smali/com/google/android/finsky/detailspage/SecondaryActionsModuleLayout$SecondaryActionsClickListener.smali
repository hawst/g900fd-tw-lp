.class public interface abstract Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout$SecondaryActionsClickListener;
.super Ljava/lang/Object;
.source "SecondaryActionsModuleLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/detailspage/SecondaryActionsModuleLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SecondaryActionsClickListener"
.end annotation


# virtual methods
.method public abstract onPlusOneClick(Landroid/view/View;)V
.end method

.method public abstract onShareClick(Landroid/view/View;)V
.end method

.method public abstract onWishlistClick(Landroid/view/View;)V
.end method
