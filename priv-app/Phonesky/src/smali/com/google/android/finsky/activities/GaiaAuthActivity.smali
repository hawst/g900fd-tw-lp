.class public Lcom/google/android/finsky/activities/GaiaAuthActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "GaiaAuthActivity.java"

# interfaces
.implements Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;


# instance fields
.field private mGaiaAuthFragment:Lcom/google/android/finsky/fragments/GaiaAuthFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method public static getIntent(Landroid/content/Context;Ljava/lang/String;ZZZLandroid/os/Bundle;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "showWarning"    # Z
    .param p3, "useGmsCoreForAuth"    # Z
    .param p4, "usePinBasedAuth"    # Z
    .param p5, "extraParams"    # Landroid/os/Bundle;

    .prologue
    .line 40
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/activities/GaiaAuthActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 41
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "GaiaAuthActivity_useGmsCoreForAuth"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 42
    const-string v1, "GaiaAuthActivity_usePinBasedAuth"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 43
    const-string v1, "GaiaAuthActivity_accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    const-string v1, "GaiaAuthActivity_showWarning"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 45
    const-string v1, "GaiaAuthActivity_extraParams"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 46
    return-object v0
.end method


# virtual methods
.method public onCancel()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->setResult(I)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->finish()V

    .line 95
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 51
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const v3, 0x7f0400a9

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->setContentView(I)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "GaiaAuthActivity_usePinBasedAuth"

    invoke-virtual {v3, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 55
    .local v2, "usePinBasedAuth":Z
    if-eqz v2, :cond_0

    const v3, 0x7f0c02cf

    :goto_0
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 58
    if-eqz p1, :cond_1

    .line 59
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const-string v4, "GaiaAuthActivity_GaiaAuthFragment"

    invoke-virtual {v3, p1, v4}, Landroid/support/v4/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    iput-object v3, p0, Lcom/google/android/finsky/activities/GaiaAuthActivity;->mGaiaAuthFragment:Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    .line 61
    iget-object v3, p0, Lcom/google/android/finsky/activities/GaiaAuthActivity;->mGaiaAuthFragment:Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->setListener(Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;)V

    .line 74
    :goto_1
    return-void

    .line 55
    :cond_0
    const v3, 0x7f0c02ce

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 64
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "GaiaAuthActivity_accountName"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "GaiaAuthActivity_showWarning"

    invoke-virtual {v0, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "GaiaAuthActivity_useGmsCoreForAuth"

    invoke-virtual {v0, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v6, "GaiaAuthActivity_usePinBasedAuth"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static {v3, v4, v5, v6}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->newInstance(Ljava/lang/String;ZZZ)Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/activities/GaiaAuthActivity;->mGaiaAuthFragment:Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    .line 69
    iget-object v3, p0, Lcom/google/android/finsky/activities/GaiaAuthActivity;->mGaiaAuthFragment:Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->setListener(Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 71
    .local v1, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v3, 0x7f0a00c4

    iget-object v4, p0, Lcom/google/android/finsky/activities/GaiaAuthActivity;->mGaiaAuthFragment:Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    invoke-virtual {v1, v3, v4}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 72
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "GaiaAuthActivity_GaiaAuthFragment"

    iget-object v2, p0, Lcom/google/android/finsky/activities/GaiaAuthActivity;->mGaiaAuthFragment:Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    invoke-virtual {v0, p1, v1, v2}, Landroid/support/v4/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 80
    return-void
.end method

.method public onSuccess(II)V
    .locals 4
    .param p1, "authenticationType"    # I
    .param p2, "retryCount"    # I

    .prologue
    .line 84
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 85
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "GaiaAuthActivity_extraParams"

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "GaiaAuthActivity_extraParams"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 87
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->setResult(ILandroid/content/Intent;)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->finish()V

    .line 89
    return-void
.end method
