.class Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;
.super Ljava/lang/Object;
.source "AuthChallengeStep.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->onAuthFailure(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "bundleAccountManagerFuture":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 444
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->isResumed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 445
    const-string v3, "Not resumed, ignoring account manager auth."

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 469
    :goto_0
    return-void

    .line 449
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 450
    .local v2, "result":Landroid/os/Bundle;
    const-string v3, "booleanResult"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 451
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->onAuthSuccess()V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 459
    .end local v2    # "result":Landroid/os/Bundle;
    :catch_0
    move-exception v0

    .line 460
    .local v0, "e":Landroid/accounts/OperationCanceledException;
    const-string v3, "OperationCanceledException: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 461
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->fail()V
    invoke-static {v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->access$100(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;)V

    goto :goto_0

    .line 452
    .end local v0    # "e":Landroid/accounts/OperationCanceledException;
    .restart local v2    # "result":Landroid/os/Bundle;
    :cond_1
    :try_start_1
    const-string v3, "intent"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 453
    const-string v3, "intent"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 454
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    const/16 v4, 0x65

    invoke-virtual {v3, v1, v4}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 462
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "result":Landroid/os/Bundle;
    :catch_1
    move-exception v0

    .line 463
    .local v0, "e":Ljava/io/IOException;
    const-string v3, "IOException: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 464
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->fail()V
    invoke-static {v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->access$100(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;)V

    goto :goto_0

    .line 457
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "result":Landroid/os/Bundle;
    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->fail()V
    invoke-static {v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->access$100(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;)V
    :try_end_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 465
    .end local v2    # "result":Landroid/os/Bundle;
    :catch_2
    move-exception v0

    .line 466
    .local v0, "e":Landroid/accounts/AuthenticatorException;
    const-string v3, "AuthenticatorException: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 467
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep$2;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->fail()V
    invoke-static {v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;->access$100(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/AuthChallengeStep;)V

    goto :goto_0
.end method
