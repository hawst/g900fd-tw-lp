.class public Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;
.super Landroid/widget/BaseAdapter;
.source "MyAppsInstalledAdapter.java"

# interfaces
.implements Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;,
        Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;,
        Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;,
        Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;,
        Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;
    }
.end annotation


# static fields
.field private static final sDocumentAbcCollator:Ljava/text/Collator;

.field private static final sDocumentAbcSorter:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/adapters/AggregatedAdapter",
            "<",
            "Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private final mAppStates:Lcom/google/android/finsky/appstate/AppStates;

.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private final mBucketsChangedListener:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;

.field protected mContext:Landroid/content/Context;

.field private final mDownloadingSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

.field private final mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

.field private final mInstalledSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

.field private final mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field protected final mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mLeadingSpacerHeight:I

.field private final mOnClickListener:Landroid/view/View$OnClickListener;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mRecentlyUpdatedSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

.field private final mUnsortedDocuments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private final mUpdatesSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->sDocumentAbcCollator:Ljava/text/Collator;

    .line 89
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$1;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->sDocumentAbcSorter:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/play/image/BitmapLoader;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "installer"    # Lcom/google/android/finsky/receivers/Installer;
    .param p3, "installThresholds"    # Lcom/google/android/finsky/installer/InstallPolicies;
    .param p4, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "onClickListener"    # Landroid/view/View$OnClickListener;
    .param p7, "bucketsChangedListener"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 286
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 69
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUnsortedDocuments:Ljava/util/List;

    .line 83
    iput-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 287
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mContext:Landroid/content/Context;

    .line 288
    iput-object p2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    .line 289
    iput-object p3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    .line 290
    iput-object p4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    .line 291
    iput-object p6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 292
    iput-object p5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 293
    iput-object p8, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 295
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 297
    iput-object p7, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mBucketsChangedListener:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;

    .line 299
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    sget-object v1, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;->DOWNLOADING:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;

    const v2, 0x7f0c01c4

    sget-object v3, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;->STOP_ALL_DOWNLOADS:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;ILcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mDownloadingSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    .line 301
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    sget-object v1, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;->UPDATE_AVAILABLE:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;

    const v2, 0x7f0c01c5

    sget-object v3, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;->UPDATE_ALL:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;ILcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUpdatesSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    .line 303
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    sget-object v1, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;->RECENTLY_UPDATED:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;

    const v2, 0x7f0c01c6

    invoke-direct {v0, p0, v1, v2, v5}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;ILcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mRecentlyUpdatedSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    .line 305
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    sget-object v1, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;->INSTALLED:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;

    const v2, 0x7f0c01c7

    invoke-direct {v0, p0, v1, v2, v5}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;ILcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstalledSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    .line 308
    new-instance v0, Lcom/google/android/finsky/adapters/AggregatedAdapter;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mDownloadingSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    aput-object v2, v1, v4

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUpdatesSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mRecentlyUpdatedSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstalledSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/finsky/adapters/AggregatedAdapter;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    .line 313
    invoke-static {p1, v4, v4}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mLeadingSpacerHeight:I

    .line 315
    return-void
.end method

.method static synthetic access$000()Ljava/text/Collator;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->sDocumentAbcCollator:Ljava/text/Collator;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->sDocumentAbcSorter:Ljava/util/Comparator;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUnsortedDocuments:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;)Lcom/google/android/finsky/installer/InstallPolicies;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;Lcom/google/android/finsky/api/model/Document;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;
    .param p1, "x1"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # Landroid/view/ViewGroup;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->getDownloadingDocView(Lcom/google/android/finsky/api/model/Document;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;Lcom/google/android/finsky/api/model/Document;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;Z)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;
    .param p1, "x1"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # Landroid/view/ViewGroup;
    .param p4, "x4"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;
    .param p5, "x5"    # Z

    .prologue
    .line 49
    invoke-direct/range {p0 .. p5}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->getDocView(Lcom/google/android/finsky/api/model/Document;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;ILandroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # Landroid/view/ViewGroup;
    .param p4, "x4"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;)Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUpdatesSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;)Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mDownloadingSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;)Lcom/google/android/finsky/receivers/Installer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    return-object v0
.end method

.method private dumpState()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 634
    const-string v7, "****** INSTALLED ADAPTER START ******"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 635
    const-string v7, "Total docs: %d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUnsortedDocuments:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 636
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "Total items: "

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 637
    .local v2, "countBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->getCount()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 638
    const-string v7, " [ "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 639
    iget-object v7, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    invoke-virtual {v7}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->getAdapters()[Landroid/widget/BaseAdapter;

    move-result-object v1

    check-cast v1, [Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    .local v1, "arr$":[Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, v1, v4

    .line 640
    .local v0, "adapter":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->getCount()I

    move-result v7

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 641
    const-string v7, " "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 639
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 643
    .end local v0    # "adapter":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    :cond_0
    const-string v7, "]"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 644
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 645
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Index translation: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 646
    .local v6, "viewTypeBuilder":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->getCount()I

    move-result v7

    if-ge v3, v7, :cond_1

    .line 647
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 648
    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->getItemViewType(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 650
    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 646
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 652
    :cond_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 653
    const-string v7, "****** INSTALLED ADAPTER  END  ******"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 655
    iget-object v7, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    invoke-virtual {v7}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->dumpState()V

    .line 656
    return-void
.end method

.method private getDocView(Lcom/google/android/finsky/api/model/Document;Landroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;Z)Landroid/view/View;
    .locals 7
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "documentState"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentState;
    .param p5, "isLastInSection"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 393
    if-nez p2, :cond_0

    .line 394
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f04012a

    invoke-virtual {v1, v2, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    move-object v0, p2

    .line 398
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;

    .line 399
    .local v0, "entry":Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;
    const-string v2, "my_apps:installed"

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 400
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 401
    invoke-virtual {v0, v6, v4}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setArchivable(ZLcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;)V

    .line 403
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setTag(Ljava/lang/Object;)V

    .line 404
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setIdentifier(Ljava/lang/String;)V

    .line 406
    return-object p2
.end method

.method private getDownloadingDocView(Lcom/google/android/finsky/api/model/Document;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 411
    if-nez p2, :cond_0

    .line 412
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v2, 0x7f04012b

    invoke-virtual {v1, v2, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 416
    :cond_0
    instance-of v1, p2, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;

    if-nez v1, :cond_1

    .line 417
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->dumpState()V

    :cond_1
    move-object v0, p2

    .line 420
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;

    .line 421
    .local v0, "card":Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;
    const-string v2, "my_apps:installed"

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 424
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/finsky/receivers/Installer;->getProgress(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    move-result-object v7

    .line 426
    .local v7, "progress":Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;
    invoke-virtual {v0, v7}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->bindProgress(Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;)V

    .line 428
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 431
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v10}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getTitleContentDescriptionResourceId(Landroid/content/res/Resources;I)I

    move-result v8

    .line 434
    .local v8, "titleDescriptionResourceId":I
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->getTitle()Landroid/widget/TextView;

    move-result-object v6

    .line 435
    .local v6, "cardTitle":Landroid/widget/TextView;
    if-eqz v6, :cond_2

    .line 436
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mContext:Landroid/content/Context;

    new-array v2, v10, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v1, v8, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 440
    :cond_2
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->setTag(Ljava/lang/Object;)V

    .line 441
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->setIdentifier(Ljava/lang/String;)V

    .line 443
    return-object p2
.end method

.method private getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "sectionAdapter"    # Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    .prologue
    .line 448
    move-object v0, p2

    check-cast v0, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;

    .line 449
    .local v0, "header":Lcom/google/android/finsky/layout/IdentifiableLinearLayout;
    if-nez v0, :cond_0

    .line 450
    iget-object v7, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f0400df

    const/4 v9, 0x0

    invoke-virtual {v7, v8, p3, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .end local v0    # "header":Lcom/google/android/finsky/layout/IdentifiableLinearLayout;
    check-cast v0, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;

    .line 454
    .restart local v0    # "header":Lcom/google/android/finsky/layout/IdentifiableLinearLayout;
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 455
    .local v1, "headerTag":Ljava/lang/Object;
    instance-of v7, v1, Lcom/google/android/finsky/api/model/Document;

    if-eqz v7, :cond_1

    .line 456
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->dumpState()V

    :cond_1
    move-object v2, v1

    .line 459
    check-cast v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;

    .line 461
    .local v2, "holder":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;
    if-nez v2, :cond_2

    .line 462
    new-instance v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;

    .end local v2    # "holder":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;
    invoke-direct {v2, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;-><init>(Landroid/view/View;)V

    .line 465
    .restart local v2    # "holder":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;
    :cond_2
    iget-object v7, v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;->titleView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mContext:Landroid/content/Context;

    # getter for: Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->mHeaderTextId:I
    invoke-static {p4}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->access$1100(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 468
    iget-object v7, v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;->bulkActionButton:Lcom/google/android/play/layout/PlayActionButton;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 469
    iget-object v7, v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;->loadingProgress:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 471
    # getter for: Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->mHeaderAction:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;
    invoke-static {p4}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->access$1200(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;)Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;

    move-result-object v6

    .line 472
    .local v6, "sectionAction":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;
    if-eqz v6, :cond_6

    invoke-virtual {v6, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;->isVisible(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;)Z

    move-result v7

    if-eqz v7, :cond_6

    const/4 v3, 0x1

    .line 473
    .local v3, "isActionVisible":Z
    :goto_0
    if-eqz v6, :cond_7

    invoke-virtual {v6, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;->isWaiting(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;)Z

    move-result v7

    if-eqz v7, :cond_7

    const/4 v4, 0x1

    .line 475
    .local v4, "isActionWaiting":Z
    :goto_1
    if-eqz v4, :cond_8

    .line 476
    iget-object v7, v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;->loadingProgress:Landroid/view/View;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 490
    :cond_3
    :goto_2
    if-nez v4, :cond_4

    if-nez v3, :cond_9

    :cond_4
    const/4 v5, 0x1

    .line 491
    .local v5, "isCountVisible":Z
    :goto_3
    iget-object v8, v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;->countView:Landroid/widget/TextView;

    if-eqz v5, :cond_a

    const/4 v7, 0x0

    :goto_4
    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 492
    if-eqz v5, :cond_5

    .line 493
    iget-object v7, v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;->countView:Landroid/widget/TextView;

    invoke-virtual {p4}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 496
    :cond_5
    # getter for: Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->mHeaderIdentifier:Ljava/lang/String;
    invoke-static {p4}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->access$1400(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/android/finsky/layout/IdentifiableLinearLayout;->setIdentifier(Ljava/lang/String;)V

    .line 498
    return-object v0

    .line 472
    .end local v3    # "isActionVisible":Z
    .end local v4    # "isActionWaiting":Z
    .end local v5    # "isCountVisible":Z
    :cond_6
    const/4 v3, 0x0

    goto :goto_0

    .line 473
    .restart local v3    # "isActionVisible":Z
    :cond_7
    const/4 v4, 0x0

    goto :goto_1

    .line 477
    .restart local v4    # "isActionWaiting":Z
    :cond_8
    if-eqz v3, :cond_3

    .line 478
    iget-object v7, v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;->bulkActionButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 479
    iget-object v7, v2, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionHeaderHolder;->bulkActionButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v8, 0x3

    iget-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;->getLabelId()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$2;

    invoke-direct {v10, p0, v6}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$2;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$DocumentBulkAction;)V

    invoke-virtual {v7, v8, v9, v10}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 490
    :cond_9
    const/4 v5, 0x0

    goto :goto_3

    .line 491
    .restart local v5    # "isCountVisible":Z
    :cond_a
    const/16 v7, 0x8

    goto :goto_4
.end method

.method public static getViewDoc(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;
    .locals 2
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 626
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 627
    .local v0, "tag":Ljava/lang/Object;
    instance-of v1, v0, Lcom/google/android/finsky/api/model/Document;

    if-eqz v1, :cond_0

    .line 628
    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 630
    .end local v0    # "tag":Ljava/lang/Object;
    :goto_0
    return-object v0

    .restart local v0    # "tag":Ljava/lang/Object;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private putDocsInBuckets()V
    .locals 20

    .prologue
    .line 570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->getAdapters()[Landroid/widget/BaseAdapter;

    move-result-object v3

    check-cast v3, [Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    .line 571
    .local v3, "adapters":[Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    move-object v5, v3

    .local v5, "arr$":[Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    array-length v10, v5

    .local v10, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v10, :cond_0

    aget-object v2, v5, v7

    .line 572
    .local v2, "adapter":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    invoke-virtual {v2}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->clearDocs()V

    .line 571
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 575
    .end local v2    # "adapter":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v13

    .line 577
    .local v13, "skipped":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sget-object v16, Lcom/google/android/finsky/config/G;->recentlyUpdatedWindowSizeMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Long;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    sub-long v14, v18, v16

    .line 581
    .local v14, "recentlyUpdatedTimeThresholdMs":J
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUnsortedDocuments:Ljava/util/List;

    move-object/from16 v16, v0

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/api/model/Document;

    .line 582
    .local v6, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v4

    .line 583
    .local v4, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    iget-object v11, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 584
    .local v11, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v12

    .line 585
    .local v12, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v9

    .line 586
    .local v9, "installerState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v8

    .line 588
    .local v8, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {v9}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->isDownloadingOrInstalling()Z

    move-result v16

    if-eqz v16, :cond_1

    .line 590
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mDownloadingSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->addDoc(Lcom/google/android/finsky/api/model/Document;)V

    goto :goto_1

    .line 591
    :cond_1
    if-eqz v12, :cond_2

    iget-boolean v0, v12, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabled:Z

    move/from16 v16, v0

    if-eqz v16, :cond_3

    .line 593
    :cond_2
    invoke-interface {v13, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 594
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstallPolicies:Lcom/google/android/finsky/installer/InstallPolicies;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v12, v6}, Lcom/google/android/finsky/installer/InstallPolicies;->canUpdateApp(Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;Lcom/google/android/finsky/api/model/Document;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 596
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUpdatesSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->addDoc(Lcom/google/android/finsky/api/model/Document;)V

    goto :goto_1

    .line 597
    :cond_4
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getLastUpdateTimestampMs()J

    move-result-wide v16

    cmp-long v16, v16, v14

    if-lez v16, :cond_5

    .line 600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mRecentlyUpdatedSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->addDoc(Lcom/google/android/finsky/api/model/Document;)V

    goto/16 :goto_1

    .line 603
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mInstalledSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->addDoc(Lcom/google/android/finsky/api/model/Document;)V

    goto/16 :goto_1

    .line 609
    .end local v4    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    .end local v6    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v8    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .end local v9    # "installerState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    .end local v11    # "packageName":Ljava/lang/String;
    .end local v12    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUnsortedDocuments:Ljava/util/List;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v0, v13}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 611
    move-object v5, v3

    array-length v10, v5

    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_2
    if-ge v7, v10, :cond_7

    aget-object v2, v5, v7

    .line 613
    .restart local v2    # "adapter":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    invoke-virtual {v2}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->sortDocs()V

    .line 611
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 615
    .end local v2    # "adapter":Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;
    :cond_7
    return-void
.end method


# virtual methods
.method public addDocs(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 502
    .local p1, "docs":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUnsortedDocuments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 503
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mUnsortedDocuments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 504
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->notifyDataSetChanged()V

    .line 505
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 350
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    invoke-virtual {v1}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->getCount()I

    move-result v0

    .line 351
    .local v0, "count":I
    if-nez v0, :cond_0

    .line 354
    const/4 v1, 0x0

    .line 357
    :goto_0
    return v1

    :cond_0
    add-int/lit8 v1, v0, 0x1

    goto :goto_0
.end method

.method public getDocument(I)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 619
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 376
    if-nez p1, :cond_0

    .line 377
    const/4 v0, 0x0

    .line 379
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 319
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 324
    if-nez p1, :cond_0

    .line 325
    const/4 v0, 0x3

    .line 327
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 362
    if-nez p1, :cond_1

    .line 363
    if-nez p2, :cond_0

    .line 364
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f04014d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 367
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mLeadingSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 368
    const v0, 0x7f0a0029

    invoke-virtual {p2, v0}, Landroid/view/View;->setId(I)V

    move-object v0, p2

    .line 371
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 332
    const/4 v0, 0x4

    return v0
.end method

.method public invalidateInstallingItem(Ljava/lang/String;Landroid/widget/ListView;)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "list"    # Landroid/widget/ListView;

    .prologue
    .line 528
    if-nez p2, :cond_1

    .line 557
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    const/4 v5, 0x0

    .line 534
    .local v5, "target":Lcom/google/android/finsky/api/model/Document;
    iget-object v8, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mDownloadingSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    invoke-virtual {v8}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->getCount()I

    move-result v6

    .line 535
    .local v6, "totalCount":I
    const/4 v4, 0x1

    .local v4, "position":I
    :goto_1
    if-ge v4, v6, :cond_2

    .line 536
    iget-object v8, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mDownloadingSectionAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;

    invoke-virtual {v8, v4}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$SectionAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 537
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v8

    iget-object v8, v8, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 538
    move-object v5, v0

    .line 542
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_2
    if-eqz v5, :cond_0

    .line 546
    invoke-virtual {p2}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    .line 547
    .local v1, "firstPosition":I
    invoke-virtual {p2}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v3

    .line 548
    .local v3, "lastPosition":I
    move v2, v1

    .local v2, "i":I
    :goto_2
    if-gt v2, v3, :cond_0

    .line 549
    invoke-virtual {p2, v2}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v8

    if-ne v5, v8, :cond_4

    .line 551
    sub-int v8, v2, v1

    invoke-virtual {p2, v8}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 553
    .local v7, "view":Landroid/view/View;
    invoke-virtual {p2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v8

    invoke-interface {v8, v2, v7, p2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_0

    .line 535
    .end local v1    # "firstPosition":I
    .end local v2    # "i":I
    .end local v3    # "lastPosition":I
    .end local v7    # "view":Landroid/view/View;
    .restart local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 548
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .restart local v1    # "firstPosition":I
    .restart local v2    # "i":I
    .restart local v3    # "lastPosition":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 342
    if-nez p1, :cond_0

    .line 343
    const/4 v0, 0x0

    .line 345
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 509
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->putDocsInBuckets()V

    .line 510
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->notifyDataSetChanged()V

    .line 511
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 512
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mBucketsChangedListener:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mBucketsChangedListener:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;

    invoke-interface {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;->bucketsChanged()V

    .line 515
    :cond_0
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 1

    .prologue
    .line 519
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->putDocsInBuckets()V

    .line 520
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mAggregatedAdapter:Lcom/google/android/finsky/adapters/AggregatedAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/AggregatedAdapter;->notifyDataSetInvalidated()V

    .line 521
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    .line 522
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mBucketsChangedListener:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;

    if-eqz v0, :cond_0

    .line 523
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter;->mBucketsChangedListener:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;

    invoke-interface {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledAdapter$BucketsChangedListener;->bucketsChanged()V

    .line 525
    :cond_0
    return-void
.end method
