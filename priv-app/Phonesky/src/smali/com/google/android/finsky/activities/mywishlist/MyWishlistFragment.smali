.class public Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;
.super Lcom/google/android/finsky/fragments/PageFragment;
.source "MyWishlistFragment.java"


# instance fields
.field private mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

.field private mBreadcrumb:Ljava/lang/String;

.field private mDfeList:Lcom/google/android/finsky/api/model/DfeList;

.field private mIsAdapterSet:Z

.field private mLastLoadedTimeMs:J

.field private mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

.field private mRecyclerViewRestoreBundle:Landroid/os/Bundle;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/PageFragment;-><init>()V

    .line 54
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    .line 59
    const/4 v0, 0x5

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDataView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private getLibraryList()Lcom/google/android/finsky/api/model/DfeList;
    .locals 6

    .prologue
    .line 209
    iget-object v2, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v2

    const-string v3, "u-wl"

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/library/AccountLibrary;->getServerToken(Ljava/lang/String;)[B

    move-result-object v1

    .line 211
    .local v1, "serverToken":[B
    iget-object v2, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    const/4 v3, 0x0

    const-string v4, "u-wl"

    const/4 v5, 0x7

    invoke-interface {v2, v3, v4, v5, v1}, Lcom/google/android/finsky/api/DfeApi;->getLibraryUrl(ILjava/lang/String;I[B)Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "libraryUrl":Ljava/lang/String;
    new-instance v2, Lcom/google/android/finsky/api/model/DfeList;

    iget-object v3, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    const/4 v4, 0x1

    invoke-direct {v2, v3, v0, v4}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    return-object v2
.end method

.method public static newInstance()Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;-><init>()V

    .line 64
    .local v0, "fragment":Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->setDfeToc(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 65
    return-object v0
.end method

.method private rebindAdapter()V
    .locals 11

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    if-nez v0, :cond_0

    .line 251
    const-string v0, "Recycler view null, ignoring."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 295
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->hasRestoreData(Landroid/os/Bundle;)Z

    move-result v9

    .line 257
    .local v9, "isRestoring":Z
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    if-nez v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v10

    .line 259
    .local v10, "cookie":[B
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v0, v10}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 261
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v6

    .line 263
    .local v6, "clientMutationCache":Lcom/google/android/finsky/utils/ClientMutationCache;
    new-instance v0, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v4, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    iget-object v7, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    move-object v8, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/DfeList;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    .line 265
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    invoke-static {v0}, Lcom/google/android/finsky/utils/WishlistHelper;->addWishlistStatusListener(Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;)V

    .line 269
    .end local v6    # "clientMutationCache":Lcom/google/android/finsky/utils/ClientMutationCache;
    .end local v10    # "cookie":[B
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mIsAdapterSet:Z

    if-nez v0, :cond_2

    .line 270
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mIsAdapterSet:Z

    .line 273
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    new-instance v1, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;

    invoke-direct {v1, p0, v9}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$2;-><init>(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 293
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V

    goto :goto_0
.end method


# virtual methods
.method protected clearDfeList()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 204
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 206
    :cond_0
    return-void
.end method

.method protected createLayoutSwitcher(Lcom/google/android/finsky/layout/ContentFrame;)Lcom/google/android/finsky/layout/LayoutSwitcher;
    .locals 7
    .param p1, "frame"    # Lcom/google/android/finsky/layout/ContentFrame;

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;

    const v2, 0x7f0a0219

    const v3, 0x7f0a02a3

    const v4, 0x7f0a0109

    const/4 v6, 0x2

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;-><init>(Landroid/view/View;IIILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;I)V

    return-object v0
.end method

.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 179
    const v0, 0x7f0400ae

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-nez v0, :cond_0

    .line 185
    const/4 v0, 0x0

    .line 187
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 139
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 141
    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f0c0359

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mBreadcrumb:Ljava/lang/String;

    .line 143
    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDataView:Landroid/view/ViewGroup;

    const v2, 0x7f0a03ca

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    iput-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    .line 144
    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setVisibility(I)V

    .line 145
    new-instance v0, Landroid/support/v7/widget/LinearLayoutManager;

    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 146
    .local v0, "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 147
    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    new-instance v2, Lcom/google/android/finsky/adapters/EmptyRecyclerViewAdapter;

    invoke-direct {v2}, Lcom/google/android/finsky/adapters/EmptyRecyclerViewAdapter;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 148
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->rebindActionBar()V

    .line 151
    iget-wide v2, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mLastLoadedTimeMs:J

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/WishlistHelper;->hasMutationOccurredSince(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->clearDfeList()V

    .line 154
    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->clear()V

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->isDataReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 158
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->requestData()V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->switchToLoading()V

    .line 167
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-interface {v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->enableActionBarOverlay()V

    .line 168
    return-void

    .line 164
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->rebindAdapter()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->setRetainInstance(Z)V

    .line 72
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 73
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/PageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/ContentFrame;

    .line 81
    .local v1, "frame":Lcom/google/android/finsky/layout/ContentFrame;
    iget-object v2, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v2, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;

    .line 82
    .local v2, "headerListLayout":Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;
    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 83
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment$1;-><init>(Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 133
    const v3, 0x7f0a03c9

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setContentViewId(I)V

    .line 134
    return-object v1
.end method

.method public onDataChanged()V
    .locals 0

    .prologue
    .line 218
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDataChanged()V

    .line 219
    invoke-direct {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->rebindAdapter()V

    .line 220
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 304
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    iget-object v2, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->onSaveInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 309
    :cond_0
    iput-object v3, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mMyWishlistView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    .line 310
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    if-eqz v0, :cond_1

    .line 311
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->onDestroyView()V

    .line 312
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;->onDestroy()V

    .line 313
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    invoke-static {v0}, Lcom/google/android/finsky/utils/WishlistHelper;->removeWishlistStatusListener(Lcom/google/android/finsky/utils/WishlistHelper$WishlistStatusListener;)V

    .line 314
    iput-object v3, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mAdapter:Lcom/google/android/finsky/adapters/MyWishlistRecyclerViewAdapter;

    .line 315
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mIsAdapterSet:Z

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDataView:Landroid/view/ViewGroup;

    instance-of v0, v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_2

    .line 321
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->detach()V

    .line 324
    :cond_2
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDestroyView()V

    .line 325
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    .prologue
    .line 333
    return-void
.end method

.method public rebindActionBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 224
    iget-object v3, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDataView:Landroid/view/ViewGroup;

    if-eqz v3, :cond_0

    .line 225
    iget-object v2, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v2, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 226
    .local v2, "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    iget-object v3, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mContext:Landroid/content/Context;

    invoke-static {v3, v5}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v0

    .line 228
    .local v0, "actionBarColor":I
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 229
    const v3, 0x7f0a0039

    invoke-virtual {v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 232
    .local v1, "backgroundLayer":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 233
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 237
    .end local v0    # "actionBarColor":I
    .end local v1    # "backgroundLayer":Landroid/view/View;
    .end local v2    # "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v4, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-interface {v3, v4}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 238
    iget-object v3, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    const/4 v4, 0x1

    invoke-interface {v3, v5, v4}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 239
    return-void
.end method

.method protected rebindViews()V
    .locals 0

    .prologue
    .line 329
    return-void
.end method

.method protected requestData()V
    .locals 2

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->clearDfeList()V

    .line 193
    invoke-direct {p0}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->getLibraryList()Lcom/google/android/finsky/api/model/DfeList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 197
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->mLastLoadedTimeMs:J

    .line 198
    return-void
.end method
