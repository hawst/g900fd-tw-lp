.class Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$1;
.super Ljava/lang/Object;
.source "PlayCardRateClusterView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayCardViewRate$RateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

.field final synthetic val$rateCard:Lcom/google/android/finsky/layout/play/PlayCardViewRate;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$1;->val$rateCard:Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRate(IZ)V
    .locals 2
    .param p1, "rating"    # I
    .param p2, "committed"    # Z

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mRejectTouchEvents:Z
    invoke-static {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->access$002(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Z)Z

    .line 131
    if-eqz p2, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$1;->val$rateCard:Lcom/google/android/finsky/layout/play/PlayCardViewRate;

    # invokes: Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->fadeOutCard(Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->access$100(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Lcom/google/android/finsky/layout/play/PlayCardViewRate;)V

    .line 134
    :cond_0
    return-void
.end method

.method public onRateCleared()V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$1;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mRejectTouchEvents:Z
    invoke-static {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->access$002(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;Z)Z

    .line 139
    return-void
.end method
