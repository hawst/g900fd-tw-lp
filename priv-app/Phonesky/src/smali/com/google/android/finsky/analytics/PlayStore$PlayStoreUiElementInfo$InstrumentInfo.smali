.class public final Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstrumentInfo"
.end annotation


# instance fields
.field public hasInstrumentFamily:Z

.field public hasIsDefault:Z

.field public instrumentFamily:I

.field public isDefault:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    .line 38
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->instrumentFamily:I

    .line 42
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasInstrumentFamily:Z

    .line 43
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->isDefault:Z

    .line 44
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasIsDefault:Z

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->cachedSize:I

    .line 46
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 64
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasInstrumentFamily:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->instrumentFamily:I

    if-eqz v1, :cond_1

    .line 65
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->instrumentFamily:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasIsDefault:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->isDefault:Z

    if-eqz v1, :cond_3

    .line 69
    :cond_2
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->isDefault:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 80
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 81
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 85
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 86
    :sswitch_0
    return-object p0

    .line 91
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->instrumentFamily:I

    .line 92
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasInstrumentFamily:Z

    goto :goto_0

    .line 96
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->isDefault:Z

    .line 97
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasIsDefault:Z

    goto :goto_0

    .line 81
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasInstrumentFamily:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->instrumentFamily:I

    if-eqz v0, :cond_1

    .line 53
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->instrumentFamily:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 55
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasIsDefault:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->isDefault:Z

    if-eqz v0, :cond_3

    .line 56
    :cond_2
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->isDefault:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 58
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 59
    return-void
.end method
