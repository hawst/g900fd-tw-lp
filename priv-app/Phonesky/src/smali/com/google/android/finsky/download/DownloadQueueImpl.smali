.class public Lcom/google/android/finsky/download/DownloadQueueImpl;
.super Ljava/lang/Object;
.source "DownloadQueueImpl.java"

# interfaces
.implements Lcom/google/android/finsky/download/DownloadQueue;
.implements Lcom/google/android/finsky/download/DownloadQueueListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/download/DownloadQueueImpl$8;,
        Lcom/google/android/finsky/download/DownloadQueueImpl$PurgeCacheCallback;,
        Lcom/google/android/finsky/download/DownloadQueueImpl$StartNextDownloadRunnable;,
        Lcom/google/android/finsky/download/DownloadQueueImpl$ListenerNotifier;,
        Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

.field private mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;

.field private mListeners:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/finsky/download/DownloadQueueListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mMaxConcurrent:I

.field private mPendingQueue:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/download/Download;",
            ">;"
        }
    .end annotation
.end field

.field private mPreviousContentUri:Landroid/net/Uri;

.field private mPreviousProgressStatus:I

.field private mRunningMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/download/Download;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/google/android/finsky/download/DownloadManagerFacade;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "maxConcurrent"    # I
    .param p3, "downloadManager"    # Lcom/google/android/finsky/download/DownloadManagerFacade;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPreviousContentUri:Landroid/net/Uri;

    .line 86
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPreviousProgressStatus:I

    .line 90
    invoke-direct {p0}, Lcom/google/android/finsky/download/DownloadQueueImpl;->setupQueue()V

    .line 91
    iput p2, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mMaxConcurrent:I

    .line 92
    iput-object p3, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    .line 93
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mContext:Landroid/content/Context;

    .line 94
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mListeners:Ljava/util/LinkedList;

    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/download/DownloadManagerFacade;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "downloadManager"    # Lcom/google/android/finsky/download/DownloadManagerFacade;

    .prologue
    .line 99
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/finsky/download/DownloadQueueImpl;-><init>(Landroid/content/Context;ILcom/google/android/finsky/download/DownloadManagerFacade;)V

    .line 100
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/download/DownloadQueueImpl;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadQueueImpl;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mListeners:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/download/DownloadQueueImpl;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadQueueImpl;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/download/DownloadQueueImpl;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadQueueImpl;

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mMaxConcurrent:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/download/DownloadQueueImpl;)Ljava/util/LinkedHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadQueueImpl;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/download/DownloadQueueImpl;)Lcom/google/android/finsky/download/DownloadProgressManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadQueueImpl;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;

    return-object v0
.end method

.method static synthetic access$402(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadProgressManager;)Lcom/google/android/finsky/download/DownloadProgressManager;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadQueueImpl;
    .param p1, "x1"    # Lcom/google/android/finsky/download/DownloadProgressManager;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/finsky/download/DownloadQueueImpl;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadQueueImpl;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/finsky/download/DownloadQueueImpl;)Lcom/google/android/finsky/download/DownloadManagerFacade;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/download/DownloadQueueImpl;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    return-object v0
.end method

.method private enqueueDownload(Lcom/google/android/finsky/download/Download;)V
    .locals 2
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    new-instance v1, Lcom/google/android/finsky/download/DownloadQueueImpl$7;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl$7;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/Download;)V

    invoke-interface {v0, p1, v1}, Lcom/google/android/finsky/download/DownloadManagerFacade;->enqueue(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/utils/ParameterizedRunnable;)V

    .line 564
    return-void
.end method

.method private remove(Lcom/google/android/finsky/download/Download;)V
    .locals 5
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 318
    const-string v1, "Download %s removed from DownloadQueue"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 320
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 321
    .local v0, "url":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 322
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    :goto_0
    return-void

    .line 326
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    invoke-direct {p0}, Lcom/google/android/finsky/download/DownloadQueueImpl;->startNextDownload()V

    goto :goto_0
.end method

.method private removeFromDownloadManager(Lcom/google/android/finsky/download/Download;)V
    .locals 2
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 620
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    .line 621
    .local v0, "contentUri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 622
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    invoke-interface {v1, v0}, Lcom/google/android/finsky/download/DownloadManagerFacade;->remove(Landroid/net/Uri;)V

    .line 624
    :cond_0
    return-void
.end method

.method private setupQueue()V
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newLinkedHashMap()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    .line 104
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    .line 105
    return-void
.end method

.method private startNextDownload()V
    .locals 9

    .prologue
    .line 509
    iget-object v6, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    iget v7, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mMaxConcurrent:I

    if-lt v6, v7, :cond_0

    .line 525
    :goto_0
    return-void

    .line 513
    :cond_0
    const-wide/16 v2, 0x0

    .line 514
    .local v2, "largestDownloadSize":J
    iget-object v6, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v6}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 515
    .local v4, "pendingDownloadKeyset":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 517
    .local v5, "urls":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 518
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 519
    .local v1, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v6, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/download/Download;

    .line 520
    .local v0, "current":Lcom/google/android/finsky/download/Download;
    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getMaximumSize()J

    move-result-wide v6

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 521
    goto :goto_1

    .line 522
    .end local v0    # "current":Lcom/google/android/finsky/download/Download;
    .end local v1    # "key":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mContext:Landroid/content/Context;

    new-instance v7, Lcom/google/android/finsky/download/DownloadQueueImpl$PurgeCacheCallback;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/google/android/finsky/download/DownloadQueueImpl$PurgeCacheCallback;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadQueueImpl$1;)V

    invoke-static {v6, v2, v3, v7}, Lcom/google/android/finsky/utils/PackageManagerUtils;->freeStorageAndNotify(Landroid/content/Context;JLcom/google/android/finsky/utils/PackageManagerUtils$FreeSpaceListener;)V

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/google/android/finsky/download/Download;)V
    .locals 8
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 114
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 115
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getState()Lcom/google/android/finsky/download/Download$DownloadState;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/download/Download$DownloadState;->UNQUEUED:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/download/Download$DownloadState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    const-string v1, "Added download %s (url=%s) while in state %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getState()Lcom/google/android/finsky/download/Download$DownloadState;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    :cond_0
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->getExisting(Ljava/lang/String;)Lcom/google/android/finsky/download/Download;

    move-result-object v0

    .line 120
    .local v0, "existing":Lcom/google/android/finsky/download/Download;
    if-eqz v0, :cond_1

    .line 121
    const-string v1, "Added download %s (url=%s) while existing found %s (url=%s)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    aput-object v0, v2, v6

    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    :cond_1
    const-string v1, "Download %s added to DownloadQueue"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;

    if-nez v1, :cond_2

    .line 129
    new-instance v1, Lcom/google/android/finsky/download/DownloadProgressManager;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/download/DownloadProgressManager;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;)V

    iput-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;

    .line 131
    :cond_2
    sget-object v1, Lcom/google/android/finsky/download/Download$DownloadState;->QUEUED:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {p0, p1, v1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V

    .line 132
    invoke-direct {p0}, Lcom/google/android/finsky/download/DownloadQueueImpl;->startNextDownload()V

    .line 133
    return-void
.end method

.method public addListener(Lcom/google/android/finsky/download/DownloadQueueListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/finsky/download/DownloadQueueListener;

    .prologue
    .line 274
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 275
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 276
    return-void
.end method

.method public addRecoveredDownload(Lcom/google/android/finsky/download/Download;)V
    .locals 4
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 137
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 139
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "url":Ljava/lang/String;
    const-string v1, "Download queue recovering download %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 143
    sget-object v1, Lcom/google/android/finsky/download/Download$DownloadState;->DOWNLOADING:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {p0, p1, v1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V

    .line 144
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;

    if-nez v1, :cond_0

    .line 147
    new-instance v1, Lcom/google/android/finsky/download/DownloadProgressManager;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/download/DownloadProgressManager;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;)V

    iput-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadProgressManager:Lcom/google/android/finsky/download/DownloadProgressManager;

    .line 149
    :cond_0
    return-void
.end method

.method public cancel(Lcom/google/android/finsky/download/Download;)V
    .locals 2
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 333
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 337
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 344
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getState()Lcom/google/android/finsky/download/Download$DownloadState;

    move-result-object v0

    sget-object v1, Lcom/google/android/finsky/download/Download$DownloadState;->DOWNLOADING:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/download/Download$DownloadState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 341
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/download/DownloadManagerFacade;->remove(Landroid/net/Uri;)V

    .line 343
    :cond_2
    sget-object v0, Lcom/google/android/finsky/download/Download$DownloadState;->CANCELLED:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/download/DownloadQueueImpl;->setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V

    goto :goto_0
.end method

.method public getByPackageName(Ljava/lang/String;)Lcom/google/android/finsky/download/Download;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 163
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 164
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "empty packageName"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 167
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/download/Download;

    .line 168
    .local v1, "pending":Lcom/google/android/finsky/download/Download;
    invoke-interface {v1}, Lcom/google/android/finsky/download/Download;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 169
    .local v2, "pendingPackageName":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 179
    .end local v1    # "pending":Lcom/google/android/finsky/download/Download;
    .end local v2    # "pendingPackageName":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 173
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/download/Download;

    .line 174
    .local v3, "running":Lcom/google/android/finsky/download/Download;
    invoke-interface {v3}, Lcom/google/android/finsky/download/Download;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 175
    .local v4, "runningPackageName":Ljava/lang/String;
    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    move-object v1, v3

    .line 176
    goto :goto_0

    .line 179
    .end local v3    # "running":Lcom/google/android/finsky/download/Download;
    .end local v4    # "runningPackageName":Ljava/lang/String;
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDownloadByContentUri(Landroid/net/Uri;)Lcom/google/android/finsky/download/Download;
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x0

    .line 290
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 291
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 292
    .local v2, "uriString":Ljava/lang/String;
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 300
    :cond_0
    :goto_1
    return-object v3

    .end local v2    # "uriString":Ljava/lang/String;
    :cond_1
    move-object v2, v3

    .line 291
    goto :goto_0

    .line 295
    .restart local v2    # "uriString":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/download/Download;

    .line 296
    .local v0, "checkRunning":Lcom/google/android/finsky/download/Download;
    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v3, v0

    .line 297
    goto :goto_1
.end method

.method public getDownloadManager()Lcom/google/android/finsky/download/DownloadManagerFacade;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    return-object v0
.end method

.method getExisting(Ljava/lang/String;)Lcom/google/android/finsky/download/Download;
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/download/Download;

    .line 314
    :goto_0
    return-object v0

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 312
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPendingQueue:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/download/Download;

    goto :goto_0

    .line 314
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRunningDownloads()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 642
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    invoke-interface {v0, v1, v1}, Lcom/google/android/finsky/download/DownloadManagerFacade;->query(Landroid/net/Uri;Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public notifyClicked(Lcom/google/android/finsky/download/Download;)V
    .locals 4
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 353
    const-string v0, "%s: onNotificationClicked"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 354
    sget-object v0, Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;->NOTIFICATION_CLICKED:Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->notifyListeners(Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    .line 355
    return-void
.end method

.method notifyListeners(Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V
    .locals 5
    .param p1, "type"    # Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;
    .param p2, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 208
    if-nez p2, :cond_0

    const/4 v1, 0x0

    .line 210
    .local v1, "currentProgress":Lcom/google/android/finsky/download/DownloadProgress;
    :goto_0
    if-nez p2, :cond_1

    const/4 v0, -0x1

    .line 213
    .local v0, "currentHttpStatus":I
    :goto_1
    sget-object v3, Lcom/google/android/finsky/download/DownloadQueueImpl$8;->$SwitchMap$com$google$android$finsky$download$DownloadQueueImpl$UpdateListenerType:[I

    invoke-virtual {p1}, Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 263
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Bad listener type."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 208
    .end local v0    # "currentHttpStatus":I
    .end local v1    # "currentProgress":Lcom/google/android/finsky/download/DownloadProgress;
    :cond_0
    invoke-interface {p2}, Lcom/google/android/finsky/download/Download;->getProgress()Lcom/google/android/finsky/download/DownloadProgress;

    move-result-object v1

    goto :goto_0

    .line 210
    .restart local v1    # "currentProgress":Lcom/google/android/finsky/download/DownloadProgress;
    :cond_1
    invoke-interface {p2}, Lcom/google/android/finsky/download/Download;->getHttpStatus()I

    move-result v0

    goto :goto_1

    .line 215
    .restart local v0    # "currentHttpStatus":I
    :pswitch_0
    new-instance v2, Lcom/google/android/finsky/download/DownloadQueueImpl$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/finsky/download/DownloadQueueImpl$1;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    .line 265
    .local v2, "r":Ljava/lang/Runnable;
    :goto_2
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v3, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 266
    return-void

    .line 223
    .end local v2    # "r":Ljava/lang/Runnable;
    :pswitch_1
    new-instance v2, Lcom/google/android/finsky/download/DownloadQueueImpl$2;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/finsky/download/DownloadQueueImpl$2;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    .line 229
    .restart local v2    # "r":Ljava/lang/Runnable;
    goto :goto_2

    .line 231
    .end local v2    # "r":Ljava/lang/Runnable;
    :pswitch_2
    new-instance v2, Lcom/google/android/finsky/download/DownloadQueueImpl$3;

    invoke-direct {v2, p0, p1, p2, v1}, Lcom/google/android/finsky/download/DownloadQueueImpl$3;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V

    .line 237
    .restart local v2    # "r":Ljava/lang/Runnable;
    goto :goto_2

    .line 239
    .end local v2    # "r":Ljava/lang/Runnable;
    :pswitch_3
    new-instance v2, Lcom/google/android/finsky/download/DownloadQueueImpl$4;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/finsky/download/DownloadQueueImpl$4;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    .line 245
    .restart local v2    # "r":Ljava/lang/Runnable;
    goto :goto_2

    .line 247
    .end local v2    # "r":Ljava/lang/Runnable;
    :pswitch_4
    new-instance v2, Lcom/google/android/finsky/download/DownloadQueueImpl$5;

    invoke-direct {v2, p0, p1, p2, v0}, Lcom/google/android/finsky/download/DownloadQueueImpl$5;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;I)V

    .line 253
    .restart local v2    # "r":Ljava/lang/Runnable;
    goto :goto_2

    .line 255
    .end local v2    # "r":Ljava/lang/Runnable;
    :pswitch_5
    new-instance v2, Lcom/google/android/finsky/download/DownloadQueueImpl$6;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/finsky/download/DownloadQueueImpl$6;-><init>(Lcom/google/android/finsky/download/DownloadQueueImpl;Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    .line 261
    .restart local v2    # "r":Ljava/lang/Runnable;
    goto :goto_2

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public notifyProgress(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V
    .locals 5
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "progress"    # Lcom/google/android/finsky/download/DownloadProgress;

    .prologue
    .line 363
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getProgress()Lcom/google/android/finsky/download/DownloadProgress;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/google/android/finsky/download/DownloadProgress;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 385
    :goto_0
    return-void

    .line 366
    :cond_0
    invoke-interface {p1, p2}, Lcom/google/android/finsky/download/Download;->setProgress(Lcom/google/android/finsky/download/DownloadProgress;)V

    .line 370
    const/4 v0, 0x0

    .line 371
    .local v0, "logProgress":Z
    iget v1, p2, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    iget v2, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPreviousProgressStatus:I

    if-eq v1, v2, :cond_3

    .line 372
    const/4 v0, 0x1

    .line 377
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 378
    const-string v1, "%s: onProgress %s."

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/finsky/download/DownloadProgress;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 379
    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPreviousContentUri:Landroid/net/Uri;

    .line 380
    iget v1, p2, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    iput v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPreviousProgressStatus:I

    .line 384
    :cond_2
    sget-object v1, Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;->PROGRESS:Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;

    invoke-virtual {p0, v1, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->notifyListeners(Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    goto :goto_0

    .line 373
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPreviousContentUri:Landroid/net/Uri;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mPreviousContentUri:Landroid/net/Uri;

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 375
    :cond_4
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public onCancel(Lcom/google/android/finsky/download/Download;)V
    .locals 4
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 587
    const-string v0, "%s: onCancel"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 588
    invoke-direct {p0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->remove(Lcom/google/android/finsky/download/Download;)V

    .line 589
    invoke-direct {p0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->removeFromDownloadManager(Lcom/google/android/finsky/download/Download;)V

    .line 590
    return-void
.end method

.method public onComplete(Lcom/google/android/finsky/download/Download;)V
    .locals 4
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 577
    const-string v0, "%s: onComplete"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 578
    invoke-direct {p0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->remove(Lcom/google/android/finsky/download/Download;)V

    .line 579
    return-void
.end method

.method public onError(Lcom/google/android/finsky/download/Download;I)V
    .locals 5
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "httpStatus"    # I

    .prologue
    const/4 v4, 0x1

    .line 598
    const-string v0, "%s: onError %d."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 599
    const/16 v0, 0x193

    if-eq p2, v0, :cond_0

    const/16 v0, 0x191

    if-ne p2, v0, :cond_1

    .line 601
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getVendingApi()Lcom/google/android/vending/remoting/api/VendingApi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/vending/remoting/api/VendingApi;->getApiContext()Lcom/google/android/vending/remoting/api/VendingApiContext;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/vending/remoting/api/VendingApiContext;->scheduleReauthentication(Z)V

    .line 606
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->remove(Lcom/google/android/finsky/download/Download;)V

    .line 607
    invoke-direct {p0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->removeFromDownloadManager(Lcom/google/android/finsky/download/Download;)V

    .line 608
    return-void
.end method

.method public onNotificationClicked(Lcom/google/android/finsky/download/Download;)V
    .locals 0
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 631
    return-void
.end method

.method public onProgress(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/DownloadProgress;)V
    .locals 0
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "progress"    # Lcom/google/android/finsky/download/DownloadProgress;

    .prologue
    .line 638
    return-void
.end method

.method public onStart(Lcom/google/android/finsky/download/Download;)V
    .locals 4
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 616
    const-string v0, "%s: onStart"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 617
    return-void
.end method

.method public release(Landroid/net/Uri;)V
    .locals 1
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 647
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/download/DownloadManagerFacade;->remove(Landroid/net/Uri;)V

    .line 648
    return-void
.end method

.method public removeListener(Lcom/google/android/finsky/download/DownloadQueueListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/finsky/download/DownloadQueueListener;

    .prologue
    .line 284
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 285
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mListeners:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 286
    return-void
.end method

.method public setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V
    .locals 3
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;
    .param p2, "state"    # Lcom/google/android/finsky/download/Download$DownloadState;

    .prologue
    .line 400
    invoke-interface {p1, p2}, Lcom/google/android/finsky/download/Download;->setState(Lcom/google/android/finsky/download/Download$DownloadState;)V

    .line 402
    sget-object v0, Lcom/google/android/finsky/download/DownloadQueueImpl$8;->$SwitchMap$com$google$android$finsky$download$Download$DownloadState:[I

    invoke-virtual {p2}, Lcom/google/android/finsky/download/Download$DownloadState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 419
    const-string v0, "enum %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 422
    :goto_0
    :pswitch_0
    return-void

    .line 404
    :pswitch_1
    sget-object v0, Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;->START:Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->notifyListeners(Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    goto :goto_0

    .line 407
    :pswitch_2
    sget-object v0, Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;->CANCEL:Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->notifyListeners(Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    goto :goto_0

    .line 410
    :pswitch_3
    sget-object v0, Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;->ERROR:Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->notifyListeners(Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    goto :goto_0

    .line 413
    :pswitch_4
    sget-object v0, Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;->COMPLETE:Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->notifyListeners(Lcom/google/android/finsky/download/DownloadQueueImpl$UpdateListenerType;Lcom/google/android/finsky/download/Download;)V

    goto :goto_0

    .line 402
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method startDownload(Lcom/google/android/finsky/download/Download;)V
    .locals 4
    .param p1, "download"    # Lcom/google/android/finsky/download/Download;

    .prologue
    .line 528
    if-nez p1, :cond_0

    .line 534
    :goto_0
    return-void

    .line 531
    :cond_0
    const-string v0, "Download %s starting"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 532
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadQueueImpl;->mRunningMap:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/finsky/download/Download;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 533
    invoke-direct {p0, p1}, Lcom/google/android/finsky/download/DownloadQueueImpl;->enqueueDownload(Lcom/google/android/finsky/download/Download;)V

    goto :goto_0
.end method
