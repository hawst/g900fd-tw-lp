.class Lcom/google/android/finsky/layout/SongList$2;
.super Ljava/lang/Object;
.source "SongList.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/SongList;->setListDetails(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Ljava/util/List;ZLjava/util/Set;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/SongList;

.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$isMature:Z

.field final synthetic val$songListControl:Lcom/google/android/finsky/layout/PlaylistControlButtons;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/SongList;ZLandroid/accounts/Account;Landroid/content/Context;Lcom/google/android/finsky/layout/PlaylistControlButtons;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/finsky/layout/SongList$2;->this$0:Lcom/google/android/finsky/layout/SongList;

    iput-boolean p2, p0, Lcom/google/android/finsky/layout/SongList$2;->val$isMature:Z

    iput-object p3, p0, Lcom/google/android/finsky/layout/SongList$2;->val$account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/finsky/layout/SongList$2;->val$context:Landroid/content/Context;

    iput-object p5, p0, Lcom/google/android/finsky/layout/SongList$2;->val$songListControl:Lcom/google/android/finsky/layout/PlaylistControlButtons;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 135
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/SongList$2;->val$isMature:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/SongList$2;->val$account:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->isAgeVerificationRequired()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lcom/google/android/finsky/layout/SongList$2;->val$account:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/AgeVerificationActivity;->createIntent(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 140
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/layout/SongList$2;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 144
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/SongList$2;->val$songListControl:Lcom/google/android/finsky/layout/PlaylistControlButtons;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/layout/PlaylistControlButtons;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method
