.class public final Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResponseFormat"
.end annotation


# instance fields
.field public encryptionKey:[B

.field public hasEncryptionKey:Z

.field public hasResponseFormatType:Z

.field public hasVendorSpecificSalt:Z

.field public responseFormatType:I

.field public vendorSpecificSalt:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1493
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1494
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->clear()Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    .line 1495
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1498
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->responseFormatType:I

    .line 1499
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasResponseFormatType:Z

    .line 1500
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->encryptionKey:[B

    .line 1501
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasEncryptionKey:Z

    .line 1502
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->vendorSpecificSalt:Ljava/lang/String;

    .line 1503
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasVendorSpecificSalt:Z

    .line 1504
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->cachedSize:I

    .line 1505
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1525
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1526
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->responseFormatType:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasResponseFormatType:Z

    if-eqz v1, :cond_1

    .line 1527
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->responseFormatType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1530
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasEncryptionKey:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->encryptionKey:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1531
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->encryptionKey:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1534
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasVendorSpecificSalt:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->vendorSpecificSalt:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1535
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->vendorSpecificSalt:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1538
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1546
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1547
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1551
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1552
    :sswitch_0
    return-object p0

    .line 1557
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1558
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1562
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->responseFormatType:I

    .line 1563
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasResponseFormatType:Z

    goto :goto_0

    .line 1569
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->encryptionKey:[B

    .line 1570
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasEncryptionKey:Z

    goto :goto_0

    .line 1574
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->vendorSpecificSalt:Ljava/lang/String;

    .line 1575
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasVendorSpecificSalt:Z

    goto :goto_0

    .line 1547
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    .line 1558
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1459
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1511
    iget v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->responseFormatType:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasResponseFormatType:Z

    if-eqz v0, :cond_1

    .line 1512
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->responseFormatType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1514
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasEncryptionKey:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->encryptionKey:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1515
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->encryptionKey:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 1517
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->hasVendorSpecificSalt:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->vendorSpecificSalt:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1518
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ResponseFormat;->vendorSpecificSalt:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1520
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1521
    return-void
.end method
