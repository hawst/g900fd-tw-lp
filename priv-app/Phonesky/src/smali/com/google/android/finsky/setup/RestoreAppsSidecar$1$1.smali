.class Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$1;
.super Ljava/lang/Object;
.source "RestoreAppsSidecar.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->onTokenReceived(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$1;->this$1:Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$1;->this$1:Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;

    iget-object v0, v0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->this$0:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    iget-object v1, p1, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;->backupDocumentInfo:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    # setter for: Lcom/google/android/finsky/setup/RestoreAppsSidecar;->mBackupDocumentInfos:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    invoke-static {v0, v1}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->access$002(Lcom/google/android/finsky/setup/RestoreAppsSidecar;[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;)[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    .line 70
    iget-object v0, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$1;->this$1:Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;

    iget-object v0, v0, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;->this$0:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    const/4 v1, 0x5

    const/4 v2, 0x0

    # invokes: Lcom/google/android/finsky/setup/RestoreAppsSidecar;->setState(II)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->access$100(Lcom/google/android/finsky/setup/RestoreAppsSidecar;II)V

    .line 71
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 65
    check-cast p1, Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1$1;->onResponse(Lcom/google/android/finsky/protos/Restore$GetBackupDocumentChoicesResponse;)V

    return-void
.end method
