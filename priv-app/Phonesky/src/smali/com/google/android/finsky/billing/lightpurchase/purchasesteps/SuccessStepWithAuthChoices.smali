.class public Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "SuccessStepWithAuthChoices.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;",
        ">;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mBackend:I

.field private mEveryTimeButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mSessionButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mStepFragmentView:Landroid/view/View;

.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mUsedPinBasedAuth:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 39
    const/16 v0, 0x4e2

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method private finish()V
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    .line 138
    .local v0, "purchaseFragment":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->finish()V

    .line 141
    :cond_0
    return-void
.end method

.method public static newInstance(Ljava/lang/String;IZ)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "backend"    # I
    .param p2, "usedPinBasedAuth"    # Z

    .prologue
    .line 56
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;-><init>()V

    .line 58
    .local v1, "stepFragment":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v2, "SuccessStepWithAuthChoices.backend"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 60
    const-string v2, "SuccessStepWithAuthChoices.usedPinBasedAuth"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 61
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->setArguments(Landroid/os/Bundle;)V

    .line 62
    return-object v1
.end method

.method private setPurchaseAuth(I)V
    .locals 4
    .param p1, "purchaseAuth"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mAccountName:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mAccountName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    const-string v3, "success-step-with-choices"

    invoke-static {v0, p1, v1, v2, v3}, Lcom/google/android/finsky/config/PurchaseAuth;->setAndLogPurchaseAuth(Ljava/lang/String;ILjava/lang/Integer;Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;)V

    .line 112
    return-void
.end method


# virtual methods
.method public allowButtonBar()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 116
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mEveryTimeButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v0, :cond_1

    .line 99
    const/16 v0, 0x4e3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->logClick(I)V

    .line 100
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->setPurchaseAuth(I)V

    .line 105
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->finish()V

    .line 106
    return-void

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mSessionButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v0, :cond_0

    .line 102
    const/16 v0, 0x4e4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->logClick(I)V

    .line 103
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->setPurchaseAuth(I)V

    goto :goto_0
.end method

.method public onContinueButtonClicked()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 69
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mAccountName:Ljava/lang/String;

    .line 70
    const-string v1, "SuccessStepWithAuthChoices.backend"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mBackend:I

    .line 71
    const-string v1, "SuccessStepWithAuthChoices.usedPinBasedAuth"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mUsedPinBasedAuth:Z

    .line 72
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    const v1, 0x7f0400cc

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mStepFragmentView:Landroid/view/View;

    .line 81
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mStepFragmentView:Landroid/view/View;

    const v2, 0x7f0a00bb

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 82
    .local v0, "textView":Landroid/widget/TextView;
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mUsedPinBasedAuth:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0c0114

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 85
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 87
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mStepFragmentView:Landroid/view/View;

    const v2, 0x7f0a0251

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mEveryTimeButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 89
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mStepFragmentView:Landroid/view/View;

    const v2, 0x7f0a0252

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mSessionButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 91
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mEveryTimeButton:Lcom/google/android/play/layout/PlayActionButton;

    iget v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mBackend:I

    const v3, 0x7f0c03c5

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 92
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mSessionButton:Lcom/google/android/play/layout/PlayActionButton;

    iget v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mBackend:I

    const v3, 0x7f0c03c6

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 93
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithAuthChoices;->mStepFragmentView:Landroid/view/View;

    return-object v1

    .line 82
    :cond_0
    const v1, 0x7f0c0113

    goto :goto_0
.end method
