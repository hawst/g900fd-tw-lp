.class public Lcom/google/android/finsky/layout/SubscriptionsSection;
.super Lcom/google/android/finsky/layout/DetailsSectionStack;
.source "SubscriptionsSection.java"


# instance fields
.field private final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/SubscriptionsSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/DetailsSectionStack;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/SubscriptionsSection;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 29
    return-void
.end method


# virtual methods
.method public addSubscription(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;ILcom/google/android/finsky/layout/SubscriptionView$CancelListener;Landroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 6
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "subscriptionDetails"    # Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
    .param p3, "subscriptionItemLayoutId"    # I
    .param p4, "cancelListener"    # Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;
    .param p5, "savedState"    # Landroid/os/Bundle;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 35
    iget-object v1, p0, Lcom/google/android/finsky/layout/SubscriptionsSection;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p3, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/SubscriptionView;

    .local v0, "subscriptionView":Lcom/google/android/finsky/layout/SubscriptionView;
    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    .line 37
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/SubscriptionView;->configure(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/LibrarySubscriptionEntry;Lcom/google/android/finsky/layout/SubscriptionView$CancelListener;Landroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 39
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SubscriptionsSection;->addView(Landroid/view/View;)V

    .line 40
    return-void
.end method

.method public clearSubscriptions()V
    .locals 0

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SubscriptionsSection;->removeAllViews()V

    .line 44
    return-void
.end method

.method public saveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 47
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SubscriptionsSection;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 48
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/SubscriptionsSection;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 49
    .local v0, "child":Landroid/view/View;
    instance-of v2, v0, Lcom/google/android/finsky/layout/SubscriptionView;

    if-eqz v2, :cond_0

    .line 50
    check-cast v0, Lcom/google/android/finsky/layout/SubscriptionView;

    .end local v0    # "child":Landroid/view/View;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/SubscriptionView;->saveInstanceState(Landroid/os/Bundle;)V

    .line 47
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 53
    :cond_1
    return-void
.end method
