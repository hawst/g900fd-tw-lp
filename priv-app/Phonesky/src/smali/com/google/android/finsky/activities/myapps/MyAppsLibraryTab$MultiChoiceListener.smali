.class Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;
.super Ljava/lang/Object;
.source "MyAppsLibraryTab.java"

# interfaces
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MultiChoiceListener"
.end annotation


# instance fields
.field private final mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;)V
    .locals 0
    .param p1, "tab"    # Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    .prologue
    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    .line 284
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;
    .param p2, "x1"    # Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$1;

    .prologue
    .line 278
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;)Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    return-object v0
.end method

.method private getCheckedDocuments()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation

    .prologue
    .line 358
    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    invoke-virtual {v4}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    .line 359
    .local v0, "checkedItems":Landroid/util/SparseBooleanArray;
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 360
    .local v1, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 361
    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    .line 362
    .local v3, "position":I
    invoke-virtual {v0, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 363
    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    invoke-virtual {v4}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->getAdapter()Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;

    move-result-object v4

    invoke-interface {v4, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;->getDocument(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 366
    .end local v3    # "position":I
    :cond_1
    return-object v1
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "actionMode"    # Landroid/view/ActionMode;
    .param p2, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    # getter for: Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mFragment:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
    invoke-static {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->access$000(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;)Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->getCheckedDocuments()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->handleMenuItem(Ljava/util/List;Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    const/4 v0, 0x1

    .line 327
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 8
    .param p1, "actionMode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x1

    .line 297
    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 298
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v4, 0x7f110002

    invoke-virtual {v2, v4, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 299
    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    # setter for: Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mCurrentActionMode:Landroid/view/ActionMode;
    invoke-static {v4, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->access$202(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 301
    const v4, 0x7f0a03cf

    invoke-interface {p2, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 302
    .local v3, "item":Landroid/view/MenuItem;
    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    iget-object v4, v4, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAuthenticatedActivity:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    invoke-virtual {v4}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0400da

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 304
    .local v0, "actionView":Landroid/view/View;
    const v4, 0x7f0a026d

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/PlayActionButton;

    .line 306
    .local v1, "button":Lcom/google/android/play/layout/PlayActionButton;
    invoke-virtual {v1, v7}, Lcom/google/android/play/layout/PlayActionButton;->setActionStyle(I)V

    .line 307
    const/4 v4, 0x3

    const v5, 0x7f0c01e6

    new-instance v6, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener$1;

    invoke-direct {v6, p0, p1, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener$1;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;Landroid/view/ActionMode;Landroid/view/MenuItem;)V

    invoke-virtual {v1, v4, v5, v6}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 313
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    .line 314
    return v7
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2
    .param p1, "actionMode"    # Landroid/view/ActionMode;

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mCurrentActionMode:Landroid/view/ActionMode;
    invoke-static {v0, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->access$202(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;Landroid/view/ActionMode;)Landroid/view/ActionMode;

    .line 335
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->getListView()Landroid/widget/ListView;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener$2;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 355
    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 8
    .param p1, "actionMode"    # Landroid/view/ActionMode;
    .param p2, "position"    # I
    .param p3, "id"    # J
    .param p5, "checked"    # Z

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->getCheckedDocuments()Ljava/util/List;

    move-result-object v0

    .line 290
    .local v0, "checkedDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 291
    .local v1, "selectedCount":I
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    # getter for: Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mCurrentActionMode:Landroid/view/ActionMode;
    invoke-static {v2}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->access$200(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;)Landroid/view/ActionMode;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;->mTab:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    iget-object v3, v3, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAuthenticatedActivity:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    invoke-virtual {v3}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100006

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v1, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    .line 293
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "actionMode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 319
    const/4 v0, 0x0

    return v0
.end method
