.class public Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;
.super Lcom/google/android/finsky/activities/AuthenticatedActivity;
.source "LightPurchaseFlowActivity.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/billing/DownloadSizeDialogFragment$Listener;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mAppTitle:Ljava/lang/String;

.field private mAppVersionCode:I

.field private mAppsContinueUrl:Ljava/lang/String;

.field private mCalledByFirstPartyPackage:Z

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mDocid:Lcom/google/android/finsky/protos/Common$Docid;

.field private mDocidStr:Ljava/lang/String;

.field private mFailed:Z

.field private final mHandler:Landroid/os/Handler;

.field private mHeavyDialogShown:Z

.field private mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

.field private mOfferRequiresCheckout:Z

.field private mOfferType:I

.field private mReferralUrl:Ljava/lang/String;

.field private mSavedInstanceState:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/finsky/activities/AuthenticatedActivity;-><init>()V

    .line 319
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;)Lcom/google/android/finsky/protos/Common$Docid;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->logConfirmFreeDownload()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->success()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;)Landroid/accounts/Account;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;
    .param p1, "x1"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->handleAgeVerificationResult(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;
    .param p1, "x1"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->handlePromptForFopResult(I)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;ILandroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/content/Intent;

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->handleAppPermissionResult(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;ILandroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/content/Intent;

    .prologue
    .line 90
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->handleOfferResolutionResult(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;
    .param p1, "x1"    # I

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->handlePaymentResult(I)V

    return-void
.end method

.method private acquire(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "appDownloadSizeWarningArgs"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x1

    .line 488
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v8

    .line 489
    .local v8, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-static {v0, v8, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 490
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v0, v0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    if-ne v0, v5, :cond_1

    .line 491
    if-eqz p1, :cond_0

    .line 494
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->showDownloadSizeWarning(Landroid/os/Bundle;)V

    .line 587
    :goto_0
    return-void

    .line 498
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->logConfirmFreeDownload()V

    .line 501
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppsContinueUrl:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setContinueUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppVersionCode:I

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppTitle:Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "single_install"

    const/4 v7, 0x2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 508
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->success()V

    goto :goto_0

    .line 510
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    if-eqz v0, :cond_3

    invoke-direct {p0, v8}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->isCanceledMusicSubscription(Lcom/google/android/finsky/library/AccountLibrary;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/finsky/config/G;->musicAppSubscriptionResignupEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 524
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    if-eqz v0, :cond_5

    .line 525
    invoke-static {}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;->builder()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocid(Lcom/google/android/finsky/protos/Common$Docid;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocidStr(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setOfferType(I)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppVersionCode:I

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppTitle:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppsContinueUrl:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setAppData(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->build()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    move-result-object v11

    .line 531
    .local v11, "params":Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "LightPurchaseFlowActivity.serverLogsCookie"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v12

    .line 532
    .local v12, "serverLogsCookie":[B
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    invoke-static {v0, v11, v12, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseActivity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;[BLandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v10

    .line 534
    .local v10, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v10, v5}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 517
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v11    # "params":Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    .end local v12    # "serverLogsCookie":[B
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v0, v0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_4

    const v9, 0x7f0c0225

    .line 519
    .local v9, "errorMessageId":I
    :goto_1
    invoke-virtual {p0, v9}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->showError(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 517
    .end local v9    # "errorMessageId":I
    :cond_4
    const v9, 0x7f0c022c

    goto :goto_1

    .line 536
    :cond_5
    if-eqz p1, :cond_6

    .line 539
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->showDownloadSizeWarning(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 542
    :cond_6
    new-instance v4, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$1;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;)V

    .line 584
    .local v4, "successListener":Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppsContinueUrl:Ljava/lang/String;

    move v6, v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PurchaseInitiator;->makeFreePurchase(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILjava/lang/String;Lcom/google/android/finsky/utils/PurchaseInitiator$SuccessListener;ZZ)V

    goto/16 :goto_0
.end method

.method public static createExternalPurchaseIntent(Lcom/google/android/finsky/api/model/Document;I)Landroid/content/Intent;
    .locals 3
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "offerType"    # I

    .prologue
    .line 345
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 346
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.android.vending.billing.PURCHASE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    const-string v1, "backend"

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 349
    const-string v1, "document_type"

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 350
    const-string v1, "backend_docid"

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    const-string v1, "full_docid"

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    if-eqz p1, :cond_0

    .line 353
    const-string v1, "offer_type"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 355
    :cond_0
    return-object v0
.end method

.method public static createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;[BLjava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "offerType"    # I
    .param p3, "offerFilter"    # Lcom/google/android/finsky/utils/DocUtils$OfferFilter;
    .param p4, "serverLogsCookie"    # [B
    .param p5, "appsContinueUrl"    # Ljava/lang/String;

    .prologue
    .line 331
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 332
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "LightPurchaseFlowActivity.account"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 333
    const-string v1, "LightPurchaseFlowActivity.doc"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 334
    const-string v1, "LightPurchaseFlowActivity.offerType"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 335
    if-eqz p3, :cond_0

    .line 336
    const-string v1, "LightPurchaseFlowActivity.offerFilter"

    invoke-virtual {p3}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 338
    :cond_0
    const-string v1, "LightPurchaseFlowActivity.appsContinueUrl"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 339
    const-string v1, "LightPurchaseFlowActivity.serverLogsCookie"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 340
    return-object v0
.end method

.method private fail()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 611
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mFailed:Z

    .line 612
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->setResult(I)V

    .line 613
    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->logFinish(Z)V

    .line 614
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->finish()V

    .line 615
    return-void
.end method

.method private getBackgroundEvent(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 887
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setDocument(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setCallingPackage(Ljava/lang/String;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 890
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    if-eqz v1, :cond_0

    .line 891
    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setOfferType(I)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    .line 892
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setOfferCheckoutFlowRequired(Z)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    .line 896
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v1

    return-object v1
.end method

.method private handleAgeVerificationResult(I)V
    .locals 4
    .param p1, "resultCode"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 693
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 694
    const-string v0, "Age verification activity success: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 695
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startFopRequiredOrAcquisitionFlow()V

    .line 700
    :goto_0
    return-void

    .line 697
    :cond_0
    const-string v0, "Age verification activity canceled: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 698
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    goto :goto_0
.end method

.method private handleAppPermissionResult(ILandroid/content/Intent;)V
    .locals 9
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 713
    const/4 v4, -0x1

    if-ne p1, v4, :cond_3

    .line 714
    const-string v4, "Permissions accepted: %s"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 715
    invoke-static {p2}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->extractDoc(Landroid/content/Intent;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 716
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget v5, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v3

    .line 717
    .local v3, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    if-nez v3, :cond_0

    .line 720
    const-string v4, "Offer not found: type=%d"

    new-array v5, v8, [Ljava/lang/Object;

    iget v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 721
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    .line 746
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :goto_0
    return-void

    .line 724
    .restart local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_0
    iget-boolean v4, v3, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    iput-boolean v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    .line 725
    invoke-static {p2}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->extractVersionCode(Landroid/content/Intent;)I

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppVersionCode:I

    .line 726
    invoke-static {p2}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->extractTitle(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppTitle:Ljava/lang/String;

    .line 727
    invoke-static {p2}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->getDownloadSizeWarningArguments(Landroid/content/Intent;)Landroid/os/Bundle;

    move-result-object v2

    .line 729
    .local v2, "downloadSizeWarningArguments":Landroid/os/Bundle;
    invoke-static {p2}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->extractAcceptedNewBuckets(Landroid/content/Intent;)Z

    move-result v0

    .line 730
    .local v0, "acceptedNewBuckets":Z
    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v1, v4, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 733
    .local v1, "docId":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 734
    invoke-static {v1}, Lcom/google/android/finsky/utils/PermissionsBucketer;->setAcceptedNewBuckets(Ljava/lang/String;)V

    .line 736
    :cond_1
    if-nez v2, :cond_2

    .line 739
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/google/android/finsky/receivers/Installer;->setMobileDataAllowed(Ljava/lang/String;)V

    .line 741
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->acquire(Landroid/os/Bundle;)V

    goto :goto_0

    .line 743
    .end local v0    # "acceptedNewBuckets":Z
    .end local v1    # "docId":Ljava/lang/String;
    .end local v2    # "downloadSizeWarningArguments":Landroid/os/Bundle;
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_3
    const-string v4, "Permissions declined: %s"

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 744
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    goto :goto_0
.end method

.method private handleOfferResolutionResult(ILandroid/content/Intent;)V
    .locals 7
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 754
    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    .line 755
    invoke-static {p2}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->extractAvailableOffer(Landroid/content/Intent;)Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    move-result-object v1

    .line 757
    .local v1, "selectedOffer":Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
    iget-object v2, v1, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->doc:Lcom/google/android/finsky/api/model/Document;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 758
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getFullDocid()Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 759
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    .line 760
    iget-object v0, v1, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    .line 761
    .local v0, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget v2, v0, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    iput v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    .line 762
    iget-boolean v2, v0, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    iput-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    .line 763
    const-string v2, "Offer resolution: %s, offerType=%d, checkoutFlowRequired=%b"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    aput-object v4, v3, v5

    iget v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x2

    iget-boolean v5, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 765
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->acquire(Landroid/os/Bundle;)V

    .line 770
    .end local v0    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "selectedOffer":Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
    :goto_0
    return-void

    .line 767
    :cond_0
    const-string v2, "Offer resolution canceled: %s"

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 768
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    goto :goto_0
.end method

.method private handlePaymentResult(I)V
    .locals 5
    .param p1, "resultCode"    # I

    .prologue
    const/4 v4, 0x1

    .line 773
    const-string v0, "Payment screen finished with result: %d"

    new-array v1, v4, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 774
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 775
    iput-boolean v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHeavyDialogShown:Z

    .line 776
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->success()V

    .line 780
    :goto_0
    return-void

    .line 778
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    goto :goto_0
.end method

.method private handlePromptForFopResult(I)V
    .locals 4
    .param p1, "resultCode"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 703
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 704
    const-string v0, "Prompt for FOP activity success: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 705
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startAcquisitionFlow()V

    .line 710
    :goto_0
    return-void

    .line 707
    :cond_0
    const-string v0, "Prompt for FOP activity canceled: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 708
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    goto :goto_0
.end method

.method private isCanceledMusicSubscription(Lcom/google/android/finsky/library/AccountLibrary;)Z
    .locals 9
    .param p1, "accountLibrary"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    const/16 v5, 0xf

    const/4 v6, 0x1

    const/4 v3, 0x2

    .line 590
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v0, v0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v0, v0, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    if-ne v0, v5, :cond_0

    .line 591
    invoke-static {v3}, Lcom/google/android/finsky/library/AccountLibrary;->getLibraryIdFromBackend(I)Ljava/lang/String;

    move-result-object v2

    .line 592
    .local v2, "musicLibraryId":Ljava/lang/String;
    invoke-virtual {p1, v2}, Lcom/google/android/finsky/library/AccountLibrary;->getLibrary(Ljava/lang/String;)Lcom/google/android/finsky/library/HashingLibrary;

    move-result-object v8

    .line 593
    .local v8, "musicLibrary":Lcom/google/android/finsky/library/Library;
    new-instance v0, Lcom/google/android/finsky/library/LibraryEntry;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v4, v4, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/library/LibraryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;II)V

    invoke-interface {v8, v0}, Lcom/google/android/finsky/library/Library;->get(Lcom/google/android/finsky/library/LibraryEntry;)Lcom/google/android/finsky/library/LibraryEntry;

    move-result-object v7

    .line 597
    .local v7, "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    if-eqz v7, :cond_0

    check-cast v7, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;

    .end local v7    # "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    iget-boolean v0, v7, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;->isAutoRenewing:Z

    if-nez v0, :cond_0

    .line 601
    .end local v2    # "musicLibraryId":Ljava/lang/String;
    .end local v8    # "musicLibrary":Lcom/google/android/finsky/library/Library;
    :goto_0
    return v6

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private isExternalPurchaseIntent()Z
    .locals 2

    .prologue
    .line 476
    const-string v0, "com.android.vending.billing.PURCHASE"

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private launchAppsPermissions()V
    .locals 5

    .prologue
    .line 605
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->createIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Z)Landroid/content/Intent;

    move-result-object v0

    .line 607
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 608
    return-void
.end method

.method private logConfirmFreeDownload()V
    .locals 3

    .prologue
    .line 638
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getAnalytics(Ljava/lang/String;)Lcom/google/android/finsky/analytics/Analytics;

    move-result-object v0

    .line 639
    .local v0, "analytics":Lcom/google/android/finsky/analytics/Analytics;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "confirmFreeDownload?doc="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/analytics/Analytics;->logAdMobPageView(Ljava/lang/String;)V

    .line 640
    return-void
.end method

.method private logFinish(Z)V
    .locals 4
    .param p1, "success"    # Z

    .prologue
    .line 878
    const/16 v2, 0x259

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getBackgroundEvent(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    .line 880
    .local v0, "event":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    iput-boolean p1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->operationSuccess:Z

    .line 881
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasOperationSuccess:Z

    .line 882
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    .line 883
    .local v1, "eventLog":Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 884
    return-void
.end method

.method private logStart()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 865
    const/16 v2, 0x258

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getBackgroundEvent(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    .line 867
    .local v0, "event":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    .line 868
    .local v1, "eventLog":Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 871
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mReferralUrl:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 872
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mReferralUrl:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 875
    :cond_0
    return-void
.end method

.method private setupFromExternalPurchaseIntent(Landroid/content/Intent;)Z
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 430
    invoke-static {p0}, Lcom/google/android/finsky/utils/SignatureUtils;->isCalledByFirstPartyPackage(Landroid/app/Activity;)Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mCalledByFirstPartyPackage:Z

    .line 431
    sget-object v6, Lcom/google/android/finsky/config/G;->enableThirdPartyDirectPurchases:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mCalledByFirstPartyPackage:Z

    if-nez v6, :cond_0

    .line 432
    const-string v6, "Called from untrusted package."

    new-array v8, v7, [Ljava/lang/Object;

    invoke-static {v6, v8}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move v6, v7

    .line 472
    :goto_0
    return v6

    .line 436
    :cond_0
    const-string v6, "backend"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "document_type"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "backend_docid"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string v6, "full_docid"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 440
    :cond_1
    const-string v6, "Missing argument."

    new-array v8, v7, [Ljava/lang/Object;

    invoke-static {v6, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    move v6, v7

    .line 441
    goto :goto_0

    .line 443
    :cond_2
    const-string v6, "authAccount"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 444
    const-string v6, "authAccount"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 445
    .local v0, "accountName":Ljava/lang/String;
    invoke-static {v0, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    .line 446
    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    if-nez v6, :cond_4

    .line 447
    const-string v6, "Invalid account passed: %s"

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v7

    invoke-static {v6, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v6, v7

    .line 448
    goto :goto_0

    .line 451
    .end local v0    # "accountName":Ljava/lang/String;
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    .line 453
    :cond_4
    const-string v6, "backend"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 454
    .local v1, "backend":I
    const-string v6, "document_type"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 455
    .local v3, "docType":I
    const-string v6, "backend_docid"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 456
    .local v2, "backendDocid":Ljava/lang/String;
    invoke-static {v1, v3, v2}, Lcom/google/android/finsky/utils/DocUtils;->createDocid(IILjava/lang/String;)Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 457
    const-string v6, "full_docid"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    .line 458
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 459
    const-string v6, "offer_type"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    .line 461
    iput-boolean v8, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    .line 462
    const-string v6, "offer_filter"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 463
    .local v5, "offerFilter":Ljava/lang/String;
    if-eqz v5, :cond_5

    .line 465
    :try_start_0
    invoke-static {v5}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    :cond_5
    const-string v6, "referral_url"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mReferralUrl:Ljava/lang/String;

    move v6, v8

    .line 472
    goto/16 :goto_0

    .line 466
    :catch_0
    move-exception v4

    .line 467
    .local v4, "e":Ljava/lang/IllegalArgumentException;
    const-string v6, "Invalid offer types passed: %s"

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v5, v8, v7

    invoke-static {v6, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    move v6, v7

    .line 468
    goto/16 :goto_0
.end method

.method private setupFromInternalIntent(Landroid/content/Intent;)Z
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 401
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    .line 402
    .local v0, "caller":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 403
    :cond_0
    const-string v2, "Blocked request from external package: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v3

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v3

    .line 426
    :goto_0
    return v2

    .line 406
    :cond_1
    iput-boolean v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mCalledByFirstPartyPackage:Z

    .line 408
    const-string v2, "LightPurchaseFlowActivity.account"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    .line 409
    const-string v2, "LightPurchaseFlowActivity.docid"

    invoke-static {p1, v2}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/Common$Docid;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 410
    const-string v2, "LightPurchaseFlowActivity.doc"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 411
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getFullDocid()Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 412
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    .line 413
    const-string v2, "LightPurchaseFlowActivity.offerType"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    .line 414
    const-string v2, "LightPurchaseFlowActivity.offerFilter"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 415
    const-string v2, "LightPurchaseFlowActivity.offerFilter"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    .line 417
    :cond_2
    iget v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    if-eqz v2, :cond_4

    .line 418
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget v5, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v1

    .line 419
    .local v1, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    if-nez v1, :cond_3

    .line 420
    const-string v2, "Offer type not available: %d"

    new-array v4, v4, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v3

    invoke-static {v2, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v3

    .line 421
    goto :goto_0

    .line 423
    :cond_3
    iget-boolean v2, v1, Lcom/google/android/finsky/protos/Common$Offer;->checkoutFlowRequired:Z

    iput-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    .line 425
    .end local v1    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_4
    const-string v2, "LightPurchaseFlowActivity.appsContinueUrl"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppsContinueUrl:Ljava/lang/String;

    move v2, v4

    .line 426
    goto :goto_0
.end method

.method private shouldStartAgeVerificationFlow()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 816
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/utils/ClientMutationCache;->isAgeVerificationRequired()Z

    move-result v3

    if-nez v3, :cond_1

    .line 836
    :cond_0
    :goto_0
    return v1

    .line 819
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    .line 820
    .local v0, "libraries":Lcom/google/android/finsky/library/Libraries;
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v3, v3, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    if-ne v3, v2, :cond_3

    .line 822
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 831
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v1, :cond_4

    move v1, v2

    .line 834
    goto :goto_0

    .line 827
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;)Z

    move-result v3

    if-eqz v3, :cond_2

    goto :goto_0

    .line 836
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isMature()Z

    move-result v1

    goto :goto_0
.end method

.method private shouldStartFopRequiredFlow()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 844
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->isExternalPurchaseIntent()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 861
    :cond_0
    :goto_0
    return v0

    .line 848
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 851
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    if-nez v1, :cond_0

    .line 855
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 861
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/PromptForFopHelper;->shouldPromptForFop(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private showDownloadSizeWarning(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 749
    const/4 v1, 0x0

    invoke-static {v1, p1}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->newInstance(Landroid/support/v4/app/Fragment;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;

    move-result-object v0

    .line 750
    .local v0, "dialog":Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "LightPurchaseFlowActivity.appDownloadSizeWarningDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 751
    return-void
.end method

.method private startAcquisitionFlow()V
    .locals 6

    .prologue
    .line 923
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 924
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->launchAppsPermissions()V

    .line 932
    :goto_0
    return-void

    .line 925
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    if-nez v1, :cond_1

    .line 926
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->createIntent(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;)Landroid/content/Intent;

    move-result-object v0

    .line 928
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 930
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->acquire(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private startAgeVerificationFlow()V
    .locals 4

    .prologue
    .line 902
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v2, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v3, v1, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    :goto_0
    invoke-static {v2, v3, v1}, Lcom/google/android/finsky/billing/lightpurchase/AgeVerificationActivity;->createIntent(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 904
    .local v0, "intent":Landroid/content/Intent;
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 905
    return-void

    .line 902
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startFopRequiredFlow()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 908
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHeavyDialogShown:Z

    .line 909
    new-instance v6, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 910
    .local v6, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "LightPurchaseFlowActivity.serverLogsCookie"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    .line 911
    .local v2, "serverLogsCookie":[B
    const v0, 0x7f0c011b

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setTitleId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0119

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c020d

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v4, v1, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x3e8

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 917
    invoke-virtual {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v7

    .line 918
    .local v7, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/finsky/billing/PromptForFopHelper;->recordDialogImpression(Ljava/lang/String;)V

    .line 919
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "LightPurchaseFlowActivity.fopRequiredDialog"

    invoke-virtual {v7, v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 920
    return-void
.end method

.method private startFopRequiredOrAcquisitionFlow()V
    .locals 1

    .prologue
    .line 800
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->shouldStartFopRequiredFlow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 801
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startFopRequiredFlow()V

    .line 805
    :goto_0
    return-void

    .line 803
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startAcquisitionFlow()V

    goto :goto_0
.end method

.method private success()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 618
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mCalledByFirstPartyPackage:Z

    if-eqz v1, :cond_0

    .line 619
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 621
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "authAccount"

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAccount:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 622
    const-string v1, "backend"

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v2, v2, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 623
    const-string v1, "document_type"

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v2, v2, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 624
    const-string v1, "backend_docid"

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v2, v2, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 625
    const-string v1, "offer_type"

    iget v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 626
    const-string v1, "involved_heavy_dialogs"

    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHeavyDialogShown:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 627
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v1, v0}, Lcom/google/android/finsky/activities/PlayGamesInstallHelper;->addGameIntentExtras(Lcom/google/android/finsky/api/model/Document;Landroid/content/Intent;)V

    .line 628
    invoke-virtual {p0, v3, v0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->setResult(ILandroid/content/Intent;)V

    .line 632
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->logFinish(Z)V

    .line 633
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->finish()V

    .line 634
    return-void

    .line 630
    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->setResult(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 646
    packed-switch p1, :pswitch_data_0

    .line 688
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 690
    :goto_0
    return-void

    .line 648
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$2;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$2;-><init>(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 656
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$3;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$3;-><init>(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 664
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$4;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$4;-><init>(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;ILandroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 672
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$5;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$5;-><init>(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;ILandroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 680
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$6;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity$6;-><init>(Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 646
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 360
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 361
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->isExternalPurchaseIntent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 362
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->setupFromExternalPurchaseIntent(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 363
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    .line 371
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 372
    const-string v1, "LightPurchaseFlowActivity.docid"

    invoke-static {p1, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Common$Docid;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 373
    const-string v1, "LightPurchaseFlowActivity.docidStr"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    .line 374
    const-string v1, "LightPurchaseFlowActivity.doc"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 375
    const-string v1, "LightPurchaseFlowActivity.offerType"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    .line 376
    const-string v1, "LightPurchaseFlowActivity.offerRequiresCheckout"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    .line 377
    const-string v1, "LightPurchaseFlowActivity.appTitle"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppTitle:Ljava/lang/String;

    .line 378
    const-string v1, "LightPurchaseFlowActivity.appVersionCode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppVersionCode:I

    .line 379
    const-string v1, "LightPurchaseFlowActivity.failed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mFailed:Z

    .line 380
    const-string v1, "LightPurchaseFlowActivity.purchasePerformed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHeavyDialogShown:Z

    .line 382
    :cond_1
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mSavedInstanceState:Landroid/os/Bundle;

    .line 383
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onCreate(Landroid/os/Bundle;)V

    .line 384
    return-void

    .line 366
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->setupFromInternalIntent(Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 367
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    goto :goto_0
.end method

.method public onDownloadCancel()V
    .locals 4

    .prologue
    .line 993
    const-string v0, "Download size warning dismissed for app = %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 994
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    .line 995
    return-void
.end method

.method public onDownloadOk(Z)V
    .locals 5
    .param p1, "wifiOnly"    # Z

    .prologue
    .line 977
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v0, v1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 978
    .local v0, "packageName":Ljava/lang/String;
    const-string v1, "Will download %s using wifi only = %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 979
    if-nez p1, :cond_0

    .line 980
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/finsky/receivers/Installer;->setMobileDataAllowed(Ljava/lang/String;)V

    .line 982
    :cond_0
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->acquire(Landroid/os/Bundle;)V

    .line 983
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 971
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    .line 972
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 946
    packed-switch p1, :pswitch_data_0

    .line 965
    const-string v5, "Unknown dialog callback: %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 967
    :goto_0
    return-void

    .line 948
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    goto :goto_0

    .line 951
    :pswitch_1
    const-string v5, "dialog_details_url"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 952
    .local v1, "detailsUrl":Ljava/lang/String;
    invoke-static {p0, v1}, Lcom/google/android/finsky/utils/IntentUtils;->createViewDocumentUrlIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 953
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startActivity(Landroid/content/Intent;)V

    .line 954
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->fail()V

    goto :goto_0

    .line 958
    .end local v1    # "detailsUrl":Ljava/lang/String;
    .end local v2    # "intent":Landroid/content/Intent;
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "LightPurchaseFlowActivity.serverLogsCookie"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    .line 959
    .local v4, "serverLogsCookie":[B
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 960
    .local v0, "currentAccount":Landroid/accounts/Account;
    invoke-static {v0, v4}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopActivity;->createIntent(Landroid/accounts/Account;[B)Landroid/content/Intent;

    move-result-object v3

    .line 962
    .local v3, "promptForFopIntent":Landroid/content/Intent;
    const/4 v5, 0x7

    invoke-virtual {p0, v3, v5}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 946
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onReady(Z)V
    .locals 1
    .param p1, "shouldHandleIntent"    # Z

    .prologue
    .line 784
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mFailed:Z

    if-eqz v0, :cond_1

    .line 797
    :cond_0
    :goto_0
    return-void

    .line 788
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mSavedInstanceState:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 789
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->logStart()V

    .line 791
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->shouldStartAgeVerificationFlow()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 792
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startAgeVerificationFlow()V

    goto :goto_0

    .line 794
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startFopRequiredOrAcquisitionFlow()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 388
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/AuthenticatedActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 389
    const-string v0, "LightPurchaseFlowActivity.docid"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 390
    const-string v0, "LightPurchaseFlowActivity.docidStr"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDocidStr:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const-string v0, "LightPurchaseFlowActivity.doc"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 392
    const-string v0, "LightPurchaseFlowActivity.offerType"

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 393
    const-string v0, "LightPurchaseFlowActivity.offerRequiresCheckout"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mOfferRequiresCheckout:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 394
    const-string v0, "LightPurchaseFlowActivity.purchasePerformed"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mHeavyDialogShown:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 395
    const-string v0, "LightPurchaseFlowActivity.appTitle"

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const-string v0, "LightPurchaseFlowActivity.appVersionCode"

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mAppVersionCode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 397
    const-string v0, "LightPurchaseFlowActivity.failed"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->mFailed:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 398
    return-void
.end method

.method public onSetupWifi()V
    .locals 2

    .prologue
    .line 987
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.WIFI_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 988
    .local v0, "settingsIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->startActivity(Landroid/content/Intent;)V

    .line 989
    return-void
.end method

.method public showError(Ljava/lang/String;)V
    .locals 5
    .param p1, "errorMessage"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 935
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 936
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c02a0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v4, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 939
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 940
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "LightPurchaseFlowActivity.errorDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 941
    return-void
.end method
