.class public final Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreLogEvent"
.end annotation


# instance fields
.field public backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

.field public click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

.field public deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

.field public impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

.field public search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3546
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3547
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;

    .line 3548
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3551
    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    .line 3552
    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    .line 3553
    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    .line 3554
    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    .line 3555
    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    .line 3556
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->cachedSize:I

    .line 3557
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 3583
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 3584
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    if-eqz v1, :cond_0

    .line 3585
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3588
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    if-eqz v1, :cond_1

    .line 3589
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3592
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    if-eqz v1, :cond_2

    .line 3593
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3596
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    if-eqz v1, :cond_3

    .line 3597
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3600
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    if-eqz v1, :cond_4

    .line 3601
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3604
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3612
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 3613
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 3617
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3618
    :sswitch_0
    return-object p0

    .line 3623
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    if-nez v1, :cond_1

    .line 3624
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    .line 3626
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3630
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    if-nez v1, :cond_2

    .line 3631
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    .line 3633
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3637
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    if-nez v1, :cond_3

    .line 3638
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    .line 3640
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3644
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    if-nez v1, :cond_4

    .line 3645
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    .line 3647
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3651
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    if-nez v1, :cond_5

    .line 3652
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    .line 3654
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 3613
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3514
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3563
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    if-eqz v0, :cond_0

    .line 3564
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->impression:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreImpressionEvent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3566
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    if-eqz v0, :cond_1

    .line 3567
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->click:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreClickEvent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3569
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    if-eqz v0, :cond_2

    .line 3570
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->backgroundAction:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3572
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    if-eqz v0, :cond_3

    .line 3573
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->search:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3575
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    if-eqz v0, :cond_4

    .line 3576
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreLogEvent;->deepLink:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 3578
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3579
    return-void
.end method
