.class Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;
.super Landroid/text/style/ForegroundColorSpan;
.source "ActionBarHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlphaSpan"
.end annotation


# instance fields
.field private mAlpha:F


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 107
    invoke-direct {p0, p1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 104
    const/high16 v0, 0x437f0000    # 255.0f

    iput v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;->mAlpha:F

    .line 108
    return-void
.end method


# virtual methods
.method public setAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 111
    iput p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;->mAlpha:F

    .line 112
    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 6
    .param p1, "textPaint"    # Landroid/text/TextPaint;

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;->getForegroundColor()I

    move-result v1

    .line 118
    .local v1, "fgColor":I
    iget v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;->mAlpha:F

    float-to-int v2, v2

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {v2, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 120
    .local v0, "blendedColor":I
    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 121
    return-void
.end method
