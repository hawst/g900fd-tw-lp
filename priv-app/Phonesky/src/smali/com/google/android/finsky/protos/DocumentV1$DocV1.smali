.class public final Lcom/google/android/finsky/protos/DocumentV1$DocV1;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocumentV1.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocumentV1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DocV1"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;


# instance fields
.field public creator:Ljava/lang/String;

.field public descriptionHtml:Ljava/lang/String;

.field public details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

.field public detailsUrl:Ljava/lang/String;

.field public docid:Ljava/lang/String;

.field public finskyDoc:Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

.field public hasCreator:Z

.field public hasDescriptionHtml:Z

.field public hasDetailsUrl:Z

.field public hasDocid:Z

.field public hasMoreByBrowseUrl:Z

.field public hasMoreByHeader:Z

.field public hasMoreByListUrl:Z

.field public hasRelatedBrowseUrl:Z

.field public hasRelatedHeader:Z

.field public hasRelatedListUrl:Z

.field public hasReviewsUrl:Z

.field public hasShareUrl:Z

.field public hasTitle:Z

.field public hasWarningMessage:Z

.field public moreByBrowseUrl:Ljava/lang/String;

.field public moreByHeader:Ljava/lang/String;

.field public moreByListUrl:Ljava/lang/String;

.field public plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

.field public relatedBrowseUrl:Ljava/lang/String;

.field public relatedHeader:Ljava/lang/String;

.field public relatedListUrl:Ljava/lang/String;

.field public reviewsUrl:Ljava/lang/String;

.field public shareUrl:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public warningMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->clear()Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    .line 92
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    sput-object v0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->_emptyArray:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 95
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->finskyDoc:Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->docid:Ljava/lang/String;

    .line 97
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDocid:Z

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->detailsUrl:Ljava/lang/String;

    .line 99
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDetailsUrl:Z

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->reviewsUrl:Ljava/lang/String;

    .line 101
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasReviewsUrl:Z

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedListUrl:Ljava/lang/String;

    .line 103
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedListUrl:Z

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedBrowseUrl:Ljava/lang/String;

    .line 105
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedBrowseUrl:Z

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedHeader:Ljava/lang/String;

    .line 107
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedHeader:Z

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByListUrl:Ljava/lang/String;

    .line 109
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByListUrl:Z

    .line 110
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByBrowseUrl:Ljava/lang/String;

    .line 111
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByBrowseUrl:Z

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByHeader:Ljava/lang/String;

    .line 113
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByHeader:Z

    .line 114
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->shareUrl:Ljava/lang/String;

    .line 115
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasShareUrl:Z

    .line 116
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->title:Ljava/lang/String;

    .line 117
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasTitle:Z

    .line 118
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->creator:Ljava/lang/String;

    .line 119
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasCreator:Z

    .line 120
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    .line 121
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->descriptionHtml:Ljava/lang/String;

    .line 122
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDescriptionHtml:Z

    .line 123
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 124
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->warningMessage:Ljava/lang/String;

    .line 125
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasWarningMessage:Z

    .line 126
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->cachedSize:I

    .line 127
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 189
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 190
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->finskyDoc:Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    if-eqz v1, :cond_0

    .line 191
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->finskyDoc:Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDocid:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->docid:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 195
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->docid:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDetailsUrl:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->detailsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 199
    :cond_3
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->detailsUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 202
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasReviewsUrl:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->reviewsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 203
    :cond_5
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->reviewsUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 206
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedListUrl:Z

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedListUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 207
    :cond_7
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedListUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByListUrl:Z

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByListUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 211
    :cond_9
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByListUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 214
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasShareUrl:Z

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->shareUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 215
    :cond_b
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->shareUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    :cond_c
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasCreator:Z

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->creator:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 219
    :cond_d
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->creator:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    :cond_e
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    if-eqz v1, :cond_f

    .line 223
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 226
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDescriptionHtml:Z

    if-nez v1, :cond_10

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->descriptionHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 227
    :cond_10
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->descriptionHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedBrowseUrl:Z

    if-nez v1, :cond_12

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedBrowseUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 231
    :cond_12
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedBrowseUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByBrowseUrl:Z

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByBrowseUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 235
    :cond_14
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByBrowseUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    :cond_15
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedHeader:Z

    if-nez v1, :cond_16

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedHeader:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    .line 239
    :cond_16
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedHeader:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    :cond_17
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByHeader:Z

    if-nez v1, :cond_18

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByHeader:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 243
    :cond_18
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByHeader:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 246
    :cond_19
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasTitle:Z

    if-nez v1, :cond_1a

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 247
    :cond_1a
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    :cond_1b
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-eqz v1, :cond_1c

    .line 251
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    :cond_1c
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasWarningMessage:Z

    if-nez v1, :cond_1d

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->warningMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 255
    :cond_1d
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->warningMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_1e
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 266
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 267
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 271
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 272
    :sswitch_0
    return-object p0

    .line 277
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->finskyDoc:Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    if-nez v1, :cond_1

    .line 278
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->finskyDoc:Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    .line 280
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->finskyDoc:Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 284
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->docid:Ljava/lang/String;

    .line 285
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDocid:Z

    goto :goto_0

    .line 289
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->detailsUrl:Ljava/lang/String;

    .line 290
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDetailsUrl:Z

    goto :goto_0

    .line 294
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->reviewsUrl:Ljava/lang/String;

    .line 295
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasReviewsUrl:Z

    goto :goto_0

    .line 299
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedListUrl:Ljava/lang/String;

    .line 300
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedListUrl:Z

    goto :goto_0

    .line 304
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByListUrl:Ljava/lang/String;

    .line 305
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByListUrl:Z

    goto :goto_0

    .line 309
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->shareUrl:Ljava/lang/String;

    .line 310
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasShareUrl:Z

    goto :goto_0

    .line 314
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->creator:Ljava/lang/String;

    .line 315
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasCreator:Z

    goto :goto_0

    .line 319
    :sswitch_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    if-nez v1, :cond_2

    .line 320
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    .line 322
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 326
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->descriptionHtml:Ljava/lang/String;

    .line 327
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDescriptionHtml:Z

    goto :goto_0

    .line 331
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedBrowseUrl:Ljava/lang/String;

    .line 332
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedBrowseUrl:Z

    goto :goto_0

    .line 336
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByBrowseUrl:Ljava/lang/String;

    .line 337
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByBrowseUrl:Z

    goto/16 :goto_0

    .line 341
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedHeader:Ljava/lang/String;

    .line 342
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedHeader:Z

    goto/16 :goto_0

    .line 346
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByHeader:Ljava/lang/String;

    .line 347
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByHeader:Z

    goto/16 :goto_0

    .line 351
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->title:Ljava/lang/String;

    .line 352
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasTitle:Z

    goto/16 :goto_0

    .line 356
    :sswitch_10
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-nez v1, :cond_3

    .line 357
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    .line 359
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 363
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->warningMessage:Ljava/lang/String;

    .line 364
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasWarningMessage:Z

    goto/16 :goto_0

    .line 267
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
        0x8a -> :sswitch_11
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->finskyDoc:Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->finskyDoc:Lcom/google/android/finsky/protos/DocumentV1$OBSOLETE_FinskyDoc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 136
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDocid:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->docid:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->docid:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 139
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDetailsUrl:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->detailsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 140
    :cond_3
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->detailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 142
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasReviewsUrl:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->reviewsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 143
    :cond_5
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->reviewsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 145
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedListUrl:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedListUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 146
    :cond_7
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedListUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 148
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByListUrl:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByListUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 149
    :cond_9
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByListUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 151
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasShareUrl:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->shareUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 152
    :cond_b
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->shareUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 154
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasCreator:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->creator:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 155
    :cond_d
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->creator:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 157
    :cond_e
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    if-eqz v0, :cond_f

    .line 158
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->details:Lcom/google/android/finsky/protos/DocDetails$DocumentDetails;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 160
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasDescriptionHtml:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->descriptionHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 161
    :cond_10
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->descriptionHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 163
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedBrowseUrl:Z

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedBrowseUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 164
    :cond_12
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedBrowseUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 166
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByBrowseUrl:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByBrowseUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 167
    :cond_14
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByBrowseUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 169
    :cond_15
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasRelatedHeader:Z

    if-nez v0, :cond_16

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedHeader:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 170
    :cond_16
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->relatedHeader:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 172
    :cond_17
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasMoreByHeader:Z

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByHeader:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 173
    :cond_18
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->moreByHeader:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 175
    :cond_19
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasTitle:Z

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 176
    :cond_1a
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 178
    :cond_1b
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    if-eqz v0, :cond_1c

    .line 179
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->plusOneData:Lcom/google/android/finsky/protos/DocumentV2$PlusOneData;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 181
    :cond_1c
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->hasWarningMessage:Z

    if-nez v0, :cond_1d

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->warningMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 182
    :cond_1d
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->warningMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 184
    :cond_1e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 185
    return-void
.end method
