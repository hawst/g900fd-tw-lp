.class Lcom/google/android/finsky/activities/ReviewsFragment$1;
.super Ljava/lang/Object;
.source "ReviewsFragment.java"

# interfaces
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/ReviewsFragment;->onReviewFeedback(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/ReviewsFragment;

.field final synthetic val$newRating:Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/ReviewsFragment;Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;)V
    .locals 0

    .prologue
    .line 239
    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewsFragment$1;->this$0:Lcom/google/android/finsky/activities/ReviewsFragment;

    iput-object p2, p0, Lcom/google/android/finsky/activities/ReviewsFragment$1;->val$newRating:Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged()V
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment$1;->val$newRating:Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;

    sget-object v1, Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;->SPAM:Lcom/google/android/finsky/activities/ReviewFeedbackDialog$CommentRating;

    if-ne v0, v1, :cond_0

    .line 243
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewsFragment$1;->this$0:Lcom/google/android/finsky/activities/ReviewsFragment;

    # invokes: Lcom/google/android/finsky/activities/ReviewsFragment;->reloadReviews()V
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewsFragment;->access$000(Lcom/google/android/finsky/activities/ReviewsFragment;)V

    .line 245
    :cond_0
    return-void
.end method
