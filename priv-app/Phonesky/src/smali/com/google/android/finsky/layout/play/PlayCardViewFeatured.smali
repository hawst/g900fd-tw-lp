.class public Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayCardViewFeatured.java"


# instance fields
.field private mActionButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mBackgroundColor:I

.field private mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

.field private mHasBackgroundImage:Z

.field private mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

.field private mIconList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;",
            ">;"
        }
    .end annotation
.end field

.field private mLayoutType:I

.field private mMiniBottomRow:Landroid/view/View;

.field private mSmallTitleRow:Landroid/view/View;

.field private mStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mThumbnailAspectRatio:F

    .line 71
    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mHasBackgroundImage:Z

    .line 74
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundColor:I

    .line 76
    sget-object v1, Lcom/android/vending/R$styleable;->PlayCardViewFeatured:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 78
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mLayoutType:I

    .line 80
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 81
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;
    .param p1, "x1"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundColor:I

    return p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->setCardColor()V

    return-void
.end method

.method private fillIconList(Lcom/google/android/finsky/api/model/Document;)V
    .locals 23
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 252
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconList:Ljava/util/List;

    .line 254
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasBadgeContainer()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBadgeContainer()Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;

    move-result-object v14

    .line 256
    .local v14, "badgeContainer":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    iget-object v2, v14, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->badge:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    array-length v15, v2

    .line 257
    .local v15, "badgeCount":I
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_0
    move/from16 v0, v18

    if-ge v0, v15, :cond_0

    .line 258
    iget-object v2, v14, Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;->badge:[Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    aget-object v13, v2, v18

    .line 259
    .local v13, "badge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    const/4 v2, 0x6

    invoke-static {v13, v2}, Lcom/google/android/finsky/utils/BadgeUtils;->getImage(Lcom/google/android/finsky/protos/DocAnnotations$Badge;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v6

    .line 260
    .local v6, "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconList:Ljava/util/List;

    new-instance v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    iget-object v3, v13, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->title:Ljava/lang/String;

    iget-object v4, v13, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->description:Ljava/lang/String;

    iget-object v5, v13, Lcom/google/android/finsky/protos/DocAnnotations$Badge;->browseUrl:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Z)V

    invoke-interface {v8, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    .line 265
    .end local v6    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v13    # "badge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    .end local v14    # "badgeContainer":Lcom/google/android/finsky/protos/DocAnnotations$BadgeContainer;
    .end local v15    # "badgeCount":I
    .end local v18    # "i":I
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasProductDetails()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 266
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getProductDetails()Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    move-result-object v20

    .line 267
    .local v20, "productDetails":Lcom/google/android/finsky/protos/DocDetails$ProductDetails;
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    array-length v0, v2

    move/from16 v21, v0

    .line 268
    .local v21, "productDetailsSectionCount":I
    const/16 v18, 0x0

    .restart local v18    # "i":I
    :goto_1
    move/from16 v0, v18

    move/from16 v1, v21

    if-ge v0, v1, :cond_2

    .line 269
    move-object/from16 v0, v20

    iget-object v2, v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    aget-object v22, v2, v18

    .line 270
    .local v22, "section":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    array-length v0, v2

    move/from16 v17, v0

    .line 271
    .local v17, "descriptionCount":I
    const/16 v19, 0x0

    .local v19, "j":I
    :goto_2
    move/from16 v0, v19

    move/from16 v1, v17

    if-ge v0, v1, :cond_1

    .line 272
    move-object/from16 v0, v22

    iget-object v2, v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->description:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;

    aget-object v16, v2, v19

    .line 273
    .local v16, "description":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconList:Ljava/util/List;

    new-instance v7, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    move-object/from16 v0, v22

    iget-object v8, v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->title:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;->description:Ljava/lang/String;

    const/4 v10, 0x0

    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;->image:Lcom/google/android/finsky/protos/Common$Image;

    const/4 v12, 0x0

    invoke-direct/range {v7 .. v12}, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Z)V

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    add-int/lit8 v19, v19, 0x1

    goto :goto_2

    .line 268
    .end local v16    # "description":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsDescription;
    :cond_1
    add-int/lit8 v18, v18, 0x1

    goto :goto_1

    .line 278
    .end local v17    # "descriptionCount":I
    .end local v18    # "i":I
    .end local v19    # "j":I
    .end local v20    # "productDetails":Lcom/google/android/finsky/protos/DocDetails$ProductDetails;
    .end local v21    # "productDetailsSectionCount":I
    .end local v22    # "section":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    :cond_2
    return-void
.end method

.method private setBackgroundImage(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 209
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v4

    .line 210
    .local v4, "preferredHeroImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    const/16 v7, 0xd

    invoke-virtual {p1, v7}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v1

    .line 212
    .local v1, "fallbackHeroImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    move v3, v5

    .line 213
    .local v3, "hasPreferredHero":Z
    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    move v2, v5

    .line 215
    .local v2, "hasFallbackHero":Z
    :goto_1
    if-nez v3, :cond_2

    if-nez v2, :cond_2

    .line 216
    iput-boolean v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mHasBackgroundImage:Z

    .line 228
    :goto_2
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->setCardColor()V

    .line 229
    return-void

    .end local v2    # "hasFallbackHero":Z
    .end local v3    # "hasPreferredHero":Z
    :cond_0
    move v3, v6

    .line 212
    goto :goto_0

    .restart local v3    # "hasPreferredHero":Z
    :cond_1
    move v2, v6

    .line 213
    goto :goto_1

    .line 220
    .restart local v2    # "hasFallbackHero":Z
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->setCardColor()V

    .line 222
    iput-boolean v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mHasBackgroundImage:Z

    .line 223
    if-eqz v3, :cond_3

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/protos/Common$Image;

    move-object v0, v5

    .line 225
    .local v0, "backgroundImage":Lcom/google/android/finsky/protos/Common$Image;
    :goto_3
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v6, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v7, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v5, v6, v7, p2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    goto :goto_2

    .line 223
    .end local v0    # "backgroundImage":Lcom/google/android/finsky/protos/Common$Image;
    :cond_3
    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/protos/Common$Image;

    move-object v0, v5

    goto :goto_3
.end method

.method private setCardColor()V
    .locals 5

    .prologue
    .line 191
    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mLayoutType:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 204
    :goto_0
    return-void

    .line 194
    :cond_0
    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundColor:I

    invoke-static {v3}, Landroid/graphics/Color;->red(I)I

    move-result v2

    .line 195
    .local v2, "red":I
    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundColor:I

    invoke-static {v3}, Landroid/graphics/Color;->green(I)I

    move-result v1

    .line 196
    .local v1, "green":I
    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundColor:I

    invoke-static {v3}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 197
    .local v0, "blue":I
    iget-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mHasBackgroundImage:Z

    if-nez v3, :cond_1

    .line 199
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundColor:I

    invoke-virtual {v3, v4}, Lcom/google/android/play/image/FifeImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 202
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    const/16 v4, 0xbe

    invoke-static {v4, v2, v1, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/play/image/FifeImageView;->setColorFilter(I)V

    goto :goto_0
.end method

.method private setIcons(Lcom/google/android/finsky/api/model/Document;)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 233
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->fillIconList(Lcom/google/android/finsky/api/model/Document;)V

    .line 236
    invoke-static {}, Lcom/google/android/play/utils/collections/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 237
    .local v0, "extraIconPairs":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Lcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;>;>;"
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconList:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 238
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    .line 239
    .local v2, "primary":Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;
    iget-boolean v4, v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->skipInCollapsedMode:Z

    if-nez v4, :cond_0

    iget-object v4, v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v4, :cond_0

    .line 240
    new-instance v4, Landroid/util/Pair;

    iget-object v5, v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->image:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v6, v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->title:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 245
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "primary":Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 246
    .local v3, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v6

    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mLayoutType:I

    const/4 v7, 0x2

    if-ne v4, v7, :cond_2

    const v4, 0x7f09009d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    :goto_1
    invoke-virtual {v5, v0, v6, v4}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->populate(Ljava/util/List;Lcom/google/android/play/image/BitmapLoader;I)V

    .line 249
    return-void

    .line 246
    :cond_2
    const v4, 0x7f09004c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    goto :goto_1
.end method

.method private setThumbnail(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 3
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 155
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v1}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DocImageView;

    .line 156
    .local v0, "thumbnailImage":Lcom/google/android/finsky/layout/DocImageView;
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DocImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 157
    sget-object v1, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->CORE_IMAGE_TYPES:[I

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getItemThumbnailContentDescription(Lcom/google/android/finsky/api/model/Document;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DocImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 163
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mLayoutType:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 165
    new-instance v1, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DocImageView;->setOnLoadedListener(Lcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    .line 187
    :cond_0
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Z)V
    .locals 5
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "hasDetailsDataLoaded"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 122
    if-nez p3, :cond_1

    .line 123
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->setVisibility(I)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->setVisibility(I)V

    .line 129
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->setIcons(Lcom/google/android/finsky/api/model/Document;)V

    .line 130
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->setBackgroundImage(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;)V

    .line 131
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->setThumbnail(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;)V

    .line 133
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mTitle:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasRating()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 135
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getStarRating()F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 136
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v1, v3}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 140
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getPromotionalDescription()Ljava/lang/String;

    move-result-object v0

    .line 141
    .local v0, "description":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    if-eqz v1, :cond_0

    .line 142
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 143
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/PlayTextView;->setMaxLines(I)V

    .line 144
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v1, v0}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v1, v3}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto :goto_0

    .line 138
    .end local v0    # "description":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v1, v4}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    goto :goto_1

    .line 147
    .restart local v0    # "description":Ljava/lang/String;
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v1, v4}, Lcom/google/android/play/layout/PlayTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public bindLoading()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 102
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->bindLoading()V

    .line 103
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    if-eqz v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->setVisibility(I)V

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mActionButton:Lcom/google/android/play/layout/PlayActionButton;

    if-eqz v0, :cond_3

    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mActionButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 115
    :cond_3
    return-void
.end method

.method public getCardType()I
    .locals 1

    .prologue
    .line 97
    const/16 v0, 0xf

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->onFinishInflate()V

    .line 86
    const v0, 0x7f0a02c1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    .line 87
    const v0, 0x7f0a0225

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 88
    const v0, 0x7f0a02c2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    .line 89
    const v0, 0x7f0a02c3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mActionButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 91
    const v0, 0x7f0a02c7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mSmallTitleRow:Landroid/view/View;

    .line 92
    const v0, 0x7f0a02c8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mMiniBottomRow:Landroid/view/View;

    .line 93
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 328
    invoke-super/range {p0 .. p5}, Lcom/google/android/play/layout/PlayCardViewBase;->onLayout(ZIIII)V

    .line 329
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v3}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 331
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 286
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;->onMeasure(II)V

    .line 287
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 288
    .local v2, "availableWidth":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 289
    .local v1, "availableHeight":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 293
    .local v4, "heightMode":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mActionButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayActionButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 294
    .local v0, "actionButtonLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mLayoutType:I

    packed-switch v5, :pswitch_data_0

    .line 305
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mThumbnail:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v6}, Lcom/google/android/play/layout/StarRatingBar;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mActionButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayActionButton;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v3, v5, v6

    .line 311
    .local v3, "height":I
    :goto_0
    if-ne v4, v7, :cond_2

    .line 312
    move v3, v1

    .line 317
    :cond_0
    :goto_1
    iget v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mLayoutType:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_1

    .line 318
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->getPaddingLeft()I

    move-result v5

    sub-int v5, v2, v5

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 320
    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 321
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v5, p1, p2}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 323
    :cond_1
    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->setMeasuredDimension(II)V

    .line 324
    return-void

    .line 296
    .end local v3    # "height":I
    :pswitch_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mSmallTitleRow:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mBackgroundImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mDescription:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mIconContainer:Lcom/google/android/finsky/layout/DetailsTextIconContainer;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/DetailsTextIconContainer;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mActionButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayActionButton;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v3, v5, v6

    .line 299
    .restart local v3    # "height":I
    goto :goto_0

    .line 301
    .end local v3    # "height":I
    :pswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mSmallTitleRow:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardViewFeatured;->mMiniBottomRow:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int v3, v5, v6

    .line 302
    .restart local v3    # "height":I
    goto :goto_0

    .line 313
    :cond_2
    const/high16 v5, -0x80000000

    if-ne v4, v5, :cond_0

    .line 314
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_1

    .line 294
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
