.class public Lcom/google/android/finsky/widget/WidgetUtils;
.super Ljava/lang/Object;
.source "WidgetUtils.java"


# static fields
.field private static SUPPORTED_BACKENDS:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/widget/WidgetUtils;->SUPPORTED_BACKENDS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3
        0x2
        0x6
        0x1
        0x4
    .end array-data
.end method

.method static synthetic access$000(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-static {p0}, Lcom/google/android/finsky/widget/WidgetUtils;->getWidgetTypeForLibraryId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getAwarenessThreshold(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 197
    packed-switch p0, :pswitch_data_0

    .line 207
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 199
    :pswitch_1
    sget-object v0, Lcom/google/android/finsky/config/G;->corpusAwarenessThresholdMusic:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 205
    :goto_0
    return v0

    .line 201
    :pswitch_2
    sget-object v0, Lcom/google/android/finsky/config/G;->corpusAwarenessThresholdBooks:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 203
    :pswitch_3
    sget-object v0, Lcom/google/android/finsky/config/G;->corpusAwarenessThresholdMagazines:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 205
    :pswitch_4
    sget-object v0, Lcom/google/android/finsky/config/G;->corpusAwarenessThresholdMovies:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getBackendIcon(I)I
    .locals 1
    .param p0, "backend"    # I

    .prologue
    .line 164
    packed-switch p0, :pswitch_data_0

    .line 177
    :pswitch_0
    const v0, 0x7f030006

    :goto_0
    return v0

    .line 166
    :pswitch_1
    const/high16 v0, 0x7f030000

    goto :goto_0

    .line 168
    :pswitch_2
    const v0, 0x7f030003

    goto :goto_0

    .line 170
    :pswitch_3
    const v0, 0x7f030001

    goto :goto_0

    .line 172
    :pswitch_4
    const v0, 0x7f030004

    goto :goto_0

    .line 174
    :pswitch_5
    const v0, 0x7f030002

    goto :goto_0

    .line 164
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_5
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static getDips(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dimensResource"    # I

    .prologue
    .line 159
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 160
    .local v0, "res":Landroid/content/res/Resources;
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method public static getHotseatCheckIcon(I)I
    .locals 3
    .param p0, "backend"    # I

    .prologue
    .line 182
    packed-switch p0, :pswitch_data_0

    .line 192
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported backend ID ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :pswitch_1
    const v0, 0x7f0200d6

    .line 190
    :goto_0
    return v0

    .line 186
    :pswitch_2
    const v0, 0x7f0200d4

    goto :goto_0

    .line 188
    :pswitch_3
    const v0, 0x7f0200d7

    goto :goto_0

    .line 190
    :pswitch_4
    const v0, 0x7f0200d5

    goto :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static getWidgetTypeForLibraryId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "libraryId"    # Ljava/lang/String;

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_APPS:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    const-string v0, "apps"

    .line 82
    :goto_0
    return-object v0

    .line 72
    :cond_0
    sget-object v0, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_OCEAN:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    const-string v0, "books"

    goto :goto_0

    .line 74
    :cond_1
    sget-object v0, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_YOUTUBE:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    const-string v0, "movies"

    goto :goto_0

    .line 76
    :cond_2
    sget-object v0, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_MUSIC:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 77
    const-string v0, "music"

    goto :goto_0

    .line 78
    :cond_3
    sget-object v0, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_MAGAZINE:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 79
    const-string v0, "magazines"

    goto :goto_0

    .line 82
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static notifyAccountSwitch(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 147
    sget-object v2, Lcom/google/android/finsky/widget/WidgetUtils;->SUPPORTED_BACKENDS:[I

    .local v2, "arr$":[I
    array-length v6, v2

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget v3, v2, v5

    .line 148
    .local v3, "backend":I
    invoke-static {p0, v3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->deleteCachedRecommendations(Landroid/content/Context;I)V

    .line 147
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 150
    .end local v3    # "backend":I
    :cond_0
    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 151
    .local v1, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    new-instance v4, Landroid/content/ComponentName;

    const-class v7, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;

    invoke-direct {v4, p0, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    .local v4, "componentName":Landroid/content/ComponentName;
    invoke-virtual {v1, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 153
    .local v0, "appWidgetIds":[I
    array-length v7, v0

    if-lez v7, :cond_1

    .line 154
    invoke-static {p0, v0}, Lcom/google/android/finsky/widget/recommendation/RecommendationsViewFactory;->notifyDataSetChanged(Landroid/content/Context;[I)V

    .line 156
    :cond_1
    return-void
.end method

.method public static parseSparseIntArray(Ljava/lang/String;)Landroid/util/SparseIntArray;
    .locals 15
    .param p0, "map"    # Ljava/lang/String;

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 90
    const-string v10, ","

    invoke-virtual {p0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 91
    .local v9, "tuples":[Ljava/lang/String;
    new-instance v6, Landroid/util/SparseIntArray;

    array-length v10, v9

    invoke-direct {v6, v10}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 92
    .local v6, "result":Landroid/util/SparseIntArray;
    move-object v0, v9

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v8, v0, v3

    .line 93
    .local v8, "tuple":Ljava/lang/String;
    const/16 v10, 0x3a

    invoke-virtual {v8, v10}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    .line 94
    .local v2, "colonIndex":I
    if-gez v2, :cond_0

    .line 95
    const-string v10, "Invalid tuple: map=%s, tuple=%s"

    new-array v11, v14, [Ljava/lang/Object;

    aput-object p0, v11, v12

    aput-object v8, v11, v13

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 92
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 99
    :cond_0
    const/4 v10, 0x0

    :try_start_0
    invoke-virtual {v8, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 100
    .local v1, "backend":I
    add-int/lit8 v10, v2, 0x1

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v8, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 101
    .local v7, "score":I
    invoke-virtual {v6, v1, v7}, Landroid/util/SparseIntArray;->put(II)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 102
    .end local v1    # "backend":I
    .end local v7    # "score":I
    :catch_0
    move-exception v5

    .line 103
    .local v5, "nfe":Ljava/lang/NumberFormatException;
    const-string v10, "Malformed key or value: map=%s, tuple=%s"

    new-array v11, v14, [Ljava/lang/Object;

    aput-object p0, v11, v12

    aput-object v8, v11, v13

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 106
    .end local v2    # "colonIndex":I
    .end local v5    # "nfe":Ljava/lang/NumberFormatException;
    .end local v8    # "tuple":Ljava/lang/String;
    :cond_1
    return-object v6
.end method

.method public static registerLibraryMutationsListener(Landroid/content/Context;Lcom/google/android/finsky/library/LibraryReplicators;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "replicators"    # Lcom/google/android/finsky/library/LibraryReplicators;

    .prologue
    .line 131
    new-instance v0, Lcom/google/android/finsky/widget/WidgetUtils$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/widget/WidgetUtils$1;-><init>(Landroid/content/Context;)V

    invoke-interface {p1, v0}, Lcom/google/android/finsky/library/LibraryReplicators;->addListener(Lcom/google/android/finsky/library/LibraryReplicators$Listener;)V

    .line 144
    return-void
.end method

.method public static serializeSparseIntArray(Landroid/util/SparseIntArray;)Ljava/lang/String;
    .locals 6
    .param p0, "array"    # Landroid/util/SparseIntArray;

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .local v0, "buf":Ljava/lang/StringBuilder;
    const/4 v2, 0x1

    .line 115
    .local v2, "isFirst":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Landroid/util/SparseIntArray;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 116
    invoke-virtual {p0, v1}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v3

    .line 117
    .local v3, "key":I
    if-gez v3, :cond_0

    .line 115
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 120
    :cond_0
    if-nez v2, :cond_1

    .line 121
    const/16 v4, 0x2c

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 123
    :cond_1
    const/4 v2, 0x0

    .line 124
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x3a

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 126
    .end local v3    # "key":I
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static translate(Ljava/lang/String;)I
    .locals 3
    .param p0, "backendType"    # Ljava/lang/String;

    .prologue
    .line 35
    const-string v0, "all"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 46
    :goto_0
    return v0

    .line 37
    :cond_0
    const-string v0, "apps"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 38
    const/4 v0, 0x3

    goto :goto_0

    .line 39
    :cond_1
    const-string v0, "books"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 40
    const/4 v0, 0x1

    goto :goto_0

    .line 41
    :cond_2
    const-string v0, "movies"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 42
    const/4 v0, 0x4

    goto :goto_0

    .line 43
    :cond_3
    const-string v0, "music"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 44
    const/4 v0, 0x2

    goto :goto_0

    .line 45
    :cond_4
    const-string v0, "magazines"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 46
    const/4 v0, 0x6

    goto :goto_0

    .line 48
    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid backend type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static translate(I)Ljava/lang/String;
    .locals 3
    .param p0, "backendId"    # I

    .prologue
    .line 52
    packed-switch p0, :pswitch_data_0

    .line 66
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid backend ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :pswitch_1
    const-string v0, "apps"

    .line 64
    :goto_0
    return-object v0

    .line 56
    :pswitch_2
    const-string v0, "books"

    goto :goto_0

    .line 58
    :pswitch_3
    const-string v0, "movies"

    goto :goto_0

    .line 60
    :pswitch_4
    const-string v0, "music"

    goto :goto_0

    .line 62
    :pswitch_5
    const-string v0, "magazines"

    goto :goto_0

    .line 64
    :pswitch_6
    const-string v0, "all"

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_2
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method
