.class Lcom/google/android/finsky/layout/DetailsBylinesSection$1;
.super Ljava/lang/Object;
.source "DetailsBylinesSection.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsBylinesSection;->bind(Lcom/google/android/finsky/api/model/Document;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsBylinesSection;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsBylinesSection;Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$1;->this$0:Lcom/google/android/finsky/layout/DetailsBylinesSection;

    iput-object p2, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 147
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$1;->val$doc:Lcom/google/android/finsky/api/model/Document;

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->createIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Z)Landroid/content/Intent;

    move-result-object v0

    .line 150
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsBylinesSection$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 152
    return-void
.end method
