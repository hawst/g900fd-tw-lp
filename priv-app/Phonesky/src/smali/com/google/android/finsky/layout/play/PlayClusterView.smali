.class public abstract Lcom/google/android/finsky/layout/play/PlayClusterView;
.super Lcom/google/android/finsky/layout/IdentifiableRelativeLayout;
.source "PlayClusterView.java"

# interfaces
.implements Lcom/google/android/finsky/adapters/Recyclable;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field protected mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mParentOfChildren:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private final mVerticalPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayClusterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b014c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mVerticalPadding:I

    .line 47
    return-void
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 136
    invoke-static {p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 137
    return-void
.end method

.method protected configureLogging([BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p1, "serverLogsCookie"    # [B
    .param p2, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v0, 0x0

    .line 73
    if-eqz p1, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayClusterView;->getPlayStoreUiElementType()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 76
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 77
    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 78
    iput-object p0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mParentOfChildren:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 84
    :goto_0
    return-void

    .line 80
    :cond_0
    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 81
    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 82
    iput-object p2, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mParentOfChildren:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    goto :goto_0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method protected getParentOfChildren()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mParentOfChildren:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 66
    const/16 v0, 0x190

    return v0
.end method

.method public hasHeader()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideHeader()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setVisibility(I)V

    .line 104
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/google/android/finsky/layout/play/PlayClusterView;->setPadding(IIII)V

    .line 105
    return-void
.end method

.method protected logEmptyClusterImpression()V
    .locals 1

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayClusterView;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 96
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lcom/google/android/finsky/layout/IdentifiableRelativeLayout;->onFinishInflate()V

    .line 53
    const v0, 0x7f0a013f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayClusterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    .line 54
    return-void
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 61
    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mParentOfChildren:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 62
    return-void
.end method

.method public showHeader(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;I)V
    .locals 7
    .param p1, "backendId"    # I
    .param p2, "titleMain"    # Ljava/lang/String;
    .param p3, "titleSecondary"    # Ljava/lang/String;
    .param p4, "more"    # Ljava/lang/String;
    .param p5, "clickListener"    # Landroid/view/View$OnClickListener;
    .param p6, "headerExtraHorizontalPadding"    # I

    .prologue
    const/4 v6, 0x0

    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v0, v6}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mHeader:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v0, p6}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setExtraHorizontalPadding(I)V

    .line 115
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mVerticalPadding:I

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayClusterView;->mVerticalPadding:I

    invoke-virtual {p0, v6, v0, v6, v1}, Lcom/google/android/finsky/layout/play/PlayClusterView;->setPadding(IIII)V

    .line 116
    return-void
.end method
