.class final Lcom/google/android/finsky/gearhead/GearheadStateMonitor$2;
.super Ljava/lang/Object;
.source "GearheadStateMonitor.java"

# interfaces
.implements Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->initialize(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 104
    sget-object v0, Lcom/google/android/gms/car/Car;->CarApi:Lcom/google/android/gms/car/Car$CarApi;

    # getter for: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$200()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/Car$CarApi;->isConnectedToCar(Lcom/google/android/gms/common/api/GoogleApiClient;)Z

    move-result v0

    # setter for: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sIsProjecting:Z
    invoke-static {v0}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$002(Z)Z

    .line 105
    # invokes: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->onReady()V
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$100()V

    .line 108
    # getter for: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$200()Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->disconnect()V

    .line 109
    return-void
.end method

.method public onConnectionSuspended(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 114
    # invokes: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->onReady()V
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$100()V

    .line 115
    return-void
.end method
