.class public Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;
.super Ljava/lang/Object;
.source "CarrierBillingUtils.java"


# direct methods
.method public static addPrepareOrBillingProfileParams(ZZLjava/util/Map;)V
    .locals 1
    .param p0, "billingProfile"    # Z
    .param p1, "optimisticProvisioning"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 243
    .local p2, "outParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getCarrierBillingStorage()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->isDcb30(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 244
    invoke-static {p1, p2}, Lcom/google/android/finsky/billing/carrierbilling/Dcb2Util;->addPrepareOrBillingProfileParams(ZLjava/util/Map;)V

    .line 246
    :cond_0
    invoke-static {p0, p2}, Lcom/google/android/finsky/billing/carrierbilling/Dcb3Util;->addPrepareOrBillingProfileParams(ZLjava/util/Map;)V

    .line 247
    return-void
.end method

.method public static areCredentialsValid(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z
    .locals 12
    .param p0, "dcbStorage"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    .prologue
    const/4 v8, 0x0

    .line 58
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;->getCredentials()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;

    move-result-object v0

    .line 59
    .local v0, "credentials":Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;
    if-nez v0, :cond_0

    .line 65
    :goto_0
    return v8

    .line 62
    :cond_0
    sget-object v1, Lcom/google/android/finsky/config/G;->vendingCarrierCredentialsBufferMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 63
    .local v4, "expiryBufferTime":J
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->getExpirationTime()J

    move-result-wide v10

    sub-long v2, v10, v4

    .line 64
    .local v2, "expiresAt":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 65
    .local v6, "now":J
    invoke-static {p0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->isProvisioned(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->getCredentials()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v8, v1

    goto :goto_0

    :cond_1
    move v1, v8

    goto :goto_1
.end method

.method private static getInvalidEntries([Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Ljava/util/ArrayList;
    .locals 10
    .param p0, "inputErrors"    # [Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 113
    .local v2, "errors":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object v0, p0

    .local v0, "arr$":[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v0, v3

    .line 114
    .local v1, "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    iget v4, v1, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    .line 115
    .local v4, "inputField":I
    packed-switch v4, :pswitch_data_0

    .line 126
    :pswitch_0
    const-string v6, "InputValidationError that can\'t be edited: type=%d, message=%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, v1, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 123
    :pswitch_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 131
    .end local v1    # "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .end local v4    # "inputField":I
    :cond_0
    return-object v2

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getRetriableErrors(Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;)Ljava/util/ArrayList;
    .locals 3
    .param p0, "response"    # Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    iget v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->result:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 142
    iget-object v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$UpdateInstrumentResponse;->errorInputField:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    invoke-static {v1}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->getInvalidEntries([Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Ljava/util/ArrayList;

    move-result-object v0

    .line 144
    .local v0, "retriableErrorList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 148
    .end local v0    # "retriableErrorList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getSimIdentifier()Ljava/lang/String;
    .locals 3

    .prologue
    .line 260
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getSubscriberIdFromTelephony()Ljava/lang/String;

    move-result-object v1

    .line 261
    .local v1, "subscriberId":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 262
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/Sha1Util;->secureHash([B)Ljava/lang/String;

    move-result-object v2

    .line 269
    :goto_0
    return-object v2

    .line 265
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getDeviceIdFromTelephony()Ljava/lang/String;

    move-result-object v0

    .line 266
    .local v0, "deviceId":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 267
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/Sha1Util;->secureHash([B)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 269
    :cond_1
    const-string v2, "invalid_sim_id"

    goto :goto_0
.end method

.method public static initializeCarrierBillingParams(Landroid/content/Context;ZLjava/lang/Runnable;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "getProvisioningIfNotOnWifi"    # Z
    .param p2, "completionCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 171
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v2

    .line 172
    .local v2, "toc":Lcom/google/android/finsky/api/model/DfeToc;
    if-nez v2, :cond_0

    const/4 v0, 0x0

    .line 173
    .local v0, "config":Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;
    :goto_0
    new-instance v1, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierParamsAction;

    invoke-direct {v1, v0}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierParamsAction;-><init>(Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;)V

    .line 174
    .local v1, "cpa":Lcom/google/android/finsky/billing/carrierbilling/action/CarrierParamsAction;
    new-instance v3, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils$1;

    invoke-direct {v3, p1, p0, p2}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils$1;-><init>(ZLandroid/content/Context;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierParamsAction;->run(Ljava/lang/Runnable;)V

    .line 188
    return-void

    .line 172
    .end local v0    # "config":Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;
    .end local v1    # "cpa":Lcom/google/android/finsky/billing/carrierbilling/action/CarrierParamsAction;
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeToc;->getCarrierBillingConfig()Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    move-result-object v0

    goto :goto_0
.end method

.method public static initializeCarrierBillingProvisioning(Ljava/lang/Runnable;)V
    .locals 2
    .param p0, "completionCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 214
    invoke-static {}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->isCarrierBillingParamsAvailable()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 215
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierProvisioningAction;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierProvisioningAction;-><init>()V

    .line 216
    .local v0, "carrierProvisioningAction":Lcom/google/android/finsky/billing/carrierbilling/action/CarrierProvisioningAction;
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierProvisioningAction;->run(Ljava/lang/Runnable;)V

    .line 220
    .end local v0    # "carrierProvisioningAction":Lcom/google/android/finsky/billing/carrierbilling/action/CarrierProvisioningAction;
    :cond_0
    :goto_0
    return-void

    .line 217
    :cond_1
    if-eqz p0, :cond_0

    .line 218
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public static initializeCarrierBillingStorage(Ljava/lang/Runnable;)V
    .locals 1
    .param p0, "completionCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 157
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;-><init>()V

    .line 158
    .local v0, "cba":Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/billing/carrierbilling/action/CarrierBillingAction;->run(Ljava/lang/Runnable;)V

    .line 159
    return-void
.end method

.method public static initializeStorageAndParams(Landroid/content/Context;ZLjava/lang/Runnable;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "getProvisioningIfNotOnWifi"    # Z
    .param p2, "successRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 199
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils$2;-><init>(Landroid/content/Context;ZLjava/lang/Runnable;)V

    invoke-static {v0}, Lcom/google/android/finsky/billing/carrierbilling/CarrierBillingUtils;->initializeCarrierBillingStorage(Ljava/lang/Runnable;)V

    .line 206
    return-void
.end method

.method public static isCarrierBillingParamsAvailable()Z
    .locals 1

    .prologue
    .line 229
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getCarrierBillingStorage()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getCarrierBillingStorage()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;->getParams()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingParameters;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDcb30(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z
    .locals 5
    .param p0, "dcbStorage"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 94
    if-nez p0, :cond_1

    .line 95
    const-string v3, "CarrierBillingStorage is null, fallback to 3.0"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    :cond_0
    :goto_0
    return v1

    .line 98
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;->getParams()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingParameters;

    move-result-object v0

    .line 99
    .local v0, "params":Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingParameters;
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingParameters;->getCarrierApiVersion()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public static isProvisioned(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;)Z
    .locals 4
    .param p0, "dcbStorage"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;

    .prologue
    const/4 v1, 0x0

    .line 75
    if-nez p0, :cond_1

    .line 76
    const-string v2, "CarrierBillingStorage is null. Return false"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    :cond_0
    :goto_0
    return v1

    .line 79
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingStorage;->getProvisioning()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;

    move-result-object v0

    .line 80
    .local v0, "provisioning":Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;
    if-eqz v0, :cond_0

    .line 83
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->isProvisioned()Z

    move-result v1

    goto :goto_0
.end method
