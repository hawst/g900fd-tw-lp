.class public final Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "SearchSuggest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/SearchSuggest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SearchSuggestResponse"
.end annotation


# instance fields
.field public hasServerLogsCookie:Z

.field public serverLogsCookie:[B

.field public suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 401
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 402
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->clear()Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    .line 403
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;
    .locals 1

    .prologue
    .line 406
    invoke-static {}, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;->emptyArray()[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    .line 407
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->serverLogsCookie:[B

    .line 408
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->hasServerLogsCookie:Z

    .line 409
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->cachedSize:I

    .line 410
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 432
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 433
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 434
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 435
    iget-object v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    aget-object v0, v3, v1

    .line 436
    .local v0, "element":Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    if-eqz v0, :cond_0

    .line 437
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 434
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 442
    .end local v0    # "element":Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->hasServerLogsCookie:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->serverLogsCookie:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_3

    .line 443
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->serverLogsCookie:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 446
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 454
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 455
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 459
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 460
    :sswitch_0
    return-object p0

    .line 465
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 467
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    if-nez v5, :cond_2

    move v1, v4

    .line 468
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    .line 470
    .local v2, "newArray":[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    if-eqz v1, :cond_1

    .line 471
    iget-object v5, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 473
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 474
    new-instance v5, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;-><init>()V

    aput-object v5, v2, v1

    .line 475
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 476
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 473
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 467
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    array-length v1, v5

    goto :goto_1

    .line 479
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;-><init>()V

    aput-object v5, v2, v1

    .line 480
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 481
    iput-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    goto :goto_0

    .line 485
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->serverLogsCookie:[B

    .line 486
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->hasServerLogsCookie:Z

    goto :goto_0

    .line 455
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 377
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 416
    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 417
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 418
    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->suggestion:[Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;

    aget-object v0, v2, v1

    .line 419
    .local v0, "element":Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    if-eqz v0, :cond_0

    .line 420
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 417
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 424
    .end local v0    # "element":Lcom/google/android/finsky/protos/SearchSuggest$Suggestion;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->hasServerLogsCookie:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->serverLogsCookie:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    .line 425
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/SearchSuggest$SearchSuggestResponse;->serverLogsCookie:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 427
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 428
    return-void
.end method
