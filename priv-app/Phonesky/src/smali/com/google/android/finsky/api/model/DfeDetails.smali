.class public Lcom/google/android/finsky/api/model/DfeDetails;
.super Lcom/google/android/finsky/api/model/DfeModel;
.source "DfeDetails.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/api/model/DfeModel;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Details$DetailsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

.field private final mDetailsUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V
    .locals 2
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "detailsUrl"    # Ljava/lang/String;

    .prologue
    .line 30
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;ZLjava/util/Collection;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;ZLjava/util/Collection;)V
    .locals 7
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "detailsUrl"    # Ljava/lang/String;
    .param p3, "noPrefetch"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/DfeApi;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 45
    .local p4, "voucherIds":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/api/model/DfeModel;-><init>()V

    .line 46
    iput-object p2, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsUrl:Ljava/lang/String;

    .line 47
    iget-object v1, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsUrl:Ljava/lang/String;

    const/4 v3, 0x0

    move-object v0, p1

    move v2, p3

    move-object v4, p4

    move-object v5, p0

    move-object v6, p0

    invoke-interface/range {v0 .. v6}, Lcom/google/android/finsky/api/DfeApi;->getDetails(Ljava/lang/String;ZZLjava/util/Collection;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 49
    return-void
.end method


# virtual methods
.method public getDiscoveryBadges()[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    goto :goto_0
.end method

.method public getDocument()Lcom/google/android/finsky/api/model/Document;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v0, :cond_1

    .line 57
    :cond_0
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v0, v1}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    goto :goto_0
.end method

.method public getFooterHtml()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 112
    :cond_0
    const/4 v0, 0x0

    .line 114
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml:Ljava/lang/String;

    goto :goto_0
.end method

.method public getServerLogsCookie()[B
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie:[B

    array-length v0, v0

    if-nez v0, :cond_1

    .line 122
    :cond_0
    const/4 v0, 0x0

    .line 124
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie:[B

    goto :goto_0
.end method

.method public getUserReview()Lcom/google/android/finsky/protos/DocumentV2$Review;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-nez v0, :cond_1

    .line 105
    :cond_0
    const/4 v0, 0x0

    .line 107
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    goto :goto_0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResponse(Lcom/google/android/finsky/protos/Details$DetailsResponse;)V
    .locals 0
    .param p1, "response"    # Lcom/google/android/finsky/protos/Details$DetailsResponse;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/finsky/api/model/DfeDetails;->mDetailsResponse:Lcom/google/android/finsky/protos/Details$DetailsResponse;

    .line 91
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/DfeDetails;->notifyDataSetChanged()V

    .line 92
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Lcom/google/android/finsky/protos/Details$DetailsResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/DfeDetails;->onResponse(Lcom/google/android/finsky/protos/Details$DetailsResponse;)V

    return-void
.end method
