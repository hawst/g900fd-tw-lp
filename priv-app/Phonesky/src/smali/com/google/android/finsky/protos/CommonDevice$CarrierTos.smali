.class public final Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CarrierTos"
.end annotation


# instance fields
.field public dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

.field public hasNeedsDcbTosAcceptance:Z

.field public hasNeedsPiiTosAcceptance:Z

.field public needsDcbTosAcceptance:Z

.field public needsPiiTosAcceptance:Z

.field public piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2432
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2433
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->clear()Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    .line 2434
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2437
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    .line 2438
    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    .line 2439
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsDcbTosAcceptance:Z

    .line 2440
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->hasNeedsDcbTosAcceptance:Z

    .line 2441
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsPiiTosAcceptance:Z

    .line 2442
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->hasNeedsPiiTosAcceptance:Z

    .line 2443
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->cachedSize:I

    .line 2444
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2467
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2468
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    if-eqz v1, :cond_0

    .line 2469
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2472
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    if-eqz v1, :cond_1

    .line 2473
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2476
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->hasNeedsDcbTosAcceptance:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsDcbTosAcceptance:Z

    if-eqz v1, :cond_3

    .line 2477
    :cond_2
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsDcbTosAcceptance:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2480
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->hasNeedsPiiTosAcceptance:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsPiiTosAcceptance:Z

    if-eqz v1, :cond_5

    .line 2481
    :cond_4
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsPiiTosAcceptance:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2484
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2492
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2493
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2497
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2498
    :sswitch_0
    return-object p0

    .line 2503
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    if-nez v1, :cond_1

    .line 2504
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    .line 2506
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2510
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    if-nez v1, :cond_2

    .line 2511
    new-instance v1, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    .line 2513
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2517
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsDcbTosAcceptance:Z

    .line 2518
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->hasNeedsDcbTosAcceptance:Z

    goto :goto_0

    .line 2522
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsPiiTosAcceptance:Z

    .line 2523
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->hasNeedsPiiTosAcceptance:Z

    goto :goto_0

    .line 2493
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2401
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2450
    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    if-eqz v0, :cond_0

    .line 2451
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->dcbTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2453
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    if-eqz v0, :cond_1

    .line 2454
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->piiTos:Lcom/google/android/finsky/protos/CommonDevice$CarrierTosEntry;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2456
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->hasNeedsDcbTosAcceptance:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsDcbTosAcceptance:Z

    if-eqz v0, :cond_3

    .line 2457
    :cond_2
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsDcbTosAcceptance:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2459
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->hasNeedsPiiTosAcceptance:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsPiiTosAcceptance:Z

    if-eqz v0, :cond_5

    .line 2460
    :cond_4
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$CarrierTos;->needsPiiTosAcceptance:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2462
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2463
    return-void
.end method
