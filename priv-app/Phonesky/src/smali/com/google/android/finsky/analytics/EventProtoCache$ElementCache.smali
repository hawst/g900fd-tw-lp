.class Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;
.super Ljava/lang/Object;
.source "EventProtoCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/EventProtoCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ElementCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mCache:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field mClazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private mCount:I

.field private mHighWater:I

.field private final mLimit:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 1
    .param p2, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;, "Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache<TT;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    iput p2, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mLimit:I

    .line 175
    iput v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    .line 176
    iput v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mHighWater:I

    .line 177
    invoke-static {p1, p2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCache:[Ljava/lang/Object;

    .line 178
    iput-object p1, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mClazz:Ljava/lang/Class;

    .line 179
    return-void
.end method


# virtual methods
.method public obtain()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;, "Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache<TT;>;"
    const/4 v1, 0x0

    .line 184
    monitor-enter p0

    .line 185
    :try_start_0
    iget v2, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    if-lez v2, :cond_0

    .line 189
    iget v2, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    .line 190
    iget-object v2, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCache:[Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    aget-object v1, v2, v3

    .line 191
    .local v1, "result":Ljava/lang/Object;, "TT;"
    iget-object v2, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCache:[Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    const/4 v4, 0x0

    aput-object v4, v2, v3

    .line 192
    monitor-exit p0

    .line 200
    .end local v1    # "result":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object v1

    .line 194
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :try_start_1
    iget-object v2, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mClazz:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v1

    goto :goto_0

    .line 194
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Exception from mClazz.newInstance "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public recycle(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 206
    .local p0, "this":Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;, "Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache<TT;>;"
    .local p1, "element":Ljava/lang/Object;, "TT;"
    monitor-enter p0

    .line 217
    :try_start_0
    iget v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    iget v1, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mLimit:I

    if-ge v0, v1, :cond_0

    .line 218
    iget-object v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCache:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    aput-object p1, v0, v1

    .line 219
    iget v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    .line 220
    iget v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    iget v1, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mHighWater:I

    if-le v0, v1, :cond_0

    .line 221
    iget v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mCount:I

    iput v0, p0, Lcom/google/android/finsky/analytics/EventProtoCache$ElementCache;->mHighWater:I

    .line 228
    :cond_0
    monitor-exit p0

    .line 229
    return-void

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
