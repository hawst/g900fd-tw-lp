.class public final Lcom/google/android/finsky/analytics/PlayStore$ReviewData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReviewData"
.end annotation


# instance fields
.field public containsText:Z

.field public hasContainsText:Z

.field public hasRating:Z

.field public hasReviewSource:Z

.field public rating:I

.field public reviewSource:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5227
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5228
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->clear()Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    .line 5229
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$ReviewData;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5232
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->reviewSource:I

    .line 5233
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasReviewSource:Z

    .line 5234
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->rating:I

    .line 5235
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasRating:Z

    .line 5236
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->containsText:Z

    .line 5237
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasContainsText:Z

    .line 5238
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->cachedSize:I

    .line 5239
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 5259
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 5260
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->reviewSource:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasReviewSource:Z

    if-eqz v1, :cond_1

    .line 5261
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->reviewSource:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5264
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasRating:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->rating:I

    if-eqz v1, :cond_3

    .line 5265
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->rating:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5268
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasContainsText:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->containsText:Z

    if-eqz v1, :cond_5

    .line 5269
    :cond_4
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->containsText:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5272
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$ReviewData;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 5280
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5281
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5285
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5286
    :sswitch_0
    return-object p0

    .line 5291
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5292
    .local v1, "value":I
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    .line 5684
    :sswitch_2
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->reviewSource:I

    .line 5685
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasReviewSource:Z

    goto :goto_0

    .line 5691
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->rating:I

    .line 5692
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasRating:Z

    goto :goto_0

    .line 5696
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->containsText:Z

    .line 5697
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasContainsText:Z

    goto :goto_0

    .line 5281
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
    .end sparse-switch

    .line 5292
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0x8 -> :sswitch_2
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xb -> :sswitch_2
        0xc -> :sswitch_2
        0xd -> :sswitch_2
        0xe -> :sswitch_2
        0x64 -> :sswitch_2
        0x65 -> :sswitch_2
        0x66 -> :sswitch_2
        0x67 -> :sswitch_2
        0x68 -> :sswitch_2
        0x69 -> :sswitch_2
        0x6a -> :sswitch_2
        0x6b -> :sswitch_2
        0x6c -> :sswitch_2
        0x6d -> :sswitch_2
        0x6e -> :sswitch_2
        0x6f -> :sswitch_2
        0x70 -> :sswitch_2
        0x71 -> :sswitch_2
        0x72 -> :sswitch_2
        0x73 -> :sswitch_2
        0x74 -> :sswitch_2
        0x75 -> :sswitch_2
        0x76 -> :sswitch_2
        0x77 -> :sswitch_2
        0x78 -> :sswitch_2
        0x79 -> :sswitch_2
        0x7a -> :sswitch_2
        0x7b -> :sswitch_2
        0x7c -> :sswitch_2
        0x7d -> :sswitch_2
        0x7e -> :sswitch_2
        0x7f -> :sswitch_2
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_2
        0xca -> :sswitch_2
        0xcb -> :sswitch_2
        0xcc -> :sswitch_2
        0xcd -> :sswitch_2
        0xce -> :sswitch_2
        0xcf -> :sswitch_2
        0xd0 -> :sswitch_2
        0xd1 -> :sswitch_2
        0xd2 -> :sswitch_2
        0xd3 -> :sswitch_2
        0xd4 -> :sswitch_2
        0xd5 -> :sswitch_2
        0xd6 -> :sswitch_2
        0xd7 -> :sswitch_2
        0xd8 -> :sswitch_2
        0xd9 -> :sswitch_2
        0xda -> :sswitch_2
        0xdb -> :sswitch_2
        0xdc -> :sswitch_2
        0xdd -> :sswitch_2
        0xde -> :sswitch_2
        0xdf -> :sswitch_2
        0xe0 -> :sswitch_2
        0xe1 -> :sswitch_2
        0xe2 -> :sswitch_2
        0xe3 -> :sswitch_2
        0xe4 -> :sswitch_2
        0xe5 -> :sswitch_2
        0xe6 -> :sswitch_2
        0xe7 -> :sswitch_2
        0xe8 -> :sswitch_2
        0xe9 -> :sswitch_2
        0xea -> :sswitch_2
        0xeb -> :sswitch_2
        0xec -> :sswitch_2
        0xed -> :sswitch_2
        0xee -> :sswitch_2
        0xef -> :sswitch_2
        0xf0 -> :sswitch_2
        0xf1 -> :sswitch_2
        0xf2 -> :sswitch_2
        0xf3 -> :sswitch_2
        0xf4 -> :sswitch_2
        0xf5 -> :sswitch_2
        0xf6 -> :sswitch_2
        0xf7 -> :sswitch_2
        0xf8 -> :sswitch_2
        0xf9 -> :sswitch_2
        0xfa -> :sswitch_2
        0xfb -> :sswitch_2
        0xfc -> :sswitch_2
        0xfd -> :sswitch_2
        0xfe -> :sswitch_2
        0xff -> :sswitch_2
        0x100 -> :sswitch_2
        0x101 -> :sswitch_2
        0x102 -> :sswitch_2
        0x103 -> :sswitch_2
        0x104 -> :sswitch_2
        0x105 -> :sswitch_2
        0x106 -> :sswitch_2
        0x107 -> :sswitch_2
        0x108 -> :sswitch_2
        0x109 -> :sswitch_2
        0x10a -> :sswitch_2
        0x10b -> :sswitch_2
        0x10c -> :sswitch_2
        0x10d -> :sswitch_2
        0x10e -> :sswitch_2
        0x10f -> :sswitch_2
        0x110 -> :sswitch_2
        0x111 -> :sswitch_2
        0x112 -> :sswitch_2
        0x113 -> :sswitch_2
        0x114 -> :sswitch_2
        0x115 -> :sswitch_2
        0x116 -> :sswitch_2
        0x117 -> :sswitch_2
        0x118 -> :sswitch_2
        0x119 -> :sswitch_2
        0x11a -> :sswitch_2
        0x11b -> :sswitch_2
        0x11c -> :sswitch_2
        0x11d -> :sswitch_2
        0x11e -> :sswitch_2
        0x11f -> :sswitch_2
        0x120 -> :sswitch_2
        0x121 -> :sswitch_2
        0x122 -> :sswitch_2
        0x123 -> :sswitch_2
        0x124 -> :sswitch_2
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x12e -> :sswitch_2
        0x12f -> :sswitch_2
        0x130 -> :sswitch_2
        0x131 -> :sswitch_2
        0x132 -> :sswitch_2
        0x133 -> :sswitch_2
        0x134 -> :sswitch_2
        0x135 -> :sswitch_2
        0x136 -> :sswitch_2
        0x137 -> :sswitch_2
        0x138 -> :sswitch_2
        0x139 -> :sswitch_2
        0x13a -> :sswitch_2
        0x13b -> :sswitch_2
        0x13c -> :sswitch_2
        0x13d -> :sswitch_2
        0x13e -> :sswitch_2
        0x13f -> :sswitch_2
        0x140 -> :sswitch_2
        0x190 -> :sswitch_2
        0x191 -> :sswitch_2
        0x192 -> :sswitch_2
        0x193 -> :sswitch_2
        0x194 -> :sswitch_2
        0x195 -> :sswitch_2
        0x196 -> :sswitch_2
        0x197 -> :sswitch_2
        0x198 -> :sswitch_2
        0x199 -> :sswitch_2
        0x19a -> :sswitch_2
        0x19b -> :sswitch_2
        0x19c -> :sswitch_2
        0x19d -> :sswitch_2
        0x19e -> :sswitch_2
        0x19f -> :sswitch_2
        0x1a0 -> :sswitch_2
        0x1a1 -> :sswitch_2
        0x1f4 -> :sswitch_2
        0x1f5 -> :sswitch_2
        0x1f6 -> :sswitch_2
        0x1f7 -> :sswitch_2
        0x1f8 -> :sswitch_2
        0x1f9 -> :sswitch_2
        0x1fa -> :sswitch_2
        0x1fb -> :sswitch_2
        0x1fc -> :sswitch_2
        0x1fd -> :sswitch_2
        0x1fe -> :sswitch_2
        0x1ff -> :sswitch_2
        0x200 -> :sswitch_2
        0x201 -> :sswitch_2
        0x202 -> :sswitch_2
        0x203 -> :sswitch_2
        0x204 -> :sswitch_2
        0x205 -> :sswitch_2
        0x206 -> :sswitch_2
        0x258 -> :sswitch_2
        0x259 -> :sswitch_2
        0x25a -> :sswitch_2
        0x25b -> :sswitch_2
        0x2bc -> :sswitch_2
        0x2c6 -> :sswitch_2
        0x2c7 -> :sswitch_2
        0x2c8 -> :sswitch_2
        0x2c9 -> :sswitch_2
        0x2ca -> :sswitch_2
        0x2cb -> :sswitch_2
        0x2cc -> :sswitch_2
        0x2cd -> :sswitch_2
        0x2ce -> :sswitch_2
        0x2cf -> :sswitch_2
        0x2e4 -> :sswitch_2
        0x2ee -> :sswitch_2
        0x2ef -> :sswitch_2
        0x2f0 -> :sswitch_2
        0x2f1 -> :sswitch_2
        0x2f2 -> :sswitch_2
        0x302 -> :sswitch_2
        0x303 -> :sswitch_2
        0x307 -> :sswitch_2
        0x308 -> :sswitch_2
        0x309 -> :sswitch_2
        0x30a -> :sswitch_2
        0x30c -> :sswitch_2
        0x30d -> :sswitch_2
        0x30e -> :sswitch_2
        0x316 -> :sswitch_2
        0x317 -> :sswitch_2
        0x318 -> :sswitch_2
        0x319 -> :sswitch_2
        0x320 -> :sswitch_2
        0x321 -> :sswitch_2
        0x322 -> :sswitch_2
        0x32a -> :sswitch_2
        0x32b -> :sswitch_2
        0x32c -> :sswitch_2
        0x32d -> :sswitch_2
        0x32e -> :sswitch_2
        0x334 -> :sswitch_2
        0x335 -> :sswitch_2
        0x336 -> :sswitch_2
        0x337 -> :sswitch_2
        0x338 -> :sswitch_2
        0x339 -> :sswitch_2
        0x348 -> :sswitch_2
        0x349 -> :sswitch_2
        0x34a -> :sswitch_2
        0x34b -> :sswitch_2
        0x34c -> :sswitch_2
        0x34d -> :sswitch_2
        0x34e -> :sswitch_2
        0x34f -> :sswitch_2
        0x350 -> :sswitch_2
        0x351 -> :sswitch_2
        0x35c -> :sswitch_2
        0x35d -> :sswitch_2
        0x35e -> :sswitch_2
        0x35f -> :sswitch_2
        0x360 -> :sswitch_2
        0x361 -> :sswitch_2
        0x362 -> :sswitch_2
        0x370 -> :sswitch_2
        0x371 -> :sswitch_2
        0x372 -> :sswitch_2
        0x373 -> :sswitch_2
        0x374 -> :sswitch_2
        0x375 -> :sswitch_2
        0x376 -> :sswitch_2
        0x377 -> :sswitch_2
        0x378 -> :sswitch_2
        0x379 -> :sswitch_2
        0x37a -> :sswitch_2
        0x37b -> :sswitch_2
        0x37c -> :sswitch_2
        0x37d -> :sswitch_2
        0x37e -> :sswitch_2
        0x37f -> :sswitch_2
        0x380 -> :sswitch_2
        0x381 -> :sswitch_2
        0x382 -> :sswitch_2
        0x384 -> :sswitch_2
        0x385 -> :sswitch_2
        0x386 -> :sswitch_2
        0x387 -> :sswitch_2
        0x388 -> :sswitch_2
        0x3e8 -> :sswitch_2
        0x3e9 -> :sswitch_2
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_2
        0x3ec -> :sswitch_2
        0x3ed -> :sswitch_2
        0x3ee -> :sswitch_2
        0x3ef -> :sswitch_2
        0x3f0 -> :sswitch_2
        0x3f1 -> :sswitch_2
        0x3f2 -> :sswitch_2
        0x44d -> :sswitch_2
        0x44e -> :sswitch_2
        0x44f -> :sswitch_2
        0x450 -> :sswitch_2
        0x451 -> :sswitch_2
        0x452 -> :sswitch_2
        0x453 -> :sswitch_2
        0x4b0 -> :sswitch_2
        0x4b1 -> :sswitch_2
        0x4b2 -> :sswitch_2
        0x4b3 -> :sswitch_2
        0x4b4 -> :sswitch_2
        0x4b5 -> :sswitch_2
        0x4b6 -> :sswitch_2
        0x4b7 -> :sswitch_2
        0x4b8 -> :sswitch_2
        0x4c4 -> :sswitch_2
        0x4c5 -> :sswitch_2
        0x4ce -> :sswitch_2
        0x4cf -> :sswitch_2
        0x4d0 -> :sswitch_2
        0x4d1 -> :sswitch_2
        0x4d2 -> :sswitch_2
        0x4e2 -> :sswitch_2
        0x4e3 -> :sswitch_2
        0x4e4 -> :sswitch_2
        0x4f6 -> :sswitch_2
        0x4f7 -> :sswitch_2
        0x500 -> :sswitch_2
        0x501 -> :sswitch_2
        0x578 -> :sswitch_2
        0x579 -> :sswitch_2
        0x57a -> :sswitch_2
        0x57b -> :sswitch_2
        0x57c -> :sswitch_2
        0x57d -> :sswitch_2
        0x57e -> :sswitch_2
        0x57f -> :sswitch_2
        0x580 -> :sswitch_2
        0x581 -> :sswitch_2
        0x5dc -> :sswitch_2
        0x5dd -> :sswitch_2
        0x5de -> :sswitch_2
        0x5fa -> :sswitch_2
        0x5fb -> :sswitch_2
        0x5fc -> :sswitch_2
        0x640 -> :sswitch_2
        0x654 -> :sswitch_2
        0x655 -> :sswitch_2
        0x656 -> :sswitch_2
        0x657 -> :sswitch_2
        0x658 -> :sswitch_2
        0x659 -> :sswitch_2
        0x65a -> :sswitch_2
        0x65b -> :sswitch_2
        0x65c -> :sswitch_2
        0x65d -> :sswitch_2
        0x672 -> :sswitch_2
        0x673 -> :sswitch_2
        0x674 -> :sswitch_2
        0x675 -> :sswitch_2
        0x681 -> :sswitch_2
        0x682 -> :sswitch_2
        0x683 -> :sswitch_2
        0x684 -> :sswitch_2
        0x690 -> :sswitch_2
        0x691 -> :sswitch_2
        0x708 -> :sswitch_2
        0x709 -> :sswitch_2
        0x70a -> :sswitch_2
        0x70b -> :sswitch_2
        0x70c -> :sswitch_2
        0x70d -> :sswitch_2
        0x71c -> :sswitch_2
        0x9c4 -> :sswitch_2
        0x9c5 -> :sswitch_2
        0x9c6 -> :sswitch_2
        0x9c7 -> :sswitch_2
        0x9c8 -> :sswitch_2
        0x9c9 -> :sswitch_2
        0x9ca -> :sswitch_2
        0x9cb -> :sswitch_2
        0x9cc -> :sswitch_2
        0x9cd -> :sswitch_2
        0x9ce -> :sswitch_2
        0xa28 -> :sswitch_2
        0xa29 -> :sswitch_2
        0xa2a -> :sswitch_2
        0xa2b -> :sswitch_2
        0xa2c -> :sswitch_2
        0xa2d -> :sswitch_2
        0xa3c -> :sswitch_2
        0xa3d -> :sswitch_2
        0xa3e -> :sswitch_2
        0xa3f -> :sswitch_2
        0xa40 -> :sswitch_2
        0xa41 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5198
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$ReviewData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5245
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->reviewSource:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasReviewSource:Z

    if-eqz v0, :cond_1

    .line 5246
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->reviewSource:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5248
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasRating:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->rating:I

    if-eqz v0, :cond_3

    .line 5249
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->rating:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5251
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->hasContainsText:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->containsText:Z

    if-eqz v0, :cond_5

    .line 5252
    :cond_4
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$ReviewData;->containsText:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 5254
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5255
    return-void
.end method
