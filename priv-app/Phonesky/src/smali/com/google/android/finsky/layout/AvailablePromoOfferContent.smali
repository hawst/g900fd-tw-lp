.class public Lcom/google/android/finsky/layout/AvailablePromoOfferContent;
.super Landroid/widget/LinearLayout;
.source "AvailablePromoOfferContent.java"


# instance fields
.field private mDescriptionTextView:Landroid/widget/TextView;

.field private final mEdgeToEdgeThreshold:I

.field private mImageView:Lcom/google/android/play/image/FifeImageView;

.field private mIntroTextView:Landroid/widget/TextView;

.field private mTermsTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b011b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mEdgeToEdgeThreshold:I

    .line 43
    return-void
.end method


# virtual methods
.method public configure(Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Common$Image;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 5
    .param p1, "intro"    # Ljava/lang/CharSequence;
    .param p2, "image"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p3, "description"    # Ljava/lang/CharSequence;
    .param p4, "terms"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v4, 0x0

    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mIntroTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    if-eqz p2, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    iget-object v1, p2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v2, p2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0, v4}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 65
    :cond_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 70
    :cond_1
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    :cond_2
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 49
    const v0, 0x7f0a037c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mIntroTextView:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0a037d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    .line 51
    const v0, 0x7f0a037e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    .line 52
    const v0, 0x7f0a037f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    .line 53
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/16 v6, 0x8

    .line 117
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->getPaddingLeft()I

    move-result v0

    .line 118
    .local v0, "contentLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->getPaddingTop()I

    move-result v1

    .line 120
    .local v1, "contentTop":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mIntroTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mIntroTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mIntroTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 123
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mIntroTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v1, v3

    .line 125
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v3}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v3

    if-eq v3, v6, :cond_0

    .line 126
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v3}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 128
    .local v2, "imageViewLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v3

    .line 129
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v5}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 132
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v3}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v3

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 135
    .end local v2    # "imageViewLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-eq v3, v6, :cond_1

    .line 136
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 139
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v1, v3

    .line 142
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-eq v3, v6, :cond_2

    .line 143
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v3, v0, v1, v4, v5}, Landroid/widget/TextView;->layout(IIII)V

    .line 147
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 79
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 81
    .local v5, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->getPaddingBottom()I

    move-result v7

    add-int v2, v6, v7

    .line 82
    .local v2, "height":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->getPaddingLeft()I

    move-result v6

    sub-int v6, v5, v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->getPaddingRight()I

    move-result v7

    sub-int v0, v6, v7

    .line 83
    .local v0, "contentWidth":I
    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 85
    .local v1, "contentWidthSpec":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mIntroTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v8}, Landroid/widget/TextView;->measure(II)V

    .line 86
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mIntroTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v2, v6

    .line 88
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v6

    if-eq v6, v9, :cond_0

    .line 89
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 93
    .local v4, "imageViewLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mEdgeToEdgeThreshold:I

    if-gt v0, v6, :cond_3

    int-to-float v6, v0

    const/high16 v7, 0x3f100000    # 0.5625f

    mul-float/2addr v6, v7

    float-to-int v3, v6

    .line 96
    .local v3, "imageHeight":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-static {v3, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v6, v1, v7}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 99
    iget v6, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v6, v3

    iget v7, v4, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v6, v7

    add-int/2addr v2, v6

    .line 102
    .end local v3    # "imageHeight":I
    .end local v4    # "imageViewLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-eq v6, v9, :cond_1

    .line 103
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v8}, Landroid/widget/TextView;->measure(II)V

    .line 104
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mDescriptionTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v2, v6

    .line 107
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-eq v6, v9, :cond_2

    .line 108
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v1, v8}, Landroid/widget/TextView;->measure(II)V

    .line 109
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->mTermsTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v2, v6

    .line 112
    :cond_2
    invoke-virtual {p0, v5, v2}, Lcom/google/android/finsky/layout/AvailablePromoOfferContent;->setMeasuredDimension(II)V

    .line 113
    return-void

    .line 93
    .restart local v4    # "imageViewLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_3
    iget v3, v4, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    goto :goto_0
.end method
