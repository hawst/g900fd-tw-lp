.class public final Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InputValidationError"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;


# instance fields
.field public errorMessage:Ljava/lang/String;

.field public hasErrorMessage:Z

.field public hasInputField:Z

.field public inputField:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 246
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->clear()Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .line 248
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .locals 2

    .prologue
    .line 227
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    if-nez v0, :cond_1

    .line 228
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 230
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    if-nez v0, :cond_0

    .line 231
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    sput-object v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .line 233
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    return-object v0

    .line 233
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 251
    iput v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    .line 252
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasInputField:Z

    .line 253
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    .line 254
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasErrorMessage:Z

    .line 255
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->cachedSize:I

    .line 256
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 273
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 274
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasInputField:Z

    if-eqz v1, :cond_1

    .line 275
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 278
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasErrorMessage:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 279
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 282
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 290
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 291
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 295
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 296
    :sswitch_0
    return-object p0

    .line 301
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 302
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 323
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    .line 324
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasInputField:Z

    goto :goto_0

    .line 330
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    .line 331
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasErrorMessage:Z

    goto :goto_0

    .line 291
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    .line 302
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 221
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 262
    iget v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasInputField:Z

    if-eqz v0, :cond_1

    .line 263
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->inputField:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 265
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->hasErrorMessage:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 266
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;->errorMessage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 268
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 269
    return-void
.end method
