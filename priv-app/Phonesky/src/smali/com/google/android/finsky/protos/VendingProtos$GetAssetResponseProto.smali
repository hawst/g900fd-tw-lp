.class public final Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GetAssetResponseProto"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;
    }
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;


# instance fields
.field public additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

.field public installAsset:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2013
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2014
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    .line 2015
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;
    .locals 2

    .prologue
    .line 1996
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    if-nez v0, :cond_1

    .line 1997
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1999
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    if-nez v0, :cond_0

    .line 2000
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    sput-object v0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    .line 2002
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2004
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    return-object v0

    .line 2002
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;
    .locals 1

    .prologue
    .line 2018
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->installAsset:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    .line 2019
    invoke-static {}, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    .line 2020
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->cachedSize:I

    .line 2021
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 2043
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 2044
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->installAsset:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    if-eqz v3, :cond_0

    .line 2045
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->installAsset:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2048
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    array-length v3, v3

    if-lez v3, :cond_2

    .line 2049
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 2050
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    aget-object v0, v3, v1

    .line 2051
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    if-eqz v0, :cond_1

    .line 2052
    const/16 v3, 0xf

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2049
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2057
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    .end local v1    # "i":I
    :cond_2
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2065
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2066
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2070
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2071
    :sswitch_0
    return-object p0

    .line 2076
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->installAsset:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    if-nez v5, :cond_1

    .line 2077
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->installAsset:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    .line 2079
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->installAsset:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    const/4 v6, 0x1

    invoke-virtual {p1, v5, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    goto :goto_0

    .line 2083
    :sswitch_2
    const/16 v5, 0x7a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2085
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    if-nez v5, :cond_3

    move v1, v4

    .line 2086
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    .line 2088
    .local v2, "newArray":[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    if-eqz v1, :cond_2

    .line 2089
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2091
    :cond_2
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_4

    .line 2092
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;-><init>()V

    aput-object v5, v2, v1

    .line 2093
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2094
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2091
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2085
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    array-length v1, v5

    goto :goto_1

    .line 2097
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    :cond_4
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;-><init>()V

    aput-object v5, v2, v1

    .line 2098
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2099
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    goto :goto_0

    .line 2066
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xb -> :sswitch_1
        0x7a -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1669
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2027
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->installAsset:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    if-eqz v2, :cond_0

    .line 2028
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->installAsset:Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto$InstallAsset;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2030
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 2031
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 2032
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$GetAssetResponseProto;->additionalFile:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    aget-object v0, v2, v1

    .line 2033
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    if-eqz v0, :cond_1

    .line 2034
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2031
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2038
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    .end local v1    # "i":I
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2039
    return-void
.end method
