.class public Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;
.source "DetailsAvatarClusterHeader.java"


# instance fields
.field private final mAvatarSize:I

.field private mAvatarView:Lcom/google/android/play/image/FifeImageView;

.field private final mBackgroundHeight:I

.field private final mRegularHeaderXPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 37
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b010f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mBackgroundHeight:I

    .line 38
    const v1, 0x7f0b0165

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarSize:I

    .line 39
    const v1, 0x7f0b013c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mRegularHeaderXPadding:I

    .line 40
    return-void
.end method


# virtual methods
.method public getBackgroundImageHeight()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mBackgroundHeight:I

    return v0
.end method

.method public getBackgroundImageTopMargin()I
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->onFinishInflate()V

    .line 45
    const v0, 0x7f0a0141

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    .line 46
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 28
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getWidth()I

    move-result v22

    .line 119
    .local v22, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getHeight()I

    move-result v9

    .line 120
    .local v9, "height":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getPaddingLeft()I

    move-result v15

    .line 121
    .local v15, "paddingLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getPaddingTop()I

    move-result v16

    .line 122
    .local v16, "paddingTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getBackgroundImageTopMargin()I

    move-result v24

    add-int v24, v24, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v25

    add-int v25, v25, v15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getBackgroundImageTopMargin()I

    move-result v26

    add-int v26, v26, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v27

    add-int v26, v26, v27

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v26

    invoke-virtual {v0, v15, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 126
    div-int/lit8 v7, v22, 0x2

    .line 130
    .local v7, "centerX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v23

    const/16 v24, 0x8

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_0

    .line 131
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v23

    div-int/lit8 v23, v23, 0x2

    sub-int v5, v7, v23

    .line 132
    .local v5, "avatarLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v23

    add-int v6, v5, v23

    .line 133
    .local v6, "avatarRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v24

    add-int v24, v24, v16

    move-object/from16 v0, v23

    move/from16 v1, v16

    move/from16 v2, v24

    invoke-virtual {v0, v5, v1, v6, v2}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/play/image/FifeImageView;->getBottom()I

    move-result v21

    .line 137
    .local v21, "titleTop":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredWidth()I

    move-result v23

    div-int/lit8 v23, v23, 0x2

    sub-int v19, v7, v23

    .line 138
    .local v19, "titleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredWidth()I

    move-result v23

    add-int v20, v19, v23

    .line 139
    .local v20, "titleRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getMeasuredHeight()I

    move-result v24

    add-int v24, v24, v21

    move-object/from16 v0, v23

    move/from16 v1, v19

    move/from16 v2, v21

    move/from16 v3, v20

    move/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mMoreView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v23

    div-int/lit8 v23, v23, 0x2

    sub-int v12, v7, v23

    .line 143
    .local v12, "moreLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mMoreView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v23

    add-int v13, v12, v23

    .line 144
    .local v13, "moreRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mMoreView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getBottom()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getBottom()I

    move-result v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mMoreView:Landroid/widget/TextView;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v26

    add-int v25, v25, v26

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v12, v1, v13, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 162
    .end local v5    # "avatarLeft":I
    .end local v6    # "avatarRight":I
    .end local v12    # "moreLeft":I
    .end local v13    # "moreRight":I
    .end local v21    # "titleTop":I
    :goto_0
    return-void

    .line 149
    .end local v19    # "titleLeft":I
    .end local v20    # "titleRight":I
    :cond_0
    mul-int/lit8 v23, v9, 0x2

    div-int/lit8 v8, v23, 0x3

    .line 150
    .local v8, "contentBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v18

    .line 151
    .local v18, "titleHeight":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mRegularHeaderXPadding:I

    move/from16 v23, v0

    add-int v19, v15, v23

    .line 152
    .restart local v19    # "titleLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredWidth()I

    move-result v23

    add-int v20, v19, v23

    .line 153
    .restart local v20    # "titleRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    move-object/from16 v23, v0

    sub-int v24, v8, v18

    move-object/from16 v0, v23

    move/from16 v1, v19

    move/from16 v2, v24

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3, v8}, Landroid/view/View;->layout(IIII)V

    .line 156
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getPaddingRight()I

    move-result v23

    sub-int v23, v22, v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mRegularHeaderXPadding:I

    move/from16 v24, v0

    sub-int v17, v23, v24

    .line 157
    .local v17, "rightX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mMoreView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v14

    .line 158
    .local v14, "moreWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mMoreView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v11

    .line 159
    .local v11, "moreHeight":I
    sub-int v23, v18, v11

    div-int/lit8 v23, v23, 0x2

    sub-int v10, v8, v23

    .line 160
    .local v10, "moreBottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mMoreView:Landroid/widget/TextView;

    move-object/from16 v23, v0

    sub-int v24, v17, v14

    sub-int v25, v10, v11

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3, v10}, Landroid/widget/TextView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 89
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 90
    .local v1, "availableWidth":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 91
    .local v0, "availableHeight":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 93
    .local v5, "heightMode":I
    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 94
    iget v6, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mBackgroundHeight:I

    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 96
    .local v3, "backgroundHeightMeasureSpec":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6, p1, v3}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 98
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_0

    .line 99
    iget v6, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarSize:I

    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 100
    .local v2, "avatarMeasureSpec":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6, v2, v2}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 103
    .end local v2    # "avatarMeasureSpec":I
    :cond_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleGroup:Landroid/view/View;

    invoke-virtual {v6, v8, v8}, Landroid/view/View;->measure(II)V

    .line 104
    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v6, v8, v8}, Landroid/widget/TextView;->measure(II)V

    .line 106
    iget v6, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mBackgroundHeight:I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getPaddingTop()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getPaddingBottom()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getBackgroundImageTopMargin()I

    move-result v7

    add-int v4, v6, v7

    .line 108
    .local v4, "height":I
    if-ne v5, v9, :cond_2

    .line 109
    move v4, v0

    .line 113
    :cond_1
    :goto_0
    invoke-virtual {p0, v1, v4}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->setMeasuredDimension(II)V

    .line 114
    return-void

    .line 110
    :cond_2
    const/high16 v6, -0x80000000

    if-ne v5, v6, :cond_1

    .line 111
    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto :goto_0
.end method

.method public setContent(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/finsky/protos/Common$Image;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 11
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "backgroundImage"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p4, "avatarImage"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p5, "hasAvatarImage"    # Z
    .param p6, "titleMain"    # Ljava/lang/String;
    .param p7, "titleSecondary"    # Ljava/lang/String;
    .param p8, "more"    # Ljava/lang/String;
    .param p9, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 52
    .local v10, "res":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    const v1, 0x7f09005e

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setBackgroundColor(I)V

    .line 54
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    invoke-super/range {v0 .. v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mTitleMain:Landroid/widget/TextView;

    check-cast v0, Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-static {p2, p1, v0}, Lcom/google/android/finsky/utils/BadgeUtils;->configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    .line 60
    const/16 v8, 0x33

    .line 61
    .local v8, "alpha":I
    const v0, 0x7f09005a

    invoke-virtual {v10, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 62
    .local v9, "color":I
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-static {v9}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {v9}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {v9}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    invoke-static {v8, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setColorFilter(I)V

    .line 65
    if-eqz p5, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    iget-object v1, p4, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v2, p4, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->mAvatarView:Lcom/google/android/play/image/FifeImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_0
.end method
