.class public Lcom/google/android/finsky/adapters/CardListAdapter;
.super Lcom/google/android/finsky/adapters/FinskyListAdapter;
.source "CardListAdapter.java"

# interfaces
.implements Landroid/widget/AbsListView$RecyclerListener;
.implements Lcom/google/android/finsky/layout/play/PlayCardDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    }
.end annotation


# instance fields
.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field protected final mCardContentPadding:I

.field private final mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

.field private final mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field private mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

.field private final mColumnCount:I

.field private final mContainerId:Ljava/lang/String;

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private final mHasBannerHeader:Z

.field private mHasFilters:Z

.field private final mHasPlainHeader:Z

.field private final mHasSocialHeader:Z

.field private final mIsOrdered:Z

.field public mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final mLeadingExtraSpacerHeight:I

.field private final mLeadingSpacerHeight:I

.field private final mLooseDocsWithReasons:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;"
        }
    .end annotation
.end field

.field private final mLooseItemCellId:I

.field private final mLooseItemColCount:I

.field private final mNumQuickLinkRows:I

.field private final mNumQuickLinksPerRow:I

.field protected final mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

.field private final mShowLooseItemReasons:Z

.field private final mTitle:Ljava/lang/String;

.field private final mToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private final mUseMiniCards:Z

.field private final mUseTallTemplates:Z

.field private final mWarmWelcomeCardColumns:I

.field private final mWarmWelcomeHideGraphic:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/ContainerList;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "loader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p5, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p8, "quickLinks"    # [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    .param p9, "title"    # Ljava/lang/String;
    .param p10, "isRestoring"    # Z
    .param p11, "showLooseItemReasons"    # Z
    .param p12, "tabMode"    # I
    .param p13, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/finsky/api/DfeApi;",
            "Lcom/google/android/finsky/navigationmanager/NavigationManager;",
            "Lcom/google/android/play/image/BitmapLoader;",
            "Lcom/google/android/finsky/api/model/DfeToc;",
            "Lcom/google/android/finsky/utils/ClientMutationCache;",
            "Lcom/google/android/finsky/api/model/ContainerList",
            "<*>;[",
            "Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;",
            "Ljava/lang/String;",
            "ZZI",
            "Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 322
    .local p7, "containerList":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<*>;"
    move-object/from16 v0, p7

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/finsky/adapters/FinskyListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/ContainerList;)V

    .line 303
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    .line 323
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 324
    .local v5, "res":Landroid/content/res/Resources;
    iput-object p2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 325
    iput-object p4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 326
    iput-object p5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 327
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 328
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mTitle:Ljava/lang/String;

    .line 329
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static {v5, v8, v9}, Lcom/google/android/finsky/utils/UiUtils;->getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    .line 330
    new-instance v8, Lcom/google/android/finsky/layout/play/PlayCardHeap;

    invoke-direct {v8}, Lcom/google/android/finsky/layout/play/PlayCardHeap;-><init>()V

    iput-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    .line 331
    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    iget v6, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 332
    .local v6, "screenHeightPx":I
    const v8, 0x7f0b0139

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    if-le v6, v8, :cond_2

    const/4 v8, 0x1

    :goto_0
    iput-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    .line 334
    const v8, 0x7f0f000c

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseMiniCards:Z

    .line 336
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/finsky/utils/UiUtils;->getGridHorizontalPadding(Landroid/content/res/Resources;)I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    .line 338
    if-eqz p8, :cond_3

    move-object/from16 v0, p8

    array-length v4, v0

    .line 339
    .local v4, "quickLinkCount":I
    :goto_1
    const/4 v8, 0x0

    invoke-static {v5, v4, v8}, Lcom/google/android/finsky/utils/UiUtils;->getStreamQuickLinkColumnCount(Landroid/content/res/Resources;II)I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNumQuickLinksPerRow:I

    .line 340
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    .line 341
    iget v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNumQuickLinksPerRow:I

    invoke-static {v4, v8}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNumQuickLinkRows:I

    .line 343
    const v8, 0x7f0e0011

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mWarmWelcomeCardColumns:I

    .line 345
    iget v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mWarmWelcomeCardColumns:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    const v8, 0x7f0f000e

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-nez v8, :cond_4

    const/4 v8, 0x1

    :goto_2
    iput-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mWarmWelcomeHideGraphic:Z

    .line 348
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 350
    iget-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    .line 353
    .local v2, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->isOrdered()Z

    move-result v8

    :goto_3
    iput-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mIsOrdered:Z

    .line 354
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->hasContainerViews()Z

    move-result v8

    :goto_4
    iput-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasFilters:Z

    .line 355
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v8

    :goto_5
    iput-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerId:Ljava/lang/String;

    .line 357
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mShowLooseItemReasons:Z

    .line 360
    iget-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseMiniCards:Z

    if-eqz v8, :cond_8

    iget-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mShowLooseItemReasons:Z

    if-nez v8, :cond_8

    iget-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mIsOrdered:Z

    if-nez v8, :cond_8

    const/4 v7, 0x1

    .line 364
    .local v7, "useMiniCardsForLooseItems":Z
    :goto_6
    iget-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/ContainerList;->getBackendId()I

    move-result v8

    const/16 v9, 0x9

    if-ne v8, v9, :cond_9

    const/4 v3, 0x1

    .line 365
    .local v3, "isPeopleList":Z
    :goto_7
    if-eqz v3, :cond_a

    .line 366
    const v8, 0x7f04012d

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemCellId:I

    .line 369
    iget v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iget-boolean v9, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    invoke-static {v8, v9}, Lcom/google/android/finsky/layout/play/PlayCardPersonClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    .line 382
    :goto_8
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseDocsWithReasons:Ljava/util/List;

    .line 384
    if-eqz v2, :cond_d

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->hasContainerWithBannerTemplate()Z

    move-result v8

    :goto_9
    iput-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasBannerHeader:Z

    .line 386
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->hasRecommendationsContainerWithHeaderTemplate()Z

    move-result v8

    :goto_a
    iput-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasSocialHeader:Z

    .line 389
    iget-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasBannerHeader:Z

    if-nez v8, :cond_0

    iget-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasSocialHeader:Z

    if-eqz v8, :cond_f

    .line 390
    :cond_0
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasPlainHeader:Z

    .line 395
    :goto_b
    const/4 v8, 0x0

    move/from16 v0, p12

    invoke-static {p1, v0, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLeadingSpacerHeight:I

    .line 396
    const v8, 0x7f0b0176

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLeadingExtraSpacerHeight:I

    .line 399
    if-nez p10, :cond_1

    .line 401
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->syncItemEntries()V

    .line 409
    :cond_1
    new-instance v8, Lcom/google/android/finsky/adapters/CardListAdapter$1;

    invoke-direct {v8, p0}, Lcom/google/android/finsky/adapters/CardListAdapter$1;-><init>(Lcom/google/android/finsky/adapters/CardListAdapter;)V

    iput-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    .line 416
    return-void

    .line 332
    .end local v2    # "containerDocument":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "isPeopleList":Z
    .end local v4    # "quickLinkCount":I
    .end local v7    # "useMiniCardsForLooseItems":Z
    :cond_2
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 338
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_1

    .line 345
    .restart local v4    # "quickLinkCount":I
    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 353
    .restart local v2    # "containerDocument":Lcom/google/android/finsky/api/model/Document;
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 354
    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_4

    .line 355
    :cond_7
    iget-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/ContainerList;->getListPageUrls()Ljava/util/List;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v8, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    goto/16 :goto_5

    .line 360
    :cond_8
    const/4 v7, 0x0

    goto :goto_6

    .line 364
    .restart local v7    # "useMiniCardsForLooseItems":Z
    :cond_9
    const/4 v3, 0x0

    goto :goto_7

    .line 371
    .restart local v3    # "isPeopleList":Z
    :cond_a
    iget-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mShowLooseItemReasons:Z

    if-eqz v8, :cond_b

    .line 372
    const v8, 0x7f040136

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemCellId:I

    .line 373
    iget v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    goto :goto_8

    .line 374
    :cond_b
    if-eqz v7, :cond_c

    .line 375
    const v8, 0x7f040128

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemCellId:I

    .line 376
    const v8, 0x7f0e000a

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    goto :goto_8

    .line 378
    :cond_c
    const v8, 0x7f040124

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemCellId:I

    .line 379
    invoke-static {v5}, Lcom/google/android/finsky/utils/UiUtils;->getRegularGridColumnCount(Landroid/content/res/Resources;)I

    move-result v8

    iput v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    goto/16 :goto_8

    .line 384
    :cond_d
    const/4 v8, 0x0

    goto/16 :goto_9

    .line 386
    :cond_e
    const/4 v8, 0x0

    goto :goto_a

    .line 392
    :cond_f
    iget-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mTitle:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_10

    const/4 v8, 0x1

    :goto_c
    iput-boolean v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasPlainHeader:Z

    goto :goto_b

    :cond_10
    const/4 v8, 0x0

    goto :goto_c
.end method

.method static synthetic access$400(Lcom/google/android/finsky/adapters/CardListAdapter;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardListAdapter;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/adapters/CardListAdapter;)Lcom/google/android/finsky/api/model/DfeToc;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardListAdapter;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/adapters/CardListAdapter;)Lcom/google/android/finsky/utils/ClientMutationCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardListAdapter;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/adapters/CardListAdapter;)Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/CardListAdapter;

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    return-object v0
.end method

.method private bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V
    .locals 16
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "clusterMetadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p3, "cluster"    # Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    .param p4, "clusterClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 1576
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->hasHeader()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1577
    if-eqz p4, :cond_1

    invoke-direct/range {p0 .. p2}, Lcom/google/android/finsky/adapters/CardListAdapter;->getMoreResultsStringForCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;)Ljava/lang/String;

    move-result-object v6

    .line 1579
    .local v6, "moreString":Ljava/lang/String;
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    move-object/from16 v2, p3

    move-object/from16 v7, p4

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->showHeader(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    .line 1586
    .end local v6    # "moreString":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->withClusterDocumentData(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getPlayCardDismissListener()Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object/from16 v8, p2

    invoke-virtual/range {v7 .. v15}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1590
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->setCardContentHorizontalPadding(I)V

    .line 1592
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 1593
    return-void

    .line 1577
    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private bindLooseItem(Lcom/google/android/finsky/api/model/Document;ILandroid/view/View;Z)V
    .locals 11
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "trueIndex"    # I
    .param p3, "docEntry"    # Landroid/view/View;
    .param p4, "bindNoDoc"    # Z

    .prologue
    .line 1072
    move-object v0, p3

    check-cast v0, Lcom/google/android/play/layout/PlayCardViewBase;

    .line 1074
    .local v0, "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    if-nez p1, :cond_3

    .line 1076
    if-eqz p4, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v1

    if-ge p2, v1, :cond_2

    .line 1077
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayCardViewBase;->setVisibility(I)V

    .line 1078
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->bindLoading()V

    .line 1095
    .end local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_1
    :goto_0
    return-void

    .line 1080
    .restart local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardViewBase;->clearCardState()V

    goto :goto_0

    .line 1083
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/CardListAdapter;->isDismissed(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v5

    .line 1084
    .local v5, "isDismissed":Z
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getPlayCardDismissListener()Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    const/4 v8, 0x1

    iget-boolean v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mIsOrdered:Z

    if-eqz v1, :cond_4

    move v9, p2

    :goto_1
    move-object v1, p1

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/utils/PlayCardUtils;->bindCard(Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZI)V

    .line 1090
    instance-of v1, v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;

    if-eqz v1, :cond_1

    .line 1091
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isMyCirclesContainer()Z

    move-result v10

    .line 1092
    .local v10, "isMyCircles":Z
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;

    .end local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    if-nez v10, :cond_5

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->showCirclesIcon(Z)V

    goto :goto_0

    .line 1084
    .end local v10    # "isMyCircles":Z
    .restart local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    :cond_4
    const/4 v9, -0x1

    goto :goto_1

    .line 1092
    .end local v0    # "cardView":Lcom/google/android/play/layout/PlayCardViewBase;
    .restart local v10    # "isMyCircles":Z
    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private endLastEntry(Ljava/util/List;I)V
    .locals 4
    .param p2, "newStartIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 758
    .local p1, "entryList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;>;"
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 759
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 760
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    .line 761
    .local v1, "lastItem":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 762
    add-int/lit8 v2, p2, -0x1

    # setter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static {v1, v2}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$102(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;I)I

    .line 765
    .end local v1    # "lastItem":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    :cond_0
    return-void
.end method

.method private getActionBannerCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1395
    if-eqz p2, :cond_0

    const/4 v13, 0x1

    .line 1396
    .local v13, "canReuse":Z
    :goto_0
    if-eqz v13, :cond_1

    check-cast p2, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;

    .end local p2    # "convertView":Landroid/view/View;
    move-object/from16 v1, p2

    .line 1401
    .local v1, "cluster":Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/api/model/Document;

    .line 1406
    .local v4, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iget-boolean v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v14

    .line 1409
    .local v14, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getActionBanner()Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;

    move-result-object v11

    .line 1410
    .local v11, "actionBanner":Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
    iget-object v12, v11, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .line 1411
    .local v12, "actions":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    array-length v2, v12

    if-lez v2, :cond_2

    const/4 v2, 0x0

    aget-object v10, v12, v2

    .line 1413
    .local v10, "action":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :goto_2
    const/4 v2, 0x0

    invoke-direct {p0, v4, v14, v1, v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1415
    if-eqz v10, :cond_3

    iget-object v7, v10, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    .line 1417
    .local v7, "buttonText":Ljava/lang/String;
    :goto_3
    new-instance v9, Lcom/google/android/finsky/adapters/CardListAdapter$2;

    invoke-direct {v9, p0, v1, v10}, Lcom/google/android/finsky/adapters/CardListAdapter$2;-><init>(Lcom/google/android/finsky/adapters/CardListAdapter;Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;Lcom/google/android/finsky/protos/DocumentV2$CallToAction;)V

    .line 1429
    .local v9, "exploreClickListener":Landroid/view/View$OnClickListener;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v5, v11, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v6, v11, Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    move-object v8, v1

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;->configureExtraContent(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/view/View$OnClickListener;)V

    .line 1433
    return-object v1

    .line 1395
    .end local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;
    .end local v4    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v7    # "buttonText":Ljava/lang/String;
    .end local v9    # "exploreClickListener":Landroid/view/View$OnClickListener;
    .end local v10    # "action":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v11    # "actionBanner":Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
    .end local v12    # "actions":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v13    # "canReuse":Z
    .end local v14    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    const/4 v13, 0x0

    goto :goto_0

    .line 1396
    .restart local v13    # "canReuse":Z
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040115

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;

    move-object v1, v2

    goto :goto_1

    .line 1411
    .end local p2    # "convertView":Landroid/view/View;
    .restart local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;
    .restart local v4    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .restart local v11    # "actionBanner":Lcom/google/android/finsky/protos/DocumentV2$ActionBanner;
    .restart local v12    # "actions":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .restart local v14    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    :cond_2
    const/4 v10, 0x0

    goto :goto_2

    .line 1415
    .restart local v10    # "action":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_3
    const/4 v7, 0x0

    goto :goto_3
.end method

.method private getAddToCirclesCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1325
    if-eqz p2, :cond_0

    move v0, v7

    .line 1327
    .local v0, "canReuse":Z
    :goto_0
    if-eqz v0, :cond_1

    move-object v1, p2

    .line 1328
    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;

    .line 1335
    .local v1, "cluster":Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;
    :goto_1
    iget-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v8, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    .line 1336
    .local v3, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iget-boolean v9, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    invoke-static {v8, v9}, Lcom/google/android/finsky/layout/play/PlayCardPersonClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v4

    .line 1339
    .local v4, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v3, v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 1340
    .local v2, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v3, v4, v1, v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1342
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;->getCardChildCount()I

    move-result v8

    if-ge v5, v8, :cond_2

    .line 1343
    invoke-virtual {v1, v5}, Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;

    .line 1344
    .local v6, "personCard":Lcom/google/android/finsky/layout/play/PlayCardViewPerson;
    invoke-virtual {v6, v7}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->showCirclesIcon(Z)V

    .line 1342
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .end local v0    # "canReuse":Z
    .end local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;
    .end local v2    # "clusterClickListener":Landroid/view/View$OnClickListener;
    .end local v3    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v4    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .end local v5    # "i":I
    .end local v6    # "personCard":Lcom/google/android/finsky/layout/play/PlayCardViewPerson;
    :cond_0
    move v0, v8

    .line 1325
    goto :goto_0

    .line 1330
    .restart local v0    # "canReuse":Z
    :cond_1
    iget-object v9, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f040116

    invoke-virtual {v9, v10, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;

    .restart local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardAddToCirclesClusterView;
    goto :goto_1

    .line 1346
    .restart local v2    # "clusterClickListener":Landroid/view/View$OnClickListener;
    .restart local v3    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .restart local v4    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .restart local v5    # "i":I
    :cond_2
    return-object v1
.end method

.method private getBannerHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 975
    if-eqz p1, :cond_1

    move-object v3, p1

    :goto_0
    check-cast v3, Lcom/google/android/finsky/layout/DocImageView;

    move-object v0, v3

    check-cast v0, Lcom/google/android/finsky/layout/DocImageView;

    .line 978
    .local v0, "bannerImage":Lcom/google/android/finsky/layout/DocImageView;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 979
    .local v1, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getContainerWithBannerTemplate()Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;

    move-result-object v2

    .line 982
    .local v2, "template":Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 983
    iget-object v3, v2, Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/DocImageView;->setBackgroundColor(I)V

    .line 985
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v4, 0x1

    new-array v4, v4, [I

    const/16 v5, 0x9

    aput v5, v4, v6

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 987
    return-object v0

    .line 975
    .end local v0    # "bannerImage":Lcom/google/android/finsky/layout/DocImageView;
    .end local v1    # "containerDocument":Lcom/google/android/finsky/api/model/Document;
    .end local v2    # "template":Lcom/google/android/finsky/protos/DocumentV2$ContainerWithBanner;
    :cond_1
    const v3, 0x7f04004e

    invoke-virtual {p0, v3, p2, v6}, Lcom/google/android/finsky/adapters/CardListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto :goto_0
.end method

.method private getBaseCount()I
    .locals 3

    .prologue
    .line 769
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 770
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-eq v1, v2, :cond_0

    .line 771
    add-int/lit8 v0, v0, 0x1

    .line 774
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getBasePrependedRowsCount()I

    move-result v1

    add-int/2addr v1, v0

    return v1
.end method

.method private getBasePrependedRowsCount()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 779
    iget v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNumQuickLinkRows:I

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->isShowingPlainHeader()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v3, v0

    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasBannerHeader:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->hasFilters()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    iget-boolean v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasSocialHeader:Z

    if-eqz v3, :cond_3

    :goto_3
    add-int/2addr v0, v1

    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_3
.end method

.method private getCardListAdapterViewType(I)I
    .locals 10
    .param p1, "position"    # I

    .prologue
    const/16 v8, 0xe

    const/4 v7, 0x6

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1132
    if-nez p1, :cond_1

    .line 1133
    const/16 v5, 0x16

    .line 1261
    :cond_0
    :goto_0
    return v5

    .line 1135
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 1137
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->hasExtraLeadingSpacer()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1138
    if-nez p1, :cond_2

    .line 1139
    const/16 v5, 0x17

    goto :goto_0

    .line 1141
    :cond_2
    add-int/lit8 p1, p1, -0x1

    .line 1144
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    move-result-object v2

    .line 1145
    .local v2, "footerMode":Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;
    const/4 v3, 0x1

    .line 1146
    .local v3, "footerOffset":I
    sget-object v9, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-eq v2, v9, :cond_5

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getBaseCount()I

    move-result v9

    sub-int/2addr v9, v3

    if-ne p1, v9, :cond_5

    .line 1148
    sget-object v7, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->LOADING:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-eq v2, v7, :cond_0

    .line 1150
    sget-object v7, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->ERROR:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-ne v2, v7, :cond_4

    move v5, v6

    .line 1151
    goto :goto_0

    .line 1153
    :cond_4
    const-string v7, "Unexpected footer mode: %d"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v7, v5}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move v5, v6

    .line 1154
    goto :goto_0

    .line 1158
    :cond_5
    iget v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNumQuickLinkRows:I

    if-ge p1, v5, :cond_6

    .line 1159
    const/4 v5, 0x2

    goto :goto_0

    .line 1163
    :cond_6
    iget v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNumQuickLinkRows:I

    sub-int/2addr p1, v5

    .line 1165
    iget-boolean v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasSocialHeader:Z

    if-eqz v5, :cond_8

    .line 1166
    if-nez p1, :cond_7

    .line 1167
    const/16 v5, 0x11

    goto :goto_0

    .line 1169
    :cond_7
    add-int/lit8 p1, p1, -0x1

    .line 1172
    :cond_8
    iget-boolean v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasBannerHeader:Z

    if-eqz v5, :cond_a

    .line 1173
    if-nez p1, :cond_9

    .line 1174
    const/16 v5, 0x10

    goto :goto_0

    .line 1176
    :cond_9
    add-int/lit8 p1, p1, -0x1

    .line 1179
    :cond_a
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->isShowingPlainHeader()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 1180
    if-nez p1, :cond_b

    .line 1181
    const/4 v5, 0x7

    goto :goto_0

    .line 1183
    :cond_b
    add-int/lit8 p1, p1, -0x1

    .line 1186
    :cond_c
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->hasFilters()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1187
    if-nez p1, :cond_d

    .line 1188
    const/16 v5, 0x8

    goto :goto_0

    .line 1190
    :cond_d
    add-int/lit8 p1, p1, -0x1

    .line 1193
    :cond_e
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    .line 1194
    .local v0, "entry":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v0}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 1197
    .local v1, "firstItem":Lcom/google/android/finsky/api/model/Document;
    if-nez v1, :cond_f

    move v5, v7

    .line 1198
    goto/16 :goto_0

    .line 1201
    :cond_f
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->hasNextBanner()Z

    move-result v5

    if-eqz v5, :cond_10

    .line 1202
    const/4 v5, 0x3

    goto/16 :goto_0

    .line 1205
    :cond_10
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->hasDealOfTheDayInfo()Z

    move-result v5

    if-eqz v5, :cond_11

    .line 1206
    const/4 v5, 0x5

    goto/16 :goto_0

    .line 1209
    :cond_11
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isRateCluster()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 1210
    const/16 v5, 0xc

    goto/16 :goto_0

    .line 1213
    :cond_12
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isRateAndSuggestCluster()Z

    move-result v5

    if-eqz v5, :cond_13

    .line 1214
    const/16 v5, 0xd

    goto/16 :goto_0

    .line 1217
    :cond_13
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isAddToCirclesContainer()Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1218
    const/16 v5, 0xa

    goto/16 :goto_0

    .line 1221
    :cond_14
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isMyCirclesContainer()Z

    move-result v5

    if-eqz v5, :cond_15

    .line 1222
    const/16 v5, 0x13

    goto/16 :goto_0

    .line 1225
    :cond_15
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isTrustedSourceContainer()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 1226
    const/16 v5, 0xb

    goto/16 :goto_0

    .line 1229
    :cond_16
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isEmptyContainer()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 1230
    const/16 v5, 0xf

    goto/16 :goto_0

    .line 1233
    :cond_17
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isActionBanner()Z

    move-result v5

    if-eqz v5, :cond_18

    move v5, v8

    .line 1234
    goto/16 :goto_0

    .line 1237
    :cond_18
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isWarmWelcome()Z

    move-result v5

    if-eqz v5, :cond_19

    .line 1238
    const/16 v5, 0x12

    goto/16 :goto_0

    .line 1241
    :cond_19
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isSingleCardWithButton()Z

    move-result v5

    if-eqz v5, :cond_1a

    .line 1242
    const/16 v5, 0x15

    goto/16 :goto_0

    .line 1247
    :cond_1a
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->hasContainerAnnotation()Z

    move-result v4

    .line 1248
    .local v4, "hasContainer":Z
    if-nez v4, :cond_1b

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->hasAntennaInfo()Z

    move-result v5

    if-eqz v5, :cond_1e

    .line 1249
    :cond_1b
    invoke-virtual {v1, v8}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v5

    if-eqz v5, :cond_1c

    .line 1250
    const/4 v5, 0x4

    goto/16 :goto_0

    .line 1253
    :cond_1c
    if-eqz v4, :cond_1d

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v5

    iget-boolean v5, v5, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->ordered:Z

    if-eqz v5, :cond_1d

    .line 1254
    const/16 v5, 0x9

    goto/16 :goto_0

    .line 1258
    :cond_1d
    const/16 v5, 0x14

    goto/16 :goto_0

    :cond_1e
    move v5, v7

    .line 1261
    goto/16 :goto_0
.end method

.method private getContainerFilterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 1749
    if-nez p1, :cond_0

    .line 1750
    const v2, 0x7f04015c

    invoke-virtual {p0, v2, p2, v3}, Lcom/google/android/finsky/adapters/CardListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 1753
    :cond_0
    const v2, 0x7f0a0311

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 1754
    .local v1, "spinner":Landroid/widget/Spinner;
    const v2, 0x7f0a0312

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .local v0, "corpusHeaderStrip":Landroid/view/View;
    move-object v2, p1

    .line 1755
    check-cast v2, Lcom/google/android/finsky/layout/play/Identifiable;

    invoke-virtual {p0, v2, v1, v0}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindSpinnerData(Lcom/google/android/finsky/layout/play/Identifiable;Landroid/widget/Spinner;Landroid/view/View;)V

    .line 1756
    invoke-virtual {v1, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 1758
    return-object p1
.end method

.method private getEmptyCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 1469
    if-eqz p2, :cond_0

    const/4 v7, 0x1

    .line 1470
    .local v7, "canReuse":Z
    :goto_0
    if-eqz v7, :cond_1

    move-object v1, p2

    :goto_1
    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;

    move-object v0, v1

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;

    .line 1473
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/api/model/Document;

    .line 1475
    .local v8, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v3

    iget v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    move-object v5, v4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->showHeader(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    .line 1477
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v8, v1}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->createContent(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1479
    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 1481
    return-object v0

    .end local v0    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardEmptyClusterView;
    .end local v7    # "canReuse":Z
    .end local v8    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    move v7, v1

    .line 1469
    goto :goto_0

    .line 1470
    .restart local v7    # "canReuse":Z
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f04011b

    invoke-virtual {v2, v3, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    goto :goto_1
.end method

.method private getGenericCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 1311
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 1312
    .local v0, "canReuse":Z
    :goto_0
    if-eqz v0, :cond_1

    check-cast p2, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    .end local p2    # "convertView":Landroid/view/View;
    move-object v1, p2

    .line 1316
    .local v1, "cluster":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v5, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    .line 1317
    .local v3, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-direct {p0, v3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getGenericClusterMetadata(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v4

    .line 1319
    .local v4, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v3, v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 1320
    .local v2, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v3, v4, v1, v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1321
    return-object v1

    .end local v0    # "canReuse":Z
    .end local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    .end local v2    # "clusterClickListener":Landroid/view/View$OnClickListener;
    .end local v3    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v4    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    move v0, v5

    .line 1311
    goto :goto_0

    .line 1312
    .restart local v0    # "canReuse":Z
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f040119

    invoke-virtual {v6, v7, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    move-object v1, v5

    goto :goto_1
.end method

.method private getGenericClusterMetadata(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 7
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x0

    .line 1857
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v3

    .line 1858
    .local v3, "numChildren":I
    const/4 v2, 0x0

    .line 1859
    .local v2, "firstChildHasReason":Z
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    .line 1866
    .local v0, "documentType":I
    if-lez v3, :cond_0

    .line 1867
    invoke-virtual {p1, v5}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 1868
    .local v1, "firstChild":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    .line 1869
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v6

    if-eqz v6, :cond_1

    const/4 v2, 0x1

    .line 1873
    .end local v1    # "firstChild":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getSignalStrengthForCluster(Lcom/google/android/finsky/api/model/Document;)I

    move-result v4

    .line 1876
    .local v4, "signalStrength":I
    iget-boolean v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseMiniCards:Z

    if-eqz v5, :cond_2

    if-nez v2, :cond_2

    if-nez v4, :cond_2

    .line 1879
    iget v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v0, v5}, Lcom/google/android/finsky/layout/play/PlayCardMiniClusterRepository;->getMetadata(II)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v5

    .line 1883
    :goto_1
    return-object v5

    .end local v4    # "signalStrength":I
    .restart local v1    # "firstChild":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    move v2, v5

    .line 1869
    goto :goto_0

    .line 1883
    .end local v1    # "firstChild":Lcom/google/android/finsky/api/model/Document;
    .restart local v4    # "signalStrength":I
    :cond_2
    iget v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iget-boolean v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    invoke-static {v0, v5, v6, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->getMetadata(IIZI)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v5

    goto :goto_1
.end method

.method private getLooseItemRow(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "mTrueStartIndex"    # I
    .param p2, "mTrueEndIndex"    # I
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1019
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mShowLooseItemReasons:Z

    if-eqz v0, :cond_0

    .line 1020
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/adapters/CardListAdapter;->getRowOfLooseItemsWithReasons(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1023
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/adapters/CardListAdapter;->getRowOfLooseItemsWithoutReasons(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private getMerchBanner(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "trueStartIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 1727
    if-eqz p2, :cond_0

    move-object v0, p2

    .line 1728
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;

    .line 1734
    .local v0, "banner":Lcom/google/android/finsky/layout/play/PlayMerchBannerView;
    :goto_0
    iget v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iget v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v2, v4}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->init(II)V

    .line 1736
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/api/model/Document;

    .line 1737
    .local v7, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->setIdentifier(Ljava/lang/String;)V

    .line 1738
    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getNextBannerInfo()Lcom/google/android/finsky/protos/DocumentV2$NextBanner;

    move-result-object v1

    .line 1740
    .local v1, "nextBanner":Lcom/google/android/finsky/protos/DocumentV2$NextBanner;
    const/16 v2, 0xe

    invoke-virtual {v7, v2}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Common$Image;

    .line 1741
    .local v3, "bannerImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v4, v7, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->configureMerch(Lcom/google/android/finsky/protos/DocumentV2$NextBanner;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;[B)V

    .line 1745
    return-object v0

    .line 1730
    .end local v0    # "banner":Lcom/google/android/finsky/layout/play/PlayMerchBannerView;
    .end local v1    # "nextBanner":Lcom/google/android/finsky/protos/DocumentV2$NextBanner;
    .end local v3    # "bannerImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v7    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f04014e

    invoke-virtual {v2, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;

    .restart local v0    # "banner":Lcom/google/android/finsky/layout/play/PlayMerchBannerView;
    goto :goto_0
.end method

.method private getMerchCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v2, 0x0

    .line 1639
    if-eqz p2, :cond_0

    const/4 v6, 0x1

    .line 1640
    .local v6, "canReuse":Z
    :goto_0
    if-eqz v6, :cond_1

    check-cast p2, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;

    .end local p2    # "convertView":Landroid/view/View;
    move-object v0, p2

    .line 1644
    .local v0, "cluster":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;
    :goto_1
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/api/model/Document;

    .line 1645
    .local v7, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-direct {p0, v7}, Lcom/google/android/finsky/adapters/CardListAdapter;->getMerchClusterMetadata(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v8

    .line 1647
    .local v8, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v7, v0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v5

    .line 1648
    .local v5, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v7, v8, v0, v5}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1649
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/16 v3, 0xe

    invoke-virtual {v7, v3}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->configureMerch(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1653
    return-object v0

    .end local v0    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;
    .end local v5    # "clusterClickListener":Landroid/view/View$OnClickListener;
    .end local v6    # "canReuse":Z
    .end local v7    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v8    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    move v6, v2

    .line 1639
    goto :goto_0

    .line 1640
    .restart local v6    # "canReuse":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040127

    invoke-virtual {v1, v3, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;

    move-object v0, v1

    goto :goto_1
.end method

.method private getMerchClusterMetadata(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .locals 3
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 1850
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    .line 1852
    .local v0, "documentType":I
    :goto_0
    iget v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iget-boolean v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterRepository;->getMetadata(IIZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v1

    return-object v1

    .line 1850
    .end local v0    # "documentType":I
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    goto :goto_0
.end method

.method private getMoreResultsStringForCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;)Ljava/lang/String;
    .locals 12
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "clusterMetadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    .prologue
    .line 1604
    const/4 v1, 0x0

    .line 1605
    .local v1, "moreString":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v0

    .line 1607
    .local v0, "containerData":Lcom/google/android/finsky/protos/Containers$ContainerMetadata;
    if-eqz v0, :cond_4

    iget-object v5, v0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_4

    .line 1609
    iget-wide v2, v0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->estimatedResults:J

    .line 1610
    .local v2, "estimatedResults":J
    const-wide/16 v8, 0x0

    cmp-long v5, v2, v8

    if-lez v5, :cond_3

    .line 1614
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v8

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1616
    .local v4, "numDocsShown":I
    int-to-long v8, v4

    sub-long v6, v2, v8

    .line 1617
    .local v6, "remaining":J
    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gtz v5, :cond_1

    .line 1635
    .end local v2    # "estimatedResults":J
    .end local v4    # "numDocsShown":I
    .end local v6    # "remaining":J
    :cond_0
    :goto_0
    return-object v1

    .line 1619
    .restart local v2    # "estimatedResults":J
    .restart local v4    # "numDocsShown":I
    .restart local v6    # "remaining":J
    :cond_1
    const-wide/16 v8, 0x63

    cmp-long v5, v6, v8

    if-gtz v5, :cond_2

    .line 1621
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0c02e9

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v5, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1624
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0c02ea

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1628
    .end local v4    # "numDocsShown":I
    .end local v6    # "remaining":J
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0c02ea

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 1630
    .end local v2    # "estimatedResults":J
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasAntennaInfo()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1633
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0c02ea

    invoke-virtual {v5, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getMyCirclesCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 1350
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 1352
    .local v0, "canReuse":Z
    :goto_0
    if-eqz v0, :cond_1

    move-object v1, p2

    .line 1353
    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;

    .line 1359
    .local v1, "cluster":Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;
    :goto_1
    iget-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v8, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    .line 1360
    .local v3, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iget-boolean v9, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    invoke-static {v8, v9}, Lcom/google/android/finsky/layout/play/PlayCardPersonClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v4

    .line 1363
    .local v4, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v3, v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 1364
    .local v2, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v3, v4, v1, v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1366
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;->getCardChildCount()I

    move-result v8

    if-ge v5, v8, :cond_2

    .line 1367
    invoke-virtual {v1, v5}, Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;

    .line 1368
    .local v6, "personCard":Lcom/google/android/finsky/layout/play/PlayCardViewPerson;
    invoke-virtual {v6, v7}, Lcom/google/android/finsky/layout/play/PlayCardViewPerson;->showCirclesIcon(Z)V

    .line 1366
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .end local v0    # "canReuse":Z
    .end local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;
    .end local v2    # "clusterClickListener":Landroid/view/View$OnClickListener;
    .end local v3    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v4    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .end local v5    # "i":I
    .end local v6    # "personCard":Lcom/google/android/finsky/layout/play/PlayCardViewPerson;
    :cond_0
    move v0, v7

    .line 1350
    goto :goto_0

    .line 1355
    .restart local v0    # "canReuse":Z
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v9, 0x7f040129

    invoke-virtual {v8, v9, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;

    .restart local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardMyCirclesClusterView;
    goto :goto_1

    .line 1370
    .restart local v2    # "clusterClickListener":Landroid/view/View$OnClickListener;
    .restart local v3    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .restart local v4    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .restart local v5    # "i":I
    :cond_2
    return-object v1
.end method

.method private getOrderedCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 909
    if-nez p2, :cond_0

    .line 910
    const v2, 0x7f04012c

    const/4 v3, 0x0

    invoke-virtual {p0, v2, p3, v3}, Lcom/google/android/finsky/adapters/CardListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;

    .line 912
    .local v0, "clusterView":Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 913
    .local v7, "res":Landroid/content/res/Resources;
    const v2, 0x7f0e0017

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-static {v7}, Lcom/google/android/finsky/utils/UiUtils;->getRegularGridColumnCount(Landroid/content/res/Resources;)I

    move-result v3

    iget v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->inflateGrid(III)V

    .line 920
    .end local v7    # "res":Landroid/content/res/Resources;
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 921
    .local v1, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 923
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getChildren()[Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;->bind(Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 925
    return-object v0

    .end local v0    # "clusterView":Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;
    .end local v1    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    move-object v0, p2

    .line 916
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;

    .restart local v0    # "clusterView":Lcom/google/android/finsky/layout/play/PlayCardOrderedClusterView;
    goto :goto_0
.end method

.method private getPlainHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 961
    if-nez p1, :cond_0

    .line 962
    const v1, 0x7f04011a

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :cond_0
    move-object v0, p1

    .line 965
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    .line 967
    .local v0, "plainHeader":Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getBackendId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mTitle:Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 968
    iget v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setExtraHorizontalPadding(I)V

    .line 969
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "plain_header:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setIdentifier(Ljava/lang/String;)V

    .line 971
    return-object p1
.end method

.method private getQuickLinksRowView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 11
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "position"    # I

    .prologue
    .line 948
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object v4, p1

    check-cast v4, Landroid/view/ViewGroup;

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mQuickLinks:[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;

    add-int/lit8 v7, p3, -0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->hasExtraLeadingSpacer()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    sub-int/2addr v7, v5

    iget v8, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNumQuickLinksPerRow:I

    iget-object v9, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object v5, p2

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/adapters/QuickLinkHelper;->getQuickLinksRow(Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/view/LayoutInflater;Lcom/google/android/play/image/BitmapLoader;Landroid/view/ViewGroup;Landroid/view/ViewGroup;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;IILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View;

    move-result-object v10

    .local v10, "quickLinksRow":Landroid/view/View;
    move-object v0, v10

    .line 952
    check-cast v0, Lcom/google/android/finsky/layout/play/Identifiable;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "quick_link_row:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/layout/play/Identifiable;->setIdentifier(Ljava/lang/String;)V

    .line 953
    invoke-virtual {v10}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    add-int/2addr v0, v1

    invoke-virtual {v10}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {v10}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    iget v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    add-int/2addr v2, v3

    invoke-virtual {v10}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 957
    return-object v10

    .line 948
    .end local v10    # "quickLinksRow":Landroid/view/View;
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private getQuickSuggestionsCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 1452
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 1453
    .local v0, "canReuse":Z
    :goto_0
    if-eqz v0, :cond_1

    move-object v4, p2

    :goto_1
    check-cast v4, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    move-object v1, v4

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;

    .line 1456
    .local v1, "cluster":Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v4, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 1457
    .local v2, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->setClusterFadeOutListener(Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;)V

    .line 1459
    invoke-virtual {v2, v5}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    iget v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iget-boolean v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    invoke-static {v4, v5, v6}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterRepository;->getMetadata(IIZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v3

    .line 1464
    .local v3, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1465
    return-object v1

    .end local v0    # "canReuse":Z
    .end local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;
    .end local v2    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    :cond_0
    move v0, v5

    .line 1452
    goto :goto_0

    .line 1453
    .restart local v0    # "canReuse":Z
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f040131

    invoke-virtual {v4, v6, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    goto :goto_1
.end method

.method private getRateCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 1437
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 1438
    .local v0, "canReuse":Z
    :goto_0
    if-eqz v0, :cond_1

    move-object v4, p2

    :goto_1
    check-cast v4, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    move-object v1, v4

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    .line 1441
    .local v1, "cluster":Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v4, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 1442
    .local v2, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->setClusterFadeOutListener(Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;)V

    .line 1444
    iget v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    invoke-static {v4}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterRepository;->getMetadata(I)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v3

    .line 1447
    .local v3, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1448
    return-object v1

    .end local v0    # "canReuse":Z
    .end local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;
    .end local v2    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    :cond_0
    move v0, v4

    .line 1437
    goto :goto_0

    .line 1438
    .restart local v0    # "canReuse":Z
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f040132

    invoke-virtual {v5, v6, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    goto :goto_1
.end method

.method private getRowOfLooseItemsWithReasons(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 17
    .param p1, "trueStartIndex"    # I
    .param p2, "trueEndIndex"    # I
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1275
    if-eqz p3, :cond_2

    const/4 v10, 0x1

    .line 1276
    .local v10, "canReuse":Z
    :goto_0
    if-eqz v10, :cond_3

    check-cast p3, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    .end local p3    # "convertView":Landroid/view/View;
    move-object/from16 v12, p3

    .line 1280
    .local v12, "cluster":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseDocsWithReasons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1281
    const/4 v14, 0x0

    .line 1282
    .local v14, "firstNonNull":Lcom/google/android/finsky/api/model/Document;
    move/from16 v15, p1

    .local v15, "i":I
    :goto_2
    move/from16 v0, p2

    if-gt v15, v0, :cond_4

    .line 1283
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1, v15}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/api/model/Document;

    .line 1284
    .local v11, "child":Lcom/google/android/finsky/api/model/Document;
    if-eqz v11, :cond_1

    .line 1285
    if-nez v14, :cond_0

    .line 1286
    move-object v14, v11

    .line 1288
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseDocsWithReasons:Ljava/util/List;

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1282
    :cond_1
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 1275
    .end local v10    # "canReuse":Z
    .end local v11    # "child":Lcom/google/android/finsky/api/model/Document;
    .end local v12    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    .end local v14    # "firstNonNull":Lcom/google/android/finsky/api/model/Document;
    .end local v15    # "i":I
    .restart local p3    # "convertView":Landroid/view/View;
    :cond_2
    const/4 v10, 0x0

    goto :goto_0

    .line 1276
    .restart local v10    # "canReuse":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f040119

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v1, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    move-object v12, v1

    goto :goto_1

    .line 1293
    .end local p3    # "convertView":Landroid/view/View;
    .restart local v12    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    .restart local v14    # "firstNonNull":Lcom/google/android/finsky/api/model/Document;
    .restart local v15    # "i":I
    :cond_4
    if-eqz v14, :cond_5

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v13

    .line 1294
    .local v13, "documentType":I
    :goto_3
    if-eqz v14, :cond_6

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    :goto_4
    invoke-virtual {v12, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 1296
    const/16 v1, 0x1c

    if-ne v13, v1, :cond_7

    const/16 v16, 0x1

    .line 1297
    .local v16, "isPersonCluster":Z
    :goto_5
    if-eqz v16, :cond_8

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    invoke-static {v1, v3}, Lcom/google/android/finsky/layout/play/PlayCardPersonClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v2

    .line 1302
    .local v2, "looseClusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    :goto_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseDocsWithReasons:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerId:Ljava/lang/String;

    invoke-virtual {v12, v1, v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->withLooseDocumentsData(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getPlayCardDismissListener()Lcom/google/android/finsky/layout/play/PlayCardDismissListener;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1305
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    invoke-virtual {v12, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->setCardContentHorizontalPadding(I)V

    .line 1306
    invoke-virtual {v12}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->hideHeader()V

    .line 1307
    return-object v12

    .line 1293
    .end local v2    # "looseClusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .end local v13    # "documentType":I
    .end local v16    # "isPersonCluster":Z
    :cond_5
    const/4 v13, -0x1

    goto :goto_3

    .line 1294
    .restart local v13    # "documentType":I
    :cond_6
    const-string v1, ""

    goto :goto_4

    .line 1296
    :cond_7
    const/16 v16, 0x0

    goto :goto_5

    .line 1297
    .restart local v16    # "isPersonCluster":Z
    :cond_8
    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    const/4 v4, 0x0

    invoke-static {v13, v1, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterRepository;->getMetadata(IIZI)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v2

    goto :goto_6
.end method

.method private getRowOfLooseItemsWithoutReasons(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .param p1, "trueStartIndex"    # I
    .param p2, "trueEndIndex"    # I
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    .line 1030
    if-nez p3, :cond_0

    .line 1033
    const v7, 0x7f040044

    invoke-virtual {p0, v7, p4, v9}, Lcom/google/android/finsky/adapters/CardListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 1035
    const/4 v1, 0x0

    .local v1, "column":I
    :goto_0
    iget v7, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    if-ge v1, v7, :cond_0

    move-object v7, p3

    .line 1038
    check-cast v7, Landroid/view/ViewGroup;

    iget v10, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemCellId:I

    move-object v8, p3

    check-cast v8, Landroid/view/ViewGroup;

    invoke-virtual {p0, v10, v8, v9}, Lcom/google/android/finsky/adapters/CardListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1035
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .end local v1    # "column":I
    :cond_0
    move-object v0, p3

    .line 1042
    check-cast v0, Lcom/google/android/finsky/layout/BucketRow;

    .line 1043
    .local v0, "bucketRow":Lcom/google/android/finsky/layout/BucketRow;
    iget v7, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v7}, Lcom/google/android/finsky/layout/BucketRow;->setContentHorizontalPadding(I)V

    .line 1044
    const/4 v3, 0x0

    .line 1045
    .local v3, "firstDocId":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget v7, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    if-ge v4, v7, :cond_4

    .line 1046
    add-int v6, p1, v4

    .line 1049
    .local v6, "trueIndex":I
    if-le v6, p2, :cond_2

    const/4 v5, 0x1

    .line 1050
    .local v5, "pastBounds":Z
    :goto_2
    if-eqz v5, :cond_3

    const/4 v2, 0x0

    .line 1051
    .local v2, "doc":Lcom/google/android/finsky/api/model/Document;
    :goto_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz v2, :cond_1

    .line 1052
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    .line 1054
    :cond_1
    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/BucketRow;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-direct {p0, v2, v6, v7, v5}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindLooseItem(Lcom/google/android/finsky/api/model/Document;ILandroid/view/View;Z)V

    .line 1045
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .end local v2    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v5    # "pastBounds":Z
    :cond_2
    move v5, v9

    .line 1049
    goto :goto_2

    .line 1050
    .restart local v5    # "pastBounds":Z
    :cond_3
    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v7, v6}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/api/model/Document;

    move-object v2, v7

    goto :goto_3

    .line 1056
    .end local v5    # "pastBounds":Z
    .end local v6    # "trueIndex":I
    :cond_4
    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/BucketRow;->setIdentifier(Ljava/lang/String;)V

    .line 1058
    return-object p3
.end method

.method private getSignalStrengthForCluster(Lcom/google/android/finsky/api/model/Document;)I
    .locals 7
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 1893
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v6

    if-ge v6, v5, :cond_1

    .line 1894
    const-string v5, "Not enough children in cluster."

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1922
    :cond_0
    :goto_0
    return v4

    .line 1898
    :cond_1
    invoke-virtual {p1, v4}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 1899
    .local v0, "firstChild":Lcom/google/android/finsky/api/model/Document;
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    .line 1900
    .local v2, "secondChild":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v1

    .line 1902
    .local v1, "firstReason":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getSuggestionReasons()Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/finsky/utils/PlayCardUtils;->findHighestPriorityReason(Lcom/google/android/finsky/protos/DocumentV2$SuggestionReasons;)Lcom/google/android/finsky/protos/DocumentV2$Reason;

    move-result-object v3

    .line 1906
    .local v3, "secondReason":Lcom/google/android/finsky/protos/DocumentV2$Reason;
    if-eqz v1, :cond_0

    .line 1910
    iget-object v6, v1, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    if-eqz v6, :cond_3

    .line 1912
    if-eqz v3, :cond_2

    iget-object v4, v3, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonReview:Lcom/google/android/finsky/protos/DocumentV2$ReasonReview;

    if-eqz v4, :cond_2

    .line 1913
    const/4 v4, 0x4

    goto :goto_0

    :cond_2
    move v4, v5

    .line 1916
    goto :goto_0

    .line 1917
    :cond_3
    iget-object v5, v1, Lcom/google/android/finsky/protos/DocumentV2$Reason;->reasonPlusOne:Lcom/google/android/finsky/protos/DocumentV2$ReasonPlusOne;

    if-eqz v5, :cond_0

    .line 1919
    const/4 v4, 0x3

    goto :goto_0
.end method

.method private getSingleDocCluster(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 21
    .param p1, "trueStartIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;
    .param p4, "showActionButton"    # Z

    .prologue
    .line 1665
    if-eqz p2, :cond_1

    const/4 v13, 0x1

    .line 1666
    .local v13, "canReuse":Z
    :goto_0
    if-eqz v13, :cond_2

    check-cast p2, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;

    .end local p2    # "convertView":Landroid/view/View;
    move-object/from16 v16, p2

    .line 1670
    .local v16, "cluster":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    move/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/google/android/finsky/api/model/Document;

    .line 1671
    .local v17, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    const/4 v4, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v15

    .line 1672
    .local v15, "child":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->setIdentifier(Ljava/lang/String;)V

    .line 1677
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/api/model/Document;->getRawDescription()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v15, v4}, Lcom/google/android/finsky/api/model/Document;->setDescription(Ljava/lang/String;)V

    .line 1679
    if-eqz p4, :cond_3

    const/16 v20, 0x1a1

    .line 1682
    .local v20, "uiElementType":I
    :goto_2
    new-instance v18, Lcom/google/android/finsky/adapters/CardListAdapter$5;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move/from16 v2, v20

    move-object/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/adapters/CardListAdapter$5;-><init>(Lcom/google/android/finsky/adapters/CardListAdapter;ILcom/google/android/finsky/api/model/Document;)V

    .line 1693
    .local v18, "headerClickListener":Landroid/view/View$OnClickListener;
    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    move/from16 v0, p4

    invoke-static {v4, v6, v0}, Lcom/google/android/finsky/layout/play/PlayCardSingleCardClusterRepository;->getMetadata(IIZ)Lcom/google/android/finsky/layout/play/PlayCardSingleCardClusterMetadata;

    move-result-object v5

    .line 1699
    .local v5, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual/range {v16 .. v17}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->withClusterDocumentData(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v4 .. v12}, Lcom/google/android/finsky/layout/play/PlayCardClusterView;->createContent(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayCardDismissListener;Lcom/google/android/finsky/layout/play/PlayCardHeap;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1703
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->setCardContentHorizontalPadding(I)V

    .line 1705
    const/16 v4, 0xe

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v19

    .line 1706
    .local v19, "promoImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    const/4 v6, 0x2

    if-le v4, v6, :cond_4

    if-eqz v19, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1707
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const/4 v8, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v6, v16

    move-object/from16 v11, v18

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->configureMerch(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1714
    :goto_3
    const/4 v4, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v14

    .line 1715
    .local v14, "card":Lcom/google/android/play/layout/PlayCardViewBase;
    if-eqz p4, :cond_0

    .line 1716
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->getContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, v16

    invoke-static {v15, v4, v14, v0, v6}, Lcom/google/android/finsky/utils/PlayCardUtils;->updatePrimaryActionButton(Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/play/layout/PlayCardViewBase;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 1719
    :cond_0
    invoke-virtual {v15}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v7

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    move-object/from16 v6, v16

    move-object/from16 v11, v18

    invoke-virtual/range {v6 .. v12}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->showHeader(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    .line 1722
    return-object v16

    .line 1665
    .end local v5    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .end local v13    # "canReuse":Z
    .end local v14    # "card":Lcom/google/android/play/layout/PlayCardViewBase;
    .end local v15    # "child":Lcom/google/android/finsky/api/model/Document;
    .end local v16    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;
    .end local v17    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v18    # "headerClickListener":Landroid/view/View$OnClickListener;
    .end local v19    # "promoImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    .end local v20    # "uiElementType":I
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_1
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 1666
    .restart local v13    # "canReuse":Z
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f040127

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v4, v6, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;

    move-object/from16 v16, v4

    goto/16 :goto_1

    .line 1679
    .end local p2    # "convertView":Landroid/view/View;
    .restart local v15    # "child":Lcom/google/android/finsky/api/model/Document;
    .restart local v16    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;
    .restart local v17    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    :cond_3
    const/16 v20, 0x197

    goto/16 :goto_2

    .line 1711
    .restart local v5    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .restart local v18    # "headerClickListener":Landroid/view/View$OnClickListener;
    .restart local v19    # "promoImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    .restart local v20    # "uiElementType":I
    :cond_4
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/layout/play/PlayCardMerchClusterView;->configureNoMerch()V

    goto :goto_3
.end method

.method private getSocialHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 992
    if-eqz p1, :cond_0

    move-object v0, p1

    .line 993
    check-cast v0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;

    .line 998
    .local v0, "header":Lcom/google/android/finsky/layout/play/PlaySocialHeader;
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v6

    .line 999
    .local v6, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getRecommendationsContainerWithHeaderTemplate()Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;

    move-result-object v7

    .line 1002
    .local v7, "template":Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
    const/16 v4, 0xe

    invoke-virtual {v6, v4}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Common$Image;

    .line 1003
    .local v1, "cover":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v2, v7, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->primaryFace:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1004
    .local v2, "primaryPerson":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v3, v7, Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;->secondaryFace:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1006
    .local v3, "secondaryPersons":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlaySocialHeader;->bind(Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/finsky/protos/DocumentV2$DocV2;[Lcom/google/android/finsky/protos/DocumentV2$DocV2;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1007
    return-object v0

    .line 995
    .end local v0    # "header":Lcom/google/android/finsky/layout/play/PlaySocialHeader;
    .end local v1    # "cover":Lcom/google/android/finsky/protos/Common$Image;
    .end local v2    # "primaryPerson":Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v3    # "secondaryPersons":[Lcom/google/android/finsky/protos/DocumentV2$DocV2;
    .end local v6    # "containerDocument":Lcom/google/android/finsky/api/model/Document;
    .end local v7    # "template":Lcom/google/android/finsky/protos/DocumentV2$RecommendationsContainerWithHeader;
    :cond_0
    const v4, 0x7f0401b2

    invoke-virtual {p0, v4, p2, v5}, Lcom/google/android/finsky/adapters/CardListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlaySocialHeader;

    .restart local v0    # "header":Lcom/google/android/finsky/layout/play/PlaySocialHeader;
    goto :goto_0
.end method

.method private getTrustedSourceCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 1374
    if-eqz p2, :cond_0

    const/4 v0, 0x1

    .line 1375
    .local v0, "canReuse":Z
    :goto_0
    if-eqz v0, :cond_1

    check-cast p2, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;

    .end local p2    # "convertView":Landroid/view/View;
    move-object v1, p2

    .line 1380
    .local v1, "cluster":Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v5, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    .line 1382
    .local v3, "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    iget v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mColumnCount:I

    iget-boolean v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mUseTallTemplates:Z

    invoke-static {v5, v6}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterRepository;->getMetadata(IZ)Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v4

    .line 1385
    .local v4, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    invoke-virtual {p0, v3, v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 1386
    .local v2, "clusterClickListener":Landroid/view/View$OnClickListener;
    invoke-direct {p0, v3, v4, v1, v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->bindCluster(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/layout/play/PlayCardClusterView;Landroid/view/View$OnClickListener;)V

    .line 1388
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getTrustedSourcePersonDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v7

    invoke-virtual {v1, v5, v6, v7, v1}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;->configurePersonProfile(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1391
    return-object v1

    .end local v0    # "canReuse":Z
    .end local v1    # "cluster":Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;
    .end local v2    # "clusterClickListener":Landroid/view/View$OnClickListener;
    .end local v3    # "clusterDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v4    # "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_0
    move v0, v5

    .line 1374
    goto :goto_0

    .line 1375
    .restart local v0    # "canReuse":Z
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f04013c

    invoke-virtual {v6, v7, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterView;

    move-object v1, v5

    goto :goto_1
.end method

.method private getWarmWelcomeCardView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 23
    .param p1, "index"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1485
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mWarmWelcomeCardColumns:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    const v18, 0x7f0401d2

    .line 1487
    .local v18, "layoutId":I
    :goto_0
    const/4 v2, 0x0

    .line 1488
    .local v2, "warmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    if-eqz p2, :cond_1

    move-object/from16 v2, p2

    .line 1489
    check-cast v2, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

    .line 1494
    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/api/model/Document;

    .line 1495
    .local v12, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getWarmWelcome()Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;

    move-result-object v22

    .line 1497
    .local v22, "warmWelcomeTemplate":Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;
    const/4 v3, 0x4

    invoke-virtual {v12, v3}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v13

    .line 1498
    .local v13, "docImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v21, 0x0

    .line 1499
    .local v21, "warmWelcomeGraphic":Lcom/google/android/finsky/protos/Common$Image;
    :goto_2
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mWarmWelcomeHideGraphic:Z

    if-eqz v5, :cond_3

    const/4 v5, 0x0

    :goto_3
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->configureContent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Common$Image;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;[B)V

    .line 1503
    const/4 v11, 0x0

    .line 1504
    .local v11, "dismissAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    const/16 v20, 0x0

    .line 1506
    .local v20, "secondaryAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    move-object/from16 v0, v22

    iget-object v9, v0, Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;->action:[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    .local v9, "arr$":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    array-length v0, v9

    move/from16 v19, v0

    .local v19, "len$":I
    const/16 v17, 0x0

    .local v17, "i$":I
    :goto_4
    move/from16 v0, v17

    move/from16 v1, v19

    if-ge v0, v1, :cond_5

    aget-object v10, v9, v17

    .line 1507
    .local v10, "callToAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    iget v3, v10, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    .line 1508
    move-object v11, v10

    .line 1506
    :goto_5
    add-int/lit8 v17, v17, 0x1

    goto :goto_4

    .line 1485
    .end local v2    # "warmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    .end local v9    # "arr$":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v10    # "callToAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v11    # "dismissAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v12    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v13    # "docImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    .end local v17    # "i$":I
    .end local v18    # "layoutId":I
    .end local v19    # "len$":I
    .end local v20    # "secondaryAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .end local v21    # "warmWelcomeGraphic":Lcom/google/android/finsky/protos/Common$Image;
    .end local v22    # "warmWelcomeTemplate":Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;
    :cond_0
    const v18, 0x7f0401d1

    goto :goto_0

    .line 1491
    .restart local v2    # "warmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    .restart local v18    # "layoutId":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const/4 v4, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p3

    invoke-virtual {v3, v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .end local v2    # "warmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    check-cast v2, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

    .restart local v2    # "warmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    goto :goto_1

    .line 1498
    .restart local v12    # "doc":Lcom/google/android/finsky/api/model/Document;
    .restart local v13    # "docImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    .restart local v22    # "warmWelcomeTemplate":Lcom/google/android/finsky/protos/DocumentV2$WarmWelcome;
    :cond_2
    const/4 v3, 0x0

    invoke-interface {v13, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/protos/Common$Image;

    move-object/from16 v21, v3

    goto :goto_2

    .restart local v21    # "warmWelcomeGraphic":Lcom/google/android/finsky/protos/Common$Image;
    :cond_3
    move-object/from16 v5, v21

    .line 1499
    goto :goto_3

    .line 1510
    .restart local v9    # "arr$":[Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .restart local v10    # "callToAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .restart local v11    # "dismissAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .restart local v17    # "i$":I
    .restart local v19    # "len$":I
    .restart local v20    # "secondaryAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_4
    move-object/from16 v20, v10

    goto :goto_5

    .line 1513
    .end local v10    # "callToAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    :cond_5
    move-object v14, v11

    .line 1514
    .local v14, "finalDismissAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    move-object/from16 v15, v20

    .line 1515
    .local v15, "finalSecondaryAction":Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    move-object/from16 v16, v2

    .line 1517
    .local v16, "finalWarmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    iget-object v3, v14, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    iget-object v4, v14, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    new-instance v5, Lcom/google/android/finsky/adapters/CardListAdapter$3;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v5, v0, v1, v14, v12}, Lcom/google/android/finsky/adapters/CardListAdapter$3;-><init>(Lcom/google/android/finsky/adapters/CardListAdapter;Lcom/google/android/finsky/layout/play/WarmWelcomeCard;Lcom/google/android/finsky/protos/DocumentV2$CallToAction;Lcom/google/android/finsky/api/model/Document;)V

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->configureDismissAction(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V

    .line 1545
    if-nez v15, :cond_6

    .line 1546
    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->hideSecondaryAction()V

    .line 1564
    :goto_6
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->setPadding(IIII)V

    .line 1565
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->setIdentifier(Ljava/lang/String;)V

    .line 1567
    return-object v2

    .line 1548
    :cond_6
    iget-object v3, v15, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonText:Ljava/lang/String;

    iget-object v4, v15, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->buttonIcon:Lcom/google/android/finsky/protos/Common$Image;

    new-instance v5, Lcom/google/android/finsky/adapters/CardListAdapter$4;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v5, v0, v1, v15}, Lcom/google/android/finsky/adapters/CardListAdapter$4;-><init>(Lcom/google/android/finsky/adapters/CardListAdapter;Lcom/google/android/finsky/layout/play/WarmWelcomeCard;Lcom/google/android/finsky/protos/DocumentV2$CallToAction;)V

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->showSecondaryButton(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V

    goto :goto_6
.end method

.method public static hasRestoreData(Landroid/os/Bundle;)Z
    .locals 1
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 485
    if-eqz p0, :cond_0

    const-string v0, "CardListAdapter.itemEntriesList"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isShowingPlainHeader()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 813
    iget-boolean v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasPlainHeader:Z

    if-nez v3, :cond_1

    .line 831
    :cond_0
    :goto_0
    return v2

    .line 817
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v1

    .line 818
    .local v1, "listSize":I
    if-lez v1, :cond_2

    .line 821
    iget-boolean v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasPlainHeader:Z

    goto :goto_0

    .line 824
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    move-result-object v0

    .line 825
    .local v0, "footerMode":Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->shouldHidePlainHeaderDuringInitialLoading()Z

    move-result v3

    if-eqz v3, :cond_3

    sget-object v3, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->LOADING:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-eq v0, v3, :cond_0

    .line 828
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->shouldHidePlainHeaderOnEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedListAdapter$FooterMode;

    if-eq v0, v3, :cond_0

    .line 831
    :cond_4
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private restoreDespiteColumnCountChange(Landroid/widget/ListView;Ljava/util/ArrayList;IIII)V
    .locals 25
    .param p1, "list"    # Landroid/widget/ListView;
    .param p3, "oldFirstVisibleRow"    # I
    .param p4, "oldRowPixelOffset"    # I
    .param p5, "oldRowPixelHeight"    # I
    .param p6, "oldLooseItemColCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/ListView;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;",
            ">;IIII)V"
        }
    .end annotation

    .prologue
    .line 537
    .local p2, "restoredItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;>;"
    add-int v18, p5, p4

    .line 538
    .local v18, "oldRowVisiblePixelHeight":I
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, p5

    int-to-float v0, v0

    move/from16 v23, v0

    div-float v22, v22, v23

    const/high16 v23, 0x3f000000    # 0.5f

    cmpl-float v22, v22, v23

    if-lez v22, :cond_0

    const/4 v6, 0x1

    .line 540
    .local v6, "isOldRowSufficientlyVisible":Z
    :goto_0
    if-eqz v6, :cond_1

    const/16 v22, 0x0

    :goto_1
    add-int p3, p3, v22

    .line 546
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getPrependedRowsCount()I

    move-result v22

    move/from16 v0, p3

    move/from16 v1, v22

    if-ge v0, v1, :cond_2

    .line 548
    move/from16 v11, p3

    .line 586
    .local v11, "newFirstVisibleRow":I
    :goto_2
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v12

    .line 587
    .local v12, "newOutput":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_3
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v5, v0, :cond_9

    .line 592
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 596
    move v9, v5

    .line 597
    .local v9, "lastLooseItemRow":I
    const/16 v20, 0x0

    .line 601
    .local v20, "totalDocsFound":I
    move v7, v5

    .local v7, "j":I
    :goto_4
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v7, v0, :cond_6

    .line 602
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    .line 603
    .local v19, "row":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 604
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$100(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v22

    add-int/lit8 v22, v22, 0x1

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v23

    sub-int v22, v22, v23

    add-int v20, v20, v22

    .line 605
    move v9, v7

    .line 601
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 538
    .end local v5    # "i":I
    .end local v6    # "isOldRowSufficientlyVisible":Z
    .end local v7    # "j":I
    .end local v9    # "lastLooseItemRow":I
    .end local v11    # "newFirstVisibleRow":I
    .end local v12    # "newOutput":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;>;"
    .end local v19    # "row":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    .end local v20    # "totalDocsFound":I
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 540
    .restart local v6    # "isOldRowSufficientlyVisible":Z
    :cond_1
    const/16 v22, 0x1

    goto :goto_1

    .line 549
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getPrependedRowsCount()I

    move-result v22

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v23

    add-int v22, v22, v23

    move/from16 v0, p3

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    .line 554
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getPrependedRowsCount()I

    move-result v22

    sub-int v4, p3, v22

    .line 557
    .local v4, "firstVisibleItem":I
    const/4 v15, 0x0

    .line 558
    .local v15, "numLooseRowsBeforeFirstVisibleItem":I
    const/4 v5, 0x0

    .restart local v5    # "i":I
    :goto_5
    if-ge v5, v4, :cond_4

    .line 559
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 560
    add-int/lit8 v15, v15, 0x1

    .line 558
    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 565
    :cond_4
    mul-int v21, p6, v15

    .line 569
    .local v21, "totalLooseItems":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    move/from16 v22, v0

    invoke-static/range {v21 .. v22}, Lcom/google/android/finsky/utils/IntMath;->floor(II)I

    move-result v16

    .line 573
    .local v16, "numNewLooseRows":I
    sub-int v17, v4, v15

    .line 577
    .local v17, "numNonLooseRows":I
    add-int v22, v17, v16

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getPrependedRowsCount()I

    move-result v23

    add-int v11, v22, v23

    .line 578
    .restart local v11    # "newFirstVisibleRow":I
    goto/16 :goto_2

    .line 581
    .end local v4    # "firstVisibleItem":I
    .end local v5    # "i":I
    .end local v11    # "newFirstVisibleRow":I
    .end local v15    # "numLooseRowsBeforeFirstVisibleItem":I
    .end local v16    # "numNewLooseRows":I
    .end local v17    # "numNonLooseRows":I
    .end local v21    # "totalLooseItems":I
    :cond_5
    const v11, 0x7fffffff

    .restart local v11    # "newFirstVisibleRow":I
    goto/16 :goto_2

    .line 611
    .restart local v5    # "i":I
    .restart local v7    # "j":I
    .restart local v9    # "lastLooseItemRow":I
    .restart local v12    # "newOutput":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;>;"
    .restart local v20    # "totalDocsFound":I
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    move/from16 v22, v0

    move/from16 v0, v20

    move/from16 v1, v22

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v13

    .line 612
    .local v13, "newRowCount":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    .line 613
    .local v3, "firstLooseItemIndex":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static/range {v22 .. v22}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$100(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v8

    .line 616
    .local v8, "lastLooseItemIndex":I
    const/4 v14, 0x0

    .local v14, "newRowIndex":I
    :goto_6
    if-ge v14, v13, :cond_7

    .line 618
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    move/from16 v22, v0

    add-int v22, v22, v3

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v22

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 620
    .local v10, "newEndIndex":I
    new-instance v22, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    const/16 v23, 0x1

    const/16 v24, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    move-object/from16 v2, v24

    invoke-direct {v0, v3, v10, v1, v2}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;-><init>(IIZLcom/google/android/finsky/adapters/CardListAdapter$1;)V

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 622
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    move/from16 v22, v0

    add-int v3, v3, v22

    .line 616
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 625
    .end local v10    # "newEndIndex":I
    :cond_7
    move v5, v9

    .line 587
    .end local v3    # "firstLooseItemIndex":I
    .end local v7    # "j":I
    .end local v8    # "lastLooseItemIndex":I
    .end local v9    # "lastLooseItemRow":I
    .end local v13    # "newRowCount":I
    .end local v14    # "newRowIndex":I
    .end local v20    # "totalDocsFound":I
    :goto_7
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_3

    .line 628
    :cond_8
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 631
    :cond_9
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    .line 635
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->notifyDataSetChanged()V

    .line 637
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getCount()I

    move-result v22

    move/from16 v0, v22

    if-ge v11, v0, :cond_a

    .line 638
    :goto_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/widget/ListView;->setSelection(I)V

    .line 639
    return-void

    .line 637
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getCount()I

    move-result v22

    add-int/lit8 v11, v22, -0x1

    goto :goto_8
.end method

.method private syncItemEntries()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 671
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v5

    .line 674
    .local v5, "underlyingCount":I
    const/4 v3, 0x0

    .line 675
    .local v3, "lastItemEntryIndex":I
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 676
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static {v6}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$100(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v6

    add-int/lit8 v3, v6, 0x1

    .line 681
    :cond_0
    move v2, v3

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_a

    .line 684
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6, v2, v10}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(IZ)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 685
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    if-nez v0, :cond_2

    .line 690
    const-string v6, "Loaded null doc, forcing a hard reload of list data."

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 691
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/ContainerList;->resetItems()V

    .line 692
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/ContainerList;->startLoadItems()V

    .line 693
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 751
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    :goto_1
    return-void

    .line 697
    .restart local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasContainerAnnotation()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasAntennaInfo()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasDealOfTheDayInfo()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->hasNextBanner()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isRateCluster()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isRateAndSuggestCluster()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isActionBanner()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isWarmWelcome()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isEmptyContainer()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 701
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-direct {p0, v6, v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->endLastEntry(Ljava/util/List;I)V

    .line 707
    sget-boolean v6, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v6, :cond_6

    .line 709
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isRateCluster()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-static {v6, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->isClusterDismissed(Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/Document;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 681
    :cond_4
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 715
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isRateAndSuggestCluster()Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-static {v6, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestClusterView;->isClusterDismissed(Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/Document;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 722
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->isWarmWelcome()Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/utils/ClientMutationCache;->isDismissedRecommendation(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 728
    :cond_7
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    invoke-direct {v7, v2, v2, v10, v11}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;-><init>(IIZLcom/google/android/finsky/adapters/CardListAdapter$1;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 732
    :cond_8
    const/4 v4, 0x0

    .line 733
    .local v4, "rowReused":Z
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_9

    .line 734
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    .line 735
    .local v1, "existingItem":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mIsLooseItemRow:Z
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$000(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)Z

    move-result v6

    if-eqz v6, :cond_9

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v6

    sub-int v6, v2, v6

    iget v7, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    if-ge v6, v7, :cond_9

    .line 737
    const/4 v4, 0x1

    .line 740
    .end local v1    # "existingItem":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    :cond_9
    if-nez v4, :cond_4

    .line 742
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-direct {p0, v6, v2}, Lcom/google/android/finsky/adapters/CardListAdapter;->endLastEntry(Ljava/util/List;I)V

    .line 743
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v7, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    const/4 v8, -0x1

    const/4 v9, 0x1

    invoke-direct {v7, v2, v8, v9, v11}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;-><init>(IIZLcom/google/android/finsky/adapters/CardListAdapter$1;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 747
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v4    # "rowReused":Z
    :cond_a
    if-lez v5, :cond_1

    .line 749
    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v7

    invoke-direct {p0, v6, v7}, Lcom/google/android/finsky/adapters/CardListAdapter;->endLastEntry(Ljava/util/List;I)V

    goto/16 :goto_1
.end method


# virtual methods
.method protected bindSpinnerData(Lcom/google/android/finsky/layout/play/Identifiable;Landroid/widget/Spinner;Landroid/view/View;)V
    .locals 7
    .param p1, "identifiable"    # Lcom/google/android/finsky/layout/play/Identifiable;
    .param p2, "spinner"    # Landroid/widget/Spinner;
    .param p3, "corpusHeaderStrip"    # Landroid/view/View;

    .prologue
    .line 1763
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/ContainerList;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 1764
    .local v0, "containerDocument":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1, v5}, Lcom/google/android/finsky/layout/play/Identifiable;->setIdentifier(Ljava/lang/String;)V

    .line 1766
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getCorpusSpinnerDrawable(I)I

    move-result v5

    invoke-virtual {p2, v5}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    .line 1768
    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {p3, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 1772
    invoke-virtual {p2}, Landroid/widget/Spinner;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b012c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iget v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardContentPadding:I

    add-int v2, v5, v6

    .line 1774
    .local v2, "horizontalMargin":I
    invoke-virtual {p2}, Landroid/widget/Spinner;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1776
    .local v4, "spinnerLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v2, v4, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1777
    iput v2, v4, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 1778
    invoke-virtual {p2, v4}, Landroid/widget/Spinner;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1780
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getContainerViews()[Lcom/google/android/finsky/protos/Containers$ContainerView;

    move-result-object v1

    .line 1781
    .local v1, "containerViews":[Lcom/google/android/finsky/protos/Containers$ContainerView;
    new-instance v5, Lcom/google/android/finsky/adapters/ContainerViewSpinnerAdapter;

    iget-object v6, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6, v1}, Lcom/google/android/finsky/adapters/ContainerViewSpinnerAdapter;-><init>(Landroid/content/Context;[Lcom/google/android/finsky/protos/Containers$ContainerView;)V

    invoke-virtual {p2, v5}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 1782
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v5, v1

    if-ge v3, v5, :cond_0

    .line 1783
    aget-object v5, v1, v3

    iget-boolean v5, v5, Lcom/google/android/finsky/protos/Containers$ContainerView;->selected:Z

    if-eqz v5, :cond_1

    .line 1784
    invoke-virtual {p2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 1789
    :cond_0
    new-instance v5, Lcom/google/android/finsky/adapters/CardListAdapter$6;

    invoke-direct {v5, p0, p2, v1}, Lcom/google/android/finsky/adapters/CardListAdapter$6;-><init>(Lcom/google/android/finsky/adapters/CardListAdapter;Landroid/widget/Spinner;[Lcom/google/android/finsky/protos/Containers$ContainerView;)V

    invoke-virtual {p2, v5}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1816
    return-void

    .line 1782
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method protected getClusterClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "clusterDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "clusterNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 1598
    invoke-static {p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->hasClickListener(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 788
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getDataRowsCount()I

    move-result v0

    .line 789
    .local v0, "dataRowsCount":I
    if-nez v0, :cond_0

    .line 796
    :goto_0
    return v2

    .line 795
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->hasExtraLeadingSpacer()Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v2, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    add-int v1, v2, v0

    .local v1, "result":I
    move v2, v1

    .line 796
    goto :goto_0
.end method

.method protected getDataRowsCount()I
    .locals 1

    .prologue
    .line 800
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getBaseCount()I

    move-result v0

    return v0
.end method

.method protected getExtraLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 939
    if-nez p1, :cond_0

    .line 940
    const v0, 0x7f04014d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 943
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLeadingExtraSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 944
    return-object p1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1122
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getCardListAdapterViewType(I)I

    move-result v0

    return v0
.end method

.method protected getLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 929
    if-nez p1, :cond_0

    .line 930
    const v0, 0x7f04014d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 933
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLeadingSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 934
    const v0, 0x7f0a0029

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 935
    return-object p1
.end method

.method protected getPlayCardDismissListener()Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .locals 0

    .prologue
    .line 1834
    return-object p0
.end method

.method public getPrependedRowsCount()I
    .locals 1

    .prologue
    .line 809
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getBasePrependedRowsCount()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 843
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getCardListAdapterViewType(I)I

    move-result v2

    .line 846
    .local v2, "itemViewType":I
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->getBasePrependedRowsCount()I

    move-result v3

    sub-int v3, p1, v3

    add-int/lit8 v6, v3, -0x1

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->hasExtraLeadingSpacer()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v4

    :goto_0
    sub-int v0, v6, v3

    .line 848
    .local v0, "adjustedIndex":I
    const/4 v1, 0x0

    .line 849
    .local v1, "entry":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    if-ltz v0, :cond_0

    .line 850
    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "entry":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    check-cast v1, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;

    .line 853
    .restart local v1    # "entry":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    :cond_0
    packed-switch v2, :pswitch_data_0

    .line 903
    :pswitch_0
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getGenericCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    :goto_1
    return-object v3

    .end local v0    # "adjustedIndex":I
    .end local v1    # "entry":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    :cond_1
    move v3, v5

    .line 846
    goto :goto_0

    .line 855
    .restart local v0    # "adjustedIndex":I
    .restart local v1    # "entry":Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;
    :pswitch_1
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 857
    :pswitch_2
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getExtraLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 859
    :pswitch_3
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getErrorFooterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 861
    :pswitch_4
    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getLoadingFooterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 863
    :pswitch_5
    invoke-direct {p0, p2, p3, p1}, Lcom/google/android/finsky/adapters/CardListAdapter;->getQuickLinksRowView(Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 865
    :pswitch_6
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getPlainHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 867
    :pswitch_7
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getBannerHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 869
    :pswitch_8
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getSocialHeaderView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 871
    :pswitch_9
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getMerchBanner(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 873
    :pswitch_a
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getContainerFilterView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 875
    :pswitch_b
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueEndIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$100(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v4

    invoke-direct {p0, v3, v4, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getLooseItemRow(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 878
    :pswitch_c
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getMerchCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 880
    :pswitch_d
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getAddToCirclesCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 882
    :pswitch_e
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getTrustedSourceCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 884
    :pswitch_f
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getActionBannerCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 886
    :pswitch_10
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3, v5}, Lcom/google/android/finsky/adapters/CardListAdapter;->getSingleDocCluster(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 888
    :pswitch_11
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getOrderedCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_1

    .line 890
    :pswitch_12
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getRateCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_1

    .line 892
    :pswitch_13
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getQuickSuggestionsCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_1

    .line 894
    :pswitch_14
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getEmptyCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_1

    .line 896
    :pswitch_15
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getWarmWelcomeCardView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_1

    .line 898
    :pswitch_16
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3}, Lcom/google/android/finsky/adapters/CardListAdapter;->getMyCirclesCluster(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_1

    .line 900
    :pswitch_17
    # getter for: Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->mTrueStartIndex:I
    invoke-static {v1}, Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;->access$200(Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;)I

    move-result v3

    invoke-direct {p0, v3, p2, p3, v4}, Lcom/google/android/finsky/adapters/CardListAdapter;->getSingleDocCluster(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_1

    .line 853
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_9
        :pswitch_c
        :pswitch_10
        :pswitch_b
        :pswitch_6
        :pswitch_a
        :pswitch_11
        :pswitch_d
        :pswitch_e
        :pswitch_12
        :pswitch_13
        :pswitch_f
        :pswitch_14
        :pswitch_7
        :pswitch_8
        :pswitch_15
        :pswitch_16
        :pswitch_0
        :pswitch_17
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 1266
    const/16 v0, 0x18

    return v0
.end method

.method protected hasExtraLeadingSpacer()Z
    .locals 1

    .prologue
    .line 429
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasBannerHeader:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasSocialHeader:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected hasFilters()Z
    .locals 1

    .prologue
    .line 436
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mHasFilters:Z

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 1117
    const/4 v0, 0x0

    return v0
.end method

.method protected isDismissed(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 2
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 1106
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->isDismissable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->isDismissedRecommendation(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDataChanged()V
    .locals 0

    .prologue
    .line 644
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->syncItemEntries()V

    .line 645
    invoke-super {p0}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->onDataChanged()V

    .line 646
    return-void
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 1111
    return-void
.end method

.method public onDismissDocument(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayCardViewBase;)V
    .locals 2
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "card"    # Lcom/google/android/play/layout/PlayCardViewBase;

    .prologue
    .line 1839
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->dismissRecommendation(Ljava/lang/String;)V

    .line 1840
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->notifyDataSetChanged()V

    .line 1841
    return-void
.end method

.method public onMovedToScrapHeap(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 1820
    instance-of v1, p1, Lcom/google/android/finsky/adapters/Recyclable;

    if-eqz v1, :cond_0

    move-object v1, p1

    .line 1821
    check-cast v1, Lcom/google/android/finsky/adapters/Recyclable;

    invoke-interface {v1}, Lcom/google/android/finsky/adapters/Recyclable;->onRecycle()V

    .line 1824
    :cond_0
    instance-of v1, p1, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 1825
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterView;

    .line 1826
    .local v0, "clusterView":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mCardHeap:Lcom/google/android/finsky/layout/play/PlayCardHeap;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/play/PlayCardHeap;->recycle(Lcom/google/android/finsky/layout/play/PlayCardClusterView;)V

    .line 1828
    .end local v0    # "clusterView":Lcom/google/android/finsky/layout/play/PlayCardClusterView;
    :cond_1
    return-void
.end method

.method public onRestoreInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "list"    # Landroid/widget/ListView;
    .param p2, "restoreBundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, -0x1

    .line 490
    const-string v0, "CardListAdapter.itemEntriesList"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 495
    .local v2, "restoredItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/adapters/CardListAdapter$ItemEntry;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 496
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->syncItemEntries()V

    .line 497
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->notifyDataSetChanged()V

    .line 498
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/ContainerList;->startLoadItems()V

    .line 499
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->onRestoreInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V

    .line 519
    :goto_0
    return-void

    .line 503
    :cond_1
    const-string v0, "CardListAdapter.firstVisibleRow"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 504
    .local v3, "firstVisibleRow":I
    const-string v0, "CardListAdapter.rowPixelOffset"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 505
    .local v4, "rowPixelOffset":I
    const-string v0, "CardListAdapter.rowPixelHeight"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 506
    .local v5, "rowPixelHeight":I
    const-string v0, "CardListAdapter.columnCount"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 508
    .local v6, "oldLooseItemColCount":I
    iget v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    if-eq v6, v0, :cond_2

    if-ne v6, v1, :cond_3

    .line 510
    :cond_2
    iput-object v2, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    .line 513
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->notifyDataSetChanged()V

    .line 514
    invoke-virtual {p1, v3, v4}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0

    :cond_3
    move-object v0, p0

    move-object v1, p1

    .line 516
    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/adapters/CardListAdapter;->restoreDespiteColumnCountChange(Landroid/widget/ListView;Ljava/util/ArrayList;IIII)V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "view"    # Landroid/widget/ListView;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 457
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->onSaveInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V

    .line 459
    iget-object v4, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-nez v4, :cond_0

    .line 482
    :goto_0
    return-void

    .line 462
    :cond_0
    invoke-virtual {p1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    .line 464
    .local v0, "firstVisibleRow":I
    const/4 v3, 0x0

    .line 465
    .local v3, "rowPixelOffset":I
    const/4 v2, 0x0

    .line 466
    .local v2, "rowPixelHeight":I
    invoke-virtual {p1}, Landroid/widget/ListView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_1

    .line 467
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 468
    .local v1, "firstVisibleView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    .line 469
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 473
    .end local v1    # "firstVisibleView":Landroid/view/View;
    :cond_1
    const-string v4, "CardListAdapter.firstVisibleRow"

    invoke-virtual {p2, v4, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 475
    const-string v4, "CardListAdapter.rowPixelOffset"

    invoke-virtual {p2, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 477
    const-string v4, "CardListAdapter.rowPixelHeight"

    invoke-virtual {p2, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 479
    const-string v4, "CardListAdapter.columnCount"

    iget v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mLooseItemColCount:I

    invoke-virtual {p2, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 481
    const-string v4, "CardListAdapter.itemEntriesList"

    iget-object v5, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {p2, v4, v5}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method protected shouldHidePlainHeaderDuringInitialLoading()Z
    .locals 1

    .prologue
    .line 444
    const/4 v0, 0x0

    return v0
.end method

.method protected shouldHidePlainHeaderOnEmpty()Z
    .locals 1

    .prologue
    .line 452
    const/4 v0, 0x0

    return v0
.end method

.method public updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/ContainerList",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 650
    .local p1, "containerList":Lcom/google/android/finsky/api/model/ContainerList;, "Lcom/google/android/finsky/api/model/ContainerList<*>;"
    invoke-super {p0, p1}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V

    .line 651
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 652
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->syncItemEntries()V

    .line 653
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/CardListAdapter;->notifyDataSetChanged()V

    .line 654
    return-void
.end method
