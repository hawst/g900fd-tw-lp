.class public Lcom/google/android/finsky/layout/DecoratedTextView;
.super Lcom/google/android/play/layout/PlayTextView;
.source "DecoratedTextView.java"

# interfaces
.implements Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/play/layout/PlayTextView;-><init>(Landroid/content/Context;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method public loadDecoration(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/protos/Common$Image;I)V
    .locals 4
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "image"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p3, "imageDimension"    # I

    .prologue
    .line 25
    iget-boolean v3, p2, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v3, :cond_1

    move v2, p3

    .line 26
    .local v2, "requestSize":I
    :goto_0
    iget-object v3, p2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    invoke-virtual {p1, v3, v2, v2, p0}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v0

    .line 28
    .local v0, "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    invoke-virtual {v0}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 29
    .local v1, "loadedBitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    .line 30
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/DecoratedTextView;->setDecorationBitmap(Landroid/graphics/Bitmap;)V

    .line 32
    :cond_0
    return-void

    .line 25
    .end local v0    # "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .end local v1    # "loadedBitmap":Landroid/graphics/Bitmap;
    .end local v2    # "requestSize":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 1
    .param p1, "bitmapContainer"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    .line 42
    invoke-virtual {p1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 44
    .local v0, "response":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 49
    :goto_0
    return-void

    .line 48
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setDecorationBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 15
    check-cast p1, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/DecoratedTextView;->onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    return-void
.end method
