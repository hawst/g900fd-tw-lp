.class public Lcom/google/android/finsky/activities/AppActionAnalyzer;
.super Ljava/lang/Object;
.source "AppActionAnalyzer.java"


# instance fields
.field public certificateHashes:[Ljava/lang/String;

.field public installedVersion:I

.field public isActiveDeviceAdmin:Z

.field public isContinueLaunch:Z

.field public isDisabled:Z

.field public isDisabledByUser:Z

.field public isInstalled:Z

.field public isInstalledOwnedPackage:Z

.field public isInstalledSystemApp:Z

.field public isLaunchable:Z

.field public isRefundable:Z

.field public isUninstallable:Z

.field public isUpdatedSystemApp:Z

.field public refundAccount:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p3, "libraries"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    .line 37
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalledOwnedPackage:Z

    .line 38
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalledSystemApp:Z

    .line 39
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isUpdatedSystemApp:Z

    .line 40
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isUninstallable:Z

    .line 41
    const/4 v7, -0x1

    iput v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->installedVersion:I

    .line 42
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isActiveDeviceAdmin:Z

    .line 43
    sget-object v7, Lcom/google/android/finsky/library/LibraryAppEntry;->ANY_CERTIFICATE_HASHES:[Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->certificateHashes:[Ljava/lang/String;

    .line 46
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isRefundable:Z

    .line 47
    iput-object v3, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->refundAccount:Ljava/lang/String;

    .line 50
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isLaunchable:Z

    .line 51
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isContinueLaunch:Z

    .line 52
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabled:Z

    .line 53
    iput-boolean v9, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabledByUser:Z

    .line 57
    invoke-virtual {p2, p1}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v1

    .line 60
    .local v1, "appState":Lcom/google/android/finsky/appstate/AppStates$AppState;
    if-eqz v1, :cond_1

    iget-object v7, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    if-eqz v7, :cond_1

    .line 62
    iput-boolean v8, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    .line 64
    iget-object v6, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    .line 65
    .local v6, "packageManagerState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    iget v7, v6, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    iput v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->installedVersion:I

    .line 67
    iget-boolean v7, v6, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    iput-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalledSystemApp:Z

    .line 68
    iget-boolean v7, v6, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isUpdatedSystemApp:Z

    iput-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isUpdatedSystemApp:Z

    .line 69
    iget-boolean v7, v6, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isActiveDeviceAdmin:Z

    iput-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isActiveDeviceAdmin:Z

    .line 70
    iget-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isActiveDeviceAdmin:Z

    if-nez v7, :cond_5

    iget-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalledSystemApp:Z

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isUpdatedSystemApp:Z

    if-eqz v7, :cond_5

    :cond_0
    move v7, v8

    :goto_0
    iput-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isUninstallable:Z

    .line 72
    invoke-virtual {p2}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v7

    invoke-interface {v7, p1}, Lcom/google/android/finsky/appstate/PackageStateRepository;->canLaunch(Ljava/lang/String;)Z

    move-result v2

    .line 73
    .local v2, "canLaunch":Z
    iget-boolean v7, v6, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabled:Z

    iput-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabled:Z

    .line 74
    iget-boolean v7, v6, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isDisabledByUser:Z

    iput-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabledByUser:Z

    .line 75
    if-eqz v2, :cond_6

    iget-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isDisabled:Z

    if-nez v7, :cond_6

    move v7, v8

    :goto_1
    iput-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isLaunchable:Z

    .line 80
    .end local v2    # "canLaunch":Z
    .end local v6    # "packageManagerState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_1
    iget-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    if-eqz v7, :cond_2

    .line 81
    iget-object v7, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->packageManagerState:Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    iget-object v7, v7, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->certificateHashes:[Ljava/lang/String;

    iput-object v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->certificateHashes:[Ljava/lang/String;

    .line 83
    :cond_2
    iget-object v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->certificateHashes:[Ljava/lang/String;

    invoke-virtual {p3, p1, v7}, Lcom/google/android/finsky/library/Libraries;->getAppEntries(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 85
    .local v4, "libraryEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryAppEntry;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_7

    move v5, v8

    .line 86
    .local v5, "ownedByAnyAccount":Z
    :goto_2
    iget-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    if-eqz v7, :cond_8

    if-eqz v5, :cond_8

    move v7, v8

    :goto_3
    iput-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalledOwnedPackage:Z

    .line 87
    if-nez v1, :cond_9

    .line 88
    .local v3, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :goto_4
    invoke-static {v3, v4}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->getRefundAccount(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/util/List;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->refundAccount:Ljava/lang/String;

    .line 89
    iget-object v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->refundAccount:Ljava/lang/String;

    if-eqz v7, :cond_a

    move v7, v8

    :goto_5
    iput-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isRefundable:Z

    .line 92
    iget-boolean v7, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    if-eqz v7, :cond_3

    if-nez v5, :cond_3

    .line 93
    invoke-virtual {p3, p1}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 94
    .local v0, "anyCertificateOwners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    .line 95
    const-string v7, "%s is installed but certificate mistmatch"

    new-array v10, v8, [Ljava/lang/Object;

    aput-object p1, v10, v9

    invoke-static {v7, v10}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    .end local v0    # "anyCertificateOwners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    :cond_3
    if-eqz v1, :cond_4

    iget-object v7, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    if-eqz v7, :cond_4

    .line 100
    iget-object v7, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v7}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getContinueUrl()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_b

    :goto_6
    iput-boolean v8, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isContinueLaunch:Z

    .line 102
    :cond_4
    return-void

    .end local v3    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .end local v4    # "libraryEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryAppEntry;>;"
    .end local v5    # "ownedByAnyAccount":Z
    .restart local v6    # "packageManagerState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_5
    move v7, v9

    .line 70
    goto :goto_0

    .restart local v2    # "canLaunch":Z
    :cond_6
    move v7, v9

    .line 75
    goto :goto_1

    .end local v2    # "canLaunch":Z
    .end local v6    # "packageManagerState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    .restart local v4    # "libraryEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryAppEntry;>;"
    :cond_7
    move v5, v9

    .line 85
    goto :goto_2

    .restart local v5    # "ownedByAnyAccount":Z
    :cond_8
    move v7, v9

    .line 86
    goto :goto_3

    .line 87
    :cond_9
    iget-object v3, v1, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    goto :goto_4

    .restart local v3    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_a
    move v7, v9

    .line 89
    goto :goto_5

    :cond_b
    move v8, v9

    .line 100
    goto :goto_6
.end method

.method public static canRemoveFromLibrary(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 8
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 285
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v6

    if-eq v6, v5, :cond_1

    .line 286
    const-string v5, "Method invalid for non-ANDROID_APP docs."

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 308
    :cond_0
    :goto_0
    return v4

    .line 289
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v3

    .line 290
    .local v3, "libraries":Lcom/google/android/finsky/library/Libraries;
    new-instance v0, Lcom/google/android/finsky/activities/AppActionAnalyzer;

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v7

    invoke-direct {v0, v6, v7, v3}, Lcom/google/android/finsky/activities/AppActionAnalyzer;-><init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V

    .line 292
    .local v0, "analyzer":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    iget-boolean v6, v0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    if-nez v6, :cond_0

    .line 295
    iget-boolean v6, v0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isRefundable:Z

    if-nez v6, :cond_0

    .line 298
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/google/android/finsky/utils/DocUtils;->hasAutoRenewingSubscriptions(Lcom/google/android/finsky/library/Libraries;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 301
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    .line 302
    .local v1, "installer":Lcom/google/android/finsky/receivers/Installer;
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-interface {v1, v6}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v2

    .line 303
    .local v2, "installerState":Lcom/google/android/finsky/receivers/Installer$InstallerState;
    sget-object v6, Lcom/google/android/finsky/receivers/Installer$InstallerState;->NOT_TRACKED:Lcom/google/android/finsky/receivers/Installer$InstallerState;

    if-ne v2, v6, :cond_0

    move v4, v5

    .line 308
    goto :goto_0
.end method

.method private static findAccountInOwners(Ljava/lang/String;Ljava/util/List;)Z
    .locals 2
    .param p0, "accountName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/library/LibraryAppEntry;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 196
    .local p1, "libraryEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryAppEntry;>;"
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 197
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 198
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/library/LibraryAppEntry;

    invoke-virtual {v1}, Lcom/google/android/finsky/library/LibraryAppEntry;->getAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    const/4 v1, 0x1

    .line 203
    .end local v0    # "i":I
    :goto_1
    return v1

    .line 197
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    .end local v0    # "i":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected static findCertificateMatch([Ljava/lang/String;[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;)Z
    .locals 10
    .param p0, "installedHashes"    # [Ljava/lang/String;
    .param p1, "documentCertificateSets"    # [Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    .prologue
    const/4 v7, 0x0

    .line 364
    array-length v8, p1

    if-eqz v8, :cond_0

    array-length v8, p0

    if-nez v8, :cond_1

    .line 383
    :cond_0
    :goto_0
    return v7

    .line 367
    :cond_1
    move-object v0, p1

    .local v0, "arr$":[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_1
    if-ge v5, v6, :cond_0

    aget-object v1, v0, v5

    .line 368
    .local v1, "documentCertificateSet":Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    iget-object v8, v1, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    array-length v8, v8

    array-length v9, p0

    if-eq v8, v9, :cond_3

    .line 367
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 371
    :cond_3
    const/4 v3, 0x1

    .line 372
    .local v3, "found":Z
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    iget-object v8, v1, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    array-length v8, v8

    if-ge v4, v8, :cond_4

    .line 373
    iget-object v8, v1, Lcom/google/android/finsky/protos/DocDetails$CertificateSet;->certificateHash:[Ljava/lang/String;

    aget-object v2, v8, v4

    .line 374
    .local v2, "documentHash":Ljava/lang/String;
    aget-object v8, p0, v4

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 375
    const/4 v3, 0x0

    .line 379
    .end local v2    # "documentHash":Ljava/lang/String;
    :cond_4
    if-eqz v3, :cond_2

    .line 380
    const/4 v7, 0x1

    goto :goto_0

    .line 372
    .restart local v2    # "documentHash":Ljava/lang/String;
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method public static getAppDetailsAccount(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)Ljava/lang/String;
    .locals 9
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "currentAccountName"    # Ljava/lang/String;
    .param p2, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p3, "libraries"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    const/4 v7, 0x0

    .line 152
    invoke-virtual {p2}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v8

    invoke-interface {v8, p0}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v3

    .line 154
    .local v3, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    invoke-virtual {p2}, Lcom/google/android/finsky/appstate/AppStates;->getPackageStateRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v8

    invoke-interface {v8, p0}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v6

    .line 155
    .local v6, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    invoke-static {v6}, Lcom/google/android/finsky/appstate/AppStates;->getCertificateHashes(Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;)[Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, "certificateHashes":[Ljava/lang/String;
    invoke-virtual {p3, p0, v1}, Lcom/google/android/finsky/library/Libraries;->getAppEntries(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 161
    .local v5, "libraryEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryAppEntry;>;"
    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_1

    const/4 v4, 0x1

    .line 164
    .local v4, "isInstalledAndOwnedPackage":Z
    :goto_0
    if-eqz v4, :cond_2

    if-eqz v3, :cond_2

    .line 165
    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountForUpdate()Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "accountForUpdate":Ljava/lang/String;
    invoke-static {v0, v5}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->findAccountInOwners(Ljava/lang/String;Ljava/util/List;)Z

    move-result v8

    if-eqz v8, :cond_2

    move-object p1, v0

    .line 190
    .end local v0    # "accountForUpdate":Ljava/lang/String;
    .end local p1    # "currentAccountName":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object p1

    .end local v4    # "isInstalledAndOwnedPackage":Z
    .restart local p1    # "currentAccountName":Ljava/lang/String;
    :cond_1
    move v4, v7

    .line 161
    goto :goto_0

    .line 172
    .restart local v4    # "isInstalledAndOwnedPackage":Z
    :cond_2
    if-eqz v4, :cond_3

    if-eqz v3, :cond_3

    .line 173
    invoke-virtual {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountName()Ljava/lang/String;

    move-result-object v2

    .line 174
    .local v2, "installerAccount":Ljava/lang/String;
    invoke-static {v2, v5}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->findAccountInOwners(Ljava/lang/String;Ljava/util/List;)Z

    move-result v8

    if-eqz v8, :cond_3

    move-object p1, v2

    .line 175
    goto :goto_1

    .line 180
    .end local v2    # "installerAccount":Ljava/lang/String;
    :cond_3
    invoke-static {p1, v5}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->findAccountInOwners(Ljava/lang/String;Ljava/util/List;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 185
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_0

    .line 186
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/library/LibraryAppEntry;

    invoke-virtual {v7}, Lcom/google/android/finsky/library/LibraryAppEntry;->getAccountName()Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method public static getInstallAccount(Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)Landroid/accounts/Account;
    .locals 6
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "preferredAccount"    # Landroid/accounts/Account;
    .param p2, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p3, "libraries"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 117
    invoke-virtual {p3, p0}, Lcom/google/android/finsky/library/Libraries;->getAppOwners(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 118
    .local v3, "owners":Ljava/util/List;, "Ljava/util/List<Landroid/accounts/Account;>;"
    invoke-interface {v3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 132
    .end local p1    # "preferredAccount":Landroid/accounts/Account;
    :cond_0
    :goto_0
    return-object p1

    .line 121
    .restart local p1    # "preferredAccount":Landroid/accounts/Account;
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v4

    invoke-interface {v4, p0}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v1

    .line 122
    .local v1, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    if-eqz v1, :cond_3

    .line 123
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    .line 124
    .local v2, "owner":Landroid/accounts/Account;
    iget-object v4, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getAccountName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object p1, v2

    .line 125
    goto :goto_0

    .line 129
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "owner":Landroid/accounts/Account;
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 130
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/accounts/Account;

    move-object p1, v4

    goto :goto_0
.end method

.method private static getRefundAccount(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/util/List;)Ljava/lang/String;
    .locals 2
    .param p0, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/library/LibraryAppEntry;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "libraryEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryAppEntry;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->internalGetRefundAccount(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/util/List;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static internalGetRefundAccount(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Ljava/util/List;J)Ljava/lang/String;
    .locals 10
    .param p0, "installerData"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .param p2, "nowMs"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/library/LibraryAppEntry;",
            ">;J)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 214
    .local p1, "libraryEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryAppEntry;>;"
    const-wide/16 v2, 0x0

    .line 215
    .local v2, "firstDownloadMs":J
    if-eqz p0, :cond_0

    .line 216
    invoke-virtual {p0}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getFirstDownloadMs()J

    move-result-wide v2

    .line 218
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 219
    .local v4, "libraryEntryCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_3

    .line 220
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/LibraryAppEntry;

    .line 222
    .local v0, "appEntry":Lcom/google/android/finsky/library/LibraryAppEntry;
    iget-wide v6, v0, Lcom/google/android/finsky/library/LibraryAppEntry;->refundPreDeliveryEndtimeMs:J

    .line 223
    .local v6, "refundEndtimeMs":J
    const-wide/16 v8, 0x0

    cmp-long v5, v2, v8

    if-eqz v5, :cond_1

    .line 224
    iget-wide v8, v0, Lcom/google/android/finsky/library/LibraryAppEntry;->refundPostDeliveryWindowMs:J

    add-long/2addr v8, v2

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 227
    :cond_1
    cmp-long v5, v6, p2

    if-ltz v5, :cond_2

    .line 228
    invoke-virtual {v0}, Lcom/google/android/finsky/library/LibraryAppEntry;->getAccountName()Ljava/lang/String;

    move-result-object v5

    .line 231
    .end local v0    # "appEntry":Lcom/google/android/finsky/library/LibraryAppEntry;
    .end local v6    # "refundEndtimeMs":J
    :goto_1
    return-object v5

    .line 219
    .restart local v0    # "appEntry":Lcom/google/android/finsky/library/LibraryAppEntry;
    .restart local v6    # "refundEndtimeMs":J
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    .end local v0    # "appEntry":Lcom/google/android/finsky/library/LibraryAppEntry;
    .end local v6    # "refundEndtimeMs":J
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method


# virtual methods
.method public hasConversionUpdateAvailable(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 5
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 339
    iget-boolean v3, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalledSystemApp:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalledOwnedPackage:Z

    if-eqz v3, :cond_1

    .line 353
    :cond_0
    :goto_0
    return v2

    .line 343
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v3

    iget-object v0, v3, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->certificateSet:[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;

    .line 344
    .local v0, "documentCertificateSets":[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;
    iget-object v3, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->certificateHashes:[Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->findCertificateMatch([Ljava/lang/String;[Lcom/google/android/finsky/protos/DocDetails$CertificateSet;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 348
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionCode:Z

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v3

    iget v3, v3, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    iget v4, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->installedVersion:I

    if-le v3, v4, :cond_0

    .line 353
    invoke-virtual {p1, v1}, Lcom/google/android/finsky/api/model/Document;->needsCheckoutFlow(I)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public hasUpdateAvailable(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 2
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalledOwnedPackage:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalledSystemApp:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->hasVersionCode:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    iget v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    iget v1, p0, Lcom/google/android/finsky/activities/AppActionAnalyzer;->installedVersion:I

    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
