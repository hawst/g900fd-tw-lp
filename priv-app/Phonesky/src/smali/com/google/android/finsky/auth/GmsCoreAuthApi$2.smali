.class Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;
.super Landroid/os/AsyncTask;
.source "GmsCoreAuthApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/auth/GmsCoreAuthApi;->validateUserPin(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/auth/GmsCoreAuthApi;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$gaiaPin:Ljava/lang/String;

.field final synthetic val$listener:Lcom/google/android/finsky/auth/AuthResponseListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/auth/GmsCoreAuthApi;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->this$0:Lcom/google/android/finsky/auth/GmsCoreAuthApi;

    iput-object p2, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->val$accountName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->val$gaiaPin:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->val$listener:Lcom/google/android/finsky/auth/AuthResponseListener;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;
    .locals 5
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    .line 92
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    .line 93
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;-><init>(Landroid/content/Context;)V

    .line 95
    .local v0, "authClient":Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataClient;
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinRequest;

    iget-object v3, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->val$accountName:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->val$gaiaPin:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataClient;->verifyPin(Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 89
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->this$0:Lcom/google/android/finsky/auth/GmsCoreAuthApi;

    iget-object v1, p0, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->val$listener:Lcom/google/android/finsky/auth/AuthResponseListener;

    # invokes: Lcom/google/android/finsky/auth/GmsCoreAuthApi;->handleVerifyPinResponse(Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;)V
    invoke-static {v0, v1, p1}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->access$100(Lcom/google/android/finsky/auth/GmsCoreAuthApi;Lcom/google/android/finsky/auth/AuthResponseListener;Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;)V

    .line 102
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 89
    check-cast p1, Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/auth/GmsCoreAuthApi$2;->onPostExecute(Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;)V

    return-void
.end method
