.class public Lcom/google/android/finsky/providers/RecentSuggestionsProvider;
.super Landroid/content/SearchRecentSuggestionsProvider;
.source "RecentSuggestionsProvider.java"


# static fields
.field private static sCurrentBackendId:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/content/SearchRecentSuggestionsProvider;-><init>()V

    .line 34
    const-string v0, "com.google.android.finsky.RecentSuggestionsProvider"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->setupSuggestions(Ljava/lang/String;I)V

    .line 35
    return-void
.end method

.method public static getDocIdFromNavigationalQuery(Landroid/content/Intent;)Ljava/lang/String;
    .locals 3
    .param p0, "fromIntent"    # Landroid/content/Intent;

    .prologue
    .line 74
    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 75
    .local v0, "data":Landroid/net/Uri;
    if-nez v0, :cond_0

    .line 76
    const/4 v1, 0x0

    .line 79
    :goto_0
    return-object v1

    .line 78
    :cond_0
    const-string v2, "id"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "docId":Ljava/lang/String;
    goto :goto_0
.end method

.method public static sendSuggestionClickedLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "suggestionData"    # Ljava/lang/String;
    .param p1, "suggestedQuery"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 53
    const/16 v5, 0xa

    :try_start_0
    invoke-static {p0, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v4

    .line 54
    .local v4, "reportBytes":[B
    invoke-static {v4}, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->parseFrom([B)Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    move-result-object v3

    .line 55
    .local v3, "report":Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;
    iput-object p1, v3, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestedQuery:Ljava/lang/String;

    .line 56
    const/4 v5, 0x1

    iput-boolean v5, v3, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestedQuery:Z

    .line 57
    new-instance v5, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/16 v6, 0x1ff

    invoke-direct {v5, v6}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v5, v3}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setSearchSuggestionReport(Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 60
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_1

    .line 66
    .end local v0    # "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    .end local v3    # "report":Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;
    .end local v4    # "reportBytes":[B
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v1

    .line 62
    .local v1, "iae":Ljava/lang/IllegalArgumentException;
    const-string v5, "Couldn\'t decode bytes from suggestion"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v1, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    .end local v1    # "iae":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 64
    .local v2, "ipbne":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    const-string v5, "Couldn\'t reconstitute proto from suggestion"

    new-array v6, v7, [Ljava/lang/Object;

    aput-object v2, v6, v8

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static setCurrentBackendId(I)V
    .locals 0
    .param p0, "backendId"    # I

    .prologue
    .line 41
    sput p0, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->sCurrentBackendId:I

    .line 42
    return-void
.end method


# virtual methods
.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 93
    if-eqz p4, :cond_0

    array-length v2, p4

    if-nez v2, :cond_1

    .line 94
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SelectionArgs must be provided for the Uri: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 98
    :cond_1
    const/4 v2, 0x0

    aget-object v2, p4, v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "query":Ljava/lang/String;
    new-instance v4, Lcom/google/android/finsky/providers/SuggestionHandler;

    invoke-virtual {p0}, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v4, v1, v2}, Lcom/google/android/finsky/providers/SuggestionHandler;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 100
    .local v4, "suggestionHandler":Lcom/google/android/finsky/providers/SuggestionHandler;
    new-instance v0, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;

    invoke-super/range {p0 .. p5}, Landroid/content/SearchRecentSuggestionsProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/providers/RecentSearchSuggestionFetcher;-><init>(Ljava/lang/String;Landroid/database/Cursor;Landroid/content/Context;Lcom/google/android/finsky/providers/SuggestionHandler;Lcom/google/android/finsky/experiments/FinskyExperiments;)V

    .line 106
    .local v0, "recentSearchSuggestionFetcher":Lcom/google/android/finsky/providers/SuggestionFetcher;
    sget v2, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->sCurrentBackendId:I

    packed-switch v2, :pswitch_data_0

    .line 120
    :cond_2
    new-instance v6, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;

    invoke-virtual {p0}, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->sCurrentBackendId:I

    invoke-direct {v6, v2, v3, v1, v4}, Lcom/google/android/finsky/providers/SearchSuggestionFetcher;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/google/android/finsky/providers/SuggestionHandler;)V

    .line 125
    .local v6, "searchSuggestionFetcher":Lcom/google/android/finsky/providers/SuggestionFetcher;
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/finsky/providers/SuggestionFetcher;->gatherSuggestions()V

    .line 126
    invoke-virtual {v6}, Lcom/google/android/finsky/providers/SuggestionFetcher;->gatherSuggestions()V

    .line 127
    invoke-virtual {v4}, Lcom/google/android/finsky/providers/SuggestionHandler;->getSuggestions()Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    .line 109
    .end local v6    # "searchSuggestionFetcher":Lcom/google/android/finsky/providers/SuggestionFetcher;
    :pswitch_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v2

    const-string v3, "cl:search.use_dfe_for_music_search_suggestions"

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 111
    new-instance v6, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;

    invoke-virtual {p0}, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v6, v1, v2, v4}, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/providers/SuggestionHandler;)V

    .line 113
    .restart local v6    # "searchSuggestionFetcher":Lcom/google/android/finsky/providers/SuggestionFetcher;
    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
