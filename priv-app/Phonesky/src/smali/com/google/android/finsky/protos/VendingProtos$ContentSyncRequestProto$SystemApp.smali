.class public final Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SystemApp"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;


# instance fields
.field public certificateHash:[Ljava/lang/String;

.field public hasPackageName:Z

.field public hasVersionCode:Z

.field public packageName:Ljava/lang/String;

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->clear()Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    .line 266
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    .locals 2

    .prologue
    .line 242
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    if-nez v0, :cond_1

    .line 243
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 245
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    if-nez v0, :cond_0

    .line 246
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    sput-object v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    .line 248
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    return-object v0

    .line 248
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 269
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->packageName:Ljava/lang/String;

    .line 270
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasPackageName:Z

    .line 271
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->versionCode:I

    .line 272
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasVersionCode:Z

    .line 273
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    .line 274
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->cachedSize:I

    .line 275
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 300
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 301
    .local v4, "size":I
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasPackageName:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->packageName:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 302
    :cond_0
    const/16 v5, 0xb

    iget-object v6, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->packageName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 305
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasVersionCode:Z

    if-nez v5, :cond_2

    iget v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->versionCode:I

    if-eqz v5, :cond_3

    .line 306
    :cond_2
    const/16 v5, 0xc

    iget v6, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->versionCode:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 309
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_6

    .line 310
    const/4 v0, 0x0

    .line 311
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 312
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_5

    .line 313
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 314
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 315
    add-int/lit8 v0, v0, 0x1

    .line 316
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 312
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 320
    .end local v2    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v4, v1

    .line 321
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 323
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_6
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 331
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 332
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 336
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 337
    :sswitch_0
    return-object p0

    .line 342
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->packageName:Ljava/lang/String;

    .line 343
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasPackageName:Z

    goto :goto_0

    .line 347
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->versionCode:I

    .line 348
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasVersionCode:Z

    goto :goto_0

    .line 352
    :sswitch_3
    const/16 v5, 0x6a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 354
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 355
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 356
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 357
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 359
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 360
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 361
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 359
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 354
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 364
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 365
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    goto :goto_0

    .line 332
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0x60 -> :sswitch_2
        0x6a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 236
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 281
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasPackageName:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->packageName:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 282
    :cond_0
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 284
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->hasVersionCode:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->versionCode:I

    if-eqz v2, :cond_3

    .line 285
    :cond_2
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->versionCode:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 287
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 288
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 289
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ContentSyncRequestProto$SystemApp;->certificateHash:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 290
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 291
    const/16 v2, 0xd

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 288
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 295
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 296
    return-void
.end method
