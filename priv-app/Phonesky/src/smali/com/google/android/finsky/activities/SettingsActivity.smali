.class public Lcom/google/android/finsky/activities/SettingsActivity;
.super Landroid/preference/PreferenceActivity;
.source "SettingsActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/SettingsActivity$4;,
        Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;,
        Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    }
.end annotation


# static fields
.field private static sSelfUpdateChecked:Ljava/lang/Boolean;


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mIsResumed:Z

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mPageNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/finsky/activities/SettingsActivity;->sSelfUpdateChecked:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 139
    new-instance v0, Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/FakeNavigationManager;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/finsky/activities/SettingsActivity;->sSelfUpdateChecked:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Ljava/lang/Boolean;

    .prologue
    .line 54
    sput-object p0, Lcom/google/android/finsky/activities/SettingsActivity;->sSelfUpdateChecked:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/SettingsActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/SettingsActivity;->showSelfUpdateCheckResult(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/SettingsActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/SettingsActivity;Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsActivity;
    .param p1, "x1"    # Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/activities/SettingsActivity;->handleReauthSettingsResponse(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;II)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/activities/SettingsActivity;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/activities/SettingsActivity;->getReauthSettingsOverNetwork(II)V

    return-void
.end method

.method private configureAboutSection(Landroid/preference/PreferenceScreen;)V
    .locals 6
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    .line 600
    const-string v3, "build-version"

    invoke-virtual {p1, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 601
    .local v0, "buildVersion":Landroid/preference/Preference;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v2

    .line 602
    .local v2, "repository":Lcom/google/android/finsky/appstate/PackageStateRepository;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/finsky/appstate/PackageStateRepository;->getVersionName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 603
    .local v1, "marketVersionName":Ljava/lang/String;
    const v3, 0x7f0c0308

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/finsky/activities/SettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 604
    return-void
.end method

.method private configureAutoAddShortcuts(Landroid/preference/PreferenceScreen;)V
    .locals 2
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    .line 676
    const-string v1, "auto-add-shortcuts"

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 678
    .local v0, "autoAddShortcuts":Landroid/preference/CheckBoxPreference;
    sget-object v1, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_ADD_SHORTCUTS:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 679
    return-void
.end method

.method private configureAutoUpdateMode(Landroid/preference/PreferenceScreen;)V
    .locals 7
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 641
    const-string v4, "auto-update-mode"

    invoke-virtual {p1, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/activities/SettingsListPreference;

    .line 645
    .local v2, "listPreference":Lcom/google/android/finsky/activities/SettingsListPreference;
    invoke-static {}, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->values()[Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    move-result-object v0

    .line 646
    .local v0, "entries":[Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/installer/InstallPolicies;->hasMobileNetwork()Z

    move-result v3

    .line 647
    .local v3, "showWifiOption":Z
    if-nez v3, :cond_0

    .line 648
    new-array v1, v6, [Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    .line 649
    .local v1, "entriesWithNoWifi":[Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    invoke-static {v0, v5, v1, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 650
    move-object v0, v1

    .line 652
    .end local v1    # "entriesWithNoWifi":[Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    :cond_0
    invoke-virtual {v2, v0}, Lcom/google/android/finsky/activities/SettingsListPreference;->setEntriesAndValues([Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;)V

    .line 653
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getCurrentAutoUpdateEntry()Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/activities/SettingsListPreference;->setValueAndUpdateSummary(Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;)V

    .line 654
    return-void
.end method

.method private configureUpdateNotifications(Landroid/preference/PreferenceScreen;)V
    .locals 2
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    .line 633
    const-string v1, "update-notifications"

    invoke-virtual {p1, v1}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 635
    .local v0, "updateNotifications":Landroid/preference/CheckBoxPreference;
    sget-object v1, Lcom/google/android/finsky/utils/VendingPreferences;->NOTIFY_UPDATES:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 636
    return-void
.end method

.method private configureUserControlsSection(Landroid/preference/PreferenceScreen;)V
    .locals 4
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;

    .prologue
    .line 607
    sget-object v3, Lcom/google/android/finsky/config/G;->vendingHideContentRating:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 608
    .local v1, "hideContentRating":Z
    if-eqz v1, :cond_0

    .line 609
    const-string v3, "content-level"

    invoke-virtual {p1, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 610
    .local v0, "contentLevel":Landroid/preference/Preference;
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 613
    .end local v0    # "contentLevel":Landroid/preference/Preference;
    :cond_0
    const-string v3, "purchase-auth"

    invoke-virtual {p1, v3}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/activities/SettingsListPreference;

    .line 616
    .local v2, "listPreference":Lcom/google/android/finsky/activities/SettingsListPreference;
    const v3, 0x7f0c02c6

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SettingsListPreference;->setTitle(I)V

    .line 617
    invoke-static {}, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->values()[Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SettingsListPreference;->setEntriesAndValues([Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;)V

    .line 618
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getCurrentPurchaseAuthEntry()Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SettingsListPreference;->setValueAndUpdateSummary(Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;)V

    .line 619
    return-void
.end method

.method private doSelfUpdateCheck()V
    .locals 6

    .prologue
    .line 253
    iget-object v2, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v3, 0x11a

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mPageNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 255
    sget-object v2, Lcom/google/android/finsky/config/G;->userCheckForSelfUpdateEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 256
    sget-object v2, Lcom/google/android/finsky/activities/SettingsActivity;->sSelfUpdateChecked:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 258
    sget-object v2, Lcom/google/android/finsky/activities/SettingsActivity;->sSelfUpdateChecked:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/SettingsActivity;->showSelfUpdateCheckResult(Z)V

    .line 283
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    .line 261
    .local v1, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-interface {v1}, Lcom/google/android/finsky/api/DfeApi;->invalidateSelfUpdateCache()V

    .line 262
    invoke-interface {v1}, Lcom/google/android/finsky/api/DfeApi;->getAccountName()Ljava/lang/String;

    move-result-object v0

    .line 264
    .local v0, "accountName":Ljava/lang/String;
    new-instance v2, Lcom/google/android/finsky/activities/SettingsActivity$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/finsky/activities/SettingsActivity$1;-><init>(Lcom/google/android/finsky/activities/SettingsActivity;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->getSelfUpdate(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    goto :goto_0
.end method

.method private getCurrentAutoUpdateEntry()Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    .locals 4

    .prologue
    .line 660
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/installer/InstallPolicies;->hasMobileNetwork()Z

    move-result v1

    .line 661
    .local v1, "showWifiOption":Z
    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 662
    .local v0, "enabled":Z
    sget-object v3, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 665
    .local v2, "wifiOnly":Z
    if-eqz v0, :cond_1

    .line 666
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 667
    sget-object v3, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->AUTO_UPDATE_WIFI:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    .line 672
    :goto_0
    return-object v3

    .line 669
    :cond_0
    sget-object v3, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->AUTO_UPDATE_ALWAYS:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    goto :goto_0

    .line 672
    :cond_1
    sget-object v3, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->AUTO_UPDATE_NEVER:Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    goto :goto_0
.end method

.method private getCurrentPurchaseAuthEntry()Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;
    .locals 8

    .prologue
    .line 622
    iget-object v5, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/finsky/config/PurchaseAuth;->getPurchaseAuthType(Ljava/lang/String;)I

    move-result v4

    .line 623
    .local v4, "purchaseAuth":I
    invoke-static {}, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->values()[Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 624
    .local v1, "entry":Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;
    # getter for: Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->mPurchaseAuth:I
    invoke-static {v1}, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->access$200(Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;)I

    move-result v5

    if-ne v5, v4, :cond_0

    .line 625
    return-object v1

    .line 623
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 628
    .end local v1    # "entry":Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;
    :cond_1
    new-instance v5, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "PurchaseAuth undefined in PurchaseAuthEntry: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5
.end method

.method private getReauthSettingsOverNetwork(II)V
    .locals 2
    .param p1, "previousPurchaseAuth"    # I
    .param p2, "newPurchaseAuth"    # I

    .prologue
    .line 514
    new-instance v0, Lcom/google/android/finsky/activities/SettingsActivity$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/activities/SettingsActivity$3;-><init>(Lcom/google/android/finsky/activities/SettingsActivity;II)V

    .line 534
    .local v0, "fetchReauthSettingsOverNetwork":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 535
    return-void
.end method

.method private handleReauthSettingsResponse(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;II)V
    .locals 4
    .param p1, "response"    # Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;
    .param p2, "previousPurchaseAuth"    # I
    .param p3, "newPurchaseAuth"    # I

    .prologue
    .line 543
    const/4 v0, 0x0

    .line 544
    .local v0, "useGmsCoreForAuth":Z
    const/4 v1, 0x0

    .line 545
    .local v1, "usePinBasedAuth":Z
    if-eqz p1, :cond_0

    iget v2, p1, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;->status:I

    if-nez v2, :cond_0

    .line 546
    const/4 v0, 0x1

    .line 547
    iget-object v2, p1, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;->pin:Lcom/google/android/gms/auth/firstparty/dataservice/PinSettings;

    if-eqz v2, :cond_0

    const-string v2, "ACTIVE"

    iget-object v3, p1, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;->pin:Lcom/google/android/gms/auth/firstparty/dataservice/PinSettings;

    iget-object v3, v3, Lcom/google/android/gms/auth/firstparty/dataservice/PinSettings;->status:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 549
    const/4 v1, 0x1

    .line 553
    :cond_0
    invoke-direct {p0, p2, p3, v0, v1}, Lcom/google/android/finsky/activities/SettingsActivity;->startGaiaAuthActivity(IIZZ)V

    .line 555
    return-void
.end method

.method private setContentFilterLevel(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "extraParams"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, -0x64

    .line 579
    const-string v1, "content-level-to-set"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 581
    .local v0, "contentFilterLevel":I
    if-ne v0, v2, :cond_0

    .line 582
    const-string v1, "Content filter authenticated but no level to set"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 597
    :goto_0
    return-void

    .line 587
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v3, 0x191

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->contentFilterLevel:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    const-string v5, "settings-page"

    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logSettingsBackgroundEvent(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 593
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->contentFilterLevel:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 595
    const/16 v1, 0x28

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/SettingsActivity;->setResult(I)V

    .line 596
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->finish()V

    goto :goto_0
.end method

.method private showSelfUpdateCheckResult(Z)V
    .locals 4
    .param p1, "selfUpdateFound"    # Z

    .prologue
    .line 294
    iget-boolean v2, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mIsResumed:Z

    if-eqz v2, :cond_0

    .line 295
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 296
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    if-eqz p1, :cond_1

    const v2, 0x7f0c0317

    :goto_0
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 298
    const v2, 0x7f0c02a0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 299
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 300
    .local v0, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 302
    .end local v0    # "alert":Landroid/app/AlertDialog;
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_0
    return-void

    .line 296
    .restart local v1    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_1
    const v2, 0x7f0c0316

    goto :goto_0
.end method

.method private startAuthChallenge(II)V
    .locals 2
    .param p1, "previousPurchaseAuth"    # I
    .param p2, "newPurchaseAuth"    # I

    .prologue
    const/4 v1, 0x0

    .line 471
    invoke-static {}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->isAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 472
    invoke-direct {p0, p1, p2, v1, v1}, Lcom/google/android/finsky/activities/SettingsActivity;->startGaiaAuthActivity(IIZZ)V

    .line 503
    :goto_0
    return-void

    .line 476
    :cond_0
    new-instance v0, Lcom/google/android/finsky/activities/SettingsActivity$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/activities/SettingsActivity$2;-><init>(Lcom/google/android/finsky/activities/SettingsActivity;II)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SettingsActivity$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private startGaiaAuthActivity(IIZZ)V
    .locals 6
    .param p1, "previousPurchaseAuth"    # I
    .param p2, "newPurchaseAuth"    # I
    .param p3, "useGmsCoreForAuth"    # Z
    .param p4, "usePinBasedAuth"    # Z

    .prologue
    .line 563
    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    const/4 v2, 0x1

    .line 564
    .local v2, "showWarning":Z
    :goto_0
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 565
    .local v5, "extraParams":Landroid/os/Bundle;
    const-string v0, "purchase-auth-previous"

    invoke-virtual {v5, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 566
    const-string v0, "purchase-auth-new"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 567
    iget-object v1, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;

    move-object v0, p0

    move v3, p3

    move v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/activities/GaiaAuthActivity;->getIntent(Landroid/content/Context;Ljava/lang/String;ZZZLandroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/activities/SettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 570
    return-void

    .line 563
    .end local v2    # "showWarning":Z
    .end local v5    # "extraParams":Landroid/os/Bundle;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 359
    const/16 v8, 0x1e

    if-ne p1, v8, :cond_2

    const/4 v8, -0x1

    if-ne p2, v8, :cond_2

    .line 369
    const-string v8, "ContentFilterActivity_selectedFilterLevel"

    const/16 v9, -0x64

    invoke-virtual {p3, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 372
    .local v0, "contentFilterLevel":I
    const/16 v8, -0x64

    if-ne v0, v8, :cond_0

    .line 373
    const-string v8, "Content filter returned code \'OK\' but no level to set"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 464
    .end local v0    # "contentFilterLevel":I
    :goto_0
    return-void

    .line 376
    .restart local v0    # "contentFilterLevel":I
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 377
    .local v2, "extraParams":Landroid/os/Bundle;
    const-string v8, "content-level-to-set"

    invoke-virtual {v2, v8, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 378
    sget-object v8, Lcom/google/android/finsky/utils/FinskyPreferences;->contentPin:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v8}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 379
    .local v1, "currentPin":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 381
    const v8, 0x7f0c02bd

    const v9, 0x7f0c02be

    const/4 v10, 0x0

    invoke-static {p0, v8, v9, v10, v2}, Lcom/google/android/finsky/activities/PinEntryDialog;->getIntent(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    const/16 v9, 0x21

    invoke-virtual {p0, v8, v9}, Lcom/google/android/finsky/activities/SettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 385
    :cond_1
    const v8, 0x7f0c02c1

    const v9, 0x7f0c02c2

    invoke-static {p0, v8, v9, v1, v2}, Lcom/google/android/finsky/activities/PinEntryDialog;->getIntent(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    const/16 v9, 0x1f

    invoke-virtual {p0, v8, v9}, Lcom/google/android/finsky/activities/SettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 390
    .end local v0    # "contentFilterLevel":I
    .end local v1    # "currentPin":Ljava/lang/String;
    .end local v2    # "extraParams":Landroid/os/Bundle;
    :cond_2
    const/16 v8, 0x21

    if-ne p1, v8, :cond_4

    const/4 v8, -0x1

    if-ne p2, v8, :cond_4

    .line 392
    const-string v8, "PinEntryDialog-extra-params"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 393
    .restart local v2    # "extraParams":Landroid/os/Bundle;
    const-string v8, "PinEntryDialog-result-pin"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 394
    .local v4, "newPin":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 395
    const-string v8, "Create PIN result OK but no PIN sent back."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 398
    :cond_3
    const v8, 0x7f0c02bf

    const v9, 0x7f0c02c0

    invoke-static {p0, v8, v9, v4, v2}, Lcom/google/android/finsky/activities/PinEntryDialog;->getIntent(Landroid/content/Context;IILjava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v8

    const/16 v9, 0x22

    invoke-virtual {p0, v8, v9}, Lcom/google/android/finsky/activities/SettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 401
    .end local v2    # "extraParams":Landroid/os/Bundle;
    .end local v4    # "newPin":Ljava/lang/String;
    :cond_4
    const/16 v8, 0x22

    if-ne p1, v8, :cond_6

    const/4 v8, -0x1

    if-ne p2, v8, :cond_6

    .line 405
    const-string v8, "PinEntryDialog-result-pin"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 406
    .restart local v4    # "newPin":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 407
    const-string v8, "Confirm PIN result OK but no PIN sent back."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 410
    :cond_5
    sget-object v8, Lcom/google/android/finsky/utils/FinskyPreferences;->contentPin:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v8, v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 413
    const-string v8, "PinEntryDialog-extra-params"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 415
    .restart local v2    # "extraParams":Landroid/os/Bundle;
    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/SettingsActivity;->setContentFilterLevel(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 417
    .end local v2    # "extraParams":Landroid/os/Bundle;
    .end local v4    # "newPin":Ljava/lang/String;
    :cond_6
    const/16 v8, 0x1f

    if-ne p1, v8, :cond_8

    const/4 v8, -0x1

    if-ne p2, v8, :cond_8

    .line 422
    const-string v8, "PinEntryDialog-extra-params"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 425
    .restart local v2    # "extraParams":Landroid/os/Bundle;
    const-string v8, "content-level-to-set"

    const/16 v9, -0x64

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 427
    .restart local v0    # "contentFilterLevel":I
    sget-object v8, Lcom/google/android/finsky/config/ContentLevel;->SHOW_ALL:Lcom/google/android/finsky/config/ContentLevel;

    invoke-virtual {v8}, Lcom/google/android/finsky/config/ContentLevel;->getValue()I

    move-result v8

    if-ne v0, v8, :cond_7

    .line 428
    sget-object v8, Lcom/google/android/finsky/utils/FinskyPreferences;->contentPin:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v8}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 431
    :cond_7
    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/SettingsActivity;->setContentFilterLevel(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 433
    .end local v0    # "contentFilterLevel":I
    .end local v2    # "extraParams":Landroid/os/Bundle;
    :cond_8
    const/16 v8, 0x20

    if-ne p1, v8, :cond_b

    const/4 v8, -0x1

    if-ne p2, v8, :cond_b

    .line 438
    const-string v8, "GaiaAuthActivity_extraParams"

    invoke-virtual {p3, v8}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 439
    .restart local v2    # "extraParams":Landroid/os/Bundle;
    const-string v8, "purchase-auth-previous"

    const/4 v9, -0x1

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    .line 441
    .local v7, "previousPurchaseAuth":I
    const-string v8, "purchase-auth-new"

    const/4 v9, -0x1

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 444
    .local v5, "newPurchaseAuth":I
    const/4 v8, -0x1

    if-ne v5, v8, :cond_a

    .line 445
    const-string v8, "Missing new value for PurchaseAuth"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 457
    :cond_9
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v6

    .line 458
    .local v6, "preferenceScreen":Landroid/preference/PreferenceScreen;
    const-string v8, "purchase-auth"

    invoke-virtual {v6, v8}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/activities/SettingsListPreference;

    .line 460
    .local v3, "listPreference":Lcom/google/android/finsky/activities/SettingsListPreference;
    invoke-virtual {v3}, Lcom/google/android/finsky/activities/SettingsListPreference;->updateListPreferenceSummary()V

    goto/16 :goto_0

    .line 447
    .end local v3    # "listPreference":Lcom/google/android/finsky/activities/SettingsListPreference;
    .end local v6    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    :cond_a
    iget-object v8, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-string v11, "settings-page"

    invoke-static {v8, v5, v9, v10, v11}, Lcom/google/android/finsky/config/PurchaseAuth;->setAndLogPurchaseAuth(Ljava/lang/String;ILjava/lang/Integer;Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;)V

    .line 452
    const/4 v8, 0x1

    if-eq v5, v8, :cond_9

    .line 453
    sget-object v8, Lcom/google/android/finsky/utils/FinskyPreferences;->lastGaiaAuthTimestamp:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    iget-object v9, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    goto :goto_1

    .line 463
    .end local v2    # "extraParams":Landroid/os/Bundle;
    .end local v5    # "newPurchaseAuth":I
    .end local v7    # "previousPurchaseAuth":I
    :cond_b
    invoke-super {p0, p1, p2, p3}, Landroid/preference/PreferenceActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 147
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 149
    const v0, 0x7f070007

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SettingsActivity;->addPreferencesFromResource(I)V

    .line 150
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 152
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 154
    const-string v0, "Exit SettingsActivity - no current account."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->finish()V

    .line 166
    :goto_0
    return-void

    .line 159
    :cond_0
    new-instance v0, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v1, 0xc

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mPageNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 160
    if-nez p1, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mPageNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 165
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 196
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 201
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 198
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->onBackPressed()V

    .line 199
    const/4 v0, 0x1

    goto :goto_0

    .line 196
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    .line 187
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mIsResumed:Z

    .line 190
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 191
    .local v0, "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 192
    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 9
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 211
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v4

    .line 213
    .local v4, "key":Ljava/lang/String;
    const/4 v1, 0x0

    .line 214
    .local v1, "doBackup":Z
    const-string v6, "update-notifications"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move-object v5, p2

    .line 215
    check-cast v5, Landroid/preference/CheckBoxPreference;

    .line 216
    .local v5, "updateNotifications":Landroid/preference/CheckBoxPreference;
    sget-object v6, Lcom/google/android/finsky/utils/VendingPreferences;->NOTIFY_UPDATES:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v5}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 239
    .end local v5    # "updateNotifications":Landroid/preference/CheckBoxPreference;
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 240
    new-instance v6, Landroid/app/backup/BackupManager;

    invoke-direct {v6, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Landroid/app/backup/BackupManager;->dataChanged()V

    .line 243
    :cond_1
    const/4 v6, 0x1

    return v6

    .line 217
    :cond_2
    const-string v6, "auto-add-shortcuts"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move-object v0, p2

    .line 218
    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 219
    .local v0, "autoAddShortcuts":Landroid/preference/CheckBoxPreference;
    sget-object v6, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_ADD_SHORTCUTS:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 220
    const/4 v1, 0x1

    .line 221
    goto :goto_0

    .end local v0    # "autoAddShortcuts":Landroid/preference/CheckBoxPreference;
    :cond_3
    const-string v6, "clear-history"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 222
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    .line 223
    .local v2, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getRecentSuggestions()Landroid/provider/SearchRecentSuggestions;

    move-result-object v6

    invoke-virtual {v6}, Landroid/provider/SearchRecentSuggestions;->clearHistory()V

    goto :goto_0

    .line 224
    .end local v2    # "finskyApp":Lcom/google/android/finsky/FinskyApp;
    :cond_4
    const-string v6, "content-level"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 225
    const-class v6, Lcom/google/android/finsky/activities/ContentFilterActivity;

    const-string v7, "authAccount"

    iget-object v8, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;

    invoke-static {p0, v6, v7, v8}, Lcom/google/android/finsky/utils/IntentUtils;->createAccountSpecificIntent(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 230
    .local v3, "intent":Landroid/content/Intent;
    const/16 v6, 0x1e

    invoke-virtual {p0, v3, v6}, Lcom/google/android/finsky/activities/SettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 231
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_5
    const-string v6, "os-licenses"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 232
    const v6, 0x7f0c02cb

    const-string v7, "file:///android_asset/licenses.html"

    invoke-static {p0, v6, v7}, Lcom/google/android/finsky/activities/WebViewDialog;->getIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 234
    .restart local v3    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/activities/SettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 235
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_6
    const-string v6, "build-version"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 236
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->doSelfUpdateCheck()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 170
    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    .line 171
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mIsResumed:Z

    .line 174
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 175
    .local v0, "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/SettingsActivity;->configureUpdateNotifications(Landroid/preference/PreferenceScreen;)V

    .line 176
    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/SettingsActivity;->configureAutoUpdateMode(Landroid/preference/PreferenceScreen;)V

    .line 177
    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/SettingsActivity;->configureAutoAddShortcuts(Landroid/preference/PreferenceScreen;)V

    .line 178
    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/SettingsActivity;->configureUserControlsSection(Landroid/preference/PreferenceScreen;)V

    .line 179
    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/SettingsActivity;->configureAboutSection(Landroid/preference/PreferenceScreen;)V

    .line 181
    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 182
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 13
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 311
    const-string v8, "auto-update-mode"

    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 312
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    .line 313
    .local v5, "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-virtual {v5, p2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/activities/SettingsListPreference;

    .line 315
    .local v3, "listPreference":Lcom/google/android/finsky/activities/SettingsListPreference;
    invoke-virtual {v3}, Lcom/google/android/finsky/activities/SettingsListPreference;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 316
    .local v7, "value":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    move-result-object v2

    .line 317
    .local v2, "entry":Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    const/4 v0, 0x0

    .line 318
    .local v0, "autoUpdateEnabled":Z
    const/4 v1, 0x0

    .line 319
    .local v1, "autoUpdateWifiOnly":Z
    sget-object v8, Lcom/google/android/finsky/activities/SettingsActivity$4;->$SwitchMap$com$google$android$finsky$activities$SettingsActivity$AutoUpdateEntry:[I

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    .line 330
    const-string v8, "Unexpected list pref value %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v7, v9, v10

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 334
    :goto_0
    :pswitch_0
    iget-object v8, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v9, 0x192

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->ordinal()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-direct {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getCurrentAutoUpdateEntry()Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;->ordinal()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logSettingsBackgroundEvent(ILjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;)V

    .line 340
    sget-object v8, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_ENABLED:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 341
    sget-object v8, Lcom/google/android/finsky/utils/FinskyPreferences;->AUTO_UPDATE_WIFI_ONLY:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 342
    new-instance v8, Landroid/app/backup/BackupManager;

    invoke-direct {v8, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8}, Landroid/app/backup/BackupManager;->dataChanged()V

    .line 343
    invoke-virtual {v3}, Lcom/google/android/finsky/activities/SettingsListPreference;->updateListPreferenceSummary()V

    .line 355
    .end local v0    # "autoUpdateEnabled":Z
    .end local v1    # "autoUpdateWifiOnly":Z
    .end local v2    # "entry":Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    .end local v3    # "listPreference":Lcom/google/android/finsky/activities/SettingsListPreference;
    .end local v5    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .end local v7    # "value":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 323
    .restart local v0    # "autoUpdateEnabled":Z
    .restart local v1    # "autoUpdateWifiOnly":Z
    .restart local v2    # "entry":Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    .restart local v3    # "listPreference":Lcom/google/android/finsky/activities/SettingsListPreference;
    .restart local v5    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .restart local v7    # "value":Ljava/lang/String;
    :pswitch_1
    const/4 v0, 0x1

    .line 324
    goto :goto_0

    .line 326
    :pswitch_2
    const/4 v0, 0x1

    .line 327
    const/4 v1, 0x1

    .line 328
    goto :goto_0

    .line 344
    .end local v0    # "autoUpdateEnabled":Z
    .end local v1    # "autoUpdateWifiOnly":Z
    .end local v2    # "entry":Lcom/google/android/finsky/activities/SettingsActivity$AutoUpdateEntry;
    .end local v3    # "listPreference":Lcom/google/android/finsky/activities/SettingsListPreference;
    .end local v5    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    .end local v7    # "value":Ljava/lang/String;
    :cond_1
    const-string v8, "purchase-auth"

    invoke-virtual {v8, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 345
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v5

    .line 346
    .restart local v5    # "preferenceScreen":Landroid/preference/PreferenceScreen;
    invoke-virtual {v5, p2}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    .line 347
    .local v3, "listPreference":Landroid/preference/ListPreference;
    invoke-virtual {v3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 348
    .restart local v7    # "value":Ljava/lang/String;
    invoke-static {v7}, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    move-result-object v2

    .line 349
    .local v2, "entry":Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;
    # getter for: Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->mPurchaseAuth:I
    invoke-static {v2}, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->access$200(Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;)I

    move-result v4

    .line 350
    .local v4, "newPurchaseAuth":I
    iget-object v8, p0, Lcom/google/android/finsky/activities/SettingsActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/finsky/config/PurchaseAuth;->getPurchaseAuthType(Ljava/lang/String;)I

    move-result v6

    .line 351
    .local v6, "previousPurchaseAuth":I
    if-eq v6, v4, :cond_0

    .line 352
    invoke-direct {p0, v6, v4}, Lcom/google/android/finsky/activities/SettingsActivity;->startAuthChallenge(II)V

    goto :goto_1

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
