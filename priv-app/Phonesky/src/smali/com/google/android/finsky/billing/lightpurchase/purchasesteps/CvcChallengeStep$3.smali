.class Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$3;
.super Ljava/lang/Object;
.source "CvcChallengeStep.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 127
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "CvcChallengeStep.cvc_popup"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 136
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mMainView:Landroid/view/View;
    invoke-static {v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->access$300(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;)Landroid/view/View;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/UiUtils;->hideKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 131
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 132
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v2, 0x7f040028

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setLayoutId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c02a1

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 134
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 135
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    invoke-virtual {v2}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "CvcChallengeStep.cvc_popup"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
