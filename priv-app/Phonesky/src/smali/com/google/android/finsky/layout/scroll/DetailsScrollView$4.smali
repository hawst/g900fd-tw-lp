.class Lcom/google/android/finsky/layout/scroll/DetailsScrollView$4;
.super Ljava/lang/Object;
.source "DetailsScrollView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$4;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(II)V
    .locals 2
    .param p1, "left"    # I
    .param p2, "top"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$4;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$4;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mExpanderAnimation:Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;
    invoke-static {v1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$600(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;

    move-result-object v1

    # invokes: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->cancelScrollAnimationIfWaiting(Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$400(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;Lcom/google/android/finsky/utils/PlayAnimationUtils$ScrollAnimation;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$4;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->removeOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V

    .line 159
    return-void
.end method
