.class Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;
.super Ljava/lang/Object;
.source "GPlusUtils.java"

# interfaces
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;
.implements Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;
.implements Lcom/google/android/gms/people/PeopleClient$OnCirclesLoadedListener;
.implements Lcom/google/android/gms/people/PeopleClient$OnPeopleLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/GPlusUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CirclesLoader"
.end annotation


# instance fields
.field private mBelongingCircleIds:[Ljava/lang/String;

.field private mCircles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mCirclesLoaded:Z

.field private final mCurrentAccountName:Ljava/lang/String;

.field private final mGetCirclesListener:Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;

.field private final mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

.field private mPeopleLoaded:Z

.field private final mUserToLookUpGaiaObfId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/PeopleClient;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;)V
    .locals 0
    .param p1, "peopleClient"    # Lcom/google/android/gms/people/PeopleClient;
    .param p2, "currentAccountName"    # Ljava/lang/String;
    .param p3, "userToLookUpGaiaObfId"    # Ljava/lang/String;
    .param p4, "circleStatusListener"    # Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;

    .prologue
    .line 305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 306
    iput-object p1, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    .line 307
    iput-object p2, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mCurrentAccountName:Ljava/lang/String;

    .line 308
    iput-object p3, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mUserToLookUpGaiaObfId:Ljava/lang/String;

    .line 309
    iput-object p4, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mGetCirclesListener:Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;

    .line 310
    return-void
.end method

.method private computeBelongingCircles()V
    .locals 8

    .prologue
    .line 419
    iget-boolean v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mCirclesLoaded:Z

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleLoaded:Z

    if-nez v7, :cond_1

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 427
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 428
    .local v2, "belongingCircles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/gms/common/people/data/AudienceMember;>;"
    iget-object v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mBelongingCircleIds:[Ljava/lang/String;

    if-eqz v7, :cond_4

    .line 429
    const/4 v3, 0x0

    .local v3, "i":I
    iget-object v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mBelongingCircleIds:[Ljava/lang/String;

    array-length v5, v7

    .local v5, "length":I
    :goto_1
    if-ge v3, v5, :cond_4

    .line 430
    iget-object v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mBelongingCircleIds:[Ljava/lang/String;

    aget-object v1, v7, v3

    .line 432
    .local v1, "belongingCircleId":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "j":I
    iget-object v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mCircles:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v6

    .local v6, "size":I
    :goto_2
    if-ge v4, v6, :cond_3

    .line 433
    iget-object v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mCircles:Ljava/util/ArrayList;

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 434
    .local v0, "audienceMember":Lcom/google/android/gms/common/people/data/AudienceMember;
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->getCircleId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 435
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 432
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 429
    .end local v0    # "audienceMember":Lcom/google/android/gms/common/people/data/AudienceMember;
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 442
    .end local v1    # "belongingCircleId":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "j":I
    .end local v5    # "length":I
    .end local v6    # "size":I
    :cond_4
    iget-object v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v7, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    .line 443
    iget-object v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v7, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    .line 445
    iget-object v7, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mGetCirclesListener:Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;

    invoke-interface {v7, v2}, Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;->onCirclesLoaded(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private loadData()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 351
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    iget-object v2, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mCurrentAccountName:Ljava/lang/String;

    const/4 v5, -0x1

    const/4 v7, 0x0

    move-object v1, p0

    move-object v4, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/people/PeopleClient;->loadCircles(Lcom/google/android/gms/people/PeopleClient$OnCirclesLoadedListener;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V

    .line 356
    new-instance v8, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    invoke-direct {v8}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;-><init>()V

    .line 357
    .local v8, "options":Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 358
    .local v9, "qualifiedIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mUserToLookUpGaiaObfId:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/utils/GPlusUtils;->gaiaIdToPeopleQualifiedId(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/utils/GPlusUtils;->access$100(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    invoke-virtual {v8, v9}, Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;->setQualifiedIds(Ljava/util/Collection;)Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;

    .line 360
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    iget-object v1, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mCurrentAccountName:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v3, v8}, Lcom/google/android/gms/people/PeopleClient;->loadPeople(Lcom/google/android/gms/people/PeopleClient$OnPeopleLoadedListener;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/PeopleClient$LoadPeopleOptions;)V

    .line 361
    return-void
.end method


# virtual methods
.method public loadCircles()V
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/PeopleClient;->registerConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    .line 321
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/PeopleClient;->registerConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0}, Lcom/google/android/gms/people/PeopleClient;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0}, Lcom/google/android/gms/people/PeopleClient;->isConnecting()Z

    move-result v0

    if-nez v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0}, Lcom/google/android/gms/people/PeopleClient;->connect()V

    .line 327
    :cond_0
    return-void
.end method

.method public onCirclesLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/people/model/CircleBuffer;)V
    .locals 5
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2, "circleBuffer"    # Lcom/google/android/gms/people/model/CircleBuffer;

    .prologue
    .line 365
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 367
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v3

    if-nez v3, :cond_0

    .line 368
    iget-object v3, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mGetCirclesListener:Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;

    invoke-interface {v3}, Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;->onCirclesLoadedFailed()V

    .line 369
    iget-object v3, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v3, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    .line 370
    iget-object v3, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v3, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    .line 389
    :goto_0
    return-void

    .line 375
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mCircles:Ljava/util/ArrayList;

    .line 378
    invoke-virtual {p2}, Lcom/google/android/gms/people/model/CircleBuffer;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/model/Circle;

    .line 379
    .local v1, "circle":Lcom/google/android/gms/people/model/Circle;
    invoke-interface {v1}, Lcom/google/android/gms/people/model/Circle;->getCircleId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1}, Lcom/google/android/gms/people/model/Circle;->getCircleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->forCircle(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    .line 381
    .local v0, "audienceMember":Lcom/google/android/gms/common/people/data/AudienceMember;
    iget-object v3, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mCircles:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 387
    .end local v0    # "audienceMember":Lcom/google/android/gms/common/people/data/AudienceMember;
    .end local v1    # "circle":Lcom/google/android/gms/people/model/Circle;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    invoke-virtual {p2}, Lcom/google/android/gms/people/model/CircleBuffer;->close()V

    throw v3

    .line 384
    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_1
    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mCirclesLoaded:Z

    .line 385
    invoke-direct {p0}, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->computeBelongingCircles()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 387
    invoke-virtual {p2}, Lcom/google/android/gms/people/model/CircleBuffer;->close()V

    goto :goto_0
.end method

.method public onConnected(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "arg"    # Landroid/os/Bundle;

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->loadData()V

    .line 332
    return-void
.end method

.method public onConnectionFailed(Lcom/google/android/gms/common/ConnectionResult;)V
    .locals 1
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;

    .prologue
    .line 342
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 343
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    .line 344
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mGetCirclesListener:Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;

    invoke-interface {v0}, Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;->onCirclesLoadedFailed()V

    .line 346
    return-void
.end method

.method public onDisconnected()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    .line 338
    return-void
.end method

.method public onPeopleLoaded(Lcom/google/android/gms/common/ConnectionResult;Lcom/google/android/gms/people/model/PersonBuffer;)V
    .locals 2
    .param p1, "connectionResult"    # Lcom/google/android/gms/common/ConnectionResult;
    .param p2, "personBuffer"    # Lcom/google/android/gms/people/model/PersonBuffer;

    .prologue
    .line 393
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 395
    invoke-virtual {p1}, Lcom/google/android/gms/common/ConnectionResult;->isSuccess()Z

    move-result v1

    if-nez v1, :cond_0

    .line 396
    iget-object v1, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mGetCirclesListener:Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;

    invoke-interface {v1}, Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;->onCirclesLoadedFailed()V

    .line 397
    iget-object v1, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionCallbacks(Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;)V

    .line 398
    iget-object v1, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleClient:Lcom/google/android/gms/people/PeopleClient;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/people/PeopleClient;->unregisterConnectionFailedListener(Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V

    .line 414
    :goto_0
    return-void

    .line 404
    :cond_0
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/people/model/PersonBuffer;->getCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 405
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/google/android/gms/people/model/PersonBuffer;->get(I)Lcom/google/android/gms/people/model/Person;

    move-result-object v0

    .line 406
    .local v0, "person":Lcom/google/android/gms/people/model/Person;
    invoke-interface {v0}, Lcom/google/android/gms/people/model/Person;->getBelongingCircleIds()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mBelongingCircleIds:[Ljava/lang/String;

    .line 409
    .end local v0    # "person":Lcom/google/android/gms/people/model/Person;
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->mPeopleLoaded:Z

    .line 410
    invoke-direct {p0}, Lcom/google/android/finsky/utils/GPlusUtils$CirclesLoader;->computeBelongingCircles()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    invoke-virtual {p2}, Lcom/google/android/gms/people/model/PersonBuffer;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {p2}, Lcom/google/android/gms/people/model/PersonBuffer;->close()V

    throw v1
.end method
