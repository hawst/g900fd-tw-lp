.class public final Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "VideoCredit"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;


# instance fields
.field public credit:Ljava/lang/String;

.field public creditType:I

.field public hasCredit:Z

.field public hasCreditType:Z

.field public name:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3287
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 3288
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->clear()Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    .line 3289
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .locals 2

    .prologue
    .line 3265
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-nez v0, :cond_1

    .line 3266
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 3268
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    if-nez v0, :cond_0

    .line 3269
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    .line 3271
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3273
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    return-object v0

    .line 3271
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3292
    iput v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    .line 3293
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCreditType:Z

    .line 3294
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    .line 3295
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCredit:Z

    .line 3296
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    .line 3297
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->cachedSize:I

    .line 3298
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 3323
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 3324
    .local v4, "size":I
    iget v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCreditType:Z

    if-eqz v5, :cond_1

    .line 3325
    :cond_0
    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 3328
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCredit:Z

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 3329
    :cond_2
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 3332
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_6

    .line 3333
    const/4 v0, 0x0

    .line 3334
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 3335
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_5

    .line 3336
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 3337
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_4

    .line 3338
    add-int/lit8 v0, v0, 0x1

    .line 3339
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 3335
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3343
    .end local v2    # "element":Ljava/lang/String;
    :cond_5
    add-int/2addr v4, v1

    .line 3344
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 3346
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_6
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoCredit;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 3354
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 3355
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 3359
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 3360
    :sswitch_0
    return-object p0

    .line 3365
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 3366
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 3371
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    .line 3372
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCreditType:Z

    goto :goto_0

    .line 3378
    .end local v4    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    .line 3379
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCredit:Z

    goto :goto_0

    .line 3383
    :sswitch_3
    const/16 v6, 0x1a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 3385
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    if-nez v6, :cond_2

    move v1, v5

    .line 3386
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 3387
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 3388
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3390
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 3391
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 3392
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 3390
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3385
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_1

    .line 3395
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 3396
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    goto :goto_0

    .line 3355
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    .line 3366
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3253
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$VideoCredit;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3304
    iget v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCreditType:Z

    if-eqz v2, :cond_1

    .line 3305
    :cond_0
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->creditType:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 3307
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->hasCredit:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 3308
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->credit:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3310
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 3311
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 3312
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$VideoCredit;->name:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 3313
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 3314
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 3311
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3318
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 3319
    return-void
.end method
