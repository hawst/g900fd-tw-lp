.class public Lcom/google/android/finsky/layout/SongList;
.super Landroid/widget/LinearLayout;
.source "SongList.java"


# instance fields
.field private mHighlightedSongDocId:Ljava/lang/String;

.field private mParent:Landroid/widget/ScrollView;

.field private final mScrollRunnable:Ljava/lang/Runnable;

.field private final mSongSnippets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/layout/SongSnippet;",
            ">;"
        }
    .end annotation
.end field

.field private mSongsContainer:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongList;->mSongSnippets:Ljava/util/Map;

    .line 45
    new-instance v0, Lcom/google/android/finsky/layout/SongList$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/SongList$1;-><init>(Lcom/google/android/finsky/layout/SongList;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongList;->mScrollRunnable:Ljava/lang/Runnable;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/SongList;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongList;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongList;->mHighlightedSongDocId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/SongList;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongList;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongList;->mSongSnippets:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/SongList;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongList;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongList;->mParent:Landroid/widget/ScrollView;

    return-object v0
.end method

.method private hideUi()V
    .locals 2

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SongList;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 238
    .local v0, "parent":Landroid/view/ViewGroup;
    if-eqz v0, :cond_0

    .line 239
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/SongList;->setVisibility(I)V

    goto :goto_0
.end method

.method private highlightSong()V
    .locals 4

    .prologue
    .line 227
    iget-object v2, p0, Lcom/google/android/finsky/layout/SongList;->mSongSnippets:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/finsky/layout/SongList;->mHighlightedSongDocId:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 228
    iget-object v2, p0, Lcom/google/android/finsky/layout/SongList;->mSongSnippets:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 229
    .local v1, "songDocId":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/layout/SongList;->mSongSnippets:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/SongSnippet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/SongSnippet;->setState(I)V

    goto :goto_0

    .line 232
    .end local v1    # "songDocId":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/SongList;->mScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/SongList;->post(Ljava/lang/Runnable;)Z

    .line 234
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method private isSongListMature(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 190
    .local p1, "songList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 191
    .local v1, "songDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->isMature()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 192
    const/4 v2, 0x1

    .line 195
    .end local v1    # "songDoc":Lcom/google/android/finsky/api/model/Document;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private shouldShowArtistNames(Lcom/google/android/finsky/api/model/Document;Ljava/util/List;)Z
    .locals 5
    .param p1, "album"    # Lcom/google/android/finsky/api/model/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/model/Document;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p2, "songs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    const/4 v4, 0x0

    .line 205
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDisplayArtist()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 206
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDisplayArtist()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    move-result-object v3

    iget-object v1, v3, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    .line 213
    .local v1, "representativeTitle":Ljava/lang/String;
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    .line 214
    .local v2, "song":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 215
    const/4 v3, 0x1

    .line 218
    .end local v2    # "song":Lcom/google/android/finsky/api/model/Document;
    :goto_1
    return v3

    .line 209
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "representativeTitle":Ljava/lang/String;
    :cond_1
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "representativeTitle":Ljava/lang/String;
    goto :goto_0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    move v3, v4

    .line 218
    goto :goto_1
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongList;->mScrollRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongList;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 82
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 83
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 76
    const v0, 0x7f0a025f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongList;->mSongsContainer:Landroid/widget/LinearLayout;

    .line 77
    return-void
.end method

.method public setHighlightedSong(Ljava/lang/String;Landroid/widget/ScrollView;)V
    .locals 0
    .param p1, "songDocumentId"    # Ljava/lang/String;
    .param p2, "parent"    # Landroid/widget/ScrollView;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/google/android/finsky/layout/SongList;->mHighlightedSongDocId:Ljava/lang/String;

    .line 223
    iput-object p2, p0, Lcom/google/android/finsky/layout/SongList;->mParent:Landroid/widget/ScrollView;

    .line 224
    return-void
.end method

.method public setListDetails(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Ljava/util/List;ZLjava/util/Set;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 31
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "album"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "useActualTrackNumbers"    # Z
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/navigationmanager/NavigationManager;",
            "Lcom/google/android/play/image/BitmapLoader;",
            "Lcom/google/android/finsky/api/model/Document;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;Z",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    .local p4, "songs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    .local p6, "initiallyOwnedDocuments":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/SongList;->mSongSnippets:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 90
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/SongList;->mSongSnippets:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 91
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/SongList;->mSongsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 94
    :cond_0
    if-eqz p4, :cond_1

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 95
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/layout/SongList;->hideUi()V

    .line 181
    :goto_0
    return-void

    .line 99
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/layout/SongList;->shouldShowArtistNames(Lcom/google/android/finsky/api/model/Document;Ljava/util/List;)Z

    move-result v14

    .line 101
    .local v14, "showArtistNames":Z
    const v3, 0x7f0a025e

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/SongList;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/layout/PlaylistControlButtons;

    .line 104
    .local v8, "songListControl":Lcom/google/android/finsky/layout/PlaylistControlButtons;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v28

    .line 105
    .local v28, "songList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/api/model/Document;

    .line 106
    .local v12, "song":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getSongDetails()Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    move-result-object v3

    iget-object v0, v3, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    move-object/from16 v26, v0

    .line 107
    .local v26, "musicDetails":Lcom/google/android/finsky/protos/DocDetails$MusicDetails;
    if-eqz v26, :cond_3

    move-object/from16 v0, v26

    iget v3, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->durationSec:I

    if-lez v3, :cond_3

    .line 108
    move-object/from16 v0, v28

    invoke-interface {v0, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 112
    .end local v12    # "song":Lcom/google/android/finsky/api/model/Document;
    .end local v26    # "musicDetails":Lcom/google/android/finsky/protos/DocDetails$MusicDetails;
    :cond_4
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v25

    .line 113
    .local v25, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v6

    .line 114
    .local v6, "account":Landroid/accounts/Account;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/SongList;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 115
    .local v7, "context":Landroid/content/Context;
    const v3, 0x7f0a025d

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/SongList;->findViewById(I)Landroid/view/View;

    move-result-object v20

    .line 118
    .local v20, "header":Landroid/view/View;
    const/16 v29, 0x0

    .line 119
    .local v29, "songsWithPreviews":I
    invoke-interface/range {v28 .. v28}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_5
    :goto_2
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/google/android/finsky/api/model/Document;

    .line 120
    .local v27, "songDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual/range {v27 .. v27}, Lcom/google/android/finsky/api/model/Document;->getSongDetails()Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    move-result-object v18

    .line 121
    .local v18, "details":Lcom/google/android/finsky/protos/DocDetails$SongDetails;
    if-eqz v18, :cond_5

    move-object/from16 v0, v18

    iget-boolean v3, v0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->hasPreviewUrl:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->previewUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 123
    add-int/lit8 v29, v29, 0x1

    goto :goto_2

    .line 126
    .end local v18    # "details":Lcom/google/android/finsky/protos/DocDetails$SongDetails;
    .end local v27    # "songDoc":Lcom/google/android/finsky/api/model/Document;
    :cond_6
    const/4 v3, 0x2

    move/from16 v0, v29

    if-lt v0, v3, :cond_8

    .line 129
    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Lcom/google/android/finsky/layout/PlaylistControlButtons;->setVisibility(I)V

    .line 130
    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Lcom/google/android/finsky/layout/PlaylistControlButtons;->configure(Ljava/util/Collection;)V

    .line 131
    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/google/android/finsky/layout/SongList;->isSongListMature(Ljava/util/List;)Z

    move-result v5

    .line 132
    .local v5, "isMature":Z
    new-instance v3, Lcom/google/android/finsky/layout/SongList$2;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/google/android/finsky/layout/SongList$2;-><init>(Lcom/google/android/finsky/layout/SongList;ZLandroid/accounts/Account;Landroid/content/Context;Lcom/google/android/finsky/layout/PlaylistControlButtons;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 155
    .end local v5    # "isMature":Z
    :goto_3
    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v23

    .line 157
    .local v23, "inflater":Landroid/view/LayoutInflater;
    const/16 v19, 0x0

    .line 158
    .local v19, "hasInitializedSnippet":Z
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_4
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v21

    if-ge v0, v3, :cond_b

    .line 159
    move-object/from16 v0, p4

    move/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/api/model/Document;

    .line 160
    .restart local v12    # "song":Lcom/google/android/finsky/api/model/Document;
    const v3, 0x7f0400d6

    const/4 v4, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/finsky/layout/SongSnippet;

    .line 162
    .local v9, "snippet":Lcom/google/android/finsky/layout/SongSnippet;
    if-eqz p5, :cond_9

    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getSongDetails()Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    move-result-object v3

    iget v13, v3, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->trackNumber:I

    .line 165
    .local v13, "trackNumber":I
    :goto_5
    move-object/from16 v0, v25

    invoke-static {v12, v0}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v24

    .line 166
    .local v24, "isOwned":Z
    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v30

    .line 167
    .local v30, "wasOwned":Z
    if-eqz v24, :cond_a

    if-nez v30, :cond_a

    const/16 v16, 0x1

    .local v16, "isNewPurchase":Z
    :goto_6
    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v15, p1

    move-object/from16 v17, p7

    .line 168
    invoke-virtual/range {v9 .. v17}, Lcom/google/android/finsky/layout/SongSnippet;->setSongDetails(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;IZLcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 170
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/SongList;->mSongsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 171
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/SongList;->mSongSnippets:Ljava/util/Map;

    invoke-virtual {v12}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    if-nez v19, :cond_7

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/SongSnippet;->isPlayable()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 175
    invoke-virtual {v9}, Lcom/google/android/finsky/layout/SongSnippet;->initialize()V

    .line 176
    const/16 v19, 0x1

    .line 158
    :cond_7
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 150
    .end local v9    # "snippet":Lcom/google/android/finsky/layout/SongSnippet;
    .end local v12    # "song":Lcom/google/android/finsky/api/model/Document;
    .end local v13    # "trackNumber":I
    .end local v16    # "isNewPurchase":Z
    .end local v19    # "hasInitializedSnippet":Z
    .end local v21    # "i":I
    .end local v23    # "inflater":Landroid/view/LayoutInflater;
    .end local v24    # "isOwned":Z
    .end local v30    # "wasOwned":Z
    :cond_8
    const/16 v3, 0x8

    invoke-virtual {v8, v3}, Lcom/google/android/finsky/layout/PlaylistControlButtons;->setVisibility(I)V

    .line 151
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 152
    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_3

    .line 162
    .restart local v9    # "snippet":Lcom/google/android/finsky/layout/SongSnippet;
    .restart local v12    # "song":Lcom/google/android/finsky/api/model/Document;
    .restart local v19    # "hasInitializedSnippet":Z
    .restart local v21    # "i":I
    .restart local v23    # "inflater":Landroid/view/LayoutInflater;
    :cond_9
    add-int/lit8 v13, v21, 0x1

    goto :goto_5

    .line 167
    .restart local v13    # "trackNumber":I
    .restart local v24    # "isOwned":Z
    .restart local v30    # "wasOwned":Z
    :cond_a
    const/16 v16, 0x0

    goto :goto_6

    .line 180
    .end local v9    # "snippet":Lcom/google/android/finsky/layout/SongSnippet;
    .end local v12    # "song":Lcom/google/android/finsky/api/model/Document;
    .end local v13    # "trackNumber":I
    .end local v24    # "isOwned":Z
    .end local v30    # "wasOwned":Z
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/layout/SongList;->highlightSong()V

    goto/16 :goto_0
.end method
