.class public final Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AndroidAppDelivery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/AndroidAppDelivery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EncryptionParams"
.end annotation


# instance fields
.field public encryptionKey:Ljava/lang/String;

.field public hasEncryptionKey:Z

.field public hasHmacKey:Z

.field public hasVersion:Z

.field public hmacKey:Ljava/lang/String;

.field public version:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 290
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    .line 291
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 294
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->version:I

    .line 295
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasVersion:Z

    .line 296
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->encryptionKey:Ljava/lang/String;

    .line 297
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasEncryptionKey:Z

    .line 298
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hmacKey:Ljava/lang/String;

    .line 299
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasHmacKey:Z

    .line 300
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->cachedSize:I

    .line 301
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 321
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 322
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasVersion:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->version:I

    if-eq v1, v2, :cond_1

    .line 323
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->version:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasEncryptionKey:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->encryptionKey:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 327
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->encryptionKey:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasHmacKey:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hmacKey:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 331
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hmacKey:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 334
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 342
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 343
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 347
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 348
    :sswitch_0
    return-object p0

    .line 353
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->version:I

    .line 354
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasVersion:Z

    goto :goto_0

    .line 358
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->encryptionKey:Ljava/lang/String;

    .line 359
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasEncryptionKey:Z

    goto :goto_0

    .line 363
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hmacKey:Ljava/lang/String;

    .line 364
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasHmacKey:Z

    goto :goto_0

    .line 343
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 307
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasVersion:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->version:I

    if-eq v0, v1, :cond_1

    .line 308
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->version:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 310
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasEncryptionKey:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->encryptionKey:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 311
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->encryptionKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 313
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hasHmacKey:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hmacKey:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 314
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;->hmacKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 316
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 317
    return-void
.end method
