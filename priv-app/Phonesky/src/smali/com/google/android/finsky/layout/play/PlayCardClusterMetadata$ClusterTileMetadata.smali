.class public Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;
.super Ljava/lang/Object;
.source "PlayCardClusterMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ClusterTileMetadata"
.end annotation


# instance fields
.field private final mCardMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

.field private final mRespectChildThumbnailAspectRatio:Z

.field private final mXStart:I

.field private final mYStart:I


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;IIZ)V
    .locals 0
    .param p1, "cardMetadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .param p2, "xStart"    # I
    .param p3, "yStart"    # I
    .param p4, "respectChildThumbnailAspectRatio"    # Z

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->mCardMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    .line 186
    iput p2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->mXStart:I

    .line 187
    iput p3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->mYStart:I

    .line 188
    iput-boolean p4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->mRespectChildThumbnailAspectRatio:Z

    .line 189
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;

    .prologue
    .line 177
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->mRespectChildThumbnailAspectRatio:Z

    return v0
.end method


# virtual methods
.method public getCardMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->mCardMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;

    return-object v0
.end method

.method public getXStart()I
    .locals 1

    .prologue
    .line 196
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->mXStart:I

    return v0
.end method

.method public getYStart()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$ClusterTileMetadata;->mYStart:I

    return v0
.end method
