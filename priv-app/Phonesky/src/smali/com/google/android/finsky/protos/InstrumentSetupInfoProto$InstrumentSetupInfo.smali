.class public final Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "InstrumentSetupInfoProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/InstrumentSetupInfoProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstrumentSetupInfo"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;


# instance fields
.field public addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

.field public balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

.field public footerHtml:[Ljava/lang/String;

.field public hasInstrumentFamily:Z

.field public hasSupported:Z

.field public instrumentFamily:I

.field public supported:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->clear()Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    .line 44
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->_emptyArray:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->_emptyArray:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    sput-object v0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->_emptyArray:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->_emptyArray:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 47
    iput v0, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->instrumentFamily:I

    .line 48
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->hasInstrumentFamily:Z

    .line 49
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->supported:Z

    .line 50
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->hasSupported:Z

    .line 51
    iput-object v1, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 52
    iput-object v1, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    .line 53
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->cachedSize:I

    .line 55
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 86
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 87
    .local v4, "size":I
    iget v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->instrumentFamily:I

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->hasInstrumentFamily:Z

    if-eqz v5, :cond_1

    .line 88
    :cond_0
    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->instrumentFamily:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 91
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->hasSupported:Z

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->supported:Z

    if-eqz v5, :cond_3

    .line 92
    :cond_2
    const/4 v5, 0x2

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->supported:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 95
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-eqz v5, :cond_4

    .line 96
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 99
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    if-eqz v5, :cond_5

    .line 100
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 103
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_8

    .line 104
    const/4 v0, 0x0

    .line 105
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 106
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_7

    .line 107
    iget-object v5, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 108
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_6

    .line 109
    add-int/lit8 v0, v0, 0x1

    .line 110
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 106
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 114
    .end local v2    # "element":Ljava/lang/String;
    :cond_7
    add-int/2addr v4, v1

    .line 115
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 117
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_8
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 125
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 126
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 130
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 131
    :sswitch_0
    return-object p0

    .line 136
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 137
    .local v4, "value":I
    sparse-switch v4, :sswitch_data_1

    goto :goto_0

    .line 149
    :sswitch_2
    iput v4, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->instrumentFamily:I

    .line 150
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->hasInstrumentFamily:Z

    goto :goto_0

    .line 156
    .end local v4    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->supported:Z

    .line 157
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->hasSupported:Z

    goto :goto_0

    .line 161
    :sswitch_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-nez v6, :cond_1

    .line 162
    new-instance v6, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 164
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 168
    :sswitch_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    if-nez v6, :cond_2

    .line 169
    new-instance v6, Lcom/google/android/finsky/protos/CommonDevice$Money;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CommonDevice$Money;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    .line 171
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 175
    :sswitch_6
    const/16 v6, 0x2a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 177
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    if-nez v6, :cond_4

    move v1, v5

    .line 178
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 179
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_3

    .line 180
    iget-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182
    :cond_3
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_5

    .line 183
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 184
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 182
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 177
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_1

    .line 187
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 188
    iput-object v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    goto :goto_0

    .line 126
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
    .end sparse-switch

    .line 137
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0x8 -> :sswitch_2
        0x9 -> :sswitch_2
        0x64 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->instrumentFamily:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->hasInstrumentFamily:Z

    if-eqz v2, :cond_1

    .line 62
    :cond_0
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->instrumentFamily:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 64
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->hasSupported:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->supported:Z

    if-eqz v2, :cond_3

    .line 65
    :cond_2
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->supported:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 67
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-eqz v2, :cond_4

    .line 68
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 70
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    if-eqz v2, :cond_5

    .line 71
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->balance:Lcom/google/android/finsky/protos/CommonDevice$Money;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 73
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 74
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 75
    iget-object v2, p0, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->footerHtml:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 76
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 77
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 74
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 81
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 82
    return-void
.end method
