.class public Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;
.super Lcom/google/android/finsky/adapters/CardSimpleListAdapter;
.source "EditorialDfeListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$1;,
        Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    }
.end annotation


# instance fields
.field private final mContainerDocument:Lcom/google/android/finsky/api/model/Document;

.field private final mDescriptionPaddingLeftRight:I

.field private mDetailsTextViewBinder:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

.field private final mExtraPaddingLeftRight:I

.field private final mInitialRestoreState:Landroid/os/Bundle;

.field private final mLeadingSpacerHeight:I

.field private final mNumItemsPerFooterRow:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeList;Landroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "loader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5, "containerDocument"    # Lcom/google/android/finsky/api/model/Document;
    .param p6, "listData"    # Lcom/google/android/finsky/api/model/DfeList;
    .param p7, "savedState"    # Landroid/os/Bundle;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 94
    const/4 v7, 0x0

    const/4 v8, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p6

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/DfeList;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 96
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/finsky/api/model/DfeList;->getBackendId()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const v1, 0x7f04008b

    :goto_0
    iput v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mCellLayoutId:I

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    .line 101
    .local v10, "res":Landroid/content/res/Resources;
    const v1, 0x7f0e0018

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mColumnCount:I

    .line 102
    const v1, 0x7f0e0018

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mNumItemsPerFooterRow:I

    .line 103
    const v1, 0x7f0b00f8

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mDescriptionPaddingLeftRight:I

    .line 105
    const v1, 0x7f0b0087

    invoke-virtual {v10, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mExtraPaddingLeftRight:I

    .line 107
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mLeadingSpacerHeight:I

    .line 110
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    .line 112
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mInitialRestoreState:Landroid/os/Bundle;

    .line 113
    return-void

    .line 96
    .end local v10    # "res":Landroid/content/res/Resources;
    :cond_0
    const v1, 0x7f04008e

    goto :goto_0
.end method

.method private applyExtraPadding(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 228
    iget v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mExtraPaddingLeftRight:I

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    iget v2, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mExtraPaddingLeftRight:I

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 232
    return-void
.end method

.method private getEditorialDescriptionView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 347
    if-eqz p1, :cond_1

    check-cast p1, Lcom/google/android/finsky/layout/DetailsTextSection;

    .end local p1    # "convertView":Landroid/view/View;
    move-object v0, p1

    .line 354
    .local v0, "description":Lcom/google/android/finsky/layout/DetailsTextSection;
    :goto_0
    iget v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mDescriptionPaddingLeftRight:I

    iget v2, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mExtraPaddingLeftRight:I

    add-int v8, v1, v2

    .line 356
    .local v8, "fullDescriptionPaddingLeftRight":I
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getPaddingTop()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, v8, v1, v8, v2}, Lcom/google/android/finsky/layout/DetailsTextSection;->setPadding(IIII)V

    .line 362
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsTextSection;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 363
    .local v9, "res":Landroid/content/res/Resources;
    const v1, 0x7f09009c

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 364
    .local v6, "backgroundFillColor":I
    iget-object v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getEditorialSeriesContainer()Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/google/android/finsky/utils/UiUtils;->getFillColor(Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;I)I

    move-result v6

    .line 372
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v5, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/DetailsTextSection;->bindEditorialDescription(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;ZLandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ILcom/google/android/finsky/activities/TextSectionStateListener;)V

    .line 375
    return-object v0

    .line 347
    .end local v0    # "description":Lcom/google/android/finsky/layout/DetailsTextSection;
    .end local v6    # "backgroundFillColor":I
    .end local v8    # "fullDescriptionPaddingLeftRight":I
    .end local v9    # "res":Landroid/content/res/Resources;
    .restart local p1    # "convertView":Landroid/view/View;
    :cond_1
    const v1, 0x7f04008f

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DetailsTextSection;

    move-object v0, v1

    goto :goto_0
.end method

.method private getEditorialFooterHeader(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v3, 0x0

    .line 215
    if-nez p1, :cond_0

    .line 216
    const v1, 0x7f04011a

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :cond_0
    move-object v0, p1

    .line 219
    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    .line 220
    .local v0, "plainHeader":Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;
    iget-object v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/ContainerList;->getBackendId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0c01b5

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 222
    iget v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mExtraPaddingLeftRight:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setExtraHorizontalPadding(I)V

    .line 224
    return-object v0
.end method

.method private getEditorialFooterRow(Landroid/view/ViewGroup;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 23
    .param p1, "convertView"    # Landroid/view/ViewGroup;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "position"    # I

    .prologue
    .line 237
    invoke-super/range {p0 .. p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getCount()I

    move-result v2

    sub-int v2, p3, v2

    add-int/lit8 v12, v2, -0x4

    .line 238
    .local v12, "footerRowNumber":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mNumItemsPerFooterRow:I

    mul-int v18, v12, v2

    .line 239
    .local v18, "offset":I
    if-nez p1, :cond_0

    .line 240
    const v2, 0x7f040044

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v2, v1, v4}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .end local p1    # "convertView":Landroid/view/ViewGroup;
    check-cast p1, Landroid/view/ViewGroup;

    .line 242
    .restart local p1    # "convertView":Landroid/view/ViewGroup;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mNumItemsPerFooterRow:I

    if-ge v14, v2, :cond_0

    .line 243
    const v2, 0x7f04008d

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v2, v1, v4}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 246
    .local v11, "emptyEntry":Landroid/view/ViewGroup;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getEditorialVideoWrapper(Landroid/view/ViewGroup;)Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;

    move-result-object v13

    .line 247
    .local v13, "holder":Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    invoke-virtual {v11, v13}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 248
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 242
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .end local v11    # "emptyEntry":Landroid/view/ViewGroup;
    .end local v13    # "holder":Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    .end local v14    # "i":I
    :cond_0
    move-object/from16 v10, p1

    .line 251
    check-cast v10, Lcom/google/android/finsky/layout/BucketRow;

    .line 254
    .local v10, "bucketRow":Lcom/google/android/finsky/layout/BucketRow;
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Lcom/google/android/finsky/layout/BucketRow;->setSameChildHeight(Z)V

    .line 256
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getEditorialSeriesContainer()Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    move-result-object v2

    iget-object v0, v2, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    move-object/from16 v22, v0

    .line 259
    .local v22, "videoSnippets":[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v21

    .line 260
    .local v21, "videoImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    const/4 v14, 0x0

    .restart local v14    # "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mNumItemsPerFooterRow:I

    if-ge v14, v2, :cond_6

    .line 261
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;

    .line 265
    .restart local v13    # "holder":Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    add-int v20, v18, v14

    .line 267
    .local v20, "snippetNumber":I
    move-object/from16 v0, v22

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    move/from16 v0, v20

    if-le v0, v2, :cond_1

    .line 268
    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mWrapper:Landroid/view/ViewGroup;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$000(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Landroid/view/ViewGroup;

    move-result-object v2

    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 260
    :goto_2
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 272
    :cond_1
    aget-object v19, v22, v20

    .line 274
    .local v19, "snippet":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mWrapper:Landroid/view/ViewGroup;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$000(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Landroid/view/ViewGroup;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 275
    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mTitle:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$100(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Landroid/widget/TextView;

    move-result-object v2

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 276
    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mDescription:Landroid/widget/TextView;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$200(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Landroid/widget/TextView;

    move-result-object v2

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->description:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 278
    const/4 v3, 0x0

    .line 280
    .local v3, "videoUrl":Ljava/lang/String;
    move-object/from16 v0, v19

    iget-object v9, v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->image:[Lcom/google/android/finsky/protos/Common$Image;

    .local v9, "arr$":[Lcom/google/android/finsky/protos/Common$Image;
    array-length v0, v9

    move/from16 v17, v0

    .local v17, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_3
    move/from16 v0, v17

    if-ge v15, v0, :cond_4

    aget-object v16, v9, v15

    .line 281
    .local v16, "image":Lcom/google/android/finsky/protos/Common$Image;
    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    const/4 v4, 0x3

    if-ne v2, v4, :cond_3

    .line 282
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    .line 280
    :cond_2
    :goto_4
    add-int/lit8 v15, v15, 0x1

    goto :goto_3

    .line 283
    :cond_3
    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    .line 284
    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 289
    .end local v16    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :cond_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_5

    .line 290
    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$300(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Lcom/google/android/finsky/layout/HeroGraphicView;

    move-result-object v2

    const/high16 v4, 0x3f100000    # 0.5625f

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/layout/HeroGraphicView;->setDefaultAspectRatio(F)V

    .line 294
    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$300(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Lcom/google/android/finsky/layout/HeroGraphicView;

    move-result-object v2

    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$300(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Lcom/google/android/finsky/layout/HeroGraphicView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/HeroGraphicView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b010f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/layout/HeroGraphicView;->setMaximumHeight(I)V

    .line 297
    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$300(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Lcom/google/android/finsky/layout/HeroGraphicView;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, v21

    invoke-virtual {v2, v4, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;->load(Lcom/google/android/play/image/BitmapLoader;Ljava/util/List;)V

    .line 299
    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$300(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Lcom/google/android/finsky/layout/HeroGraphicView;

    move-result-object v2

    move-object/from16 v0, v19

    iget-object v4, v0, Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;->title:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->isMature()Z

    move-result v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/finsky/layout/HeroGraphicView;->showPlayIcon(Ljava/lang/String;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 303
    # getter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;
    invoke-static {v13}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$300(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Lcom/google/android/finsky/layout/HeroGraphicView;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/finsky/layout/HeroGraphicView;->setBackgroundResource(I)V

    .line 306
    :cond_5
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->clear()V

    goto/16 :goto_2

    .line 309
    .end local v3    # "videoUrl":Ljava/lang/String;
    .end local v9    # "arr$":[Lcom/google/android/finsky/protos/Common$Image;
    .end local v13    # "holder":Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    .end local v15    # "i$":I
    .end local v17    # "len$":I
    .end local v19    # "snippet":Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;
    .end local v20    # "snippetNumber":I
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->applyExtraPadding(Landroid/view/View;)V

    .line 310
    return-object p1
.end method

.method private getEditorialHeroView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 337
    if-eqz p1, :cond_0

    check-cast p1, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;

    .end local p1    # "convertView":Landroid/view/View;
    move-object v0, p1

    .line 341
    .local v0, "header":Lcom/google/android/finsky/layout/EditorialHeroGraphicView;
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v2, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v3, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->bindDetailsEditorial(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 343
    return-object v0

    .line 337
    .end local v0    # "header":Lcom/google/android/finsky/layout/EditorialHeroGraphicView;
    .restart local p1    # "convertView":Landroid/view/View;
    :cond_0
    const v1, 0x7f04008c

    const/4 v2, 0x0

    invoke-virtual {p0, v1, p2, v2}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;

    move-object v0, v1

    goto :goto_0
.end method

.method private getEditorialVideoWrapper(Landroid/view/ViewGroup;)Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    .locals 2
    .param p1, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 317
    new-instance v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;-><init>(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$1;)V

    .line 318
    .local v0, "wrapper":Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    # setter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mWrapper:Landroid/view/ViewGroup;
    invoke-static {v0, p1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$002(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;

    .line 319
    const v1, 0x7f0a01bb

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/HeroGraphicView;

    # setter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;
    invoke-static {v0, v1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$302(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;Lcom/google/android/finsky/layout/HeroGraphicView;)Lcom/google/android/finsky/layout/HeroGraphicView;

    .line 320
    const v1, 0x7f0a01bc

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    # setter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mTitle:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$102(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 321
    const v1, 0x7f0a01bd

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    # setter for: Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mDescription:Landroid/widget/TextView;
    invoke-static {v0, v1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->access$202(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;Landroid/widget/TextView;)Landroid/widget/TextView;

    .line 322
    return-object v0
.end method

.method private getFooterItemCount()I
    .locals 6

    .prologue
    .line 178
    iget-object v2, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mContainerDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getEditorialSeriesContainer()Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    move-result-object v0

    .line 181
    .local v0, "templateData":Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
    iget-object v2, v0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->videoSnippet:[Lcom/google/android/finsky/protos/DocumentV2$VideoSnippet;

    array-length v1, v2

    .line 182
    .local v1, "videoCount":I
    int-to-double v2, v1

    iget v4, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mNumItemsPerFooterRow:I

    int-to-double v4, v4

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    return v2
.end method

.method private getLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "convertView"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 327
    if-nez p1, :cond_0

    .line 328
    const v0, 0x7f04014d

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p2, v1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 331
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mLeadingSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 332
    const v0, 0x7f0a0029

    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    .line 333
    return-object p1
.end method


# virtual methods
.method protected configureBucketRow(Lcom/google/android/finsky/layout/BucketRow;)V
    .locals 1
    .param p1, "row"    # Lcom/google/android/finsky/layout/BucketRow;

    .prologue
    .line 380
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/layout/BucketRow;->setSameChildHeight(Z)V

    .line 381
    return-void
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 193
    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v1, v2, 0x3

    .line 194
    .local v1, "result":I
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getFooterItemCount()I

    move-result v0

    .line 195
    .local v0, "footerItemCount":I
    if-lez v0, :cond_0

    .line 197
    add-int/lit8 v2, v0, 0x1

    add-int/2addr v1, v2

    .line 200
    :cond_0
    return v1
.end method

.method public getItemViewType(I)I
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v4, 0x1

    .line 148
    if-nez p1, :cond_0

    .line 149
    const/16 v4, 0x9

    .line 174
    :goto_0
    return v4

    .line 152
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getFooterItemCount()I

    move-result v0

    .line 153
    .local v0, "footerRowCount":I
    if-lez v0, :cond_1

    move v3, v4

    .line 154
    .local v3, "shouldShowFooter":Z
    :goto_1
    add-int/lit8 v1, v0, 0x1

    .line 155
    .local v1, "footerRowCountWithHeader":I
    if-ne p1, v4, :cond_2

    .line 156
    const/4 v4, 0x5

    goto :goto_0

    .line 153
    .end local v1    # "footerRowCountWithHeader":I
    .end local v3    # "shouldShowFooter":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 157
    .restart local v1    # "footerRowCountWithHeader":I
    .restart local v3    # "shouldShowFooter":Z
    :cond_2
    const/4 v4, 0x2

    if-ne p1, v4, :cond_3

    .line 158
    const/4 v4, 0x6

    goto :goto_0

    .line 159
    :cond_3
    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->isMoreDataAvailable()Z

    move-result v4

    if-nez v4, :cond_5

    .line 162
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getCount()I

    move-result v2

    .line 163
    .local v2, "itemCount":I
    sub-int v4, v2, v1

    if-ne p1, v4, :cond_4

    .line 165
    const/4 v4, 0x7

    goto :goto_0

    .line 166
    :cond_4
    sub-int v4, v2, v0

    if-lt p1, v4, :cond_5

    .line 168
    const/16 v4, 0x8

    goto :goto_0

    .line 174
    .end local v2    # "itemCount":I
    :cond_5
    add-int/lit8 v4, p1, -0x3

    invoke-super {p0, v4}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getItemViewType(I)I

    move-result v4

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 125
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getItemViewType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 139
    add-int/lit8 v1, p1, -0x3

    invoke-super {p0, v1, p2, p3}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 142
    .local v0, "result":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->applyExtraPadding(Landroid/view/View;)V

    .line 143
    .end local v0    # "result":Landroid/view/View;
    .end local p2    # "convertView":Landroid/view/View;
    :goto_0
    return-object v0

    .line 127
    .restart local p2    # "convertView":Landroid/view/View;
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getLeadingSpacer(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 129
    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getEditorialHeroView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 131
    :pswitch_2
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getEditorialDescriptionView(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 133
    :pswitch_3
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getEditorialFooterHeader(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 135
    :pswitch_4
    check-cast p2, Landroid/view/ViewGroup;

    .end local p2    # "convertView":Landroid/view/View;
    invoke-direct {p0, p2, p3, p1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->getEditorialFooterRow(Landroid/view/ViewGroup;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 125
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 187
    const/16 v0, 0xa

    return v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 393
    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->onDestroyView()V

    .line 394
    iget-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mDetailsTextViewBinder:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mDetailsTextViewBinder:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/DetailsTextViewBinder;->onDestroyView()V

    .line 396
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mDetailsTextViewBinder:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    .line 398
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/ListView;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mDetailsTextViewBinder:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    if-eqz v0, :cond_0

    .line 386
    iget-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;->mDetailsTextViewBinder:Lcom/google/android/finsky/activities/DetailsTextViewBinder;

    invoke-virtual {v0, p2}, Lcom/google/android/finsky/activities/DetailsTextViewBinder;->saveInstanceState(Landroid/os/Bundle;)V

    .line 388
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/adapters/CardSimpleListAdapter;->onSaveInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V

    .line 389
    return-void
.end method
