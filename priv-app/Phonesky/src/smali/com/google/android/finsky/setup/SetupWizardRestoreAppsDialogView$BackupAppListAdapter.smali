.class Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SetupWizardRestoreAppsDialogView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackupAppListAdapter"
.end annotation


# instance fields
.field private mAppsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    .local p2, "appsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;>;"
    iput-object p1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 127
    iput-object p2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;->mAppsList:Ljava/util/ArrayList;

    .line 128
    invoke-virtual {p1}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 130
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;->mAppsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 139
    if-nez p1, :cond_0

    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;

    invoke-virtual {v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c03ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;->mAppsList:Ljava/util/ArrayList;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->title:Ljava/lang/String;

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 147
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 153
    if-nez p2, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040187

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    move-object v0, p2

    .line 156
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsDialogView$BackupAppListAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    return-object p2
.end method
