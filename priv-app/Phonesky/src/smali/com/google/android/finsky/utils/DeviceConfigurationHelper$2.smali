.class final Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;
.super Ljava/lang/Object;
.source "DeviceConfigurationHelper.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->doUploadDeviceConfiguration(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

.field final synthetic val$request:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/analytics/FinskyEventLog;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iput-object p2, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;->val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    iput-object p3, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;->val$request:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 7
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;->val$eventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x78

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    move-object v3, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 228
    const-string v0, "Couldn\'t upload device config"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;->val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;->val$listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;->onError(Lcom/android/volley/VolleyError;)V

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;->val$request:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    # invokes: Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->doNextRequest(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V
    invoke-static {v0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->access$000(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V

    .line 233
    return-void
.end method
