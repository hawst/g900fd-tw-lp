.class public Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;
.super Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;
.source "PlayCardViewMyAppsDownloading.java"


# instance fields
.field private mDownloadingBytes:Landroid/widget/TextView;

.field private mDownloadingPercentage:Landroid/widget/TextView;

.field private mProgressBar:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->mThumbnailAspectRatio:F

    .line 32
    return-void
.end method


# virtual methods
.method public bindProgress(Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;)V
    .locals 4
    .param p1, "progressReport"    # Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->mDownloadingBytes:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->mDownloadingPercentage:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-static {v0, p1, v1, v2, v3}, Lcom/google/android/finsky/adapters/DownloadProgressHelper;->configureDownloadProgressUi(Landroid/content/Context;Lcom/google/android/finsky/receivers/Installer$InstallerProgressReport;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ProgressBar;)V

    .line 53
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->onFinishInflate()V

    .line 38
    const v0, 0x7f0a014d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->mDownloadingBytes:Landroid/widget/TextView;

    .line 39
    const v0, 0x7f0a014c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->mDownloadingPercentage:Landroid/widget/TextView;

    .line 40
    const v0, 0x7f0a010c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->mProgressBar:Landroid/widget/ProgressBar;

    .line 41
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyAppsDownloading;->measureThumbnailSpanningHeight(I)V

    .line 47
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->onMeasure(II)V

    .line 48
    return-void
.end method
