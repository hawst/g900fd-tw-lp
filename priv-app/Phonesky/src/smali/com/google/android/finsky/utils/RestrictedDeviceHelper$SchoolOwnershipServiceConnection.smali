.class Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;
.super Ljava/lang/Object;
.source "RestrictedDeviceHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/RestrictedDeviceHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SchoolOwnershipServiceConnection"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mListener:Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p1, p0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;->mContext:Landroid/content/Context;

    .line 128
    iput-object p2, p0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;->mListener:Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;

    .line 129
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;Lcom/google/android/finsky/utils/RestrictedDeviceHelper$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;
    .param p3, "x2"    # Lcom/google/android/finsky/utils/RestrictedDeviceHelper$1;

    .prologue
    .line 121
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;-><init>(Landroid/content/Context;Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 133
    invoke-static {p2}, Lcom/google/android/nfcprovision/ISchoolOwnedService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/google/android/nfcprovision/ISchoolOwnedService;

    move-result-object v1

    .line 135
    .local v1, "ownedService":Lcom/google/android/nfcprovision/ISchoolOwnedService;
    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;->mListener:Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;

    invoke-interface {v1}, Lcom/google/android/nfcprovision/ISchoolOwnedService;->isSchoolOwned()Z

    move-result v3

    invoke-interface {v2, v3}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;->onComplete(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    iget-object v2, p0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 142
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_1
    const-string v2, "Error calling school-ownership service; assume not school-owned"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 138
    iget-object v2, p0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;->mListener:Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$OnCompleteListener;->onComplete(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    iget-object v2, p0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/finsky/utils/RestrictedDeviceHelper$SchoolOwnershipServiceConnection;->mContext:Landroid/content/Context;

    invoke-virtual {v3, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    throw v2
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 147
    return-void
.end method
