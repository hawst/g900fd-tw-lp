.class Lcom/google/android/finsky/receivers/InstallerTask$1;
.super Ljava/lang/Object;
.source "InstallerTask.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/receivers/InstallerTask;->requestDeliveryData(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/receivers/InstallerTask;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 996
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->val$packageName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;)V
    .locals 12
    .param p1, "deliveryResponse"    # Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 999
    iget v7, p1, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->status:I

    .line 1000
    .local v7, "responseCode":I
    if-ne v7, v5, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mInstallerDataStore:Lcom/google/android/finsky/appstate/InstallerDataStore;
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$000(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->val$packageName:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;->appDeliveryData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-interface {v0, v1, v2, v10, v11}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDeliveryData(Ljava/lang/String;Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;J)V

    .line 1004
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$100(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->val$packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/AppStates;->getApp(Ljava/lang/String;)Lcom/google/android/finsky/appstate/AppStates$AppState;

    move-result-object v0

    iget-object v8, v0, Lcom/google/android/finsky/appstate/AppStates$AppState;->installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 1005
    .local v8, "updated":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->processDeliveryData(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Z)V
    invoke-static {v0, v8, v5}, Lcom/google/android/finsky/receivers/InstallerTask;->access$200(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Z)V

    .line 1006
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->startNextDownload(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V
    invoke-static {v0, v8}, Lcom/google/android/finsky/receivers/InstallerTask;->access$300(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    .line 1018
    .end local v8    # "updated":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :goto_0
    return-void

    .line 1009
    :cond_0
    const-string v0, "Received non-OK response %d"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1010
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    invoke-virtual {v0, v6}, Lcom/google/android/finsky/receivers/InstallerTask;->cancel(Z)V

    .line 1011
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->deliveryResponseToInstallerError(I)I
    invoke-static {v0, v7}, Lcom/google/android/finsky/receivers/InstallerTask;->access$400(Lcom/google/android/finsky/receivers/InstallerTask;I)I

    move-result v4

    .line 1012
    .local v4, "deliveryError":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x68

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->val$packageName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-static {v5}, Lcom/google/android/finsky/receivers/InstallerTask;->access$500(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1015
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    sget-object v1, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->DOWNLOAD_ERROR:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->notifyListeners(Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    invoke-static {v0, v1, v4}, Lcom/google/android/finsky/receivers/InstallerTask;->access$600(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V

    .line 1016
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$1;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->showDownloadNotification(ILjava/lang/String;)V
    invoke-static {v0, v4, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->access$700(Lcom/google/android/finsky/receivers/InstallerTask;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 996
    check-cast p1, Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask$1;->onResponse(Lcom/google/android/finsky/protos/Delivery$DeliveryResponse;)V

    return-void
.end method
