.class public Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3Activity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;
.source "AddDcb3Activity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3BaseActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;)Landroid/content/Intent;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "instrumentStatus"    # Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    .prologue
    .line 18
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3Activity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 19
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, p1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/AddDcb3Activity;->putIntentExtras(Ljava/lang/String;Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;Landroid/content/Intent;)V

    .line 20
    return-object v0
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 27
    const/16 v0, 0x349

    return v0
.end method
