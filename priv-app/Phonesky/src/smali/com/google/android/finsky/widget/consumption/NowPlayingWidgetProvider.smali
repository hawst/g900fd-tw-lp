.class public Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;
.super Lcom/google/android/finsky/widget/BaseWidgetProvider;
.source "NowPlayingWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;,
        Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;,
        Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    }
.end annotation


# static fields
.field private static final ACCESSIBILITY_OVERLAY_IDS:[I

.field private static final BACKENDS:[I

.field private static final BG_IMAGE_OVERRIDE:Ljava/io/File;

.field private static final BG_OVERRIDE_IMAGE_PATH:Ljava/lang/String;

.field private static final CONTAINERS:[I

.field private static final IMAGE_IDS:[I

.field private static final PORTRAIT_BLOCKS:[Lcom/google/android/finsky/widget/consumption/Block;

.field private static final PORTRAIT_LARGE_REPLACEMENT:Lcom/google/android/finsky/widget/consumption/Block;

.field private static final SQUARE_BLOCKS:[Lcom/google/android/finsky/widget/consumption/Block;

.field public static mImageLoader:Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;

.field private static final sRandom:Ljava/util/Random;

.field private static sSupportedBackendIds:[I


# instance fields
.field private final mRowStartCounts:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    const v7, 0x7f0b01c7

    const/4 v6, 0x4

    const v5, 0x7f0b01c8

    .line 84
    sget-object v0, Lcom/google/android/finsky/config/G;->myLibraryWidgetBackgroundImageLocation:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->BG_OVERRIDE_IMAGE_PATH:Ljava/lang/String;

    .line 89
    sget-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->BG_OVERRIDE_IMAGE_PATH:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->BG_IMAGE_OVERRIDE:Ljava/io/File;

    .line 94
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->sRandom:Ljava/util/Random;

    .line 97
    new-array v0, v6, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->sSupportedBackendIds:[I

    .line 1375
    new-array v0, v6, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->IMAGE_IDS:[I

    .line 1382
    new-array v0, v6, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->ACCESSIBILITY_OVERLAY_IDS:[I

    .line 1389
    new-array v0, v6, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->BACKENDS:[I

    .line 1393
    new-array v0, v6, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->CONTAINERS:[I

    .line 1397
    new-instance v0, Lcom/google/android/finsky/widget/consumption/Block;

    const v1, 0x7f0400e8

    invoke-direct {v0, v1}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v1, 0x7f0b01bf

    const v2, 0x7f0b01c0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v0

    const v1, 0x7f0b01c1

    const v2, 0x7f0b01c2

    invoke-virtual {v0, v6, v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;->hosting(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->PORTRAIT_LARGE_REPLACEMENT:Lcom/google/android/finsky/widget/consumption/Block;

    .line 1404
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/finsky/widget/consumption/Block;

    new-instance v1, Lcom/google/android/finsky/widget/consumption/Block;

    const v2, 0x7f0400e5

    invoke-direct {v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v2, 0x7f0b01bf

    const v3, 0x7f0b01c0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->PORTRAIT_LARGE_REPLACEMENT:Lcom/google/android/finsky/widget/consumption/Block;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;->replaceLastOccurenceInRowWith(Lcom/google/android/finsky/widget/consumption/Block;)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/widget/consumption/Block;->markToSupportMetadata()Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    aput-object v1, v0, v9

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/finsky/widget/consumption/Block;

    const v3, 0x7f0400ed

    invoke-direct {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v3, 0x7f0b01c1

    const v4, 0x7f0b01c0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    const v3, 0x7f0b01c1

    const v4, 0x7f0b01c2

    invoke-virtual {v2, v8, v3, v4}, Lcom/google/android/finsky/widget/consumption/Block;->hosting(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v1, Lcom/google/android/finsky/widget/consumption/Block;

    const v2, 0x7f0400ee

    invoke-direct {v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v2, 0x7f0b01c3

    const v3, 0x7f0b01c0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    const/4 v2, 0x3

    const v3, 0x7f0b01c3

    const v4, 0x7f0b01c4

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/widget/consumption/Block;->hosting(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/finsky/widget/consumption/Block;

    const v3, 0x7f0400e6

    invoke-direct {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v3, 0x7f0b01c1

    const v4, 0x7f0b01c2

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v1, Lcom/google/android/finsky/widget/consumption/Block;

    const v2, 0x7f0400e7

    invoke-direct {v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v2, 0x7f0b01c3

    const v3, 0x7f0b01c4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->PORTRAIT_BLOCKS:[Lcom/google/android/finsky/widget/consumption/Block;

    .line 1428
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/android/finsky/widget/consumption/Block;

    new-instance v1, Lcom/google/android/finsky/widget/consumption/Block;

    const v2, 0x7f0400eb

    invoke-direct {v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v2, 0x7f0b01c5

    const v3, 0x7f0b01c5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;->limitRowStartTo(I)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/widget/consumption/Block;->markToSupportMetadata()Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    aput-object v1, v0, v9

    const/4 v1, 0x1

    new-instance v2, Lcom/google/android/finsky/widget/consumption/Block;

    const v3, 0x7f0400e9

    invoke-direct {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v3, 0x7f0b01c6

    const v4, 0x7f0b01c6

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/widget/consumption/Block;->markToSupportMetadata()Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v1, Lcom/google/android/finsky/widget/consumption/Block;

    const v2, 0x7f0400ef

    invoke-direct {v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v2, 0x7f0b01c5

    invoke-virtual {v1, v7, v2}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    invoke-virtual {v1, v8, v7, v7}, Lcom/google/android/finsky/widget/consumption/Block;->hosting(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    aput-object v1, v0, v8

    const/4 v1, 0x3

    new-instance v2, Lcom/google/android/finsky/widget/consumption/Block;

    const v3, 0x7f0400ea

    invoke-direct {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    invoke-virtual {v2, v7, v7}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;->limitRowStartTo(I)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/widget/consumption/Block;->markToSupportMetadata()Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v1, Lcom/google/android/finsky/widget/consumption/Block;

    const v2, 0x7f0400f1

    invoke-direct {v1, v2}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v2, 0x7f0b01c5

    invoke-virtual {v1, v5, v2}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    invoke-virtual {v1, v6, v5, v5}, Lcom/google/android/finsky/widget/consumption/Block;->hosting(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x5

    new-instance v2, Lcom/google/android/finsky/widget/consumption/Block;

    const v3, 0x7f0400f2

    invoke-direct {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v3, 0x7f0b01c6

    invoke-virtual {v2, v5, v3}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v5, v5}, Lcom/google/android/finsky/widget/consumption/Block;->hosting(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/finsky/widget/consumption/Block;

    const v3, 0x7f0400f0

    invoke-direct {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    invoke-virtual {v2, v5, v7}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2, v8, v5, v5}, Lcom/google/android/finsky/widget/consumption/Block;->hosting(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/finsky/widget/consumption/Block;

    const v3, 0x7f0400f3

    invoke-direct {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v3, 0x7f0b01c6

    const v4, 0x7f0b01c5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/google/android/finsky/widget/consumption/Block;->hosting(I)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    const v3, 0x7f0b01c6

    const v4, 0x7f0b01c5

    invoke-virtual {v2, v9, v3, v4}, Lcom/google/android/finsky/widget/consumption/Block;->withChild(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v5, v5}, Lcom/google/android/finsky/widget/consumption/Block;->withChild(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2, v8, v5, v5}, Lcom/google/android/finsky/widget/consumption/Block;->withChild(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3, v5, v5}, Lcom/google/android/finsky/widget/consumption/Block;->withChild(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/finsky/widget/consumption/Block;->limitRowStartTo(I)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/google/android/finsky/widget/consumption/Block;

    const v3, 0x7f0400f4

    invoke-direct {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    const v3, 0x7f0b01c6

    invoke-virtual {v2, v7, v3}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;->hosting(I)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2, v9, v7, v7}, Lcom/google/android/finsky/widget/consumption/Block;->withChild(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3, v5, v5}, Lcom/google/android/finsky/widget/consumption/Block;->withChild(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2, v8, v5, v5}, Lcom/google/android/finsky/widget/consumption/Block;->withChild(III)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/finsky/widget/consumption/Block;->limitRowStartTo(I)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/google/android/finsky/widget/consumption/Block;

    const v3, 0x7f0400ec

    invoke-direct {v2, v3}, Lcom/google/android/finsky/widget/consumption/Block;-><init>(I)V

    invoke-virtual {v2, v5, v5}, Lcom/google/android/finsky/widget/consumption/Block;->sized(II)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/widget/consumption/Block;->markToSupportMetadata()Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->SQUARE_BLOCKS:[Lcom/google/android/finsky/widget/consumption/Block;

    return-void

    .line 89
    :cond_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->BG_OVERRIDE_IMAGE_PATH:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 97
    :array_0
    .array-data 4
        0x2
        0x4
        0x1
        0x6
    .end array-data

    .line 1375
    :array_1
    .array-data 4
        0x7f0a027a
        0x7f0a027e
        0x7f0a027c
        0x7f0a0280
    .end array-data

    .line 1382
    :array_2
    .array-data 4
        0x7f0a027b
        0x7f0a027f
        0x7f0a027d
        0x7f0a0281
    .end array-data

    .line 1389
    :array_3
    .array-data 4
        0x1
        0x4
        0x2
        0x6
    .end array-data

    .line 1393
    :array_4
    .array-data 4
        0x7f0a028d
        0x7f0a028f
        0x7f0a028e
        0x7f0a0290
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/finsky/widget/BaseWidgetProvider;-><init>()V

    .line 64
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->mRowStartCounts:Landroid/util/SparseIntArray;

    .line 1492
    return-void
.end method

.method protected static varargs fetchConsumptionDataIfNecessary(Landroid/content/Context;[I)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendIds"    # [I

    .prologue
    const/4 v11, 0x0

    .line 1326
    invoke-static {}, Lcom/google/android/finsky/services/ConsumptionAppDataCache;->get()Lcom/google/android/finsky/services/ConsumptionAppDataCache;

    move-result-object v3

    .line 1328
    .local v3, "cache":Lcom/google/android/finsky/services/ConsumptionAppDataCache;
    array-length v0, p1

    .line 1329
    .local v0, "N":I
    new-array v2, v0, [I

    .line 1330
    .local v2, "backendsToUpdate":[I
    const/4 v6, 0x0

    .line 1331
    .local v6, "numBackendsToUpdate":I
    const/4 v5, 0x0

    .local v5, "i":I
    move v7, v6

    .end local v6    # "numBackendsToUpdate":I
    .local v7, "numBackendsToUpdate":I
    :goto_0
    if-ge v5, v0, :cond_2

    .line 1332
    aget v1, p1, v5

    .line 1333
    .local v1, "backendId":I
    invoke-virtual {v3, v1}, Lcom/google/android/finsky/services/ConsumptionAppDataCache;->hasConsumptionAppData(I)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {v3, v1}, Lcom/google/android/finsky/services/ConsumptionAppDataCache;->isLoadingData(I)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1334
    :cond_0
    sget-boolean v8, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v8, :cond_4

    .line 1335
    const-string v8, "Data for [%s] is available or loading, skipping"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v11

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    move v6, v7

    .line 1331
    .end local v7    # "numBackendsToUpdate":I
    .restart local v6    # "numBackendsToUpdate":I
    :goto_1
    add-int/lit8 v5, v5, 0x1

    move v7, v6

    .end local v6    # "numBackendsToUpdate":I
    .restart local v7    # "numBackendsToUpdate":I
    goto :goto_0

    .line 1339
    :cond_1
    invoke-virtual {v3, v1}, Lcom/google/android/finsky/services/ConsumptionAppDataCache;->startLoading(I)V

    .line 1340
    add-int/lit8 v6, v7, 0x1

    .end local v7    # "numBackendsToUpdate":I
    .restart local v6    # "numBackendsToUpdate":I
    aput v1, v2, v7

    goto :goto_1

    .line 1343
    .end local v1    # "backendId":I
    .end local v6    # "numBackendsToUpdate":I
    .restart local v7    # "numBackendsToUpdate":I
    :cond_2
    if-lez v7, :cond_3

    .line 1344
    new-array v4, v7, [I

    .line 1345
    .local v4, "copyOfBackendsToUpdate":[I
    invoke-static {v2, v11, v4, v11, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1346
    invoke-static {p0, v4}, Lcom/google/android/finsky/services/LoadConsumptionDataService;->scheduleAlarmForUpdate(Landroid/content/Context;[I)V

    .line 1348
    .end local v4    # "copyOfBackendsToUpdate":[I
    :cond_3
    return-void

    .restart local v1    # "backendId":I
    :cond_4
    move v6, v7

    .end local v7    # "numBackendsToUpdate":I
    .restart local v6    # "numBackendsToUpdate":I
    goto :goto_1
.end method

.method private generateAccountNeededState(Landroid/content/Context;II)Landroid/widget/RemoteViews;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetId"    # I
    .param p3, "backendId"    # I

    .prologue
    const v5, 0x7f0a0288

    const v4, 0x7f0a0284

    .line 478
    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateBaseTree(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v1

    .line 480
    .local v1, "views":Landroid/widget/RemoteViews;
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0400e4

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 482
    .local v0, "content":Landroid/widget/RemoteViews;
    invoke-virtual {v1, v4}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 483
    invoke-virtual {v1, v4, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 485
    invoke-static {p1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getAddAccountIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 487
    const/4 v2, 0x0

    invoke-virtual {v1, v5, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 489
    invoke-direct {p0, p1, v1, p3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->useCustomStylingIfNecessary(Landroid/content/Context;Landroid/widget/RemoteViews;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 490
    const v2, 0x7f0a00f4

    const v3, 0x7f0c01e3

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 494
    :cond_0
    return-object v1
.end method

.method private generateBaseTree(Landroid/content/Context;Z)Landroid/widget/RemoteViews;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "showBackground"    # Z

    .prologue
    const v6, 0x7f0a028a

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 341
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0400f5

    invoke-direct {v1, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 343
    .local v1, "views":Landroid/widget/RemoteViews;
    const v2, 0x7f0a028b

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 344
    invoke-static {v5}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getHeaderIconRes(I)I

    move-result v2

    invoke-virtual {v1, v6, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 345
    const-string v2, ""

    invoke-virtual {v1, v6, v2}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 346
    invoke-static {v5}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getEmptyBackgroundRes(I)I

    move-result v0

    .line 347
    .local v0, "emptyBackgroundRes":I
    if-eqz p2, :cond_0

    if-eqz v0, :cond_0

    .line 348
    const v2, 0x7f0a0282

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 352
    :cond_0
    const v2, 0x7f0a0284

    invoke-virtual {v1, v2}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 353
    const v2, 0x7f0a0288

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 354
    const v2, 0x7f0a00d0

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 355
    const v2, 0x7f0a0291

    invoke-virtual {v1, v2, v4}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 356
    const v2, 0x7f0a0285

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 358
    return-object v1
.end method

.method private generateConfigurationState(Landroid/content/Context;II)Landroid/widget/RemoteViews;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetId"    # I
    .param p3, "backendId"    # I

    .prologue
    const v5, 0x7f0a0288

    .line 363
    const/4 v3, 0x1

    invoke-direct {p0, p1, v3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateBaseTree(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v2

    .line 365
    .local v2, "views":Landroid/widget/RemoteViews;
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0400f6

    invoke-direct {v0, v3, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 367
    .local v0, "content":Landroid/widget/RemoteViews;
    const v3, 0x7f0a0284

    invoke-virtual {v2, v3, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 369
    const-class v3, Lcom/google/android/finsky/widget/consumption/NowPlayingTrampoline;

    invoke-static {p1, v3, p2}, Lcom/google/android/finsky/widget/TrampolineActivity;->getPendingLaunchIntent(Landroid/content/Context;Ljava/lang/Class;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 371
    .local v1, "intent":Landroid/app/PendingIntent;
    invoke-virtual {v2, v5, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 372
    const/4 v3, 0x0

    invoke-virtual {v2, v5, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 374
    invoke-direct {p0, p1, v2, p3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->useCustomStylingIfNecessary(Landroid/content/Context;Landroid/widget/RemoteViews;I)Z

    .line 376
    return-object v2
.end method

.method private generateDisabledState(Landroid/content/Context;II)Landroid/widget/RemoteViews;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetId"    # I
    .param p3, "backendId"    # I

    .prologue
    .line 380
    const/4 v9, 0x1

    invoke-direct {p0, p1, v9}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateBaseTree(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v8

    .line 382
    .local v8, "views":Landroid/widget/RemoteViews;
    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v9

    const v10, 0x7f0400f6

    invoke-direct {v1, v9, v10}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 387
    .local v1, "content":Landroid/widget/RemoteViews;
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    .line 388
    .local v6, "pm":Landroid/content/pm/PackageManager;
    invoke-static {p3}, Lcom/google/android/finsky/utils/IntentUtils;->getPackageName(I)Ljava/lang/String;

    move-result-object v5

    .line 389
    .local v5, "packageName":Ljava/lang/String;
    const/4 v9, 0x0

    invoke-virtual {v6, v5, v9}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 390
    .local v3, "info":Landroid/content/pm/ApplicationInfo;
    invoke-virtual {v3, v6}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 391
    .local v0, "appName":Ljava/lang/String;
    const v9, 0x7f0c0344

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v0, v10, v11

    invoke-virtual {p1, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 396
    .end local v0    # "appName":Ljava/lang/String;
    .end local v3    # "info":Landroid/content/pm/ApplicationInfo;
    .end local v5    # "packageName":Ljava/lang/String;
    .end local v6    # "pm":Landroid/content/pm/PackageManager;
    .local v7, "text":Ljava/lang/String;
    :goto_0
    const v9, 0x7f0a00f4

    invoke-virtual {v1, v9, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 398
    const v9, 0x7f0a0284

    invoke-virtual {v8, v9, v1}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 400
    invoke-static {p1, p3}, Lcom/google/android/finsky/widget/consumption/EnableAppReceiver;->getEnableIntent(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 401
    .local v4, "intent":Landroid/app/PendingIntent;
    const v9, 0x7f0a0288

    invoke-virtual {v8, v9, v4}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 402
    const v9, 0x7f0a0288

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 404
    invoke-direct {p0, p1, v8, p3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->useCustomStylingIfNecessary(Landroid/content/Context;Landroid/widget/RemoteViews;I)Z

    .line 406
    return-object v8

    .line 392
    .end local v4    # "intent":Landroid/app/PendingIntent;
    .end local v7    # "text":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 394
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v7, ""

    .restart local v7    # "text":Ljava/lang/String;
    goto :goto_0
.end method

.method private generateUnavailableState(Landroid/content/Context;II)Landroid/widget/RemoteViews;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetId"    # I
    .param p3, "backendId"    # I

    .prologue
    const v10, 0x7f0a0288

    const/4 v9, 0x0

    .line 445
    const/4 v7, 0x1

    invoke-direct {p0, p1, v7}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateBaseTree(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v6

    .line 447
    .local v6, "views":Landroid/widget/RemoteViews;
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0400f6

    invoke-direct {v0, v7, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 450
    .local v0, "content":Landroid/widget/RemoteViews;
    const v7, 0x7f0c0345

    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 451
    .local v3, "text":Ljava/lang/String;
    const v7, 0x7f0a00f4

    invoke-virtual {v0, v7, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 453
    const v7, 0x7f0a0284

    invoke-virtual {v6, v7, v0}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 455
    invoke-static {p3}, Lcom/google/android/finsky/utils/IntentUtils;->getPackageName(I)Ljava/lang/String;

    move-result-object v2

    .line 456
    .local v2, "packageName":Ljava/lang/String;
    new-instance v7, Landroid/net/Uri$Builder;

    invoke-direct {v7}, Landroid/net/Uri$Builder;-><init>()V

    const-string v8, "https"

    invoke-virtual {v7, v8}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v8, "play.google.com"

    invoke-virtual {v7, v8}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v8, "store/apps/details"

    invoke-virtual {v7, v8}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    const-string v8, "id"

    invoke-virtual {v7, v8, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 462
    .local v4, "uri":Landroid/net/Uri;
    new-instance v5, Landroid/content/Intent;

    const-class v7, Lcom/google/android/finsky/activities/MainActivity;

    invoke-direct {v5, p1, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 463
    .local v5, "viewIntent":Landroid/content/Intent;
    const-string v7, "android.intent.action.VIEW"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 464
    invoke-virtual {v5, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 465
    const/high16 v7, 0x10000000

    invoke-virtual {v5, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 467
    invoke-static {p1, v9, v5, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 468
    .local v1, "intent":Landroid/app/PendingIntent;
    invoke-virtual {v6, v10, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 469
    invoke-virtual {v6, v10, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 471
    invoke-direct {p0, p1, v6, p3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->useCustomStylingIfNecessary(Landroid/content/Context;Landroid/widget/RemoteViews;I)Z

    .line 473
    return-object v6
.end method

.method private generateViewTree(IILcom/google/android/finsky/api/model/DfeToc;Landroid/content/Context;Ljava/util/Map;II)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    .locals 39
    .param p1, "backend"    # I
    .param p2, "appWidgetId"    # I
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "context"    # Landroid/content/Context;
    .param p6, "areaWidth"    # I
    .param p7, "areaHeight"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/google/android/finsky/api/model/DfeToc;",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;",
            "Landroid/graphics/Bitmap;",
            ">;II)",
            "Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;"
        }
    .end annotation

    .prologue
    .line 501
    .local p5, "cachedImages":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;Landroid/graphics/Bitmap;>;"
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getBackends(I)[I

    move-result-object v4

    move-object/from16 v0, p4

    invoke-static {v0, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->fetchConsumptionDataIfNecessary(Landroid/content/Context;[I)V

    .line 503
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v1, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateBaseTree(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v16

    .line 506
    .local v16, "widget":Landroid/widget/RemoteViews;
    new-instance v31, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;-><init>(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;)V

    .line 507
    .local v31, "output":Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    move-object/from16 v0, v31

    move-object/from16 v1, v16

    # setter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static {v0, v1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$002(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;Landroid/widget/RemoteViews;)Landroid/widget/RemoteViews;

    .line 509
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getTitleRes(Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v37

    .line 510
    .local v37, "title":Ljava/lang/String;
    const v4, 0x7f0a028b

    move-object/from16 v0, v16

    move-object/from16 v1, v37

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 512
    const v4, 0x7f0a028a

    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getHeaderIconRes(I)I

    move-result v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 514
    move-object/from16 v0, p4

    move/from16 v1, p1

    move/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getHeaderIntent(Landroid/content/Context;II)Landroid/content/Intent;

    move-result-object v24

    .line 515
    .local v24, "headerIntent":Landroid/content/Intent;
    if-eqz v24, :cond_0

    .line 516
    const/4 v4, 0x0

    move-object/from16 v0, p4

    move/from16 v1, p2

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v32

    .line 518
    .local v32, "pendingIntent":Landroid/app/PendingIntent;
    const v4, 0x7f0a028c

    move-object/from16 v0, v16

    move-object/from16 v1, v32

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 519
    const v4, 0x7f0a028c

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 520
    const v4, 0x7f0a028c

    move-object/from16 v0, v16

    move-object/from16 v1, v37

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 523
    .end local v32    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0e001d

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v29

    .line 524
    .local v29, "maxCorpora":I
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getConsumptionDocLists(I)Ljava/util/List;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move/from16 v0, v29

    invoke-static {v4, v0, v14, v15}, Lcom/google/android/finsky/widget/consumption/NowPlayingScorer;->score(Ljava/util/List;IJ)Ljava/util/List;

    move-result-object v33

    .line 528
    .local v33, "scoredBackends":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;>;"
    const/4 v4, 0x0

    move-object/from16 v0, v33

    invoke-static {v0, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger;->arrange(Ljava/util/List;I)Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;

    move-result-object v21

    .line 529
    .local v21, "arrangement":Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;->quadrantToData:[Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;

    array-length v0, v4

    move/from16 v20, v0

    .line 531
    .local v20, "N":I
    move-object/from16 v0, v21

    iget v4, v0, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;->layoutVariant:I

    if-nez v4, :cond_7

    const/16 v35, 0x0

    .line 533
    .local v35, "stretchedQuadrant":I
    :goto_0
    move-object/from16 v5, v16

    .line 534
    .local v5, "target":Landroid/widget/RemoteViews;
    const/4 v4, 0x1

    move/from16 v0, v20

    if-le v0, v4, :cond_1

    .line 535
    invoke-static/range {v21 .. v21}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getLayout(Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;)I

    move-result v36

    .line 536
    .local v36, "subLayout":I
    const v4, 0x7f0a0284

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 537
    new-instance v23, Landroid/widget/RemoteViews;

    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v23

    move/from16 v1, v36

    invoke-direct {v0, v4, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 538
    .local v23, "content":Landroid/widget/RemoteViews;
    const v4, 0x7f0a0284

    move-object/from16 v0, v16

    move-object/from16 v1, v23

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 539
    move-object/from16 v5, v23

    .line 542
    .end local v23    # "content":Landroid/widget/RemoteViews;
    .end local v36    # "subLayout":I
    :cond_1
    const/4 v4, 0x0

    move-object/from16 v0, v31

    # setter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showBackground:Z
    invoke-static {v0, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$302(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;Z)Z

    .line 543
    const/4 v4, 0x1

    move-object/from16 v0, v31

    # setter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showEmptyBackground:Z
    invoke-static {v0, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$402(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;Z)Z

    .line 544
    const/16 v25, 0x0

    .line 545
    .local v25, "heightRemaining":I
    const/16 v26, 0x0

    .local v26, "i":I
    :goto_1
    move/from16 v0, v26

    move/from16 v1, v20

    if-ge v0, v1, :cond_c

    .line 546
    move/from16 v11, p6

    .line 547
    .local v11, "width":I
    move/from16 v12, p7

    .line 549
    .local v12, "height":I
    const v7, 0x7f0a0284

    .line 550
    .local v7, "container":I
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;->quadrantToData:[Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;

    array-length v4, v4

    move-object/from16 v0, v21

    iget v6, v0, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;->layoutVariant:I

    move/from16 v0, v26

    invoke-static {v4, v0, v6}, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;->getLocation(III)B

    move-result v8

    .line 552
    .local v8, "location":I
    const/4 v4, 0x1

    move/from16 v0, v20

    if-le v0, v4, :cond_3

    .line 553
    sget-object v4, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->CONTAINERS:[I

    aget v7, v4, v26

    .line 554
    div-int/lit8 v11, v11, 0x2

    .line 556
    const/4 v4, 0x4

    move/from16 v0, v20

    if-eq v0, v4, :cond_2

    const/4 v4, 0x3

    move/from16 v0, v20

    if-ne v0, v4, :cond_3

    move/from16 v0, v26

    move/from16 v1, v35

    if-eq v0, v1, :cond_3

    .line 557
    :cond_2
    move/from16 v0, p7

    int-to-float v4, v0

    int-to-float v6, v11

    div-float/2addr v4, v6

    const v6, 0x400ccccd    # 2.2f

    cmpl-float v4, v4, v6

    if-lez v4, :cond_b

    .line 561
    and-int/lit8 v4, v8, 0x1

    if-eqz v4, :cond_9

    .line 564
    and-int/lit8 v4, v8, 0x4

    if-eqz v4, :cond_8

    div-int/lit8 v12, p7, 0x3

    .line 577
    :goto_2
    add-int v12, v12, v25

    .line 581
    :cond_3
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;->quadrantToData:[Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;

    aget-object v10, v4, v26

    .local v10, "docList":Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;
    move-object/from16 v4, p0

    move/from16 v6, p2

    move-object/from16 v9, p4

    move-object/from16 v13, p5

    .line 582
    invoke-virtual/range {v4 .. v13}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->populateWidget(Landroid/widget/RemoteViews;IIILandroid/content/Context;Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;IILjava/util/Map;)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;

    move-result-object v38

    .line 586
    .local v38, "uris":Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    const/4 v4, 0x4

    move/from16 v0, v20

    if-eq v0, v4, :cond_4

    const/4 v4, 0x3

    move/from16 v0, v20

    if-ne v0, v4, :cond_5

    move/from16 v0, v26

    move/from16 v1, v35

    if-eq v0, v1, :cond_5

    .line 587
    :cond_4
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->heightRemaining:I
    invoke-static/range {v38 .. v38}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$500(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)I

    move-result v25

    .line 590
    :cond_5
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showBackground:Z
    invoke-static/range {v38 .. v38}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$300(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Z

    move-result v4

    move-object/from16 v0, v31

    # |= operator for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showBackground:Z
    invoke-static {v0, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$376(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;I)Z

    .line 592
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showEmptyBackground:Z
    invoke-static/range {v38 .. v38}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$400(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Z

    move-result v4

    move-object/from16 v0, v31

    # &= operator for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showEmptyBackground:Z
    invoke-static {v0, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$472(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;I)Z

    .line 595
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mLoadedSuccessfully:Z
    invoke-static/range {v38 .. v38}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$100(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 596
    const/4 v4, 0x0

    move-object/from16 v0, v31

    # setter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mLoadedSuccessfully:Z
    invoke-static {v0, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$102(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;Z)Z

    .line 598
    :cond_6
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mUris:Ljava/util/List;
    invoke-static/range {v31 .. v31}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$200(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Ljava/util/List;

    move-result-object v4

    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mUris:Ljava/util/List;
    invoke-static/range {v38 .. v38}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$200(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 545
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_1

    .line 531
    .end local v5    # "target":Landroid/widget/RemoteViews;
    .end local v7    # "container":I
    .end local v8    # "location":I
    .end local v10    # "docList":Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;
    .end local v11    # "width":I
    .end local v12    # "height":I
    .end local v25    # "heightRemaining":I
    .end local v26    # "i":I
    .end local v35    # "stretchedQuadrant":I
    .end local v38    # "uris":Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    :cond_7
    const/16 v35, 0x1

    goto/16 :goto_0

    .line 564
    .restart local v5    # "target":Landroid/widget/RemoteViews;
    .restart local v7    # "container":I
    .restart local v8    # "location":I
    .restart local v11    # "width":I
    .restart local v12    # "height":I
    .restart local v25    # "heightRemaining":I
    .restart local v26    # "i":I
    .restart local v35    # "stretchedQuadrant":I
    :cond_8
    mul-int/lit8 v4, p7, 0x2

    div-int/lit8 v12, v4, 0x3

    goto :goto_2

    .line 569
    :cond_9
    and-int/lit8 v4, v8, 0x4

    if-eqz v4, :cond_a

    mul-int/lit8 v4, p7, 0x2

    div-int/lit8 v12, v4, 0x3

    :goto_3
    goto :goto_2

    :cond_a
    div-int/lit8 v12, p7, 0x3

    goto :goto_3

    .line 574
    :cond_b
    div-int/lit8 v12, p7, 0x2

    goto :goto_2

    .line 601
    .end local v7    # "container":I
    .end local v8    # "location":I
    .end local v11    # "width":I
    .end local v12    # "height":I
    :cond_c
    const/16 v22, 0x0

    .line 602
    .local v22, "backgroundRes":I
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showBackground:Z
    invoke-static/range {v31 .. v31}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$300(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 603
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getBackgroundRes(I)I

    move-result v22

    .line 625
    :cond_d
    :goto_4
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v16

    move/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->useCustomStylingIfNecessary(Landroid/content/Context;Landroid/widget/RemoteViews;I)Z

    move-result v28

    .line 628
    .local v28, "isUsingOverrideStyling":Z
    if-nez v28, :cond_e

    if-eqz v22, :cond_e

    .line 629
    const v4, 0x7f0a0282

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 632
    :cond_e
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showEmptyBackground:Z
    invoke-static/range {v31 .. v31}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$400(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Z

    move-result v4

    if-eqz v4, :cond_12

    if-nez p1, :cond_12

    const v4, 0x7f0b0159

    move-object/from16 v0, p4

    invoke-static {v0, v4}, Lcom/google/android/finsky/widget/WidgetUtils;->getDips(Landroid/content/Context;I)I

    move-result v4

    move/from16 v0, p6

    if-lt v0, v4, :cond_12

    const/16 v34, 0x1

    .line 636
    .local v34, "shouldShowEmptyHolder":Z
    :goto_5
    if-eqz v34, :cond_13

    .line 637
    const v4, 0x7f0a0285

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 643
    :goto_6
    if-nez p1, :cond_14

    move-object/from16 v13, p0

    move-object/from16 v14, p4

    move-object/from16 v15, p3

    move/from16 v17, p2

    move/from16 v18, p6

    move/from16 v19, p7

    .line 644
    invoke-direct/range {v13 .. v19}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->updateHotseat(Landroid/content/Context;Lcom/google/android/finsky/api/model/DfeToc;Landroid/widget/RemoteViews;III)V

    .line 651
    :goto_7
    return-object v31

    .line 604
    .end local v28    # "isUsingOverrideStyling":Z
    .end local v34    # "shouldShowEmptyHolder":Z
    :cond_f
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showEmptyBackground:Z
    invoke-static/range {v31 .. v31}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$400(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 605
    if-eqz p1, :cond_11

    .line 606
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getEmptyBackgroundRes(I)I

    move-result v22

    .line 607
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p4

    move/from16 v1, p1

    invoke-static {v0, v1, v4}, Lcom/google/android/finsky/utils/IntentUtils;->buildConsumptionAppLaunchIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v30

    .line 609
    .local v30, "openIntent":Landroid/content/Intent;
    if-eqz v30, :cond_10

    .line 610
    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p4

    move-object/from16 v1, v30

    invoke-static {v0, v4, v1, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v27

    .line 611
    .local v27, "intent":Landroid/app/PendingIntent;
    const v4, 0x7f0a0288

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 613
    .end local v27    # "intent":Landroid/app/PendingIntent;
    :cond_10
    const v4, 0x7f0a0288

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_4

    .line 615
    .end local v30    # "openIntent":Landroid/content/Intent;
    :cond_11
    if-eqz v24, :cond_d

    .line 616
    const/4 v4, 0x0

    move-object/from16 v0, p4

    move/from16 v1, p2

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v27

    .line 618
    .restart local v27    # "intent":Landroid/app/PendingIntent;
    const v4, 0x7f0a0288

    move-object/from16 v0, v16

    move-object/from16 v1, v27

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 619
    const v4, 0x7f0a0288

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_4

    .line 632
    .end local v27    # "intent":Landroid/app/PendingIntent;
    .restart local v28    # "isUsingOverrideStyling":Z
    :cond_12
    const/16 v34, 0x0

    goto :goto_5

    .line 639
    .restart local v34    # "shouldShowEmptyHolder":Z
    :cond_13
    const v4, 0x7f0a0285

    const/4 v6, 0x4

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_6

    .line 647
    :cond_14
    const v4, 0x7f0a0291

    const/16 v6, 0x8

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_7
.end method

.method private generateWidgetLayout(Landroid/content/Context;IIII)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;
    .locals 25
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "numDocuments"    # I
    .param p3, "listBackend"    # I
    .param p4, "rowWidth"    # I
    .param p5, "height"    # I

    .prologue
    .line 963
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v22

    .line 965
    .local v22, "rows":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Lcom/google/android/finsky/widget/consumption/Block;>;>;"
    const/16 v17, 0x0

    .line 969
    .local v17, "numImages":I
    :goto_0
    if-lez p5, :cond_1

    move/from16 v0, p2

    move/from16 v1, v17

    if-le v0, v1, :cond_1

    .line 970
    move/from16 v5, p4

    .line 972
    .local v5, "width":I
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v21

    .line 976
    .local v21, "row":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/Block;>;"
    const/16 v23, 0x0

    .line 978
    .local v23, "tallestElement":I
    const/16 v16, 0x0

    .line 980
    .local v16, "numBlocksAdded":I
    const/4 v9, 0x1

    .line 983
    .local v9, "isFirstInRow":Z
    :goto_1
    if-lez v5, :cond_0

    move/from16 v0, p2

    move/from16 v1, v17

    if-le v0, v1, :cond_0

    .line 985
    if-nez v23, :cond_2

    move/from16 v6, p5

    .line 986
    .local v6, "rowHeight":I
    :goto_2
    sub-int v8, p2, v17

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v7, p3

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->findLargestBlock(Landroid/content/Context;IIIIZ)Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v10

    .line 988
    .local v10, "block":Lcom/google/android/finsky/widget/consumption/Block;
    if-nez v10, :cond_3

    .line 1006
    .end local v6    # "rowHeight":I
    .end local v10    # "block":Lcom/google/android/finsky/widget/consumption/Block;
    :cond_0
    if-nez v16, :cond_4

    .line 1056
    .end local v5    # "width":I
    .end local v9    # "isFirstInRow":Z
    .end local v16    # "numBlocksAdded":I
    .end local v21    # "row":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/Block;>;"
    .end local v23    # "tallestElement":I
    :cond_1
    new-instance v3, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$2;-><init>(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;Landroid/content/Context;)V

    move-object/from16 v0, v22

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1101
    new-instance v4, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;

    move/from16 v0, v17

    move/from16 v1, p2

    if-lt v0, v1, :cond_d

    const/4 v3, 0x1

    :goto_3
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, p5

    invoke-direct {v4, v0, v1, v3, v2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;-><init>(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;Ljava/util/List;ZI)V

    return-object v4

    .restart local v5    # "width":I
    .restart local v9    # "isFirstInRow":Z
    .restart local v16    # "numBlocksAdded":I
    .restart local v21    # "row":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/Block;>;"
    .restart local v23    # "tallestElement":I
    :cond_2
    move/from16 v6, v23

    .line 985
    goto :goto_2

    .line 993
    .restart local v6    # "rowHeight":I
    .restart local v10    # "block":Lcom/google/android/finsky/widget/consumption/Block;
    :cond_3
    add-int/lit8 v16, v16, 0x1

    .line 995
    invoke-virtual {v10}, Lcom/google/android/finsky/widget/consumption/Block;->getNumImages()I

    move-result v3

    add-int v17, v17, v3

    .line 997
    move-object/from16 v0, v21

    invoke-interface {v0, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 999
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/widget/consumption/Block;->getWidth(Landroid/content/Context;)I

    move-result v3

    sub-int/2addr v5, v3

    .line 1001
    move-object/from16 v0, p1

    invoke-virtual {v10, v0}, Lcom/google/android/finsky/widget/consumption/Block;->getHeight(Landroid/content/Context;)I

    move-result v3

    move/from16 v0, v23

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v23

    .line 1002
    const/4 v9, 0x0

    .line 1003
    goto :goto_1

    .line 1011
    .end local v6    # "rowHeight":I
    .end local v10    # "block":Lcom/google/android/finsky/widget/consumption/Block;
    :cond_4
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v18

    .line 1012
    .local v18, "replaceable":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/finsky/widget/consumption/Block;Ljava/lang/Integer;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/widget/consumption/Block;

    .line 1013
    .restart local v10    # "block":Lcom/google/android/finsky/widget/consumption/Block;
    invoke-virtual {v10}, Lcom/google/android/finsky/widget/consumption/Block;->hasLastOccurenceInRowReplacement()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1014
    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 1016
    .local v11, "currCount":I
    :goto_5
    add-int/lit8 v3, v11, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v0, v10, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1014
    .end local v11    # "currCount":I
    :cond_6
    const/4 v11, 0x0

    goto :goto_5

    .line 1020
    .end local v10    # "block":Lcom/google/android/finsky/widget/consumption/Block;
    :cond_7
    invoke-interface/range {v18 .. v18}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_8
    :goto_6
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/Map$Entry;

    .line 1022
    .local v19, "replaceableEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/finsky/widget/consumption/Block;Ljava/lang/Integer;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_8

    .line 1026
    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/google/android/finsky/widget/consumption/Block;

    .line 1027
    .local v24, "toReplace":Lcom/google/android/finsky/widget/consumption/Block;
    const/4 v15, -0x1

    .line 1028
    .local v15, "lastOccurenceIndex":I
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v12, v3, -0x1

    .local v12, "i":I
    :goto_7
    if-ltz v12, :cond_9

    .line 1029
    move-object/from16 v0, v21

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v0, v24

    if-ne v3, v0, :cond_b

    .line 1030
    move v15, v12

    .line 1034
    :cond_9
    invoke-virtual/range {v24 .. v24}, Lcom/google/android/finsky/widget/consumption/Block;->getLastOccurenceInRowReplacement()Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v20

    .line 1035
    .local v20, "replacement":Lcom/google/android/finsky/widget/consumption/Block;
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/widget/consumption/Block;->getNumImages()I

    move-result v3

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/finsky/widget/consumption/Block;->getNumImages()I

    move-result v4

    sub-int v14, v3, v4

    .line 1036
    .local v14, "imageCountDifference":I
    if-lez v14, :cond_a

    .line 1038
    sub-int v3, p2, v17

    if-lt v3, v14, :cond_8

    .line 1044
    :cond_a
    move-object/from16 v0, v21

    invoke-interface {v0, v15}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1046
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-interface {v0, v15, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1048
    add-int v17, v17, v14

    .line 1049
    goto :goto_6

    .line 1028
    .end local v14    # "imageCountDifference":I
    .end local v20    # "replacement":Lcom/google/android/finsky/widget/consumption/Block;
    :cond_b
    add-int/lit8 v12, v12, -0x1

    goto :goto_7

    .line 1051
    .end local v12    # "i":I
    .end local v15    # "lastOccurenceIndex":I
    .end local v19    # "replaceableEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/finsky/widget/consumption/Block;Ljava/lang/Integer;>;"
    .end local v24    # "toReplace":Lcom/google/android/finsky/widget/consumption/Block;
    :cond_c
    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1052
    sub-int p5, p5, v23

    .line 1053
    goto/16 :goto_0

    .line 1101
    .end local v5    # "width":I
    .end local v9    # "isFirstInRow":Z
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v16    # "numBlocksAdded":I
    .end local v18    # "replaceable":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/finsky/widget/consumption/Block;Ljava/lang/Integer;>;"
    .end local v21    # "row":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/Block;>;"
    .end local v23    # "tallestElement":I
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_3
.end method

.method private static getBackends(I)[I
    .locals 2
    .param p0, "backend"    # I

    .prologue
    .line 1310
    if-nez p0, :cond_0

    .line 1311
    sget-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->BACKENDS:[I

    .line 1313
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p0, v0, v1

    goto :goto_0
.end method

.method private static getBackgroundRes(I)I
    .locals 1
    .param p0, "backend"    # I

    .prologue
    .line 1211
    packed-switch p0, :pswitch_data_0

    .line 1224
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1213
    :pswitch_1
    const v0, 0x7f020045

    goto :goto_0

    .line 1215
    :pswitch_2
    const v0, 0x7f020046

    goto :goto_0

    .line 1217
    :pswitch_3
    const v0, 0x7f020047

    goto :goto_0

    .line 1219
    :pswitch_4
    const v0, 0x7f020048

    goto :goto_0

    .line 1221
    :pswitch_5
    const v0, 0x7f020049

    goto :goto_0

    .line 1211
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private static getBlocks(I)[Lcom/google/android/finsky/widget/consumption/Block;
    .locals 2
    .param p0, "listBackend"    # I

    .prologue
    .line 1176
    packed-switch p0, :pswitch_data_0

    .line 1185
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid backend"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1180
    :pswitch_1
    sget-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->PORTRAIT_BLOCKS:[Lcom/google/android/finsky/widget/consumption/Block;

    .line 1183
    :goto_0
    return-object v0

    :pswitch_2
    sget-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->SQUARE_BLOCKS:[Lcom/google/android/finsky/widget/consumption/Block;

    goto :goto_0

    .line 1176
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static getCachedImage(Landroid/content/Context;Ljava/util/Map;Landroid/net/Uri;II)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "uriToFind"    # Landroid/net/Uri;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/net/Uri;",
            "II)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 947
    .local p1, "cachedImages":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;Landroid/graphics/Bitmap;>;"
    invoke-static {p0, p3}, Lcom/google/android/finsky/widget/WidgetUtils;->getDips(Landroid/content/Context;I)I

    move-result v2

    .line 948
    .local v2, "targetWidth":I
    invoke-static {p0, p4}, Lcom/google/android/finsky/widget/WidgetUtils;->getDips(Landroid/content/Context;I)I

    move-result v1

    .line 949
    .local v1, "targetHeight":I
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;

    .line 950
    .local v3, "wrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    invoke-virtual {v3, p2, v2, v1}, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;->satisfies(Landroid/net/Uri;II)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 951
    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    .line 954
    .end local v3    # "wrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    :goto_0
    return-object v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static getConsumptionData(I)Ljava/util/List;
    .locals 1
    .param p0, "backend"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/services/ConsumptionAppDoc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1317
    invoke-static {}, Lcom/google/android/finsky/services/ConsumptionAppDataCache;->get()Lcom/google/android/finsky/services/ConsumptionAppDataCache;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/services/ConsumptionAppDataCache;->getConsumptionAppData(I)Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getDocsWithImages(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static getConsumptionDocLists(I)Ljava/util/List;
    .locals 7
    .param p0, "widgetBackend"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1297
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 1298
    .local v0, "allDocs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;>;"
    invoke-static {p0}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getBackends(I)[I

    move-result-object v1

    .local v1, "arr$":[I
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget v5, v1, v3

    .line 1299
    .local v5, "listBackend":I
    invoke-static {v5}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getConsumptionData(I)Ljava/util/List;

    move-result-object v2

    .line 1300
    .local v2, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/services/ConsumptionAppDoc;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1301
    new-instance v6, Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;

    invoke-direct {v6, v5, v2}, Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;-><init>(ILjava/util/List;)V

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1298
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1305
    .end local v2    # "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/services/ConsumptionAppDoc;>;"
    .end local v5    # "listBackend":I
    :cond_1
    sget-object v6, Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;->NEWEST_FIRST:Ljava/util/Comparator;

    invoke-static {v0, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1306
    return-object v0
.end method

.method private static getDocsWithImages(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/services/ConsumptionAppDoc;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/services/ConsumptionAppDoc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1362
    .local p0, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/services/ConsumptionAppDoc;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 1363
    .local v2, "output":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/services/ConsumptionAppDoc;>;"
    if-eqz p0, :cond_1

    .line 1364
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/services/ConsumptionAppDoc;

    .line 1365
    .local v0, "doc":Lcom/google/android/finsky/services/ConsumptionAppDoc;
    invoke-virtual {v0}, Lcom/google/android/finsky/services/ConsumptionAppDoc;->getImageUri()Landroid/net/Uri;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1366
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1368
    :cond_0
    const-string v3, "filtering out docId=[%s] because uri was null"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/google/android/finsky/services/ConsumptionAppDoc;->getDocId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1372
    .end local v0    # "doc":Lcom/google/android/finsky/services/ConsumptionAppDoc;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    return-object v2
.end method

.method private static getEmptyBackgroundRes(I)I
    .locals 1
    .param p0, "backend"    # I

    .prologue
    .line 1229
    packed-switch p0, :pswitch_data_0

    .line 1242
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1231
    :pswitch_1
    const v0, 0x7f02003e

    goto :goto_0

    .line 1233
    :pswitch_2
    const v0, 0x7f020043

    goto :goto_0

    .line 1235
    :pswitch_3
    const v0, 0x7f02003f

    goto :goto_0

    .line 1237
    :pswitch_4
    const v0, 0x7f020040

    goto :goto_0

    .line 1239
    :pswitch_5
    const v0, 0x7f020041

    goto :goto_0

    .line 1229
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private static getFallbackTitleRes(I)I
    .locals 1
    .param p0, "backend"    # I

    .prologue
    .line 1265
    packed-switch p0, :pswitch_data_0

    .line 1275
    :pswitch_0
    const v0, 0x7f0c033d

    :goto_0
    return v0

    .line 1267
    :pswitch_1
    const v0, 0x7f0c033e

    goto :goto_0

    .line 1269
    :pswitch_2
    const v0, 0x7f0c033f

    goto :goto_0

    .line 1271
    :pswitch_3
    const v0, 0x7f0c0340

    goto :goto_0

    .line 1273
    :pswitch_4
    const v0, 0x7f0c0341

    goto :goto_0

    .line 1265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private static getHeaderIconRes(I)I
    .locals 2
    .param p0, "backend"    # I

    .prologue
    .line 1280
    packed-switch p0, :pswitch_data_0

    .line 1293
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid backend"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1282
    :pswitch_1
    const v0, 0x7f020126

    .line 1291
    :goto_0
    return v0

    .line 1284
    :pswitch_2
    const v0, 0x7f020128

    goto :goto_0

    .line 1286
    :pswitch_3
    const v0, 0x7f020129

    goto :goto_0

    .line 1288
    :pswitch_4
    const v0, 0x7f02012a

    goto :goto_0

    .line 1291
    :pswitch_5
    const v0, 0x7f02012b

    goto :goto_0

    .line 1280
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private static getHeaderIntent(Landroid/content/Context;II)Landroid/content/Intent;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backend"    # I
    .param p2, "appWidgetId"    # I

    .prologue
    .line 1192
    packed-switch p1, :pswitch_data_0

    .line 1204
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1198
    :pswitch_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/google/android/finsky/utils/IntentUtils;->buildConsumptionAppLaunchIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 1201
    :pswitch_2
    const-class v0, Lcom/google/android/finsky/widget/consumption/MyLibraryTrampoline;

    invoke-static {p0, v0, p2}, Lcom/google/android/finsky/widget/TrampolineActivity;->getLaunchIntent(Landroid/content/Context;Ljava/lang/Class;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 1192
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static getImageLoader(Landroid/content/Context;)Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1352
    sget-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->mImageLoader:Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;

    if-nez v0, :cond_0

    .line 1353
    new-instance v0, Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getBitmapCache()Lcom/android/volley/Cache;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;-><init>(Landroid/content/Context;Lcom/android/volley/Cache;)V

    sput-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->mImageLoader:Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;

    .line 1355
    :cond_0
    sget-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->mImageLoader:Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;

    return-object v0
.end method

.method private static getLayout(Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;)I
    .locals 3
    .param p0, "arrangement"    # Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;

    .prologue
    .line 1154
    iget-object v0, p0, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;->quadrantToData:[Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;

    array-length v0, v0

    packed-switch v0, :pswitch_data_0

    .line 1172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;->quadrantToData:[Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1158
    :pswitch_0
    const/4 v0, 0x0

    .line 1170
    :goto_0
    return v0

    .line 1161
    :pswitch_1
    const v0, 0x7f0400ff

    goto :goto_0

    .line 1164
    :pswitch_2
    iget v0, p0, Lcom/google/android/finsky/widget/consumption/NowPlayingArranger$Arrangement;->layoutVariant:I

    if-nez v0, :cond_0

    .line 1165
    const v0, 0x7f0400fd

    goto :goto_0

    .line 1167
    :cond_0
    const v0, 0x7f0400fe

    goto :goto_0

    .line 1170
    :pswitch_3
    const v0, 0x7f0400f9

    goto :goto_0

    .line 1154
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getTitleRes(Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backend"    # I

    .prologue
    .line 1247
    packed-switch p2, :pswitch_data_0

    .line 1253
    :pswitch_0
    if-eqz p0, :cond_0

    invoke-virtual {p0, p2}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1254
    invoke-virtual {p0, p2}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->libraryName:Ljava/lang/String;

    .line 1256
    :goto_0
    return-object v0

    .line 1249
    :pswitch_1
    const v0, 0x7f0c033d

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1251
    :pswitch_2
    const v0, 0x7f0c0342

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1256
    :cond_0
    invoke-static {p2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getFallbackTitleRes(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1247
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private isConsumptionAppDisabled(Landroid/content/Context;I)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backendId"    # I

    .prologue
    const/4 v2, 0x0

    .line 267
    invoke-static {p2}, Lcom/google/android/finsky/utils/IntentUtils;->getPackageName(I)Ljava/lang/String;

    move-result-object v0

    .line 268
    .local v0, "packageName":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 272
    :cond_0
    :goto_0
    return v2

    .line 271
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v1

    .line 272
    .local v1, "state":I
    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private mergePortAndLandRequests(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;",
            ">;"
        }
    .end annotation

    .prologue
    .line 300
    .local p1, "port":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    .local p2, "land":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 303
    .local v5, "output":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    if-eqz p2, :cond_8

    if-eqz p1, :cond_8

    .line 304
    move-object v4, p2

    .line 305
    .local v4, "largerSet":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    move-object v8, p1

    .line 306
    .local v8, "smallerSet":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v9

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    if-ge v9, v10, :cond_0

    .line 307
    move-object v4, p1

    .line 308
    move-object v8, p2

    .line 310
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;

    .line 311
    .local v3, "largeWrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    const/4 v0, 0x0

    .line 312
    .local v0, "found":Z
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;

    .line 313
    .local v7, "smallWrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    iget-object v9, v3, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;->uri:Landroid/net/Uri;

    iget-object v10, v7, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;->uri:Landroid/net/Uri;

    invoke-virtual {v9, v10}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 314
    invoke-static {v7, v3}, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;->merge(Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;)Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    const/4 v0, 0x1

    .line 319
    .end local v7    # "smallWrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    :cond_3
    if-nez v0, :cond_1

    .line 320
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 324
    .end local v0    # "found":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "largeWrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    :cond_4
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;

    .line 325
    .restart local v7    # "smallWrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    const/4 v0, 0x0

    .line 326
    .restart local v0    # "found":Z
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;

    .line 327
    .local v6, "outputWrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    iget-object v9, v6, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;->uri:Landroid/net/Uri;

    iget-object v10, v7, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;->uri:Landroid/net/Uri;

    invoke-virtual {v9, v10}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 328
    const/4 v0, 0x1

    .line 332
    .end local v6    # "outputWrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    :cond_7
    if-nez v0, :cond_5

    .line 333
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 337
    .end local v0    # "found":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "largerSet":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    .end local v7    # "smallWrapper":Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;
    .end local v8    # "smallerSet":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    :cond_8
    return-object v5
.end method

.method private populateWidget(Landroid/content/Context;Landroid/widget/RemoteViews;IIILjava/util/List;Ljava/util/List;Ljava/util/Map;)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    .locals 33
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widget"    # Landroid/widget/RemoteViews;
    .param p3, "appWidgetId"    # I
    .param p4, "containerId"    # I
    .param p5, "location"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/RemoteViews;",
            "III",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/widget/consumption/Block;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/services/ConsumptionAppDoc;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;"
        }
    .end annotation

    .prologue
    .line 855
    .local p6, "rows":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Lcom/google/android/finsky/widget/consumption/Block;>;>;"
    .local p7, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/services/ConsumptionAppDoc;>;"
    .local p8, "cachedImages":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 856
    .local v19, "res":Landroid/content/res/Resources;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v28

    .line 857
    .local v28, "urisToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    const/16 v25, 0x0

    .line 859
    .local v25, "shouldAsyncLoadImages":Z
    const/16 v26, 0x0

    .line 860
    .local v26, "shownMetadata":Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v17

    .line 861
    .local v17, "packageName":Ljava/lang/String;
    move-object/from16 v0, p2

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 863
    new-instance v27, Lcom/google/android/finsky/widget/consumption/NowPlayingCellSorter;

    invoke-direct/range {v27 .. v27}, Lcom/google/android/finsky/widget/consumption/NowPlayingCellSorter;-><init>()V

    .line 864
    .local v27, "sorter":Lcom/google/android/finsky/widget/consumption/NowPlayingCellSorter;
    move-object/from16 v0, v27

    move-object/from16 v1, p6

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/widget/consumption/NowPlayingCellSorter;->sort(Ljava/util/List;Landroid/content/res/Resources;)V

    .line 867
    const/16 v22, 0x0

    .local v22, "rowIndex":I
    :goto_0
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    move-result v30

    move/from16 v0, v22

    move/from16 v1, v30

    if-ge v0, v1, :cond_f

    .line 868
    move-object/from16 v0, p6

    move/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/List;

    .line 870
    .local v20, "row":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/Block;>;"
    new-instance v24, Landroid/widget/RemoteViews;

    const v30, 0x7f0400f7

    move-object/from16 v0, v24

    move-object/from16 v1, v17

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 871
    .local v24, "rowLayout":Landroid/widget/RemoteViews;
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v23

    .line 872
    .local v23, "rowItems":Ljava/util/List;, "Ljava/util/List<Landroid/widget/RemoteViews;>;"
    const/4 v7, 0x0

    .local v7, "blockIndex":I
    :goto_1
    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v30

    move/from16 v0, v30

    if-ge v7, v0, :cond_7

    .line 873
    move-object/from16 v0, v20

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/widget/consumption/Block;

    .line 875
    .local v6, "block":Lcom/google/android/finsky/widget/consumption/Block;
    new-instance v16, Landroid/widget/RemoteViews;

    invoke-virtual {v6}, Lcom/google/android/finsky/widget/consumption/Block;->getLayoutId()I

    move-result v30

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    move/from16 v2, v30

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 877
    .local v16, "item":Landroid/widget/RemoteViews;
    sget-object v30, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->IMAGE_IDS:[I

    move-object/from16 v0, v30

    array-length v0, v0

    move/from16 v30, v0

    invoke-virtual {v6}, Lcom/google/android/finsky/widget/consumption/Block;->getNumImages()I

    move-result v31

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 878
    .local v5, "N":I
    const/4 v12, 0x0

    .local v12, "i":I
    :goto_2
    if-ge v12, v5, :cond_6

    .line 880
    move-object/from16 v0, v27

    move/from16 v1, v22

    invoke-virtual {v0, v1, v7, v12}, Lcom/google/android/finsky/widget/consumption/NowPlayingCellSorter;->getSortedIndex(III)I

    move-result v10

    .line 881
    .local v10, "docIndex":I
    if-ltz v10, :cond_0

    invoke-interface/range {p7 .. p7}, Ljava/util/List;->size()I

    move-result v30

    move/from16 v0, v30

    if-lt v10, v0, :cond_1

    .line 878
    :cond_0
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 884
    :cond_1
    move-object/from16 v0, p7

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/finsky/services/ConsumptionAppDoc;

    .line 885
    .local v9, "doc":Lcom/google/android/finsky/services/ConsumptionAppDoc;
    if-eqz p8, :cond_4

    .line 886
    invoke-virtual {v9}, Lcom/google/android/finsky/services/ConsumptionAppDoc;->getImageUri()Landroid/net/Uri;

    move-result-object v30

    invoke-virtual {v6, v12}, Lcom/google/android/finsky/widget/consumption/Block;->getImageWidthRes(I)I

    move-result v31

    invoke-virtual {v6, v12}, Lcom/google/android/finsky/widget/consumption/Block;->getImageHeightRes(I)I

    move-result v32

    move-object/from16 v0, p1

    move-object/from16 v1, p8

    move-object/from16 v2, v30

    move/from16 v3, v31

    move/from16 v4, v32

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getCachedImage(Landroid/content/Context;Ljava/util/Map;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 889
    .local v8, "cachedBitmap":Landroid/graphics/Bitmap;
    sget-object v30, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->IMAGE_IDS:[I

    aget v30, v30, v12

    move-object/from16 v0, v16

    move/from16 v1, v30

    invoke-virtual {v0, v1, v8}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    .line 890
    if-nez v8, :cond_2

    .line 891
    const/16 v25, 0x1

    .line 896
    .end local v8    # "cachedBitmap":Landroid/graphics/Bitmap;
    :cond_2
    :goto_4
    invoke-virtual {v6, v12}, Lcom/google/android/finsky/widget/consumption/Block;->getImageWidthRes(I)I

    move-result v30

    move-object/from16 v0, v19

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v29

    .line 897
    .local v29, "widthDp":I
    invoke-virtual {v6, v12}, Lcom/google/android/finsky/widget/consumption/Block;->getImageHeightRes(I)I

    move-result v30

    move-object/from16 v0, v19

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 898
    .local v11, "heightDp":I
    new-instance v30, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;

    invoke-virtual {v9}, Lcom/google/android/finsky/services/ConsumptionAppDoc;->getImageUri()Landroid/net/Uri;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    move/from16 v2, v29

    invoke-direct {v0, v1, v2, v11}, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;-><init>(Landroid/net/Uri;II)V

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 899
    invoke-virtual {v6}, Lcom/google/android/finsky/widget/consumption/Block;->supportsMetadata()Z

    move-result v30

    if-eqz v30, :cond_3

    .line 900
    invoke-virtual {v9}, Lcom/google/android/finsky/services/ConsumptionAppDoc;->getReason()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-nez v30, :cond_5

    if-nez v26, :cond_5

    .line 901
    const v30, 0x7f0a0299

    invoke-virtual {v9}, Lcom/google/android/finsky/services/ConsumptionAppDoc;->getReason()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v16

    move/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 902
    const v30, 0x7f0a0299

    const/16 v31, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 903
    const/16 v26, 0x1

    .line 909
    :cond_3
    :goto_5
    invoke-virtual {v9}, Lcom/google/android/finsky/services/ConsumptionAppDoc;->getViewIntent()Landroid/content/Intent;

    move-result-object v30

    const/16 v31, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-static {v0, v12, v1, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v18

    .line 911
    .local v18, "pendingIntent":Landroid/app/PendingIntent;
    sget-object v30, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->ACCESSIBILITY_OVERLAY_IDS:[I

    aget v30, v30, v12

    move-object/from16 v0, v16

    move/from16 v1, v30

    move-object/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    goto/16 :goto_3

    .line 894
    .end local v11    # "heightDp":I
    .end local v18    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v29    # "widthDp":I
    :cond_4
    const/16 v25, 0x1

    goto/16 :goto_4

    .line 905
    .restart local v11    # "heightDp":I
    .restart local v29    # "widthDp":I
    :cond_5
    const v30, 0x7f0a0299

    const/16 v31, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_5

    .line 913
    .end local v9    # "doc":Lcom/google/android/finsky/services/ConsumptionAppDoc;
    .end local v10    # "docIndex":I
    .end local v11    # "heightDp":I
    .end local v29    # "widthDp":I
    :cond_6
    move-object/from16 v0, v23

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 872
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 918
    .end local v5    # "N":I
    .end local v6    # "block":Lcom/google/android/finsky/widget/consumption/Block;
    .end local v12    # "i":I
    .end local v16    # "item":Landroid/widget/RemoteViews;
    :cond_7
    and-int/lit8 v30, p5, 0x1

    if-eqz v30, :cond_a

    const/4 v14, 0x1

    .line 919
    .local v14, "isInLeftHalf":Z
    :goto_6
    and-int/lit8 v30, p5, 0x2

    if-eqz v30, :cond_b

    const/4 v15, 0x1

    .line 920
    .local v15, "isInRightHalf":Z
    :goto_7
    const/16 v21, 0x1

    .line 921
    .local v21, "rowGravity":I
    if-eqz v14, :cond_c

    .line 922
    const/16 v21, 0x5

    .line 926
    :cond_8
    :goto_8
    rem-int/lit8 v30, v22, 0x2

    if-nez v30, :cond_d

    .line 927
    or-int/lit8 v21, v21, 0x50

    .line 931
    :goto_9
    const v30, 0x7f0a0289

    const-string v31, "setGravity"

    move-object/from16 v0, v24

    move/from16 v1, v30

    move-object/from16 v2, v31

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 933
    if-eqz v14, :cond_9

    .line 935
    invoke-static/range {v23 .. v23}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 937
    :cond_9
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_a
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v30

    if-eqz v30, :cond_e

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/widget/RemoteViews;

    .line 938
    .restart local v16    # "item":Landroid/widget/RemoteViews;
    const v30, 0x7f0a0289

    move-object/from16 v0, v24

    move/from16 v1, v30

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    goto :goto_a

    .line 918
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "isInLeftHalf":Z
    .end local v15    # "isInRightHalf":Z
    .end local v16    # "item":Landroid/widget/RemoteViews;
    .end local v21    # "rowGravity":I
    :cond_a
    const/4 v14, 0x0

    goto :goto_6

    .line 919
    .restart local v14    # "isInLeftHalf":Z
    :cond_b
    const/4 v15, 0x0

    goto :goto_7

    .line 923
    .restart local v15    # "isInRightHalf":Z
    .restart local v21    # "rowGravity":I
    :cond_c
    if-eqz v15, :cond_8

    .line 924
    const/16 v21, 0x3

    goto :goto_8

    .line 929
    :cond_d
    or-int/lit8 v21, v21, 0x30

    goto :goto_9

    .line 940
    .restart local v13    # "i$":Ljava/util/Iterator;
    :cond_e
    move-object/from16 v0, p2

    move/from16 v1, p4

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    .line 867
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_0

    .line 942
    .end local v7    # "blockIndex":I
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "isInLeftHalf":Z
    .end local v15    # "isInRightHalf":Z
    .end local v20    # "row":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/Block;>;"
    .end local v21    # "rowGravity":I
    .end local v23    # "rowItems":Ljava/util/List;, "Ljava/util/List<Landroid/widget/RemoteViews;>;"
    .end local v24    # "rowLayout":Landroid/widget/RemoteViews;
    :cond_f
    new-instance v31, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;

    const/16 v32, 0x0

    if-nez v25, :cond_10

    const/16 v30, 0x1

    :goto_b
    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v32

    move/from16 v3, v30

    move-object/from16 v4, v28

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;-><init>(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;Landroid/widget/RemoteViews;ZLjava/util/List;)V

    return-object v31

    :cond_10
    const/16 v30, 0x0

    goto :goto_b
.end method

.method private updateHotseat(Landroid/content/Context;Lcom/google/android/finsky/api/model/DfeToc;Landroid/widget/RemoteViews;III)V
    .locals 23
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p3, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p4, "appWidgetId"    # I
    .param p5, "areaWidth"    # I
    .param p6, "areaHeight"    # I

    .prologue
    .line 711
    const v19, 0x7f0a0291

    const/16 v20, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 714
    if-nez p2, :cond_1

    .line 849
    :cond_0
    :goto_0
    return-void

    .line 718
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 721
    .local v15, "res":Landroid/content/res/Resources;
    const v19, 0x7f0b0158

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/finsky/widget/WidgetUtils;->getDips(Landroid/content/Context;I)I

    move-result v19

    move/from16 v0, p6

    move/from16 v1, v19

    if-le v0, v1, :cond_0

    const v19, 0x7f0b0159

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/finsky/widget/WidgetUtils;->getDips(Landroid/content/Context;I)I

    move-result v19

    move/from16 v0, p5

    move/from16 v1, v19

    if-le v0, v1, :cond_0

    .line 726
    invoke-static/range {p4 .. p4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;->loadDataFromDisk(I)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;

    move-result-object v18

    .line 729
    .local v18, "wrapper":Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;->mHasDismissedHotseat:Z
    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;->access$600(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 730
    const-string v19, "Hiding hotseat because the user had dismissed it for %d"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 735
    :cond_2
    const v19, 0x7f0b015b

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-static {v0, v1}, Lcom/google/android/finsky/widget/WidgetUtils;->getDips(Landroid/content/Context;I)I

    move-result v19

    div-int v5, p5, v19

    .line 740
    .local v5, "corporaMaxCount":I
    invoke-static {}, Lcom/google/android/finsky/services/ConsumptionAppDataCache;->get()Lcom/google/android/finsky/services/ConsumptionAppDataCache;

    move-result-object v3

    .line 741
    .local v3, "appDataCache":Lcom/google/android/finsky/services/ConsumptionAppDataCache;
    const/4 v8, 0x0

    .line 742
    .local v8, "corporaWithSufficientlyOwnedDataCount":I
    const/4 v7, 0x0

    .line 743
    .local v7, "corporaWithLaunchedAppsCount":I
    const/4 v6, 0x0

    .line 744
    .local v6, "corporaTotalCount":I
    const/4 v12, 0x0

    .line 745
    .local v12, "isAnyContentOwned":Z
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    sget-object v19, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->sSupportedBackendIds:[I

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v10, v0, :cond_9

    .line 746
    sget-object v19, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->sSupportedBackendIds:[I

    aget v4, v19, v10

    .line 748
    .local v4, "backendId":I
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v9

    .line 749
    .local v9, "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    if-nez v9, :cond_4

    .line 745
    :cond_3
    :goto_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 754
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v0, v4}, Lcom/google/android/finsky/utils/IntentUtils;->isConsumptionAppInstalled(Landroid/content/pm/PackageManager;I)Z

    move-result v13

    .line 756
    .local v13, "isInstalled":Z
    if-eqz v13, :cond_3

    .line 761
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v0, v4}, Lcom/google/android/finsky/utils/IntentUtils;->isConsumptionAppDisabled(Lcom/google/android/finsky/appstate/PackageStateRepository;I)Z

    move-result v19

    if-nez v19, :cond_3

    .line 767
    add-int/lit8 v6, v6, 0x1

    .line 769
    new-instance v16, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v19

    const v20, 0x7f0400fb

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 771
    .local v16, "selector":Landroid/widget/RemoteViews;
    const v19, 0x7f0a0297

    invoke-static {v4}, Lcom/google/android/finsky/widget/WidgetUtils;->getHotseatCheckIcon(I)I

    move-result v20

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 773
    const v19, 0x7f0a0298

    iget-object v0, v9, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 774
    const v19, 0x7f0a0298

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v20

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 777
    invoke-virtual {v3, v4}, Lcom/google/android/finsky/services/ConsumptionAppDataCache;->getConsumptionAppDataSize(I)I

    move-result v14

    .line 779
    .local v14, "ownedDocumentCount":I
    if-lez v14, :cond_6

    const/16 v19, 0x1

    :goto_3
    or-int v12, v12, v19

    .line 781
    invoke-virtual {v3, v4}, Lcom/google/android/finsky/services/ConsumptionAppDataCache;->hasConsumptionAppData(I)Z

    move-result v19

    if-eqz v19, :cond_7

    invoke-static {v4}, Lcom/google/android/finsky/widget/WidgetUtils;->getAwarenessThreshold(I)I

    move-result v19

    move/from16 v0, v19

    if-lt v14, v0, :cond_7

    .line 784
    add-int/lit8 v8, v8, 0x1

    .line 785
    const v19, 0x7f0a0297

    const/16 v20, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 799
    :goto_4
    new-instance v11, Landroid/content/Intent;

    const-class v19, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 800
    .local v11, "intent":Landroid/content/Intent;
    const-string v19, "NowPlayingWidgetProvider.WarmWelcome"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 801
    const-string v19, "appWidgetId"

    move-object/from16 v0, v19

    move/from16 v1, p4

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 802
    const-string v19, "backendId"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 803
    const v19, 0x7f0a00b8

    sget-object v20, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->sRandom:Ljava/util/Random;

    invoke-virtual/range {v20 .. v20}, Ljava/util/Random;->nextInt()I

    move-result v20

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v1, v11, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v20

    move-object/from16 v0, v16

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 808
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v6, v0, :cond_5

    .line 809
    const v19, 0x7f0a0296

    const/16 v20, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 812
    :cond_5
    if-gt v6, v5, :cond_3

    .line 814
    const v19, 0x7f0a0295

    move-object/from16 v0, p3

    move/from16 v1, v19

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->addView(ILandroid/widget/RemoteViews;)V

    goto/16 :goto_2

    .line 779
    .end local v11    # "intent":Landroid/content/Intent;
    :cond_6
    const/16 v19, 0x0

    goto/16 :goto_3

    .line 786
    :cond_7
    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;->isBackendChecked(I)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 791
    add-int/lit8 v7, v7, 0x1

    .line 792
    const v19, 0x7f0a0297

    const/16 v20, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4

    .line 794
    :cond_8
    const v19, 0x7f0a0297

    const/16 v20, 0x8

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_4

    .line 823
    .end local v4    # "backendId":I
    .end local v9    # "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    .end local v13    # "isInstalled":Z
    .end local v14    # "ownedDocumentCount":I
    .end local v16    # "selector":Landroid/widget/RemoteViews;
    :cond_9
    if-gtz v8, :cond_a

    if-lez v7, :cond_b

    if-eqz v12, :cond_b

    :cond_a
    const/16 v17, 0x1

    .line 825
    .local v17, "shouldShowDoneButton":Z
    :goto_5
    if-eqz v17, :cond_c

    .line 826
    const v19, 0x7f0a0292

    const v20, 0x7f0c0384

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v20

    move-object/from16 v0, p3

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 828
    const v19, 0x7f0a0293

    const/16 v20, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 829
    const v19, 0x7f0a0294

    const/16 v20, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 832
    new-instance v11, Landroid/content/Intent;

    const-class v19, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 833
    .restart local v11    # "intent":Landroid/content/Intent;
    const-string v19, "NowPlayingWidgetProvider.DoneButton"

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 834
    const-string v19, "appWidgetId"

    move-object/from16 v0, v19

    move/from16 v1, p4

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 838
    const v19, 0x7f0a0294

    sget-object v20, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->sRandom:Ljava/util/Random;

    invoke-virtual/range {v20 .. v20}, Ljava/util/Random;->nextInt()I

    move-result v20

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-static {v0, v1, v11, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v20

    move-object/from16 v0, p3

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 848
    .end local v11    # "intent":Landroid/content/Intent;
    :goto_6
    const v19, 0x7f0a0291

    const/16 v20, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_0

    .line 823
    .end local v17    # "shouldShowDoneButton":Z
    :cond_b
    const/16 v17, 0x0

    goto/16 :goto_5

    .line 841
    .restart local v17    # "shouldShowDoneButton":Z
    :cond_c
    const v19, 0x7f0a0292

    const v20, 0x7f0c0384

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v20

    move-object/from16 v0, p3

    move/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 843
    const v19, 0x7f0a0293

    const/16 v20, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 844
    const v19, 0x7f0a0294

    const/16 v20, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_6
.end method

.method private useCustomStylingIfNecessary(Landroid/content/Context;Landroid/widget/RemoteViews;I)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widget"    # Landroid/widget/RemoteViews;
    .param p3, "backendId"    # I

    .prologue
    const v9, 0x7f0a00f4

    const v8, 0x7f09004c

    const v3, 0x7f020085

    const/4 v7, 0x4

    const/4 v2, 0x0

    .line 417
    sget-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->BG_IMAGE_OVERRIDE:Ljava/io/File;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->BG_IMAGE_OVERRIDE:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_1

    .line 441
    :cond_0
    :goto_0
    return v2

    .line 422
    :cond_1
    const v0, 0x7f0a0285

    invoke-virtual {p2, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 425
    const v1, 0x7f0a0287

    move-object v0, p2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    move-object v0, p2

    move v1, v9

    move v4, v2

    move v5, v2

    .line 427
    invoke-virtual/range {v0 .. v5}, Landroid/widget/RemoteViews;->setTextViewCompoundDrawables(IIIII)V

    .line 429
    const v0, 0x7f0a0282

    sget-object v1, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->BG_IMAGE_OVERRIDE:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setImageViewUri(ILandroid/net/Uri;)V

    .line 430
    const v0, 0x7f0a028c

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 431
    const v0, 0x7f0a028c

    invoke-virtual {p2, v0, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 433
    const v0, 0x7f0a0287

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 434
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v9, v0}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 436
    const v0, 0x7f0a0286

    const/16 v1, 0x8

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 438
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v0

    invoke-static {v0, p1, p3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getTitleRes(Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    .line 439
    .local v6, "title":Ljava/lang/String;
    const v0, 0x7f0a028b

    invoke-virtual {p2, v0, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 441
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected findLargestBlock(Landroid/content/Context;IIIIZ)Lcom/google/android/finsky/widget/consumption/Block;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "availableWidth"    # I
    .param p3, "availableHeight"    # I
    .param p4, "listBackend"    # I
    .param p5, "numImagesLeft"    # I
    .param p6, "isFirstInRow"    # Z

    .prologue
    .line 1116
    const/4 v5, 0x0

    .line 1117
    .local v5, "largest":Lcom/google/android/finsky/widget/consumption/Block;
    invoke-static/range {p4 .. p4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getBlocks(I)[Lcom/google/android/finsky/widget/consumption/Block;

    move-result-object v1

    .local v1, "arr$":[Lcom/google/android/finsky/widget/consumption/Block;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_5

    aget-object v2, v1, v4

    .line 1118
    .local v2, "block":Lcom/google/android/finsky/widget/consumption/Block;
    iget-object v9, p0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->mRowStartCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Lcom/google/android/finsky/widget/consumption/Block;->hashCode()I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    .line 1119
    .local v3, "currentRowStarts":I
    if-eqz p6, :cond_1

    invoke-virtual {v2}, Lcom/google/android/finsky/widget/consumption/Block;->hasMaxRowStartCount()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v2}, Lcom/google/android/finsky/widget/consumption/Block;->getMaxRowStartCount()I

    move-result v9

    if-lt v3, v9, :cond_1

    .line 1117
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1124
    :cond_1
    const/4 v7, 0x1

    .line 1125
    .local v7, "taller":Z
    const/4 v8, 0x1

    .line 1127
    .local v8, "wider":Z
    if-eqz v5, :cond_2

    .line 1128
    invoke-virtual {v2, p1}, Lcom/google/android/finsky/widget/consumption/Block;->getHeight(Landroid/content/Context;)I

    move-result v9

    invoke-virtual {v5, p1}, Lcom/google/android/finsky/widget/consumption/Block;->getHeight(Landroid/content/Context;)I

    move-result v10

    if-le v9, v10, :cond_3

    const/4 v7, 0x1

    .line 1129
    :goto_2
    invoke-virtual {v2, p1}, Lcom/google/android/finsky/widget/consumption/Block;->getWidth(Landroid/content/Context;)I

    move-result v9

    invoke-virtual {v5, p1}, Lcom/google/android/finsky/widget/consumption/Block;->getWidth(Landroid/content/Context;)I

    move-result v10

    if-lt v9, v10, :cond_4

    const/4 v8, 0x1

    .line 1132
    :cond_2
    :goto_3
    if-eqz v7, :cond_0

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/widget/consumption/Block;->getHeight(Landroid/content/Context;)I

    move-result v9

    if-gt v9, p3, :cond_0

    .line 1136
    if-eqz v8, :cond_0

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/widget/consumption/Block;->getWidth(Landroid/content/Context;)I

    move-result v9

    if-gt v9, p2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/finsky/widget/consumption/Block;->getNumImages()I

    move-result v9

    move/from16 v0, p5

    if-lt v0, v9, :cond_0

    .line 1138
    move-object v5, v2

    goto :goto_1

    .line 1128
    :cond_3
    const/4 v7, 0x0

    goto :goto_2

    .line 1129
    :cond_4
    const/4 v8, 0x0

    goto :goto_3

    .line 1143
    .end local v2    # "block":Lcom/google/android/finsky/widget/consumption/Block;
    .end local v3    # "currentRowStarts":I
    .end local v7    # "taller":Z
    .end local v8    # "wider":Z
    :cond_5
    if-eqz v5, :cond_6

    .line 1144
    if-eqz p6, :cond_6

    .line 1145
    iget-object v9, p0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->mRowStartCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v5}, Lcom/google/android/finsky/widget/consumption/Block;->hashCode()I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/util/SparseIntArray;->get(II)I

    move-result v3

    .line 1146
    .restart local v3    # "currentRowStarts":I
    iget-object v9, p0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->mRowStartCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v5}, Lcom/google/android/finsky/widget/consumption/Block;->hashCode()I

    move-result v10

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v9, v10, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 1150
    .end local v3    # "currentRowStarts":I
    :cond_6
    return-object v5
.end method

.method protected getWidgetClassId()I
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x3

    return v0
.end method

.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetId"    # I
    .param p4, "newOptions"    # Landroid/os/Bundle;

    .prologue
    .line 161
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p3, v0, v1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->updateWidgets(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 162
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 166
    move-object v1, p2

    .local v1, "arr$":[I
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget v0, v1, v2

    .line 167
    .local v0, "appWidgetId":I
    const-string v4, "Deleting widget data for widget ID=%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    sget-object v4, Lcom/google/android/finsky/utils/FinskyPreferences;->libraryWidgetData:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 166
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 170
    .end local v0    # "appWidgetId":I
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/widget/BaseWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 171
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 102
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/widget/BaseWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 104
    const-string v7, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 105
    const-string v7, "android.intent.extra.UID"

    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 106
    .local v6, "uid":I
    if-ne v6, v9, :cond_1

    .line 151
    .end local v6    # "uid":I
    :cond_0
    :goto_0
    return-void

    .line 110
    .restart local v6    # "uid":I
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 111
    .local v3, "packageName":Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/widget/WidgetTypeMap;->get(Landroid/content/Context;)Lcom/google/android/finsky/widget/WidgetTypeMap;

    move-result-object v5

    .line 112
    .local v5, "typeMap":Lcom/google/android/finsky/widget/WidgetTypeMap;
    invoke-static {v3}, Lcom/google/android/finsky/utils/IntentUtils;->getBackendId(Ljava/lang/String;)I

    move-result v2

    .line 114
    .local v2, "backendId":I
    if-eq v2, v9, :cond_0

    const/16 v7, 0x9

    if-eq v2, v7, :cond_0

    .line 118
    invoke-static {v2}, Lcom/google/android/finsky/widget/WidgetUtils;->translate(I)Ljava/lang/String;

    move-result-object v4

    .line 119
    .local v4, "type":Ljava/lang/String;
    const-class v7, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;

    invoke-virtual {v5, v7, v4}, Lcom/google/android/finsky/widget/WidgetTypeMap;->getWidgets(Ljava/lang/Class;Ljava/lang/String;)[I

    move-result-object v1

    .line 120
    .local v1, "appWidgetIds":[I
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v7

    invoke-virtual {p0, p1, v7, v1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->updateWidgets(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 121
    .end local v1    # "appWidgetIds":[I
    .end local v2    # "backendId":I
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "type":Ljava/lang/String;
    .end local v5    # "typeMap":Lcom/google/android/finsky/widget/WidgetTypeMap;
    .end local v6    # "uid":I
    :cond_2
    const-string v7, "NowPlayingWidgetProvider.DoneButton"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 123
    const-string v7, "appWidgetId"

    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 124
    .local v0, "appWidgetId":I
    if-eq v0, v9, :cond_3

    .line 125
    const-string v7, "Received ACTION_DONE_BUTTON, updating widget %d."

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 127
    invoke-static {v0}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;->writeHotseatDismissed(I)V

    .line 130
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v7

    new-array v8, v11, [I

    aput v0, v8, v10

    invoke-virtual {p0, p1, v7, v8}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->updateWidgets(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 132
    :cond_3
    const-string v7, "Received ACTION_DONE_BUTTON, but no appWidgetId was specified."

    new-array v8, v10, [Ljava/lang/Object;

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 134
    .end local v0    # "appWidgetId":I
    :cond_4
    const-string v7, "NowPlayingWidgetProvider.WarmWelcome"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 135
    const-string v7, "appWidgetId"

    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 136
    .restart local v0    # "appWidgetId":I
    const-string v7, "backendId"

    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 137
    .restart local v2    # "backendId":I
    if-eq v0, v9, :cond_0

    if-eq v2, v9, :cond_0

    .line 138
    const-string v7, "Received ACTION_LAUNCH_WARM_WELCOME for backend %d and widget %d"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    invoke-static {v0, v2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$HotseatDataHolder;->writeBackendChecked(II)V

    .line 144
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v2, v8}, Lcom/google/android/finsky/utils/IntentUtils;->buildConsumptionAppLaunchIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/FinskyApp;->startActivity(Landroid/content/Intent;)V

    .line 148
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v7

    new-array v8, v11, [I

    aput v0, v8, v10

    invoke-virtual {p0, p1, v7, v8}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->updateWidgets(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    goto/16 :goto_0
.end method

.method protected populateWidget(Landroid/widget/RemoteViews;IIILandroid/content/Context;Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;IILjava/util/Map;)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    .locals 11
    .param p1, "widget"    # Landroid/widget/RemoteViews;
    .param p2, "appWidgetId"    # I
    .param p3, "containerId"    # I
    .param p4, "location"    # I
    .param p5, "context"    # Landroid/content/Context;
    .param p6, "docList"    # Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;
    .param p7, "width"    # I
    .param p8, "height"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/RemoteViews;",
            "III",
            "Landroid/content/Context;",
            "Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;"
        }
    .end annotation

    .prologue
    .line 658
    .local p9, "cachedImages":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;Landroid/graphics/Bitmap;>;"
    iget-object v1, p0, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->mRowStartCounts:Landroid/util/SparseIntArray;

    invoke-virtual {v1}, Landroid/util/SparseIntArray;->clear()V

    .line 660
    new-instance v7, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;

    const/4 v1, 0x0

    const/4 v2, 0x1

    move/from16 v0, p8

    invoke-direct {v7, p0, v1, v2, v0}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;-><init>(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;Ljava/util/List;ZI)V

    .line 663
    .local v7, "rows":Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;->size()I

    move-result v3

    invoke-virtual/range {p6 .. p6}, Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;->getBackend()I

    move-result v4

    move-object v1, p0

    move-object/from16 v2, p5

    move/from16 v5, p7

    move/from16 v6, p8

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateWidgetLayout(Landroid/content/Context;IIII)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;

    move-result-object v7

    move-object v1, p0

    move-object/from16 v2, p5

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p9

    .line 666
    invoke-direct/range {v1 .. v9}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->populateWidget(Landroid/content/Context;Landroid/widget/RemoteViews;IIILjava/util/List;Ljava/util/List;Ljava/util/Map;)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;

    move-result-object v10

    .line 668
    .local v10, "viewTree":Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    iget-boolean v1, v7, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;->showBackground:Z

    # setter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showBackground:Z
    invoke-static {v10, v1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$302(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;Z)Z

    .line 669
    invoke-virtual/range {p6 .. p6}, Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;->isEmpty()Z

    move-result v1

    # setter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->showEmptyBackground:Z
    invoke-static {v10, v1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$402(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;Z)Z

    .line 670
    iget v1, v7, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$WidgetLayout;->heightRemaining:I

    # setter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->heightRemaining:I
    invoke-static {v10, v1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$502(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;I)I

    .line 672
    return-object v10
.end method

.method protected varargs updateWidgets(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;Ljava/util/Map;[I)V
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/appwidget/AppWidgetManager;",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;",
            "Landroid/graphics/Bitmap;",
            ">;[I)V"
        }
    .end annotation

    .prologue
    .line 182
    .local p3, "cachedImages":Ljava/util/Map;, "Ljava/util/Map<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;Landroid/graphics/Bitmap;>;"
    if-nez p4, :cond_1

    .line 264
    :cond_0
    return-void

    .line 186
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/widget/WidgetTypeMap;->get(Landroid/content/Context;)Lcom/google/android/finsky/widget/WidgetTypeMap;

    move-result-object v27

    .line 188
    .local v27, "typeMap":Lcom/google/android/finsky/widget/WidgetTypeMap;
    const v2, 0x7f0b01b7

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/finsky/widget/WidgetUtils;->getDips(Landroid/content/Context;I)I

    move-result v26

    .line 190
    .local v26, "titleHeight":I
    move-object/from16 v18, p4

    .local v18, "arr$":[I
    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v21, 0x0

    .local v21, "i$":I
    :goto_0
    move/from16 v0, v21

    move/from16 v1, v24

    if-ge v0, v1, :cond_0

    aget v4, v18, v21

    .line 191
    .local v4, "appWidgetId":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v2

    if-nez v2, :cond_2

    .line 193
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateAccountNeededState(Landroid/content/Context;II)Landroid/widget/RemoteViews;

    move-result-object v28

    .line 195
    .local v28, "views":Landroid/widget/RemoteViews;
    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 190
    .end local v28    # "views":Landroid/widget/RemoteViews;
    :goto_1
    add-int/lit8 v21, v21, 0x1

    goto :goto_0

    .line 199
    :cond_2
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v4}, Lcom/google/android/finsky/widget/WidgetTypeMap;->get(Ljava/lang/Class;I)Ljava/lang/String;

    move-result-object v19

    .line 200
    .local v19, "backendType":Ljava/lang/String;
    if-nez v19, :cond_3

    .line 202
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateConfigurationState(Landroid/content/Context;II)Landroid/widget/RemoteViews;

    move-result-object v28

    .line 204
    .restart local v28    # "views":Landroid/widget/RemoteViews;
    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_1

    .line 208
    .end local v28    # "views":Landroid/widget/RemoteViews;
    :cond_3
    invoke-static/range {v19 .. v19}, Lcom/google/android/finsky/widget/WidgetUtils;->translate(Ljava/lang/String;)I

    move-result v3

    .line 210
    .local v3, "backend":I
    if-eqz v3, :cond_4

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/IntentUtils;->isConsumptionAppInstalled(Landroid/content/pm/PackageManager;I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 212
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateUnavailableState(Landroid/content/Context;II)Landroid/widget/RemoteViews;

    move-result-object v28

    .line 213
    .restart local v28    # "views":Landroid/widget/RemoteViews;
    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_1

    .line 217
    .end local v28    # "views":Landroid/widget/RemoteViews;
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->isConsumptionAppDisabled(Landroid/content/Context;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 218
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v4, v3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateDisabledState(Landroid/content/Context;II)Landroid/widget/RemoteViews;

    move-result-object v28

    .line 219
    .restart local v28    # "views":Landroid/widget/RemoteViews;
    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto :goto_1

    .line 223
    .end local v28    # "views":Landroid/widget/RemoteViews;
    :cond_5
    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getBoundingBoxes(Landroid/content/Context;I)[I

    move-result-object v20

    .line 225
    .local v20, "dimens":[I
    const/4 v2, 0x0

    aget v2, v20, v2

    if-nez v2, :cond_7

    const/4 v2, 0x1

    aget v2, v20, v2

    if-nez v2, :cond_7

    const/4 v2, 0x2

    aget v2, v20, v2

    if-nez v2, :cond_7

    const/4 v2, 0x3

    aget v2, v20, v2

    if-nez v2, :cond_7

    .line 226
    if-nez v3, :cond_6

    .line 228
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->warmImageCache(Landroid/content/Context;I)V

    .line 230
    :cond_6
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateBaseTree(Landroid/content/Context;Z)Landroid/widget/RemoteViews;

    move-result-object v28

    .line 231
    .restart local v28    # "views":Landroid/widget/RemoteViews;
    move-object/from16 v0, p2

    move-object/from16 v1, v28

    invoke-virtual {v0, v4, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto/16 :goto_1

    .line 235
    .end local v28    # "views":Landroid/widget/RemoteViews;
    :cond_7
    const/4 v2, 0x0

    aget v16, v20, v2

    .line 236
    .local v16, "minWidth":I
    const/4 v2, 0x1

    aget v2, v20, v2

    sub-int v9, v2, v26

    .line 237
    .local v9, "minHeight":I
    const/4 v2, 0x2

    aget v8, v20, v2

    .line 238
    .local v8, "maxWidth":I
    const/4 v2, 0x3

    aget v2, v20, v2

    sub-int v17, v2, v26

    .line 240
    .local v17, "maxHeight":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    .local v5, "dfeToc":Lcom/google/android/finsky/api/model/DfeToc;
    move-object/from16 v2, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p3

    .line 241
    invoke-direct/range {v2 .. v9}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateViewTree(IILcom/google/android/finsky/api/model/DfeToc;Landroid/content/Context;Ljava/util/Map;II)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;

    move-result-object v23

    .local v23, "land":Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    move-object/from16 v10, p0

    move v11, v3

    move v12, v4

    move-object v13, v5

    move-object/from16 v14, p1

    move-object/from16 v15, p3

    .line 243
    invoke-direct/range {v10 .. v17}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->generateViewTree(IILcom/google/android/finsky/api/model/DfeToc;Landroid/content/Context;Ljava/util/Map;II)Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;

    move-result-object v25

    .line 246
    .local v25, "port":Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;
    new-instance v29, Landroid/widget/RemoteViews;

    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static/range {v23 .. v23}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$000(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Landroid/widget/RemoteViews;

    move-result-object v2

    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mRemoteViews:Landroid/widget/RemoteViews;
    invoke-static/range {v25 .. v25}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$000(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Landroid/widget/RemoteViews;

    move-result-object v6

    move-object/from16 v0, v29

    invoke-direct {v0, v2, v6}, Landroid/widget/RemoteViews;-><init>(Landroid/widget/RemoteViews;Landroid/widget/RemoteViews;)V

    .line 248
    .local v29, "widget":Landroid/widget/RemoteViews;
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mLoadedSuccessfully:Z
    invoke-static/range {v23 .. v23}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$100(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Z

    move-result v2

    if-eqz v2, :cond_8

    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mLoadedSuccessfully:Z
    invoke-static/range {v25 .. v25}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$100(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 250
    :cond_8
    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mUris:Ljava/util/List;
    invoke-static/range {v25 .. v25}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$200(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Ljava/util/List;

    move-result-object v2

    # getter for: Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->mUris:Ljava/util/List;
    invoke-static/range {v23 .. v23}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;->access$200(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$ViewTreeWrapper;)Ljava/util/List;

    move-result-object v6

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->mergePortAndLandRequests(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v22

    .line 252
    .local v22, "imagesToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getImageLoader(Landroid/content/Context;)Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;

    move-result-object v2

    new-instance v6, Lcom/google/android/finsky/widget/consumption/ImageBatch;

    new-instance v7, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v7, v0, v1, v4}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider$1;-><init>(Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;Landroid/content/Context;I)V

    move-object/from16 v0, v22

    invoke-direct {v6, v3, v0, v7}, Lcom/google/android/finsky/widget/consumption/ImageBatch;-><init>(ILjava/util/List;Lcom/google/android/finsky/widget/consumption/BatchedImageLoader$BatchedImageCallback;)V

    invoke-virtual {v2, v6}, Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;->enqueue(Lcom/google/android/finsky/widget/consumption/ImageBatch;)V

    goto/16 :goto_1

    .line 261
    .end local v22    # "imagesToLoad":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    :cond_9
    move-object/from16 v0, p2

    move-object/from16 v1, v29

    invoke-virtual {v0, v4, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto/16 :goto_1
.end method

.method protected varargs updateWidgets(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 176
    invoke-static {p1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getImageLoader(Landroid/content/Context;)Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;->getCachedBitmaps()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->updateWidgets(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;Ljava/util/Map;[I)V

    .line 178
    return-void
.end method

.method public warmImageCache(Landroid/content/Context;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "backend"    # I

    .prologue
    const/4 v8, 0x0

    .line 277
    invoke-static {p2}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getConsumptionDocLists(I)Ljava/util/List;

    move-result-object v1

    .line 278
    .local v1, "docLists":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 280
    .local v4, "imagesToPrefetch":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;

    .line 281
    .local v0, "docList":Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v5, 0x4

    invoke-virtual {v0}, Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;->size()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 282
    new-instance v6, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/services/ConsumptionAppDoc;

    invoke-virtual {v5}, Lcom/google/android/finsky/services/ConsumptionAppDoc;->getImageUri()Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v6, v5, v8, v8}, Lcom/google/android/finsky/widget/consumption/ImageBatch$ImageSpec;-><init>(Landroid/net/Uri;II)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 285
    .end local v0    # "docList":Lcom/google/android/finsky/widget/consumption/ConsumptionAppDocList;
    .end local v2    # "i":I
    :cond_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 286
    sget-boolean v5, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v5, :cond_2

    .line 287
    const-string v5, "Warming cache for %s with %d images"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    const/4 v7, 0x1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 290
    :cond_2
    invoke-static {p1}, Lcom/google/android/finsky/widget/consumption/NowPlayingWidgetProvider;->getImageLoader(Landroid/content/Context;)Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;

    move-result-object v5

    new-instance v6, Lcom/google/android/finsky/widget/consumption/ImageBatch;

    const/4 v7, 0x0

    invoke-direct {v6, p2, v4, v7}, Lcom/google/android/finsky/widget/consumption/ImageBatch;-><init>(ILjava/util/List;Lcom/google/android/finsky/widget/consumption/BatchedImageLoader$BatchedImageCallback;)V

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/widget/consumption/BatchedImageLoader;->enqueue(Lcom/google/android/finsky/widget/consumption/ImageBatch;)V

    .line 292
    :cond_3
    return-void
.end method
