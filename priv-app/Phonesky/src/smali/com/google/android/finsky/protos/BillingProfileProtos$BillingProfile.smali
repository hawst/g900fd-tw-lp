.class public final Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BillingProfileProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BillingProfileProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillingProfile"
.end annotation


# instance fields
.field public billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

.field public hasPaymentsIntegratorCommonToken:Z

.field public hasSelectedExternalInstrumentId:Z

.field public instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

.field public paymentsIntegratorCommonToken:[B

.field public remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

.field public selectedExternalInstrumentId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->clear()Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    .line 44
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-static {}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->emptyArray()[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->selectedExternalInstrumentId:Ljava/lang/String;

    .line 49
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->hasSelectedExternalInstrumentId:Z

    .line 50
    invoke-static {}, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;->emptyArray()[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    .line 51
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->paymentsIntegratorCommonToken:[B

    .line 52
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->hasPaymentsIntegratorCommonToken:Z

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->cachedSize:I

    .line 55
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 92
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 93
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 94
    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    aget-object v0, v3, v1

    .line 95
    .local v0, "element":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    if-eqz v0, :cond_0

    .line 96
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 93
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 101
    .end local v0    # "element":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->hasSelectedExternalInstrumentId:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->selectedExternalInstrumentId:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 102
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->selectedExternalInstrumentId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 105
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 106
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 107
    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    aget-object v0, v3, v1

    .line 108
    .local v0, "element":Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    if-eqz v0, :cond_4

    .line 109
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 106
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 114
    .end local v0    # "element":Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    .end local v1    # "i":I
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->hasPaymentsIntegratorCommonToken:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->paymentsIntegratorCommonToken:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_7

    .line 115
    :cond_6
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->paymentsIntegratorCommonToken:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 118
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_8

    .line 119
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 122
    :cond_8
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 130
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 131
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 135
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 136
    :sswitch_0
    return-object p0

    .line 141
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 143
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-nez v5, :cond_2

    move v1, v4

    .line 144
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .line 146
    .local v2, "newArray":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    if-eqz v1, :cond_1

    .line 147
    iget-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 149
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 150
    new-instance v5, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    aput-object v5, v2, v1

    .line 151
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 152
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 149
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 143
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v1, v5

    goto :goto_1

    .line 155
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/CommonDevice$Instrument;-><init>()V

    aput-object v5, v2, v1

    .line 156
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 157
    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    goto :goto_0

    .line 161
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->selectedExternalInstrumentId:Ljava/lang/String;

    .line 162
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->hasSelectedExternalInstrumentId:Z

    goto :goto_0

    .line 166
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 168
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    if-nez v5, :cond_5

    move v1, v4

    .line 169
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    .line 171
    .local v2, "newArray":[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    if-eqz v1, :cond_4

    .line 172
    iget-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 174
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 175
    new-instance v5, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;-><init>()V

    aput-object v5, v2, v1

    .line 176
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 177
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 174
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 168
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    array-length v1, v5

    goto :goto_3

    .line 180
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;-><init>()V

    aput-object v5, v2, v1

    .line 181
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 182
    iput-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    goto/16 :goto_0

    .line 186
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->paymentsIntegratorCommonToken:[B

    .line 187
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->hasPaymentsIntegratorCommonToken:Z

    goto/16 :goto_0

    .line 191
    :sswitch_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v5, :cond_7

    .line 192
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 194
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 131
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 62
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 63
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->instrument:[Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    aget-object v0, v2, v1

    .line 64
    .local v0, "element":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    if-eqz v0, :cond_0

    .line 65
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 62
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 69
    .end local v0    # "element":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->hasSelectedExternalInstrumentId:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->selectedExternalInstrumentId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 70
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->selectedExternalInstrumentId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 72
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 73
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 74
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->billingProfileOption:[Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;

    aget-object v0, v2, v1

    .line 75
    .local v0, "element":Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    if-eqz v0, :cond_4

    .line 76
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 73
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 80
    .end local v0    # "element":Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfileOption;
    .end local v1    # "i":I
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->hasPaymentsIntegratorCommonToken:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->paymentsIntegratorCommonToken:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_7

    .line 81
    :cond_6
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->paymentsIntegratorCommonToken:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 83
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_8

    .line 84
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->remindMeLaterIconImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 86
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 87
    return-void
.end method
