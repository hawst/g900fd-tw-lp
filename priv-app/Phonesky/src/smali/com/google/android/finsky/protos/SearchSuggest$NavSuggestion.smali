.class public final Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;
.super Lcom/google/protobuf/nano/MessageNano;
.source "SearchSuggest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/SearchSuggest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NavSuggestion"
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public docId:Ljava/lang/String;

.field public hasDescription:Z

.field public hasDocId:Z

.field public hasImageBlob:Z

.field public image:Lcom/google/android/finsky/protos/Common$Image;

.field public imageBlob:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->clear()Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    .line 104
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->docId:Ljava/lang/String;

    .line 108
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasDocId:Z

    .line 109
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->description:Ljava/lang/String;

    .line 110
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasDescription:Z

    .line 111
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->imageBlob:[B

    .line 112
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasImageBlob:Z

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 114
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->cachedSize:I

    .line 115
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 138
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 139
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasDocId:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->docId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 140
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->docId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasImageBlob:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->imageBlob:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_3

    .line 144
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->imageBlob:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_4

    .line 148
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasDescription:Z

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->description:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 152
    :cond_5
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->description:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    :cond_6
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 163
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 164
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 168
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    :sswitch_0
    return-object p0

    .line 174
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->docId:Ljava/lang/String;

    .line 175
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasDocId:Z

    goto :goto_0

    .line 179
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->imageBlob:[B

    .line 180
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasImageBlob:Z

    goto :goto_0

    .line 184
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_1

    .line 185
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 187
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 191
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->description:Ljava/lang/String;

    .line 192
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasDescription:Z

    goto :goto_0

    .line 164
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasDocId:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->docId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 122
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->docId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 124
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasImageBlob:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->imageBlob:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_3

    .line 125
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->imageBlob:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 127
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_4

    .line 128
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 130
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->hasDescription:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->description:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 131
    :cond_5
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/SearchSuggest$NavSuggestion;->description:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 133
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 134
    return-void
.end method
