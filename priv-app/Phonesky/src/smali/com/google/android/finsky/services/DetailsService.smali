.class public Lcom/google/android/finsky/services/DetailsService;
.super Landroid/app/Service;
.source "DetailsService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private static addImage(Lcom/google/android/finsky/api/model/Document;ILandroid/os/Bundle;)Z
    .locals 8
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "imageType"    # I
    .param p2, "response"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 262
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v2

    .line 263
    .local v2, "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 265
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Image;

    .line 266
    .local v0, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-boolean v5, v0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    if-eqz v5, :cond_1

    .line 268
    iget-boolean v5, v0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v5, :cond_0

    .line 269
    const-string v1, "fife_url"

    .line 274
    .local v1, "imageKey":Ljava/lang/String;
    :goto_0
    iget-object v4, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    invoke-virtual {p2, v1, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    .end local v0    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "imageKey":Ljava/lang/String;
    :goto_1
    return v3

    .line 271
    .restart local v0    # "image":Lcom/google/android/finsky/protos/Common$Image;
    :cond_0
    const-string v5, "App %s no FIFE URL for %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v3

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 272
    const-string v1, "image_url"

    .restart local v1    # "imageKey":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v1    # "imageKey":Ljava/lang/String;
    :cond_1
    move v3, v4

    .line 278
    goto :goto_1
.end method

.method private static createDetailsIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "callingApp"    # Ljava/lang/String;

    .prologue
    .line 237
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "market"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "details"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "id"

    invoke-virtual {v2, v3, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "api_source"

    const-string v4, "DetailsService"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "referrer_package"

    invoke-virtual {v2, v3, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 244
    .local v0, "detailsUri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/finsky/activities/MainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 245
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 246
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 247
    return-object v1
.end method

.method public static getBundle(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;Ljava/lang/String;Lcom/google/android/finsky/analytics/FinskyEventLog;)Landroid/os/Bundle;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "currentAccount"    # Landroid/accounts/Account;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "installer"    # Lcom/google/android/finsky/receivers/Installer;
    .param p5, "appStates"    # Lcom/google/android/finsky/appstate/AppStates;
    .param p6, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p7, "callingApp"    # Ljava/lang/String;
    .param p8, "eventLog"    # Lcom/google/android/finsky/analytics/FinskyEventLog;

    .prologue
    .line 149
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v4

    .line 152
    .local v4, "packageName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v14

    .line 153
    .local v14, "backend":I
    const/4 v2, 0x3

    if-eq v14, v2, :cond_0

    .line 154
    const-string v2, "Document %s is not an app, backend=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v5, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 155
    const/16 v3, 0x200

    const-string v5, "doc-backend"

    const/4 v7, 0x0

    move-object/from16 v2, p8

    move-object/from16 v6, p7

    invoke-static/range {v2 .. v7}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const/16 v20, 0x0

    .line 222
    :goto_0
    return-object v20

    .line 160
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v16

    .line 161
    .local v16, "docType":I
    const/4 v2, 0x1

    move/from16 v0, v16

    if-eq v0, v2, :cond_1

    .line 162
    const-string v2, "Document %s is not an app, doc_type=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v5, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    const/16 v3, 0x200

    const-string v5, "doc-type"

    const/4 v7, 0x0

    move-object/from16 v2, p8

    move-object/from16 v6, p7

    invoke-static/range {v2 .. v7}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    const/16 v20, 0x0

    goto :goto_0

    .line 168
    :cond_1
    new-instance v11, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    invoke-direct {v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;-><init>()V

    .local v11, "actions":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;
    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p5

    move-object/from16 v9, p3

    move-object/from16 v10, p1

    .line 169
    invoke-static/range {v5 .. v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getDocumentActions(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    .line 172
    invoke-static {v11}, Lcom/google/android/finsky/services/DetailsService;->getBuyOrInstallAction(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    move-result-object v12

    .line 173
    .local v12, "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    if-nez v12, :cond_2

    .line 174
    const-string v2, "App %s has no buy or install action, actions=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v5, 0x1

    invoke-virtual {v11}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 176
    const/16 v3, 0x200

    const-string v5, "doc-actions"

    const/4 v7, 0x0

    move-object/from16 v2, p8

    move-object/from16 v6, p7

    invoke-static/range {v2 .. v7}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    const/16 v20, 0x0

    goto :goto_0

    .line 181
    :cond_2
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    .line 182
    .local v19, "res":Landroid/content/res/Resources;
    invoke-virtual/range {v19 .. v19}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v0, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    move-object/from16 v17, v0

    .line 184
    .local v17, "locale":Ljava/util/Locale;
    new-instance v20, Landroid/os/Bundle;

    invoke-direct/range {v20 .. v20}, Landroid/os/Bundle;-><init>()V

    .line 185
    .local v20, "response":Landroid/os/Bundle;
    const-string v2, "title"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const-string v2, "creator"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasRating()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 188
    const-string v2, "star_rating"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getStarRating()F

    move-result v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 189
    const-string v2, "rating_count"

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getRatingCount()J

    move-result-wide v6

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 191
    :cond_3
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v2, v1}, Lcom/google/android/finsky/services/DetailsService;->addImage(Lcom/google/android/finsky/api/model/Document;ILandroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 192
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v2, v1}, Lcom/google/android/finsky/services/DetailsService;->addImage(Lcom/google/android/finsky/api/model/Document;ILandroid/os/Bundle;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 193
    const-string v2, "App %s using thumbnail image"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    :cond_4
    :goto_1
    new-instance v13, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-direct {v13}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;-><init>()V

    .line 203
    .local v13, "actionStyle":Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;
    invoke-static {v12, v14, v13}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionStyleWithoutBuyPrefix(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 204
    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->getString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v15

    .line 205
    .local v15, "buttonText":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v15

    .line 206
    const-string v2, "purchase_button_text"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v15}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->createExternalPurchaseIntent(Lcom/google/android/finsky/api/model/Document;I)Landroid/content/Intent;

    move-result-object v18

    .line 213
    .local v18, "purchaseIntent":Landroid/content/Intent;
    const-string v2, "detailsservice"

    const/4 v3, 0x0

    invoke-static {v2, v4, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 214
    const-string v2, "purchase_intent"

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-static {v0, v3, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 217
    const-string v2, "details_intent"

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p7

    invoke-static {v0, v4, v1}, Lcom/google/android/finsky/services/DetailsService;->createDetailsIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v3, v5, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 220
    const/16 v3, 0x200

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p8

    move-object/from16 v6, p7

    invoke-static/range {v2 .. v7}, Lcom/google/android/finsky/utils/SignatureUtils;->logEvent(Lcom/google/android/finsky/analytics/FinskyEventLog;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 195
    .end local v13    # "actionStyle":Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;
    .end local v15    # "buttonText":Ljava/lang/String;
    .end local v18    # "purchaseIntent":Landroid/content/Intent;
    :cond_5
    const-string v2, "App %s failed to find any image"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static getBuyOrInstallAction(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .locals 4
    .param p0, "actions"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    .prologue
    .line 251
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    if-ge v1, v2, :cond_2

    .line 252
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->getAction(I)Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    move-result-object v0

    .line 253
    .local v0, "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    iget v2, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/4 v3, 0x7

    if-eq v2, v3, :cond_0

    iget v2, v0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 258
    .end local v0    # "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    :cond_0
    :goto_1
    return-object v0

    .line 251
    .restart local v0    # "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 258
    .end local v0    # "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/finsky/services/DetailsService$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/services/DetailsService$1;-><init>(Lcom/google/android/finsky/services/DetailsService;)V

    return-object v0
.end method
