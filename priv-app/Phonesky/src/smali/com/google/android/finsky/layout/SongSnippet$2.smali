.class Lcom/google/android/finsky/layout/SongSnippet$2;
.super Ljava/lang/Object;
.source "SongSnippet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/SongSnippet;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/SongSnippet;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$isMature:Z


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/SongSnippet;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/finsky/layout/SongSnippet$2;->this$0:Lcom/google/android/finsky/layout/SongSnippet;

    iput-boolean p2, p0, Lcom/google/android/finsky/layout/SongSnippet$2;->val$isMature:Z

    iput-object p3, p0, Lcom/google/android/finsky/layout/SongSnippet$2;->val$accountName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 149
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/SongSnippet$2;->val$isMature:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/SongSnippet$2;->val$accountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->isAgeVerificationRequired()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lcom/google/android/finsky/layout/SongSnippet$2;->val$accountName:Ljava/lang/String;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/AgeVerificationActivity;->createIntent(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 154
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/layout/SongSnippet$2;->this$0:Lcom/google/android/finsky/layout/SongSnippet;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/SongSnippet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 158
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/SongSnippet$2;->this$0:Lcom/google/android/finsky/layout/SongSnippet;

    # getter for: Lcom/google/android/finsky/layout/SongSnippet;->mConnection:Lcom/google/android/finsky/previews/PreviewController;
    invoke-static {v1}, Lcom/google/android/finsky/layout/SongSnippet;->access$200(Lcom/google/android/finsky/layout/SongSnippet;)Lcom/google/android/finsky/previews/PreviewController;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/SongSnippet$2;->this$0:Lcom/google/android/finsky/layout/SongSnippet;

    # getter for: Lcom/google/android/finsky/layout/SongSnippet;->mSongDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;
    invoke-static {v2}, Lcom/google/android/finsky/layout/SongSnippet;->access$100(Lcom/google/android/finsky/layout/SongSnippet;)Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/previews/PreviewController;->togglePlayback(Lcom/google/android/finsky/protos/DocDetails$SongDetails;)V

    goto :goto_0
.end method
