.class Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$1;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"

# interfaces
.implements Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Landroid/support/v7/app/ActionBarActivity;Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$1;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 1

    .prologue
    .line 236
    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sSawFirstBackstackChange:Z
    invoke-static {}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$1;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # invokes: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->clearSearch()V
    invoke-static {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$100(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V

    .line 241
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$1;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # invokes: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncState()V
    invoke-static {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$200(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V

    .line 242
    return-void

    .line 239
    :cond_0
    const/4 v0, 0x1

    # setter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sSawFirstBackstackChange:Z
    invoke-static {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$002(Z)Z

    goto :goto_0
.end method
