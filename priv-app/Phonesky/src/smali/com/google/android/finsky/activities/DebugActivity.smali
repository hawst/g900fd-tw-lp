.class public Lcom/google/android/finsky/activities/DebugActivity;
.super Landroid/preference/PreferenceActivity;
.source "DebugActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;,
        Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;,
        Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;
    }
.end annotation


# static fields
.field private static final ORIGINAL_DFE_URL:Ljava/lang/String;

.field private static final ORIGINAL_VENDING_API_URL:Ljava/lang/String;


# instance fields
.field private final mCarrierHelper:Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper",
            "<",
            "Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final mEnvironmentHelper:Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper",
            "<",
            "Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;",
            ">;"
        }
    .end annotation
.end field

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/finsky/api/DfeApi;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/activities/DebugActivity;->ORIGINAL_DFE_URL:Ljava/lang/String;

    .line 64
    const-string v0, "https://android.clients.google.com/vending/api/ApiRequest"

    const-string v1, "api/ApiRequest"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/activities/DebugActivity;->ORIGINAL_VENDING_API_URL:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    .line 67
    new-instance v0, Lcom/google/android/finsky/activities/FakeNavigationManager;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/FakeNavigationManager;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/DebugActivity;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 386
    new-instance v0, Lcom/google/android/finsky/activities/DebugActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/DebugActivity$3;-><init>(Lcom/google/android/finsky/activities/DebugActivity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/DebugActivity;->mCarrierHelper:Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;

    .line 411
    new-instance v0, Lcom/google/android/finsky/activities/DebugActivity$4;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/activities/DebugActivity$4;-><init>(Lcom/google/android/finsky/activities/DebugActivity;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/DebugActivity;->mEnvironmentHelper:Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/DebugActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DebugActivity;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->clearCacheAndRestart()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/DebugActivity;Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DebugActivity;
    .param p1, "x1"    # Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/DebugActivity;->selectCarrier(Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;)V

    return-void
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/finsky/activities/DebugActivity;->ORIGINAL_DFE_URL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/DebugActivity;Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DebugActivity;
    .param p1, "x1"    # Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/finsky/activities/DebugActivity;->selectEnvironment(Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;)V

    return-void
.end method

.method private clearCacheAndRestart()V
    .locals 2

    .prologue
    .line 362
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/activities/DebugActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/DebugActivity$2;-><init>(Lcom/google/android/finsky/activities/DebugActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->clearCacheAsync(Ljava/lang/Runnable;)V

    .line 375
    return-void
.end method

.method private exportDatabase(Ljava/lang/String;)V
    .locals 13
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    .line 172
    const/4 v7, 0x0

    .line 174
    .local v7, "exported":Z
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/DebugActivity;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v8

    .line 175
    .local v8, "inputFile":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v8}, Ljava/io/File;->canRead()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 176
    new-instance v10, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-direct {v10, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 178
    .local v10, "outputFile":Ljava/io/File;
    new-instance v9, Ljava/io/FileInputStream;

    invoke-direct {v9, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 179
    .local v9, "inputStream":Ljava/io/FileInputStream;
    new-instance v11, Ljava/io/FileOutputStream;

    invoke-direct {v11, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 180
    .local v11, "outputStream":Ljava/io/FileOutputStream;
    invoke-virtual {v9}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 181
    .local v1, "input":Ljava/nio/channels/FileChannel;
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 183
    .local v6, "output":Ljava/nio/channels/FileChannel;
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J

    .line 184
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V

    .line 185
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V

    .line 186
    invoke-virtual {v9}, Ljava/io/FileInputStream;->close()V

    .line 187
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    const/4 v7, 0x1

    .line 194
    .end local v1    # "input":Ljava/nio/channels/FileChannel;
    .end local v6    # "output":Ljava/nio/channels/FileChannel;
    .end local v8    # "inputFile":Ljava/io/File;
    .end local v9    # "inputStream":Ljava/io/FileInputStream;
    .end local v10    # "outputFile":Ljava/io/File;
    .end local v11    # "outputStream":Ljava/io/FileOutputStream;
    :cond_0
    :goto_0
    if-nez v7, :cond_1

    .line 195
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to export "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 197
    :cond_1
    return-void

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "Unable to export: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v12

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private getOptionsFromCsv(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;)I
    .locals 15
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 282
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    .local p3, "options":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "helper":Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;, "Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper<TT;>;"
    const/4 v11, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v11}, Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;->fromStringList([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    const-string v11, "Default"

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    const/4 v1, 0x0

    .line 286
    .local v1, "checkedIndex":I
    const/4 v9, 0x0

    .line 288
    .local v9, "reader":Ljava/io/BufferedReader;
    :try_start_0
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    .line 289
    .local v4, "external":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v2, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 293
    .local v2, "csvFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    .line 294
    new-instance v2, Ljava/io/File;

    .end local v2    # "csvFile":Ljava/io/File;
    new-instance v11, Ljava/io/File;

    const-string v12, "Download"

    invoke-direct {v11, v4, v12}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-direct {v2, v11, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 296
    .restart local v2    # "csvFile":Ljava/io/File;
    :cond_0
    new-instance v10, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/FileReader;

    invoke-direct {v11, v2}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v10, v11}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    .end local v9    # "reader":Ljava/io/BufferedReader;
    .local v10, "reader":Ljava/io/BufferedReader;
    :cond_1
    :goto_0
    :try_start_1
    invoke-virtual {v10}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    .local v7, "line":Ljava/lang/String;
    if-eqz v7, :cond_3

    .line 299
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 300
    const-string v11, "#"

    invoke-virtual {v7, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_1

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 304
    const-string v11, ","

    invoke-virtual {v7, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 306
    .local v5, "fields":[Ljava/lang/String;
    const/4 v11, 0x0

    aget-object v8, v5, v11

    .line 308
    .local v8, "name":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 309
    const-string v11, "Skipping an item from csv without a name"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-static {v11, v12}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 321
    .end local v5    # "fields":[Ljava/lang/String;
    .end local v7    # "line":Ljava/lang/String;
    .end local v8    # "name":Ljava/lang/String;
    :catch_0
    move-exception v3

    move-object v9, v10

    .line 322
    .end local v2    # "csvFile":Ljava/io/File;
    .end local v4    # "external":Ljava/io/File;
    .end local v10    # "reader":Ljava/io/BufferedReader;
    .local v3, "e":Ljava/lang/Exception;
    .restart local v9    # "reader":Ljava/io/BufferedReader;
    :goto_1
    :try_start_2
    const-string v11, "Unable to build selector: %s"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-static {v11, v12}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    const-string v11, "Unable to build selector."

    const/4 v12, 0x1

    invoke-static {p0, v11, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/Toast;->show()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 324
    const/4 v1, 0x0

    .line 326
    invoke-static {v9}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 328
    .end local v3    # "e":Ljava/lang/Exception;
    :goto_2
    return v1

    .line 313
    .end local v9    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "csvFile":Ljava/io/File;
    .restart local v4    # "external":Ljava/io/File;
    .restart local v5    # "fields":[Ljava/lang/String;
    .restart local v7    # "line":Ljava/lang/String;
    .restart local v8    # "name":Ljava/lang/String;
    .restart local v10    # "reader":Ljava/io/BufferedReader;
    :cond_2
    :try_start_3
    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 314
    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;->fromStringList([Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 315
    .local v6, "item":Ljava/lang/Object;, "TT;"
    move-object/from16 v0, p2

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 317
    move-object/from16 v0, p4

    invoke-interface {v0, v6}, Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;->isSelected(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 318
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v11

    add-int/lit8 v1, v11, -0x1

    goto :goto_0

    .line 326
    .end local v5    # "fields":[Ljava/lang/String;
    .end local v6    # "item":Ljava/lang/Object;, "TT;"
    .end local v8    # "name":Ljava/lang/String;
    :cond_3
    invoke-static {v10}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    move-object v9, v10

    .line 327
    .end local v10    # "reader":Ljava/io/BufferedReader;
    .restart local v9    # "reader":Ljava/io/BufferedReader;
    goto :goto_2

    .line 326
    .end local v2    # "csvFile":Ljava/io/File;
    .end local v4    # "external":Ljava/io/File;
    .end local v7    # "line":Ljava/lang/String;
    :catchall_0
    move-exception v11

    :goto_3
    invoke-static {v9}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    throw v11

    .end local v9    # "reader":Ljava/io/BufferedReader;
    .restart local v2    # "csvFile":Ljava/io/File;
    .restart local v4    # "external":Ljava/io/File;
    .restart local v10    # "reader":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v11

    move-object v9, v10

    .end local v10    # "reader":Ljava/io/BufferedReader;
    .restart local v9    # "reader":Ljava/io/BufferedReader;
    goto :goto_3

    .line 321
    .end local v2    # "csvFile":Ljava/io/File;
    .end local v4    # "external":Ljava/io/File;
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method private override(Lcom/google/android/play/utils/config/GservicesValue;Ljava/lang/String;)V
    .locals 2
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/play/utils/config/GservicesValue",
            "<+",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 356
    .local p1, "setting":Lcom/google/android/play/utils/config/GservicesValue;, "Lcom/google/android/play/utils/config/GservicesValue<+Ljava/lang/Object;>;"
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.gservices.intent.action.GSERVICES_OVERRIDE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 357
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Lcom/google/android/play/utils/config/GservicesValue;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 358
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 359
    return-void
.end method

.method private selectCarrier(Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;)V
    .locals 2
    .param p1, "newCarrier"    # Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;

    .prologue
    .line 351
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->ipCountryOverride:Lcom/google/android/play/utils/config/GservicesValue;

    iget-object v1, p1, Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;->countryCode:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/activities/DebugActivity;->override(Lcom/google/android/play/utils/config/GservicesValue;Ljava/lang/String;)V

    .line 352
    sget-object v0, Lcom/google/android/finsky/api/DfeApiConfig;->mccMncOverride:Lcom/google/android/play/utils/config/GservicesValue;

    iget-object v1, p1, Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;->simCode:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/activities/DebugActivity;->override(Lcom/google/android/play/utils/config/GservicesValue;Ljava/lang/String;)V

    .line 353
    return-void
.end method

.method private selectEnvironment(Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;)V
    .locals 7
    .param p1, "newEnvironment"    # Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;

    .prologue
    const/4 v4, 0x0

    .line 332
    new-instance v0, Landroid/content/Intent;

    const-string v5, "com.google.gservices.intent.action.GSERVICES_OVERRIDE"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 333
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    .line 334
    .local v1, "urlRewrite":Ljava/lang/String;
    const/4 v2, 0x0

    .line 335
    .local v2, "vendingUrlRewrite":Ljava/lang/String;
    const/4 v3, 0x0

    .line 336
    .local v3, "walletEscrowUrl":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 337
    iget-object v5, p1, Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;->dfeBaseUrl:Ljava/lang/String;

    if-nez v5, :cond_1

    move-object v1, v4

    .line 339
    :goto_0
    iget-object v5, p1, Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;->vendingBaseUrl:Ljava/lang/String;

    if-nez v5, :cond_2

    move-object v2, v4

    .line 341
    :goto_1
    iget-object v3, p1, Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;->escrowUrl:Ljava/lang/String;

    .line 343
    :cond_0
    const-string v4, "url:finsky_dfe_url"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    const-string v4, "url:vending_machine_ssl_url"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const-string v4, "url:licensing_url"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 346
    const-string v4, "finsky.wallet_escrow_url"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 347
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DebugActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 348
    return-void

    .line 337
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/google/android/finsky/activities/DebugActivity;->ORIGINAL_DFE_URL:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " rewrite "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;->dfeBaseUrl:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 339
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/google/android/finsky/activities/DebugActivity;->ORIGINAL_VENDING_API_URL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " rewrite "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;->vendingBaseUrl:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private setupFakeItemRater()V
    .locals 2

    .prologue
    .line 227
    const-string v1, "fake_item_rater"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/DebugActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 228
    .local v0, "pref":Landroid/preference/CheckBoxPreference;
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->internalFakeItemRaterEnabled:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 229
    return-void
.end method

.method private setupImageDebugging()V
    .locals 2

    .prologue
    .line 200
    const-string v1, "image_debugging"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/DebugActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 201
    .local v0, "pref":Landroid/preference/CheckBoxPreference;
    sget-object v1, Lcom/google/android/play/utils/config/PlayG;->debugImageSizes:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 202
    return-void
.end method

.method private setupPrexDisabledCheckbox()V
    .locals 2

    .prologue
    .line 215
    const-string v1, "disable_personalization"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/DebugActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 216
    .local v0, "pref":Landroid/preference/CheckBoxPreference;
    sget-object v1, Lcom/google/android/finsky/api/DfeApiConfig;->prexDisabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 217
    return-void
.end method

.method private setupShowStagingMerchData()V
    .locals 2

    .prologue
    .line 210
    const-string v1, "show_staging_data"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/DebugActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 211
    .local v0, "pref":Landroid/preference/CheckBoxPreference;
    sget-object v1, Lcom/google/android/finsky/api/DfeApiConfig;->showStagingData:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 212
    return-void
.end method

.method private setupSkipDfeCache()V
    .locals 2

    .prologue
    .line 205
    const-string v1, "skip_all_caches"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/DebugActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 206
    .local v0, "pref":Landroid/preference/CheckBoxPreference;
    sget-object v1, Lcom/google/android/finsky/api/DfeApiConfig;->skipAllCaches:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 207
    return-void
.end method

.method private setupVolleyLogging()V
    .locals 3

    .prologue
    .line 220
    const-string v1, "verbose_volley_logging"

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/DebugActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 221
    .local v0, "pref":Landroid/preference/CheckBoxPreference;
    const-string v1, "Volley"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 223
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 224
    return-void
.end method


# virtual methods
.method public buildSelectCountryDialog()V
    .locals 4

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/finsky/activities/DebugActivity;->mCarrierHelper:Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;

    const-string v1, "carriers.csv"

    const-string v2, "Switching country..."

    const v3, 0x7f0c040b

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/finsky/activities/DebugActivity;->buildSelectorFromCsv(Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;Ljava/lang/String;Ljava/lang/String;I)V

    .line 237
    return-void
.end method

.method public buildSelectEnvironmentDialog()V
    .locals 4

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/finsky/activities/DebugActivity;->mEnvironmentHelper:Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;

    const-string v1, "marketenvs.csv"

    const-string v2, "Switching environment..."

    const v3, 0x7f0c040a

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/finsky/activities/DebugActivity;->buildSelectorFromCsv(Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;Ljava/lang/String;Ljava/lang/String;I)V

    .line 245
    return-void
.end method

.method public buildSelectorFromCsv(Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p2, "filename"    # Ljava/lang/String;
    .param p3, "toastText"    # Ljava/lang/String;
    .param p4, "dialogTitle"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper",
            "<TT;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 249
    .local p1, "helper":Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;, "Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper<TT;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v3

    .line 250
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<TT;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 251
    .local v5, "options":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0, p2, v3, v5, p1}, Lcom/google/android/finsky/activities/DebugActivity;->getOptionsFromCsv(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;)I

    move-result v1

    .line 254
    .local v1, "checkedIndex":I
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 255
    .local v2, "dialog":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v2, p4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 258
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    .line 259
    .local v4, "optionNames":[Ljava/lang/String;
    new-instance v6, Lcom/google/android/finsky/activities/DebugActivity$1;

    invoke-direct {v6, p0, p3, p1, v3}, Lcom/google/android/finsky/activities/DebugActivity$1;-><init>(Lcom/google/android/finsky/activities/DebugActivity;Ljava/lang/String;Lcom/google/android/finsky/activities/DebugActivity$ICsvSelectorHelper;Ljava/util/List;)V

    invoke-virtual {v2, v4, v1, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 276
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 277
    .local v0, "builtDialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 278
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    sget-object v0, Lcom/google/android/finsky/config/G;->debugOptionsEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DebugActivity;->finish()V

    .line 114
    :goto_0
    return-void

    .line 104
    :cond_0
    const v0, 0x7f070001

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DebugActivity;->addPreferencesFromResource(I)V

    .line 106
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->setupImageDebugging()V

    .line 107
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->setupSkipDfeCache()V

    .line 108
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->setupShowStagingMerchData()V

    .line 109
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->setupPrexDisabledCheckbox()V

    .line 110
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->setupVolleyLogging()V

    .line 111
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->setupFakeItemRater()V

    .line 113
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DebugActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DebugActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090054

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 6
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 118
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    .line 119
    .local v1, "key":Ljava/lang/String;
    const-string v2, "image_debugging"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    sget-object v5, Lcom/google/android/play/utils/config/PlayG;->debugImageSizes:Lcom/google/android/play/utils/config/GservicesValue;

    sget-object v2, Lcom/google/android/play/utils/config/PlayG;->debugImageSizes:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/google/android/finsky/activities/DebugActivity;->override(Lcom/google/android/play/utils/config/GservicesValue;Ljava/lang/String;)V

    .line 168
    :goto_1
    return v3

    :cond_0
    move v2, v4

    .line 120
    goto :goto_0

    .line 123
    :cond_1
    const-string v2, "skip_all_caches"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 124
    sget-object v5, Lcom/google/android/finsky/api/DfeApiConfig;->skipAllCaches:Lcom/google/android/play/utils/config/GservicesValue;

    sget-object v2, Lcom/google/android/finsky/api/DfeApiConfig;->skipAllCaches:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/google/android/finsky/activities/DebugActivity;->override(Lcom/google/android/play/utils/config/GservicesValue;Ljava/lang/String;)V

    .line 168
    :cond_2
    :goto_3
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v3

    goto :goto_1

    :cond_3
    move v3, v4

    .line 124
    goto :goto_2

    .line 126
    :cond_4
    const-string v2, "environments"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 127
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DebugActivity;->buildSelectEnvironmentDialog()V

    goto :goto_1

    .line 129
    :cond_5
    const-string v2, "clear_cache"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 130
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->clearCacheAndRestart()V

    goto :goto_1

    .line 132
    :cond_6
    const-string v2, "show_staging_data"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 133
    sget-object v5, Lcom/google/android/finsky/api/DfeApiConfig;->showStagingData:Lcom/google/android/play/utils/config/GservicesValue;

    sget-object v2, Lcom/google/android/finsky/api/DfeApiConfig;->showStagingData:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_7

    move v4, v3

    :cond_7
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/google/android/finsky/activities/DebugActivity;->override(Lcom/google/android/play/utils/config/GservicesValue;Ljava/lang/String;)V

    .line 135
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->clearCacheAndRestart()V

    goto :goto_1

    .line 137
    :cond_8
    const-string v2, "disable_personalization"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 138
    sget-object v5, Lcom/google/android/finsky/api/DfeApiConfig;->prexDisabled:Lcom/google/android/play/utils/config/GservicesValue;

    sget-object v2, Lcom/google/android/finsky/api/DfeApiConfig;->prexDisabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_9

    move v4, v3

    :cond_9
    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/google/android/finsky/activities/DebugActivity;->override(Lcom/google/android/play/utils/config/GservicesValue;Ljava/lang/String;)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->clearCacheAndRestart()V

    goto/16 :goto_1

    .line 141
    :cond_a
    const-string v2, "reset_all"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 142
    sget-object v2, Lcom/google/android/play/utils/config/PlayG;->debugImageSizes:Lcom/google/android/play/utils/config/GservicesValue;

    const-string v4, "false"

    invoke-direct {p0, v2, v4}, Lcom/google/android/finsky/activities/DebugActivity;->override(Lcom/google/android/play/utils/config/GservicesValue;Ljava/lang/String;)V

    .line 143
    new-instance v2, Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;

    invoke-direct {v2, v5, v5, v5}, Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/DebugActivity;->selectEnvironment(Lcom/google/android/finsky/activities/DebugActivity$EnvironmentOverride;)V

    .line 144
    new-instance v2, Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;

    invoke-direct {v2, v5, v5, v5}, Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/DebugActivity;->selectCarrier(Lcom/google/android/finsky/activities/DebugActivity$CarrierOverride;)V

    .line 145
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DebugActivity;->clearCacheAndRestart()V

    goto/16 :goto_1

    .line 147
    :cond_b
    const-string v2, "country"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 148
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DebugActivity;->buildSelectCountryDialog()V

    goto/16 :goto_1

    .line 150
    :cond_c
    const-string v2, "export_library"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 151
    const-string v2, "localappstate.db"

    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/DebugActivity;->exportDatabase(Ljava/lang/String;)V

    .line 152
    const-string v2, "library.db"

    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/DebugActivity;->exportDatabase(Ljava/lang/String;)V

    .line 154
    const-string v2, "Finished database exports"

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 156
    :cond_d
    const-string v2, "dump_library_state"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 157
    const-string v2, "FinskyLibrary"

    const-string v5, "------------ LIBRARY DUMP BEGIN"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/library/Libraries;->dumpState()V

    .line 159
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/finsky/library/LibraryReplicators;->dumpState()V

    .line 160
    const-string v2, "FinskyLibrary"

    const-string v5, "------------ LIBRARY DUMP END"

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    const-string v2, "Library state dumped to logcat."

    invoke-static {p0, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 163
    :cond_e
    const-string v2, "fake_item_rater"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 164
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->internalFakeItemRaterEnabled:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 166
    .local v0, "isFakeRaterCurrentlyActive":Z
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->internalFakeItemRaterEnabled:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    if-nez v0, :cond_f

    :goto_4
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_f
    move v3, v4

    goto :goto_4
.end method
