.class public Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;
.super Lcom/google/android/play/layout/PlayCardViewBase;
.source "PlayCardViewMyApps.java"

# interfaces
.implements Landroid/widget/Checkable;
.implements Lcom/google/android/finsky/layout/play/Identifiable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;
    }
.end annotation


# static fields
.field private static final CHECKED_STATE_SET:[I


# instance fields
.field private final mArchiveArea:Landroid/graphics/Rect;

.field private mArchiveView:Landroid/view/View;

.field private mChecked:Z

.field private mIdentifier:Ljava/lang/String;

.field private final mOldArchiveArea:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->CHECKED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mChecked:Z

    .line 55
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mThumbnailAspectRatio:F

    .line 56
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    .line 57
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mOldArchiveArea:Landroid/graphics/Rect;

    .line 58
    return-void
.end method


# virtual methods
.method public getCardType()I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0x9

    return v0
.end method

.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mChecked:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2
    .param p1, "extraSpace"    # I

    .prologue
    .line 121
    add-int/lit8 v1, p1, 0x1

    invoke-super {p0, v1}, Lcom/google/android/play/layout/PlayCardViewBase;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 122
    .local v0, "drawableState":[I
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mChecked:Z

    if-eqz v1, :cond_0

    .line 123
    sget-object v1, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->CHECKED_STATE_SET:[I

    invoke-static {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mergeDrawableStates([I[I)[I

    .line 125
    :cond_0
    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/android/play/layout/PlayCardViewBase;->onFinishInflate()V

    .line 68
    const v0, 0x7f0a02cc

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    .line 69
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 131
    invoke-super/range {p0 .. p5}, Lcom/google/android/play/layout/PlayCardViewBase;->onLayout(ZIIII)V

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 144
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mOldArchiveArea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    new-instance v0, Landroid/view/TouchDelegate;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mOldArchiveArea:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveArea:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 73
    invoke-virtual {p0, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->measureThumbnailSpanningHeight(I)V

    .line 75
    invoke-super {p0, p1, p2}, Lcom/google/android/play/layout/PlayCardViewBase;->onMeasure(II)V

    .line 76
    return-void
.end method

.method public setArchivable(ZLcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;)V
    .locals 3
    .param p1, "isArchivable"    # Z
    .param p2, "onArchiveActionListener"    # Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;

    .prologue
    const/4 v2, 0x0

    .line 80
    if-nez p1, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 82
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setNextFocusRightId(I)V

    .line 83
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 98
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 88
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    const v1, 0x7f0a00d0

    invoke-virtual {v0, v1}, Landroid/view/View;->setNextFocusLeftId(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setNextFocusRightId(I)V

    .line 90
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mArchiveView:Landroid/view/View;

    new-instance v1, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$1;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mChecked:Z

    if-ne v0, p1, :cond_0

    .line 107
    :goto_0
    return-void

    .line 105
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mChecked:Z

    .line 106
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->refreshDrawableState()V

    goto :goto_0
.end method

.method public setIdentifier(Ljava/lang/String;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mIdentifier:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->mChecked:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardViewMyApps;->setChecked(Z)V

    .line 117
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
