.class public final Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PromoCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PromoCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseFlowAction"
.end annotation


# instance fields
.field public hasPurchaseOfferType:Z

.field public purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public purchaseOfferType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1455
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1456
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->clear()Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    .line 1457
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;
    .locals 1

    .prologue
    .line 1460
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1461
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseOfferType:I

    .line 1462
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->hasPurchaseOfferType:Z

    .line 1463
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->cachedSize:I

    .line 1464
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1481
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1482
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v1, :cond_0

    .line 1483
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1486
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseOfferType:I

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->hasPurchaseOfferType:Z

    if-eqz v1, :cond_2

    .line 1487
    :cond_1
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseOfferType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1490
    :cond_2
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1498
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1499
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1503
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1504
    :sswitch_0
    return-object p0

    .line 1509
    :sswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v2, :cond_1

    .line 1510
    new-instance v2, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 1512
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1516
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1517
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1530
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseOfferType:I

    .line 1531
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->hasPurchaseOfferType:Z

    goto :goto_0

    .line 1499
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    .line 1517
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1431
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1470
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_0

    .line 1471
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseDoc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1473
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseOfferType:I

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->hasPurchaseOfferType:Z

    if-eqz v0, :cond_2

    .line 1474
    :cond_1
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/PromoCode$PurchaseFlowAction;->purchaseOfferType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1476
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1477
    return-void
.end method
