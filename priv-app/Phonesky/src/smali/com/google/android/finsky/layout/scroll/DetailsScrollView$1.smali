.class Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;
.super Ljava/lang/Object;
.source "DetailsScrollView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->bindDetailsHero(Landroid/view/ViewGroup;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/HeroGraphicView;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

.field final synthetic val$heroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;Lcom/google/android/finsky/layout/HeroGraphicView;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    iput-object p2, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;->val$heroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 6

    .prologue
    .line 73
    iget-object v3, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;->val$heroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/HeroGraphicView;->getWidth()I

    move-result v1

    .line 74
    .local v1, "imageWidth":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;->val$heroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/HeroGraphicView;->getCurrentAspectRatio()F

    move-result v2

    .line 75
    .local v2, "initialAspectRatio":F
    int-to-float v3, v1

    mul-float/2addr v3, v2

    float-to-int v0, v3

    .line 78
    .local v0, "imageHeight":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;->this$0:Lcom/google/android/finsky/layout/scroll/DetailsScrollView;

    # getter for: Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->mDefaultHeroImageHeight:I
    invoke-static {v5}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->access$000(Lcom/google/android/finsky/layout/scroll/DetailsScrollView;)I

    move-result v5

    sub-int v5, v0, v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/finsky/layout/scroll/DetailsScrollView;->scrollTo(II)V

    .line 79
    iget-object v3, p0, Lcom/google/android/finsky/layout/scroll/DetailsScrollView$1;->val$heroGraphicView:Lcom/google/android/finsky/layout/HeroGraphicView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/HeroGraphicView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 80
    const/4 v3, 0x1

    return v3
.end method
