.class public Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;
.super Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;
.source "FlagItemFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/FlagItemFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MusicFlagType"
.end annotation


# instance fields
.field private final mContentFlagType:I


# direct methods
.method private constructor <init>(III)V
    .locals 0
    .param p1, "contentFlagType"    # I
    .param p2, "stringId"    # I
    .param p3, "textEntryStringId"    # I

    .prologue
    .line 441
    invoke-direct {p0, p2, p3}, Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;-><init>(II)V

    .line 442
    iput p1, p0, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;->mContentFlagType:I

    .line 443
    return-void
.end method

.method public static getMusicFlags()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;",
            ">;"
        }
    .end annotation

    .prologue
    const v4, 0x7f0c0303

    .line 420
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 421
    .local v0, "output":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/activities/FlagItemFragment$FlagType;>;"
    new-instance v1, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;

    const/4 v2, 0x5

    const v3, 0x7f0c0302

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 423
    new-instance v1, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;

    const/4 v2, 0x1

    const v3, 0x7f0c02fa

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 425
    new-instance v1, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;

    const/4 v2, 0x4

    const v3, 0x7f0c02fc

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    new-instance v1, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;

    const/4 v2, 0x6

    const v3, 0x7f0c0301

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    new-instance v1, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;

    const/4 v2, 0x2

    const v3, 0x7f0c0300

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 431
    new-instance v1, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;

    const/16 v2, 0x8

    const v3, 0x7f0c02ff

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;-><init>(III)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 433
    return-object v0
.end method


# virtual methods
.method public postFlag(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "flagMessage"    # Ljava/lang/String;

    .prologue
    .line 447
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    .line 448
    .local v0, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;->mContentFlagType:I

    new-instance v4, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType$1;

    invoke-direct {v4, p0, p1}, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType$1;-><init>(Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;Landroid/content/Context;)V

    new-instance v5, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType$2;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType$2;-><init>(Lcom/google/android/finsky/activities/FlagItemFragment$MusicFlagType;)V

    move-object v3, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi;->flagContent(Ljava/lang/String;ILjava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 459
    return-void
.end method
