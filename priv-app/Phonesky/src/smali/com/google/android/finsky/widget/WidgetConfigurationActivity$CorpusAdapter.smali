.class Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;
.super Landroid/widget/BaseAdapter;
.source "WidgetConfigurationActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/widget/WidgetConfigurationActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CorpusAdapter"
.end annotation


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private final mActivityManager:Landroid/app/ActivityManager;

.field private final mAppWidgetId:I

.field private final mCorpora:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Toc$CorpusMetadata;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/List;I)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p3, "appWidgetId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/Toc$CorpusMetadata;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p2, "corpora":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Toc$CorpusMetadata;>;"
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 104
    iput-object p1, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mActivity:Landroid/app/Activity;

    .line 105
    iput-object p2, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mCorpora:Ljava/util/List;

    .line 106
    iput p3, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mAppWidgetId:I

    .line 107
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mActivity:Landroid/app/Activity;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mActivityManager:Landroid/app/ActivityManager;

    .line 108
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mAppWidgetId:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private getBackend(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 126
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->getItem(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    iget v0, v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    return v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mCorpora:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mCorpora:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->getItem(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->getBackend(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 131
    if-nez p2, :cond_0

    .line 132
    iget-object v6, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mActivity:Landroid/app/Activity;

    invoke-static {v6}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f0401d5

    const/4 v8, 0x0

    invoke-virtual {v6, v7, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 136
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;

    .line 137
    .local v4, "holder":Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;
    if-nez v4, :cond_1

    .line 138
    new-instance v4, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;

    .end local v4    # "holder":Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;
    invoke-direct {v4, p2}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;-><init>(Landroid/view/View;)V

    .line 141
    .restart local v4    # "holder":Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->getBackend(I)I

    move-result v0

    .line 142
    .local v0, "backend":I
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->getItem(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v1

    .line 144
    .local v1, "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    iget-object v6, v4, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;->name:Landroid/widget/TextView;

    iget-object v7, v1, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object v6, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mActivityManager:Landroid/app/ActivityManager;

    invoke-virtual {v6}, Landroid/app/ActivityManager;->getLauncherLargeIconDensity()I

    move-result v3

    .line 147
    .local v3, "density":I
    invoke-static {v0}, Lcom/google/android/finsky/widget/WidgetUtils;->getBackendIcon(I)I

    move-result v5

    .line 148
    .local v5, "icon":I
    iget-object v6, p0, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v5, v3}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 149
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    iget-object v6, v4, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;->icon:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 151
    iget-object v6, v4, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$Holder;->container:Landroid/view/ViewGroup;

    new-instance v7, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter$1;

    invoke-direct {v7, p0, v0}, Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter$1;-><init>(Lcom/google/android/finsky/widget/WidgetConfigurationActivity$CorpusAdapter;I)V

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    return-object p2
.end method
