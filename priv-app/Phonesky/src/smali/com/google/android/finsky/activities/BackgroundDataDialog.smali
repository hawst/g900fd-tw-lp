.class public Lcom/google/android/finsky/activities/BackgroundDataDialog;
.super Lcom/google/android/finsky/activities/SimpleAlertDialog;
.source "BackgroundDataDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/BackgroundDataDialog$BackgroundDataSettingListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/google/android/finsky/activities/BackgroundDataDialog$BackgroundDataSettingListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;-><init>()V

    .line 23
    return-void
.end method

.method public static dismissExisting(Landroid/support/v4/app/FragmentManager;)V
    .locals 3
    .param p0, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 57
    const-string v2, "bg_data_dialog"

    invoke-virtual {p0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 59
    .local v1, "previousBgDataDialog":Landroid/support/v4/app/Fragment;
    if-eqz v1, :cond_0

    move-object v0, v1

    .line 60
    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 61
    .local v0, "dialogFragment":Landroid/support/v4/app/DialogFragment;
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->dismiss()V

    .line 63
    .end local v0    # "dialogFragment":Landroid/support/v4/app/DialogFragment;
    :cond_0
    return-void
.end method

.method public static show(Landroid/support/v4/app/FragmentManager;Landroid/app/Activity;)V
    .locals 8
    .param p0, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p1, "mainActivity"    # Landroid/app/Activity;

    .prologue
    const/4 v5, 0x0

    const/4 v3, -0x1

    .line 32
    const-string v0, "bg_data_dialog"

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    :goto_0
    return-void

    .line 36
    :cond_0
    new-instance v7, Lcom/google/android/finsky/activities/BackgroundDataDialog;

    invoke-direct {v7}, Lcom/google/android/finsky/activities/BackgroundDataDialog;-><init>()V

    .line 38
    .local v7, "dialog":Lcom/google/android/finsky/activities/BackgroundDataDialog;
    instance-of v0, p1, Lcom/google/android/finsky/activities/BackgroundDataDialog$BackgroundDataSettingListener;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 39
    check-cast v0, Lcom/google/android/finsky/activities/BackgroundDataDialog$BackgroundDataSettingListener;

    iput-object v0, v7, Lcom/google/android/finsky/activities/BackgroundDataDialog;->mListener:Lcom/google/android/finsky/activities/BackgroundDataDialog$BackgroundDataSettingListener;

    .line 42
    :cond_1
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    const v1, 0x7f0c01dc

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setTitleId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c01de

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c01dd

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    sget-object v4, Lcom/google/android/finsky/config/G;->helpCenterBackgroundDataUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v5

    invoke-virtual {p1, v1, v2}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCanceledOnTouchOutside(Z)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x140

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v5

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v6

    .line 51
    .local v6, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v6, v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->configureDialog(Lcom/google/android/finsky/activities/SimpleAlertDialog;)V

    .line 53
    const-string v0, "bg_data_dialog"

    invoke-virtual {v7, p0, v0}, Lcom/google/android/finsky/activities/BackgroundDataDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected onNegativeClick()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->onNegativeClick()V

    .line 69
    iget-object v0, p0, Lcom/google/android/finsky/activities/BackgroundDataDialog;->mListener:Lcom/google/android/finsky/activities/BackgroundDataDialog$BackgroundDataSettingListener;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/finsky/activities/BackgroundDataDialog;->mListener:Lcom/google/android/finsky/activities/BackgroundDataDialog$BackgroundDataSettingListener;

    invoke-interface {v0}, Lcom/google/android/finsky/activities/BackgroundDataDialog$BackgroundDataSettingListener;->onBackgroundDataNotEnabled()V

    .line 72
    :cond_0
    return-void
.end method
