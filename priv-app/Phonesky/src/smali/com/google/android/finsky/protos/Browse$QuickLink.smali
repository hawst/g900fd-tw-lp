.class public final Lcom/google/android/finsky/protos/Browse$QuickLink;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Browse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Browse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QuickLink"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Browse$QuickLink;


# instance fields
.field public backendId:I

.field public displayRequired:Z

.field public hasBackendId:Z

.field public hasDisplayRequired:Z

.field public hasName:Z

.field public hasPrismStyle:Z

.field public hasServerLogsCookie:Z

.field public image:Lcom/google/android/finsky/protos/Common$Image;

.field public link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

.field public name:Ljava/lang/String;

.field public prismStyle:Z

.field public serverLogsCookie:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 730
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 731
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$QuickLink;->clear()Lcom/google/android/finsky/protos/Browse$QuickLink;

    .line 732
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 2

    .prologue
    .line 693
    sget-object v0, Lcom/google/android/finsky/protos/Browse$QuickLink;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    if-nez v0, :cond_1

    .line 694
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 696
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Browse$QuickLink;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    if-nez v0, :cond_0

    .line 697
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Browse$QuickLink;

    sput-object v0, Lcom/google/android/finsky/protos/Browse$QuickLink;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    .line 699
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 701
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Browse$QuickLink;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    return-object v0

    .line 699
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 735
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->name:Ljava/lang/String;

    .line 736
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasName:Z

    .line 737
    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 738
    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 739
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired:Z

    .line 740
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasDisplayRequired:Z

    .line 741
    iput v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId:I

    .line 742
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasBackendId:Z

    .line 743
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle:Z

    .line 744
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasPrismStyle:Z

    .line 745
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie:[B

    .line 746
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasServerLogsCookie:Z

    .line 747
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->cachedSize:I

    .line 748
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 780
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 781
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 782
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 785
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_2

    .line 786
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 789
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-eqz v1, :cond_3

    .line 790
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 793
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasDisplayRequired:Z

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired:Z

    if-eqz v1, :cond_5

    .line 794
    :cond_4
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 797
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasServerLogsCookie:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    .line 798
    :cond_6
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 801
    :cond_7
    iget v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId:I

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasBackendId:Z

    if-eqz v1, :cond_9

    .line 802
    :cond_8
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 805
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasPrismStyle:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle:Z

    if-eqz v1, :cond_b

    .line 806
    :cond_a
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 809
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Browse$QuickLink;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 817
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 818
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 822
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 823
    :sswitch_0
    return-object p0

    .line 828
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->name:Ljava/lang/String;

    .line 829
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasName:Z

    goto :goto_0

    .line 833
    :sswitch_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v2, :cond_1

    .line 834
    new-instance v2, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image:Lcom/google/android/finsky/protos/Common$Image;

    .line 836
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 840
    :sswitch_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-nez v2, :cond_2

    .line 841
    new-instance v2, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;-><init>()V

    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    .line 843
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 847
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired:Z

    .line 848
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasDisplayRequired:Z

    goto :goto_0

    .line 852
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie:[B

    .line 853
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasServerLogsCookie:Z

    goto :goto_0

    .line 857
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 858
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 870
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId:I

    .line 871
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasBackendId:Z

    goto :goto_0

    .line 877
    .end local v1    # "value":I
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle:Z

    .line 878
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasPrismStyle:Z

    goto :goto_0

    .line 818
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    .line 858
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 687
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Browse$QuickLink;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 754
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 755
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 757
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_2

    .line 758
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->image:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 760
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-eqz v0, :cond_3

    .line 761
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->link:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 763
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasDisplayRequired:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired:Z

    if-eqz v0, :cond_5

    .line 764
    :cond_4
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->displayRequired:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 766
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasServerLogsCookie:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_7

    .line 767
    :cond_6
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 769
    :cond_7
    iget v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId:I

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasBackendId:Z

    if-eqz v0, :cond_9

    .line 770
    :cond_8
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->backendId:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 772
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->hasPrismStyle:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle:Z

    if-eqz v0, :cond_b

    .line 773
    :cond_a
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$QuickLink;->prismStyle:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 775
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 776
    return-void
.end method
