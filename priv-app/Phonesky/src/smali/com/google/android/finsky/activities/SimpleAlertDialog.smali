.class public Lcom/google/android/finsky/activities/SimpleAlertDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "SimpleAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;,
        Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;,
        Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    }
.end annotation


# instance fields
.field private mConfigurableView:Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

.field private mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mHasBeenDismissed:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 85
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mConfigurableView:Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    .line 130
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/SimpleAlertDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SimpleAlertDialog;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->doPositiveClick()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/SimpleAlertDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SimpleAlertDialog;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->doNegativeClick()V

    return-void
.end method

.method private addViewResults(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 499
    iget-object v1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mConfigurableView:Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    if-eqz v1, :cond_1

    .line 500
    iget-object v1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mConfigurableView:Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    invoke-interface {v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;->getResult()Landroid/os/Bundle;

    move-result-object v0

    .line 501
    .local v0, "viewResultBundle":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 502
    if-nez p1, :cond_0

    .line 503
    new-instance p1, Landroid/os/Bundle;

    .end local p1    # "arguments":Landroid/os/Bundle;
    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 505
    .restart local p1    # "arguments":Landroid/os/Bundle;
    :cond_0
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 508
    .end local v0    # "viewResultBundle":Landroid/os/Bundle;
    :cond_1
    return-object p1
.end method

.method private buildAlertDialog(Landroid/os/Bundle;)Landroid/app/AlertDialog;
    .locals 8
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 379
    const-string v7, "theme_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "theme_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 382
    .local v6, "themeId":I
    :goto_0
    new-instance v2, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-direct {v2, v7, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 383
    .local v2, "context":Landroid/content/Context;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 385
    .local v1, "b":Landroid/app/AlertDialog$Builder;
    const-string v7, "title_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 386
    const-string v7, "title_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 389
    :cond_0
    const-string v7, "message_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 390
    const-string v7, "message_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 398
    :cond_1
    :goto_1
    const-string v7, "positive_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 399
    const-string v7, "positive_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 400
    .local v5, "positiveId":I
    new-instance v7, Lcom/google/android/finsky/activities/SimpleAlertDialog$2;

    invoke-direct {v7, p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$2;-><init>(Lcom/google/android/finsky/activities/SimpleAlertDialog;)V

    invoke-virtual {v1, v5, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 408
    .end local v5    # "positiveId":I
    :cond_2
    const-string v7, "negative_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 409
    const-string v7, "negative_id"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 410
    .local v4, "negativeId":I
    new-instance v7, Lcom/google/android/finsky/activities/SimpleAlertDialog$3;

    invoke-direct {v7, p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$3;-><init>(Lcom/google/android/finsky/activities/SimpleAlertDialog;)V

    invoke-virtual {v1, v4, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 419
    .end local v4    # "negativeId":I
    :cond_3
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 422
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    const-string v7, "layoutId"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 423
    invoke-direct {p0, v0, p1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setCustomLayout(Landroid/app/AlertDialog;Landroid/os/Bundle;)V

    .line 428
    :goto_2
    const-string v7, "cancel_on_touch_outside"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 429
    const-string v7, "cancel_on_touch_outside"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 433
    :cond_4
    return-object v0

    .line 379
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    .end local v1    # "b":Landroid/app/AlertDialog$Builder;
    .end local v2    # "context":Landroid/content/Context;
    .end local v6    # "themeId":I
    :cond_5
    const v6, 0x7f0d01b1

    goto :goto_0

    .line 391
    .restart local v1    # "b":Landroid/app/AlertDialog$Builder;
    .restart local v2    # "context":Landroid/content/Context;
    .restart local v6    # "themeId":I
    :cond_6
    const-string v7, "message"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 392
    const-string v7, "message"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 393
    :cond_7
    const-string v7, "messageHtml"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 394
    const-string v7, "messageHtml"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 395
    .local v3, "messageHtml":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_1

    .line 426
    .end local v3    # "messageHtml":Ljava/lang/String;
    .restart local v0    # "alertDialog":Landroid/app/AlertDialog;
    :cond_8
    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setMessageViewClickable(Landroid/app/AlertDialog;)V

    goto :goto_2
.end method

.method private doNegativeClick()V
    .locals 8

    .prologue
    const/4 v6, -0x1

    .line 472
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->dismiss()V

    .line 473
    iget-boolean v5, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    if-eqz v5, :cond_0

    .line 495
    :goto_0
    return-void

    .line 476
    :cond_0
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    .line 478
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 479
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v5, "target_request_code"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 480
    .local v4, "requestCode":I
    const-string v5, "extra_arguments"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 481
    .local v2, "extraArguments":Landroid/os/Bundle;
    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->addViewResults(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 482
    const-string v5, "click_event_type_negative"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 484
    .local v1, "clickEventTypeNegative":I
    if-eq v1, v6, :cond_1

    .line 485
    iget-object v5, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v5, v1, v6, v7}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 488
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    move-result-object v3

    .line 489
    .local v3, "l":Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    if-eqz v3, :cond_2

    .line 490
    invoke-interface {v3, v4, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;->onNegativeClick(ILandroid/os/Bundle;)V

    .line 494
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->onNegativeClick()V

    goto :goto_0
.end method

.method private doPositiveClick()V
    .locals 8

    .prologue
    const/4 v6, -0x1

    .line 446
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->dismiss()V

    .line 447
    iget-boolean v5, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    if-eqz v5, :cond_0

    .line 469
    :goto_0
    return-void

    .line 450
    :cond_0
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mHasBeenDismissed:Z

    .line 452
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 453
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v5, "target_request_code"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 454
    .local v4, "requestCode":I
    const-string v5, "extra_arguments"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 455
    .local v2, "extraArguments":Landroid/os/Bundle;
    invoke-direct {p0, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->addViewResults(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v2

    .line 456
    const-string v5, "click_event_type_positive"

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 458
    .local v1, "clickEventTypePositive":I
    if-eq v1, v6, :cond_1

    .line 459
    iget-object v5, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v5, v1, v6, v7}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 462
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    move-result-object v3

    .line 463
    .local v3, "l":Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    if-eqz v3, :cond_2

    .line 464
    invoke-interface {v3, v4, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;->onPositiveClick(ILandroid/os/Bundle;)V

    .line 468
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->onPositiveClick()V

    goto :goto_0
.end method

.method private getListener()Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
    .locals 3

    .prologue
    .line 527
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 528
    .local v1, "f":Landroid/support/v4/app/Fragment;
    instance-of v2, v1, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    if-eqz v2, :cond_0

    .line 529
    check-cast v1, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    .line 535
    .end local v1    # "f":Landroid/support/v4/app/Fragment;
    :goto_0
    return-object v1

    .line 531
    .restart local v1    # "f":Landroid/support/v4/app/Fragment;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 532
    .local v0, "a":Landroid/app/Activity;
    instance-of v2, v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    if-eqz v2, :cond_1

    .line 533
    check-cast v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;

    .end local v0    # "a":Landroid/app/Activity;
    move-object v1, v0

    goto :goto_0

    .line 535
    .restart local v0    # "a":Landroid/app/Activity;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setCustomLayout(Landroid/app/AlertDialog;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "alertDialog"    # Landroid/app/AlertDialog;
    .param p2, "arguments"    # Landroid/os/Bundle;

    .prologue
    .line 366
    const-string v3, "layoutId"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 367
    .local v1, "layoutId":I
    invoke-virtual {p1}, Landroid/app/AlertDialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 368
    .local v2, "view":Landroid/view/View;
    invoke-virtual {p1, v2}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 369
    instance-of v3, v2, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    if-eqz v3, :cond_0

    .line 370
    check-cast v2, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    .end local v2    # "view":Landroid/view/View;
    iput-object v2, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mConfigurableView:Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    .line 371
    const-string v3, "config_arguments"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 372
    const-string v3, "config_arguments"

    invoke-virtual {p2, v3}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 373
    .local v0, "configArguments":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mConfigurableView:Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;

    invoke-interface {v3, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;->configureView(Landroid/os/Bundle;)V

    .line 376
    .end local v0    # "configArguments":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method private setMessageViewClickable(Landroid/app/AlertDialog;)V
    .locals 1
    .param p1, "alertDialog"    # Landroid/app/AlertDialog;

    .prologue
    .line 339
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$1;-><init>(Lcom/google/android/finsky/activities/SimpleAlertDialog;Landroid/app/AlertDialog;)V

    invoke-virtual {p1, v0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 346
    return-void
.end method

.method private setUpEventLogger(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 350
    const-string v3, "log_account"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 351
    .local v0, "logAccount":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 352
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v0

    .line 354
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 357
    iput-object v4, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 358
    const-string v3, "impression_type"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 359
    const-string v3, "impression_type"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 360
    .local v2, "type":I
    const-string v3, "impression_cookie"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    .line 361
    .local v1, "serverLogsCookie":[B
    new-instance v3, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-direct {v3, v2, v1, v4, v4}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v3, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 363
    .end local v1    # "serverLogsCookie":[B
    .end local v2    # "type":I
    :cond_1
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 441
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 442
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->doNegativeClick()V

    .line 443
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 326
    .local v1, "arguments":Landroid/os/Bundle;
    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setUpEventLogger(Landroid/os/Bundle;)V

    .line 329
    if-nez p1, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    if-eqz v2, :cond_0

    .line 330
    iget-object v2, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v4, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog;->mDialogNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v2, v4, v5, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 333
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->buildAlertDialog(Landroid/os/Bundle;)Landroid/app/AlertDialog;

    move-result-object v0

    .line 335
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    return-object v0
.end method

.method protected onNegativeClick()V
    .locals 0

    .prologue
    .line 521
    return-void
.end method

.method protected onPositiveClick()V
    .locals 0

    .prologue
    .line 515
    return-void
.end method
