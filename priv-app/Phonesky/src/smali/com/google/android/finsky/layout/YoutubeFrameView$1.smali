.class Lcom/google/android/finsky/layout/YoutubeFrameView$1;
.super Ljava/lang/Object;
.source "YoutubeFrameView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/YoutubeFrameView;->showPlayIcon(Ljava/lang/String;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/YoutubeFrameView;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$backend:I

.field final synthetic val$isMature:Z

.field final synthetic val$parentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field final synthetic val$preferVideosAppForPlayback:Z

.field final synthetic val$youtubeUrl:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/YoutubeFrameView;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZLjava/lang/String;ILjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->this$0:Lcom/google/android/finsky/layout/YoutubeFrameView;

    iput-object p2, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$parentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iput-boolean p3, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$isMature:Z

    iput-object p4, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$accountName:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$backend:I

    iput-object p6, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$youtubeUrl:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$preferVideosAppForPlayback:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 83
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    const/16 v2, 0x78

    iget-object v3, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$parentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v1, v2, v4, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 86
    iget-boolean v1, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$isMature:Z

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$accountName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/utils/ClientMutationCache;->isAgeVerificationRequired()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    iget-object v1, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$accountName:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$backend:I

    invoke-static {v1, v2, v4}, Lcom/google/android/finsky/billing/lightpurchase/AgeVerificationActivity;->createIntent(Ljava/lang/String;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 95
    .local v0, "intent":Landroid/content/Intent;
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->this$0:Lcom/google/android/finsky/layout/YoutubeFrameView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/YoutubeFrameView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 96
    return-void

    .line 93
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->this$0:Lcom/google/android/finsky/layout/YoutubeFrameView;

    iget-object v2, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$youtubeUrl:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;->val$preferVideosAppForPlayback:Z

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/layout/YoutubeFrameView;->getClickIntent(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method
