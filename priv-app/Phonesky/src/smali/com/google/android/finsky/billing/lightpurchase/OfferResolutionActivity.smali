.class public Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "OfferResolutionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;
.implements Lcom/google/android/finsky/layout/play/RootUiElementNode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;,
        Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
    }
.end annotation


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

.field private mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 81
    const/16 v0, 0x30c

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 370
    return-void
.end method

.method private applyOfferFilter(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 249
    .local p1, "availableOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;>;"
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    if-nez v2, :cond_1

    .line 259
    :cond_0
    return-void

    .line 252
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 253
    .local v1, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;>;"
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 254
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    .line 255
    .local v0, "availableOffer":Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    iget v3, v3, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->matches(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 256
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0
.end method

.method public static createIntent(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/DocUtils$OfferFilter;)Landroid/content/Intent;
    .locals 3
    .param p0, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "docid"    # Ljava/lang/String;
    .param p3, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "offerFilter"    # Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    .prologue
    .line 92
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "OfferResolutionActivity.dfeToc"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 94
    const-string v1, "OfferResolutionActivity.account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 95
    const-string v1, "OfferResolutionActivity.docid"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 96
    const-string v1, "OfferResolutionActivity.doc"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 97
    if-eqz p4, :cond_0

    .line 98
    const-string v1, "OfferResolutionActivity.offerFilter"

    invoke-virtual {p4}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    :cond_0
    return-object v0
.end method

.method public static extractAvailableOffer(Landroid/content/Intent;)Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
    .locals 4
    .param p0, "resultIntent"    # Landroid/content/Intent;

    .prologue
    .line 330
    const-string v2, "OfferResolutionActivity.document"

    invoke-virtual {p0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 331
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    const-string v2, "OfferResolutionActivity.offer"

    invoke-static {p0, v2}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromIntent(Landroid/content/Intent;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Common$Offer;

    .line 332
    .local v1, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    new-instance v2, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;-><init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;)V

    return-object v2
.end method

.method private getAvailableOffers()Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 223
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 224
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;>;"
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->hasSubscriptions()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 225
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v9, v10}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/google/android/finsky/utils/DocUtils;->getSubscriptions(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Ljava/util/List;

    .line 227
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getSubscriptionsList()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/finsky/api/model/Document;

    .line 228
    .local v5, "subscription":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v5, v11}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v6

    .line 229
    .local v6, "subscriptionOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-nez v6, :cond_0

    .line 230
    const-string v7, "Skipping subscription doc, no PURCHASE offer: %s"

    new-array v8, v11, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 234
    :cond_0
    new-instance v7, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    invoke-direct {v7, v5, v6, v12}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;-><init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 237
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v5    # "subscription":Lcom/google/android/finsky/api/model/Document;
    .end local v6    # "subscriptionOffer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_1
    iget-object v7, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getAvailableOffers()[Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    .local v0, "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_3

    aget-object v3, v0, v1

    .line 239
    .local v3, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    iget v7, v3, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_2

    .line 237
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 242
    :cond_2
    new-instance v7, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    iget-object v8, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-direct {v7, v8, v3, v12}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;-><init>(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$1;)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 245
    .end local v0    # "arr$":[Lcom/google/android/finsky/protos/Common$Offer;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "offer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_3
    return-object v4
.end method

.method private hideLoading()V
    .locals 2

    .prologue
    .line 268
    const v0, 0x7f0a022d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 269
    const v0, 0x7f0a0109

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 270
    return-void
.end method

.method private showError(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 310
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 311
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c02a0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 312
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 313
    .local v1, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "OfferResolutionActivity.errorDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 314
    return-void
.end method

.method private showLoading()V
    .locals 4

    .prologue
    .line 262
    const v0, 0x7f0a022d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 263
    const v0, 0x7f0a0109

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v1, 0xd5

    invoke-virtual {v0, v2, v3, v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 265
    return-void
.end method

.method private updateFromDoc()V
    .locals 25

    .prologue
    .line 156
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->hideLoading()V

    .line 159
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v18

    .line 160
    .local v18, "serverLogsCookie":[B
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-object/from16 v20, v0

    const-wide/16 v22, 0x0

    const/16 v21, 0x30d

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    move/from16 v3, v21

    move-object/from16 v4, p0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 163
    const v20, 0x7f0a0245

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    .line 164
    .local v10, "container":Landroid/view/ViewGroup;
    invoke-virtual {v10}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 165
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getAvailableOffers()Ljava/util/List;

    move-result-object v7

    .line 166
    .local v7, "availableOffers":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->applyOfferFilter(Ljava/util/List;)V

    .line 167
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 168
    const v20, 0x7f0c00e5

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->showError(Ljava/lang/String;)V

    .line 220
    :cond_0
    return-void

    .line 171
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v8

    .line 172
    .local v8, "backend":I
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v16

    .line 173
    .local v16, "offerCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v15

    .line 174
    .local v15, "layoutInflater":Landroid/view/LayoutInflater;
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move/from16 v0, v16

    if-ge v14, v0, :cond_0

    .line 175
    invoke-interface {v7, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    .line 176
    .local v6, "availableOffer":Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
    const v20, 0x7f0400c8

    const/16 v21, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v15, v0, v10, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 178
    .local v11, "entry":Landroid/view/ViewGroup;
    const v20, 0x7f0a009c

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 179
    .local v19, "title":Landroid/widget/TextView;
    const v20, 0x7f0a0246

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 180
    .local v17, "price":Landroid/widget/TextView;
    const v20, 0x7f0a0247

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 181
    .local v12, "fullPrice":Landroid/widget/TextView;
    const v20, 0x7f0a010a

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 183
    .local v9, "byline":Landroid/widget/TextView;
    iget-object v0, v6, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 185
    iget-object v0, v6, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 188
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isHideSalesPricesExperimentEnabled()Z

    move-result v13

    .line 190
    .local v13, "hideSalePrices":Z
    iget-object v0, v6, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/google/android/finsky/utils/DocUtils;->hasDiscount(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v20

    if-eqz v20, :cond_3

    if-nez v13, :cond_3

    .line 191
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 192
    iget-object v0, v6, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    invoke-virtual {v12}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v20

    or-int/lit8 v20, v20, 0x10

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 194
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0c0125

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    iget-object v0, v6, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    invoke-virtual/range {v20 .. v22}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 198
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0c0126

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    iget-object v0, v6, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    move-object/from16 v24, v0

    aput-object v24, v22, v23

    invoke-virtual/range {v20 .. v22}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 205
    :goto_1
    iget-object v0, v6, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_4

    .line 206
    iget-object v0, v6, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Offer;->formattedDescription:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    :goto_2
    invoke-virtual {v11, v6}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 212
    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 215
    add-int/lit8 v20, v16, -0x1

    move/from16 v0, v20

    if-ge v14, v0, :cond_2

    .line 216
    const v20, 0x7f0400ca

    const/16 v21, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v15, v0, v10, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 174
    :cond_2
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 202
    :cond_3
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 208
    :cond_4
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 349
    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    return-void
.end method

.method public finish()V
    .locals 4

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v1, 0x25b

    invoke-virtual {v0, v2, v3, v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 367
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 368
    return-void
.end method

.method public flushImpression()V
    .locals 2

    .prologue
    .line 359
    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 360
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 344
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 274
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;

    .line 275
    .local v0, "availableOffer":Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 276
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "OfferResolutionActivity.document"

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 277
    const-string v2, "OfferResolutionActivity.offer"

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-static {v3}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 278
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v3, 0x30e

    iget-object v4, v0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity$AvailableOffer;->doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v4

    invoke-virtual {v2, v3, v4, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 280
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->setResult(ILandroid/content/Intent;)V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->finish()V

    .line 282
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 105
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    const v3, 0x7f0400c7

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->setContentView(I)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 109
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "OfferResolutionActivity.dfeToc"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/DfeToc;

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 110
    const-string v3, "OfferResolutionActivity.account"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/accounts/Account;

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mAccount:Landroid/accounts/Account;

    .line 111
    const v3, 0x7f0a009c

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 112
    .local v2, "titleView":Landroid/widget/TextView;
    const v3, 0x7f0c0208

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 114
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 115
    const-string v3, "OfferResolutionActivity.docid"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "docid":Ljava/lang/String;
    const-string v3, "OfferResolutionActivity.doc"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 117
    const-string v3, "OfferResolutionActivity.offerFilter"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    const-string v3, "OfferResolutionActivity.offerFilter"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/utils/DocUtils$OfferFilter;->valueOf(Ljava/lang/String;)Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mOfferFilter:Lcom/google/android/finsky/utils/DocUtils$OfferFilter;

    .line 121
    :cond_0
    if-nez p1, :cond_1

    .line 123
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 126
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v3, :cond_2

    .line 127
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->showLoading()V

    .line 128
    new-instance v3, Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mAccount:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 130
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 131
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 135
    :goto_0
    return-void

    .line 134
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->updateFromDoc()V

    goto :goto_0
.end method

.method public onDataChanged()V
    .locals 6

    .prologue
    .line 286
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 287
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v2, :cond_0

    .line 288
    const v2, 0x7f0c00e5

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->showError(Ljava/lang/String;)V

    .line 302
    :goto_0
    return-void

    .line 293
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v0

    .line 296
    .local v0, "isAvailable":Z
    if-nez v0, :cond_1

    .line 297
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v2}, Lcom/google/android/finsky/utils/DocUtils;->getAvailabilityRestrictionResourceId(Lcom/google/android/finsky/api/model/Document;)I

    move-result v1

    .line 298
    .local v1, "messageId":I
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->showError(Ljava/lang/String;)V

    goto :goto_0

    .line 301
    .end local v1    # "messageId":I
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->updateFromDoc()V

    goto :goto_0
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 306
    invoke-static {p0, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->showError(Ljava/lang/String;)V

    .line 307
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 327
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 320
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->setResult(I)V

    .line 321
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->finish()V

    .line 322
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 139
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 144
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/OfferResolutionActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 152
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 153
    return-void
.end method

.method public startNewImpression()V
    .locals 2

    .prologue
    .line 354
    const-string v0, "Not using impression id\'s."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 355
    return-void
.end method
