.class public final Lcom/google/android/finsky/protos/Browse$BrowseLink;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Browse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Browse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BrowseLink"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseLink;


# instance fields
.field public dataUrl:Ljava/lang/String;

.field public hasDataUrl:Z

.field public hasName:Z

.field public hasServerLogsCookie:Z

.field public name:Ljava/lang/String;

.field public serverLogsCookie:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 593
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 594
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->clear()Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 595
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .locals 2

    .prologue
    .line 570
    sget-object v0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-nez v0, :cond_1

    .line 571
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 573
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-nez v0, :cond_0

    .line 574
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Browse$BrowseLink;

    sput-object v0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 576
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 578
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->_emptyArray:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    return-object v0

    .line 576
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 598
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name:Ljava/lang/String;

    .line 599
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasName:Z

    .line 600
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->dataUrl:Ljava/lang/String;

    .line 601
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasDataUrl:Z

    .line 602
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie:[B

    .line 603
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasServerLogsCookie:Z

    .line 604
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->cachedSize:I

    .line 605
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 625
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 626
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 627
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 630
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasDataUrl:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->dataUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 631
    :cond_2
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->dataUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 634
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasServerLogsCookie:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 635
    :cond_4
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 638
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 646
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 647
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 651
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 652
    :sswitch_0
    return-object p0

    .line 657
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name:Ljava/lang/String;

    .line 658
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasName:Z

    goto :goto_0

    .line 662
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->dataUrl:Ljava/lang/String;

    .line 663
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasDataUrl:Z

    goto :goto_0

    .line 667
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie:[B

    .line 668
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasServerLogsCookie:Z

    goto :goto_0

    .line 647
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 564
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Browse$BrowseLink;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 611
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 612
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 614
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasDataUrl:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->dataUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 615
    :cond_2
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->dataUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 617
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->hasServerLogsCookie:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    .line 618
    :cond_4
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseLink;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 620
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 621
    return-void
.end method
