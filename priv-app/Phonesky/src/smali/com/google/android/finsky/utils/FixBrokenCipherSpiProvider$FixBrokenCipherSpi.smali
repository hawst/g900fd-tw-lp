.class public Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;
.super Ljavax/crypto/CipherSpi;
.source "FixBrokenCipherSpiProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FixBrokenCipherSpi"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi$AES;
    }
.end annotation


# instance fields
.field private mAlgorithm:Ljava/lang/String;

.field private mInstance:Ljavax/crypto/Cipher;

.field private mMode:Ljava/lang/String;

.field private mPadding:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "algorithm"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-direct {p0}, Ljavax/crypto/CipherSpi;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mAlgorithm:Ljava/lang/String;

    .line 90
    return-void
.end method

.method private getInstance()Ljavax/crypto/Cipher;
    .locals 11

    .prologue
    .line 93
    iget-object v8, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mInstance:Ljavax/crypto/Cipher;

    if-eqz v8, :cond_0

    .line 94
    iget-object v4, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mInstance:Ljavax/crypto/Cipher;

    .line 114
    :goto_0
    return-object v4

    .line 97
    :cond_0
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Cipher."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mAlgorithm:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "cipherAlg":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mMode:Ljava/lang/String;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mPadding:Ljava/lang/String;

    if-eqz v8, :cond_1

    .line 100
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mAlgorithm:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mMode:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mPadding:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 105
    .local v2, "fullCipher":Ljava/lang/String;
    :goto_1
    invoke-static {v1}, Ljava/security/Security;->getProviders(Ljava/lang/String;)[Ljava/security/Provider;

    move-result-object v7

    .line 106
    .local v7, "providers":[Ljava/security/Provider;
    move-object v0, v7

    .local v0, "arr$":[Ljava/security/Provider;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_2
    if-ge v3, v5, :cond_3

    aget-object v6, v0, v3

    .line 107
    .local v6, "provider":Ljava/security/Provider;
    instance-of v8, v6, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider;

    if-eqz v8, :cond_2

    .line 106
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 102
    .end local v0    # "arr$":[Ljava/security/Provider;
    .end local v2    # "fullCipher":Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v5    # "len$":I
    .end local v6    # "provider":Ljava/security/Provider;
    .end local v7    # "providers":[Ljava/security/Provider;
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mAlgorithm:Ljava/lang/String;

    .restart local v2    # "fullCipher":Ljava/lang/String;
    goto :goto_1

    .line 112
    .restart local v0    # "arr$":[Ljava/security/Provider;
    .restart local v3    # "i$":I
    .restart local v5    # "len$":I
    .restart local v6    # "provider":Ljava/security/Provider;
    .restart local v7    # "providers":[Ljava/security/Provider;
    :cond_2
    :try_start_0
    invoke-static {v2, v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;

    move-result-object v4

    .line 113
    .local v4, "instance":Ljavax/crypto/Cipher;
    iput-object v4, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mInstance:Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 115
    .end local v4    # "instance":Ljavax/crypto/Cipher;
    :catch_0
    move-exception v8

    goto :goto_3

    .line 123
    .end local v6    # "provider":Ljava/security/Provider;
    :cond_3
    new-instance v8, Ljava/lang/RuntimeException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "No other providers offer "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v8
.end method


# virtual methods
.method protected engineDoFinal([BII[BI)I
    .locals 6
    .param p1, "input"    # [B
    .param p2, "inputOffset"    # I
    .param p3, "inputLen"    # I
    .param p4, "output"    # [B
    .param p5, "outputOffset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/ShortBufferException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 198
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->doFinal([BII[BI)I

    move-result v0

    return v0
.end method

.method protected engineDoFinal([BII)[B
    .locals 1
    .param p1, "input"    # [B
    .param p2, "inputOffset"    # I
    .param p3, "inputLen"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v0

    return-object v0
.end method

.method protected engineGetBlockSize()I
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v0

    return v0
.end method

.method protected engineGetIV()[B
    .locals 1

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getIV()[B

    move-result-object v0

    return-object v0
.end method

.method protected engineGetOutputSize(I)I
    .locals 1
    .param p1, "inputLen"    # I

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v0

    return v0
.end method

.method protected engineGetParameters()Ljava/security/AlgorithmParameters;
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getParameters()Ljava/security/AlgorithmParameters;

    move-result-object v0

    return-object v0
.end method

.method protected engineInit(ILjava/security/Key;Ljava/security/AlgorithmParameters;Ljava/security/SecureRandom;)V
    .locals 1
    .param p1, "opmode"    # I
    .param p2, "key"    # Ljava/security/Key;
    .param p3, "params"    # Ljava/security/AlgorithmParameters;
    .param p4, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/AlgorithmParameters;Ljava/security/SecureRandom;)V

    .line 154
    return-void
.end method

.method protected engineInit(ILjava/security/Key;Ljava/security/SecureRandom;)V
    .locals 1
    .param p1, "opmode"    # I
    .param p2, "key"    # Ljava/security/Key;
    .param p3, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/SecureRandom;)V

    .line 140
    return-void
.end method

.method protected engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 1
    .param p1, "opmode"    # I
    .param p2, "key"    # Ljava/security/Key;
    .param p3, "params"    # Ljava/security/spec/AlgorithmParameterSpec;
    .param p4, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    .line 147
    return-void
.end method

.method protected engineSetMode(Ljava/lang/String;)V
    .locals 0
    .param p1, "mode"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mMode:Ljava/lang/String;

    .line 129
    return-void
.end method

.method protected engineSetPadding(Ljava/lang/String;)V
    .locals 0
    .param p1, "padding"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->mPadding:Ljava/lang/String;

    .line 134
    return-void
.end method

.method protected engineUpdate([BII[BI)I
    .locals 6
    .param p1, "input"    # [B
    .param p2, "inputOffset"    # I
    .param p3, "inputLen"    # I
    .param p4, "output"    # [B
    .param p5, "outputOffset"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/ShortBufferException;
        }
    .end annotation

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->update([BII[BI)I

    move-result v0

    return v0
.end method

.method protected engineUpdate([BII)[B
    .locals 2
    .param p1, "input"    # [B
    .param p2, "offset"    # I
    .param p3, "len"    # I

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider$FixBrokenCipherSpi;->getInstance()Ljavax/crypto/Cipher;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Ljavax/crypto/Cipher;->update([BII)[B

    move-result-object v0

    .line 179
    .local v0, "result":[B
    if-nez v0, :cond_0

    # getter for: Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider;->EMPTY_BYTE_ARRAY:[B
    invoke-static {}, Lcom/google/android/finsky/utils/FixBrokenCipherSpiProvider;->access$100()[B

    move-result-object v0

    .end local v0    # "result":[B
    :cond_0
    return-object v0
.end method
