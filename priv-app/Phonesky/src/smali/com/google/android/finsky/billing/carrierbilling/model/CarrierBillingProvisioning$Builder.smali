.class public Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
.super Ljava/lang/Object;
.source "CarrierBillingProvisioning.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private accountType:Ljava/lang/String;

.field private addressSnippet:Ljava/lang/String;

.field private apiVersion:I

.field private country:Ljava/lang/String;

.field private credentials:Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;

.field private encryptedSubscriberInfo:Lcom/google/android/finsky/billing/carrierbilling/model/EncryptedSubscriberInfo;

.field private isProvisioned:Z

.field private passwordForgotUrl:Ljava/lang/String;

.field private passwordPrompt:Ljava/lang/String;

.field private passwordRequired:Z

.field private provisioningId:Ljava/lang/String;

.field private subscriberCurrency:Ljava/lang/String;

.field private subscriberInfo:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

.field private tosUrl:Ljava/lang/String;

.field private tosVersion:Ljava/lang/String;

.field private transactionLimit:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;)V
    .locals 2
    .param p1, "original"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getApiVersion()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->apiVersion:I

    .line 178
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->isProvisioned()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->isProvisioned:Z

    .line 179
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getProvisioningId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->provisioningId:Ljava/lang/String;

    .line 180
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getTosUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->tosUrl:Ljava/lang/String;

    .line 181
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getTosVersion()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->tosVersion:Ljava/lang/String;

    .line 182
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getSubscriberCurrency()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->subscriberCurrency:Ljava/lang/String;

    .line 183
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getTransactionLimit()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->transactionLimit:J

    .line 184
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getAccountType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->accountType:Ljava/lang/String;

    .line 185
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getSubscriberInfo()Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->subscriberInfo:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    .line 186
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getCredentials()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->credentials:Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;

    .line 187
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->isPasswordRequired()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->passwordRequired:Z

    .line 188
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getPasswordPrompt()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->passwordPrompt:Ljava/lang/String;

    .line 189
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getPasswordForgotUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->passwordForgotUrl:Ljava/lang/String;

    .line 190
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getEncryptedSubscriberInfo()Lcom/google/android/finsky/billing/carrierbilling/model/EncryptedSubscriberInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->encryptedSubscriberInfo:Lcom/google/android/finsky/billing/carrierbilling/model/EncryptedSubscriberInfo;

    .line 191
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getAddressSnippet()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->addressSnippet:Ljava/lang/String;

    .line 192
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;->getCountry()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->country:Ljava/lang/String;

    .line 193
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->apiVersion:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->isProvisioned:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->passwordRequired:Z

    return v0
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->passwordPrompt:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->passwordForgotUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Lcom/google/android/finsky/billing/carrierbilling/model/EncryptedSubscriberInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->encryptedSubscriberInfo:Lcom/google/android/finsky/billing/carrierbilling/model/EncryptedSubscriberInfo;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->addressSnippet:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->country:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->provisioningId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->tosUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->tosVersion:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->subscriberCurrency:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-wide v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->transactionLimit:J

    return-wide v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->accountType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->subscriberInfo:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->credentials:Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;
    .locals 2

    .prologue
    .line 261
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning;-><init>(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$1;)V

    return-object v0
.end method

.method public setAccountType(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "accountType"    # Ljava/lang/String;

    .prologue
    .line 224
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->accountType:Ljava/lang/String;

    .line 225
    return-object p0
.end method

.method public setAddressSnippet(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "addressSnippet"    # Ljava/lang/String;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->addressSnippet:Ljava/lang/String;

    .line 253
    return-object p0
.end method

.method public setApiVersion(I)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "apiVersion"    # I

    .prologue
    .line 196
    iput p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->apiVersion:I

    .line 197
    return-object p0
.end method

.method public setCountry(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 256
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->country:Ljava/lang/String;

    .line 257
    return-object p0
.end method

.method public setCredentials(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "credentials"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->credentials:Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;

    .line 233
    return-object p0
.end method

.method public setEncryptedSubscriberInfo(Lcom/google/android/finsky/billing/carrierbilling/model/EncryptedSubscriberInfo;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "encryptedSubscriberInfo"    # Lcom/google/android/finsky/billing/carrierbilling/model/EncryptedSubscriberInfo;

    .prologue
    .line 248
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->encryptedSubscriberInfo:Lcom/google/android/finsky/billing/carrierbilling/model/EncryptedSubscriberInfo;

    .line 249
    return-object p0
.end method

.method public setIsProvisioned(Z)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "isProvisioned"    # Z

    .prologue
    .line 200
    iput-boolean p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->isProvisioned:Z

    .line 201
    return-object p0
.end method

.method public setPasswordForgotUrl(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "passwordForgotUrl"    # Ljava/lang/String;

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->passwordForgotUrl:Ljava/lang/String;

    .line 245
    return-object p0
.end method

.method public setPasswordPrompt(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "passwordPrompt"    # Ljava/lang/String;

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->passwordPrompt:Ljava/lang/String;

    .line 241
    return-object p0
.end method

.method public setPasswordRequired(Z)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "passwordRequired"    # Z

    .prologue
    .line 236
    iput-boolean p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->passwordRequired:Z

    .line 237
    return-object p0
.end method

.method public setProvisioningId(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "provisioningId"    # Ljava/lang/String;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->provisioningId:Ljava/lang/String;

    .line 205
    return-object p0
.end method

.method public setSubscriberCurrency(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "subscriberCurrency"    # Ljava/lang/String;

    .prologue
    .line 216
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->subscriberCurrency:Ljava/lang/String;

    .line 217
    return-object p0
.end method

.method public setSubscriberInfo(Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "subscriberInfo"    # Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->subscriberInfo:Lcom/google/android/finsky/billing/carrierbilling/model/SubscriberInfo;

    .line 229
    return-object p0
.end method

.method public setTosUrl(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "tosUrl"    # Ljava/lang/String;

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->tosUrl:Ljava/lang/String;

    .line 209
    return-object p0
.end method

.method public setTosVersion(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 0
    .param p1, "tosVersion"    # Ljava/lang/String;

    .prologue
    .line 212
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->tosVersion:Ljava/lang/String;

    .line 213
    return-object p0
.end method

.method public setTransactionLimit(J)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;
    .locals 1
    .param p1, "transactionLimit"    # J

    .prologue
    .line 220
    iput-wide p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingProvisioning$Builder;->transactionLimit:J

    .line 221
    return-object p0
.end method
