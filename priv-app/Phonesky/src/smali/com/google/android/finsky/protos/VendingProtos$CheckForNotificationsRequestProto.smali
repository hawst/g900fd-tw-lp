.class public final Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckForNotificationsRequestProto"
.end annotation


# instance fields
.field public alarmDuration:J

.field public hasAlarmDuration:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4274
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4275
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    .line 4276
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;
    .locals 2

    .prologue
    .line 4279
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->alarmDuration:J

    .line 4280
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->hasAlarmDuration:Z

    .line 4281
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->cachedSize:I

    .line 4282
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 4296
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4297
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->hasAlarmDuration:Z

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->alarmDuration:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 4298
    :cond_0
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->alarmDuration:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4301
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4309
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4310
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4314
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4315
    :sswitch_0
    return-object p0

    .line 4320
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->alarmDuration:J

    .line 4321
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->hasAlarmDuration:Z

    goto :goto_0

    .line 4310
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4253
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4288
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->hasAlarmDuration:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->alarmDuration:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 4289
    :cond_0
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$CheckForNotificationsRequestProto;->alarmDuration:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4291
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4292
    return-void
.end method
