.class public Lcom/google/android/finsky/layout/play/PlayListView;
.super Landroid/widget/ListView;
.source "PlayListView.java"


# static fields
.field public static final ENABLE_ANIMATION:Z

.field private static final ENABLE_LAYER_CALLS:Z


# instance fields
.field private mAlphaInterpolator:Landroid/view/animation/Interpolator;

.field private mAnimateChanges:Z

.field private mAnimateObserver:Landroid/database/DataSetObserver;

.field private mCapturedPositions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private mTempLocation:[I

.field private mTempRect:Landroid/graphics/RectF;

.field private mTranslationInterpolator:Landroid/view/animation/Interpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    .line 39
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_1

    :goto_1
    sput-boolean v1, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_LAYER_CALLS:Z

    return-void

    :cond_0
    move v0, v2

    .line 37
    goto :goto_0

    :cond_1
    move v1, v2

    .line 39
    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateChanges:Z

    .line 62
    sget-boolean v0, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayListView$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/play/PlayListView$1;-><init>(Lcom/google/android/finsky/layout/play/PlayListView;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateObserver:Landroid/database/DataSetObserver;

    .line 75
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mTranslationInterpolator:Landroid/view/animation/Interpolator;

    .line 77
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAlphaInterpolator:Landroid/view/animation/Interpolator;

    .line 78
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mCapturedPositions:Ljava/util/Map;

    .line 79
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    .line 80
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempRect:Landroid/graphics/RectF;

    .line 82
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/PlayListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayListView;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayListView;->captureCardPositions()V

    return-void
.end method

.method private animateIfNeeded()V
    .locals 1

    .prologue
    .line 222
    sget-boolean v0, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateChanges:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mCapturedPositions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    const/4 v0, 0x0

    invoke-direct {p0, p0, v0}, Lcom/google/android/finsky/layout/play/PlayListView;->traverse(Landroid/view/View;Z)V

    .line 224
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mCapturedPositions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 226
    :cond_0
    return-void
.end method

.method private capture(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 134
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayListView;->traverse(Landroid/view/View;Z)V

    .line 135
    return-void
.end method

.method private captureCardPositions()V
    .locals 1

    .prologue
    .line 120
    sget-boolean v0, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mCapturedPositions:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 123
    invoke-static {p0}, Lcom/google/android/finsky/utils/UiUtils;->isVisibleOnScreen(Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayListView;->getChildCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    invoke-direct {p0, p0}, Lcom/google/android/finsky/layout/play/PlayListView;->capture(Landroid/view/View;)V

    goto :goto_0
.end method

.method private disableClipChildren(Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "view"    # Landroid/view/ViewGroup;

    .prologue
    .line 213
    move-object v1, p1

    .line 214
    .local v1, "viewGroup":Landroid/view/ViewGroup;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 215
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 216
    .local v0, "parent":Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    if-eq p1, p0, :cond_0

    .line 217
    check-cast v0, Landroid/view/ViewGroup;

    .end local v0    # "parent":Landroid/view/ViewParent;
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/play/PlayListView;->disableClipChildren(Landroid/view/ViewGroup;)V

    .line 219
    :cond_0
    return-void
.end method

.method private traverse(Landroid/view/View;Z)V
    .locals 19
    .param p1, "view"    # Landroid/view/View;
    .param p2, "capture"    # Z

    .prologue
    .line 139
    move-object/from16 v0, p1

    instance-of v12, v0, Lcom/google/android/finsky/layout/play/Identifiable;

    if-eqz v12, :cond_5

    move-object/from16 v6, p1

    .line 140
    check-cast v6, Lcom/google/android/finsky/layout/play/Identifiable;

    .line 141
    .local v6, "identifiable":Lcom/google/android/finsky/layout/play/Identifiable;
    invoke-interface {v6}, Lcom/google/android/finsky/layout/play/Identifiable;->getIdentifier()Ljava/lang/String;

    move-result-object v5

    .line 142
    .local v5, "id":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 145
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->getLocationInWindow([I)V

    .line 146
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mCapturedPositions:Ljava/util/Map;

    new-instance v13, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    const/4 v15, 0x0

    aget v14, v14, v15

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    const/16 v16, 0x1

    aget v15, v15, v16

    int-to-float v15, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget v16, v16, v17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v17

    add-int v16, v16, v17

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    move-object/from16 v17, v0

    const/16 v18, 0x1

    aget v17, v17, v18

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v18

    add-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-direct/range {v13 .. v17}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-interface {v12, v5, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 151
    const/high16 v12, 0x3f800000    # 1.0f

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setAlpha(F)V

    .line 152
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setTranslationX(F)V

    .line 153
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setTranslationY(F)V

    .line 210
    .end local v5    # "id":Ljava/lang/String;
    .end local v6    # "identifiable":Lcom/google/android/finsky/layout/play/Identifiable;
    :cond_0
    :goto_0
    return-void

    .line 157
    .restart local v5    # "id":Ljava/lang/String;
    .restart local v6    # "identifiable":Lcom/google/android/finsky/layout/play/Identifiable;
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mCapturedPositions:Ljava/util/Map;

    invoke-interface {v12, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/RectF;

    .line 158
    .local v7, "originalPosition":Landroid/graphics/RectF;
    if-eqz v7, :cond_4

    .line 163
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->getLocationInWindow([I)V

    .line 164
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempRect:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    const/4 v14, 0x0

    aget v13, v13, v14

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    const/4 v15, 0x1

    aget v14, v14, v15

    int-to-float v14, v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    const/16 v16, 0x0

    aget v15, v15, v16

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v16

    add-int v15, v15, v16

    int-to-float v15, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempLocation:[I

    move-object/from16 v16, v0

    const/16 v17, 0x1

    aget v16, v16, v17

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v17

    add-int v16, v16, v17

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v12 .. v16}, Landroid/graphics/RectF;->set(FFFF)V

    .line 167
    invoke-virtual {v7}, Landroid/graphics/RectF;->centerX()F

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempRect:Landroid/graphics/RectF;

    invoke-virtual {v13}, Landroid/graphics/RectF;->centerX()F

    move-result v13

    sub-float v9, v12, v13

    .line 168
    .local v9, "tx":F
    invoke-virtual {v7}, Landroid/graphics/RectF;->centerY()F

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTempRect:Landroid/graphics/RectF;

    invoke-virtual {v13}, Landroid/graphics/RectF;->centerY()F

    move-result v13

    sub-float v10, v12, v13

    .line 169
    .local v10, "ty":F
    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const/high16 v13, 0x40a00000    # 5.0f

    cmpl-float v12, v12, v13

    if-gtz v12, :cond_2

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const/high16 v13, 0x40a00000    # 5.0f

    cmpl-float v12, v12, v13

    if-lez v12, :cond_0

    .line 172
    :cond_2
    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/view/View;->setTranslationX(F)V

    .line 173
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->setTranslationY(F)V

    .line 174
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    const-wide/16 v14, 0x96

    invoke-virtual {v12, v14, v15}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mTranslationInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v12, v13}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    const-wide/16 v14, 0x0

    invoke-virtual {v12, v14, v15}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 180
    .local v2, "animator":Landroid/view/ViewPropertyAnimator;
    sget-boolean v12, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_LAYER_CALLS:Z

    if-eqz v12, :cond_3

    .line 181
    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    .line 185
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    .line 186
    .local v8, "parent":Landroid/view/ViewParent;
    instance-of v12, v8, Landroid/view/ViewGroup;

    if-eqz v12, :cond_0

    .line 187
    check-cast v8, Landroid/view/ViewGroup;

    .end local v8    # "parent":Landroid/view/ViewParent;
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/google/android/finsky/layout/play/PlayListView;->disableClipChildren(Landroid/view/ViewGroup;)V

    goto/16 :goto_0

    .line 192
    .end local v2    # "animator":Landroid/view/ViewPropertyAnimator;
    .end local v9    # "tx":F
    .end local v10    # "ty":F
    :cond_4
    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/view/View;->setAlpha(F)V

    .line 193
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-virtual {v12, v13}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    const-wide/16 v14, 0x96

    invoke-virtual {v12, v14, v15}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/play/PlayListView;->mAlphaInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v12, v13}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    const-wide/16 v14, 0x96

    invoke-virtual {v12, v14, v15}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 198
    sget-boolean v12, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_LAYER_CALLS:Z

    if-eqz v12, :cond_0

    .line 199
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_0

    .line 203
    .end local v5    # "id":Ljava/lang/String;
    .end local v6    # "identifiable":Lcom/google/android/finsky/layout/play/Identifiable;
    .end local v7    # "originalPosition":Landroid/graphics/RectF;
    :cond_5
    move-object/from16 v0, p1

    instance-of v12, v0, Landroid/view/ViewGroup;

    if-eqz v12, :cond_0

    move-object/from16 v11, p1

    .line 204
    check-cast v11, Landroid/view/ViewGroup;

    .line 205
    .local v11, "viewGroup":Landroid/view/ViewGroup;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-virtual {v11}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v12

    if-ge v4, v12, :cond_0

    .line 206
    invoke-virtual {v11, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 207
    .local v3, "childView":Landroid/view/View;
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v3, v1}, Lcom/google/android/finsky/layout/play/PlayListView;->traverse(Landroid/view/View;Z)V

    .line 205
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 116
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayListView;->animateIfNeeded()V

    .line 117
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 36
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/play/PlayListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 104
    .local v0, "oldAdapter":Landroid/widget/ListAdapter;
    sget-boolean v1, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateChanges:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 105
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 107
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 108
    sget-boolean v1, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateChanges:Z

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 109
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateObserver:Landroid/database/DataSetObserver;

    invoke-interface {p1, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 111
    :cond_1
    return-void
.end method

.method public setAnimateChanges(Z)V
    .locals 2
    .param p1, "animateChanges"    # Z

    .prologue
    .line 88
    sget-boolean v1, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateChanges:Z

    if-eq v1, p1, :cond_0

    .line 89
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateChanges:Z

    .line 90
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 91
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    .line 92
    if-eqz p1, :cond_1

    .line 93
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 99
    .end local v0    # "adapter":Landroid/widget/ListAdapter;
    :cond_0
    :goto_0
    return-void

    .line 95
    .restart local v0    # "adapter":Landroid/widget/ListAdapter;
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayListView;->mAnimateObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    goto :goto_0
.end method
