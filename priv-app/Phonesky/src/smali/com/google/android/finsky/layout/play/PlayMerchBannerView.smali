.class public Lcom/google/android/finsky/layout/play/PlayMerchBannerView;
.super Lcom/google/android/play/layout/ForegroundRelativeLayout;
.source "PlayMerchBannerView.java"

# interfaces
.implements Lcom/google/android/finsky/adapters/Recyclable;
.implements Lcom/google/android/finsky/layout/play/Identifiable;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
.implements Lcom/google/android/play/image/FifeImageView$OnLoadedListener;


# instance fields
.field private mColumnCount:I

.field private final mCompactHeight:Z

.field private mContentHorizontalPadding:I

.field private final mFallbackMerchColor:I

.field private mIdentifier:Ljava/lang/String;

.field private mMerchColor:I

.field private mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

.field private mMinTextTrailingSpace:I

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mSubtitle:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 80
    .local v0, "resources":Landroid/content/res/Resources;
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    .line 81
    const v1, 0x7f0f000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mCompactHeight:Z

    .line 82
    const v1, 0x7f090065

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mFallbackMerchColor:I

    .line 83
    return-void
.end method

.method private clearImageFadingEdge()V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->clearFadingEdges()V

    .line 173
    return-void
.end method

.method private configureImageFadingEdge()V
    .locals 5

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x4

    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchColor:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->configureFadingEdges(ZZII)V

    .line 169
    return-void
.end method

.method private getFallbackMerchTextColor()I
    .locals 2

    .prologue
    .line 178
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchColor:I

    invoke-static {v1}, Lcom/google/android/finsky/utils/UiUtils;->isColorBright(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f0900c9

    .line 180
    .local v0, "textColorResourceId":I
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1

    .line 178
    .end local v0    # "textColorResourceId":I
    :cond_0
    const v0, 0x7f0900c8

    goto :goto_0
.end method

.method private getMerchImageOffset(I)I
    .locals 2
    .param p1, "height"    # I

    .prologue
    .line 187
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    .line 189
    const/4 v0, 0x0

    .line 192
    :goto_0
    return v0

    :cond_0
    int-to-float v0, p1

    const v1, 0x3fe38e39

    mul-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method private measureTexts(II)V
    .locals 13
    .param p1, "bannerWidth"    # I
    .param p2, "bannerHeight"    # I

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/4 v11, 0x0

    .line 204
    invoke-direct {p0, p2}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getMerchImageOffset(I)I

    move-result v2

    .line 205
    .local v2, "merchImageOffset":I
    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    const/4 v9, 0x4

    if-gt v8, v9, :cond_0

    const v4, 0x3f59999a    # 0.85f

    .line 206
    .local v4, "textImageNonOverlapRatio":F
    :goto_0
    neg-int v8, v2

    int-to-float v9, p2

    const v10, 0x3fe38e39

    mul-float/2addr v9, v10

    mul-float/2addr v9, v4

    float-to-int v9, v9

    add-int v5, v8, v9

    .line 211
    .local v5, "titleAreaLeft":I
    sub-int v8, p1, v5

    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mContentHorizontalPadding:I

    sub-int v7, v8, v9

    .line 212
    .local v7, "titleWidth":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-static {v7, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v11}, Landroid/widget/TextView;->measure(II)V

    .line 214
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-static {v7, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v9, v11}, Landroid/widget/TextView;->measure(II)V

    .line 217
    const/4 v1, 0x0

    .line 218
    .local v1, "maxTextWidth":I
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v6

    .line 219
    .local v6, "titleLayout":Landroid/text/Layout;
    if-eqz v6, :cond_1

    .line 220
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v6}, Landroid/text/Layout;->getLineCount()I

    move-result v8

    if-ge v0, v8, :cond_1

    .line 221
    invoke-virtual {v6, v0}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v8

    float-to-int v8, v8

    invoke-static {v1, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 205
    .end local v0    # "i":I
    .end local v1    # "maxTextWidth":I
    .end local v4    # "textImageNonOverlapRatio":F
    .end local v5    # "titleAreaLeft":I
    .end local v6    # "titleLayout":Landroid/text/Layout;
    .end local v7    # "titleWidth":I
    :cond_0
    const/high16 v4, 0x3f800000    # 1.0f

    goto :goto_0

    .line 224
    .restart local v1    # "maxTextWidth":I
    .restart local v4    # "textImageNonOverlapRatio":F
    .restart local v5    # "titleAreaLeft":I
    .restart local v6    # "titleLayout":Landroid/text/Layout;
    .restart local v7    # "titleWidth":I
    :cond_1
    iget-object v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v8}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v3

    .line 225
    .local v3, "subtitleLayout":Landroid/text/Layout;
    if-eqz v3, :cond_2

    .line 226
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    invoke-virtual {v3}, Landroid/text/Layout;->getLineCount()I

    move-result v8

    if-ge v0, v8, :cond_2

    .line 227
    invoke-virtual {v3, v0}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v8

    float-to-int v8, v8

    invoke-static {v1, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 231
    .end local v0    # "i":I
    :cond_2
    if-nez v1, :cond_3

    .line 233
    move v1, v7

    .line 235
    :cond_3
    sub-int v8, v7, v1

    iput v8, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMinTextTrailingSpace:I

    .line 236
    return-void
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 340
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public configureMerch(Lcom/google/android/finsky/protos/DocumentV2$NextBanner;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;[B)V
    .locals 7
    .param p1, "banner"    # Lcom/google/android/finsky/protos/DocumentV2$NextBanner;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "merchImage"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p4, "onClickListener"    # Landroid/view/View$OnClickListener;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p6, "serverLogsCookie"    # [B

    .prologue
    const/4 v2, 0x0

    .line 124
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mFallbackMerchColor:I

    invoke-static {p3, v0}, Lcom/google/android/finsky/utils/UiUtils;->getFillColor(Lcom/google/android/finsky/protos/Common$Image;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchColor:I

    .line 126
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setOnLoadedListener(Lcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    iget-object v1, p3, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v3, p3, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v0, v1, v3, p2}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 131
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->configureImageFadingEdge()V

    .line 138
    :goto_0
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    new-instance v1, Landroid/graphics/drawable/PaintDrawable;

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchColor:I

    invoke-direct {v1, v3}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingBottom()I

    move-result v5

    move v4, v2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/DocumentV2$NextBanner;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/protos/DocumentV2$NextBanner;->subtitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getFallbackMerchTextColor()I

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/UiUtils;->getTextColor(Lcom/google/android/finsky/protos/DocumentV2$NextBanner;I)I

    move-result v6

    .line 145
    .local v6, "textColor":I
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 148
    invoke-virtual {p0, p4}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    const/16 v0, 0x199

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v0, p6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 154
    iput-object p5, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 157
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 163
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->requestLayout()V

    .line 164
    return-void

    .line 133
    .end local v6    # "textColor":I
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->clearImageFadingEdge()V

    goto :goto_0
.end method

.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public init(II)V
    .locals 2
    .param p1, "columnCount"    # I
    .param p2, "contentHorizontalPadding"    # I

    .prologue
    .line 108
    if-gtz p1, :cond_0

    .line 109
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Merch banner doesn\'t support non-positive number of columns: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " passed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    .line 114
    iput p2, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mContentHorizontalPadding:I

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 97
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onFinishInflate()V

    .line 99
    const v0, 0x7f0a02cb

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/FadingEdgeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    .line 100
    const v0, 0x7f0a02f5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    .line 101
    const v0, 0x7f0a02f6

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    .line 102
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getWidth()I

    move-result v10

    .line 292
    .local v10, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getHeight()I

    move-result v0

    .line 294
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingTop()I

    move-result v5

    .line 295
    .local v5, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingBottom()I

    move-result v4

    .line 297
    .local v4, "paddingBottom":I
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v11}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->getMeasuredWidth()I

    move-result v2

    .line 298
    .local v2, "merchImageWidth":I
    if-lez v2, :cond_0

    .line 299
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getMerchImageOffset(I)I

    move-result v3

    .line 300
    .local v3, "offset":I
    neg-int v11, v3

    add-int v1, v11, v2

    .line 301
    .local v1, "merchImageRight":I
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    neg-int v12, v3

    iget-object v13, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v5

    invoke-virtual {v11, v12, v5, v1, v13}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->layout(IIII)V

    .line 308
    .end local v1    # "merchImageRight":I
    .end local v3    # "offset":I
    :goto_0
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v9

    .line 309
    .local v9, "titleHeight":I
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v11}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    .line 311
    .local v6, "subtitleHeight":I
    sub-int v11, v0, v9

    sub-int/2addr v11, v6

    sub-int/2addr v11, v5

    sub-int/2addr v11, v4

    div-int/lit8 v11, v11, 0x2

    add-int v8, v5, v11

    .line 313
    .local v8, "textY":I
    iget v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mContentHorizontalPadding:I

    sub-int v11, v10, v11

    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v12

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMinTextTrailingSpace:I

    div-int/lit8 v12, v12, 0x2

    add-int v7, v11, v12

    .line 315
    .local v7, "textX":I
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v12, v7

    add-int v13, v8, v9

    invoke-virtual {v11, v7, v8, v12, v13}, Landroid/widget/TextView;->layout(IIII)V

    .line 316
    add-int/2addr v8, v9

    .line 317
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v12, v7

    add-int v13, v8, v6

    invoke-virtual {v11, v7, v8, v12, v13}, Landroid/widget/TextView;->layout(IIII)V

    .line 319
    return-void

    .line 304
    .end local v6    # "subtitleHeight":I
    .end local v7    # "textX":I
    .end local v8    # "textY":I
    .end local v9    # "titleHeight":I
    :cond_0
    iget-object v11, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v13}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->getMeasuredHeight()I

    move-result v13

    add-int/2addr v13, v5

    invoke-virtual {v11, v12, v5, v2, v13}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->layout(IIII)V

    goto :goto_0
.end method

.method public onLoaded(Lcom/google/android/play/image/FifeImageView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->configureImageFadingEdge()V

    .line 241
    return-void
.end method

.method public onLoadedAndFadedIn(Lcom/google/android/play/image/FifeImageView;)V
    .locals 0
    .param p1, "imageView"    # Lcom/google/android/play/image/FifeImageView;

    .prologue
    .line 245
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 249
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 250
    .local v0, "availableWidth":I
    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    if-gtz v6, :cond_0

    .line 252
    const/4 v6, 0x0

    invoke-virtual {p0, v0, v6}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->setMeasuredDimension(II)V

    .line 287
    :goto_0
    return-void

    .line 259
    :cond_0
    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mColumnCount:I

    div-int v3, v0, v6

    .line 260
    .local v3, "fullHeight":I
    move v2, v3

    .line 261
    .local v2, "finalContentHeight":I
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mCompactHeight:Z

    if-eqz v6, :cond_2

    .line 262
    mul-int/lit8 v6, v3, 0x2

    div-int/lit8 v1, v6, 0x3

    .line 263
    .local v1, "compactHeight":I
    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->measureTexts(II)V

    .line 264
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v6

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mSubtitle:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    add-int v5, v6, v7

    .line 265
    .local v5, "textHeights":I
    if-le v5, v1, :cond_1

    .line 267
    invoke-direct {p0, v0, v3}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->measureTexts(II)V

    .line 276
    .end local v1    # "compactHeight":I
    .end local v5    # "textHeights":I
    :goto_1
    const v6, 0x3fe38e39

    int-to-float v7, v2

    mul-float/2addr v6, v7

    float-to-int v4, v6

    .line 277
    .local v4, "merchImageWidth":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->measure(II)V

    .line 279
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v6}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->isLoaded()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 280
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->configureImageFadingEdge()V

    .line 285
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingTop()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->getPaddingBottom()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {p0, v0, v6}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 270
    .end local v4    # "merchImageWidth":I
    .restart local v1    # "compactHeight":I
    .restart local v5    # "textHeights":I
    :cond_1
    move v2, v1

    goto :goto_1

    .line 273
    .end local v1    # "compactHeight":I
    .end local v5    # "textHeights":I
    :cond_2
    invoke-direct {p0, v0, v3}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->measureTexts(II)V

    goto :goto_1

    .line 282
    .restart local v4    # "merchImageWidth":I
    :cond_3
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->clearImageFadingEdge()V

    goto :goto_2
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mMerchImage:Lcom/google/android/finsky/layout/FadingEdgeImageView;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/FadingEdgeImageView;->clearImage()V

    .line 324
    return-void
.end method

.method public setIdentifier(Ljava/lang/String;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayMerchBannerView;->mIdentifier:Ljava/lang/String;

    .line 88
    return-void
.end method
