.class Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;
.super Ljava/lang/Object;
.source "DetailsSummaryMusicViewBinder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->displayActionButtonsIfNecessary(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

.field final synthetic val$account:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iput-object p2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->val$account:Landroid/accounts/Account;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 111
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    const/16 v3, 0x11d

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v5, v5, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 113
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v2, v2, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    .line 114
    .local v0, "backendId":I
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v2, v2, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/IntentUtils;->isConsumptionAppInstalled(Landroid/content/pm/PackageManager;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 117
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v2, v2, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showAppNeededDialog(I)V

    .line 124
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v2, v2, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v3, v3, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->val$account:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/finsky/utils/IntentUtils;->buildConsumptionAppViewItemIntent(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 122
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 123
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder$2;->this$0:Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;

    iget-object v2, v2, Lcom/google/android/finsky/activities/DetailsSummaryMusicViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
