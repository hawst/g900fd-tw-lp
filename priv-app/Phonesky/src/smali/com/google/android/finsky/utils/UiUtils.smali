.class public Lcom/google/android/finsky/utils/UiUtils;
.super Ljava/lang/Object;
.source "UiUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;
    }
.end annotation


# static fields
.field private static sPaddingRequestHandler:Landroid/os/Handler;

.field private static sPendingPaddingRequests:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static sTempRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 67
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/finsky/utils/UiUtils;->sTempRect:Landroid/graphics/Rect;

    .line 69
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/UiUtils;->sPendingPaddingRequests:Ljava/util/Map;

    .line 72
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/finsky/utils/UiUtils;->sPaddingRequestHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/ObservableScrollView;
    .param p1, "x1"    # Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .param p2, "x2"    # Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;
    .param p3, "x3"    # I

    .prologue
    .line 55
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/finsky/utils/UiUtils;->syncActionBarAlpha(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V

    return-void
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/finsky/utils/UiUtils;->sPendingPaddingRequests:Ljava/util/Map;

    return-object v0
.end method

.method public static disableActionBarOverlayScrolling(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;Lcom/google/android/finsky/layout/actionbar/ActionBarController;)V
    .locals 2
    .param p0, "scrollView"    # Lcom/google/android/finsky/layout/ObservableScrollView;
    .param p1, "scrollListener"    # Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
    .param p2, "actionBarController"    # Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    .prologue
    .line 547
    invoke-interface {p2}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->disableActionBarOverlay()V

    .line 548
    const/16 v0, 0xff

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->setActionBarAlpha(IZ)V

    .line 549
    if-eqz p0, :cond_0

    .line 550
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/ObservableScrollView;->removeOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V

    .line 552
    :cond_0
    return-void
.end method

.method public static enableActionBarOverlayScrolling(Lcom/google/android/finsky/layout/ObservableScrollView;ILcom/google/android/finsky/layout/actionbar/ActionBarController;)Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
    .locals 4
    .param p0, "scrollView"    # Lcom/google/android/finsky/layout/ObservableScrollView;
    .param p1, "heroImageResourceId"    # I
    .param p2, "actionBarController"    # Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    .prologue
    .line 449
    invoke-interface {p2}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->enableActionBarOverlay()V

    .line 451
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/ObservableScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    .line 457
    .local v0, "heroImageHost":Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ObservableScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/utils/UiUtils$5;

    invoke-direct {v3, p0, p2, v0}, Lcom/google/android/finsky/utils/UiUtils$5;-><init>(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 469
    new-instance v1, Lcom/google/android/finsky/utils/UiUtils$6;

    invoke-direct {v1, p0, p2, v0}, Lcom/google/android/finsky/utils/UiUtils$6;-><init>(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;)V

    .line 476
    .local v1, "scrollListener":Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/ObservableScrollView;->addOnScrollListener(Lcom/google/android/finsky/layout/ObservableScrollView$ScrollListener;)V

    .line 477
    return-object v1
.end method

.method public static enableActionBarOverlayScrolling(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;)V
    .locals 1
    .param p0, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p1, "actionBarController"    # Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    .prologue
    const/4 v0, 0x0

    .line 366
    invoke-interface {p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->enableActionBarOverlay()V

    .line 367
    invoke-interface {p1, v0, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->setActionBarAlpha(IZ)V

    .line 368
    new-instance v0, Lcom/google/android/finsky/utils/UiUtils$4;

    invoke-direct {v0, p1}, Lcom/google/android/finsky/utils/UiUtils$4;-><init>(Lcom/google/android/finsky/layout/actionbar/ActionBarController;)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(Landroid/support/v7/widget/RecyclerView$OnScrollListener;)V

    .line 427
    return-void
.end method

.method public static fadeOutCluster(Landroid/view/View;Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;J)V
    .locals 2
    .param p0, "cluster"    # Landroid/view/View;
    .param p1, "listener"    # Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;
    .param p2, "animationDelay"    # J

    .prologue
    .line 339
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/finsky/utils/UiUtils$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/utils/UiUtils$3;-><init>(Landroid/view/View;Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;)V

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 355
    return-void
.end method

.method public static findSharedElementView(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "transitionNamePrefix"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 587
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    move-object p0, v4

    .line 604
    .end local p0    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-object p0

    .line 590
    .restart local p0    # "view":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v3

    .line 591
    .local v3, "viewTransitionName":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v3, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 594
    :cond_2
    instance-of v5, p0, Landroid/view/ViewGroup;

    if-eqz v5, :cond_4

    move-object v2, p0

    .line 595
    check-cast v2, Landroid/view/ViewGroup;

    .line 596
    .local v2, "viewGroup":Landroid/view/ViewGroup;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    if-ge v0, v5, :cond_4

    .line 597
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/google/android/finsky/utils/UiUtils;->findSharedElementView(Landroid/view/View;Ljava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 599
    .local v1, "sharedElementView":Landroid/view/View;
    if-eqz v1, :cond_3

    move-object p0, v1

    .line 600
    goto :goto_0

    .line 596
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "sharedElementView":Landroid/view/View;
    .end local v2    # "viewGroup":Landroid/view/ViewGroup;
    :cond_4
    move-object p0, v4

    .line 604
    goto :goto_0
.end method

.method public static getActionBarHeight(Landroid/content/Context;)I
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 555
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f0d01ac

    const/4 v4, 0x1

    new-array v4, v4, [I

    const v5, 0x7f010098

    aput v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 557
    .local v1, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v1, v6, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 558
    .local v0, "actionBarHeight":I
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 560
    return v0
.end method

.method private static getColor(Ljava/lang/String;I)I
    .locals 4
    .param p0, "colorRgb"    # Ljava/lang/String;
    .param p1, "fallbackColor"    # I

    .prologue
    .line 156
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 158
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 168
    .end local p1    # "fallbackColor":I
    :cond_0
    return p1

    .line 159
    .restart local p1    # "fallbackColor":I
    :catch_0
    move-exception v0

    .line 160
    .local v0, "exc":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/google/android/finsky/api/DfeApiConfig;->showStagingData:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad color: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    throw v0
.end method

.method public static getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I
    .locals 5
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "percent"    # D

    .prologue
    .line 96
    const v2, 0x7f0f000a

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    invoke-static {p0}, Lcom/google/android/finsky/utils/UiUtils;->getGridColumnContentWidth(Landroid/content/res/Resources;)I

    move-result v1

    .line 98
    .local v1, "contentWidth":I
    const v2, 0x7f0b0138

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 99
    .local v0, "cellWidth":I
    int-to-double v2, v1

    mul-double/2addr v2, p1

    double-to-int v2, v2

    div-int/2addr v2, v0

    const/4 v3, 0x5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 102
    .end local v0    # "cellWidth":I
    .end local v1    # "contentWidth":I
    :goto_0
    return v2

    :cond_0
    const v2, 0x7f0e0013

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    goto :goto_0
.end method

.method public static getFillColor(Lcom/google/android/finsky/protos/Common$Image;I)I
    .locals 1
    .param p0, "image"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p1, "fallbackColor"    # I

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/finsky/protos/Common$Image;->fillColorRgb:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/UiUtils;->getColor(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getFillColor(Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;I)I
    .locals 1
    .param p0, "editorial"    # Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
    .param p1, "fallbackColor"    # I

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/UiUtils;->getColor(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getFillColor(Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;I)I
    .locals 1
    .param p0, "antenna"    # Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
    .param p1, "fallbackColor"    # I

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->colorThemeArgb:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/UiUtils;->getColor(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static getGridColumnContentWidth(Landroid/content/res/Resources;)I
    .locals 2
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 91
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 92
    .local v0, "displayWidth":I
    invoke-static {p0}, Lcom/google/android/finsky/utils/UiUtils;->getGridHorizontalPadding(Landroid/content/res/Resources;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    return v1
.end method

.method public static getGridHorizontalPadding(Landroid/content/res/Resources;)I
    .locals 4
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 82
    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v0, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 83
    .local v0, "displayWidth":I
    const v3, 0x7f0b0087

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 85
    .local v2, "paddingLeftRight":I
    const v3, 0x7f0b0088

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 86
    .local v1, "maxContentWidth":I
    sub-int v3, v0, v1

    div-int/lit8 v3, v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    return v3
.end method

.method public static getRegularGridColumnCount(Landroid/content/res/Resources;)I
    .locals 4
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 107
    const v2, 0x7f0f000a

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    invoke-static {p0}, Lcom/google/android/finsky/utils/UiUtils;->getGridColumnContentWidth(Landroid/content/res/Resources;)I

    move-result v1

    .line 109
    .local v1, "contentWidth":I
    const v2, 0x7f0b0138

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 110
    .local v0, "cellWidth":I
    div-int v2, v1, v0

    const/4 v3, 0x5

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 112
    .end local v0    # "cellWidth":I
    .end local v1    # "contentWidth":I
    :goto_0
    return v2

    :cond_0
    const v2, 0x7f0e0014

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    goto :goto_0
.end method

.method public static getStreamQuickLinkColumnCount(Landroid/content/res/Resources;II)I
    .locals 10
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "requiredLinkCount"    # I
    .param p2, "optionalLinkCount"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 118
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {p0, v6, v7}, Lcom/google/android/finsky/utils/UiUtils;->getFeaturedGridColumnCount(Landroid/content/res/Resources;D)I

    move-result v0

    .line 119
    .local v0, "gridColumnCount":I
    if-le p1, v0, :cond_2

    .line 123
    move v3, v0

    .line 125
    .local v3, "result":I
    :goto_0
    rem-int v1, p1, v3

    .line 126
    .local v1, "leading":I
    if-nez v1, :cond_1

    .line 151
    .end local v1    # "leading":I
    .end local v3    # "result":I
    :cond_0
    :goto_1
    return v3

    .line 130
    .restart local v1    # "leading":I
    .restart local v3    # "result":I
    :cond_1
    sub-int v5, v3, v1

    .line 131
    .local v5, "trailingGap":I
    if-le v5, v8, :cond_0

    .line 134
    if-le v3, v9, :cond_0

    .line 138
    add-int/lit8 v3, v3, -0x1

    .line 139
    goto :goto_0

    .line 144
    .end local v1    # "leading":I
    .end local v3    # "result":I
    .end local v5    # "trailingGap":I
    :cond_2
    add-int v4, p1, p2

    .line 145
    .local v4, "totalLinkCount":I
    if-ne v4, v8, :cond_3

    if-ne v0, v9, :cond_3

    move v3, v0

    .line 148
    goto :goto_1

    .line 150
    :cond_3
    int-to-float v6, v0

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    .line 151
    .local v2, "minNumberOfColumns":I
    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v6

    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    goto :goto_1
.end method

.method public static getTextColor(Lcom/google/android/finsky/protos/DocumentV2$NextBanner;I)I
    .locals 1
    .param p0, "banner"    # Lcom/google/android/finsky/protos/DocumentV2$NextBanner;
    .param p1, "fallbackColor"    # I

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocumentV2$NextBanner;->colorTextArgb:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/UiUtils;->getColor(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static hideKeyboard(Landroid/app/Activity;Landroid/view/View;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 247
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 251
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 252
    return-void
.end method

.method public static interpolateColor(IIF)I
    .locals 16
    .param p0, "color1"    # I
    .param p1, "color2"    # I
    .param p2, "color1Likeness"    # F

    .prologue
    .line 616
    shr-int/lit8 v14, p0, 0x18

    and-int/lit16 v1, v14, 0xff

    .line 617
    .local v1, "alpha1":I
    shr-int/lit8 v14, p0, 0x10

    and-int/lit16 v12, v14, 0xff

    .line 618
    .local v12, "red1":I
    shr-int/lit8 v14, p0, 0x8

    and-int/lit16 v9, v14, 0xff

    .line 619
    .local v9, "green1":I
    move/from16 v0, p0

    and-int/lit16 v7, v0, 0xff

    .line 621
    .local v7, "blue1":I
    shr-int/lit8 v14, p1, 0x18

    and-int/lit16 v2, v14, 0xff

    .line 622
    .local v2, "alpha2":I
    shr-int/lit8 v14, p1, 0x10

    and-int/lit16 v13, v14, 0xff

    .line 623
    .local v13, "red2":I
    shr-int/lit8 v14, p1, 0x8

    and-int/lit16 v10, v14, 0xff

    .line 624
    .local v10, "green2":I
    move/from16 v0, p1

    and-int/lit16 v8, v0, 0xff

    .line 627
    .local v8, "blue2":I
    const/high16 v14, 0x3f800000    # 1.0f

    sub-float v11, v14, p2

    .line 628
    .local v11, "invertedLikeness":F
    int-to-float v14, v1

    mul-float v14, v14, p2

    int-to-float v15, v2

    mul-float/2addr v15, v11

    add-float/2addr v14, v15

    float-to-int v3, v14

    .line 629
    .local v3, "blendedAlpha":I
    int-to-float v14, v12

    mul-float v14, v14, p2

    int-to-float v15, v13

    mul-float/2addr v15, v11

    add-float/2addr v14, v15

    float-to-int v6, v14

    .line 630
    .local v6, "blendedRed":I
    int-to-float v14, v9

    mul-float v14, v14, p2

    int-to-float v15, v10

    mul-float/2addr v15, v11

    add-float/2addr v14, v15

    float-to-int v5, v14

    .line 631
    .local v5, "blendedGreen":I
    int-to-float v14, v7

    mul-float v14, v14, p2

    int-to-float v15, v8

    mul-float/2addr v15, v11

    add-float/2addr v14, v15

    float-to-int v4, v14

    .line 634
    .local v4, "blendedBlue":I
    shl-int/lit8 v14, v3, 0x18

    shl-int/lit8 v15, v6, 0x10

    or-int/2addr v14, v15

    shl-int/lit8 v15, v5, 0x8

    or-int/2addr v14, v15

    or-int/2addr v14, v4

    return v14
.end method

.method public static isAccessibilityEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 276
    const-string v1, "accessibility"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 278
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method public static isColorBright(I)Z
    .locals 6
    .param p0, "color"    # I

    .prologue
    .line 207
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    .line 208
    .local v3, "red":I
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    .line 209
    .local v2, "green":I
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    .line 212
    .local v0, "blue":I
    mul-int/lit8 v4, v3, 0x15

    mul-int/lit8 v5, v2, 0x48

    add-int/2addr v4, v5

    mul-int/lit8 v5, v0, 0x7

    add-int v1, v4, v5

    .line 213
    .local v1, "brightness100":I
    const/16 v4, 0x3200

    if-lt v1, v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static isVisibleOnScreen(Landroid/view/View;)Z
    .locals 1
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 331
    sget-object v0, Lcom/google/android/finsky/utils/UiUtils;->sTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public static playShakeAnimationIfPossible(Landroid/content/Context;Landroid/widget/EditText;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "view"    # Landroid/widget/EditText;

    .prologue
    .line 259
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    .line 272
    :goto_0
    return-void

    .line 262
    :cond_0
    const-string v2, "translationX"

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {p1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 263
    .local v0, "animator":Landroid/animation/ObjectAnimator;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0131

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 264
    .local v1, "shakeDelta":F
    new-instance v2, Lcom/google/android/finsky/utils/UiUtils$2;

    invoke-direct {v2, v1}, Lcom/google/android/finsky/utils/UiUtils$2;-><init>(F)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 271
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 262
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "textToAnnounce"    # Ljava/lang/String;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 291
    invoke-static {p0}, Lcom/google/android/finsky/utils/UiUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 312
    :goto_0
    return-void

    .line 295
    :cond_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_2

    const/16 v2, 0x4000

    .line 298
    .local v2, "eventType":I
    :goto_1
    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 299
    .local v1, "event":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 300
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 302
    if-eqz p2, :cond_1

    .line 304
    invoke-static {v1}, Landroid/support/v4/view/accessibility/AccessibilityEventCompat;->asRecord(Landroid/view/accessibility/AccessibilityEvent;)Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;

    move-result-object v3

    .line 305
    .local v3, "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    invoke-virtual {v3, p2}, Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;->setSource(Landroid/view/View;)V

    .line 308
    .end local v3    # "record":Landroid/support/v4/view/accessibility/AccessibilityRecordCompat;
    :cond_1
    const-string v4, "accessibility"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 311
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0

    .line 295
    .end local v0    # "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    .end local v1    # "event":Landroid/view/accessibility/AccessibilityEvent;
    .end local v2    # "eventType":I
    :cond_2
    const/16 v2, 0x8

    goto :goto_1
.end method

.method public static setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "viewLabel"    # Ljava/lang/String;
    .param p2, "viewError"    # Ljava/lang/String;

    .prologue
    .line 324
    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 325
    invoke-virtual {p0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c03ad

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 327
    .local v0, "textToAnnounce":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p0}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 328
    return-void
.end method

.method public static showKeyboard(Landroid/app/Activity;Landroid/widget/EditText;)V
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "focusView"    # Landroid/widget/EditText;

    .prologue
    .line 226
    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    .line 227
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 231
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    new-instance v1, Lcom/google/android/finsky/utils/UiUtils$1;

    invoke-direct {v1, v0, p1}, Lcom/google/android/finsky/utils/UiUtils$1;-><init>(Landroid/view/inputmethod/InputMethodManager;Landroid/widget/EditText;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {p1, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 237
    return-void
.end method

.method private static syncActionBarAlpha(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V
    .locals 11
    .param p0, "scrollView"    # Lcom/google/android/finsky/layout/ObservableScrollView;
    .param p1, "actionBarController"    # Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .param p2, "heroImageHost"    # Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;
    .param p3, "scrollTop"    # I

    .prologue
    const/4 v10, 0x0

    .line 487
    invoke-interface {p2}, Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;->getOverlayableImageHeight()I

    move-result v2

    .line 488
    .local v2, "heroImageHeight":I
    if-le p3, v2, :cond_0

    .line 492
    const/16 v8, 0xff

    invoke-interface {p1, v8, v10}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->setActionBarAlpha(IZ)V

    .line 539
    :goto_0
    return-void

    .line 502
    :cond_0
    invoke-interface {p2}, Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;->getOverlayTransparencyHeightFromTop()I

    move-result v3

    .line 503
    .local v3, "heroOverlayTransparencyHeight":I
    if-gt p3, v3, :cond_2

    const/4 v6, 0x0

    .line 506
    .local v6, "overlayFraction":F
    :goto_1
    const/high16 v8, 0x437f0000    # 255.0f

    mul-float/2addr v8, v6

    float-to-int v0, v8

    .line 507
    .local v0, "alpha":I
    invoke-interface {p1, v0, v10}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->setActionBarAlpha(IZ)V

    .line 512
    invoke-static {v10, p3}, Ljava/lang/Math;->max(II)I

    move-result v8

    int-to-float v8, v8

    int-to-float v9, v2

    div-float v1, v8, v9

    .line 513
    .local v1, "fullFraction":F
    int-to-float v8, v2

    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v8, v9

    mul-float/2addr v8, v1

    float-to-int v4, v8

    .line 516
    .local v4, "heroTopPadding":I
    sget-object v8, Lcom/google/android/finsky/utils/UiUtils;->sPendingPaddingRequests:Ljava/util/Map;

    invoke-interface {v8, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Runnable;

    .line 517
    .local v7, "pendingRequest":Ljava/lang/Runnable;
    if-eqz v7, :cond_1

    .line 520
    sget-object v8, Lcom/google/android/finsky/utils/UiUtils;->sPaddingRequestHandler:Landroid/os/Handler;

    invoke-virtual {v8, v7}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 521
    sget-object v8, Lcom/google/android/finsky/utils/UiUtils;->sPendingPaddingRequests:Ljava/util/Map;

    invoke-interface {v8, p2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 524
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ObservableScrollView;->isViewInLayout()Z

    move-result v8

    if-nez v8, :cond_3

    .line 525
    invoke-interface {p2, v4}, Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;->setOverlayableImageTopPadding(I)V

    goto :goto_0

    .line 503
    .end local v0    # "alpha":I
    .end local v1    # "fullFraction":F
    .end local v4    # "heroTopPadding":I
    .end local v6    # "overlayFraction":F
    .end local v7    # "pendingRequest":Ljava/lang/Runnable;
    :cond_2
    sub-int v8, p3, v3

    int-to-float v8, v8

    sub-int v9, v2, v3

    int-to-float v9, v9

    div-float/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v6

    goto :goto_1

    .line 528
    .restart local v0    # "alpha":I
    .restart local v1    # "fullFraction":F
    .restart local v4    # "heroTopPadding":I
    .restart local v6    # "overlayFraction":F
    .restart local v7    # "pendingRequest":Ljava/lang/Runnable;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/utils/UiUtils$7;

    invoke-direct {v5, p2, v4}, Lcom/google/android/finsky/utils/UiUtils$7;-><init>(Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V

    .line 536
    .local v5, "newRequest":Ljava/lang/Runnable;
    sget-object v8, Lcom/google/android/finsky/utils/UiUtils;->sPendingPaddingRequests:Ljava/util/Map;

    invoke-interface {v8, p2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    sget-object v8, Lcom/google/android/finsky/utils/UiUtils;->sPaddingRequestHandler:Landroid/os/Handler;

    invoke-virtual {v8, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public static syncContainerVisibility(Landroid/view/ViewGroup;I)V
    .locals 4
    .param p0, "container"    # Landroid/view/ViewGroup;
    .param p1, "visibilityIfNoVisibleChildren"    # I

    .prologue
    .line 570
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 571
    .local v1, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 572
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 573
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 574
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 579
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return-void

    .line 571
    .restart local v0    # "child":Landroid/view/View;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 578
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method
