.class public final Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "LibraryReplication.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/LibraryReplication;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LibraryReplicationResponse"
.end annotation


# instance fields
.field public autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

.field public update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 142
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->clear()Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    .line 143
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;
    .locals 1

    .prologue
    .line 146
    invoke-static {}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->emptyArray()[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 147
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    .line 148
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->cachedSize:I

    .line 149
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 176
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 177
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v5, v5

    if-lez v5, :cond_1

    .line 178
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 179
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    aget-object v2, v5, v3

    .line 180
    .local v2, "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    if-eqz v2, :cond_0

    .line 181
    const/4 v5, 0x1

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 178
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 186
    .end local v2    # "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .end local v3    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_4

    .line 187
    const/4 v0, 0x0

    .line 188
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 189
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_3

    .line 190
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 191
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 192
    add-int/lit8 v0, v0, 0x1

    .line 193
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 189
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 197
    .end local v2    # "element":Ljava/lang/String;
    :cond_3
    add-int/2addr v4, v1

    .line 198
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 200
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_4
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 208
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 209
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 213
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 214
    :sswitch_0
    return-object p0

    .line 219
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 221
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-nez v5, :cond_2

    move v1, v4

    .line 222
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 224
    .local v2, "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    if-eqz v1, :cond_1

    .line 225
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 227
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 228
    new-instance v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    aput-object v5, v2, v1

    .line 229
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 230
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 227
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 221
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v1, v5

    goto :goto_1

    .line 233
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    aput-object v5, v2, v1

    .line 234
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 235
    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    goto :goto_0

    .line 239
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 241
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    if-nez v5, :cond_5

    move v1, v4

    .line 242
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 243
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 244
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 246
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 247
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 248
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 246
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 241
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_3

    .line 251
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 252
    iput-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    goto/16 :goto_0

    .line 209
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 155
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 156
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 157
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->update:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    aget-object v0, v2, v1

    .line 158
    .local v0, "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    if-eqz v0, :cond_0

    .line 159
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 156
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 163
    .end local v0    # "element":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 164
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 165
    iget-object v2, p0, Lcom/google/android/finsky/protos/LibraryReplication$LibraryReplicationResponse;->autoAcquireFreeAppIfHigherVersionAvailableTag:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 166
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 167
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 164
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 171
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 172
    return-void
.end method
