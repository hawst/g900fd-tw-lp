.class Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$2;
.super Ljava/lang/Object;
.source "MyAccountFragment.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->requestData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 4
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 131
    const-string v1, "Could not retrieve my account content: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;

    # getter for: Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->access$200(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    .line 133
    .local v0, "errorMessage":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$2;->this$0:Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;

    # invokes: Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->switchToError(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->access$300(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;Ljava/lang/String;)V

    .line 134
    return-void
.end method
