.class final Lcom/google/android/finsky/utils/AppSupport$1;
.super Ljava/lang/Object;
.source "AppSupport.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/AppSupport;->silentRefund(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/finsky/utils/AppSupport$RefundListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/RevokeResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$listener:Lcom/google/android/finsky/utils/AppSupport$RefundListener;

.field final synthetic val$packageName:Ljava/lang/String;

.field final synthetic val$tryUninstall:Z


# direct methods
.method constructor <init>(ZLjava/lang/String;Lcom/google/android/finsky/utils/AppSupport$RefundListener;)V
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/finsky/utils/AppSupport$1;->val$tryUninstall:Z

    iput-object p2, p0, Lcom/google/android/finsky/utils/AppSupport$1;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/utils/AppSupport$1;->val$listener:Lcom/google/android/finsky/utils/AppSupport$RefundListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/RevokeResponse;)V
    .locals 2
    .param p1, "revokeResponse"    # Lcom/google/android/finsky/protos/RevokeResponse;

    .prologue
    .line 49
    invoke-static {}, Lcom/google/android/finsky/utils/OrderHistoryHelper;->onMutationOccurred()V

    .line 50
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/AppSupport$1;->val$tryUninstall:Z

    if-eqz v0, :cond_0

    .line 51
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/utils/AppSupport$1;->val$packageName:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/receivers/Installer;->uninstallAssetSilently(Ljava/lang/String;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/utils/AppSupport$1;->val$listener:Lcom/google/android/finsky/utils/AppSupport$RefundListener;

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/utils/AppSupport$1;->val$listener:Lcom/google/android/finsky/utils/AppSupport$RefundListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/utils/AppSupport$RefundListener;->onRefundComplete(Z)V

    .line 56
    :cond_1
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p1, Lcom/google/android/finsky/protos/RevokeResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/AppSupport$1;->onResponse(Lcom/google/android/finsky/protos/RevokeResponse;)V

    return-void
.end method
