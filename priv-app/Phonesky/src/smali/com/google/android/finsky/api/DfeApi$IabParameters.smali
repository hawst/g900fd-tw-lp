.class public Lcom/google/android/finsky/api/DfeApi$IabParameters;
.super Ljava/lang/Object;
.source "DfeApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/api/DfeApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IabParameters"
.end annotation


# instance fields
.field private final mApiVersion:I

.field private final mDeveloperPayload:Ljava/lang/String;

.field private final mOldSkusAsDocidStrings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mPackageName:Ljava/lang/String;

.field private final mPackageSignatureHash:Ljava/lang/String;

.field private final mPackageVersion:I


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "iabApiVersion"    # I
    .param p2, "iabPackageName"    # Ljava/lang/String;
    .param p3, "iabPackageSignatureHash"    # Ljava/lang/String;
    .param p4, "iabPackageVersion"    # I
    .param p5, "iabDeveloperPayload"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1146
    .local p6, "oldSkusAsDocidStrings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1147
    iput p1, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mApiVersion:I

    .line 1148
    iput-object p2, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mPackageName:Ljava/lang/String;

    .line 1149
    iput-object p3, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mPackageSignatureHash:Ljava/lang/String;

    .line 1150
    iput p4, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mPackageVersion:I

    .line 1151
    iput-object p5, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mDeveloperPayload:Ljava/lang/String;

    .line 1152
    iput-object p6, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mOldSkusAsDocidStrings:Ljava/util/List;

    .line 1153
    return-void
.end method


# virtual methods
.method public addToRequest(Lcom/google/android/finsky/api/DfeApi$DfePostRequest;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/api/DfeApi$DfePostRequest",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1156
    .local p1, "request":Lcom/google/android/finsky/api/DfeApi$DfePostRequest;, "Lcom/google/android/finsky/api/DfeApi$DfePostRequest<*>;"
    const-string v2, "bav"

    iget v3, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mApiVersion:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1157
    const-string v2, "shpn"

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mPackageName:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1158
    const-string v2, "shh"

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mPackageSignatureHash:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1160
    const-string v2, "shvc"

    iget v3, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mPackageVersion:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v2, v3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1162
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mDeveloperPayload:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1163
    const-string v2, "payload"

    iget-object v3, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mDeveloperPayload:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    .line 1165
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mOldSkusAsDocidStrings:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 1166
    iget-object v2, p0, Lcom/google/android/finsky/api/DfeApi$IabParameters;->mOldSkusAsDocidStrings:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1167
    .local v1, "oldSku":Ljava/lang/String;
    const-string v2, "odid"

    invoke-virtual {p1, v2, v1}, Lcom/google/android/finsky/api/DfeApi$DfePostRequest;->addPostParam(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1170
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "oldSku":Ljava/lang/String;
    :cond_1
    return-void
.end method
