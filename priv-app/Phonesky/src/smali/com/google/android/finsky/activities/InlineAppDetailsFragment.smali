.class public Lcom/google/android/finsky/activities/InlineAppDetailsFragment;
.super Lcom/google/android/finsky/fragments/PageFragment;
.source "InlineAppDetailsFragment.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/RootUiElementNode;


# instance fields
.field private mAccount:Landroid/accounts/Account;

.field private mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

.field private mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mDocId:Ljava/lang/String;

.field protected mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

.field private mHasBeenAuthenticated:Z

.field private mImpressionHandler:Landroid/os/Handler;

.field private mImpressionId:J

.field private mLibraries:Lcom/google/android/finsky/library/Libraries;

.field private mReferrer:Ljava/lang/String;

.field private mRootUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mSentImpression:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 100
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/PageFragment;-><init>()V

    .line 78
    iput-boolean v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mHasBeenAuthenticated:Z

    .line 82
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->getNextImpressionId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mImpressionId:J

    .line 85
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mRootUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    .line 91
    iput-boolean v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mSentImpression:Z

    .line 101
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/InlineAppDetailsFragment;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method private getBuyButtonListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;I)Landroid/view/View$OnClickListener;
    .locals 9
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "logElementType"    # I

    .prologue
    const/4 v4, 0x0

    .line 407
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    const/4 v3, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v5, v4

    move v6, p3

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v8

    .line 410
    .local v8, "listener":Landroid/view/View$OnClickListener;
    new-instance v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment$2;

    invoke-direct {v0, p0, v8}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment$2;-><init>(Lcom/google/android/finsky/activities/InlineAppDetailsFragment;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method private getBuyButtonLoggingElementType(Lcom/google/android/finsky/api/model/Document;ZI)I
    .locals 3
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "isOwnedByUser"    # Z
    .param p3, "offerType"    # I

    .prologue
    const/16 v0, 0xdd

    .line 426
    if-eqz p2, :cond_1

    .line 440
    :cond_0
    :goto_0
    return v0

    .line 431
    :cond_1
    invoke-virtual {p1, p3}, Lcom/google/android/finsky/api/model/Document;->needsCheckoutFlow(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 432
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 434
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 436
    const/16 v0, 0xe1

    goto :goto_0

    .line 440
    :cond_2
    const/16 v0, 0xc8

    goto :goto_0
.end method

.method private getBuyButtonString(Lcom/google/android/finsky/api/model/Document;ZI)Ljava/lang/String;
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "isOwnedByUser"    # Z
    .param p3, "offerType"    # I

    .prologue
    const v7, 0x7f0c01e6

    const/4 v6, 0x1

    .line 445
    if-eqz p2, :cond_1

    .line 446
    iget-object v4, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 472
    :cond_0
    :goto_0
    return-object v0

    .line 450
    :cond_1
    invoke-virtual {p1, p3}, Lcom/google/android/finsky/api/model/Document;->needsCheckoutFlow(I)Z

    move-result v1

    .line 451
    .local v1, "needsCheckoutFlow":Z
    if-nez v1, :cond_3

    .line 452
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 453
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 454
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    if-ne v4, v6, :cond_3

    .line 457
    iget-object v4, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mContext:Landroid/content/Context;

    const v5, 0x7f0c020b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 462
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v4

    const-string v5, "cl:billing.show_buy_verb_in_button"

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    .line 464
    .local v3, "useBuyVerb":Z
    invoke-virtual {p1, p3}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v2

    .line 465
    .local v2, "purchaseOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v2, :cond_4

    iget-boolean v4, v2, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-eqz v4, :cond_4

    .line 466
    iget-object v0, v2, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    .line 467
    .local v0, "formattedAmount":Ljava/lang/String;
    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 468
    iget-object v4, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mContext:Landroid/content/Context;

    const v5, 0x7f0c01f9

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 472
    .end local v0    # "formattedAmount":Ljava/lang/String;
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/activities/InlineAppDetailsFragment;
    .locals 1
    .param p0, "docId"    # Ljava/lang/String;
    .param p1, "referrer"    # Ljava/lang/String;

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;-><init>()V

    .line 96
    .local v0, "fragment":Lcom/google/android/finsky/activities/InlineAppDetailsFragment;
    invoke-virtual {v0, p0, p1}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->init(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-object v0
.end method

.method private setupItemDetails(Lcom/google/android/finsky/api/model/Document;Landroid/view/View;)V
    .locals 22
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "parent"    # Landroid/view/View;

    .prologue
    .line 280
    const v19, 0x7f0a009c

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 281
    .local v17, "title":Landroid/widget/TextView;
    if-eqz v17, :cond_0

    .line 282
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 288
    :cond_0
    const v19, 0x7f0a0179

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    .line 289
    .local v9, "creatorPanel":Landroid/view/ViewGroup;
    const v19, 0x7f0a0206

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 290
    .local v8, "creator":Lcom/google/android/finsky/layout/DecoratedTextView;
    if-eqz v8, :cond_1

    .line 291
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 292
    invoke-static/range {p1 .. p1}, Lcom/google/android/finsky/utils/PlayCardUtils;->getDocDisplaySubtitle(Lcom/google/android/finsky/api/model/Document;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1, v8}, Lcom/google/android/finsky/utils/BadgeUtils;->configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    .line 297
    :cond_1
    const v19, 0x7f0a015e

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;

    .line 299
    .local v18, "wishlist":Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;
    if-eqz v18, :cond_2

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->configure(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 303
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 306
    .local v12, "res":Landroid/content/res/Resources;
    const v19, 0x7f0a0186

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 309
    .local v14, "thumbnail":Lcom/google/android/play/layout/PlayCardThumbnail;
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->setVisibility(I)V

    .line 311
    const/16 v19, -0x1

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->updateCoverPadding(I)V

    .line 312
    invoke-virtual {v14}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    .line 313
    .local v16, "thumbnailLp":Landroid/view/ViewGroup$LayoutParams;
    const v19, 0x7f0b0186

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 314
    const v19, 0x7f0b0186

    move/from16 v0, v19

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 316
    invoke-virtual {v14}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v15

    check-cast v15, Lcom/google/android/finsky/layout/DocImageView;

    .line 317
    .local v15, "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    sget-object v19, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/google/android/finsky/layout/DocImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v19, v0

    sget-object v20, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->CORE_IMAGE_TYPES:[I

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-virtual {v15, v0, v1, v2}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 320
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/google/android/finsky/layout/DocImageView;->setFocusable(Z)V

    .line 321
    move-object/from16 v0, p1

    invoke-static {v0, v12}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getItemThumbnailContentDescription(Lcom/google/android/finsky/api/model/Document;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Lcom/google/android/finsky/layout/DocImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 325
    const v19, 0x7f0a0225

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/play/layout/StarRatingBar;

    .line 326
    .local v13, "starRatingBar":Lcom/google/android/play/layout/StarRatingBar;
    if-eqz v13, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasRating()Z

    move-result v19

    if-eqz v19, :cond_3

    .line 327
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getStarRating()F

    move-result v19

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 330
    :cond_3
    const v19, 0x7f0a0226

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 331
    .local v11, "ratingCount":Landroid/widget/TextView;
    if-eqz v11, :cond_4

    .line 332
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getRatingCount()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-float v7, v0

    .line 333
    .local v7, "count":F
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v19

    float-to-double v0, v7

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    .end local v7    # "count":F
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v6

    .line 339
    .local v6, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    if-eqz v6, :cond_5

    iget-boolean v0, v6, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->declaresIab:Z

    move/from16 v19, v0

    if-eqz v19, :cond_5

    .line 340
    const v19, 0x7f0a0185

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 342
    .local v10, "inAppPurchaseLabelView":Landroid/widget/TextView;
    const v19, 0x7f0c03b1

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(I)V

    .line 343
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 346
    .end local v10    # "inAppPurchaseLabelView":Landroid/widget/TextView;
    :cond_5
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mAccount:Landroid/accounts/Account;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    move-object/from16 v3, v19

    move-object/from16 v4, v20

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->syncDynamicSection(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V

    .line 347
    return-void
.end method

.method private updateDetailsSections()V
    .locals 9

    .prologue
    .line 231
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v5, :cond_1

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 237
    .local v0, "fragmentView":Landroid/view/View;
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-direct {p0, v5, v0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->setupItemDetails(Lcom/google/android/finsky/api/model/Document;Landroid/view/View;)V

    .line 239
    const v5, 0x7f0a0220

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 241
    .local v3, "shortDescriptionPanel":Landroid/widget/TextView;
    if-eqz v3, :cond_2

    .line 242
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getPromotionalDescription()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 243
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getPromotionalDescription()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    :cond_2
    :goto_1
    const v5, 0x7f0a016a

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/ScreenshotGallery;

    .line 252
    .local v2, "screenshotsPanel":Lcom/google/android/finsky/layout/ScreenshotGallery;
    if-eqz v2, :cond_3

    .line 253
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v6, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v7, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    const/4 v8, 0x1

    invoke-virtual {v2, v5, v6, v7, v8}, Lcom/google/android/finsky/layout/ScreenshotGallery;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Z)V

    .line 256
    :cond_3
    const v5, 0x7f0a0222

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 257
    .local v1, "moreDetails":Landroid/widget/TextView;
    if-eqz v1, :cond_4

    .line 258
    const v5, 0x7f0c0407

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    new-instance v5, Lcom/google/android/finsky/activities/InlineAppDetailsFragment$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment$1;-><init>(Lcom/google/android/finsky/activities/InlineAppDetailsFragment;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    :cond_4
    const v5, 0x7f0a01a3

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/WarningMessageSection;

    .line 273
    .local v4, "warningMessagePanel":Lcom/google/android/finsky/layout/WarningMessageSection;
    if-eqz v4, :cond_0

    .line 274
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v6, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v7, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    iget-object v8, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/google/android/finsky/layout/WarningMessageSection;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)V

    goto :goto_0

    .line 245
    .end local v1    # "moreDetails":Landroid/widget/TextView;
    .end local v2    # "screenshotsPanel":Lcom/google/android/finsky/layout/ScreenshotGallery;
    .end local v4    # "warningMessagePanel":Lcom/google/android/finsky/layout/WarningMessageSection;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getRawDescription()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mImpressionHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mImpressionId:J

    invoke-static {v0, v2, v3, p0, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->rootImpression(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 488
    return-void
.end method

.method public flushImpression()V
    .locals 4

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mImpressionHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mImpressionId:J

    invoke-static {v0, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->flushImpression(Landroid/os/Handler;JLcom/google/android/finsky/layout/play/RootUiElementNode;)V

    .line 483
    return-void
.end method

.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 218
    const v0, 0x7f0400b4

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mRootUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public init(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "referrer"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocId:Ljava/lang/String;

    .line 105
    iput-object p2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mReferrer:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeDetails;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 123
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mAccount:Landroid/accounts/Account;

    .line 124
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 125
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 126
    iput-object p1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    .line 128
    if-eqz p1, :cond_0

    .line 129
    const-string v0, "docId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocId:Ljava/lang/String;

    .line 130
    const-string v0, "referrer"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mReferrer:Ljava/lang/String;

    .line 132
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->switchToLoadingImmediately()V

    .line 133
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 110
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mImpressionHandler:Landroid/os/Handler;

    .line 111
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onAttach(Landroid/app/Activity;)V

    .line 112
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    .prologue
    .line 138
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 142
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 143
    if-nez p1, :cond_0

    .line 148
    :goto_0
    return-void

    .line 146
    :cond_0
    const-string v0, "docId"

    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "referrer"

    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mReferrer:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected rebindViews()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->isDataReady()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mHasBeenAuthenticated:Z

    if-nez v1, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 170
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 171
    const-string v1, "Only apps are supported: %s"

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocId:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 176
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->updateDetailsSections()V

    .line 178
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mReferrer:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mSavedInstanceState:Landroid/os/Bundle;

    if-nez v1, :cond_3

    .line 179
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mReferrer:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getFullDocid()Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->saveExternalReferrer(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;)V

    .line 180
    sget-object v1, Lcom/google/android/finsky/config/G;->logInlineAppInstallReferrerEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 181
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    .line 182
    .local v0, "logger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mReferrer:Ljava/lang/String;

    invoke-virtual {v0, v1, v3, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logDeepLinkEvent(ILjava/lang/String;Ljava/lang/String;[B)V

    .line 187
    .end local v0    # "logger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    :cond_3
    invoke-static {p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->startNewImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 192
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mRootUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeDetails;->getServerLogsCookie()[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 199
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v1, :cond_0

    .line 201
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    if-nez v1, :cond_4

    .line 202
    new-instance v1, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v2, 0xd1

    invoke-direct {v1, v2, v3, v3, p0}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    .line 205
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    iget-object v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;->setServerLogsCookie([B)V

    .line 209
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mSentImpression:Z

    if-nez v1, :cond_0

    .line 210
    iget-object v1, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocumentUiElementNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 211
    iput-boolean v4, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mSentImpression:Z

    goto/16 :goto_0
.end method

.method protected requestData()V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 157
    :cond_0
    new-instance v0, Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDocId:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 159
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 161
    return-void
.end method

.method public setHasBeenAuthenticated()V
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mHasBeenAuthenticated:Z

    .line 223
    return-void
.end method

.method protected setupActionButtons(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V
    .locals 16
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "account"    # Landroid/accounts/Account;

    .prologue
    .line 363
    const v13, 0x7f0a0138

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/layout/PlayActionButton;

    .line 364
    .local v5, "buyButton":Lcom/google/android/play/layout/PlayActionButton;
    const v13, 0x7f0a013b

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/play/layout/PlayActionButton;

    .line 367
    .local v9, "launchButton":Lcom/google/android/play/layout/PlayActionButton;
    const/16 v13, 0x8

    invoke-virtual {v9, v13}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 368
    const/16 v13, 0x8

    invoke-virtual {v5, v13}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 370
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v10

    .line 371
    .local v10, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v4

    .line 374
    .local v4, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v13

    iget-object v3, v13, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    .line 375
    .local v3, "appPackageName":Ljava/lang/String;
    new-instance v2, Lcom/google/android/finsky/activities/AppActionAnalyzer;

    invoke-direct {v2, v3, v4, v10}, Lcom/google/android/finsky/activities/AppActionAnalyzer;-><init>(Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)V

    .line 381
    .local v2, "actions":Lcom/google/android/finsky/activities/AppActionAnalyzer;
    move-object/from16 v0, p4

    invoke-static {v3, v0, v4, v10}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->getInstallAccount(Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)Landroid/accounts/Account;

    move-result-object v7

    .line 384
    .local v7, "installAccount":Landroid/accounts/Account;
    iget-boolean v13, v2, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isLaunchable:Z

    if-eqz v13, :cond_0

    iget-boolean v13, v2, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isContinueLaunch:Z

    if-nez v13, :cond_0

    .line 385
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 386
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual/range {v13 .. v16}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getOpenClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v6

    .line 388
    .local v6, "clickHandler":Landroid/view/View$OnClickListener;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v13

    const v14, 0x7f0c020b

    invoke-virtual {v9, v13, v14, v6}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 392
    .end local v6    # "clickHandler":Landroid/view/View$OnClickListener;
    :cond_0
    iget-boolean v13, v2, Lcom/google/android/finsky/activities/AppActionAnalyzer;->isInstalled:Z

    if-nez v13, :cond_1

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-static {v0, v1, v10}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 393
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v13

    invoke-virtual {v13}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v13

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v13, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v12

    .line 395
    .local v12, "owner":Landroid/accounts/Account;
    const/4 v13, 0x0

    invoke-virtual {v5, v13}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 396
    if-eqz v12, :cond_2

    const/4 v8, 0x1

    .line 397
    .local v8, "isOwned":Z
    :goto_0
    const/4 v13, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v8, v13}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->getBuyButtonLoggingElementType(Lcom/google/android/finsky/api/model/Document;ZI)I

    move-result v11

    .line 399
    .local v11, "logElementType":I
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v13

    const/4 v14, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v8, v14}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->getBuyButtonString(Lcom/google/android/finsky/api/model/Document;ZI)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v7, v1, v11}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->getBuyButtonListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;I)Landroid/view/View$OnClickListener;

    move-result-object v15

    invoke-virtual {v5, v13, v14, v15}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 403
    .end local v8    # "isOwned":Z
    .end local v11    # "logElementType":I
    .end local v12    # "owner":Landroid/accounts/Account;
    :cond_1
    return-void

    .line 396
    .restart local v12    # "owner":Landroid/accounts/Account;
    :cond_2
    const/4 v8, 0x0

    goto :goto_0
.end method

.method public showError(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->switchToError(Ljava/lang/String;)V

    .line 227
    return-void
.end method

.method public startNewImpression()V
    .locals 2

    .prologue
    .line 477
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->getNextImpressionId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->mImpressionId:J

    .line 478
    return-void
.end method

.method protected syncDynamicSection(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V
    .locals 3
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "account"    # Landroid/accounts/Account;

    .prologue
    .line 350
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v0

    .line 351
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-static {p2, p3, v0}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v1

    .line 353
    .local v1, "isAvailable":Z
    if-nez v1, :cond_0

    .line 358
    :goto_0
    return-void

    .line 357
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/activities/InlineAppDetailsFragment;->setupActionButtons(Landroid/view/View;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V

    goto :goto_0
.end method
