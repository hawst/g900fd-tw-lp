.class public abstract Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;
.source "InstrumentActivity.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/billing/BillingFlowContext;
.implements Lcom/google/android/finsky/billing/BillingFlowFragment$BillingFlowHost;
.implements Lcom/google/android/finsky/billing/BillingFlowListener;


# instance fields
.field protected mFragmentContainer:Landroid/view/ViewGroup;

.field protected mMainView:Landroid/view/View;

.field private mNeedsHideProgress:Z

.field private mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

.field private mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

.field private mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

.field private mSaveInstanceStateCalled:Z

.field protected mSavedFlowState:Landroid/os/Bundle;

.field private mSetupWizardNavBar:Lcom/google/android/finsky/setup/SetupWizardNavBar;

.field protected mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;-><init>()V

    return-void
.end method

.method private stopFlow()V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    .line 256
    return-void
.end method


# virtual methods
.method protected getActivityLayout()I
    .locals 1

    .prologue
    .line 122
    const v0, 0x7f0400bb

    return v0
.end method

.method protected abstract getBillingFlow()Lcom/google/android/finsky/billing/BillingFlow;
.end method

.method protected abstract getBillingFlowFragment()Lcom/google/android/finsky/billing/BillingFlowFragment;
.end method

.method protected getBillingUiMode()I
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x0

    return v0
.end method

.method public getContainerId()I
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getId()I

    move-result v0

    return v0
.end method

.method public getSetupWizardNavBar()Lcom/google/android/finsky/setup/SetupWizardNavBar;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSetupWizardNavBar:Lcom/google/android/finsky/setup/SetupWizardNavBar;

    return-object v0
.end method

.method public hideFragment(Landroid/support/v4/app/Fragment;Z)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "addToBackStack"    # Z

    .prologue
    .line 293
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v1, :cond_0

    .line 302
    :goto_0
    return-void

    .line 296
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 297
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 298
    if-eqz p2, :cond_1

    .line 299
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 301
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method public hideProgress()V
    .locals 1

    .prologue
    .line 401
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mNeedsHideProgress:Z

    .line 402
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 403
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v0, :cond_1

    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mNeedsHideProgress:Z

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->dismiss()V

    .line 408
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x258

    const/4 v1, 0x0

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v0, :cond_1

    .line 155
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlow;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlow;->back()V

    .line 172
    :goto_0
    return-void

    .line 159
    :cond_0
    const-string v0, "Cannot interrupt the current flow."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlowFragment;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlowFragment;->back()V

    goto :goto_0

    .line 167
    :cond_2
    const-string v0, "Cannot interrupt the current flow."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 170
    :cond_3
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getActivityLayout()I

    move-result v0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mMainView:Landroid/view/View;

    .line 100
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mMainView:Landroid/view/View;

    const v2, 0x7f0a009c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mTitleView:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mMainView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->setContentView(Landroid/view/View;)V

    .line 102
    const v0, 0x7f0a00c4

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    .line 103
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getBillingUiMode()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    invoke-static {p0}, Lcom/google/android/finsky/utils/SetupWizardUtils;->getNavBarIfPossible(Landroid/app/Activity;)Lcom/google/android/finsky/setup/SetupWizardNavBar;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSetupWizardNavBar:Lcom/google/android/finsky/setup/SetupWizardNavBar;

    .line 105
    if-eqz p1, :cond_1

    .line 106
    const-string v0, "InstrumentActivity.saved_flow_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSavedFlowState:Landroid/os/Bundle;

    .line 107
    const-string v0, "InstrumentActivity.progress_dialog"

    invoke-virtual {p0, p1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/ProgressDialogFragment;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    .line 109
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->dismiss()V

    .line 113
    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    .line 115
    :cond_0
    const-string v0, "InstrumentActivity.title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mTitleView:Landroid/widget/TextView;

    const-string v1, "InstrumentActivity.title"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 103
    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onDestroy()V

    .line 148
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->stopFlow()V

    .line 149
    return-void
.end method

.method public onError(Lcom/google/android/finsky/billing/BillingFlow;Ljava/lang/String;)V
    .locals 3
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlow;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 414
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 415
    .local v0, "result":Landroid/content/Intent;
    const-string v1, "billing_flow_error"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 416
    const-string v1, "billing_flow_error_message"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 417
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->setResult(ILandroid/content/Intent;)V

    .line 418
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->finish()V

    .line 419
    return-void
.end method

.method public onFinished(Lcom/google/android/finsky/billing/BillingFlow;ZLandroid/os/Bundle;)V
    .locals 14
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlow;
    .param p2, "canceled"    # Z
    .param p3, "flowResult"    # Landroid/os/Bundle;

    .prologue
    .line 325
    if-eqz p2, :cond_2

    const/4 v12, 0x0

    .line 326
    .local v12, "resultCode":I
    :goto_0
    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    .line 327
    .local v11, "result":Landroid/content/Intent;
    const-string v1, "billing_flow_error"

    const/4 v2, 0x0

    invoke-virtual {v11, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 328
    const-string v1, "billing_flow_canceled"

    move/from16 v0, p2

    invoke-virtual {v11, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 330
    if-nez p2, :cond_0

    if-eqz p3, :cond_0

    const-string v1, "instrument_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 332
    const-string v1, "instrument_id"

    const-string v2, "instrument_id"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 335
    :cond_0
    if-nez p2, :cond_3

    if-eqz p3, :cond_3

    const-string v1, "redeemed_offer_message_html"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 337
    const-string v1, "redeemed_offer_message_html"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 339
    .local v10, "redeemedOfferHtml":Ljava/lang/String;
    const-string v1, "redeemed_offer_message_html"

    invoke-virtual {v11, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "InstrumentActivity.setup_wizard_params"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .line 346
    .local v13, "setupWizardParams":Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getBillingUiMode()I

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v13, :cond_3

    invoke-virtual {v13}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->hasSetupCompleteScreen()Z

    move-result v1

    if-nez v1, :cond_3

    .line 348
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v1, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v6

    .line 349
    .local v6, "account":Landroid/accounts/Account;
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 350
    .local v7, "args":Landroid/os/Bundle;
    const-string v1, "result_intent"

    invoke-virtual {v7, v1, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 351
    const-string v1, "result_code"

    invoke-virtual {v7, v1, v12}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 353
    new-instance v8, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v8}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 354
    .local v8, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v8, v10}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0c02a0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v1

    const/16 v2, 0x4c4

    const/4 v3, 0x0

    const/16 v4, 0x4c5

    const/4 v5, -0x1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCanceledOnTouchOutside(Z)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3, v7}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 362
    invoke-virtual {v8}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v9

    .line 364
    .local v9, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "redeemed_promo_offer"

    invoke-virtual {v9, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 375
    .end local v6    # "account":Landroid/accounts/Account;
    .end local v7    # "args":Landroid/os/Bundle;
    .end local v8    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v9    # "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .end local v10    # "redeemedOfferHtml":Ljava/lang/String;
    .end local v13    # "setupWizardParams":Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;
    :goto_1
    return-void

    .line 325
    .end local v11    # "result":Landroid/content/Intent;
    .end local v12    # "resultCode":I
    :cond_2
    const/4 v12, -0x1

    goto/16 :goto_0

    .line 369
    .restart local v11    # "result":Landroid/content/Intent;
    .restart local v12    # "resultCode":I
    :cond_3
    if-nez p2, :cond_4

    .line 371
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/finsky/billing/PromptForFopHelper;->expireHasNoFop(Ljava/lang/String;)V

    .line 373
    :cond_4
    invoke-virtual {p0, v12, v11}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->setResult(ILandroid/content/Intent;)V

    .line 374
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->finish()V

    goto :goto_1
.end method

.method public onFlowCanceled(Lcom/google/android/finsky/billing/BillingFlowFragment;)V
    .locals 2
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlowFragment;

    .prologue
    const/4 v1, 0x0

    .line 428
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->onFinished(Lcom/google/android/finsky/billing/BillingFlow;ZLandroid/os/Bundle;)V

    .line 429
    return-void
.end method

.method public onFlowError(Lcom/google/android/finsky/billing/BillingFlowFragment;Ljava/lang/String;)V
    .locals 1
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlowFragment;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 433
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->onError(Lcom/google/android/finsky/billing/BillingFlow;Ljava/lang/String;)V

    .line 434
    return-void
.end method

.method public onFlowFinished(Lcom/google/android/finsky/billing/BillingFlowFragment;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "flow"    # Lcom/google/android/finsky/billing/BillingFlowFragment;
    .param p2, "result"    # Landroid/os/Bundle;

    .prologue
    .line 423
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->onFinished(Lcom/google/android/finsky/billing/BillingFlow;ZLandroid/os/Bundle;)V

    .line 424
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 449
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 440
    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    .line 441
    const-string v2, "result_code"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 442
    .local v1, "resultCode":I
    const-string v2, "result_intent"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 443
    .local v0, "result":Landroid/content/Intent;
    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->setResult(ILandroid/content/Intent;)V

    .line 444
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->finish()V

    .line 446
    .end local v0    # "result":Landroid/content/Intent;
    .end local v1    # "resultCode":I
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onResume()V

    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlow;->onActivityResume()V

    .line 138
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSaveInstanceStateCalled:Z

    .line 140
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mNeedsHideProgress:Z

    if-eqz v0, :cond_1

    .line 141
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->hideProgress()V

    .line 143
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 214
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSaveInstanceStateCalled:Z

    .line 215
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/LoggingFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 216
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v1, :cond_0

    .line 217
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 218
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/BillingFlow;->saveState(Landroid/os/Bundle;)V

    .line 219
    const-string v1, "InstrumentActivity.saved_flow_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 221
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    if-eqz v1, :cond_1

    .line 222
    const-string v1, "InstrumentActivity.progress_dialog"

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {p0, p1, v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->persistFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 225
    const-string v1, "InstrumentActivity.title"

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_2
    return-void
.end method

.method public persistFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/support/v4/app/Fragment;)V

    .line 315
    return-void
.end method

.method public restoreFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public setHostTitle(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 386
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 387
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    return-void
.end method

.method public showDialogFragment(Landroid/support/v4/app/DialogFragment;Ljava/lang/String;)V
    .locals 3
    .param p1, "fragment"    # Landroid/support/v4/app/DialogFragment;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 279
    iget-boolean v2, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v2, :cond_0

    .line 289
    :goto_0
    return-void

    .line 282
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 283
    .local v0, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 284
    .local v1, "prev":Landroid/support/v4/app/Fragment;
    if-eqz v1, :cond_1

    .line 285
    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 287
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 288
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {p1, v2, p2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "addToBackStack"    # Z

    .prologue
    .line 262
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v1, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    invoke-virtual {p0, p2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 266
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 267
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 268
    if-eqz p3, :cond_2

    .line 269
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 271
    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 272
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSetupWizardNavBar:Lcom/google/android/finsky/setup/SetupWizardNavBar;

    if-eqz v1, :cond_0

    .line 273
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSetupWizardNavBar:Lcom/google/android/finsky/setup/SetupWizardNavBar;

    invoke-virtual {v1}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->resetButtonsState()V

    goto :goto_0
.end method

.method public showProgress(I)V
    .locals 3
    .param p1, "messageId"    # I

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mSaveInstanceStateCalled:Z

    if-eqz v0, :cond_0

    .line 397
    :goto_0
    return-void

    .line 395
    :cond_0
    invoke-static {p1}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->newInstance(I)Lcom/google/android/finsky/billing/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    .line 396
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mProgressDialog:Lcom/google/android/finsky/billing/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/billing/ProgressDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected startOrResumeFlow(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 175
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "InstrumentActivity.billing_flow_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/BillingFlowFragment;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    .line 177
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-eqz v0, :cond_1

    .line 178
    sget-boolean v0, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 179
    const-string v0, "Re-attached to billing flow fragment."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getBillingFlowFragment()Lcom/google/android/finsky/billing/BillingFlowFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-eqz v0, :cond_2

    .line 185
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getContainerId()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    const-string v3, "InstrumentActivity.billing_flow_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 190
    :goto_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlowFragment:Lcom/google/android/finsky/billing/BillingFlowFragment;

    if-nez v0, :cond_3

    .line 191
    const-string v0, "Couldn\'t instantiate BillingFlow for FOP."

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->finish()V

    goto :goto_0

    .line 188
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getBillingFlow()Lcom/google/android/finsky/billing/BillingFlow;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    goto :goto_1

    .line 194
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    if-eqz v0, :cond_4

    .line 195
    if-nez p1, :cond_5

    .line 196
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/BillingFlow;->start()V

    .line 201
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mFragmentContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    .line 198
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->mRunningFlow:Lcom/google/android/finsky/billing/BillingFlow;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/billing/BillingFlow;->resumeFromSavedState(Landroid/os/Bundle;)V

    goto :goto_2
.end method
