.class Lcom/google/android/finsky/appstate/UpdateChecker$1$1;
.super Ljava/lang/Object;
.source "UpdateChecker.java"

# interfaces
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/appstate/UpdateChecker$1;->onPostExecute(Ljava/util/Map;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/appstate/UpdateChecker$1;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged()V
    .locals 6

    .prologue
    .line 152
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-object v3, v3, Lcom/google/android/finsky/appstate/UpdateChecker$1;->this$0:Lcom/google/android/finsky/appstate/UpdateChecker;

    # getter for: Lcom/google/android/finsky/appstate/UpdateChecker;->mLibraries:Lcom/google/android/finsky/library/Libraries;
    invoke-static {v3}, Lcom/google/android/finsky/appstate/UpdateChecker;->access$000(Lcom/google/android/finsky/appstate/UpdateChecker;)Lcom/google/android/finsky/library/Libraries;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/library/Libraries;->getLoadedAccountHash()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget v4, v4, Lcom/google/android/finsky/appstate/UpdateChecker$1;->val$accountHashBeforeAsyncTask:I

    if-eq v3, v4, :cond_1

    .line 153
    const-string v3, "Skipping update check because account hash changed."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-object v3, v3, Lcom/google/android/finsky/appstate/UpdateChecker$1;->val$errorRunnable:Ljava/lang/Runnable;

    if-eqz v3, :cond_0

    .line 155
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-object v3, v3, Lcom/google/android/finsky/appstate/UpdateChecker$1;->val$errorRunnable:Ljava/lang/Runnable;

    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-object v3, v3, Lcom/google/android/finsky/appstate/UpdateChecker$1;->this$0:Lcom/google/android/finsky/appstate/UpdateChecker;

    # getter for: Lcom/google/android/finsky/appstate/UpdateChecker;->mDfeModel:Lcom/google/android/finsky/api/model/MultiWayUpdateController;
    invoke-static {v3}, Lcom/google/android/finsky/appstate/UpdateChecker;->access$200(Lcom/google/android/finsky/appstate/UpdateChecker;)Lcom/google/android/finsky/api/model/MultiWayUpdateController;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/MultiWayUpdateController;->getDocuments()Ljava/util/List;

    move-result-object v1

    .line 163
    .local v1, "documents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 164
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/google/android/finsky/api/model/Document;>;"
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 165
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 166
    .local v0, "document":Lcom/google/android/finsky/api/model/Document;
    invoke-static {v0}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->isGmsCore(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 167
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-object v3, v3, Lcom/google/android/finsky/appstate/UpdateChecker$1;->val$gmsCoreHelper:Lcom/google/android/finsky/appstate/GmsCoreHelper;

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->updateGmsCore(Lcom/google/android/finsky/api/model/Document;)V

    .line 168
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 173
    .end local v0    # "document":Lcom/google/android/finsky/api/model/Document;
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-object v3, v3, Lcom/google/android/finsky/appstate/UpdateChecker$1;->this$0:Lcom/google/android/finsky/appstate/UpdateChecker;

    iget-object v4, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-object v4, v4, Lcom/google/android/finsky/appstate/UpdateChecker$1;->val$logReason:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-boolean v5, v5, Lcom/google/android/finsky/appstate/UpdateChecker$1;->val$invokedInJobService:Z

    # invokes: Lcom/google/android/finsky/appstate/UpdateChecker;->handleUpdates(Ljava/util/List;Ljava/lang/String;Z)V
    invoke-static {v3, v1, v4, v5}, Lcom/google/android/finsky/appstate/UpdateChecker;->access$300(Lcom/google/android/finsky/appstate/UpdateChecker;Ljava/util/List;Ljava/lang/String;Z)V

    .line 175
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-object v3, v3, Lcom/google/android/finsky/appstate/UpdateChecker$1;->val$successRunnable:Ljava/lang/Runnable;

    if-eqz v3, :cond_0

    .line 176
    iget-object v3, p0, Lcom/google/android/finsky/appstate/UpdateChecker$1$1;->this$1:Lcom/google/android/finsky/appstate/UpdateChecker$1;

    iget-object v3, v3, Lcom/google/android/finsky/appstate/UpdateChecker$1;->val$successRunnable:Ljava/lang/Runnable;

    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
