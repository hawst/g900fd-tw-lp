.class public final Lcom/google/android/finsky/protos/DocDetails$ProductDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProductDetails"
.end annotation


# instance fields
.field public hasTitle:Z

.field public section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 336
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    .line 337
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$ProductDetails;
    .locals 1

    .prologue
    .line 340
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->title:Ljava/lang/String;

    .line 341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->hasTitle:Z

    .line 342
    invoke-static {}, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;->emptyArray()[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    .line 343
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->cachedSize:I

    .line 344
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 366
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 367
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->hasTitle:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 368
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 371
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 372
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 373
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    aget-object v0, v3, v1

    .line 374
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    if-eqz v0, :cond_2

    .line 375
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 372
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 380
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    .end local v1    # "i":I
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ProductDetails;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 388
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 389
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 393
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 394
    :sswitch_0
    return-object p0

    .line 399
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->title:Ljava/lang/String;

    .line 400
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->hasTitle:Z

    goto :goto_0

    .line 404
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 406
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    if-nez v5, :cond_2

    move v1, v4

    .line 407
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    .line 409
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    if-eqz v1, :cond_1

    .line 410
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 412
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 413
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;-><init>()V

    aput-object v5, v2, v1

    .line 414
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 415
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 412
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 406
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    array-length v1, v5

    goto :goto_1

    .line 418
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;-><init>()V

    aput-object v5, v2, v1

    .line 419
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 420
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    goto :goto_0

    .line 389
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 311
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ProductDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 350
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->hasTitle:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 351
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 353
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 354
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 355
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ProductDetails;->section:[Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;

    aget-object v0, v2, v1

    .line 356
    .local v0, "element":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    if-eqz v0, :cond_2

    .line 357
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 354
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 361
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocDetails$ProductDetailsSection;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 362
    return-void
.end method
