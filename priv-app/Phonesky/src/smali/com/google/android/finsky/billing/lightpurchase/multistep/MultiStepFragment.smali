.class public abstract Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;
.super Landroid/support/v4/app/Fragment;
.source "MultiStepFragment.java"


# instance fields
.field protected mAccount:Landroid/accounts/Account;

.field private mAnimateContinueButtonBar:Z

.field private mButtonBarVisible:Z

.field protected mContinueButton:Landroid/view/View;

.field private mContinueButtonBar:Landroid/view/View;

.field protected mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
            "<*>;"
        }
    .end annotation
.end field

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mFragmentContainer:Landroid/view/View;

.field private mIsLoading:Z

.field protected mMainView:Landroid/view/View;

.field private mProgressBar:Landroid/view/View;

.field private final mProgressBarToFragmentTransition:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 55
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBarToFragmentTransition:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->fadeOutProgressBar()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->fadeInFragment()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;
    .param p1, "x1"    # Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->syncButtonBar(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mButtonBarVisible:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    return-object v0
.end method

.method private configureContinueButton(Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 2
    .param p1, "allowContinueButtonIcon"    # Ljava/lang/Boolean;
    .param p2, "continueButtonLabel"    # Ljava/lang/String;

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 347
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/finsky/layout/IconButtonGroup;

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    check-cast v0, Lcom/google/android/finsky/layout/IconButtonGroup;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/IconButtonGroup;->setAllowIcon(Z)V

    .line 349
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    check-cast v0, Lcom/google/android/finsky/layout/IconButtonGroup;

    invoke-virtual {v0, p2}, Lcom/google/android/finsky/layout/IconButtonGroup;->setText(Ljava/lang/String;)V

    .line 353
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    instance-of v0, v0, Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private fadeInButtonBar()V
    .locals 3

    .prologue
    .line 356
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mButtonBarVisible:Z

    if-eqz v0, :cond_1

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mButtonBarVisible:Z

    .line 360
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButtonBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 361
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mAnimateContinueButtonBar:Z

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButtonBar:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f05000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private fadeInFragment()V
    .locals 3

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 313
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f05000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 315
    return-void
.end method

.method private fadeOutButtonBar()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 368
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mButtonBarVisible:Z

    if-nez v0, :cond_0

    .line 377
    :goto_0
    return-void

    .line 371
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mButtonBarVisible:Z

    .line 372
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mAnimateContinueButtonBar:Z

    if-eqz v0, :cond_1

    .line 373
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButtonBar:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->fadeOutView(Landroid/view/View;)V

    goto :goto_0

    .line 375
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private fadeOutProgressBar()V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBar:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->fadeOutView(Landroid/view/View;)V

    .line 381
    return-void
.end method

.method private fadeOutView(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 384
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f05000c

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 385
    .local v0, "anim":Landroid/view/animation/Animation;
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$4;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$4;-><init>(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 391
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 392
    return-void
.end method

.method private restoreUi()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 395
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mIsLoading:Z

    if-eqz v0, :cond_1

    .line 396
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    if-eqz v0, :cond_0

    .line 398
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 399
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->syncButtonBar(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto :goto_0
.end method

.method private slideFragmentOut()V
    .locals 3

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f05001a

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 319
    .local v0, "anim":Landroid/view/animation/Animation;
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$3;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$3;-><init>(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 325
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 326
    return-void
.end method

.method private syncButtonBar(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .local p1, "step":Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;, "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment<*>;"
    const/16 v2, 0x8

    .line 329
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->allowButtonBar()Z

    move-result v1

    if-nez v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButtonBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 342
    :goto_0
    return-void

    .line 333
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mButtonBarVisible:Z

    if-nez v1, :cond_1

    .line 334
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->fadeInButtonBar()V

    .line 336
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, "continueButtonLabel":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 338
    invoke-virtual {p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->allowContinueButtonIcon()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->configureContinueButton(Ljava/lang/Boolean;Ljava/lang/String;)V

    goto :goto_0

    .line 340
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getAccount()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mAccount:Landroid/accounts/Account;

    return-object v0
.end method

.method public hideLoading()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 210
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    if-nez v0, :cond_0

    .line 211
    const-string v0, "Illegal state: hideLoading called without fragment."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 215
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f05000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 217
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->fadeOutProgressBar()V

    .line 218
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->syncButtonBar(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    .line 219
    iput-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mIsLoading:Z

    .line 220
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->logImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0
.end method

.method public isContinueButtonEnabled()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    return v0
.end method

.method public isLoading()Z
    .locals 1

    .prologue
    .line 227
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mIsLoading:Z

    return v0
.end method

.method public logClick(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p1, "leafType"    # I
    .param p2, "leafClientLogsCookie"    # Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    .param p3, "parent"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEventWithClientCookie(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 301
    return-void
.end method

.method public logImpression(I)V
    .locals 4
    .param p1, "leafType"    # I

    .prologue
    .line 284
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v1, v2, v3, p1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 285
    return-void
.end method

.method public logImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "leaf"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3, p1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 278
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 101
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "MultiStepFragment.account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "MultiStepFragment.account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mAccount:Landroid/accounts/Account;

    .line 107
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mAccount:Landroid/accounts/Account;

    if-nez v0, :cond_2

    .line 108
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No account specified."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mAccount:Landroid/accounts/Account;

    goto :goto_0

    .line 110
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 111
    if-eqz p1, :cond_3

    .line 112
    const-string v0, "MultiStepFragment.isLoading"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mIsLoading:Z

    .line 114
    :cond_3
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 119
    const-string v0, "MultiStepFragment.isLoading"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mIsLoading:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 124
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 125
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0a022c

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    .line 127
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->restoreUi()V

    .line 128
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mMainView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBarToFragmentTransition:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 133
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 134
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 142
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 143
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mMainView:Landroid/view/View;

    .line 144
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/SetupWizardUtils;->getNavBarIfPossible(Landroid/app/Activity;)Lcom/google/android/finsky/setup/SetupWizardNavBar;

    move-result-object v0

    .line 145
    .local v0, "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    if-eqz v0, :cond_0

    .line 146
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mAnimateContinueButtonBar:Z

    .line 147
    invoke-virtual {v0}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getView()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButtonBar:Landroid/view/View;

    .line 148
    invoke-virtual {v0}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    .line 154
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButtonBar:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 155
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    new-instance v2, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment$2;-><init>(Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mMainView:Landroid/view/View;

    const v2, 0x7f0a010c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBar:Landroid/view/View;

    .line 168
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mMainView:Landroid/view/View;

    const v2, 0x7f0a022c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    .line 169
    return-void

    .line 150
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mAnimateContinueButtonBar:Z

    .line 151
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mMainView:Landroid/view/View;

    const v2, 0x7f0a0230

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButtonBar:Landroid/view/View;

    .line 152
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mMainView:Landroid/view/View;

    const v2, 0x7f0a0231

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    goto :goto_0
.end method

.method public setContinueButtonEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mContinueButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 267
    return-void
.end method

.method public showLoading()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 183
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mButtonBarVisible:Z

    if-eqz v0, :cond_0

    .line 184
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->fadeOutButtonBar()V

    .line 187
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mIsLoading:Z

    if-eqz v0, :cond_1

    .line 204
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    if-eqz v0, :cond_2

    .line 192
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->slideFragmentOut()V

    .line 193
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f050017

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 202
    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mIsLoading:Z

    .line 203
    const/16 v0, 0xd5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->logImpression(I)V

    goto :goto_0

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 198
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBar:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f05000e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method public showStep(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .local p1, "step":Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;, "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment<*>;"
    const/4 v3, 0x0

    .line 238
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 240
    .local v0, "transaction":Landroid/support/v4/app/FragmentTransaction;
    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mIsLoading:Z

    if-eqz v1, :cond_1

    .line 244
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 245
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mMainView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mProgressBarToFragmentTransition:Ljava/lang/Runnable;

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 255
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    if-eqz v1, :cond_0

    .line 256
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 259
    :cond_0
    const v1, 0x7f0a022c

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 260
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 261
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    .line 262
    iput-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mIsLoading:Z

    .line 263
    return-void

    .line 248
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mCurrentFragment:Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;

    if-eqz v1, :cond_2

    .line 249
    const v1, 0x7f050017

    const v2, 0x7f05001a

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    .line 251
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->mFragmentContainer:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 252
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;->syncButtonBar(Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;)V

    goto :goto_0
.end method
