.class public Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
.super Ljava/lang/Object;
.source "SimpleAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/SimpleAlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mArguments:Landroid/os/Bundle;

.field private mTarget:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mTarget:Landroid/support/v4/app/Fragment;

    return-void
.end method


# virtual methods
.method public build()Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .locals 1

    .prologue
    .line 301
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;-><init>()V

    .line 302
    .local v0, "dialogFragment":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->configureDialog(Lcom/google/android/finsky/activities/SimpleAlertDialog;)V

    .line 303
    return-object v0
.end method

.method public configureDialog(Lcom/google/android/finsky/activities/SimpleAlertDialog;)V
    .locals 2
    .param p1, "dialogFragment"    # Lcom/google/android/finsky/activities/SimpleAlertDialog;

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setArguments(Landroid/os/Bundle;)V

    .line 316
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mTarget:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mTarget:Landroid/support/v4/app/Fragment;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 319
    :cond_0
    return-void
.end method

.method public setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "target"    # Landroid/support/v4/app/Fragment;
    .param p2, "requestCode"    # I
    .param p3, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 285
    iput-object p1, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mTarget:Landroid/support/v4/app/Fragment;

    .line 287
    if-nez p3, :cond_0

    if-eqz p2, :cond_1

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "extra_arguments"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "target_request_code"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 292
    :cond_1
    return-object p0
.end method

.method public setCanceledOnTouchOutside(Z)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "canceledOnTouchOutside"    # Z

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "cancel_on_touch_outside"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 230
    return-object p0
.end method

.method public setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "impressionType"    # I
    .param p2, "serverLogsCookie"    # [B
    .param p3, "positiveClickType"    # I
    .param p4, "negativeClickType"    # I
    .param p5, "account"    # Landroid/accounts/Account;

    .prologue
    .line 257
    if-nez p5, :cond_0

    .line 258
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object p5

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "log_account"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "impression_type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 263
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "impression_cookie"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 264
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "click_event_type_positive"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 265
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "click_event_type_negative"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 267
    return-object p0
.end method

.method public setLayoutId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "layoutId"    # I

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "layoutId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 176
    return-object p0
.end method

.method public setMessage(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-object p0
.end method

.method public setMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "messageHtml"    # Ljava/lang/String;

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "messageHtml"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    return-object p0
.end method

.method public setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "messageId"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "message_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    return-object p0
.end method

.method public setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "negativeId"    # I

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "negative_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 209
    return-object p0
.end method

.method public setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "positiveId"    # I

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "positive_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 198
    return-object p0
.end method

.method public setThemeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "themeId"    # I

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "theme_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 221
    return-object p0
.end method

.method public setTitleId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "titleId"    # I

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "title_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 187
    return-object p0
.end method

.method public setViewConfiguration(Landroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .locals 2
    .param p1, "configArguments"    # Landroid/os/Bundle;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->mArguments:Landroid/os/Bundle;

    const-string v1, "config_arguments"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 241
    return-object p0
.end method
