.class public Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;
.super Lcom/google/android/finsky/activities/SimpleAlertDialog;
.source "GPlusDialogsHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/GPlusDialogsHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GPlusSignUpDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;-><init>()V

    return-void
.end method

.method public static newInstance()Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;
    .locals 4

    .prologue
    .line 37
    new-instance v1, Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;

    invoke-direct {v1}, Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;-><init>()V

    .line 38
    .local v1, "dialog":Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 39
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v2, 0x7f0c03b8

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c03ba

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c0134

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 42
    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->configureDialog(Lcom/google/android/finsky/activities/SimpleAlertDialog;)V

    .line 43
    return-object v1
.end method


# virtual methods
.method protected onPositiveClick()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->onPositiveClick()V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/finsky/utils/GPlusDialogsHelper$GPlusSignUpDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/GPlusUtils;->launchGPlusSignUp(Landroid/app/Activity;)V

    .line 50
    return-void
.end method
