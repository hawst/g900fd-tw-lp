.class public Lcom/google/android/finsky/activities/ContentFilterActivity;
.super Landroid/app/Activity;
.source "ContentFilterActivity.java"

# interfaces
.implements Lcom/google/android/finsky/layout/ButtonBar$ClickListener;


# instance fields
.field private mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

.field private mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mLevel:Lcom/google/android/finsky/config/ContentLevel;

.field private mMoreInfoView:Landroid/widget/TextView;

.field private mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

.field private mRadioButtonsView:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v1, 0x13b

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/finsky/activities/ContentFilterActivity;Lcom/google/android/finsky/config/ContentLevel;)Lcom/google/android/finsky/config/ContentLevel;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/ContentFilterActivity;
    .param p1, "x1"    # Lcom/google/android/finsky/config/ContentLevel;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mLevel:Lcom/google/android/finsky/config/ContentLevel;

    return-object p1
.end method

.method public static getLabel(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "level"    # I

    .prologue
    .line 139
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 140
    .local v0, "res":Landroid/content/res/Resources;
    packed-switch p1, :pswitch_data_0

    .line 154
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 142
    :pswitch_0
    const v1, 0x7f0c02b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 144
    :pswitch_1
    const v1, 0x7f0c02b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 146
    :pswitch_2
    const v1, 0x7f0c02b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 148
    :pswitch_3
    const v1, 0x7f0c02b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 150
    :pswitch_4
    const v1, 0x7f0c02b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 152
    :pswitch_5
    const v1, 0x7f0c02b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 140
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setupRadioButton(Landroid/view/LayoutInflater;Lcom/google/android/finsky/config/ContentLevel;II)V
    .locals 5
    .param p1, "layoutInflater"    # Landroid/view/LayoutInflater;
    .param p2, "level"    # Lcom/google/android/finsky/config/ContentLevel;
    .param p3, "checkboxId"    # I
    .param p4, "stringResourceId"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 79
    const v3, 0x7f04016c

    iget-object v4, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mRadioButtonsView:Landroid/widget/RadioGroup;

    invoke-virtual {p1, v3, v4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 81
    .local v0, "button":Landroid/widget/RadioButton;
    invoke-virtual {v0, p4}, Landroid/widget/RadioButton;->setText(I)V

    .line 82
    invoke-virtual {v0, p2}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    .line 83
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setFocusable(Z)V

    .line 84
    iget-object v3, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mLevel:Lcom/google/android/finsky/config/ContentLevel;

    if-ne v3, p2, :cond_0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 85
    invoke-virtual {v0, p3}, Landroid/widget/RadioButton;->setId(I)V

    .line 87
    iget-object v1, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mRadioButtonsView:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 88
    return-void

    :cond_0
    move v1, v2

    .line 84
    goto :goto_0
.end method

.method private setupViews()V
    .locals 6

    .prologue
    .line 92
    iget-object v2, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mRadioButtonsView:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 94
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 95
    .local v0, "inflater":Landroid/view/LayoutInflater;
    sget-object v2, Lcom/google/android/finsky/config/ContentLevel;->EVERYONE:Lcom/google/android/finsky/config/ContentLevel;

    const v3, 0x7f0a0034

    const v4, 0x7f0c02b3

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/finsky/activities/ContentFilterActivity;->setupRadioButton(Landroid/view/LayoutInflater;Lcom/google/android/finsky/config/ContentLevel;II)V

    .line 97
    sget-object v2, Lcom/google/android/finsky/config/ContentLevel;->LOW_MATURITY:Lcom/google/android/finsky/config/ContentLevel;

    const v3, 0x7f0a0035

    const v4, 0x7f0c02b4

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/finsky/activities/ContentFilterActivity;->setupRadioButton(Landroid/view/LayoutInflater;Lcom/google/android/finsky/config/ContentLevel;II)V

    .line 99
    sget-object v2, Lcom/google/android/finsky/config/ContentLevel;->MEDIUM_MATURITY:Lcom/google/android/finsky/config/ContentLevel;

    const v3, 0x7f0a0036

    const v4, 0x7f0c02b5

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/finsky/activities/ContentFilterActivity;->setupRadioButton(Landroid/view/LayoutInflater;Lcom/google/android/finsky/config/ContentLevel;II)V

    .line 101
    sget-object v2, Lcom/google/android/finsky/config/ContentLevel;->HIGH_MATURITY:Lcom/google/android/finsky/config/ContentLevel;

    const v3, 0x7f0a0037

    const v4, 0x7f0c02b6

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/finsky/activities/ContentFilterActivity;->setupRadioButton(Landroid/view/LayoutInflater;Lcom/google/android/finsky/config/ContentLevel;II)V

    .line 103
    sget-object v2, Lcom/google/android/finsky/config/ContentLevel;->SHOW_ALL:Lcom/google/android/finsky/config/ContentLevel;

    const v3, 0x7f0a0038

    const v4, 0x7f0c02b7

    invoke-direct {p0, v0, v2, v3, v4}, Lcom/google/android/finsky/activities/ContentFilterActivity;->setupRadioButton(Landroid/view/LayoutInflater;Lcom/google/android/finsky/config/ContentLevel;II)V

    .line 105
    iget-object v2, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mRadioButtonsView:Landroid/widget/RadioGroup;

    new-instance v3, Lcom/google/android/finsky/activities/ContentFilterActivity$1;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/activities/ContentFilterActivity$1;-><init>(Lcom/google/android/finsky/activities/ContentFilterActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 112
    const-string v2, "%s <a href=\'%s\'>%s</a>"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const v5, 0x7f0c02b8

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/activities/ContentFilterActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/finsky/config/G;->contentFilterInfoUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v5}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const v5, 0x7f0c02b9

    invoke-virtual {p0, v5}, Lcom/google/android/finsky/activities/ContentFilterActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 115
    .local v1, "moreInfoText":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mMoreInfoView:Landroid/widget/TextView;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v2, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mMoreInfoView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 119
    iget-object v2, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const v3, 0x7f0c02bb

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/ButtonBar;->setPositiveButtonTitle(I)V

    .line 120
    iget-object v2, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    const v3, 0x7f0c02bc

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/ButtonBar;->setNegativeButtonTitle(I)V

    .line 121
    iget-object v2, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/layout/ButtonBar;->setClickListener(Lcom/google/android/finsky/layout/ButtonBar$ClickListener;)V

    .line 122
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v0, 0x7f04004f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/ContentFilterActivity;->setContentView(I)V

    .line 56
    if-nez p1, :cond_1

    .line 57
    invoke-static {p0}, Lcom/google/android/finsky/config/ContentLevel;->importFromSettings(Landroid/content/Context;)Lcom/google/android/finsky/config/ContentLevel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mLevel:Lcom/google/android/finsky/config/ContentLevel;

    .line 63
    :goto_0
    const v0, 0x7f0a012d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/ContentFilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mRadioButtonsView:Landroid/widget/RadioGroup;

    .line 64
    const v0, 0x7f0a012e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/ContentFilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mMoreInfoView:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0a0114

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/ContentFilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ButtonBar;

    iput-object v0, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mButtonBar:Lcom/google/android/finsky/layout/ButtonBar;

    .line 67
    invoke-direct {p0}, Lcom/google/android/finsky/activities/ContentFilterActivity;->setupViews()V

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/ContentFilterActivity;->setResult(I)V

    .line 71
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 72
    if-nez p1, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    iget-object v1, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 75
    :cond_0
    return-void

    .line 59
    :cond_1
    const-string v0, "level"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/finsky/config/ContentLevel;->convertFromLegacyValue(I)Lcom/google/android/finsky/config/ContentLevel;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mLevel:Lcom/google/android/finsky/config/ContentLevel;

    goto :goto_0
.end method

.method public onNegativeButtonClick()V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0xff

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ContentFilterActivity;->finish()V

    .line 174
    return-void
.end method

.method public onPositiveButtonClick()V
    .locals 7

    .prologue
    .line 159
    iget-object v3, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v4, 0xfe

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mNode:Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 160
    invoke-static {p0}, Lcom/google/android/finsky/config/ContentLevel;->importFromSettings(Landroid/content/Context;)Lcom/google/android/finsky/config/ContentLevel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/config/ContentLevel;->getValue()I

    move-result v0

    .line 161
    .local v0, "currentFilterLevel":I
    iget-object v3, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mLevel:Lcom/google/android/finsky/config/ContentLevel;

    invoke-virtual {v3}, Lcom/google/android/finsky/config/ContentLevel;->getValue()I

    move-result v2

    .line 162
    .local v2, "selectedFilterLevel":I
    if-eq v0, v2, :cond_0

    .line 163
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 164
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "ContentFilterActivity_selectedFilterLevel"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 165
    const/4 v3, -0x1

    invoke-virtual {p0, v3, v1}, Lcom/google/android/finsky/activities/ContentFilterActivity;->setResult(ILandroid/content/Intent;)V

    .line 167
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/ContentFilterActivity;->finish()V

    .line 168
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 126
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 127
    const-string v0, "level"

    iget-object v1, p0, Lcom/google/android/finsky/activities/ContentFilterActivity;->mLevel:Lcom/google/android/finsky/config/ContentLevel;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/ContentLevel;->getValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 128
    return-void
.end method
