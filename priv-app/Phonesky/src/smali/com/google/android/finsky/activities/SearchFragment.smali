.class public Lcom/google/android/finsky/activities/SearchFragment;
.super Lcom/google/android/finsky/fragments/PageFragment;
.source "SearchFragment.java"


# instance fields
.field private mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

.field private mContextBackendId:I

.field private mIsAdapterSet:Z

.field private mListView:Landroid/widget/ListView;

.field private mListViewRestoreBundle:Landroid/os/Bundle;

.field private mQuery:Ljava/lang/String;

.field private mRetriedSearch:Z

.field private mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

.field private mSearchUrl:Ljava/lang/String;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mhasLoggedSearchEvent:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/PageFragment;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mRetriedSearch:Z

    .line 69
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListViewRestoreBundle:Landroid/os/Bundle;

    .line 72
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SearchFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/SearchFragment;)Lcom/google/android/finsky/adapters/CardListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SearchFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SearchFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListViewRestoreBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/SearchFragment;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SearchFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDataView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/finsky/activities/SearchFragment;
    .locals 2
    .param p0, "query"    # Ljava/lang/String;
    .param p1, "searchUrl"    # Ljava/lang/String;
    .param p2, "backendId"    # I

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/finsky/activities/SearchFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SearchFragment;-><init>()V

    .line 77
    .local v0, "fragment":Lcom/google/android/finsky/activities/SearchFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SearchFragment;->setDfeToc(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 78
    const-string v1, "SearchFragment.searchUrl"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/activities/SearchFragment;->setArgument(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v1, "SearchFragment.query"

    if-eqz p0, :cond_0

    .end local p0    # "query":Ljava/lang/String;
    :goto_0
    invoke-virtual {v0, v1, p0}, Lcom/google/android/finsky/activities/SearchFragment;->setArgument(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v1, "SearchFragment.backendId"

    if-ltz p2, :cond_1

    .end local p2    # "backendId":I
    :goto_1
    invoke-virtual {v0, v1, p2}, Lcom/google/android/finsky/activities/SearchFragment;->setArgument(Ljava/lang/String;I)V

    .line 81
    return-object v0

    .line 79
    .restart local p0    # "query":Ljava/lang/String;
    .restart local p2    # "backendId":I
    :cond_0
    const-string p0, ""

    goto :goto_0

    .line 80
    .end local p0    # "query":Ljava/lang/String;
    :cond_1
    const/4 p2, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected createLayoutSwitcher(Lcom/google/android/finsky/layout/ContentFrame;)Lcom/google/android/finsky/layout/LayoutSwitcher;
    .locals 7
    .param p1, "frame"    # Lcom/google/android/finsky/layout/ContentFrame;

    .prologue
    .line 189
    new-instance v0, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;

    const v2, 0x7f0a0219

    const v3, 0x7f0a02a3

    const v4, 0x7f0a0109

    const/4 v6, 0x2

    move-object v1, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;-><init>(Landroid/view/View;IIILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;I)V

    return-object v0
.end method

.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 267
    const v0, 0x7f0400ae

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeSearch;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 157
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 159
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeSearch;->hasBackendId()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v2, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeSearch;->getBackendId()I

    move-result v2

    invoke-interface {v1, v2, v3}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 166
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->rebindActionBar()V

    .line 168
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDataView:Landroid/view/ViewGroup;

    const v2, 0x7f0a0118

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    .line 169
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 170
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 172
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDataView:Landroid/view/ViewGroup;

    const v2, 0x7f0a0278

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 173
    .local v0, "emptyResultsView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c01d0

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->isDataReady()Z

    move-result v1

    if-nez v1, :cond_1

    .line 176
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->switchToLoading()V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->requestData()V

    .line 179
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->rebindActionBar()V

    .line 184
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-interface {v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->enableActionBarOverlay()V

    .line 185
    return-void

    .line 181
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->rebindAdapter()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 86
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SearchFragment.query"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    .line 88
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SearchFragment.searchUrl"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchUrl:Ljava/lang/String;

    .line 89
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SearchFragment.backendId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mContextBackendId:I

    .line 91
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/SearchFragment;->setRetainInstance(Z)V

    .line 92
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/PageFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/ContentFrame;

    .line 99
    .local v1, "frame":Lcom/google/android/finsky/layout/ContentFrame;
    iget-object v2, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v2, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;

    .line 100
    .local v2, "headerListLayout":Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;
    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 101
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Lcom/google/android/finsky/activities/SearchFragment$1;

    invoke-direct {v3, p0, v0}, Lcom/google/android/finsky/activities/SearchFragment$1;-><init>(Lcom/google/android/finsky/activities/SearchFragment;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 151
    const v3, 0x7f0a0117

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setContentViewId(I)V

    .line 152
    return-object v1
.end method

.method public onDataChanged()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 282
    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeSearch;->isReady()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeSearch;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    if-nez v3, :cond_1

    .line 283
    iget-boolean v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mRetriedSearch:Z

    if-nez v3, :cond_0

    .line 287
    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/google/android/finsky/api/DfeUtils;->formSearchUrl(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 288
    .local v0, "allCorpusSearch":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchUrl:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 289
    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeSearch;->getBackendId()I

    move-result v1

    .line 290
    .local v1, "backendIdForMessage":I
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getNoSearchResultsMessageId(I)I

    move-result v4

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 293
    .local v2, "noResultsMessage":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    invoke-static {v3, v2, v4}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 295
    iput-boolean v7, p0, Lcom/google/android/finsky/activities/SearchFragment;->mRetriedSearch:Z

    .line 296
    iput-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchUrl:Ljava/lang/String;

    .line 297
    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/api/model/DfeSearch;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 298
    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v3, p0}, Lcom/google/android/finsky/api/model/DfeSearch;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 299
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    .line 300
    iput-boolean v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mhasLoggedSearchEvent:Z

    .line 301
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->requestData()V

    .line 310
    .end local v0    # "allCorpusSearch":Ljava/lang/String;
    .end local v1    # "backendIdForMessage":I
    .end local v2    # "noResultsMessage":Ljava/lang/String;
    :goto_0
    return-void

    .line 306
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c01d0

    new-array v6, v7, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    invoke-static {v3, v4, v5}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 309
    :cond_1
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDataChanged()V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 395
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 396
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v2, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListViewRestoreBundle:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/adapters/CardListAdapter;->onSaveInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V

    .line 400
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_1

    .line 401
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 403
    :cond_1
    iput-object v4, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    .line 405
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    if-eqz v1, :cond_2

    .line 406
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    invoke-virtual {v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->onDestroyView()V

    .line 407
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    invoke-virtual {v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->onDestroy()V

    .line 408
    iput-object v4, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    .line 410
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mIsAdapterSet:Z

    .line 413
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    check-cast v1, Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/MainActivity;->getActionBarHelper()Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    move-result-object v0

    .line 414
    .local v0, "actionBarHelper":Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;
    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->setDefaultSearchQuery(Ljava/lang/String;)V

    .line 416
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDataView:Landroid/view/ViewGroup;

    instance-of v1, v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v1, :cond_3

    .line 419
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v1, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->detach()V

    .line 422
    :cond_3
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDestroyView()V

    .line 423
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    .prologue
    .line 436
    return-void
.end method

.method public rebindActionBar()V
    .locals 15

    .prologue
    .line 322
    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    check-cast v8, Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v8}, Lcom/google/android/finsky/activities/MainActivity;->getActionBarHelper()Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    move-result-object v1

    .line 323
    .local v1, "actionBarHelper":Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;
    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    invoke-virtual {v1, v8}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->setDefaultSearchQuery(Ljava/lang/String;)V

    .line 329
    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/DfeSearch;->hasBackendId()Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/DfeSearch;->getBackendId()I

    move-result v2

    .line 332
    .local v2, "backendId":I
    :goto_0
    const/4 v8, 0x3

    if-ne v2, v8, :cond_2

    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    const-string v9, "pub:"

    invoke-virtual {v8, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 337
    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0332

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    const-string v13, "pub:"

    const-string v14, ""

    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 339
    .local v4, "breadcrumb":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v8, v4}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 347
    .end local v4    # "breadcrumb":Ljava/lang/String;
    :goto_1
    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDataView:Landroid/view/ViewGroup;

    if-eqz v8, :cond_0

    .line 348
    iget-object v5, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDataView:Landroid/view/ViewGroup;

    check-cast v5, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 349
    .local v5, "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mContext:Landroid/content/Context;

    invoke-static {v8, v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v0

    .line 351
    .local v0, "actionBarColor":I
    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v8, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 352
    const v8, 0x7f0a0039

    invoke-virtual {v5, v8}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 355
    .local v3, "backgroundLayer":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 356
    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 359
    .end local v0    # "actionBarColor":I
    .end local v3    # "backgroundLayer":Landroid/view/View;
    .end local v5    # "headerListLayout":Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    :cond_0
    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    const/4 v9, 0x1

    invoke-interface {v8, v2, v9}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 360
    return-void

    .line 329
    .end local v2    # "backendId":I
    :cond_1
    iget v2, p0, Lcom/google/android/finsky/activities/SearchFragment;->mContextBackendId:I

    goto :goto_0

    .line 341
    .restart local v2    # "backendId":I
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 342
    .local v6, "res":Landroid/content/res/Resources;
    const v8, 0x7f0f0007

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-eqz v8, :cond_3

    const v7, 0x7f0c0331

    .line 344
    .local v7, "titleResourceId":I
    :goto_2
    iget-object v8, p0, Lcom/google/android/finsky/activities/SearchFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    aput-object v11, v9, v10

    invoke-virtual {v6, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    goto :goto_1

    .line 342
    .end local v7    # "titleResourceId":I
    :cond_3
    const v7, 0x7f0c0330

    goto :goto_2
.end method

.method public rebindAdapter()V
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 203
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    if-nez v1, :cond_1

    .line 204
    const-string v0, "List view null, ignoring."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->isDataReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListViewRestoreBundle:Landroid/os/Bundle;

    invoke-static {v1}, Lcom/google/android/finsky/adapters/SearchAdapter;->hasRestoreData(Landroid/os/Bundle;)Z

    move-result v8

    .line 211
    .local v8, "isRestoring":Z
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    if-nez v1, :cond_2

    .line 213
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v2, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeSearch;->getServerLogsCookie()[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 218
    const/4 v9, 0x0

    .line 219
    .local v9, "tabElementNode":Lcom/google/android/finsky/layout/play/GenericUiElementNode;
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeSearch;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeSearch;->getContainerDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v10

    .line 221
    .local v10, "logsCookieFromRootDoc":[B
    :goto_1
    new-instance v9, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    .end local v9    # "tabElementNode":Lcom/google/android/finsky/layout/play/GenericUiElementNode;
    const/16 v1, 0x198

    invoke-direct {v9, v1, v10, v0, p0}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 224
    .restart local v9    # "tabElementNode":Lcom/google/android/finsky/layout/play/GenericUiElementNode;
    invoke-virtual {p0, v9}, Lcom/google/android/finsky/activities/SearchFragment;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 227
    new-instance v0, Lcom/google/android/finsky/adapters/SearchAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v4, p0, Lcom/google/android/finsky/activities/SearchFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/finsky/adapters/SearchAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/DfeSearch;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    .line 234
    .end local v9    # "tabElementNode":Lcom/google/android/finsky/layout/play/GenericUiElementNode;
    .end local v10    # "logsCookieFromRootDoc":[B
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mIsAdapterSet:Z

    if-nez v0, :cond_4

    .line 235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mIsAdapterSet:Z

    .line 238
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mListView:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/finsky/activities/SearchFragment$2;

    invoke-direct {v1, p0, v8}, Lcom/google/android/finsky/activities/SearchFragment$2;-><init>(Lcom/google/android/finsky/activities/SearchFragment;Z)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .restart local v9    # "tabElementNode":Lcom/google/android/finsky/layout/play/GenericUiElementNode;
    :cond_3
    move-object v10, v0

    .line 219
    goto :goto_1

    .line 260
    .end local v9    # "tabElementNode":Lcom/google/android/finsky/layout/play/GenericUiElementNode;
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mAdapter:Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/adapters/CardListAdapter;->updateAdapterData(Lcom/google/android/finsky/api/model/ContainerList;)V

    goto :goto_0
.end method

.method protected rebindViews()V
    .locals 0

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->rebindAdapter()V

    .line 315
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->rebindActionBar()V

    .line 316
    return-void
.end method

.method public refresh()V
    .locals 1

    .prologue
    .line 272
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/SearchFragment;->isDataReady()Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeSearch;->clearTransientState()V

    .line 276
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->refresh()V

    .line 278
    :cond_0
    return-void
.end method

.method protected requestData()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 364
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    if-nez v1, :cond_2

    .line 365
    new-instance v1, Lcom/google/android/finsky/api/model/DfeSearch;

    iget-object v2, p0, Lcom/google/android/finsky/activities/SearchFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchUrl:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/finsky/api/model/DfeSearch;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    .line 366
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/api/model/DfeSearch;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 367
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1, p0}, Lcom/google/android/finsky/api/model/DfeSearch;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 371
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mhasLoggedSearchEvent:Z

    if-nez v1, :cond_2

    .line 372
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreSearchEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;

    move-result-object v0

    .line 373
    .local v0, "event":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 374
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mQuery:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;->query:Ljava/lang/String;

    .line 375
    iput-boolean v5, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;->hasQuery:Z

    .line 377
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchUrl:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 378
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;->queryUrl:Ljava/lang/String;

    .line 379
    iput-boolean v5, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;->hasQueryUrl:Z

    .line 381
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logSearchEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;)V

    .line 382
    iput-boolean v5, p0, Lcom/google/android/finsky/activities/SearchFragment;->mhasLoggedSearchEvent:Z

    .line 385
    .end local v0    # "event":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreSearchEvent;
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/activities/SearchFragment;->mSearchData:Lcom/google/android/finsky/api/model/DfeSearch;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeSearch;->startLoadItems()V

    .line 386
    return-void
.end method
