.class final Lcom/google/android/finsky/gearhead/GearheadStateMonitor$4;
.super Ljava/lang/Object;
.source "GearheadStateMonitor.java"

# interfaces
.implements Lcom/google/android/gms/car/Car$CarConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->initialize(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnected(I)V
    .locals 1
    .param p1, "connectionType"    # I

    .prologue
    .line 131
    const/4 v0, 0x1

    # setter for: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sIsProjecting:Z
    invoke-static {v0}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$002(Z)Z

    .line 132
    # invokes: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->onReady()V
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$100()V

    .line 133
    return-void
.end method

.method public onDisconnected()V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    # setter for: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sIsProjecting:Z
    invoke-static {v0}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$002(Z)Z

    .line 138
    # invokes: Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->onReady()V
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->access$100()V

    .line 139
    return-void
.end method
