.class public Lcom/google/android/finsky/fragments/DfeListBinder;
.super Lcom/google/android/finsky/fragments/ViewBinder;
.source "DfeListBinder.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/fragments/ViewBinder",
        "<",
        "Lcom/google/android/finsky/api/model/DfeList;",
        ">;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/google/android/finsky/api/model/OnDataChangedListener;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

.field private final mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field private mContentLayout:Landroid/view/ViewGroup;

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private mHasLoadedAtLeastOnce:Z

.field private mListView:Lcom/google/android/finsky/layout/play/PlayListView;

.field protected mToc:Lcom/google/android/finsky/api/model/DfeToc;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/ClientMutationCache;)V
    .locals 0
    .param p1, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/ViewBinder;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 46
    iput-object p2, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 47
    iput-object p3, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 48
    return-void
.end method

.method private detachFromData()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    .line 155
    :cond_0
    return-void
.end method

.method private getAdapterForListType(Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ILandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Lcom/google/android/finsky/adapters/FinskyListAdapter;
    .locals 14
    .param p1, "containerDocument"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "quickLinks"    # [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "tabMode"    # I
    .param p5, "savedInstanceState"    # Landroid/os/Bundle;
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 178
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasEditorialSeriesContainer()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    new-instance v0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mNavManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v3, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v4, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v6, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/finsky/api/model/DfeList;

    move-object v5, p1

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeList;Landroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 185
    :goto_0
    return-object v0

    .line 182
    :cond_0
    if-eqz p5, :cond_2

    const/4 v10, 0x1

    .line 183
    .local v10, "isRestoring":Z
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasRecommendationsContainerTemplate()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasRecommendationsContainerWithHeaderTemplate()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v11, 0x1

    .line 185
    .local v11, "showReasons":Z
    :goto_2
    new-instance v0, Lcom/google/android/finsky/adapters/CardListAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v3, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mNavManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v4, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v5, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v6, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    iget-object v7, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    check-cast v7, Lcom/google/android/finsky/api/model/ContainerList;

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move/from16 v12, p4

    move-object/from16 v13, p6

    invoke-direct/range {v0 .. v13}, Lcom/google/android/finsky/adapters/CardListAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/ContainerList;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0

    .line 182
    .end local v10    # "isRestoring":Z
    .end local v11    # "showReasons":Z
    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    .line 183
    .restart local v10    # "isRestoring":Z
    :cond_3
    const/4 v11, 0x0

    goto :goto_2
.end method


# virtual methods
.method public bind(Landroid/view/ViewGroup;Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ILandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 13
    .param p1, "root"    # Landroid/view/ViewGroup;
    .param p2, "containerDocument"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "quickLinks"    # [Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "tabMode"    # I
    .param p6, "savedInstanceState"    # Landroid/os/Bundle;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mContentLayout:Landroid/view/ViewGroup;

    .line 65
    const v1, 0x7f0a0118

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayListView;

    iput-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    .line 66
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayListView;->setAnimateChanges(Z)V

    .line 67
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayListView;->setItemsCanFocus(Z)V

    .line 71
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v1

    if-nez v1, :cond_4

    .line 73
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mContentLayout:Landroid/view/ViewGroup;

    const v2, 0x7f0a026e

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .line 74
    .local v12, "noResultsView":Landroid/view/View;
    if-eqz v12, :cond_2

    .line 76
    const v1, 0x7f0a0277

    invoke-virtual {v12, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/google/android/play/image/FifeImageView;

    .line 78
    .local v9, "emptyImageContainer":Lcom/google/android/play/image/FifeImageView;
    if-eqz p2, :cond_3

    const/4 v1, 0x4

    invoke-virtual {p2, v1}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 79
    const/4 v1, 0x4

    invoke-virtual {p2, v1}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/protos/Common$Image;

    .line 80
    .local v8, "emptyImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v1, v8, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v2, v8, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v3

    invoke-virtual {v9, v1, v2, v3}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 83
    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 89
    .end local v8    # "emptyImage":Lcom/google/android/finsky/protos/Common$Image;
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->hasEmptyContainer()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getEmptyContainer()Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;

    move-result-object v1

    iget-object v10, v1, Lcom/google/android/finsky/protos/DocumentV2$EmptyContainer;->emptyMessage:Ljava/lang/String;

    .line 91
    .local v10, "emptyMessage":Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 92
    const v1, 0x7f0a0278

    invoke-virtual {v12, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 94
    .local v11, "emptyMessageView":Landroid/widget/TextView;
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    .end local v10    # "emptyMessage":Ljava/lang/String;
    .end local v11    # "emptyMessageView":Landroid/widget/TextView;
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v1, v12}, Lcom/google/android/finsky/layout/play/PlayListView;->setEmptyView(Landroid/view/View;)V

    .line 124
    .end local v9    # "emptyImageContainer":Lcom/google/android/play/image/FifeImageView;
    .end local v12    # "noResultsView":Landroid/view/View;
    :cond_2
    :goto_1
    return-void

    .line 85
    .restart local v9    # "emptyImageContainer":Lcom/google/android/play/image/FifeImageView;
    .restart local v12    # "noResultsView":Landroid/view/View;
    :cond_3
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_0

    .line 103
    .end local v9    # "emptyImageContainer":Lcom/google/android/play/image/FifeImageView;
    .end local v12    # "noResultsView":Landroid/view/View;
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    if-eqz v1, :cond_5

    .line 104
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    invoke-virtual {v1}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->onDestroyView()V

    .line 105
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    invoke-virtual {v1}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->onDestroy()V

    :cond_5
    move-object v1, p0

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    .line 108
    invoke-direct/range {v1 .. v7}, Lcom/google/android/finsky/fragments/DfeListBinder;->getAdapterForListType(Lcom/google/android/finsky/api/model/Document;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ILandroid/os/Bundle;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Lcom/google/android/finsky/adapters/FinskyListAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    .line 112
    iget-boolean v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mHasLoadedAtLeastOnce:Z

    if-eqz v1, :cond_6

    .line 113
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayListView;->setEmptyView(Landroid/view/View;)V

    .line 117
    :goto_2
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 118
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayListView;->setRecyclerListener(Landroid/widget/AbsListView$RecyclerListener;)V

    .line 121
    if-eqz p6, :cond_2

    .line 122
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    move-object/from16 v0, p6

    invoke-virtual {v1, v2, v0}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->onRestoreInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V

    goto :goto_1

    .line 115
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mContentLayout:Landroid/view/ViewGroup;

    const v3, 0x7f0a026e

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayListView;->setEmptyView(Landroid/view/View;)V

    goto :goto_2
.end method

.method public onDataChanged()V
    .locals 3

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mHasLoadedAtLeastOnce:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    iget-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mContentLayout:Landroid/view/ViewGroup;

    const v2, 0x7f0a026e

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayListView;->setEmptyView(Landroid/view/View;)V

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mHasLoadedAtLeastOnce:Z

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->notifyDataSetChanged()V

    .line 147
    :cond_1
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 158
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/DfeListBinder;->detachFromData()V

    .line 159
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->onDestroyView()V

    .line 161
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->onDestroy()V

    .line 162
    iput-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    .line 164
    :cond_0
    iput-object v1, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    .line 165
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->triggerFooterErrorMode()V

    .line 135
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mAdapter:Lcom/google/android/finsky/adapters/FinskyListAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/adapters/FinskyListAdapter;->onSaveInstanceState(Landroid/widget/ListView;Landroid/os/Bundle;)V

    .line 172
    :cond_0
    return-void
.end method

.method public setData(Lcom/google/android/finsky/api/model/DfeList;)V
    .locals 1
    .param p1, "data"    # Lcom/google/android/finsky/api/model/DfeList;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/DfeListBinder;->detachFromData()V

    .line 53
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/ViewBinder;->setData(Ljava/lang/Object;)V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mHasLoadedAtLeastOnce:Z

    .line 55
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/finsky/fragments/DfeListBinder;->mData:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 59
    :cond_0
    return-void
.end method

.method public bridge synthetic setData(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/finsky/api/model/DfeList;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/fragments/DfeListBinder;->setData(Lcom/google/android/finsky/api/model/DfeList;)V

    return-void
.end method
