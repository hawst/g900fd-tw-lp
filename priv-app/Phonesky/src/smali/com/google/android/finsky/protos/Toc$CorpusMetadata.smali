.class public final Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Toc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Toc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CorpusMetadata"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;


# instance fields
.field public backend:I

.field public hasBackend:Z

.field public hasLandingUrl:Z

.field public hasLibraryName:Z

.field public hasName:Z

.field public hasRecsWidgetUrl:Z

.field public hasShopName:Z

.field public landingUrl:Ljava/lang/String;

.field public libraryName:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public recsWidgetUrl:Ljava/lang/String;

.field public shopName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 447
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 448
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->clear()Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    .line 449
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    .locals 2

    .prologue
    .line 412
    sget-object v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    if-nez v0, :cond_1

    .line 413
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 415
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    if-nez v0, :cond_0

    .line 416
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    sput-object v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    .line 418
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 420
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->_emptyArray:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    return-object v0

    .line 418
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 452
    iput v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    .line 453
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasBackend:Z

    .line 454
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    .line 455
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasName:Z

    .line 456
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    .line 457
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasLandingUrl:Z

    .line 458
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->libraryName:Ljava/lang/String;

    .line 459
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasLibraryName:Z

    .line 460
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->shopName:Ljava/lang/String;

    .line 461
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasShopName:Z

    .line 462
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->recsWidgetUrl:Ljava/lang/String;

    .line 463
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasRecsWidgetUrl:Z

    .line 464
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->cachedSize:I

    .line 465
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 494
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 495
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasBackend:Z

    if-eqz v1, :cond_1

    .line 496
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 499
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasName:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 500
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 503
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasLandingUrl:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 504
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 507
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasLibraryName:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->libraryName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 508
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->libraryName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 511
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasRecsWidgetUrl:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->recsWidgetUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 512
    :cond_8
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->recsWidgetUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 515
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasShopName:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->shopName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 516
    :cond_a
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->shopName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 519
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 527
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 528
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 532
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 533
    :sswitch_0
    return-object p0

    .line 538
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 539
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 551
    :pswitch_1
    iput v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    .line 552
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasBackend:Z

    goto :goto_0

    .line 558
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    .line 559
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasName:Z

    goto :goto_0

    .line 563
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    .line 564
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasLandingUrl:Z

    goto :goto_0

    .line 568
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->libraryName:Ljava/lang/String;

    .line 569
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasLibraryName:Z

    goto :goto_0

    .line 573
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->recsWidgetUrl:Ljava/lang/String;

    .line 574
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasRecsWidgetUrl:Z

    goto :goto_0

    .line 578
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->shopName:Ljava/lang/String;

    .line 579
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasShopName:Z

    goto :goto_0

    .line 528
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch

    .line 539
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 406
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 471
    iget v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasBackend:Z

    if-eqz v0, :cond_1

    .line 472
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 474
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasName:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 475
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 477
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasLandingUrl:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 478
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 480
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasLibraryName:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->libraryName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 481
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->libraryName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 483
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasRecsWidgetUrl:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->recsWidgetUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 484
    :cond_8
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->recsWidgetUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 486
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->hasShopName:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->shopName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 487
    :cond_a
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->shopName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 489
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 490
    return-void
.end method
