.class final enum Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;
.super Ljava/lang/Enum;
.source "SettingsActivity.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/activities/SettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "PurchaseAuthEntry"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;",
        ">;",
        "Lcom/google/android/finsky/activities/SettingsListPreference$SettingsListEntry;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

.field public static final enum NEVER:Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

.field public static final enum PASSWORD_ALWAYS:Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

.field public static final enum PASSWORD_SESSION:Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;


# instance fields
.field private final mPurchaseAuth:I

.field private final mResource:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 75
    new-instance v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    const-string v1, "PASSWORD_ALWAYS"

    const v2, 0x7f0c02c8

    invoke-direct {v0, v1, v3, v5, v2}, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->PASSWORD_ALWAYS:Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    .line 76
    new-instance v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    const-string v1, "PASSWORD_SESSION"

    const v2, 0x7f0c02c9

    invoke-direct {v0, v1, v4, v4, v2}, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->PASSWORD_SESSION:Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    .line 77
    new-instance v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    const-string v1, "NEVER"

    const v2, 0x7f0c02ca

    invoke-direct {v0, v1, v5, v3, v2}, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->NEVER:Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    .line 74
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    sget-object v1, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->PASSWORD_ALWAYS:Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->PASSWORD_SESSION:Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->NEVER:Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->$VALUES:[Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "purchaseAuth"    # I
    .param p4, "resource"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 83
    iput p3, p0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->mPurchaseAuth:I

    .line 84
    iput p4, p0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->mResource:I

    .line 85
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->mPurchaseAuth:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 74
    const-class v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    return-object v0
.end method

.method public static values()[Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->$VALUES:[Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    invoke-virtual {v0}, [Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;

    return-object v0
.end method


# virtual methods
.method public getResource()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/finsky/activities/SettingsActivity$PurchaseAuthEntry;->mResource:I

    return v0
.end method
