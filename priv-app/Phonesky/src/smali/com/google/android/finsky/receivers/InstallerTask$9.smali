.class Lcom/google/android/finsky/receivers/InstallerTask$9;
.super Landroid/os/AsyncTask;
.source "InstallerTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/receivers/InstallerTask;->startInstallingGzippedApk(Lcom/google/android/finsky/appstate/AppStates$AppState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/receivers/InstallerTask;

.field final synthetic val$downloadUri:Landroid/net/Uri;

.field final synthetic val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerTask;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2680
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    iput-object p3, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->val$downloadUri:Landroid/net/Uri;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/io/File;
    .locals 18
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 2684
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/receivers/InstallerTask$9;->val$installerData:Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    invoke-virtual {v14}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPackageName()Ljava/lang/String;

    move-result-object v13

    .line 2685
    .local v13, "packageName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 2686
    .local v5, "gzippedStream":Ljava/io/InputStream;
    const/4 v11, 0x0

    .line 2687
    .local v11, "outputStream":Ljava/io/OutputStream;
    const/4 v10, 0x0

    .line 2688
    .local v10, "outputFile":Ljava/io/File;
    const/4 v7, 0x0

    .line 2693
    .local v7, "keepOutputFile":Z
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2697
    .local v1, "appContext":Landroid/content/Context;
    :try_start_1
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/receivers/InstallerTask$9;->val$downloadUri:Landroid/net/Uri;

    invoke-virtual {v14, v15}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 2704
    if-nez v5, :cond_1

    .line 2705
    const/4 v14, 0x0

    .line 2764
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_0

    if-eqz v10, :cond_0

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_0
    :goto_0
    return-object v14

    .line 2699
    :catch_0
    move-exception v3

    .line 2700
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v15, "source-FileNotFoundException"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportGzippedApkFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v14, v13, v15}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2400(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2701
    const-string v14, "FileNotFoundException %s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/receivers/InstallerTask$9;->val$downloadUri:Landroid/net/Uri;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2702
    const/4 v14, 0x0

    .line 2764
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_0

    if-eqz v10, :cond_0

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 2708
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :try_start_3
    new-instance v6, Ljava/util/zip/GZIPInputStream;

    new-instance v14, Ljava/io/BufferedInputStream;

    invoke-direct {v14, v5}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v14}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2720
    .end local v5    # "gzippedStream":Ljava/io/InputStream;
    .local v6, "gzippedStream":Ljava/io/InputStream;
    :try_start_4
    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    .line 2721
    .local v2, "cacheDir":Ljava/io/File;
    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v2, v14, v15}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v14

    if-nez v14, :cond_3

    .line 2723
    const-string v14, "Could not make executable %s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v2, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2724
    const/4 v14, 0x0

    .line 2764
    invoke-static {v6}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_2

    if-eqz v10, :cond_2

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_2
    move-object v5, v6

    .end local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v5    # "gzippedStream":Ljava/io/InputStream;
    goto :goto_0

    .line 2709
    .end local v2    # "cacheDir":Ljava/io/File;
    :catch_1
    move-exception v3

    .line 2710
    .local v3, "e":Ljava/io/IOException;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v15, "source-IOException"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportGzippedApkFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v14, v13, v15}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2400(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2711
    const-string v14, "IOException %s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2712
    const/4 v14, 0x0

    .line 2764
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_0

    if-eqz v10, :cond_0

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 2727
    .end local v3    # "e":Ljava/io/IOException;
    .end local v5    # "gzippedStream":Ljava/io/InputStream;
    .restart local v2    # "cacheDir":Ljava/io/File;
    .restart local v6    # "gzippedStream":Ljava/io/InputStream;
    :cond_3
    :try_start_6
    new-instance v9, Ljava/io/File;

    const-string v14, "gzipped"

    invoke-direct {v9, v2, v14}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 2728
    .local v9, "outputDirectory":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->mkdirs()Z

    .line 2730
    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v9, v14, v15}, Ljava/io/File;->setExecutable(ZZ)Z

    move-result v14

    if-eqz v14, :cond_5

    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v9, v14, v15}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v14

    if-eqz v14, :cond_5

    const/4 v8, 0x1

    .line 2732
    .local v8, "madeReadable":Z
    :goto_1
    if-nez v8, :cond_6

    .line 2733
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v15, "out-dir-readable"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportGzippedApkFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v14, v13, v15}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2400(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2734
    const-string v14, "Could not make readable %s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v9, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2735
    const/4 v14, 0x0

    .line 2764
    invoke-static {v6}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_4

    if-eqz v10, :cond_4

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_4
    move-object v5, v6

    .end local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v5    # "gzippedStream":Ljava/io/InputStream;
    goto/16 :goto_0

    .line 2730
    .end local v5    # "gzippedStream":Ljava/io/InputStream;
    .end local v8    # "madeReadable":Z
    .restart local v6    # "gzippedStream":Ljava/io/InputStream;
    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    .line 2737
    .restart local v8    # "madeReadable":Z
    :cond_6
    :try_start_7
    const-string v14, ".apk"

    invoke-static {v13, v14, v9}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v10

    .line 2739
    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-virtual {v10, v14, v15}, Ljava/io/File;->setReadable(ZZ)Z

    move-result v14

    if-nez v14, :cond_8

    .line 2740
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v15, "out-file-readable"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportGzippedApkFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v14, v13, v15}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2400(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2741
    const-string v14, "Could not make readable %s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v10, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2742
    invoke-virtual {v10}, Ljava/io/File;->delete()Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2743
    const/4 v14, 0x0

    .line 2764
    invoke-static {v6}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_7

    if-eqz v10, :cond_7

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_7
    move-object v5, v6

    .end local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v5    # "gzippedStream":Ljava/io/InputStream;
    goto/16 :goto_0

    .line 2745
    .end local v5    # "gzippedStream":Ljava/io/InputStream;
    .restart local v6    # "gzippedStream":Ljava/io/InputStream;
    :cond_8
    :try_start_8
    new-instance v12, Ljava/io/FileOutputStream;

    invoke-direct {v12, v10}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 2753
    .end local v11    # "outputStream":Ljava/io/OutputStream;
    .local v12, "outputStream":Ljava/io/OutputStream;
    :try_start_9
    invoke-static {v6, v12}, Lcom/google/android/finsky/utils/Utils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 2760
    const/4 v7, 0x1

    .line 2764
    invoke-static {v6}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v12}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_9

    if-eqz v10, :cond_9

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_9
    move-object v11, v12

    .end local v12    # "outputStream":Ljava/io/OutputStream;
    .restart local v11    # "outputStream":Ljava/io/OutputStream;
    move-object v5, v6

    .end local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v5    # "gzippedStream":Ljava/io/InputStream;
    move-object v14, v10

    goto/16 :goto_0

    .line 2746
    .end local v2    # "cacheDir":Ljava/io/File;
    .end local v5    # "gzippedStream":Ljava/io/InputStream;
    .end local v8    # "madeReadable":Z
    .end local v9    # "outputDirectory":Ljava/io/File;
    .restart local v6    # "gzippedStream":Ljava/io/InputStream;
    :catch_2
    move-exception v4

    .line 2747
    .local v4, "e1":Ljava/io/IOException;
    :try_start_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v15, "out-dir-IOException"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportGzippedApkFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v14, v13, v15}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2400(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2748
    const-string v14, "IOException while opening: %s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v4}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 2749
    const/4 v14, 0x0

    .line 2764
    invoke-static {v6}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_a

    if-eqz v10, :cond_a

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_a
    move-object v5, v6

    .end local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v5    # "gzippedStream":Ljava/io/InputStream;
    goto/16 :goto_0

    .line 2754
    .end local v4    # "e1":Ljava/io/IOException;
    .end local v5    # "gzippedStream":Ljava/io/InputStream;
    .end local v11    # "outputStream":Ljava/io/OutputStream;
    .restart local v2    # "cacheDir":Ljava/io/File;
    .restart local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v8    # "madeReadable":Z
    .restart local v9    # "outputDirectory":Ljava/io/File;
    .restart local v12    # "outputStream":Ljava/io/OutputStream;
    :catch_3
    move-exception v3

    .line 2755
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const-string v15, "ungzip-IOException"

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->reportGzippedApkFailure(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v14, v13, v15}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2400(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;Ljava/lang/String;)V

    .line 2756
    const-string v14, "IOException while copying: %s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-virtual {v3}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 2757
    const/4 v14, 0x0

    .line 2764
    invoke-static {v6}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v12}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_b

    if-eqz v10, :cond_b

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_b
    move-object v11, v12

    .end local v12    # "outputStream":Ljava/io/OutputStream;
    .restart local v11    # "outputStream":Ljava/io/OutputStream;
    move-object v5, v6

    .end local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v5    # "gzippedStream":Ljava/io/InputStream;
    goto/16 :goto_0

    .line 2764
    .end local v1    # "appContext":Landroid/content/Context;
    .end local v2    # "cacheDir":Ljava/io/File;
    .end local v3    # "e":Ljava/io/IOException;
    .end local v8    # "madeReadable":Z
    .end local v9    # "outputDirectory":Ljava/io/File;
    :catchall_0
    move-exception v14

    :goto_2
    invoke-static {v5}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2765
    invoke-static {v11}, Lcom/google/android/finsky/utils/Utils;->safeClose(Ljava/io/Closeable;)V

    .line 2766
    if-nez v7, :cond_c

    if-eqz v10, :cond_c

    .line 2767
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    :cond_c
    throw v14

    .line 2764
    .end local v5    # "gzippedStream":Ljava/io/InputStream;
    .restart local v1    # "appContext":Landroid/content/Context;
    .restart local v6    # "gzippedStream":Ljava/io/InputStream;
    :catchall_1
    move-exception v14

    move-object v5, v6

    .end local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v5    # "gzippedStream":Ljava/io/InputStream;
    goto :goto_2

    .end local v5    # "gzippedStream":Ljava/io/InputStream;
    .end local v11    # "outputStream":Ljava/io/OutputStream;
    .restart local v2    # "cacheDir":Ljava/io/File;
    .restart local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v8    # "madeReadable":Z
    .restart local v9    # "outputDirectory":Ljava/io/File;
    .restart local v12    # "outputStream":Ljava/io/OutputStream;
    :catchall_2
    move-exception v14

    move-object v11, v12

    .end local v12    # "outputStream":Ljava/io/OutputStream;
    .restart local v11    # "outputStream":Ljava/io/OutputStream;
    move-object v5, v6

    .end local v6    # "gzippedStream":Ljava/io/InputStream;
    .restart local v5    # "gzippedStream":Ljava/io/InputStream;
    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2680
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask$9;->doInBackground([Ljava/lang/Void;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/io/File;)V
    .locals 7
    .param p1, "ungzippedApk"    # Ljava/io/File;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2779
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1900(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->val$downloadUri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/download/DownloadQueue;->release(Landroid/net/Uri;)V

    .line 2782
    if-nez p1, :cond_0

    .line 2784
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->cleanExternalStorage()V
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2000(Lcom/google/android/finsky/receivers/InstallerTask;)V

    .line 2787
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const/16 v1, 0x200

    const/16 v2, 0x400

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->tryRestartWithInhibitFlag(II)Z
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$2100(Lcom/google/android/finsky/receivers/InstallerTask;II)Z

    .line 2804
    :goto_0
    return-void

    .line 2793
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x7b

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v2, v2, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-static {v5}, Lcom/google/android/finsky/receivers/InstallerTask;->access$500(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    move-object v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 2796
    const-string v0, "Successfully unpacked gzipped APK to update %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v2, v2, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2799
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const/16 v1, 0x3c

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1600(Lcom/google/android/finsky/receivers/InstallerTask;ILjava/lang/String;)V

    .line 2803
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$9;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1700(Lcom/google/android/finsky/receivers/InstallerTask;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2680
    check-cast p1, Ljava/io/File;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerTask$9;->onPostExecute(Ljava/io/File;)V

    return-void
.end method
