.class public Lcom/google/android/finsky/billing/PaymentMethodsFragment;
.super Lcom/google/android/finsky/billing/BillingProfileBaseFragment;
.source "PaymentMethodsFragment.java"


# static fields
.field private static final EDIT_FOOTER_URI:Landroid/net/Uri;


# instance fields
.field private mActionsContainer:Landroid/view/ViewGroup;

.field private mActionsView:Landroid/view/View;

.field private mInstrumentsContainer:Landroid/view/ViewGroup;

.field private mInstrumentsSwitcher:Landroid/view/View;

.field private mInstrumentsView:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;

.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/finsky/config/G;->editPaymentMethodUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->EDIT_FOOTER_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;-><init>()V

    .line 55
    const/16 v0, 0xa3c

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/PaymentMethodsFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/PaymentMethodsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->switchToInstrumentsView(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/PaymentMethodsFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/PaymentMethodsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->switchToActionsView(Z)V

    return-void
.end method

.method static synthetic access$200()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->EDIT_FOOTER_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/PaymentMethodsFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/PaymentMethodsFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mActionsView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/PaymentMethodsFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/PaymentMethodsFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsView:Landroid/view/View;

    return-object v0
.end method

.method private addEntry(Landroid/view/ViewGroup;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;ZLjava/lang/String;)V
    .locals 11
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "actionEntry"    # Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .param p3, "isDefault"    # Z
    .param p4, "errorMessage"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 236
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 237
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f04010b

    invoke-virtual {v3, v7, p1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/layout/ForegroundRelativeLayout;

    .line 241
    .local v5, "rowView":Lcom/google/android/play/layout/ForegroundRelativeLayout;
    const v7, 0x7f0a00f9

    invoke-virtual {v5, v7}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/image/FifeImageView;

    .line 242
    .local v2, "imageView":Lcom/google/android/play/image/FifeImageView;
    iget-object v1, p2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 243
    .local v1, "image":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v1, :cond_1

    .line 244
    iget-object v7, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v8, v1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v9

    invoke-virtual {v2, v7, v8, v9}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 250
    :goto_0
    const v7, 0x7f0a009c

    invoke-virtual {v5, v7}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 251
    .local v6, "titleView":Landroid/widget/TextView;
    iget-object v7, p2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->displayTitle:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    if-eqz p3, :cond_0

    .line 253
    const v7, 0x7f0a010b

    invoke-virtual {v5, v7}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    .line 256
    :cond_0
    const v7, 0x7f0a010a

    invoke-virtual {v5, v7}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 257
    .local v0, "bylineView":Landroid/widget/TextView;
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 258
    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    :goto_1
    iget-object v4, p2, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;->action:Landroid/view/View$OnClickListener;

    .line 264
    .local v4, "listener":Landroid/view/View$OnClickListener;
    if-eqz v4, :cond_3

    .line 265
    invoke-virtual {v5, v4}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    :goto_2
    invoke-virtual {p1, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 272
    return-void

    .line 247
    .end local v0    # "bylineView":Landroid/widget/TextView;
    .end local v4    # "listener":Landroid/view/View$OnClickListener;
    .end local v6    # "titleView":Landroid/widget/TextView;
    :cond_1
    const/4 v7, 0x4

    invoke-virtual {v2, v7}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_0

    .line 260
    .restart local v0    # "bylineView":Landroid/widget/TextView;
    .restart local v6    # "titleView":Landroid/widget/TextView;
    :cond_2
    const/16 v7, 0x8

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 268
    .restart local v4    # "listener":Landroid/view/View$OnClickListener;
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f020086

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->setForeground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public static newInstance(Landroid/accounts/Account;)Lcom/google/android/finsky/billing/PaymentMethodsFragment;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 66
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 67
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "BillingProfileFragment.account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 68
    new-instance v1, Lcom/google/android/finsky/billing/PaymentMethodsFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;-><init>()V

    .line 69
    .local v1, "result":Lcom/google/android/finsky/billing/PaymentMethodsFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 70
    return-object v1
.end method

.method private switchToActionsView(Z)V
    .locals 8
    .param p1, "animate"    # Z

    .prologue
    const-wide/16 v6, 0xfa

    .line 162
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v4, 0x0

    const/16 v3, 0xa3e

    invoke-virtual {v2, v4, v5, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 165
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mTitleView:Landroid/widget/TextView;

    const v3, 0x7f0c00d4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 166
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mActionsView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 167
    if-eqz p1, :cond_0

    .line 169
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v6, v7, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 171
    .local v0, "fadeInAnimation":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mActionsView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/billing/PaymentMethodsFragment$5;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment$5;-><init>(Lcom/google/android/finsky/billing/PaymentMethodsFragment;)V

    invoke-static {v2, v6, v7, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 179
    .local v1, "fadeOutAnimation":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 183
    .end local v0    # "fadeInAnimation":Landroid/view/animation/Animation;
    .end local v1    # "fadeOutAnimation":Landroid/view/animation/Animation;
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsView:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private switchToInstrumentsView(Z)V
    .locals 8
    .param p1, "animate"    # Z

    .prologue
    const-wide/16 v6, 0xfa

    .line 138
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v4, 0x0

    const/16 v3, 0xa3d

    invoke-virtual {v2, v4, v5, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 141
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mTitleView:Landroid/widget/TextView;

    const v3, 0x7f0c00d3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 142
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 143
    if-eqz p1, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v6, v7, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 147
    .local v0, "fadeInAnimation":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 148
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/billing/PaymentMethodsFragment$4;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment$4;-><init>(Lcom/google/android/finsky/billing/PaymentMethodsFragment;)V

    invoke-static {v2, v6, v7, v3}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 155
    .local v1, "fadeOutAnimation":Landroid/view/animation/Animation;
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mActionsView:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 159
    .end local v0    # "fadeInAnimation":Landroid/view/animation/Animation;
    .end local v1    # "fadeOutAnimation":Landroid/view/animation/Animation;
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mActionsView:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected createBillingProfileSidecar()V
    .locals 3

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "PaymentMethodsFragment.sidecar"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    iput-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mPurchaseContextToken:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;->newInstance(Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    .line 208
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    const-string v2, "PaymentMethodsFragment.sidecar"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 211
    :cond_0
    return-void
.end method

.method protected getBackgroundEventServerLogsCookie()[B
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getBillingProfileRequestEnum()I
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x4

    return v0
.end method

.method protected getCreditCardEventType()I
    .locals 1

    .prologue
    .line 361
    const/16 v0, 0x17c

    return v0
.end method

.method protected getDcbEventType()I
    .locals 1

    .prologue
    .line 366
    const/16 v0, 0x17d

    return v0
.end method

.method protected getGenericInstrumentEventType()I
    .locals 1

    .prologue
    .line 381
    const/16 v0, 0x180

    return v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 420
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected getRedeemEventType()I
    .locals 1

    .prologue
    .line 371
    const/16 v0, 0x17e

    return v0
.end method

.method protected getTopupEventType()I
    .locals 1

    .prologue
    .line 376
    const/16 v0, 0x17f

    return v0
.end method

.method protected logLoading()V
    .locals 4

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v1, 0xd5

    invoke-virtual {v0, v2, v3, v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 397
    return-void
.end method

.method protected logScreen()V
    .locals 4

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 402
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 76
    if-nez p1, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 79
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    const v3, 0x7f040108

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 86
    .local v2, "mainView":Landroid/view/ViewGroup;
    const v3, 0x7f0a0109

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mProgressIndicator:Landroid/view/View;

    .line 87
    const v3, 0x7f0a01cf

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mErrorIndicator:Landroid/view/View;

    .line 88
    const v3, 0x7f0a0104

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mProfileView:Landroid/view/View;

    .line 90
    const v3, 0x7f0a009c

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mTitleView:Landroid/widget/TextView;

    .line 91
    const v3, 0x7f0a02a7

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsContainer:Landroid/view/ViewGroup;

    .line 92
    const v3, 0x7f0a02a4

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mActionsContainer:Landroid/view/ViewGroup;

    .line 93
    const v3, 0x7f0a02a6

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsView:Landroid/view/View;

    .line 94
    const v3, 0x7f0a0108

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mActionsView:Landroid/view/View;

    .line 96
    const v3, 0x7f0a02a5

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsSwitcher:Landroid/view/View;

    .line 97
    iget-object v3, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsSwitcher:Landroid/view/View;

    new-instance v4, Lcom/google/android/finsky/billing/PaymentMethodsFragment$1;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment$1;-><init>(Lcom/google/android/finsky/billing/PaymentMethodsFragment;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    const v3, 0x7f0a02a8

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 108
    .local v0, "actionsSwitcher":Landroid/view/View;
    new-instance v3, Lcom/google/android/finsky/billing/PaymentMethodsFragment$2;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment$2;-><init>(Lcom/google/android/finsky/billing/PaymentMethodsFragment;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    const v3, 0x7f0a02a9

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 119
    .local v1, "editFooter":Landroid/view/View;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v3

    const-string v4, "cl:billing.hide_edit_payment_my_account"

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 121
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 134
    :goto_0
    return-object v2

    .line 123
    :cond_0
    new-instance v3, Lcom/google/android/finsky/billing/PaymentMethodsFragment$3;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment$3;-><init>(Lcom/google/android/finsky/billing/PaymentMethodsFragment;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onDetach()V
    .locals 2

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isRemoving()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mBillingProfileSidecar:Lcom/google/android/finsky/billing/lightpurchase/billingprofile/BillingProfileSidecar;

    .line 195
    :cond_1
    invoke-super {p0}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment;->onDetach()V

    .line 196
    return-void
.end method

.method protected onFatalError(Ljava/lang/String;)V
    .locals 4
    .param p1, "errorMessageHtml"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x8

    .line 342
    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mErrorIndicator:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 343
    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mProgressIndicator:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 344
    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mProfileView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 345
    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mErrorIndicator:Landroid/view/View;

    const v2, 0x7f0a00f4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 346
    .local v0, "errorMessageView":Landroid/widget/TextView;
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 347
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mErrorIndicator:Landroid/view/View;

    const v2, 0x7f0a01ce

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/finsky/billing/PaymentMethodsFragment$6;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment$6;-><init>(Lcom/google/android/finsky/billing/PaymentMethodsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 357
    return-void
.end method

.method protected onInstrumentCreated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "instrumentId"    # Ljava/lang/String;
    .param p2, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->requestBillingProfile()V

    .line 325
    return-void
.end method

.method protected onPromoCodeRedeemed(Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;)V
    .locals 0
    .param p1, "redeemCodeResult"    # Lcom/google/android/finsky/billing/giftcard/RedeemCodeResult;

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->requestBillingProfile()V

    .line 336
    return-void
.end method

.method public onRetry()V
    .locals 0

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->requestBillingProfile()V

    .line 320
    return-void
.end method

.method protected onStoredValueAdded(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "existingStoredValueInstrumentId"    # Ljava/lang/String;
    .param p2, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 330
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->requestBillingProfile()V

    .line 331
    return-void
.end method

.method protected redeemCheckoutCode()V
    .locals 3

    .prologue
    .line 406
    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mAccount:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->createIntent(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 408
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 409
    return-void
.end method

.method protected renderActions(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 276
    .local p1, "actionEntries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;>;"
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mActionsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 277
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    .line 278
    .local v0, "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    iget-object v2, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mActionsContainer:Landroid/view/ViewGroup;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, v2, v0, v3, v4}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->addEntry(Landroid/view/ViewGroup;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;ZLjava/lang/String;)V

    goto :goto_0

    .line 280
    .end local v0    # "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    :cond_0
    return-void
.end method

.method protected renderInstruments([Lcom/google/android/finsky/protos/CommonDevice$Instrument;)V
    .locals 14
    .param p1, "instruments"    # [Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .prologue
    .line 284
    iget-object v11, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v11}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 285
    array-length v11, p1

    if-nez v11, :cond_1

    .line 286
    iget-object v11, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsSwitcher:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 287
    const/4 v11, 0x0

    invoke-direct {p0, v11}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->switchToActionsView(Z)V

    .line 313
    :cond_0
    return-void

    .line 291
    :cond_1
    iget-object v11, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsSwitcher:Landroid/view/View;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    .line 292
    const/4 v11, 0x0

    invoke-direct {p0, v11}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->switchToInstrumentsView(Z)V

    .line 293
    iget-object v11, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mProfile:Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;

    iget-object v3, v11, Lcom/google/android/finsky/protos/BillingProfileProtos$BillingProfile;->selectedExternalInstrumentId:Ljava/lang/String;

    .line 294
    .local v3, "defaultInstrumentId":Ljava/lang/String;
    move-object v1, p1

    .local v1, "arr$":[Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    array-length v10, v1

    .local v10, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v10, :cond_0

    aget-object v6, v1, v5

    .line 295
    .local v6, "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    iget-object v11, v6, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    array-length v11, v11

    if-lez v11, :cond_2

    iget-object v11, v6, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    const/4 v12, 0x0

    aget-object v11, v11, v12

    iget-object v4, v11, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledMessageHtml:Ljava/lang/String;

    .line 298
    .local v4, "error":Ljava/lang/String;
    :goto_1
    iget-object v7, v6, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->externalInstrumentId:Ljava/lang/String;

    .line 299
    .local v7, "instrumentId":Ljava/lang/String;
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 300
    .local v9, "isDefault":Z
    new-instance v8, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    invoke-direct {v8}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;-><init>()V

    .line 301
    .local v8, "instrumentInfo":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
    iget v11, v6, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    iput v11, v8, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->instrumentFamily:I

    .line 302
    const/4 v11, 0x1

    iput-boolean v11, v8, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasInstrumentFamily:Z

    .line 303
    iput-boolean v9, v8, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->isDefault:Z

    .line 304
    const/4 v11, 0x1

    iput-boolean v11, v8, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;->hasIsDefault:Z

    .line 306
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 307
    .local v2, "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    iput-object v8, v2, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->instrumentInfo:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;

    .line 309
    new-instance v0, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;

    iget-object v11, v6, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle:Ljava/lang/String;

    iget-object v12, v6, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->iconImage:Lcom/google/android/finsky/protos/Common$Image;

    const/4 v13, 0x0

    invoke-direct {v0, v11, v12, v13}, Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;-><init>(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V

    .line 311
    .local v0, "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    iget-object v11, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mInstrumentsContainer:Landroid/view/ViewGroup;

    invoke-direct {p0, v11, v0, v9, v4}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->addEntry(Landroid/view/ViewGroup;Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;ZLjava/lang/String;)V

    .line 294
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 295
    .end local v0    # "actionEntry":Lcom/google/android/finsky/billing/BillingProfileBaseFragment$ActionEntry;
    .end local v2    # "clientCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    .end local v4    # "error":Ljava/lang/String;
    .end local v7    # "instrumentId":Ljava/lang/String;
    .end local v8    # "instrumentInfo":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo$InstrumentInfo;
    .end local v9    # "isDefault":Z
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 216
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "requestCode"    # I

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 221
    return-void
.end method
