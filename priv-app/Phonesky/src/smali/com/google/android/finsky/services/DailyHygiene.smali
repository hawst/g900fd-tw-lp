.class public Lcom/google/android/finsky/services/DailyHygiene;
.super Landroid/app/Service;
.source "DailyHygiene.java"


# static fields
.field public static final BOOT_DELAY_MS:J

.field private static final HOLDOFF_INTERVAL_MS:J

.field public static final IMMEDIATE_DELAY_MS:J

.field private static final INITIAL_SCHEDULE_JITTER_FACTOR:F

.field private static final REGULAR_INTERVAL_MS:J

.field private static final RESCHEDULE_JITTER_FACTOR:F

.field private static final RETRY_INTERVALS:[I

.field private static final RETRY_INTERVAL_MS:J

.field private static final UPDATE_CHECK:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneImmediateDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->IMMEDIATE_DELAY_MS:J

    .line 66
    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneBootDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->BOOT_DELAY_MS:J

    .line 68
    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneRegularIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    .line 70
    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneHoldoffIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->HOLDOFF_INTERVAL_MS:J

    .line 72
    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneRetryIntervalMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVAL_MS:J

    .line 75
    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneInitialJitterFactor:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/google/android/finsky/services/DailyHygiene;->INITIAL_SCHEDULE_JITTER_FACTOR:F

    .line 79
    sget-object v0, Lcom/google/android/finsky/config/G;->dailyHygieneJitterFactor:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sput v0, Lcom/google/android/finsky/services/DailyHygiene;->RESCHEDULE_JITTER_FACTOR:F

    .line 82
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVALS:[I

    .line 84
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/services/DailyHygiene;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sput-object v0, Lcom/google/android/finsky/services/DailyHygiene;->UPDATE_CHECK:Landroid/content/Intent;

    return-void

    .line 82
    :array_0
    .array-data 4
        0x1
        0x3
        0x9
        0x1b
        0x51
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(I)Z
    .locals 1
    .param p0, "x0"    # I

    .prologue
    .line 61
    invoke-static {p0}, Lcom/google/android/finsky/services/DailyHygiene;->emergencyDailyHygiene(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/services/DailyHygiene;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/DailyHygiene;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/DailyHygiene;->reschedule(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/services/DailyHygiene;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/DailyHygiene;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/finsky/services/DailyHygiene;->loadAndReplicateAndContinue()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/services/DailyHygiene;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/DailyHygiene;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/finsky/services/DailyHygiene;->contentSyncAndAutoUpdateAndContinue()V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/services/DailyHygiene;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/DailyHygiene;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/DailyHygiene;->logDeviceFeaturesAndContinue(Z)V

    return-void
.end method

.method private beginSelfUpdateCheck()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 138
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v6

    .line 139
    .local v6, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    if-nez v6, :cond_0

    .line 141
    invoke-direct {p0}, Lcom/google/android/finsky/services/DailyHygiene;->loadAndReplicateAndContinue()V

    .line 189
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getVersionCode()I

    move-result v5

    .line 146
    .local v5, "installedVersion":I
    new-instance v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v2}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    .line 147
    .local v2, "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    const/4 v0, 0x0

    iput v0, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 148
    iput-boolean v1, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 149
    iput-boolean v1, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->systemApp:Z

    .line 150
    iput-boolean v1, v2, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasSystemApp:Z

    .line 151
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    .line 152
    .local v3, "eventLogger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-interface {v6}, Lcom/google/android/finsky/api/DfeApi;->getAccountName()Ljava/lang/String;

    move-result-object v4

    .line 154
    .local v4, "accountName":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/services/DailyHygiene$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/services/DailyHygiene$1;-><init>(Lcom/google/android/finsky/services/DailyHygiene;Lcom/google/android/finsky/analytics/PlayStore$AppData;Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;I)V

    invoke-static {v6, v0}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->getSelfUpdate(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    goto :goto_0
.end method

.method public static cancelHoldoffPeriod()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 469
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneHoldoffComplete:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 472
    .local v0, "dailyHygieneHoldoffCompletePreference":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Boolean;>;"
    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 478
    :goto_0
    return-void

    .line 476
    :cond_0
    const-string v1, "Canceling holdoff. Provisioned=%b"

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/finsky/services/DailyHygiene;->isProvisioned()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 477
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private contentSyncAndAutoUpdateAndContinue()V
    .locals 8

    .prologue
    .line 234
    invoke-static {}, Lcom/google/android/finsky/services/ContentSyncService;->get()Lcom/google/android/finsky/services/ContentSyncService$Facade;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/finsky/services/ContentSyncService$Facade;->scheduleSync()V

    .line 235
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    .line 238
    .local v2, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getSelfUpdateScheduler()Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    move-result-object v7

    .line 239
    .local v7, "selfUpdateScheduler":Lcom/google/android/finsky/utils/SelfUpdateScheduler;
    invoke-virtual {v7}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->isSelfUpdateRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/services/DailyHygiene;->logDeviceFeaturesAndContinue(Z)V

    .line 265
    :goto_0
    return-void

    .line 244
    :cond_0
    new-instance v0, Lcom/google/android/finsky/appstate/UpdateChecker;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v4

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/appstate/UpdateChecker;-><init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/utils/Notifier;)V

    .line 251
    .local v0, "updateChecker":Lcom/google/android/finsky/appstate/UpdateChecker;
    invoke-virtual {v2}, Lcom/google/android/finsky/library/Libraries;->cleanup()V

    .line 253
    new-instance v1, Lcom/google/android/finsky/services/DailyHygiene$3;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/services/DailyHygiene$3;-><init>(Lcom/google/android/finsky/services/DailyHygiene;)V

    new-instance v3, Lcom/google/android/finsky/services/DailyHygiene$4;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/services/DailyHygiene$4;-><init>(Lcom/google/android/finsky/services/DailyHygiene;)V

    const-string v4, "daily_hygiene"

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/google/android/finsky/appstate/UpdateChecker;->checkForUpdates(Ljava/lang/Runnable;Ljava/lang/Runnable;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private static emergencyDailyHygiene(I)Z
    .locals 7
    .param p0, "marketVersionCode"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 392
    sget-object v2, Lcom/google/android/finsky/config/G;->dailyHygieneImmediateRunVersionCodeLow:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 393
    .local v1, "emergencyRangeLow":I
    sget-object v2, Lcom/google/android/finsky/config/G;->dailyHygieneImmediateRunVersionCodeHigh:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 394
    .local v0, "emergencyRangeHigh":I
    if-lez v1, :cond_0

    if-gtz v0, :cond_1

    :cond_0
    move v2, v4

    .line 402
    :goto_0
    return v2

    .line 397
    :cond_1
    if-lt p0, v1, :cond_2

    if-gt p0, v0, :cond_2

    .line 398
    const-string v2, "Scheduling emergency daily hygiene, %d <= %d <= %d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v3

    const/4 v4, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v4

    invoke-static {v2, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    move v2, v3

    .line 400
    goto :goto_0

    :cond_2
    move v2, v4

    .line 402
    goto :goto_0
.end method

.method private flushEventLogsAndContinue(Z)V
    .locals 9
    .param p1, "previousSuccess"    # Z

    .prologue
    .line 299
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    .line 300
    .local v1, "accounts":[Landroid/accounts/Account;
    move-object v2, v1

    .local v2, "arr$":[Landroid/accounts/Account;
    array-length v4, v2

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v2, v3

    .line 301
    .local v0, "account":Landroid/accounts/Account;
    const-string v5, "Flushing event logs for %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v8}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 300
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 305
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/DailyHygiene;->verifyInstalledPackagesAndContinue(Z)V

    .line 306
    return-void
.end method

.method public static goMakeHygieneIfDirty(Landroid/content/Context;I)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "marketVersion"    # I

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-static {}, Lcom/google/android/finsky/services/DailyHygiene;->needsDailyHygiene()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/finsky/services/DailyHygiene;->emergencyDailyHygiene(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/finsky/services/DailyHygiene;->hasDatabaseVersionChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    :cond_0
    const-string v0, "Dirty, need more hygiene."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    sget-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->IMMEDIATE_DELAY_MS:J

    invoke-static {p0, v0, v1}, Lcom/google/android/finsky/services/DailyHygiene;->schedule(Landroid/content/Context;J)V

    .line 99
    :goto_0
    return-void

    .line 97
    :cond_1
    const-string v0, "No need to run daily hygiene."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static hasDatabaseVersionChanged()Z
    .locals 3

    .prologue
    .line 498
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->lastReplicatedDatabaseVersion:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 499
    .local v1, "prevDatabaseVersion":I
    invoke-static {}, Lcom/google/android/finsky/library/SQLiteLibrary;->getVersion()I

    move-result v0

    .line 500
    .local v0, "currentDatabaseVersion":I
    if-eq v1, v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private inHoldoffPeriod()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 429
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneHoldoffComplete:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    .line 434
    .local v0, "dailyHygieneHoldoffCompletePreference":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/Boolean;>;"
    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v4

    .line 456
    :goto_0
    return v1

    .line 440
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/services/DailyHygiene;->isProvisioned()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 441
    const-string v1, "No holdoff required - already provisioned"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v1, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 442
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    move v1, v4

    .line 443
    goto :goto_0

    .line 448
    :cond_1
    sget-object v1, Lcom/google/android/finsky/config/G;->dailyHygieneProvisionHoldoffMaxMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 449
    .local v2, "holdoffMaxMs":J
    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-gtz v1, :cond_2

    .line 450
    const-string v1, "No holdoff required - disabled"

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v1, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 451
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    move v1, v4

    .line 452
    goto :goto_0

    .line 455
    :cond_2
    const-string v1, "DailyHygiene holdoff continue"

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v5

    .line 456
    goto :goto_0
.end method

.method private static isProvisioned()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 483
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 485
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_1

    .line 486
    const-string v3, "device_provisioned"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 491
    .local v1, "value":I
    :goto_0
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2

    .line 489
    .end local v1    # "value":I
    :cond_1
    const-string v3, "device_provisioned"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .restart local v1    # "value":I
    goto :goto_0
.end method

.method private static jitter(JF)J
    .locals 2
    .param p0, "baseValue"    # J
    .param p2, "jitterFactor"    # F

    .prologue
    .line 518
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/finsky/services/DailyHygiene;->jitterLogic(JFF)J

    move-result-wide v0

    return-wide v0
.end method

.method protected static jitterLogic(JFF)J
    .locals 4
    .param p0, "baseValue"    # J
    .param p2, "jitterFactor"    # F
    .param p3, "floatMathRandom"    # F

    .prologue
    .line 526
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x40000000    # 2.0f

    const/high16 v3, 0x3f000000    # 0.5f

    sub-float v3, p3, v3

    mul-float/2addr v2, v3

    mul-float/2addr v2, p2

    add-float v0, v1, v2

    .line 527
    .local v0, "factor":F
    long-to-float v1, p0

    mul-float/2addr v1, v0

    float-to-long v2, v1

    return-wide v2
.end method

.method private loadAndReplicateAndContinue()V
    .locals 3

    .prologue
    .line 202
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getVersionCode()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/services/DailyHygiene;->emergencyDailyHygiene(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 205
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/services/DailyHygiene;->reschedule(Z)V

    .line 224
    :goto_0
    return-void

    .line 209
    :cond_0
    new-instance v0, Lcom/google/android/finsky/services/DailyHygiene$2;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/services/DailyHygiene$2;-><init>(Lcom/google/android/finsky/services/DailyHygiene;)V

    .line 219
    .local v0, "continueRunnable":Ljava/lang/Runnable;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/Libraries;->load(Ljava/lang/Runnable;)V

    .line 220
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v1

    const-string v2, "daily-hygiene"

    invoke-interface {v1, v0, v2}, Lcom/google/android/finsky/library/LibraryReplicators;->replicateAllAccounts(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 222
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    .line 223
    invoke-static {v0}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->initialize(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private logDeviceFeaturesAndContinue(Z)V
    .locals 3
    .param p1, "previousSuccess"    # Z

    .prologue
    .line 277
    new-instance v0, Lcom/google/android/finsky/utils/GmsDeviceFeaturesHelper;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/finsky/utils/GmsDeviceFeaturesHelper;-><init>(Landroid/content/Context;)V

    .line 279
    .local v0, "gmsDeviceFeaturesHelper":Lcom/google/android/finsky/utils/GmsDeviceFeaturesHelper;
    const-string v1, "Logging device features"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 280
    invoke-virtual {v0}, Lcom/google/android/finsky/utils/GmsDeviceFeaturesHelper;->fetchAndLogDeviceFeatures()V

    .line 281
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/DailyHygiene;->flushEventLogsAndContinue(Z)V

    .line 282
    return-void
.end method

.method private static needsDailyHygiene()Z
    .locals 6

    .prologue
    .line 377
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-wide v4, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private reschedule(Z)V
    .locals 10
    .param p1, "previousSuccess"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 342
    if-eqz p1, :cond_1

    .line 343
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_0

    .line 347
    sget-wide v4, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    sget v1, Lcom/google/android/finsky/services/DailyHygiene;->INITIAL_SCHEDULE_JITTER_FACTOR:F

    invoke-static {v4, v5, v1}, Lcom/google/android/finsky/services/DailyHygiene;->jitter(JF)J

    move-result-wide v2

    .line 348
    .local v2, "interval":J
    const-string v1, "Scheduling first run in %1.1f hours"

    new-array v4, v9, [Ljava/lang/Object;

    long-to-float v5, v2

    const v6, 0x4a5bba00    # 3600000.0f

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v1, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 353
    :goto_0
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 354
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->lastReplicatedDatabaseVersion:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {}, Lcom/google/android/finsky/library/SQLiteLibrary;->getVersion()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 369
    :goto_1
    invoke-static {p0, v2, v3}, Lcom/google/android/finsky/services/DailyHygiene;->schedule(Landroid/content/Context;J)V

    .line 370
    invoke-virtual {p0}, Lcom/google/android/finsky/services/DailyHygiene;->stopSelf()V

    .line 371
    return-void

    .line 350
    .end local v2    # "interval":J
    :cond_0
    sget-wide v4, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    sget v1, Lcom/google/android/finsky/services/DailyHygiene;->RESCHEDULE_JITTER_FACTOR:F

    invoke-static {v4, v5, v1}, Lcom/google/android/finsky/services/DailyHygiene;->jitter(JF)J

    move-result-wide v2

    .restart local v2    # "interval":J
    goto :goto_0

    .line 356
    .end local v2    # "interval":J
    :cond_1
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneFailedCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 357
    .local v0, "failures":I
    sget-object v1, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVALS:[I

    array-length v1, v1

    if-gt v0, v1, :cond_2

    .line 358
    sget-object v1, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVALS:[I

    add-int/lit8 v4, v0, -0x1

    aget v1, v1, v4

    int-to-long v4, v1

    sget-wide v6, Lcom/google/android/finsky/services/DailyHygiene;->RETRY_INTERVAL_MS:J

    mul-long/2addr v4, v6

    sget v1, Lcom/google/android/finsky/services/DailyHygiene;->RESCHEDULE_JITTER_FACTOR:F

    invoke-static {v4, v5, v1}, Lcom/google/android/finsky/services/DailyHygiene;->jitter(JF)J

    move-result-wide v2

    .line 360
    .restart local v2    # "interval":J
    const-string v1, "Scheduling new run in %d minutes (failures=%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const-wide/32 v6, 0xea60

    div-long v6, v2, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-static {v1, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneFailedCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_1

    .line 364
    .end local v2    # "interval":J
    :cond_2
    const-string v1, "Giving up. (failures=%d)"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {v1, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 365
    sget-object v1, Lcom/google/android/finsky/utils/FinskyPreferences;->dailyHygieneFailedCount:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 366
    sget-wide v4, Lcom/google/android/finsky/services/DailyHygiene;->REGULAR_INTERVAL_MS:J

    sget v1, Lcom/google/android/finsky/services/DailyHygiene;->RESCHEDULE_JITTER_FACTOR:F

    invoke-static {v4, v5, v1}, Lcom/google/android/finsky/services/DailyHygiene;->jitter(JF)J

    move-result-wide v2

    .restart local v2    # "interval":J
    goto :goto_1
.end method

.method public static schedule(Landroid/content/Context;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "delayMs"    # J

    .prologue
    const/4 v4, 0x0

    .line 108
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 109
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p1

    sget-object v1, Lcom/google/android/finsky/services/DailyHygiene;->UPDATE_CHECK:Landroid/content/Intent;

    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 111
    return-void
.end method

.method private verifyInstalledPackagesAndContinue(Z)V
    .locals 8
    .param p1, "previousSuccess"    # Z

    .prologue
    .line 320
    sget-object v4, Lcom/google/android/finsky/config/G;->verifyInstalledPackagesEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 321
    sget-object v4, Lcom/google/android/finsky/utils/FinskyPreferences;->verifyInstalledPackagesLastSuccessMs:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 322
    .local v0, "lastSuccessMs":J
    sget-object v4, Lcom/google/android/finsky/config/G;->verifyInstalledPackagesMinimumWaitMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 324
    .local v2, "minimumWaitMs":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long v6, v0, v2

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    .line 325
    invoke-static {p0}, Lcom/google/android/vending/verifier/VerifyInstalledPackagesReceiver;->verifyInstalledPackages(Landroid/content/Context;)V

    .line 329
    .end local v0    # "lastSuccessMs":J
    .end local v2    # "minimumWaitMs":J
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/DailyHygiene;->reschedule(Z)V

    .line 330
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 115
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v2, 0x2

    .line 120
    invoke-direct {p0}, Lcom/google/android/finsky/services/DailyHygiene;->inHoldoffPeriod()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    sget-wide v0, Lcom/google/android/finsky/services/DailyHygiene;->HOLDOFF_INTERVAL_MS:J

    invoke-static {p0, v0, v1}, Lcom/google/android/finsky/services/DailyHygiene;->schedule(Landroid/content/Context;J)V

    .line 127
    :goto_0
    return v2

    .line 125
    :cond_0
    const-string v0, "Beginning daily hygiene"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    invoke-direct {p0}, Lcom/google/android/finsky/services/DailyHygiene;->beginSelfUpdateCheck()V

    goto :goto_0
.end method
