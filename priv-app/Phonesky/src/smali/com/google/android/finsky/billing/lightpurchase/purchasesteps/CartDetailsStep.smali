.class public Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "CartDetailsStep.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;",
        ">;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;"
    }
.end annotation


# instance fields
.field private mBackend:I

.field private mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

.field private mContinueButtonConfirmsPurchase:Z

.field private mContinueButtonShowsInstrumentManager:Z

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mExpanded:Z

.field private mHeader:Landroid/view/View;

.field private mPaymentSettingsView:Landroid/widget/TextView;

.field private mPriceView:Landroid/widget/TextView;

.field private mPurchaseDetailsView:Landroid/view/View;

.field private mRedeemView:Landroid/widget/TextView;

.field private mSelectedVoucherView:Landroid/view/View;

.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 76
    const/16 v0, 0x2c6

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;)Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;)Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    return-object v0
.end method

.method public static newInstance(ILcom/google/android/finsky/protos/Purchase$ClientCart;Z)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;
    .locals 4
    .param p0, "backend"    # I
    .param p1, "cart"    # Lcom/google/android/finsky/protos/Purchase$ClientCart;
    .param p2, "continueToInstrumentManager"    # Z

    .prologue
    .line 81
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 82
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;-><init>()V

    .line 83
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;
    const-string v2, "CartDetailsStep.backend"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    const-string v2, "CartDetailsStep.cart"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 85
    const-string v2, "CartDetailsStep.continueToInstrumentManager"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 86
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->setArguments(Landroid/os/Bundle;)V

    .line 88
    iput-object p1, v1, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .line 89
    return-object v1
.end method

.method private populateContainerWithTextViews(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I[Ljava/lang/String;I)V
    .locals 7
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "textViewLayoutId"    # I
    .param p4, "messagesHtml"    # [Ljava/lang/String;
    .param p5, "highlightColor"    # I

    .prologue
    const/4 v5, 0x0

    .line 319
    move-object v0, p4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 320
    .local v4, "messageHtml":Ljava/lang/String;
    invoke-virtual {p1, p3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 324
    .local v1, "detailHtmlView":Landroid/widget/TextView;
    invoke-static {v4, p5}, Lcom/google/android/finsky/billing/BillingUtils;->parseHtmlAndColorizeEm(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 327
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 328
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 319
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 330
    .end local v1    # "detailHtmlView":Landroid/widget/TextView;
    .end local v4    # "messageHtml":Ljava/lang/String;
    :cond_0
    array-length v6, p4

    if-lez v6, :cond_1

    :goto_1
    invoke-virtual {p2, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 331
    return-void

    .line 330
    :cond_1
    const/16 v5, 0x8

    goto :goto_1
.end method

.method private populateVoucherViews(Landroid/view/LayoutInflater;Landroid/view/View;I)V
    .locals 17
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "mainView"    # Landroid/view/View;
    .param p3, "highlightColor"    # I

    .prologue
    .line 228
    const v13, 0x7f0a0241

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 229
    .local v11, "voucherContainer":Landroid/view/ViewGroup;
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 230
    const/4 v9, 0x0

    .line 231
    .local v9, "selectedVoucher":Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v2, v13, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    .local v2, "arr$":[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v1, v2, v4

    .line 232
    .local v1, "applicableVoucher":Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    const v13, 0x7f0400c3

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v11, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 234
    .local v8, "rowView":Landroid/view/View;
    const v13, 0x7f0a009c

    invoke-virtual {v8, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 235
    .local v10, "title":Landroid/widget/TextView;
    iget-object v13, v1, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v13, v13, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 236
    iget-boolean v13, v1, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->applied:Z

    if-eqz v13, :cond_1

    .line 237
    if-eqz v9, :cond_0

    .line 238
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "Multiple applied vouchers is not supported"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 241
    :cond_0
    move-object v9, v1

    .line 242
    invoke-virtual {v10}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v10, v13, v14}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 243
    const v13, 0x7f0a010b

    invoke-virtual {v8, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 244
    new-instance v13, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;)V

    invoke-virtual {v8, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 264
    :goto_1
    invoke-virtual {v11, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 231
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 254
    :cond_1
    iget-object v13, v1, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v6, v13, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->docid:Ljava/lang/String;

    .line 255
    .local v6, "newVoucherId":Ljava/lang/String;
    new-instance v13, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v6}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep$2;-><init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;Ljava/lang/String;)V

    invoke-virtual {v8, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 268
    .end local v1    # "applicableVoucher":Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    .end local v6    # "newVoucherId":Ljava/lang/String;
    .end local v8    # "rowView":Landroid/view/View;
    .end local v10    # "title":Landroid/widget/TextView;
    :cond_2
    const v13, 0x7f0a0240

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 269
    .local v12, "voucherHeader":Landroid/widget/TextView;
    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 270
    move/from16 v0, p3

    invoke-virtual {v12, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 271
    if-nez v9, :cond_4

    const v13, 0x7f0c0109

    :goto_2
    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 274
    const v13, 0x7f0a0242

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 277
    if-eqz v9, :cond_3

    .line 278
    const v13, 0x7f0a023b

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mSelectedVoucherView:Landroid/view/View;

    .line 279
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mSelectedVoucherView:Landroid/view/View;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 280
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mSelectedVoucherView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mSelectedVoucherView:Landroid/view/View;

    const v14, 0x7f0a0378

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 284
    .restart local v10    # "title":Landroid/widget/TextView;
    iget-object v13, v9, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    iget-object v13, v13, Lcom/google/android/finsky/protos/DocumentV2$DocV2;->title:Ljava/lang/String;

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 285
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mSelectedVoucherView:Landroid/view/View;

    const v14, 0x7f0a0379

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 287
    .local v3, "discountPrice":Landroid/widget/TextView;
    iget-object v13, v9, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->formattedDiscountAmount:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 288
    const/16 v13, 0x8

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 293
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mSelectedVoucherView:Landroid/view/View;

    const v14, 0x7f0a037a

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 295
    .local v7, "originalPrice":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v13, v13, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedOriginalPrice:Ljava/lang/String;

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 296
    const/16 v13, 0x8

    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 302
    .end local v3    # "discountPrice":Landroid/widget/TextView;
    .end local v7    # "originalPrice":Landroid/widget/TextView;
    .end local v10    # "title":Landroid/widget/TextView;
    :cond_3
    :goto_4
    return-void

    .line 271
    :cond_4
    const v13, 0x7f0c010a

    goto :goto_2

    .line 290
    .restart local v3    # "discountPrice":Landroid/widget/TextView;
    .restart local v10    # "title":Landroid/widget/TextView;
    :cond_5
    const v13, 0x7f0c0105

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    iget-object v0, v9, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->formattedDiscountAmount:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 298
    .restart local v7    # "originalPrice":Landroid/widget/TextView;
    :cond_6
    const v13, 0x7f0c0106

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedOriginalPrice:Ljava/lang/String;

    move-object/from16 v16, v0

    aput-object v16, v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method private toggleExpansion()V
    .locals 4

    .prologue
    .line 352
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mExpanded:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mExpanded:Z

    .line 353
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mExpanded:Z

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v1, 0x2cb

    invoke-virtual {v0, v2, v3, v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 356
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->updateExpansion()V

    .line 357
    return-void

    .line 352
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateExpansion()V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 334
    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPurchaseDetailsView:Landroid/view/View;

    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mExpanded:Z

    if-eqz v3, :cond_1

    move v3, v4

    :goto_0
    invoke-virtual {v6, v3}, Landroid/view/View;->setVisibility(I)V

    .line 335
    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mExpanded:Z

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mBackend:I

    invoke-static {v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getMenuExpanderMaximized(I)I

    move-result v1

    .line 338
    .local v1, "expanderDrawableRes":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v3, v4, v4, v1, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 339
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mSelectedVoucherView:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 340
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mSelectedVoucherView:Landroid/view/View;

    iget-boolean v6, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mExpanded:Z

    if-eqz v6, :cond_3

    :goto_2
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 344
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 345
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mExpanded:Z

    if-eqz v3, :cond_4

    const v0, 0x7f0c02e5

    .line 347
    .local v0, "expanderDescription":I
    :goto_3
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 349
    return-void

    .end local v0    # "expanderDescription":I
    .end local v1    # "expanderDrawableRes":I
    .end local v2    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    move v3, v5

    .line 334
    goto :goto_0

    .line 335
    :cond_2
    iget v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mBackend:I

    invoke-static {v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getMenuExpanderMinimized(I)I

    move-result v1

    goto :goto_1

    .restart local v1    # "expanderDrawableRes":I
    :cond_3
    move v5, v4

    .line 340
    goto :goto_2

    .line 345
    .restart local v2    # "sb":Ljava/lang/StringBuilder;
    :cond_4
    const v0, 0x7f0c02e4

    goto :goto_3
.end method


# virtual methods
.method public allowContinueButtonIcon()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mContinueButtonConfirmsPurchase:Z

    return v0
.end method

.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Purchase$ClientCart;->buttonText:Ljava/lang/String;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mHeader:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 362
    const/16 v0, 0x2c9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->logClick(I)V

    .line 363
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->toggleExpansion()V

    .line 376
    :cond_0
    :goto_0
    return-void

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    .line 365
    const/16 v0, 0x2ca

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->logClick(I)V

    .line 366
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->launchBillingProfile()V

    goto :goto_0

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mRedeemView:Landroid/widget/TextView;

    if-ne p1, v0, :cond_3

    .line 368
    const/16 v0, 0x2cc

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->logClick(I)V

    .line 369
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->launchRedeem()V

    goto :goto_0

    .line 370
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mSelectedVoucherView:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 373
    const/16 v0, 0x2cd

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->logClick(I)V

    .line 374
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->toggleExpansion()V

    goto :goto_0
.end method

.method public onContinueButtonClicked()V
    .locals 2

    .prologue
    const/16 v1, 0x2c8

    .line 381
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mContinueButtonShowsInstrumentManager:Z

    if-eqz v0, :cond_0

    .line 382
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->logClick(I)V

    .line 383
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->switchToInstrumentManager()V

    .line 391
    :goto_0
    return-void

    .line 384
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mContinueButtonConfirmsPurchase:Z

    if-eqz v0, :cond_1

    .line 385
    const/16 v0, 0x2c7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->logClick(I)V

    .line 386
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->confirmPurchase()V

    goto :goto_0

    .line 388
    :cond_1
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->logClick(I)V

    .line 389
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->launchBillingProfile()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 105
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 106
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 107
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "CartDetailsStep.backend"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mBackend:I

    .line 108
    const-string v1, "CartDetailsStep.cart"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    .line 109
    const-string v1, "CartDetailsStep.continueToInstrumentManager"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mContinueButtonShowsInstrumentManager:Z

    .line 110
    if-eqz p1, :cond_0

    .line 111
    const-string v1, "CartDetailsStep.expanded"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mExpanded:Z

    .line 113
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 114
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 29
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 125
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mBackend:I

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v16

    .line 127
    .local v16, "corpusColor":Landroid/content/res/ColorStateList;
    invoke-virtual/range {v16 .. v16}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v8

    .line 129
    .local v8, "highlightColor":I
    const v3, 0x7f0400c0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v25

    .line 130
    .local v25, "mainView":Landroid/view/View;
    const v3, 0x7f0a0235

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v28

    check-cast v28, Landroid/widget/TextView;

    .line 131
    .local v28, "titleView":Landroid/widget/TextView;
    const v3, 0x7f0a0237

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    .line 132
    const v3, 0x7f0a0236

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/TextView;

    .line 133
    .local v23, "instrumentView":Landroid/widget/TextView;
    const v3, 0x7f0a0244

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/TextView;

    .line 135
    .local v22, "instrumentSetupView":Landroid/widget/TextView;
    const v3, 0x7f0a022b

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 136
    .local v15, "accountView":Landroid/widget/TextView;
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 137
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    const v3, 0x7f0a023c

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPurchaseDetailsView:Landroid/view/View;

    .line 139
    const v3, 0x7f0a00c2

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    .line 140
    .local v18, "footerView":Landroid/widget/TextView;
    const v3, 0x7f0a023d

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    .line 141
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPaymentSettingsView:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 143
    const v3, 0x7f0a023f

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mRedeemView:Landroid/widget/TextView;

    .line 144
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mRedeemView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mRedeemView:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 146
    const v3, 0x7f0a0173

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mHeader:Landroid/view/View;

    .line 147
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v4, v4, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mPriceView:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 151
    const v3, 0x7f0a0238

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v26

    check-cast v26, Landroid/widget/TextView;

    .line 152
    .local v26, "priceBylineView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-boolean v3, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasPriceByline:Z

    if-eqz v3, :cond_3

    .line 153
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->priceByline:Ljava/lang/String;

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    move-object/from16 v0, v26

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 155
    const/4 v3, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v0, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->instrument:Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    move-object/from16 v19, v0

    .line 161
    .local v19, "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    if-eqz v19, :cond_6

    .line 162
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->displayTitle:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 164
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    array-length v3, v3

    if-lez v3, :cond_4

    .line 165
    const v3, 0x7f0a023a

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 167
    .local v21, "instrumentErrorView":Landroid/widget/TextView;
    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget-object v0, v3, Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;->disabledMessageHtml:Ljava/lang/String;

    move-object/from16 v20, v0

    .line 168
    .local v20, "instrumentError":Ljava/lang/String;
    invoke-static/range {v20 .. v20}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 170
    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    .end local v20    # "instrumentError":Ljava/lang/String;
    .end local v21    # "instrumentErrorView":Landroid/widget/TextView;
    :goto_1
    const v3, 0x7f0a0239

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    .line 190
    .local v11, "detailMessagesContainer":Landroid/view/ViewGroup;
    const v12, 0x7f0400c1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v13, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->detailHtml:[Ljava/lang/String;

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move v14, v8

    invoke-direct/range {v9 .. v14}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->populateContainerWithTextViews(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I[Ljava/lang/String;I)V

    .line 192
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-boolean v3, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->hasFooterHtml:Z

    if-eqz v3, :cond_7

    .line 193
    const/4 v3, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 194
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->footerHtml:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/google/android/finsky/billing/BillingUtils;->parseHtmlAndColorizeEm(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 197
    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 202
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    if-eqz v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->applicableVouchers:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    array-length v3, v3

    if-lez v3, :cond_0

    .line 203
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->populateVoucherViews(Landroid/view/LayoutInflater;Landroid/view/View;I)V

    .line 206
    :cond_0
    const/16 v24, 0x1

    .line 207
    .local v24, "isExpandable":Z
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v3}, Lcom/google/android/finsky/FinskyApp;->getExperiments(Ljava/lang/String;)Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v17

    .line 209
    .local v17, "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    const-string v3, "cl:billing.cart_details_old_expander"

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 210
    if-eqz v19, :cond_8

    move-object/from16 v0, v19

    iget-object v3, v0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->disabledInfo:[Lcom/google/android/finsky/protos/CommonDevice$DisabledInfo;

    array-length v3, v3

    if-nez v3, :cond_8

    .line 211
    const v3, 0x7f0a023e

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    .line 212
    .local v27, "redeemSeparator":Landroid/view/View;
    const/16 v3, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 213
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mRedeemView:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    .end local v27    # "redeemSeparator":Landroid/view/View;
    :cond_1
    :goto_3
    if-eqz v24, :cond_2

    .line 219
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mHeader:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->updateExpansion()V

    .line 223
    :cond_2
    return-object v25

    .line 157
    .end local v11    # "detailMessagesContainer":Landroid/view/ViewGroup;
    .end local v17    # "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    .end local v19    # "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    .end local v24    # "isExpandable":Z
    :cond_3
    const/16 v3, 0x8

    move-object/from16 v0, v26

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 172
    .restart local v19    # "instrument":Lcom/google/android/finsky/protos/CommonDevice$Instrument;
    :cond_4
    const v3, 0x7f0a0243

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 174
    .local v5, "extendedDetailMessagesContainer":Landroid/view/ViewGroup;
    const v6, 0x7f0400c2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v7, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->extendedDetailHtml:[Ljava/lang/String;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-direct/range {v3 .. v8}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->populateContainerWithTextViews(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;I[Ljava/lang/String;I)V

    .line 177
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mContinueButtonShowsInstrumentManager:Z

    if-nez v3, :cond_5

    const/4 v3, 0x1

    :goto_4
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mContinueButtonConfirmsPurchase:Z

    goto/16 :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    .line 181
    .end local v5    # "extendedDetailMessagesContainer":Landroid/view/ViewGroup;
    :cond_6
    const/16 v3, 0x8

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Purchase$ClientCart;->addInstrumentPromptHtml:Ljava/lang/String;

    invoke-static {v3, v8}, Lcom/google/android/finsky/billing/BillingUtils;->parseHtmlAndColorizeEm(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 185
    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 199
    .restart local v11    # "detailMessagesContainer":Landroid/view/ViewGroup;
    :cond_7
    const/16 v3, 0x8

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 215
    .restart local v17    # "experiments":Lcom/google/android/finsky/experiments/FinskyExperiments;
    .restart local v24    # "isExpandable":Z
    :cond_8
    const/16 v24, 0x0

    goto/16 :goto_3
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 306
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onResume()V

    .line 311
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mHeader:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 312
    .local v0, "context":Landroid/content/Context;
    const v1, 0x7f0c0128

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v4, v4, Lcom/google/android/finsky/protos/Purchase$ClientCart;->title:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mCart:Lcom/google/android/finsky/protos/Purchase$ClientCart;

    iget-object v4, v4, Lcom/google/android/finsky/protos/Purchase$ClientCart;->formattedPrice:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mHeader:Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 315
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 119
    const-string v0, "CartDetailsStep.expanded"

    iget-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CartDetailsStep;->mExpanded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    return-void
.end method
