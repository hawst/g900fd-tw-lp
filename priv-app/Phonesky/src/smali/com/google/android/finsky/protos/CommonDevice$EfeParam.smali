.class public final Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EfeParam"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;


# instance fields
.field public hasKey:Z

.field public hasValue:Z

.field public key:I

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 664
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->clear()Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    .line 665
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    .locals 2

    .prologue
    .line 644
    sget-object v0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    if-nez v0, :cond_1

    .line 645
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 647
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    if-nez v0, :cond_0

    .line 648
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    sput-object v0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    .line 650
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 652
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->_emptyArray:[Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    return-object v0

    .line 650
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 668
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->key:I

    .line 669
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->hasKey:Z

    .line 670
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->value:Ljava/lang/String;

    .line 671
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->hasValue:Z

    .line 672
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->cachedSize:I

    .line 673
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 690
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 691
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->key:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->hasKey:Z

    if-eqz v1, :cond_1

    .line 692
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->key:I

    invoke-static {v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 695
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->hasValue:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->value:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 696
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->value:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 699
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$EfeParam;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 707
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 708
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 712
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 713
    :sswitch_0
    return-object p0

    .line 718
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 719
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 725
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->key:I

    .line 726
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->hasKey:Z

    goto :goto_0

    .line 732
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->value:Ljava/lang/String;

    .line 733
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->hasValue:Z

    goto :goto_0

    .line 708
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    .line 719
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 631
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$EfeParam;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 679
    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->key:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->hasKey:Z

    if-eqz v0, :cond_1

    .line 680
    :cond_0
    iget v0, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->key:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 682
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->hasValue:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->value:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 683
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$EfeParam;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 685
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 686
    return-void
.end method
