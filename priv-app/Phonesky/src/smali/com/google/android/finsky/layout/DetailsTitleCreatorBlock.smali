.class public Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;
.super Landroid/widget/RelativeLayout;
.source "DetailsTitleCreatorBlock.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;
    }
.end annotation


# instance fields
.field private mCreatorDate:Landroid/widget/TextView;

.field private mCreatorImage:Lcom/google/android/play/image/FifeImageView;

.field private mCreatorTitle:Lcom/google/android/finsky/layout/DecoratedTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 93
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 94
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/analytics/FinskyEventLog;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 14
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "eventLogger"    # Lcom/google/android/finsky/analytics/FinskyEventLog;
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v9

    .line 110
    .local v9, "docType":I
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasCreatorDoc()Z

    move-result v11

    .line 115
    .local v11, "hasCreatorDoc":Z
    const/4 v1, 0x2

    if-eq v9, v1, :cond_0

    const/4 v1, 0x4

    if-eq v9, v1, :cond_0

    const/4 v1, 0x5

    if-eq v9, v1, :cond_0

    if-eqz v11, :cond_1

    :cond_0
    const/4 v1, 0x1

    if-ne v9, v1, :cond_2

    .line 118
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->setVisibility(I)V

    .line 186
    :goto_0
    return-void

    .line 122
    :cond_2
    if-nez v11, :cond_9

    .line 124
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorImage:Lcom/google/android/play/image/FifeImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 155
    :cond_3
    :goto_1
    const/4 v1, 0x1

    if-ne v9, v1, :cond_4

    .line 156
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    move-object/from16 v0, p3

    invoke-static {p1, v0, v1}, Lcom/google/android/finsky/utils/BadgeUtils;->configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    .line 160
    :cond_4
    const/4 v1, 0x2

    if-eq v9, v1, :cond_5

    const/4 v1, 0x4

    if-eq v9, v1, :cond_5

    const/4 v1, 0x5

    if-ne v9, v1, :cond_8

    .line 162
    :cond_5
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v12

    .line 165
    .local v12, "isPreorderOffer":Z
    const/4 v13, 0x0

    .line 166
    .local v13, "releaseDate":Ljava/lang/String;
    const/4 v1, 0x2

    if-eq v9, v1, :cond_6

    const/4 v1, 0x4

    if-ne v9, v1, :cond_c

    .line 167
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAlbumDetails()Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/finsky/protos/DocDetails$AlbumDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iget-object v13, v1, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->originalReleaseDate:Ljava/lang/String;

    .line 172
    :cond_7
    :goto_2
    if-nez v12, :cond_d

    invoke-static {v13}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 174
    :try_start_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorDate:Landroid/widget/TextView;

    invoke-static {v13}, Lcom/google/android/finsky/utils/DateUtils;->formatIso8601Date(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorDate:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    .end local v12    # "isPreorderOffer":Z
    .end local v13    # "releaseDate":Ljava/lang/String;
    :cond_8
    :goto_3
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->setVisibility(I)V

    goto :goto_0

    .line 128
    :cond_9
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getCreatorDoc()Lcom/google/android/finsky/api/model/Document;

    move-result-object v6

    .line 129
    .local v6, "creatorDoc":Lcom/google/android/finsky/api/model/Document;
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v8

    .line 131
    .local v8, "creatorImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    if-eqz v8, :cond_a

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_b

    .line 132
    :cond_a
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorImage:Lcom/google/android/play/image/FifeImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 140
    :goto_4
    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    if-eqz p2, :cond_3

    .line 142
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->setFocusable(Z)V

    .line 143
    new-instance v1, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;

    move-object v2, p0

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$1;-><init>(Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;Lcom/google/android/finsky/analytics/FinskyEventLog;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;)V

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_1

    .line 134
    :cond_b
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/protos/Common$Image;

    .line 135
    .local v7, "creatorImage":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v2, v7, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v3, v7, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    move-object/from16 v0, p3

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 137
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorImage:Lcom/google/android/play/image/FifeImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_4

    .line 168
    .end local v6    # "creatorDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v7    # "creatorImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v8    # "creatorImages":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/Common$Image;>;"
    .restart local v12    # "isPreorderOffer":Z
    .restart local v13    # "releaseDate":Ljava/lang/String;
    :cond_c
    const/4 v1, 0x5

    if-ne v9, v1, :cond_7

    .line 169
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBookDetails()Lcom/google/android/finsky/protos/DocDetails$BookDetails;

    move-result-object v1

    iget-object v13, v1, Lcom/google/android/finsky/protos/DocDetails$BookDetails;->publicationDate:Ljava/lang/String;

    goto :goto_2

    .line 176
    :catch_0
    move-exception v10

    .line 177
    .local v10, "e":Ljava/text/ParseException;
    const-string v1, "Cannot parse ISO 8601 date"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v10, v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 178
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorDate:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 181
    .end local v10    # "e":Ljava/text/ParseException;
    :cond_d
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorDate:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 100
    const v0, 0x7f0a01a0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorImage:Lcom/google/android/play/image/FifeImageView;

    .line 101
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorImage:Lcom/google/android/play/image/FifeImageView;

    # getter for: Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;->INSTANCE:Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;
    invoke-static {}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;->access$000()Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock$ArtistAvatarTransformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/play/image/FifeImageView;->setBitmapTransformation(Lcom/google/android/play/image/BitmapTransformation;)V

    .line 102
    const v0, 0x7f0a01a1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DecoratedTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 103
    const v0, 0x7f0a01a2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->mCreatorDate:Landroid/widget/TextView;

    .line 104
    return-void
.end method
