.class public Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;
.super Lcom/google/android/finsky/layout/DetailsPackSection;
.source "DetailsAvatarClusterSection.java"


# instance fields
.field private mAvatarImageType:I

.field private mBackgroundImageType:I

.field private final mCardTopPadding:I

.field private mHasAvatarImage:Z

.field private mMoreLabel:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/DetailsPackSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const/16 v0, 0x190

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mCardTopPadding:I

    .line 37
    return-void
.end method

.method private getCardYOffset()I
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    check-cast v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;

    .line 66
    .local v0, "headerView":Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getBackgroundImageTopMargin()I

    move-result v2

    .line 67
    .local v2, "imageTopMargin":I
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->getBackgroundImageHeight()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    div-int/lit8 v3, v3, 0x3

    add-int v1, v3, v2

    .line 68
    .local v1, "imageOffset":I
    return v1
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 11
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "header"    # Ljava/lang/String;
    .param p3, "subheader"    # Ljava/lang/String;
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "moreLabel"    # Ljava/lang/String;
    .param p6, "avatarImageType"    # I
    .param p7, "backgroundImageType"    # I
    .param p8, "maxItemsPerRow"    # I
    .param p9, "maxRowCount"    # I
    .param p10, "hasDetailsLoaded"    # Z
    .param p11, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 42
    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move/from16 v7, p8

    move/from16 v8, p9

    move/from16 v9, p10

    move-object/from16 v10, p11

    invoke-super/range {v1 .. v10}, Lcom/google/android/finsky/layout/DetailsPackSection;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 44
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mMoreLabel:Ljava/lang/String;

    .line 45
    move/from16 v0, p6

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mAvatarImageType:I

    .line 46
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mBackgroundImageType:I

    .line 47
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->getPaddingLeft()I

    move-result v1

    .line 93
    .local v1, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->getPaddingTop()I

    move-result v2

    .line 94
    .local v2, "paddingTop":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v1, v2, v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->layout(IIII)V

    .line 97
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->getCardYOffset()I

    move-result v3

    add-int v0, v2, v3

    .line 98
    .local v0, "listingTop":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mListingContent:Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mListingContent:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mListingContent:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v1, v0, v4, v5}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 101
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/DetailsPackSection;->onMeasure(II)V

    .line 75
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 76
    .local v1, "availableWidth":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 77
    .local v0, "availableHeight":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 79
    .local v3, "heightMode":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mListingContent:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v4

    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->getCardYOffset()I

    move-result v5

    add-int v2, v4, v5

    .line 81
    .local v2, "height":I
    const/high16 v4, 0x40000000    # 2.0f

    if-ne v3, v4, :cond_1

    .line 82
    move v2, v0

    .line 87
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v2}, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->setMeasuredDimension(II)V

    .line 88
    return-void

    .line 83
    :cond_1
    const/high16 v4, -0x80000000

    if-ne v3, v4, :cond_0

    .line 84
    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    goto :goto_0
.end method

.method protected populateHeader()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget v2, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mBackgroundImageType:I

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v10

    .line 52
    .local v10, "hasBackgroundImage":Z
    if-eqz v10, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget v2, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mBackgroundImageType:I

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Image;

    move-object v3, v0

    .line 55
    .local v3, "backgroundImage":Lcom/google/android/finsky/protos/Common$Image;
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget v2, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mAvatarImageType:I

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mHasAvatarImage:Z

    .line 56
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mHasAvatarImage:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget v1, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mAvatarImageType:I

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Image;

    move-object v4, v0

    .line 58
    .local v4, "avatarImage":Lcom/google/android/finsky/protos/Common$Image;
    :goto_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mHeaderView:Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;

    check-cast v0, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-boolean v5, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mHasAvatarImage:Z

    iget-object v6, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mHeader:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mSubheader:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mMoreLabel:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v11, p0, Lcom/google/android/finsky/layout/DetailsAvatarClusterSection;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v9, v11, p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v9

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/finsky/layout/DetailsAvatarClusterHeader;->setContent(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/finsky/protos/Common$Image;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 61
    return-void

    .end local v3    # "backgroundImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v4    # "avatarImage":Lcom/google/android/finsky/protos/Common$Image;
    :cond_0
    move-object v3, v1

    .line 52
    goto :goto_0

    .restart local v3    # "backgroundImage":Lcom/google/android/finsky/protos/Common$Image;
    :cond_1
    move-object v4, v1

    .line 56
    goto :goto_1
.end method
