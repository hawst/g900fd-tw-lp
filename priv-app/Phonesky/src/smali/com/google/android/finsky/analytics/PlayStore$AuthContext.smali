.class public final Lcom/google/android/finsky/analytics/PlayStore$AuthContext;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AuthContext"
.end annotation


# instance fields
.field public hasMode:Z

.field public mode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5742
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 5743
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->clear()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 5744
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5747
    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->mode:I

    .line 5748
    iput-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->hasMode:Z

    .line 5749
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->cachedSize:I

    .line 5750
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 5764
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 5765
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->mode:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->hasMode:Z

    if-eqz v1, :cond_1

    .line 5766
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->mode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5769
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$AuthContext;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5777
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 5778
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 5782
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 5783
    :sswitch_0
    return-object p0

    .line 5788
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 5789
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 5793
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->mode:I

    .line 5794
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->hasMode:Z

    goto :goto_0

    .line 5778
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    .line 5789
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5716
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5756
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->mode:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->hasMode:Z

    if-eqz v0, :cond_1

    .line 5757
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->mode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 5759
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 5760
    return-void
.end method
