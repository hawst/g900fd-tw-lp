.class public final Lcom/google/android/finsky/protos/Common$Image$Dimension;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Common.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Common$Image;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Dimension"
.end annotation


# instance fields
.field public aspectRatio:I

.field public hasAspectRatio:Z

.field public hasHeight:Z

.field public hasWidth:Z

.field public height:I

.field public width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 986
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 987
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Common$Image$Dimension;->clear()Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .line 988
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Common$Image$Dimension;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 991
    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    .line 992
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasWidth:Z

    .line 993
    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    .line 994
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasHeight:Z

    .line 995
    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    .line 996
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasAspectRatio:Z

    .line 997
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->cachedSize:I

    .line 998
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1018
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1019
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasWidth:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    if-eqz v1, :cond_1

    .line 1020
    :cond_0
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1023
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasHeight:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    if-eqz v1, :cond_3

    .line 1024
    :cond_2
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1027
    :cond_3
    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    if-nez v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasAspectRatio:Z

    if-eqz v1, :cond_5

    .line 1028
    :cond_4
    const/16 v1, 0x12

    iget v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1031
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image$Dimension;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1039
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1040
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1044
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1045
    :sswitch_0
    return-object p0

    .line 1050
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    .line 1051
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasWidth:Z

    goto :goto_0

    .line 1055
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    .line 1056
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasHeight:Z

    goto :goto_0

    .line 1060
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1061
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1069
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    .line 1070
    iput-boolean v3, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasAspectRatio:Z

    goto :goto_0

    .line 1040
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x18 -> :sswitch_1
        0x20 -> :sswitch_2
        0x90 -> :sswitch_3
    .end sparse-switch

    .line 1061
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 957
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Common$Image$Dimension;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Common$Image$Dimension;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1004
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasWidth:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    if-eqz v0, :cond_1

    .line 1005
    :cond_0
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1007
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasHeight:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    if-eqz v0, :cond_3

    .line 1008
    :cond_2
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1010
    :cond_3
    iget v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasAspectRatio:Z

    if-eqz v0, :cond_5

    .line 1011
    :cond_4
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/android/finsky/protos/Common$Image$Dimension;->aspectRatio:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1013
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1014
    return-void
.end method
