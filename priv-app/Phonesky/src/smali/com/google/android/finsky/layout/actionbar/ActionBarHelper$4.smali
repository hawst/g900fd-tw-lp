.class Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$4;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"

# interfaces
.implements Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->configureMenu(Landroid/app/Activity;Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$4;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemActionCollapse(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$4;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    invoke-static {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$500(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$4;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    invoke-static {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$500(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->exitActionBarSearchMode()V

    .line 501
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemActionExpand(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$4;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    invoke-static {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$500(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 490
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$4;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    invoke-static {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$500(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->enterActionBarSearchMode()V

    .line 492
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
