.class public Lcom/google/android/finsky/utils/ConsistencyTokenHelper;
.super Ljava/lang/Object;
.source "ConsistencyTokenHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;
    }
.end annotation


# direct methods
.method public static get(Landroid/content/Context;Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "listener"    # Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/finsky/utils/ConsistencyTokenHelper$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper$1;-><init>(Landroid/content/Context;Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;)V

    .line 45
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/String;>;"
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 46
    return-void
.end method

.method public static getBlocking(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 54
    sget-object v2, Lcom/google/android/finsky/config/G;->consistencyTokenEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 55
    const/4 v0, 0x0

    .line 74
    :goto_0
    return-object v0

    .line 57
    :cond_0
    const/4 v0, 0x0

    .line 59
    .local v0, "consistencyToken":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/checkin/CheckinServiceClient;->getDeviceDataVersionInfo(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesRepairableException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    goto :goto_0

    .line 60
    :catch_0
    move-exception v1

    .line 63
    .local v1, "e":Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
    const-string v2, "Unable to fetch token, GooglePlayServicesRepairableException: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/android/gms/common/GooglePlayServicesRepairableException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 66
    .end local v1    # "e":Lcom/google/android/gms/common/GooglePlayServicesRepairableException;
    :catch_1
    move-exception v1

    .line 67
    .local v1, "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    const-string v2, "Unable to fetch token, GooglePlayServicesNotAvailableException: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 70
    .end local v1    # "e":Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException;
    :catch_2
    move-exception v1

    .line 71
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "Unable to fetch token, IOException: %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
