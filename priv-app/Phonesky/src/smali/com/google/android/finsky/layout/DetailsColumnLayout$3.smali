.class Lcom/google/android/finsky/layout/DetailsColumnLayout$3;
.super Landroid/view/animation/TranslateAnimation;
.source "DetailsColumnLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsColumnLayout;->expandSectionPreIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

.field final synthetic val$initialScrollerPaddingTop:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;FFFFI)V
    .locals 0
    .param p2, "x0"    # F
    .param p3, "x1"    # F
    .param p4, "x2"    # F
    .param p5, "x3"    # F

    .prologue
    .line 251
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$3;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iput p6, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$3;->val$initialScrollerPaddingTop:I

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 6
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    const/4 v5, 0x0

    .line 255
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$3;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mOriginalDistanceTop:I
    invoke-static {v3}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$200(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v1

    .line 256
    .local v1, "initialTitlePaddingTop":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$3;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    # getter for: Lcom/google/android/finsky/layout/DetailsColumnLayout;->mExpandedContainerTopPadding:I
    invoke-static {v3}, Lcom/google/android/finsky/layout/DetailsColumnLayout;->access$300(Lcom/google/android/finsky/layout/DetailsColumnLayout;)I

    move-result v3

    sub-int v3, v1, v3

    int-to-float v3, v3

    mul-float/2addr v3, p1

    float-to-int v3, v3

    sub-int v0, v1, v3

    .line 261
    .local v0, "expanderPaddingTop":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$3;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v3, v3, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->setTopPaddingOnTopView(I)V

    .line 264
    iget v3, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$3;->val$initialScrollerPaddingTop:I

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, p1

    mul-float/2addr v3, v4

    float-to-int v2, v3

    .line 266
    .local v2, "scrollerPaddingTop":I
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$3;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v3, v3, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedScroller:Lcom/google/android/finsky/layout/scroll/GestureScrollView;

    invoke-virtual {v3, v5, v2, v5, v5}, Lcom/google/android/finsky/layout/scroll/GestureScrollView;->setPadding(IIII)V

    .line 267
    return-void
.end method
