.class public Lcom/google/android/finsky/layout/DetailsFlagItemSection;
.super Landroid/widget/LinearLayout;
.source "DetailsFlagItemSection.java"

# interfaces
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoBottomSeparator;


# instance fields
.field private mFlagItemView:Lcom/google/android/finsky/layout/DetailsBylinesCell;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsFlagItemSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/DetailsFlagItemSection;)Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsFlagItemSection;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsFlagItemSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "hasDetailsLoaded"    # Z
    .param p4, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    :cond_0
    if-eqz p3, :cond_1

    .line 50
    new-instance v0, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    const v1, 0x7f0c02f4

    const v2, 0x7f0200cd

    new-instance v3, Lcom/google/android/finsky/layout/DetailsFlagItemSection$1;

    invoke-direct {v3, p0, p2, p1}, Lcom/google/android/finsky/layout/DetailsFlagItemSection$1;-><init>(Lcom/google/android/finsky/layout/DetailsFlagItemSection;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;-><init>(IILandroid/view/View$OnClickListener;)V

    .line 62
    .local v0, "flagEntry":Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsFlagItemSection;->mFlagItemView:Lcom/google/android/finsky/layout/DetailsBylinesCell;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/DetailsBylinesCell;->populate(Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;)V

    .line 64
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/DetailsFlagItemSection;->setVisibility(I)V

    .line 68
    .end local v0    # "flagEntry":Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;
    :goto_0
    return-void

    .line 66
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/DetailsFlagItemSection;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 42
    const v0, 0x7f0a0151

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsFlagItemSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsBylinesCell;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsFlagItemSection;->mFlagItemView:Lcom/google/android/finsky/layout/DetailsBylinesCell;

    .line 43
    return-void
.end method
