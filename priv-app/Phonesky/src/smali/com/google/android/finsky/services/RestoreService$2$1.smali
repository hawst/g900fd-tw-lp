.class Lcom/google/android/finsky/services/RestoreService$2$1;
.super Ljava/lang/Object;
.source "RestoreService.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/RestoreService$2;->onTokenReceived(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/finsky/services/RestoreService$2;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/RestoreService$2;)V
    .locals 0

    .prologue
    .line 1598
    iput-object p1, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 8
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const/4 v7, 0x0

    .line 1601
    iget-object v4, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v4, v4, Lcom/google/android/finsky/services/RestoreService$2;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->volleyErrorToInstallerError(Lcom/android/volley/VolleyError;)I
    invoke-static {v4, p1}, Lcom/google/android/finsky/services/RestoreService;->access$3000(Lcom/google/android/finsky/services/RestoreService;Lcom/android/volley/VolleyError;)I

    move-result v2

    .line 1602
    .local v2, "errorCode":I
    iget-object v4, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v4, v4, Lcom/google/android/finsky/services/RestoreService$2;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v4}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v5, v5, Lcom/google/android/finsky/services/RestoreService$2;->val$accountName:Ljava/lang/String;

    invoke-virtual {v4, v5, v7, v2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->finishAccount(Ljava/lang/String;ZI)V

    .line 1603
    const-string v4, "Error while getting list of applications to restore from server: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/android/volley/VolleyError;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1605
    iget-object v4, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v4, v4, Lcom/google/android/finsky/services/RestoreService$2;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # getter for: Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    invoke-static {v4}, Lcom/google/android/finsky/services/RestoreService;->access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v5, v5, Lcom/google/android/finsky/services/RestoreService$2;->val$accountName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->tryAgainOrDeleteAccount(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1607
    iget-object v4, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v4, v4, Lcom/google/android/finsky/services/RestoreService$2;->val$aid:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v5, v5, Lcom/google/android/finsky/services/RestoreService$2;->val$accountName:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v6, v6, Lcom/google/android/finsky/services/RestoreService$2;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->getRestoreIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;
    invoke-static {v4, v5, v6}, Lcom/google/android/finsky/services/RestoreService;->access$3100(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    .line 1609
    .local v3, "restoreIntent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v5, v4, Lcom/google/android/finsky/services/RestoreService$2;->this$0:Lcom/google/android/finsky/services/RestoreService;

    sget-object v4, Lcom/google/android/finsky/config/G;->appRestoreRetryFetchListHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    # invokes: Lcom/google/android/finsky/services/RestoreService;->jitterDelay(J)J
    invoke-static {v5, v6, v7}, Lcom/google/android/finsky/services/RestoreService;->access$1500(Lcom/google/android/finsky/services/RestoreService;J)J

    move-result-wide v0

    .line 1611
    .local v0, "delay":J
    iget-object v4, p0, Lcom/google/android/finsky/services/RestoreService$2$1;->this$1:Lcom/google/android/finsky/services/RestoreService$2;

    iget-object v4, v4, Lcom/google/android/finsky/services/RestoreService$2;->this$0:Lcom/google/android/finsky/services/RestoreService;

    # invokes: Lcom/google/android/finsky/services/RestoreService;->setAlarm(Landroid/content/Intent;J)J
    invoke-static {v4, v3, v0, v1}, Lcom/google/android/finsky/services/RestoreService;->access$1600(Lcom/google/android/finsky/services/RestoreService;Landroid/content/Intent;J)J

    .line 1613
    .end local v0    # "delay":J
    .end local v3    # "restoreIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
