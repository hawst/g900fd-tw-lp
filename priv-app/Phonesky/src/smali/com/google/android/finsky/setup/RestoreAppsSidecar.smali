.class public Lcom/google/android/finsky/setup/RestoreAppsSidecar;
.super Lcom/google/android/finsky/fragments/SidecarFragment;
.source "RestoreAppsSidecar.java"


# instance fields
.field private mBackupDocumentInfos:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

.field private mDfeApi:Lcom/google/android/finsky/api/DfeApi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/SidecarFragment;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/finsky/setup/RestoreAppsSidecar;[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;)[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/RestoreAppsSidecar;
    .param p1, "x1"    # [Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->mBackupDocumentInfos:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/setup/RestoreAppsSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/RestoreAppsSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/setup/RestoreAppsSidecar;II)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/RestoreAppsSidecar;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-virtual {p0, p1, p2}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->setState(II)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/setup/RestoreAppsSidecar;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/finsky/setup/RestoreAppsSidecar;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 36
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 37
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    new-instance v1, Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    invoke-direct {v1}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;-><init>()V

    .line 39
    .local v1, "result":Lcom/google/android/finsky/setup/RestoreAppsSidecar;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->setArguments(Landroid/os/Bundle;)V

    .line 40
    return-object v1
.end method


# virtual methods
.method public fetchBackupDocs(J)V
    .locals 5
    .param p1, "androidId"    # J

    .prologue
    const/4 v1, 0x4

    const/4 v3, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getState()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 57
    const-string v0, "Making another load request while in loading state: %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getState()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p0, v1, v3}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->setState(II)V

    .line 61
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/setup/RestoreAppsSidecar$1;-><init>(Lcom/google/android/finsky/setup/RestoreAppsSidecar;J)V

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper;->get(Landroid/content/Context;Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;)V

    goto :goto_0
.end method

.method public getBackupDocumentInfos()[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->mBackupDocumentInfos:[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/SidecarFragment;->onCreate(Landroid/os/Bundle;)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "accountName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 48
    return-void
.end method
