.class public final Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "EarlyUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/EarlyUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EarlyDocumentInfo"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;


# instance fields
.field public background:Z

.field public critical:Z

.field public docid:Lcom/google/android/finsky/protos/Common$Docid;

.field public hasBackground:Z

.field public hasCritical:Z

.field public hasTitle:Z

.field public hasVersionCode:Z

.field public title:Ljava/lang/String;

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->clear()Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;

    .line 133
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    .locals 2

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->_emptyArray:[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;

    if-nez v0, :cond_1

    .line 102
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 104
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->_emptyArray:[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;

    if-nez v0, :cond_0

    .line 105
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;

    sput-object v0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->_emptyArray:[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;

    .line 107
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->_emptyArray:[Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;

    return-object v0

    .line 107
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->title:Ljava/lang/String;

    .line 138
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasTitle:Z

    .line 139
    iput v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->versionCode:I

    .line 140
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasVersionCode:Z

    .line 141
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->background:Z

    .line 142
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasBackground:Z

    .line 143
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->critical:Z

    .line 144
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasCritical:Z

    .line 145
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->cachedSize:I

    .line 146
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 172
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 173
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v1, :cond_0

    .line 174
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasTitle:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 178
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasVersionCode:Z

    if-nez v1, :cond_3

    iget v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->versionCode:I

    if-eqz v1, :cond_4

    .line 182
    :cond_3
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 185
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasBackground:Z

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->background:Z

    if-eqz v1, :cond_6

    .line 186
    :cond_5
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->background:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 189
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasCritical:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->critical:Z

    if-eqz v1, :cond_8

    .line 190
    :cond_7
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->critical:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 193
    :cond_8
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 201
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 202
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 206
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 207
    :sswitch_0
    return-object p0

    .line 212
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-nez v1, :cond_1

    .line 213
    new-instance v1, Lcom/google/android/finsky/protos/Common$Docid;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Docid;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 215
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 219
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->title:Ljava/lang/String;

    .line 220
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasTitle:Z

    goto :goto_0

    .line 224
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->versionCode:I

    .line 225
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasVersionCode:Z

    goto :goto_0

    .line 229
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->background:Z

    .line 230
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasBackground:Z

    goto :goto_0

    .line 234
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->critical:Z

    .line 235
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasCritical:Z

    goto :goto_0

    .line 202
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 95
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    if-eqz v0, :cond_0

    .line 153
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 155
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasTitle:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 156
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 158
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasVersionCode:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->versionCode:I

    if-eqz v0, :cond_4

    .line 159
    :cond_3
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 161
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasBackground:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->background:Z

    if-eqz v0, :cond_6

    .line 162
    :cond_5
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->background:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 164
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->hasCritical:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->critical:Z

    if-eqz v0, :cond_8

    .line 165
    :cond_7
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EarlyUpdate$EarlyDocumentInfo;->critical:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 167
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 168
    return-void
.end method
