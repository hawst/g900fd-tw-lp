.class Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;
.super Ljava/lang/Object;
.source "SetupWizardUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/SetupWizardUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FullscreenAdjustResizeWorkaround"
.end annotation


# instance fields
.field private mActivityView:Landroid/view/View;

.field private mFrameLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

.field private mPreviousHeight:I

.field private mStatusBarHeight:I


# direct methods
.method private constructor <init>(Landroid/app/Activity;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v6, 0x0

    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    iput v6, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mStatusBarHeight:I

    .line 197
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "status_bar_height"

    const-string v4, "dimen"

    const-string v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 199
    .local v1, "resourceId":I
    if-lez v1, :cond_0

    .line 200
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mStatusBarHeight:I

    .line 202
    :cond_0
    const v2, 0x1020002

    invoke-virtual {p1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 203
    .local v0, "content":Landroid/widget/FrameLayout;
    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mActivityView:Landroid/view/View;

    .line 204
    iget-object v2, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mActivityView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround$1;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround$1;-><init>(Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 211
    iget-object v2, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mActivityView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    iput-object v2, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mFrameLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    .line 212
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->resizeOrRestoreContentSize()V

    return-void
.end method

.method private computeVisibleViewHeight()I
    .locals 3

    .prologue
    .line 232
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 233
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mActivityView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 235
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mStatusBarHeight:I

    add-int/2addr v1, v2

    return v1
.end method

.method public static forceSizeChanges(Landroid/app/Activity;)V
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 188
    new-instance v0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;-><init>(Landroid/app/Activity;)V

    .line 189
    return-void
.end method

.method private resizeOrRestoreContentSize()V
    .locals 5

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->computeVisibleViewHeight()I

    move-result v2

    .line 216
    .local v2, "visibleViewHeight":I
    iget v3, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mPreviousHeight:I

    if-eq v2, v3, :cond_0

    .line 217
    iget-object v3, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mActivityView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 218
    .local v0, "fullViewHeight":I
    sub-int v1, v0, v2

    .line 219
    .local v1, "heightDelta":I
    div-int/lit8 v3, v0, 0x4

    if-le v1, v3, :cond_1

    .line 221
    iget-object v3, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mFrameLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    sub-int v4, v0, v1

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 226
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mActivityView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->requestLayout()V

    .line 227
    iput v2, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mPreviousHeight:I

    .line 229
    .end local v0    # "fullViewHeight":I
    .end local v1    # "heightDelta":I
    :cond_0
    return-void

    .line 224
    .restart local v0    # "fullViewHeight":I
    .restart local v1    # "heightDelta":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/utils/SetupWizardUtils$FullscreenAdjustResizeWorkaround;->mFrameLayoutParams:Landroid/widget/FrameLayout$LayoutParams;

    iput v0, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    goto :goto_0
.end method
