.class public Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;
.super Lcom/google/android/finsky/activities/myapps/MyAppsTab;
.source "MyAppsLibraryTab.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/activities/myapps/MyAppsTab",
        "<",
        "Lcom/google/android/finsky/api/model/DfeList;",
        ">;",
        "Landroid/view/View$OnLongClickListener;"
    }
.end annotation


# static fields
.field public static final SUPPORTS_MULTI_CHOICE:Z


# instance fields
.field private final mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

.field private final mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

.field private mCurrentActionMode:Landroid/view/ActionMode;

.field private final mFragment:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

.field private mLibraryView:Landroid/view/ViewGroup;

.field private mListInitialized:Z

.field private mListView:Lcom/google/android/finsky/layout/play/PlayListView;

.field private mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->SUPPORTS_MULTI_CHOICE:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;Lcom/google/android/finsky/library/AccountLibrary;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 13
    .param p1, "authenticatedActivity"    # Lcom/google/android/finsky/activities/AuthenticatedActivity;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "fragment"    # Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
    .param p7, "accountLibrary"    # Lcom/google/android/finsky/library/AccountLibrary;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 76
    invoke-direct/range {p0 .. p4}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;-><init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 65
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListInitialized:Z

    .line 67
    new-instance v1, Lcom/google/android/finsky/utils/ObjectMap;

    invoke-direct {v1}, Lcom/google/android/finsky/utils/ObjectMap;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 77
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mFragment:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    .line 78
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    .line 80
    new-instance v10, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$1;

    invoke-direct {v10, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$1;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;)V

    .line 88
    .local v10, "onArchiveActionListener":Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;
    new-instance v12, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v1, 0x195

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p8

    invoke-direct {v12, v1, v2, v3, v0}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 91
    .local v12, "mUiElementNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    new-instance v1, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    move-object v2, p1

    move-object/from16 v3, p4

    move-object/from16 v8, p5

    move-object v9, p0

    move-object v11, p0

    invoke-direct/range {v1 .. v12}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;-><init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/PackageStateRepository;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/play/image/BitmapLoader;Landroid/view/View$OnClickListener;Lcom/google/android/finsky/layout/play/PlayCardViewMyApps$OnArchiveActionListener;Landroid/view/View$OnLongClickListener;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    .line 94
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->showAccountSwitcher()V

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;)Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mFragment:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;)Landroid/view/ActionMode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mCurrentActionMode:Landroid/view/ActionMode;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;Landroid/view/ActionMode;)Landroid/view/ActionMode;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;
    .param p1, "x1"    # Landroid/view/ActionMode;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mCurrentActionMode:Landroid/view/ActionMode;

    return-object p1
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;)Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    return-object v0
.end method

.method private getLibraryList()Lcom/google/android/finsky/api/model/DfeList;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 159
    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    sget-object v5, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_APPS:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/library/AccountLibrary;->getServerToken(Ljava/lang/String;)[B

    move-result-object v0

    .line 160
    .local v0, "appsToken":[B
    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    const/4 v5, 0x3

    sget-object v6, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_APPS:Ljava/lang/String;

    invoke-interface {v4, v5, v6, v7, v0}, Lcom/google/android/finsky/api/DfeApi;->getLibraryUrl(ILjava/lang/String;I[B)Ljava/lang/String;

    move-result-object v3

    .line 163
    .local v3, "libraryUrl":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v5, "MyAppsLibraryTab.ListData"

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/utils/ObjectMap;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 165
    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v5, "MyAppsLibraryTab.ListData"

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/utils/ObjectMap;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/DfeList;

    .line 166
    .local v1, "dfeList":Lcom/google/android/finsky/api/model/DfeList;
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/DfeList;->getInitialUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 167
    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/api/model/DfeList;->setDfeApi(Lcom/google/android/finsky/api/DfeApi;)V

    move-object v2, v1

    .line 173
    .end local v1    # "dfeList":Lcom/google/android/finsky/api/model/DfeList;
    .local v2, "dfeList":Ljava/lang/Object;
    :goto_0
    return-object v2

    .line 171
    .end local v2    # "dfeList":Ljava/lang/Object;
    :cond_0
    new-instance v1, Lcom/google/android/finsky/api/model/DfeList;

    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-direct {v1, v4, v3, v7}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    .line 172
    .restart local v1    # "dfeList":Lcom/google/android/finsky/api/model/DfeList;
    const-string v4, "com.google.android.gms"

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/api/model/DfeList;->filterDocId(Ljava/lang/String;)V

    move-object v2, v1

    .line 173
    .restart local v2    # "dfeList":Ljava/lang/Object;
    goto :goto_0
.end method


# virtual methods
.method protected finishActiveMode()Z
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mCurrentActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mCurrentActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 272
    const/4 v0, 0x1

    .line 274
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getAdapter()Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    return-object v0
.end method

.method protected getDocumentForView(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 178
    invoke-static {p1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->getViewDoc(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    return-object v0
.end method

.method protected getFullView()Landroid/view/View;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mLibraryView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    return-object v0
.end method

.method public getView(I)Landroid/view/View;
    .locals 3
    .param p1, "backendId"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mLibraryView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0400dd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mLibraryView:Landroid/view/ViewGroup;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mLibraryView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 231
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayListView;->getChoiceMode()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 232
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->onClick(Landroid/view/View;)V

    .line 237
    :goto_0
    return-void

    .line 235
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 236
    .local v0, "position":I
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/play/PlayListView;->isItemChecked(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v0, v1}, Lcom/google/android/finsky/layout/play/PlayListView;->setItemChecked(IZ)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onDataChanged()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 112
    invoke-super {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->onDataChanged()V

    .line 113
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListInitialized:Z

    if-nez v1, :cond_2

    .line 114
    iput-boolean v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListInitialized:Z

    .line 115
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mLibraryView:Landroid/view/ViewGroup;

    const v2, 0x7f0a0276

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayListView;

    iput-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    .line 116
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/UiUtils;->getGridHorizontalPadding(Landroid/content/res/Resources;)I

    move-result v0

    .line 117
    .local v0, "horizontalPadding":I
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayListView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayListView;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v1, v0, v2, v0, v3}, Lcom/google/android/finsky/layout/play/PlayListView;->setPadding(IIII)V

    .line 119
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/layout/play/PlayListView;->setAnimateChanges(Z)V

    .line 120
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 121
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v1, v4}, Lcom/google/android/finsky/layout/play/PlayListView;->setItemsCanFocus(Z)V

    .line 122
    sget-boolean v1, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->SUPPORTS_MULTI_CHOICE:Z

    if-eqz v1, :cond_0

    .line 123
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    new-instance v2, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$MultiChoiceListener;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab$1;)V

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v2, "MyAppsTab.KeyListParcel"

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/utils/ObjectMap;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 126
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v3, "MyAppsTab.KeyListParcel"

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/utils/ObjectMap;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/layout/play/PlayListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 130
    :cond_1
    const v1, 0x7f0c0333

    invoke-virtual {p0, v4, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->configureEmptyUI(ZI)V

    .line 132
    .end local v0    # "horizontalPadding":I
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->syncViewToState()V

    .line 133
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    invoke-virtual {v1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->onDataChanged()V

    .line 134
    return-void
.end method

.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3, "statusCode"    # I

    .prologue
    .line 198
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->UNINSTALLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-ne p2, v0, :cond_1

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->notifyDataSetChanged()V

    .line 202
    :cond_1
    return-void
.end method

.method public onLibraryContentsChanged(Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 0
    .param p1, "library"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    .line 207
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 241
    sget-boolean v3, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->SUPPORTS_MULTI_CHOICE:Z

    if-eqz v3, :cond_3

    .line 242
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayListView;->getChoiceMode()I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 243
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    invoke-virtual {v3, v2}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->setMultiChoiceMode(Z)V

    .line 244
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/layout/play/PlayListView;->setChoiceMode(I)V

    .line 246
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 247
    .local v0, "position":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    .line 248
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/layout/play/PlayListView;->isItemChecked(I)Z

    move-result v4

    if-nez v4, :cond_1

    move v1, v2

    :cond_1
    invoke-virtual {v3, v0, v1}, Lcom/google/android/finsky/layout/play/PlayListView;->setItemChecked(IZ)V

    .line 252
    .end local v0    # "position":I
    :cond_2
    :goto_0
    return v2

    :cond_3
    move v2, v1

    goto :goto_0
.end method

.method public onRestoreInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V
    .locals 0
    .param p1, "savedInstanceState"    # Lcom/google/android/finsky/utils/ObjectMap;

    .prologue
    .line 211
    if-eqz p1, :cond_0

    .line 212
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 214
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Lcom/google/android/finsky/utils/ObjectMap;
    .locals 3

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v1, "MyAppsLibraryTab.ListData"

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/ObjectMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v1, "MyAppsTab.KeyListParcel"

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mListView:Lcom/google/android/finsky/layout/play/PlayListView;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/ObjectMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    return-object v0
.end method

.method public removeItems(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 376
    .local p1, "docids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 377
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v2, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeList;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 378
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v3, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v3, v1}, Lcom/google/android/finsky/api/model/DfeList;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 379
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v2, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v2, v1}, Lcom/google/android/finsky/api/model/DfeList;->removeItem(I)V

    .line 376
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 377
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 384
    .end local v1    # "j":I
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->notifyDataSetChanged()V

    .line 385
    return-void
.end method

.method protected requestData()V
    .locals 2

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->clearState()V

    .line 151
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->getLibraryList()Lcom/google/android/finsky/api/model/DfeList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 155
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryAdapter;->setDfeList(Lcom/google/android/finsky/api/model/DfeList;)V

    .line 156
    return-void
.end method

.method protected retryFromError()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->resetItems()V

    .line 144
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->clearTransientState()V

    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v0, Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 146
    return-void
.end method

.method public setTabSelected(Z)V
    .locals 0
    .param p1, "isSelected"    # Z

    .prologue
    .line 257
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->setTabSelected(Z)V

    .line 258
    if-nez p1, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsLibraryTab;->finishActiveMode()Z

    .line 261
    :cond_0
    return-void
.end method
