.class public Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;
.super Lcom/google/android/finsky/layout/CreditCardImagesAnimator;
.source "CreditCardImagesAnimatorIcs.java"


# instance fields
.field private mGeneralImage:Landroid/widget/ImageView;

.field private mInOneCardMode:Z


# direct methods
.method public constructor <init>([Landroid/widget/ImageView;[Lcom/google/android/finsky/billing/creditcard/CreditCardType;Landroid/widget/ImageView;)V
    .locals 0
    .param p1, "images"    # [Landroid/widget/ImageView;
    .param p2, "types"    # [Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    .param p3, "generalImage"    # Landroid/widget/ImageView;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/CreditCardImagesAnimator;-><init>([Landroid/widget/ImageView;[Lcom/google/android/finsky/billing/creditcard/CreditCardType;)V

    .line 32
    iput-object p3, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    .line 33
    return-void
.end method


# virtual methods
.method public animateToType(Lcom/google/android/finsky/billing/creditcard/CreditCardType;)V
    .locals 9
    .param p1, "type"    # Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .prologue
    const/4 v8, -0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 37
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    if-eq p1, v5, :cond_5

    .line 38
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->findIndex(Lcom/google/android/finsky/billing/creditcard/CreditCardType;)I

    move-result v2

    .line 39
    .local v2, "index":I
    if-ne v2, v8, :cond_1

    iget-boolean v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-nez v5, :cond_1

    move v1, v3

    .line 40
    .local v1, "inactiveAlpha":F
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    array-length v5, v5

    if-ge v0, v5, :cond_3

    .line 41
    if-ne v0, v2, :cond_2

    .line 42
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 43
    iget-boolean v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-nez v5, :cond_0

    .line 44
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLeft()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    .line 40
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "inactiveAlpha":F
    :cond_1
    move v1, v4

    .line 39
    goto :goto_0

    .line 47
    .restart local v0    # "i":I
    .restart local v1    # "inactiveAlpha":F
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 48
    iget-boolean v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-nez v5, :cond_0

    .line 49
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_2

    .line 53
    :cond_3
    iget-boolean v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-eqz v5, :cond_4

    .line 54
    if-ne v2, v8, :cond_6

    .line 55
    iget-object v4, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 60
    :cond_4
    :goto_3
    iput-object p1, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .line 62
    .end local v0    # "i":I
    .end local v1    # "inactiveAlpha":F
    .end local v2    # "index":I
    :cond_5
    return-void

    .line 57
    .restart local v0    # "i":I
    .restart local v1    # "inactiveAlpha":F
    .restart local v2    # "index":I
    :cond_6
    iget-object v3, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_3
.end method

.method public restoreCardType(Lcom/google/android/finsky/billing/creditcard/CreditCardType;)V
    .locals 8
    .param p1, "type"    # Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 70
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->findIndex(Lcom/google/android/finsky/billing/creditcard/CreditCardType;)I

    move-result v2

    .line 71
    .local v2, "index":I
    const/4 v5, -0x1

    if-ne v2, v5, :cond_0

    move v1, v3

    .line 72
    .local v1, "inactiveAlpha":F
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    array-length v5, v5

    if-ge v0, v5, :cond_2

    .line 76
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 77
    if-ne v0, v2, :cond_1

    .line 78
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 79
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLeft()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setX(F)V

    .line 72
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    .end local v1    # "inactiveAlpha":F
    :cond_0
    move v1, v4

    .line 71
    goto :goto_0

    .line 81
    .restart local v0    # "i":I
    .restart local v1    # "inactiveAlpha":F
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 82
    iget-object v5, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v5, v5, v0

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setTranslationX(F)V

    goto :goto_2

    .line 85
    :cond_2
    return-void
.end method

.method public switchToOneCardMode()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 94
    iget-boolean v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    if-nez v2, :cond_2

    .line 95
    iget-object v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->findIndex(Lcom/google/android/finsky/billing/creditcard/CreditCardType;)I

    move-result v0

    .line 96
    .local v0, "currentIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 97
    iget-object v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 98
    iget-object v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v3, v3, v6

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLeft()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 99
    if-ne v1, v0, :cond_0

    .line 100
    iget-object v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v2, v2, v1

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 96
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 102
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mImages:[Landroid/widget/ImageView;

    aget-object v2, v2, v1

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_1

    .line 105
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 106
    const/4 v2, -0x1

    if-ne v0, v2, :cond_3

    .line 107
    iget-object v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 112
    .end local v0    # "currentIndex":I
    .end local v1    # "i":I
    :cond_2
    :goto_2
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mInOneCardMode:Z

    .line 113
    return-void

    .line 109
    .restart local v0    # "currentIndex":I
    .restart local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/layout/CreditCardImagesAnimatorIcs;->mGeneralImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_2
.end method
