.class public Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;
.super Landroid/widget/RelativeLayout;
.source "PlayCardSubtitleLabel.java"


# instance fields
.field protected mLabel:Landroid/view/View;

.field protected mSubtitle:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 46
    const v0, 0x7f0a0200

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    .line 47
    const v0, 0x7f0a01ff

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mLabel:Landroid/view/View;

    .line 48
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->getWidth()I

    move-result v4

    .line 80
    .local v4, "width":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 82
    .local v3, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    iget v6, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    const/4 v7, 0x0

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 86
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mLabel:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 88
    .local v0, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getBaseline()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mLabel:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getBaseline()I

    move-result v6

    sub-int v2, v5, v6

    .line 89
    .local v2, "labelTop":I
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v1, v4, v5

    .line 90
    .local v1, "labelRight":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mLabel:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mLabel:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    sub-int v6, v1, v6

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mLabel:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v2

    invoke-virtual {v5, v6, v2, v1, v7}, Landroid/view/View;->layout(IIII)V

    .line 92
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v8, 0x0

    .line 52
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 54
    .local v4, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->getPaddingLeft()I

    move-result v5

    sub-int v5, v4, v5

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->getPaddingRight()I

    move-result v6

    sub-int v0, v5, v6

    .line 56
    .local v0, "contentWidth":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mLabel:Landroid/view/View;

    const/high16 v6, -0x80000000

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v6, v8}, Landroid/view/View;->measure(II)V

    .line 60
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    invoke-virtual {v5, v8, v8}, Landroid/view/View;->measure(II)V

    .line 63
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 65
    .local v2, "subtitleLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mLabel:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 67
    .local v1, "labelLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    iget v6, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v6, v4, v6

    iget v7, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v6, v7

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mLabel:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v6, v7

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v6, v7

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v6, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 70
    .local v3, "subtitleWidth":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v6, v8}, Landroid/view/View;->measure(II)V

    .line 73
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->mSubtitle:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {p0, v4, v5}, Lcom/google/android/finsky/layout/play/PlayCardSubtitleLabel;->setMeasuredDimension(II)V

    .line 74
    return-void
.end method
