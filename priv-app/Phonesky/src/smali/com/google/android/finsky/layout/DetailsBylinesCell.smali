.class public Lcom/google/android/finsky/layout/DetailsBylinesCell;
.super Landroid/widget/RelativeLayout;
.source "DetailsBylinesCell.java"


# instance fields
.field private mThumbnail:Landroid/widget/ImageView;

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsBylinesCell;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 36
    const v0, 0x7f0a0148

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsBylinesCell;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesCell;->mThumbnail:Landroid/widget/ImageView;

    .line 37
    const v0, 0x7f0a0149

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsBylinesCell;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesCell;->mTitle:Landroid/widget/TextView;

    .line 38
    return-void
.end method

.method public populate(Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;)V
    .locals 3
    .param p1, "bylineEntry"    # Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;

    .prologue
    const/4 v2, 0x0

    .line 41
    iget v0, p1, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->iconResourceId:I

    if-gez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesCell;->mThumbnail:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 48
    :goto_0
    iget v0, p1, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->textResourceId:I

    if-lez v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesCell;->mTitle:Landroid/widget/TextView;

    iget v1, p1, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->textResourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 54
    :goto_1
    iget-object v0, p1, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->onClickListener:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    .line 55
    iget-object v0, p1, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsBylinesCell;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    :goto_2
    return-void

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesCell;->mThumbnail:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesCell;->mThumbnail:Landroid/widget/ImageView;

    iget v1, p1, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->iconResourceId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 51
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsBylinesCell;->mTitle:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/finsky/layout/DetailsBylinesSection$DetailsBylineEntry;->text:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 57
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsBylinesCell;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/DetailsBylinesCell;->setClickable(Z)V

    goto :goto_2
.end method
