.class public final Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SearchSuggestionReport"
.end annotation


# instance fields
.field public clientLatencyMs:J

.field public hasClientLatencyMs:Z

.field public hasQuery:Z

.field public hasResponseServerLogsCookie:Z

.field public hasResultCount:Z

.field public hasSource:Z

.field public hasSuggestedQuery:Z

.field public hasSuggestionServerLogsCookie:Z

.field public query:Ljava/lang/String;

.field public responseServerLogsCookie:[B

.field public resultCount:I

.field public source:I

.field public suggestedQuery:Ljava/lang/String;

.field public suggestionServerLogsCookie:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4081
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4082
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->clear()Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    .line 4083
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 4229
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4086
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->query:Ljava/lang/String;

    .line 4087
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasQuery:Z

    .line 4088
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestedQuery:Ljava/lang/String;

    .line 4089
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestedQuery:Z

    .line 4090
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->clientLatencyMs:J

    .line 4091
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasClientLatencyMs:Z

    .line 4092
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->source:I

    .line 4093
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSource:Z

    .line 4094
    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->resultCount:I

    .line 4095
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResultCount:Z

    .line 4096
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->responseServerLogsCookie:[B

    .line 4097
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResponseServerLogsCookie:Z

    .line 4098
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestionServerLogsCookie:[B

    .line 4099
    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestionServerLogsCookie:Z

    .line 4100
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->cachedSize:I

    .line 4101
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 4133
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4134
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasQuery:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->query:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4135
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->query:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4138
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestedQuery:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestedQuery:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4139
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestedQuery:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4142
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasClientLatencyMs:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->clientLatencyMs:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 4143
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->clientLatencyMs:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4146
    :cond_5
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->source:I

    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSource:Z

    if-eqz v1, :cond_7

    .line 4147
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->source:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4150
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResultCount:Z

    if-nez v1, :cond_8

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->resultCount:I

    if-eqz v1, :cond_9

    .line 4151
    :cond_8
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->resultCount:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4154
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResponseServerLogsCookie:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->responseServerLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_b

    .line 4155
    :cond_a
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->responseServerLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 4158
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestionServerLogsCookie:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestionServerLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_d

    .line 4159
    :cond_c
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestionServerLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 4162
    :cond_d
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 4170
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4171
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4175
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 4176
    :sswitch_0
    return-object p0

    .line 4181
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->query:Ljava/lang/String;

    .line 4182
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasQuery:Z

    goto :goto_0

    .line 4186
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestedQuery:Ljava/lang/String;

    .line 4187
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestedQuery:Z

    goto :goto_0

    .line 4191
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->clientLatencyMs:J

    .line 4192
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasClientLatencyMs:Z

    goto :goto_0

    .line 4196
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 4197
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 4202
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->source:I

    .line 4203
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSource:Z

    goto :goto_0

    .line 4209
    .end local v1    # "value":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->resultCount:I

    .line 4210
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResultCount:Z

    goto :goto_0

    .line 4214
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->responseServerLogsCookie:[B

    .line 4215
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResponseServerLogsCookie:Z

    goto :goto_0

    .line 4219
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestionServerLogsCookie:[B

    .line 4220
    iput-boolean v4, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestionServerLogsCookie:Z

    goto :goto_0

    .line 4171
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    .line 4197
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4030
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4107
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasQuery:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->query:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4108
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->query:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4110
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestedQuery:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestedQuery:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4111
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestedQuery:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4113
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasClientLatencyMs:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->clientLatencyMs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 4114
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->clientLatencyMs:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4116
    :cond_5
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->source:I

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSource:Z

    if-eqz v0, :cond_7

    .line 4117
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->source:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4119
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResultCount:Z

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->resultCount:I

    if-eqz v0, :cond_9

    .line 4120
    :cond_8
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->resultCount:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 4122
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResponseServerLogsCookie:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->responseServerLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_b

    .line 4123
    :cond_a
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->responseServerLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 4125
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestionServerLogsCookie:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestionServerLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_d

    .line 4126
    :cond_c
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestionServerLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 4128
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4129
    return-void
.end method
