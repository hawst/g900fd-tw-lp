.class public final Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CommonDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CommonDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BillingAddressSpec"
.end annotation


# instance fields
.field public billingAddressType:I

.field public hasBillingAddressType:Z

.field public requiredField:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2005
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2006
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->clear()Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    .line 2007
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    .locals 1

    .prologue
    .line 2010
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->billingAddressType:I

    .line 2011
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->hasBillingAddressType:Z

    .line 2012
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    .line 2013
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->cachedSize:I

    .line 2014
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2033
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v3

    .line 2034
    .local v3, "size":I
    iget v4, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->billingAddressType:I

    if-ne v4, v5, :cond_0

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->hasBillingAddressType:Z

    if-eqz v4, :cond_1

    .line 2035
    :cond_0
    iget v4, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->billingAddressType:I

    invoke-static {v5, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 2038
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    array-length v4, v4

    if-lez v4, :cond_3

    .line 2039
    const/4 v0, 0x0

    .line 2040
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    array-length v4, v4

    if-ge v2, v4, :cond_2

    .line 2041
    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    aget v1, v4, v2

    .line 2042
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 2040
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2045
    .end local v1    # "element":I
    :cond_2
    add-int/2addr v3, v0

    .line 2046
    iget-object v4, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    .line 2048
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_3
    return v3
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    .locals 17
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2056
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v9

    .line 2057
    .local v9, "tag":I
    sparse-switch v9, :sswitch_data_0

    .line 2061
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v14

    if-nez v14, :cond_0

    .line 2062
    :sswitch_0
    return-object p0

    .line 2067
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 2068
    .local v13, "value":I
    packed-switch v13, :pswitch_data_0

    goto :goto_0

    .line 2074
    :pswitch_0
    move-object/from16 v0, p0

    iput v13, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->billingAddressType:I

    .line 2075
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->hasBillingAddressType:Z

    goto :goto_0

    .line 2081
    .end local v13    # "value":I
    :sswitch_2
    const/16 v14, 0x10

    move-object/from16 v0, p1

    invoke-static {v0, v14}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v5

    .line 2083
    .local v5, "length":I
    new-array v12, v5, [I

    .line 2084
    .local v12, "validValues":[I
    const/4 v10, 0x0

    .line 2085
    .local v10, "validCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    move v11, v10

    .end local v10    # "validCount":I
    .local v11, "validCount":I
    :goto_1
    if-ge v3, v5, :cond_2

    .line 2086
    if-eqz v3, :cond_1

    .line 2087
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2089
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 2090
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_1

    move v10, v11

    .line 2085
    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v11, v10

    .end local v10    # "validCount":I
    .restart local v11    # "validCount":I
    goto :goto_1

    .line 2111
    :pswitch_1
    add-int/lit8 v10, v11, 0x1

    .end local v11    # "validCount":I
    .restart local v10    # "validCount":I
    aput v13, v12, v11

    goto :goto_2

    .line 2115
    .end local v10    # "validCount":I
    .end local v13    # "value":I
    .restart local v11    # "validCount":I
    :cond_2
    if-eqz v11, :cond_0

    .line 2116
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    if-nez v14, :cond_3

    const/4 v3, 0x0

    .line 2117
    :goto_3
    if-nez v3, :cond_4

    array-length v14, v12

    if-ne v11, v14, :cond_4

    .line 2118
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    goto :goto_0

    .line 2116
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    array-length v3, v14

    goto :goto_3

    .line 2120
    :cond_4
    add-int v14, v3, v11

    new-array v7, v14, [I

    .line 2121
    .local v7, "newArray":[I
    if-eqz v3, :cond_5

    .line 2122
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2124
    :cond_5
    const/4 v14, 0x0

    invoke-static {v12, v14, v7, v3, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2125
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    goto :goto_0

    .line 2131
    .end local v3    # "i":I
    .end local v5    # "length":I
    .end local v7    # "newArray":[I
    .end local v11    # "validCount":I
    .end local v12    # "validValues":[I
    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 2132
    .local v2, "bytes":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v6

    .line 2134
    .local v6, "limit":I
    const/4 v1, 0x0

    .line 2135
    .local v1, "arrayLength":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v8

    .line 2136
    .local v8, "startPos":I
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_6

    .line 2137
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v14

    packed-switch v14, :pswitch_data_2

    goto :goto_4

    .line 2158
    :pswitch_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2162
    :cond_6
    if-eqz v1, :cond_a

    .line 2163
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 2164
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    if-nez v14, :cond_8

    const/4 v3, 0x0

    .line 2165
    .restart local v3    # "i":I
    :goto_5
    add-int v14, v3, v1

    new-array v7, v14, [I

    .line 2166
    .restart local v7    # "newArray":[I
    if-eqz v3, :cond_7

    .line 2167
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v14, v15, v7, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2169
    :cond_7
    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v14

    if-lez v14, :cond_9

    .line 2170
    invoke-virtual/range {p1 .. p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v13

    .line 2171
    .restart local v13    # "value":I
    packed-switch v13, :pswitch_data_3

    goto :goto_6

    .line 2192
    :pswitch_3
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    aput v13, v7, v3

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_6

    .line 2164
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    .end local v13    # "value":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    array-length v3, v14

    goto :goto_5

    .line 2196
    .restart local v3    # "i":I
    .restart local v7    # "newArray":[I
    :cond_9
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    .line 2198
    .end local v3    # "i":I
    .end local v7    # "newArray":[I
    :cond_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 2057
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch

    .line 2068
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 2090
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 2137
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 2171
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1974
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2020
    iget v1, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->billingAddressType:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->hasBillingAddressType:Z

    if-eqz v1, :cond_1

    .line 2021
    :cond_0
    iget v1, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->billingAddressType:I

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2023
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    array-length v1, v1

    if-lez v1, :cond_2

    .line 2024
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 2025
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2024
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2028
    .end local v0    # "i":I
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2029
    return-void
.end method
