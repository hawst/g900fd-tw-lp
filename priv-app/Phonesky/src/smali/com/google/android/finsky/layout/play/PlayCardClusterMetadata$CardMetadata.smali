.class public Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;
.super Ljava/lang/Object;
.source "PlayCardClusterMetadata.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CardMetadata"
.end annotation


# instance fields
.field private final mHSpan:I

.field private final mLayoutId:I

.field private final mThumbnailAspectRatio:F

.field private final mVSpan:I


# direct methods
.method public constructor <init>(IIIF)V
    .locals 0
    .param p1, "layoutId"    # I
    .param p2, "hSpan"    # I
    .param p3, "vSpan"    # I
    .param p4, "thumbnailAspectRatio"    # F

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mLayoutId:I

    .line 85
    iput p2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mHSpan:I

    .line 86
    iput p3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mVSpan:I

    .line 87
    iput p4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mThumbnailAspectRatio:F

    .line 88
    return-void
.end method


# virtual methods
.method public getHSpan()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mHSpan:I

    return v0
.end method

.method public getLayoutId()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mLayoutId:I

    return v0
.end method

.method public getThumbnailAspectRatio()F
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mThumbnailAspectRatio:F

    return v0
.end method

.method public getVSpan()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata$CardMetadata;->mVSpan:I

    return v0
.end method
