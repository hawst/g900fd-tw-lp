.class public final Lcom/google/android/finsky/protos/DocList$Bucket;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Bucket"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocList$Bucket;


# instance fields
.field public analyticsCookie:Ljava/lang/String;

.field public document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

.field public estimatedResults:J

.field public fullContentsListUrl:Ljava/lang/String;

.field public fullContentsUrl:Ljava/lang/String;

.field public hasAnalyticsCookie:Z

.field public hasEstimatedResults:Z

.field public hasFullContentsListUrl:Z

.field public hasFullContentsUrl:Z

.field public hasIconUrl:Z

.field public hasMultiCorpus:Z

.field public hasNextPageUrl:Z

.field public hasOrdered:Z

.field public hasRelevance:Z

.field public hasTitle:Z

.field public iconUrl:Ljava/lang/String;

.field public multiCorpus:Z

.field public nextPageUrl:Ljava/lang/String;

.field public ordered:Z

.field public relevance:D

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 220
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocList$Bucket;->clear()Lcom/google/android/finsky/protos/DocList$Bucket;

    .line 221
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocList$Bucket;
    .locals 2

    .prologue
    .line 165
    sget-object v0, Lcom/google/android/finsky/protos/DocList$Bucket;->_emptyArray:[Lcom/google/android/finsky/protos/DocList$Bucket;

    if-nez v0, :cond_1

    .line 166
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 168
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocList$Bucket;->_emptyArray:[Lcom/google/android/finsky/protos/DocList$Bucket;

    if-nez v0, :cond_0

    .line 169
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocList$Bucket;

    sput-object v0, Lcom/google/android/finsky/protos/DocList$Bucket;->_emptyArray:[Lcom/google/android/finsky/protos/DocList$Bucket;

    .line 171
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocList$Bucket;->_emptyArray:[Lcom/google/android/finsky/protos/DocList$Bucket;

    return-object v0

    .line 171
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocList$Bucket;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 224
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV1$DocV1;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    .line 225
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->multiCorpus:Z

    .line 226
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasMultiCorpus:Z

    .line 227
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->title:Ljava/lang/String;

    .line 228
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasTitle:Z

    .line 229
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->iconUrl:Ljava/lang/String;

    .line 230
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasIconUrl:Z

    .line 231
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsUrl:Ljava/lang/String;

    .line 232
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasFullContentsUrl:Z

    .line 233
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsListUrl:Ljava/lang/String;

    .line 234
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasFullContentsListUrl:Z

    .line 235
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->nextPageUrl:Ljava/lang/String;

    .line 236
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasNextPageUrl:Z

    .line 237
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->relevance:D

    .line 238
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasRelevance:Z

    .line 239
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->estimatedResults:J

    .line 240
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasEstimatedResults:Z

    .line 241
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->analyticsCookie:Ljava/lang/String;

    .line 242
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasAnalyticsCookie:Z

    .line 243
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->ordered:Z

    .line 244
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasOrdered:Z

    .line 245
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->cachedSize:I

    .line 246
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 296
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 297
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 298
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 299
    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    aget-object v0, v3, v1

    .line 300
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    if-eqz v0, :cond_0

    .line 301
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 298
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 306
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasMultiCorpus:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->multiCorpus:Z

    if-eqz v3, :cond_3

    .line 307
    :cond_2
    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->multiCorpus:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 310
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasTitle:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 311
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 314
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasIconUrl:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->iconUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 315
    :cond_6
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->iconUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 318
    :cond_7
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasFullContentsUrl:Z

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 319
    :cond_8
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 322
    :cond_9
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasRelevance:Z

    if-nez v3, :cond_a

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->relevance:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-eqz v3, :cond_b

    .line 324
    :cond_a
    const/4 v3, 0x6

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->relevance:D

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeDoubleSize(ID)I

    move-result v3

    add-int/2addr v2, v3

    .line 327
    :cond_b
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasEstimatedResults:Z

    if-nez v3, :cond_c

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->estimatedResults:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_d

    .line 328
    :cond_c
    const/4 v3, 0x7

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->estimatedResults:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 331
    :cond_d
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasAnalyticsCookie:Z

    if-nez v3, :cond_e

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->analyticsCookie:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 332
    :cond_e
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->analyticsCookie:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 335
    :cond_f
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasFullContentsListUrl:Z

    if-nez v3, :cond_10

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsListUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_11

    .line 336
    :cond_10
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsListUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 339
    :cond_11
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasNextPageUrl:Z

    if-nez v3, :cond_12

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->nextPageUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    .line 340
    :cond_12
    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->nextPageUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 343
    :cond_13
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasOrdered:Z

    if-nez v3, :cond_14

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->ordered:Z

    if-eqz v3, :cond_15

    .line 344
    :cond_14
    const/16 v3, 0xb

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->ordered:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 347
    :cond_15
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocList$Bucket;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    .line 355
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 356
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 360
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 361
    :sswitch_0
    return-object p0

    .line 366
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 368
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    if-nez v5, :cond_2

    move v1, v4

    .line 369
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    .line 371
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    if-eqz v1, :cond_1

    .line 372
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 374
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 375
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV1$DocV1;-><init>()V

    aput-object v5, v2, v1

    .line 376
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 377
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 374
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 368
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    array-length v1, v5

    goto :goto_1

    .line 380
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV1$DocV1;-><init>()V

    aput-object v5, v2, v1

    .line 381
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 382
    iput-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    goto :goto_0

    .line 386
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->multiCorpus:Z

    .line 387
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasMultiCorpus:Z

    goto :goto_0

    .line 391
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->title:Ljava/lang/String;

    .line 392
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasTitle:Z

    goto :goto_0

    .line 396
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->iconUrl:Ljava/lang/String;

    .line 397
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasIconUrl:Z

    goto :goto_0

    .line 401
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsUrl:Ljava/lang/String;

    .line 402
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasFullContentsUrl:Z

    goto :goto_0

    .line 406
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readDouble()D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->relevance:D

    .line 407
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasRelevance:Z

    goto :goto_0

    .line 411
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->estimatedResults:J

    .line 412
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasEstimatedResults:Z

    goto/16 :goto_0

    .line 416
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->analyticsCookie:Ljava/lang/String;

    .line 417
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasAnalyticsCookie:Z

    goto/16 :goto_0

    .line 421
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsListUrl:Ljava/lang/String;

    .line 422
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasFullContentsListUrl:Z

    goto/16 :goto_0

    .line 426
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->nextPageUrl:Ljava/lang/String;

    .line 427
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasNextPageUrl:Z

    goto/16 :goto_0

    .line 431
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->ordered:Z

    .line 432
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasOrdered:Z

    goto/16 :goto_0

    .line 356
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x31 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 159
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocList$Bucket;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocList$Bucket;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 252
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 253
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 254
    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->document:[Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    aget-object v0, v2, v1

    .line 255
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    if-eqz v0, :cond_0

    .line 256
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 253
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 260
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV1$DocV1;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasMultiCorpus:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->multiCorpus:Z

    if-eqz v2, :cond_3

    .line 261
    :cond_2
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->multiCorpus:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 263
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasTitle:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 264
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 266
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasIconUrl:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->iconUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 267
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->iconUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 269
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasFullContentsUrl:Z

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 270
    :cond_8
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 272
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasRelevance:Z

    if-nez v2, :cond_a

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->relevance:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    .line 274
    :cond_a
    const/4 v2, 0x6

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->relevance:D

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeDouble(ID)V

    .line 276
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasEstimatedResults:Z

    if-nez v2, :cond_c

    iget-wide v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->estimatedResults:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_d

    .line 277
    :cond_c
    const/4 v2, 0x7

    iget-wide v4, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->estimatedResults:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 279
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasAnalyticsCookie:Z

    if-nez v2, :cond_e

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->analyticsCookie:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 280
    :cond_e
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->analyticsCookie:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 282
    :cond_f
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasFullContentsListUrl:Z

    if-nez v2, :cond_10

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsListUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 283
    :cond_10
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->fullContentsListUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 285
    :cond_11
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasNextPageUrl:Z

    if-nez v2, :cond_12

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->nextPageUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 286
    :cond_12
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->nextPageUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 288
    :cond_13
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->hasOrdered:Z

    if-nez v2, :cond_14

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->ordered:Z

    if-eqz v2, :cond_15

    .line 289
    :cond_14
    const/16 v2, 0xb

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DocList$Bucket;->ordered:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 291
    :cond_15
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 292
    return-void
.end method
