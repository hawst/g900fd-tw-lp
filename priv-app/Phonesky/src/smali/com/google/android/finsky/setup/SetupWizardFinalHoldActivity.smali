.class public Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;
.super Landroid/app/Activity;
.source "SetupWizardFinalHoldActivity.java"

# interfaces
.implements Lcom/google/android/finsky/services/SetupHoldListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;
    }
.end annotation


# static fields
.field public static final SETUP_WIZARD_RESTORE_FINAL_HOLD_HEADER_LANDSCAPE_URL:Ljava/lang/String;

.field public static final SETUP_WIZARD_RESTORE_FINAL_HOLD_HEADER_TABLET_URL:Ljava/lang/String;

.field public static final SETUP_WIZARD_RESTORE_FINAL_HOLD_HEADER_URL:Ljava/lang/String;

.field private static final SETUP_WIZARD_RESTORE_FINAL_HOLD_LONG_MS:J

.field private static final SETUP_WIZARD_RESTORE_FINAL_HOLD_SHORT_MS:J


# instance fields
.field private mAnimator:Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;

.field private mHandler:Landroid/os/Handler;

.field private mRegisteredWithRestoreService:Z

.field private mRegisteredWithVpaService:Z

.field private mWatchdogExpirationRealtimeMs:J

.field private mWatchdogLastPackage:Ljava/lang/String;

.field private mWatchdogRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldHeaderUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_HEADER_URL:Ljava/lang/String;

    .line 41
    sget-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldHeaderLandscapeUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_HEADER_LANDSCAPE_URL:Ljava/lang/String;

    .line 44
    sget-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldHeaderTabletUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_HEADER_TABLET_URL:Ljava/lang/String;

    .line 54
    sget-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldShortMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_SHORT_MS:J

    .line 57
    sget-object v0, Lcom/google/android/finsky/config/G;->setupWizardRestoreFinalHoldLongMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_LONG_MS:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mHandler:Landroid/os/Handler;

    .line 313
    new-instance v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$3;-><init>(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;)V

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogRunnable:Ljava/lang/Runnable;

    .line 333
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->unregisterListeners()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->registerListener()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;
    .param p1, "x1"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setResultAndFinish(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogLastPackage:Ljava/lang/String;

    return-object v0
.end method

.method public static createIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 77
    .local v0, "intent":Landroid/content/Intent;
    return-object v0
.end method

.method private finishIfTimeout()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 301
    sget-wide v2, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_SHORT_MS:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    .line 309
    :cond_0
    :goto_0
    return v0

    .line 305
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogExpirationRealtimeMs:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 307
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private registerListener()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 200
    invoke-static {p0}, Lcom/google/android/finsky/services/RestoreService;->registerListener(Lcom/google/android/finsky/services/SetupHoldListener;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 201
    iput-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mRegisteredWithRestoreService:Z

    .line 208
    :goto_0
    return v0

    .line 204
    :cond_0
    invoke-static {p0}, Lcom/google/android/finsky/services/VpaService;->registerListener(Lcom/google/android/finsky/services/SetupHoldListener;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 205
    iput-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mRegisteredWithVpaService:Z

    goto :goto_0

    .line 208
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setResultAndFinish(I)V
    .locals 2
    .param p1, "result"    # I

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 188
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setResult(I)V

    .line 190
    const-string v0, "Calling finish from final hold."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->finish()V

    .line 192
    return-void
.end method

.method private setWatchdog(JLjava/lang/String;)V
    .locals 5
    .param p1, "delayMs"    # J
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 279
    sget-wide v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_SHORT_MS:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 281
    const-string v0, "Watchdog disabled"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 289
    :goto_0
    return-void

    .line 284
    :cond_0
    const-string v0, "Set watchdog to %d sec (package %s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogExpirationRealtimeMs:J

    .line 286
    iput-object p3, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogLastPackage:Ljava/lang/String;

    .line 287
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private unregisterListeners()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 213
    iget-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mRegisteredWithVpaService:Z

    if-eqz v0, :cond_0

    .line 214
    invoke-static {v2}, Lcom/google/android/finsky/services/VpaService;->registerListener(Lcom/google/android/finsky/services/SetupHoldListener;)Z

    .line 215
    iput-boolean v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mRegisteredWithVpaService:Z

    .line 217
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mRegisteredWithRestoreService:Z

    if-eqz v0, :cond_1

    .line 218
    invoke-static {v2}, Lcom/google/android/finsky/services/RestoreService;->registerListener(Lcom/google/android/finsky/services/SetupHoldListener;)Z

    .line 219
    iput-boolean v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mRegisteredWithRestoreService:Z

    .line 221
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x0

    .line 83
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 85
    .local v6, "inflater":Landroid/view/LayoutInflater;
    const v10, 0x7f0401a2

    invoke-virtual {v6, v10, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 86
    .local v7, "mainView":Landroid/view/ViewGroup;
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setContentView(Landroid/view/View;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->getWindow()Landroid/view/Window;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 90
    .local v0, "decorView":Landroid/view/View;
    const/16 v5, 0x1602

    .line 96
    .local v5, "immersiveVis":I
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v10

    new-instance v11, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$1;

    invoke-direct {v11, p0, v0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$1;-><init>(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;Landroid/view/View;)V

    invoke-virtual {v10, v11}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 107
    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v10

    or-int/lit16 v9, v10, 0x1602

    .line 108
    .local v9, "vis":I
    invoke-virtual {v0, v9}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 110
    const v10, 0x7f0a0389

    invoke-virtual {p0, v10}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/image/FifeImageView;

    .line 113
    .local v1, "headerImageView":Lcom/google/android/play/image/FifeImageView;
    new-instance v11, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;

    const v10, 0x7f0a038b

    invoke-virtual {p0, v10}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    invoke-direct {v11, v10}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;-><init>(Landroid/widget/TextView;)V

    iput-object v11, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mAnimator:Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;

    .line 117
    sget-object v10, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v10}, Lcom/google/android/play/image/FifeImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 119
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0f0010

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    .line 120
    .local v8, "useWideImage":Z
    if-eqz v8, :cond_1

    .line 121
    sget-object v4, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_HEADER_TABLET_URL:Ljava/lang/String;

    .line 130
    .local v4, "imageUrl":Ljava/lang/String;
    :goto_0
    const/4 v10, 0x1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v11

    invoke-virtual {v1, v4, v10, v11}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 132
    if-nez p1, :cond_3

    .line 134
    sget-wide v10, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_SHORT_MS:J

    invoke-direct {p0, v10, v11, v12}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setWatchdog(JLjava/lang/String;)V

    .line 145
    :cond_0
    :goto_1
    return-void

    .line 123
    .end local v4    # "imageUrl":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v10

    iget v10, v10, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_2

    .line 125
    sget-object v4, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_HEADER_LANDSCAPE_URL:Ljava/lang/String;

    .restart local v4    # "imageUrl":Ljava/lang/String;
    goto :goto_0

    .line 127
    .end local v4    # "imageUrl":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_HEADER_URL:Ljava/lang/String;

    .restart local v4    # "imageUrl":Ljava/lang/String;
    goto :goto_0

    .line 137
    :cond_3
    const-string v10, "watchdog_expiration_ms"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogExpirationRealtimeMs:J

    .line 138
    const-string v10, "watchdog_package"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogLastPackage:Ljava/lang/String;

    .line 139
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->finishIfTimeout()Z

    move-result v10

    if-nez v10, :cond_0

    .line 141
    iget-wide v10, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogExpirationRealtimeMs:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v12

    sub-long v2, v10, v12

    .line 142
    .local v2, "delayMs":J
    iget-object v10, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogLastPackage:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v10}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setWatchdog(JLjava/lang/String;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 152
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 179
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 180
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mAnimator:Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;

    invoke-virtual {v0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->stop()V

    .line 181
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->unregisterListeners()V

    .line 182
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 162
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 164
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->finishIfTimeout()Z

    .line 166
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mAnimator:Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;

    invoke-virtual {v0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$RestoringTitleAnimator;->start()V

    .line 169
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->registerListener()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setResultAndFinish(I)V

    .line 175
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 156
    const-string v0, "watchdog_expiration_ms"

    iget-wide v2, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogExpirationRealtimeMs:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 157
    const-string v0, "watchdog_package"

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogLastPackage:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public onStatusChange(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 5
    .param p1, "eventCode"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "cancelable"    # Z
    .param p5, "logString"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 226
    const-string v1, "Final hold status change: listener=%s code=%d package=%s cancelable=%b (%s)"

    const/4 v0, 0x5

    new-array v2, v0, [Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mRegisteredWithVpaService:Z

    if-eqz v0, :cond_1

    const-string v0, "VPA"

    :goto_0
    aput-object v0, v2, v4

    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    aput-object p2, v2, v0

    const/4 v0, 0x3

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    aput-object p5, v2, v0

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    packed-switch p1, :pswitch_data_0

    .line 266
    const-string v0, "Unknown event code - finishing early"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setResultAndFinish(I)V

    .line 270
    :cond_0
    :goto_1
    return-void

    .line 226
    :cond_1
    const-string v0, "Restore"

    goto :goto_0

    .line 239
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$2;-><init>(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 251
    :pswitch_1
    sget-wide v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_SHORT_MS:J

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setWatchdog(JLjava/lang/String;)V

    goto :goto_1

    .line 257
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogLastPackage:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogLastPackage:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 258
    :cond_2
    sget-wide v0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->SETUP_WIZARD_RESTORE_FINAL_HOLD_LONG_MS:J

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setWatchdog(JLjava/lang/String;)V

    goto :goto_1

    .line 229
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
