.class public Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;
.super Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;
.source "DiscoveryBadgeSocialPlusOne.java"


# instance fields
.field private mPlusOneBackground:Landroid/graphics/drawable/GradientDrawable;

.field private mPlusOneContainer:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->mPlusOneBackground:Landroid/graphics/drawable/GradientDrawable;

    .line 41
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->setWillNotDraw(Z)V

    .line 42
    return-void
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1, "badge"    # Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p4, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p6, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 61
    invoke-super/range {p0 .. p7}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->bind(Lcom/google/android/finsky/protos/Details$DiscoveryBadge;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->mPlusOneBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0900d4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 63
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->mPlusOneContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->mPlusOneBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 70
    :goto_0
    const v0, 0x7f0a01ad

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 71
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->mPlusOneContainer:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->mPlusOneBackground:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 75
    const/16 v0, 0x70c

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeBase;->onFinishInflate()V

    .line 47
    const v0, 0x7f0a01ac

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/DiscoveryBadgeSocialPlusOne;->mPlusOneContainer:Landroid/view/View;

    .line 48
    return-void
.end method
