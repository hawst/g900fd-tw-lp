.class public Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;
.super Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
.source "PlayCardTrustedSourceClusterViewContent.java"


# instance fields
.field private mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field private mHasTallCover:Z

.field private mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

.field private mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

.field private mProfileCoverImageFrame:Landroid/widget/FrameLayout;

.field private mProfileInfoBlock:Landroid/view/View;

.field private mProfileSubtitle:Landroid/widget/TextView;

.field private mProfileTitle:Landroid/widget/TextView;

.field private final mShouldLayoutVertically:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 63
    .local v0, "resources":Landroid/content/res/Resources;
    const v1, 0x7f0f000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mShouldLayoutVertically:Z

    .line 65
    return-void
.end method


# virtual methods
.method public configurePersonProfile(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 8
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "clusterNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v7, 0x0

    .line 112
    iput-object p3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 114
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c03b3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 117
    .local v1, "profileClickDescription":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mDocument:Lcom/google/android/finsky/api/model/Document;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Common$Image;

    .line 118
    .local v0, "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v4, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v5, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v3, v4, v5, p2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 119
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v3, v1}, Lcom/google/android/play/image/FifeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 120
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {p1, v3, p4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v2

    .line 122
    .local v2, "profileClickListener":Landroid/view/View$OnClickListener;
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v3, v2}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mDocument:Lcom/google/android/finsky/api/model/Document;

    const/16 v4, 0xf

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/api/model/Document;->getImages(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "image":Lcom/google/android/finsky/protos/Common$Image;
    check-cast v0, Lcom/google/android/finsky/protos/Common$Image;

    .line 124
    .restart local v0    # "image":Lcom/google/android/finsky/protos/Common$Image;
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v4, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v5, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v3, v4, v5, p2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 125
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v3, v2}, Lcom/google/android/play/image/FifeImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v3, v1}, Lcom/google/android/play/image/FifeImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 127
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileTitle:Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileSubtitle:Landroid/widget/TextView;

    invoke-virtual {p3}, Lcom/google/android/finsky/api/model/Document;->getSubtitle()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p3, v4, p4}, Lcom/google/android/finsky/layout/play/PlayCirclesButton;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 131
    return-void
.end method

.method protected getIndexOfFirstCard()I
    .locals 1

    .prologue
    .line 104
    const/4 v0, 0x2

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onFinishInflate()V

    .line 71
    const v0, 0x7f0a02b8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    .line 72
    const v0, 0x7f0a02b9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    .line 74
    const v0, 0x7f0a02ba

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    .line 75
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const v1, 0x7f0a02dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    .line 76
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const v1, 0x7f0a02bc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileTitle:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const v1, 0x7f0a02bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileSubtitle:Landroid/widget/TextView;

    .line 78
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const v1, 0x7f0a02be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCirclesButton:Lcom/google/android/finsky/layout/play/PlayCirclesButton;

    .line 80
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v4, 0x0

    .line 227
    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onLayout(ZIIII)V

    .line 229
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->getWidth()I

    move-result v5

    .line 231
    .local v5, "width":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 232
    .local v3, "infoBlockWidth":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 234
    .local v2, "infoBlockHeight":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v6}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v0

    .line 236
    .local v0, "avatarImageHeight":I
    iget v6, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCardContentPaddingTop:I

    int-to-float v7, v0

    const/high16 v8, 0x3f000000    # 0.5f

    mul-float/2addr v7, v8

    float-to-int v7, v7

    add-int v1, v6, v7

    .line 238
    .local v1, "coverImageTop":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {v6, v4, v1, v5, v7}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 241
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mShouldLayoutVertically:Z

    if-eqz v6, :cond_0

    .line 242
    .local v4, "infoBlockX":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    iget v7, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCardContentPaddingTop:I

    add-int v8, v4, v3

    iget v9, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCardContentPaddingTop:I

    add-int/2addr v9, v2

    invoke-virtual {v6, v4, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 245
    return-void

    .line 241
    .end local v4    # "infoBlockX":I
    :cond_0
    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCardContentHorizontalPadding:I

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 17
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 135
    invoke-super/range {p0 .. p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onMeasure(II)V

    .line 137
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v13

    .line 138
    .local v13, "width":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->getMeasuredHeight()I

    move-result v8

    .line 139
    .local v8, "measuredHeight":I
    if-nez v8, :cond_1

    const/4 v2, 0x0

    .line 141
    .local v2, "cardContentHeight":I
    :goto_0
    move v12, v2

    .line 147
    .local v12, "totalHeight":I
    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 149
    .local v5, "fullWidthSpec":I
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mShouldLayoutVertically:Z

    if-eqz v14, :cond_2

    .line 150
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const/4 v15, 0x0

    invoke-virtual {v14, v5, v15}, Landroid/view/View;->measure(II)V

    .line 152
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    .line 157
    .local v11, "profileInfoBlockHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v14}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v14

    int-to-float v14, v14

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    float-to-int v14, v14

    sub-int v14, v11, v14

    int-to-float v15, v2

    const v16, 0x3f59999a    # 0.85f

    mul-float v15, v15, v16

    float-to-int v15, v15

    add-int v10, v14, v15

    .line 160
    .local v10, "profileImageHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v10, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v14, v5, v15}, Landroid/widget/FrameLayout;->measure(II)V

    .line 163
    add-int/2addr v12, v11

    .line 219
    .end local v11    # "profileInfoBlockHeight":I
    :goto_1
    if-lez v2, :cond_0

    .line 220
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCardContentPaddingTop:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCardContentPaddingBottom:I

    add-int/2addr v14, v15

    add-int/2addr v12, v14

    .line 222
    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->setMeasuredDimension(II)V

    .line 223
    return-void

    .line 139
    .end local v2    # "cardContentHeight":I
    .end local v5    # "fullWidthSpec":I
    .end local v10    # "profileImageHeight":I
    .end local v12    # "totalHeight":I
    :cond_1
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCardContentPaddingTop:I

    sub-int v14, v8, v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mCardContentPaddingBottom:I

    sub-int v2, v14, v15

    goto :goto_0

    .line 169
    .restart local v2    # "cardContentHeight":I
    .restart local v5    # "fullWidthSpec":I
    .restart local v12    # "totalHeight":I
    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->getLeadingGap(I)I

    move-result v14

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->getExtraColumnOffset()I

    move-result v15

    int-to-float v15, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->getCellSize(I)F

    move-result v16

    mul-float v15, v15, v16

    float-to-int v15, v15

    add-int v4, v14, v15

    .line 171
    .local v4, "cardContentLeadingGap":I
    const/high16 v14, 0x40000000    # 2.0f

    invoke-static {v4, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 173
    .local v7, "infoBlockWidthSpec":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const/4 v15, 0x0

    invoke-virtual {v14, v7, v15}, Landroid/view/View;->measure(II)V

    .line 175
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v14}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v1

    .line 180
    .local v1, "avatarImageHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    int-to-float v15, v1

    const/high16 v16, 0x3f000000    # 0.5f

    mul-float v15, v15, v16

    float-to-int v15, v15

    sub-int v9, v14, v15

    .line 184
    .local v9, "profileContentHeightUnderProfileImage":I
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mHasTallCover:Z

    if-eqz v14, :cond_3

    .line 189
    int-to-float v14, v2

    const v15, 0x3f59999a    # 0.85f

    mul-float/2addr v14, v15

    float-to-int v14, v14

    int-to-float v15, v1

    const/high16 v16, 0x3f000000    # 0.5f

    mul-float v15, v15, v16

    float-to-int v15, v15

    sub-int v3, v14, v15

    .line 202
    .local v3, "cardContentHeightOverProfileImage":I
    :goto_2
    invoke-static {v9, v3}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 204
    .restart local v10    # "profileImageHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImageFrame:Landroid/widget/FrameLayout;

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v10, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v14, v5, v15}, Landroid/widget/FrameLayout;->measure(II)V

    .line 210
    int-to-float v14, v1

    const/high16 v15, 0x3f000000    # 0.5f

    mul-float/2addr v14, v15

    float-to-int v14, v14

    add-int v6, v10, v14

    .line 212
    .local v6, "infoBlockFinalHeight":I
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileInfoBlock:Landroid/view/View;

    const/high16 v15, 0x40000000    # 2.0f

    invoke-static {v6, v15}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    invoke-virtual {v14, v7, v15}, Landroid/view/View;->measure(II)V

    .line 215
    int-to-float v14, v2

    const v15, 0x3e199998    # 0.14999998f

    mul-float/2addr v14, v15

    float-to-int v14, v14

    add-int v12, v6, v14

    goto/16 :goto_1

    .line 197
    .end local v3    # "cardContentHeightOverProfileImage":I
    .end local v6    # "infoBlockFinalHeight":I
    .end local v10    # "profileImageHeight":I
    :cond_3
    int-to-float v14, v1

    const/4 v15, 0x0

    mul-float/2addr v14, v15

    float-to-int v14, v14

    int-to-float v15, v2

    const v16, 0x3f59999a    # 0.85f

    mul-float v15, v15, v16

    float-to-int v15, v15

    add-int v3, v14, v15

    .restart local v3    # "cardContentHeightOverProfileImage":I
    goto :goto_2
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 249
    invoke-super {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->onRecycle()V

    .line 250
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileAvatarImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 251
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mProfileCoverImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 252
    return-void
.end method

.method public setMetadata(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;)V
    .locals 5
    .param p1, "metadata"    # Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    .param p2, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;

    .prologue
    .line 85
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setMetadata(Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;Lcom/google/android/finsky/utils/ClientMutationCache;)V

    .line 89
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mMetadata:Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getTileCount()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->getDocCount()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 90
    .local v2, "visibleCardCount":I
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mHasTallCover:Z

    .line 91
    const/4 v1, 0x0

    .local v1, "tileIndex":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 92
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->getDoc(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 93
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getAspectRatio(I)F

    move-result v3

    const v4, 0x3fb872b0    # 1.441f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_1

    .line 95
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardTrustedSourceClusterViewContent;->mHasTallCover:Z

    .line 99
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    return-void

    .line 91
    .restart local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
