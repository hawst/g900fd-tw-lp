.class Lcom/google/android/finsky/services/MarketCatalogService$1;
.super Lcom/google/android/finsky/services/IMarketCatalogService$Stub;
.source "MarketCatalogService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/services/MarketCatalogService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/MarketCatalogService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/MarketCatalogService;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/google/android/finsky/services/MarketCatalogService$1;->this$0:Lcom/google/android/finsky/services/MarketCatalogService;

    invoke-direct {p0}, Lcom/google/android/finsky/services/IMarketCatalogService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public isBackendEnabled(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "backendId"    # I

    .prologue
    const/4 v3, 0x0

    .line 28
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 31
    new-instance v2, Ljava/util/concurrent/Semaphore;

    invoke-direct {v2, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 32
    .local v2, "sem":Ljava/util/concurrent/Semaphore;
    const/4 v4, 0x1

    new-array v1, v4, [Z

    .line 34
    .local v1, "enabled":[Z
    iget-object v4, p0, Lcom/google/android/finsky/services/MarketCatalogService$1;->this$0:Lcom/google/android/finsky/services/MarketCatalogService;

    invoke-static {p1, v4}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 37
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    .line 64
    :goto_0
    return v3

    .line 41
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v4

    new-instance v5, Lcom/google/android/finsky/services/MarketCatalogService$1$1;

    invoke-direct {v5, p0, p2, v1, v2}, Lcom/google/android/finsky/services/MarketCatalogService$1$1;-><init>(Lcom/google/android/finsky/services/MarketCatalogService$1;I[ZLjava/util/concurrent/Semaphore;)V

    invoke-static {v4, v3, v5}, Lcom/google/android/finsky/utils/GetTocHelper;->getToc(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/GetTocHelper$Listener;)V

    .line 61
    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 63
    iget-object v4, p0, Lcom/google/android/finsky/services/MarketCatalogService$1;->this$0:Lcom/google/android/finsky/services/MarketCatalogService;

    invoke-virtual {v4}, Lcom/google/android/finsky/services/MarketCatalogService;->stopSelf()V

    .line 64
    aget-boolean v3, v1, v3

    goto :goto_0
.end method
