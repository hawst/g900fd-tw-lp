.class Lcom/google/android/finsky/layout/DetailsColumnLayout$8;
.super Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;
.source "DetailsColumnLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsColumnLayout;->collapseCurrentlyExpandedSectionPreIcs(Lcom/google/android/finsky/layout/DetailsTextSection;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsColumnLayout;)V
    .locals 0

    .prologue
    .line 437
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$8;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    invoke-direct {p0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$8;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsContainer:Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->setSeparatorsVisible(Z)V

    .line 448
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$8;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedFrame:Lcom/google/android/finsky/layout/DetailsExpandedFrame;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsExpandedFrame;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$8;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsExpandedContainer:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->resetContent()V

    .line 450
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsColumnLayout$8;->this$0:Lcom/google/android/finsky/layout/DetailsColumnLayout;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsColumnLayout;->mDetailsScroller:Landroid/widget/ScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 441
    return-void
.end method
