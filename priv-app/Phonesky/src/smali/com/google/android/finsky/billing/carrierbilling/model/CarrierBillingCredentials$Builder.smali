.class public Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;
.super Ljava/lang/Object;
.source "CarrierBillingCredentials.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private apiVersion:I

.field private credentials:Ljava/lang/String;

.field private expirationTime:J

.field private invalidPassword:Z

.field private isProvisioned:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;)V
    .locals 2
    .param p1, "oldCreds"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    # getter for: Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->mApiVersion:I
    invoke-static {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->access$500(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->apiVersion:I

    .line 90
    # getter for: Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->mCredentials:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->access$600(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->credentials:Ljava/lang/String;

    .line 91
    # getter for: Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->mExpirationTime:J
    invoke-static {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->access$700(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->expirationTime:J

    .line 92
    # getter for: Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->mIsProvisioned:Z
    invoke-static {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->access$800(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->isProvisioned:Z

    .line 93
    # getter for: Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->mInvalidPassword:Z
    invoke-static {p1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;->access$900(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->invalidPassword:Z

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->apiVersion:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->credentials:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->expirationTime:J

    return-wide v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->isProvisioned:Z

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->invalidPassword:Z

    return v0
.end method


# virtual methods
.method public build()Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;
    .locals 2

    .prologue
    .line 119
    new-instance v0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials;-><init>(Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$1;)V

    return-object v0
.end method

.method public setApiVersion(I)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;
    .locals 0
    .param p1, "apiVersion"    # I

    .prologue
    .line 97
    iput p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->apiVersion:I

    .line 98
    return-object p0
.end method

.method public setCredentials(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;
    .locals 0
    .param p1, "credentials"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->credentials:Ljava/lang/String;

    .line 103
    return-object p0
.end method

.method public setExpirationTime(J)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;
    .locals 1
    .param p1, "expirationTime"    # J

    .prologue
    .line 106
    iput-wide p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->expirationTime:J

    .line 107
    return-object p0
.end method

.method public setInvalidPassword(Z)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;
    .locals 0
    .param p1, "invalidPassword"    # Z

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->invalidPassword:Z

    .line 115
    return-object p0
.end method

.method public setIsProvisioned(Z)Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;
    .locals 0
    .param p1, "isProvisioned"    # Z

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/google/android/finsky/billing/carrierbilling/model/CarrierBillingCredentials$Builder;->isProvisioned:Z

    .line 111
    return-object p0
.end method
