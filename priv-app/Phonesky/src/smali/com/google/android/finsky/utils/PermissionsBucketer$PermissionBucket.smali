.class public Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
.super Ljava/lang/Object;
.source "PermissionsBucketer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/utils/PermissionsBucketer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PermissionBucket"
.end annotation


# instance fields
.field public final mBucketDescription:I

.field public final mBucketIcon:I

.field public final mBucketId:I

.field public final mBucketTitle:I

.field public final mPermissionDescriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(IIII)V
    .locals 1
    .param p1, "bucketId"    # I
    .param p2, "bucketIcon"    # I
    .param p3, "bucketTitle"    # I
    .param p4, "bucketDescription"    # I

    .prologue
    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mPermissionDescriptions:Ljava/util/List;

    .line 229
    iput p1, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketId:I

    .line 230
    iput p2, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketIcon:I

    .line 231
    iput p3, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketTitle:I

    .line 232
    iput p4, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketDescription:I

    .line 233
    return-void
.end method


# virtual methods
.method public addPermission(Ljava/lang/String;)V
    .locals 1
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mPermissionDescriptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    return-void
.end method
