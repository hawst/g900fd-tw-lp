.class public Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;
.super Ljava/lang/Object;
.source "AndroidKeyczarReader.java"

# interfaces
.implements Lorg/keyczar/interfaces/KeyczarReader;


# static fields
.field private static final CHARSET_UTF8:Ljava/nio/charset/Charset;


# instance fields
.field private final mAssetManager:Landroid/content/res/AssetManager;

.field private final mSubDirectory:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->CHARSET_UTF8:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Ljava/lang/String;)V
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "assetSubDirectory"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    invoke-virtual {p1}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->mAssetManager:Landroid/content/res/AssetManager;

    .line 60
    iput-object p2, p0, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->mSubDirectory:Ljava/lang/String;

    .line 61
    return-void
.end method

.method private getFileContentAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/keyczar/exceptions/KeyczarException;
        }
    .end annotation

    .prologue
    .line 91
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/16 v6, 0x400

    new-array v0, v6, [C

    .line 93
    .local v0, "buf":[C
    iget-object v6, p0, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->mAssetManager:Landroid/content/res/AssetManager;

    invoke-direct {p0, p1}, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->getFullFilename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 94
    .local v2, "is":Ljava/io/InputStream;
    new-instance v3, Ljava/io/InputStreamReader;

    sget-object v6, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->CHARSET_UTF8:Ljava/nio/charset/Charset;

    invoke-direct {v3, v2, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    .line 96
    .local v3, "isr":Ljava/io/InputStreamReader;
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStreamReader;->read([C)I

    move-result v4

    .local v4, "numRead":I
    if-lez v4, :cond_0

    .line 97
    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6, v4}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 100
    .end local v0    # "buf":[C
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v3    # "isr":Ljava/io/InputStreamReader;
    .end local v4    # "numRead":I
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v1

    .line 101
    .local v1, "e":Ljava/io/IOException;
    new-instance v6, Lorg/keyczar/exceptions/KeyczarException;

    const-string v7, "Couldn\'t read Keyczar \'meta\' file from assets/"

    invoke-direct {v6, v7, v1}, Lorg/keyczar/exceptions/KeyczarException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 99
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "buf":[C
    .restart local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "isr":Ljava/io/InputStreamReader;
    .restart local v4    # "numRead":I
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    return-object v6
.end method

.method private getFullFilename(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->mSubDirectory:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 110
    .end local p1    # "filename":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "filename":Ljava/lang/String;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->mSubDirectory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public getKey(I)Ljava/lang/String;
    .locals 1
    .param p1, "keyVersion"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/keyczar/exceptions/KeyczarException;
        }
    .end annotation

    .prologue
    .line 78
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->getFileContentAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMetadata()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/keyczar/exceptions/KeyczarException;
        }
    .end annotation

    .prologue
    .line 86
    const-string v0, "meta"

    invoke-direct {p0, v0}, Lcom/google/android/finsky/api/utils/AndroidKeyczarReader;->getFileContentAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
