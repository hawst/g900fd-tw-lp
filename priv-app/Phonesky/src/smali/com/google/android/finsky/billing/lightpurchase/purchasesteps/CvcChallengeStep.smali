.class public Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "CvcChallengeStep.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

.field private mCodeEntry:Landroid/widget/EditText;

.field private mCreditCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

.field private mMainView:Landroid/view/View;

.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 45
    const/16 v0, 0x4f6

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->syncContinueButtonState()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;)Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;
    .param p1, "x1"    # Z

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->handleClick(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method private handleClick(Z)V
    .locals 4
    .param p1, "isImeAction"    # Z

    .prologue
    const/4 v1, 0x1

    .line 169
    const/4 v0, 0x0

    .line 170
    .local v0, "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    if-eqz p1, :cond_0

    .line 171
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .end local v0    # "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 172
    .restart local v0    # "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    .line 173
    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasIsImeAction:Z

    .line 175
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    const/16 v2, 0x4f7

    invoke-virtual {v1, v2, v0, p0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->logClick(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->escrowHandleParam:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->escrowCvcCode(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "challenge"    # Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    .prologue
    .line 55
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;-><init>()V

    .line 56
    .local v1, "result":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const-string v2, "CvcChallengeStep.challenge"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->setArguments(Landroid/os/Bundle;)V

    .line 60
    return-object v1
.end method

.method private syncContinueButtonState()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 152
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCreditCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCreditCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    iget v3, v3, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->cvcLength:I

    if-ne v2, v3, :cond_1

    .line 155
    .local v0, "isCvcValid":Z
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->setContinueButtonEnabled(Z)V

    .line 156
    return-void

    .end local v0    # "isCvcValid":Z
    :cond_1
    move v0, v1

    .line 152
    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 160
    const v0, 0x7f0c010c

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onContinueButtonClicked()V
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->handleClick(Z)V

    .line 166
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "CvcChallengeStep.challenge"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    iget v0, v0, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->creditCardType:I

    invoke-static {v0}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->getTypeForProtobufType(I)Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCreditCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .line 68
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 73
    const v3, 0x7f0400c5

    invoke-virtual {p1, v3, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mMainView:Landroid/view/View;

    .line 75
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mMainView:Landroid/view/View;

    const v4, 0x7f0a009c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 76
    .local v2, "title":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->title:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 77
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mMainView:Landroid/view/View;

    const v4, 0x7f0a00c3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 81
    .local v1, "description":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 82
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 84
    invoke-virtual {v1}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 89
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mMainView:Landroid/view/View;

    const v4, 0x7f0a00e3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCodeEntry:Landroid/widget/EditText;

    .line 90
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCodeEntry:Landroid/widget/EditText;

    new-instance v4, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$1;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 104
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCreditCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    if-eqz v3, :cond_1

    .line 106
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCodeEntry:Landroid/widget/EditText;

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/text/InputFilter;

    new-instance v5, Landroid/text/InputFilter$LengthFilter;

    iget-object v6, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCreditCardType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    iget v6, v6, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->cvcLength:I

    invoke-direct {v5, v6}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 109
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCodeEntry:Landroid/widget/EditText;

    new-instance v4, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$2;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$2;-><init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 122
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mMainView:Landroid/view/View;

    const v4, 0x7f0a00e4

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 123
    .local v0, "cvcImage":Landroid/widget/ImageView;
    new-instance v3, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$3;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep$3;-><init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->syncContinueButtonState()V

    .line 141
    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mMainView:Landroid/view/View;

    return-object v3

    .line 86
    .end local v0    # "cvcImage":Landroid/widget/ImageView;
    :cond_2
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 146
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/CvcChallengeStep;->mCodeEntry:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->showKeyboard(Landroid/app/Activity;Landroid/widget/EditText;)V

    .line 148
    return-void
.end method
