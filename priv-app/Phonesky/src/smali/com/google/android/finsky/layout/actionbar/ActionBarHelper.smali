.class public Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;,
        Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;
    }
.end annotation


# static fields
.field private static final IS_SEARCH_ALWAYS_VISIBLE:Z

.field private static sBackgroundCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;>;"
        }
    .end annotation
.end field

.field private static sSawFirstBackstackChange:Z


# instance fields
.field private mActionBar:Landroid/support/v7/app/ActionBar;

.field private mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

.field private mActionBarStateStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;",
            ">;"
        }
    .end annotation
.end field

.field private mActivity:Landroid/app/Activity;

.field private mAutoUpdateItem:Landroid/view/MenuItem;

.field private mCurrentActionBarAlpha:I

.field private mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

.field private mCurrentBackendId:I

.field private mCurrentBackgroundAlpha:I

.field private mCurrentBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mCurrentTitle:Ljava/lang/CharSequence;

.field private mDefaultSearchQuery:Ljava/lang/String;

.field private mEnvironmentItem:Landroid/view/MenuItem;

.field private mExpandedModeTranslatable:Lcom/google/android/finsky/activities/TextSectionTranslatable;

.field private mIgnoreActionBarBackground:Z

.field private final mMultiStatusBarColor:I

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mPrevRecentsBackendForColor:I

.field private mPrevRecentsTitle:Ljava/lang/CharSequence;

.field private mRecentsIcon:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mSearchItem:Landroid/view/MenuItem;

.field private mSearchView:Landroid/support/v7/widget/SearchView;

.field private mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

.field private mTitleAlphaSpan:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;

.field private mTitleSpannableString:Landroid/text/SpannableString;

.field private mTranslateItem:Landroid/view/MenuItem;

.field private final mTransparentBackgroundDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 85
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->IS_SEARCH_ALWAYS_VISIBLE:Z

    .line 100
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sBackgroundCache:Ljava/util/Map;

    .line 127
    sput-boolean v1, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sSawFirstBackstackChange:Z

    return-void

    :cond_0
    move v0, v1

    .line 85
    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Landroid/support/v7/app/ActionBarActivity;)V
    .locals 1
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "parentActivity"    # Landroid/support/v7/app/ActionBarActivity;

    .prologue
    const/4 v0, 0x0

    .line 201
    invoke-direct {p0, p1, v0, p2, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Landroid/support/v7/app/ActionBarActivity;Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V

    .line 202
    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Landroid/support/v7/app/ActionBarActivity;Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V
    .locals 5
    .param p1, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "actionBarController"    # Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .param p3, "parentActivity"    # Landroid/support/v7/app/ActionBarActivity;
    .param p4, "sideDrawerLayout"    # Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .prologue
    const/16 v4, 0xff

    const/4 v3, 0x0

    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    iput v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlpha:I

    .line 177
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mDefaultSearchQuery:Ljava/lang/String;

    .line 182
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mPrevRecentsBackendForColor:I

    .line 207
    invoke-virtual {p3}, Landroid/support/v7/app/ActionBarActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    .line 208
    iput-object p3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    .line 209
    iput-object p4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    .line 211
    iput-object p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 212
    iput-object p2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    .line 215
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    .line 216
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    new-instance v1, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;

    const/4 v2, 0x0

    invoke-direct {v1, v3, v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;-><init>(ILjava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    iput v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    .line 219
    iput v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundAlpha:I

    .line 221
    invoke-static {p3, v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getStatusBarColor(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mMultiStatusBarColor:I

    .line 224
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v1, v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->getBackgroundColorDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 229
    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTransparentBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 231
    new-instance v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;

    invoke-virtual {p3}, Landroid/support/v7/app/ActionBarActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleAlphaSpan:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;

    .line 233
    new-instance v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$1;-><init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 244
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 77
    sget-boolean v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sSawFirstBackstackChange:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 77
    sput-boolean p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sSawFirstBackstackChange:Z

    return p0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->clearSearch()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncState()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Lcom/google/android/finsky/layout/actionbar/ActionBarController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mDefaultSearchQuery:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Landroid/support/v7/widget/SearchView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchView:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method private clearSearch()V
    .locals 3

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchView:Landroid/support/v7/widget/SearchView;

    if-nez v0, :cond_0

    .line 549
    :goto_0
    return-void

    .line 546
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchView:Landroid/support/v7/widget/SearchView;

    const-string v1, ""

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 547
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchView:Landroid/support/v7/widget/SearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/SearchView;->setIconified(Z)V

    .line 548
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->collapseActionView(Landroid/view/MenuItem;)Z

    goto :goto_0
.end method

.method private enterActionBarTransientOpacityMode(Landroid/view/View;ILjava/lang/CharSequence;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "newMode"    # I
    .param p3, "newTitle"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v1, 0x1

    .line 650
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->isInMode(Ljava/lang/Integer;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 678
    :cond_0
    :goto_0
    return-void

    .line 653
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v2

    if-ne v2, v1, :cond_2

    .line 654
    .local v1, "wasInDefaultMode":Z
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    new-instance v3, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;

    invoke-direct {v3, p2, p3}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;-><init>(ILjava/lang/CharSequence;)V

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 655
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isDrawerOpen()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isDrawerClosingOrOpening()Z

    move-result v2

    if-nez v2, :cond_0

    .line 662
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->cancelCurrentActionBarAlphaAnimation()V

    .line 663
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->getActionBarAlpha()I

    move-result v0

    .line 664
    .local v0, "currentActionBarAlpha":I
    new-instance v2, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$6;

    int-to-float v3, v0

    const/high16 v4, 0x437f0000    # 255.0f

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$6;-><init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;FFI)V

    iput-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    .line 674
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    const-wide/16 v4, 0x96

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 676
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    new-instance v3, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 677
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p1, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 653
    .end local v0    # "currentActionBarAlpha":I
    .end local v1    # "wasInDefaultMode":Z
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private exitCurrentActionBarMode(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 681
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 682
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->size()I

    move-result v1

    const/4 v2, 0x1

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isDrawerOpen()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isDrawerClosingOrOpening()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 705
    :cond_0
    :goto_0
    return-void

    .line 689
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->cancelCurrentActionBarAlphaAnimation()V

    .line 690
    iget v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlpha:I

    .line 691
    .local v0, "originalActionBarAlpha":I
    new-instance v1, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$7;

    const/high16 v2, 0x437f0000    # 255.0f

    int-to-float v3, v0

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$7;-><init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;FFI)V

    iput-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    .line 701
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 703
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 704
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    invoke-virtual {p1, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private static getBackgroundColorDrawable(I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p0, "color"    # I

    .prologue
    .line 473
    sget-object v2, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sBackgroundCache:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/SoftReference;

    .line 474
    .local v1, "ref":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/drawable/Drawable;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 475
    :cond_0
    new-instance v0, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v0, p0}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    .line 476
    .local v0, "mainLayer":Landroid/graphics/drawable/Drawable;
    new-instance v1, Ljava/lang/ref/SoftReference;

    .end local v1    # "ref":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/drawable/Drawable;>;"
    invoke-direct {v1, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .line 477
    .restart local v1    # "ref":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/drawable/Drawable;>;"
    sget-object v2, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sBackgroundCache:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 479
    .end local v0    # "mainLayer":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/Drawable;

    return-object v2
.end method

.method private static getBackgroundDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "drawableId"    # I

    .prologue
    .line 451
    sget-object v3, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sBackgroundCache:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/SoftReference;

    .line 452
    .local v2, "ref":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/drawable/Drawable;>;"
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    .line 453
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 454
    .local v1, "mainLayer":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$3;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-direct {v0, v3}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$3;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 466
    .local v0, "fullBackground":Landroid/graphics/drawable/LayerDrawable;
    new-instance v2, Ljava/lang/ref/SoftReference;

    .end local v2    # "ref":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/drawable/Drawable;>;"
    invoke-direct {v2, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    .line 467
    .restart local v2    # "ref":Ljava/lang/ref/SoftReference;, "Ljava/lang/ref/SoftReference<Landroid/graphics/drawable/Drawable;>;"
    sget-object v3, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->sBackgroundCache:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 469
    .end local v0    # "fullBackground":Landroid/graphics/drawable/LayerDrawable;
    .end local v1    # "mainLayer":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/drawable/Drawable;

    return-object v3
.end method

.method private isInMode(Ljava/lang/Integer;)Z
    .locals 2
    .param p1, "mode"    # Ljava/lang/Integer;

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;

    iget v0, v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;->mode:I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private removeModeFromStack(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "mode"    # Ljava/lang/Integer;

    .prologue
    .line 265
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->size()I

    move-result v1

    .line 266
    .local v1, "stackSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 267
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;

    iget v2, v2, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;->mode:I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 268
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->remove(I)Ljava/lang/Object;

    .line 272
    :cond_0
    return-void

    .line 266
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setTitle(Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 429
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    if-eqz v0, :cond_0

    .line 430
    iput-object p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentTitle:Ljava/lang/CharSequence;

    .line 432
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleSpannableString:Landroid/text/SpannableString;

    .line 433
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleSpannableString:Landroid/text/SpannableString;

    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleAlphaSpan:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;

    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleSpannableString:Landroid/text/SpannableString;

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 434
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleSpannableString:Landroid/text/SpannableString;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 436
    :cond_0
    return-void
.end method

.method private syncActions(Z)V
    .locals 3
    .param p1, "isInExpandedMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 795
    if-nez p1, :cond_2

    .line 796
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncDetailsPageMenuItem()V

    .line 798
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->IS_SEARCH_ALWAYS_VISIBLE:Z

    if-nez v0, :cond_0

    .line 799
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canSearch()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 821
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    if-eqz v0, :cond_1

    .line 822
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->isBackStackEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 824
    :cond_1
    return-void

    .line 803
    :cond_2
    sget-boolean v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->IS_SEARCH_ALWAYS_VISIBLE:Z

    if-nez v0, :cond_3

    .line 804
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 806
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mAutoUpdateItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 807
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 810
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mExpandedModeTranslatable:Lcom/google/android/finsky/activities/TextSectionTranslatable;

    if-eqz v0, :cond_5

    .line 811
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTranslateItem:Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mExpandedModeTranslatable:Lcom/google/android/finsky/activities/TextSectionTranslatable;

    invoke-interface {v2}, Lcom/google/android/finsky/activities/TextSectionTranslatable;->hasTranslation()Z

    move-result v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 812
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTranslateItem:Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mExpandedModeTranslatable:Lcom/google/android/finsky/activities/TextSectionTranslatable;

    invoke-interface {v0}, Lcom/google/android/finsky/activities/TextSectionTranslatable;->isShowingTranslation()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f0c0215

    :goto_2
    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0

    :cond_4
    const v0, 0x7f0c0214

    goto :goto_2

    .line 815
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTranslateItem:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 822
    goto :goto_1
.end method

.method private syncDetailsPageMenuItem()V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 827
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTranslateItem:Landroid/view/MenuItem;

    if-eqz v6, :cond_0

    .line 828
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTranslateItem:Landroid/view/MenuItem;

    invoke-interface {v6, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 830
    :cond_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentPageType()I

    move-result v0

    .line 831
    .local v0, "currentPageType":I
    const/4 v6, 0x5

    if-ne v0, v6, :cond_3

    move v3, v4

    .line 832
    .local v3, "isDetailsPage":Z
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 833
    .local v1, "doc":Lcom/google/android/finsky/api/model/Document;
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mAutoUpdateItem:Landroid/view/MenuItem;

    if-eqz v6, :cond_2

    .line 835
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v7

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v8

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v9

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/finsky/layout/AutoUpdateSection;->isAutoUpdateVisible(Ljava/lang/String;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/receivers/Installer;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 841
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mAutoUpdateItem:Landroid/view/MenuItem;

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 850
    :cond_2
    :goto_1
    return-void

    .end local v1    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "isDetailsPage":Z
    :cond_3
    move v3, v5

    .line 831
    goto :goto_0

    .line 843
    .restart local v1    # "doc":Lcom/google/android/finsky/api/model/Document;
    .restart local v3    # "isDetailsPage":Z
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/finsky/layout/AutoUpdateSection;->isAutoUpdateEnabled(Ljava/lang/String;)Z

    move-result v2

    .line 844
    .local v2, "isAutoUpdateEnabled":Z
    iget-object v5, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mAutoUpdateItem:Landroid/view/MenuItem;

    const v6, 0x7f0c01b0

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 845
    iget-object v5, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mAutoUpdateItem:Landroid/view/MenuItem;

    invoke-interface {v5, v4}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 846
    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mAutoUpdateItem:Landroid/view/MenuItem;

    invoke-interface {v4, v2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    .line 847
    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mAutoUpdateItem:Landroid/view/MenuItem;

    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method private syncEnvironment()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 375
    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;

    if-nez v4, :cond_1

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 380
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    .line 381
    .local v3, "toc":Lcom/google/android/finsky/api/model/DfeToc;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeToc;->hasIconOverrideUrl()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 382
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v2

    .line 383
    .local v2, "bl":Lcom/google/android/play/image/BitmapLoader;
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/DfeToc;->getIconOverrideUrl()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$2;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$2;-><init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V

    invoke-virtual {v2, v4, v6, v6, v5}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v1

    .line 392
    .local v1, "bc":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    invoke-virtual {v1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 393
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 395
    .local v0, "actionBarIcon":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 396
    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 400
    .end local v0    # "actionBarIcon":Landroid/graphics/drawable/BitmapDrawable;
    .end local v1    # "bc":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .end local v2    # "bl":Lcom/google/android/play/image/BitmapLoader;
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;

    invoke-interface {v4, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private syncRecentsEntry()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 340
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_2

    .line 341
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentTitle:Ljava/lang/CharSequence;

    .line 342
    .local v0, "currTitle":Ljava/lang/CharSequence;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 344
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 346
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 348
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c01ae

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 351
    :cond_1
    iget v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mPrevRecentsBackendForColor:I

    iget v7, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    if-ne v6, v7, :cond_3

    move v1, v4

    .line 352
    .local v1, "isSameBackend":Z
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mPrevRecentsTitle:Ljava/lang/CharSequence;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mPrevRecentsTitle:Ljava/lang/CharSequence;

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    move v2, v4

    .line 354
    .local v2, "isSameTitle":Z
    :goto_1
    if-eqz v1, :cond_5

    if-eqz v2, :cond_5

    .line 372
    .end local v0    # "currTitle":Ljava/lang/CharSequence;
    .end local v1    # "isSameBackend":Z
    .end local v2    # "isSameTitle":Z
    :cond_2
    :goto_2
    return-void

    .restart local v0    # "currTitle":Ljava/lang/CharSequence;
    :cond_3
    move v1, v5

    .line 351
    goto :goto_0

    .restart local v1    # "isSameBackend":Z
    :cond_4
    move v2, v5

    .line 352
    goto :goto_1

    .line 358
    .restart local v2    # "isSameTitle":Z
    :cond_5
    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mRecentsIcon:Ljava/lang/ref/SoftReference;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mRecentsIcon:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_7

    .line 359
    :cond_6
    new-instance v4, Ljava/lang/ref/SoftReference;

    iget-object v5, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f030005

    invoke-static {v5, v6}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mRecentsIcon:Ljava/lang/ref/SoftReference;

    .line 364
    :cond_7
    new-instance v3, Landroid/app/ActivityManager$TaskDescription;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mRecentsIcon:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    iget v7, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v6

    invoke-direct {v3, v5, v4, v6}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 367
    .local v3, "taskDescription":Landroid/app/ActivityManager$TaskDescription;
    iget-object v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4, v3}, Landroid/app/Activity;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 369
    iget v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    iput v4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mPrevRecentsBackendForColor:I

    .line 370
    iput-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mPrevRecentsTitle:Ljava/lang/CharSequence;

    goto :goto_2
.end method

.method private syncState()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 275
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncTitle()V

    .line 276
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncEnvironment()V

    .line 277
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncStatusBarColor()V

    .line 278
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncRecentsEntry()V

    .line 280
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->isInMode(Ljava/lang/Integer;)Z

    move-result v1

    .line 282
    .local v1, "isInSectionExpandedMode":Z
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    const v3, 0x7f020038

    invoke-static {v2, v3}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->getBackgroundDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 286
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundAlpha:I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 288
    if-eqz v1, :cond_2

    const v0, 0x7f09005b

    .line 290
    .local v0, "actionBarTitleColorId":I
    :goto_1
    new-instance v2, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;

    iget-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleAlphaSpan:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;

    .line 291
    iget-boolean v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mIgnoreActionBarBackground:Z

    if-eqz v2, :cond_3

    const/high16 v2, 0x437f0000    # 255.0f

    :goto_2
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateTitleAlpha(F)V

    .line 292
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    if-eqz v2, :cond_0

    .line 293
    iget-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    iget-boolean v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mIgnoreActionBarBackground:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTransparentBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    :goto_3
    invoke-virtual {v3, v2}, Landroid/support/v7/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 297
    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->isInMode(Ljava/lang/Integer;)Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncActions(Z)V

    .line 298
    return-void

    .line 282
    .end local v0    # "actionBarTitleColorId":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    iget v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->getBackgroundColorDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_0

    .line 288
    :cond_2
    const v0, 0x7f09004c

    goto :goto_1

    .line 291
    .restart local v0    # "actionBarTitleColorId":I
    :cond_3
    iget v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundAlpha:I

    int-to-float v2, v2

    goto :goto_2

    .line 293
    :cond_4
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_3
.end method

.method private syncStatusBarColor()V
    .locals 10

    .prologue
    const/16 v9, 0xff

    const/high16 v8, 0x437f0000    # 255.0f

    .line 301
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_1

    .line 303
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    instance-of v6, v6, Lcom/google/android/finsky/activities/MainActivity;

    if-eqz v6, :cond_3

    .line 307
    iget-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    check-cast v3, Lcom/google/android/finsky/activities/MainActivity;

    .line 308
    .local v3, "mainActivity":Lcom/google/android/finsky/activities/MainActivity;
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    iget v7, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v0

    .line 310
    .local v0, "corpusStatusBarBgColor":I
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mIgnoreActionBarBackground:Z

    if-nez v6, :cond_0

    iget v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundAlpha:I

    if-ne v6, v9, :cond_2

    .line 311
    :cond_0
    invoke-virtual {v3, v0}, Lcom/google/android/finsky/activities/MainActivity;->setStatusBarBackgroundColor(I)V

    .line 337
    .end local v0    # "corpusStatusBarBgColor":I
    .end local v3    # "mainActivity":Lcom/google/android/finsky/activities/MainActivity;
    :cond_1
    :goto_0
    return-void

    .line 313
    .restart local v0    # "corpusStatusBarBgColor":I
    .restart local v3    # "mainActivity":Lcom/google/android/finsky/activities/MainActivity;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v4

    .line 317
    .local v4, "multiStatusBarBgColor":I
    iget v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundAlpha:I

    int-to-float v6, v6

    div-float/2addr v6, v8

    invoke-static {v0, v4, v6}, Lcom/google/android/finsky/utils/UiUtils;->interpolateColor(IIF)I

    move-result v2

    .line 320
    .local v2, "interpolatedStatusBarColor":I
    invoke-virtual {v3, v2}, Lcom/google/android/finsky/activities/MainActivity;->setStatusBarBackgroundColor(I)V

    goto :goto_0

    .line 323
    .end local v0    # "corpusStatusBarBgColor":I
    .end local v2    # "interpolatedStatusBarColor":I
    .end local v3    # "mainActivity":Lcom/google/android/finsky/activities/MainActivity;
    .end local v4    # "multiStatusBarBgColor":I
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 324
    .local v5, "window":Landroid/view/Window;
    iget-object v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    iget v7, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getStatusBarColor(Landroid/content/Context;I)I

    move-result v1

    .line 326
    .local v1, "corpusStatusBarColor":I
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mIgnoreActionBarBackground:Z

    if-nez v6, :cond_4

    iget v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundAlpha:I

    if-ne v6, v9, :cond_5

    .line 327
    :cond_4
    invoke-virtual {v5, v1}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0

    .line 331
    :cond_5
    iget v6, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mMultiStatusBarColor:I

    iget v7, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundAlpha:I

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-static {v1, v6, v7}, Lcom/google/android/finsky/utils/UiUtils;->interpolateColor(IIF)I

    move-result v2

    .line 333
    .restart local v2    # "interpolatedStatusBarColor":I
    invoke-virtual {v5, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    goto :goto_0
.end method

.method private syncTitle()V
    .locals 5

    .prologue
    .line 407
    iget-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;

    iget-object v2, v3, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;->title:Ljava/lang/CharSequence;

    .line 409
    .local v2, "title":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 410
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->setTitle(Ljava/lang/CharSequence;)V

    .line 426
    :cond_0
    :goto_0
    return-void

    .line 414
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    const v4, 0x7f0c01ae

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->setTitle(Ljava/lang/CharSequence;)V

    .line 415
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    .line 416
    .local v1, "dfeToc":Lcom/google/android/finsky/api/model/DfeToc;
    iget v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    const/16 v4, 0x9

    if-ne v3, v4, :cond_2

    .line 419
    iget-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    const v4, 0x7f0c0391

    invoke-virtual {v3, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 420
    :cond_2
    if-eqz v1, :cond_0

    iget v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    if-eqz v3, :cond_0

    .line 421
    iget v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    .line 422
    .local v0, "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    if-eqz v0, :cond_0

    .line 423
    iget-object v3, v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateTitleAlpha(F)V
    .locals 4
    .param p1, "alpha"    # F

    .prologue
    const/4 v3, 0x0

    .line 439
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleAlphaSpan:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;->setAlpha(F)V

    .line 440
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleSpannableString:Landroid/text/SpannableString;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleSpannableString:Landroid/text/SpannableString;

    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleAlphaSpan:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$AlphaSpan;

    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleSpannableString:Landroid/text/SpannableString;

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 442
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTitleSpannableString:Landroid/text/SpannableString;

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->setTitle(Ljava/lang/CharSequence;)V

    .line 444
    :cond_0
    return-void
.end method


# virtual methods
.method public autoUpdateButtonClicked(Landroid/support/v4/app/FragmentActivity;)V
    .locals 2
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/finsky/layout/AutoUpdateSection;->handleAutoUpdateButtonClick(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/navigationmanager/NavigationManager;Z)V

    .line 570
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncDetailsPageMenuItem()V

    .line 571
    return-void
.end method

.method public cancelCurrentActionBarAlphaAnimation()V
    .locals 1

    .prologue
    .line 708
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlphaAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 714
    :cond_0
    return-void
.end method

.method public configureMenu(Landroid/app/Activity;Landroid/view/Menu;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    .line 484
    const v1, 0x7f0a00ab

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    .line 485
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    new-instance v2, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$4;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$4;-><init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V

    invoke-static {v1, v2}, Landroid/support/v4/view/MenuItemCompat;->setOnActionExpandListener(Landroid/view/MenuItem;Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 505
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    invoke-static {v1}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/SearchView;

    iput-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchView:Landroid/support/v7/widget/SearchView;

    .line 506
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchView:Landroid/support/v7/widget/SearchView;

    new-instance v2, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$5;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$5;-><init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 521
    const v1, 0x7f0a0188

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTranslateItem:Landroid/view/MenuItem;

    .line 522
    const v1, 0x7f0a03cb

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mAutoUpdateItem:Landroid/view/MenuItem;

    .line 523
    const v1, 0x7f0a03cc

    invoke-interface {p2, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;

    .line 525
    const-string v1, "search"

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 527
    .local v0, "searchManager":Landroid/app/SearchManager;
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {p1}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 529
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    if-nez v1, :cond_0

    .line 530
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchView:Landroid/support/v7/widget/SearchView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setVisibility(I)V

    .line 531
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTranslateItem:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 532
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mAutoUpdateItem:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 533
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 534
    sget-boolean v1, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->IS_SEARCH_ALWAYS_VISIBLE:Z

    if-nez v1, :cond_0

    .line 535
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    invoke-interface {v1, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 538
    :cond_0
    return-void
.end method

.method public enterActionBarSearchMode(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 717
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->enterActionBarTransientOpacityMode(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 718
    return-void
.end method

.method public enterActionBarSectionExpandedMode(Landroid/view/View;Ljava/lang/CharSequence;Lcom/google/android/finsky/activities/TextSectionTranslatable;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "expandedModeTitle"    # Ljava/lang/CharSequence;
    .param p3, "expandedModeTranslatable"    # Lcom/google/android/finsky/activities/TextSectionTranslatable;

    .prologue
    .line 736
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->enterActionBarTransientOpacityMode(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 738
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->clearSearch()V

    .line 739
    iput-object p3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mExpandedModeTranslatable:Lcom/google/android/finsky/activities/TextSectionTranslatable;

    .line 742
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    if-eqz v0, :cond_0

    .line 743
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    const v1, 0x7f02009d

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 747
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->setDrawerLockMode(I)V

    .line 749
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncState()V

    .line 750
    return-void
.end method

.method public enterDrawerOpenMode(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "drawerOpenModeTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 777
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->exitDrawerOpenMode()V

    .line 778
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    new-instance v1, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;

    const/4 v2, 0x3

    invoke-direct {v1, v2, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;-><init>(ILjava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 781
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncState()V

    .line 782
    return-void
.end method

.method public exitActionBarSearchMode(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 721
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->isInMode(Ljava/lang/Integer;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 722
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->exitCurrentActionBarMode(Landroid/view/View;)V

    .line 723
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncState()V

    .line 732
    :goto_0
    return-void

    .line 731
    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->removeModeFromStack(Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public exitActionBarSectionExpandedMode(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 754
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->setDrawerLockMode(I)V

    .line 757
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    if-eqz v0, :cond_0

    .line 758
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBar:Landroid/support/v7/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 761
    :cond_0
    iput-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mExpandedModeTranslatable:Lcom/google/android/finsky/activities/TextSectionTranslatable;

    .line 763
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->isInMode(Ljava/lang/Integer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 764
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->exitCurrentActionBarMode(Landroid/view/View;)V

    .line 765
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncState()V

    .line 774
    :goto_0
    return-void

    .line 773
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->removeModeFromStack(Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public exitDrawerOpenMode()V
    .locals 1

    .prologue
    .line 789
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->removeModeFromStack(Ljava/lang/Integer;)V

    .line 791
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncState()V

    .line 792
    return-void
.end method

.method public getActionBarAlpha()I
    .locals 1

    .prologue
    .line 630
    iget v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlpha:I

    return v0
.end method

.method public getCurrentBackendId()I
    .locals 1

    .prologue
    .line 447
    iget v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    return v0
.end method

.method public isActionBarInOpaqueMode()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 639
    iget-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v3}, Ljava/util/Stack;->size()I

    move-result v2

    .line 640
    .local v2, "stackSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 641
    iget-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    invoke-virtual {v3, v0}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;

    iget v3, v3, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;->mode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 642
    .local v1, "mode":Ljava/lang/Integer;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v5, 0x2

    if-ne v3, v5, :cond_1

    :cond_0
    move v3, v4

    .line 646
    .end local v1    # "mode":Ljava/lang/Integer;
    :goto_1
    return v3

    .line 640
    .restart local v1    # "mode":Ljava/lang/Integer;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 646
    .end local v1    # "mode":Ljava/lang/Integer;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public searchButtonClicked()Z
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 576
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->isActionViewExpanded(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->collapseActionView(Landroid/view/MenuItem;)Z

    .line 582
    :goto_0
    const/4 v0, 0x1

    .line 585
    :goto_1
    return v0

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->expandActionView(Landroid/view/MenuItem;)Z

    goto :goto_0

    .line 585
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setActionBarAlpha(IZ)V
    .locals 1
    .param p1, "alpha"    # I
    .param p2, "isTransient"    # Z

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/play/utils/PlayUtils;->isTv(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 598
    :cond_1
    if-nez p2, :cond_2

    .line 602
    iput p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentActionBarAlpha:I

    .line 603
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->isActionBarInOpaqueMode()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isDrawerOpen()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSideDrawerLayout:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isDrawerClosingOrOpening()Z

    move-result v0

    if-nez v0, :cond_0

    .line 611
    :cond_2
    iget v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundAlpha:I

    if-eq p1, v0, :cond_0

    .line 615
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mIgnoreActionBarBackground:Z

    if-nez v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 623
    iput p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackgroundAlpha:I

    .line 624
    int-to-float v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->updateTitleAlpha(F)V

    .line 625
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncStatusBarColor()V

    goto :goto_0
.end method

.method public setDefaultSearchQuery(Ljava/lang/String;)V
    .locals 0
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 589
    iput-object p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mDefaultSearchQuery:Ljava/lang/String;

    .line 590
    return-void
.end method

.method public translateButtonClicked()V
    .locals 5

    .prologue
    .line 552
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mExpandedModeTranslatable:Lcom/google/android/finsky/activities/TextSectionTranslatable;

    if-eqz v2, :cond_0

    .line 554
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mExpandedModeTranslatable:Lcom/google/android/finsky/activities/TextSectionTranslatable;

    invoke-interface {v2}, Lcom/google/android/finsky/activities/TextSectionTranslatable;->toggleTranslation()V

    .line 555
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mExpandedModeTranslatable:Lcom/google/android/finsky/activities/TextSectionTranslatable;

    invoke-interface {v2}, Lcom/google/android/finsky/activities/TextSectionTranslatable;->isShowingTranslation()Z

    move-result v1

    .line 557
    .local v1, "isShowingTranslation":Z
    iget-object v2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 558
    .local v0, "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v3

    if-eqz v1, :cond_1

    const/16 v2, 0x100

    :goto_0
    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 563
    iget-object v3, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mTranslateItem:Landroid/view/MenuItem;

    if-eqz v1, :cond_2

    const v2, 0x7f0c0215

    :goto_1
    invoke-interface {v3, v2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 566
    .end local v0    # "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    .end local v1    # "isShowingTranslation":Z
    :cond_0
    return-void

    .line 558
    .restart local v0    # "activePage":Lcom/google/android/finsky/fragments/PageFragment;
    .restart local v1    # "isShowingTranslation":Z
    :cond_1
    const/16 v2, 0x101

    goto :goto_0

    .line 563
    :cond_2
    const v2, 0x7f0c0214

    goto :goto_1
.end method

.method public updateCurrentBackendId(IZ)V
    .locals 1
    .param p1, "backendId"    # I
    .param p2, "ignoreActionBarBackground"    # Z

    .prologue
    .line 247
    iput p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    .line 248
    iput-boolean p2, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mIgnoreActionBarBackground:Z

    .line 250
    iget v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mCurrentBackendId:I

    invoke-static {v0}, Lcom/google/android/finsky/providers/RecentSuggestionsProvider;->setCurrentBackendId(I)V

    .line 252
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncState()V

    .line 253
    return-void
.end method

.method public updateDefaultTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActionBarStateStack:Ljava/util/Stack;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;

    iput-object p1, v0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$ActionBarState;->title:Ljava/lang/CharSequence;

    .line 257
    invoke-direct {p0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncState()V

    .line 258
    return-void
.end method
