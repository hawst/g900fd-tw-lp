.class public final Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "EncryptedSubscriberInfo.java"


# instance fields
.field public carrierKeyVersion:I

.field public data:Ljava/lang/String;

.field public encryptedKey:Ljava/lang/String;

.field public googleKeyVersion:I

.field public hasCarrierKeyVersion:Z

.field public hasData:Z

.field public hasEncryptedKey:Z

.field public hasGoogleKeyVersion:Z

.field public hasInitVector:Z

.field public hasSignature:Z

.field public initVector:Ljava/lang/String;

.field public signature:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->clear()Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;

    .line 49
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->data:Ljava/lang/String;

    .line 53
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasData:Z

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->encryptedKey:Ljava/lang/String;

    .line 55
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasEncryptedKey:Z

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->signature:Ljava/lang/String;

    .line 57
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasSignature:Z

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->initVector:Ljava/lang/String;

    .line 59
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasInitVector:Z

    .line 60
    iput v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->googleKeyVersion:I

    .line 61
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasGoogleKeyVersion:Z

    .line 62
    iput v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->carrierKeyVersion:I

    .line 63
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasCarrierKeyVersion:Z

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->cachedSize:I

    .line 65
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 94
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 95
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasData:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->data:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 96
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->data:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasEncryptedKey:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->encryptedKey:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 100
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->encryptedKey:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasSignature:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->signature:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 104
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->signature:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasInitVector:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->initVector:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 108
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->initVector:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasGoogleKeyVersion:Z

    if-nez v1, :cond_8

    iget v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->googleKeyVersion:I

    if-eqz v1, :cond_9

    .line 112
    :cond_8
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->googleKeyVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasCarrierKeyVersion:Z

    if-nez v1, :cond_a

    iget v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->carrierKeyVersion:I

    if-eqz v1, :cond_b

    .line 116
    :cond_a
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->carrierKeyVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_b
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 127
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 128
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 132
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 133
    :sswitch_0
    return-object p0

    .line 138
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->data:Ljava/lang/String;

    .line 139
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasData:Z

    goto :goto_0

    .line 143
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->encryptedKey:Ljava/lang/String;

    .line 144
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasEncryptedKey:Z

    goto :goto_0

    .line 148
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->signature:Ljava/lang/String;

    .line 149
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasSignature:Z

    goto :goto_0

    .line 153
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->initVector:Ljava/lang/String;

    .line 154
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasInitVector:Z

    goto :goto_0

    .line 158
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->googleKeyVersion:I

    .line 159
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasGoogleKeyVersion:Z

    goto :goto_0

    .line 163
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->carrierKeyVersion:I

    .line 164
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasCarrierKeyVersion:Z

    goto :goto_0

    .line 128
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasData:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->data:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 72
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->data:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 74
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasEncryptedKey:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->encryptedKey:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 75
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->encryptedKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 77
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasSignature:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->signature:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 78
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->signature:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 80
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasInitVector:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->initVector:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 81
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->initVector:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 83
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasGoogleKeyVersion:Z

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->googleKeyVersion:I

    if-eqz v0, :cond_9

    .line 84
    :cond_8
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->googleKeyVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 86
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->hasCarrierKeyVersion:Z

    if-nez v0, :cond_a

    iget v0, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->carrierKeyVersion:I

    if-eqz v0, :cond_b

    .line 87
    :cond_a
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/finsky/protos/EncryptedSubscriberInfo;->carrierKeyVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 89
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 90
    return-void
.end method
