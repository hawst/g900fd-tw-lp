.class Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;
.super Landroid/os/AsyncTask;
.source "PurchaseFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->setupAuthChallengeStep(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

.field final synthetic val$challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    .locals 0

    .prologue
    .line 471
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->val$challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 476
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    .line 477
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataServiceClient;-><init>(Landroid/content/Context;)V

    .line 479
    .local v0, "authClient":Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataClient;
    new-instance v2, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;

    iget-object v3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    # getter for: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->mAccount:Landroid/accounts/Account;
    invoke-static {v3}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$200(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;)Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountDataClient;->getReauthSettings(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 471
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->doInBackground([Ljava/lang/Void;)Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;)V
    .locals 2
    .param p1, "response"    # Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 496
    :goto_0
    return-void

    .line 488
    :cond_0
    if-eqz p1, :cond_1

    .line 490
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->val$challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->handleReauthSettingsResponse(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    invoke-static {v0, p1, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$300(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    goto :goto_0

    .line 495
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->this$0:Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->val$challenge:Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    # invokes: Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->getReauthSettingsOverNetwork(Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->access$400(Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;Lcom/google/android/finsky/protos/ChallengeProto$Challenge;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 471
    check-cast p1, Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$3;->onPostExecute(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;)V

    return-void
.end method
