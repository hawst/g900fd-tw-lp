.class Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;
.super Ljava/lang/Object;
.source "MyAppsTabbedFragment.java"

# interfaces
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->fetchPermissionsAndInstall(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$dfeModel:Lcom/google/android/finsky/api/model/DfeBulkDetails;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;Lcom/google/android/finsky/api/model/DfeBulkDetails;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 540
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    iput-object p2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;->val$dfeModel:Lcom/google/android/finsky/api/model/DfeBulkDetails;

    iput-object p3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;->val$accountName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDataChanged()V
    .locals 4

    .prologue
    .line 545
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    invoke-virtual {v2}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->canChangeFragmentManagerState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 546
    const-string v2, "Bulk install cannot start because no longer active."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 556
    :goto_0
    return-void

    .line 550
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    # invokes: Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->dismissProgressDialog()V
    invoke-static {v2}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->access$100(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)V

    .line 553
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;->val$dfeModel:Lcom/google/android/finsky/api/model/DfeBulkDetails;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->getDocuments()Ljava/util/List;

    move-result-object v0

    .line 554
    .local v0, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    # getter for: Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->access$300(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;->val$accountName:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/google/android/finsky/activities/MultiInstallActivity;->getInstallIntent(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 555
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment$4;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    # getter for: Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->access$400(Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
