.class public Lcom/google/android/finsky/layout/RateReviewSection;
.super Landroid/widget/LinearLayout;
.source "RateReviewSection.java"


# instance fields
.field private mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

.field private mDocument:Lcom/google/android/finsky/api/model/Document;

.field private mFragment:Landroid/support/v4/app/Fragment;

.field private mMyAvatar:Lcom/google/android/play/image/FifeImageView;

.field private mMyDisplayName:Landroid/widget/TextView;

.field private mMyRatingBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

.field private mMyRatingText:Landroid/widget/TextView;

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mRatingLayout:Landroid/widget/LinearLayout;

.field private mReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

.field private mReviewFromServer:Lcom/google/android/finsky/protos/DocumentV2$Review;

.field private mReviewItemLayout:Lcom/google/android/finsky/layout/ReviewItemLayout;

.field private mReviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

.field private mReviewReplyStub:Landroid/view/ViewStub;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/RateReviewSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/RateReviewSection;)Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/RateReviewSection;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/RateReviewSection;)Lcom/google/android/finsky/protos/DocumentV2$Review;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/RateReviewSection;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/RateReviewSection;Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/protos/DocumentV2$Review;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/RateReviewSection;
    .param p1, "x1"    # Landroid/support/v4/app/Fragment;
    .param p2, "x2"    # Lcom/google/android/finsky/protos/DocumentV2$Review;
    .param p3, "x3"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/layout/RateReviewSection;->launchReviewsDialog(Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/protos/DocumentV2$Review;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/RateReviewSection;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/RateReviewSection;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mDocument:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/RateReviewSection;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/RateReviewSection;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mFragment:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/layout/RateReviewSection;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/RateReviewSection;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyDisplayName:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/layout/RateReviewSection;)Lcom/google/android/play/image/FifeImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/RateReviewSection;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyAvatar:Lcom/google/android/play/image/FifeImageView;

    return-object v0
.end method

.method private launchReviewsDialog(Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/protos/DocumentV2$Review;I)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p2, "review"    # Lcom/google/android/finsky/protos/DocumentV2$Review;
    .param p3, "prefilledRating"    # I

    .prologue
    .line 166
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/layout/RateReviewSection$3;

    invoke-direct {v1, p0, p2, p3}, Lcom/google/android/finsky/layout/RateReviewSection$3;-><init>(Lcom/google/android/finsky/layout/RateReviewSection;Lcom/google/android/finsky/protos/DocumentV2$Review;I)V

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/RateReviewHelper;->checkAndConfirmGPlus(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/utils/RateReviewHelper$CheckAndConfirmGPlusListener;)V

    .line 182
    return-void
.end method

.method private updateVisibility(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/api/model/Document;Z)V
    .locals 1
    .param p1, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p2, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "hasDetailsLoaded"    # Z

    .prologue
    .line 94
    if-eqz p3, :cond_0

    invoke-static {p1, p2}, Lcom/google/android/finsky/utils/DocUtils;->canRate(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/api/model/Document;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->setVisibility(I)V

    .line 100
    :goto_0
    return-void

    .line 99
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public configure(Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/library/Libraries;Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/api/model/Document;ZLcom/google/android/finsky/protos/DocumentV2$Review;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p2, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p3, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p4, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p5, "hasDetailsLoaded"    # Z
    .param p6, "review"    # Lcom/google/android/finsky/protos/DocumentV2$Review;
    .param p7, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 121
    invoke-direct {p0, p2, p4, p5}, Lcom/google/android/finsky/layout/RateReviewSection;->updateVisibility(Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/api/model/Document;Z)V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/RateReviewSection;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    :goto_0
    return-void

    .line 126
    :cond_0
    iput-object p3, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mFragment:Landroid/support/v4/app/Fragment;

    .line 127
    iput-object p1, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    .line 128
    iput-object p4, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mDocument:Lcom/google/android/finsky/api/model/Document;

    .line 129
    iput-object p6, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewFromServer:Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 130
    iput-object p7, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 131
    iput-object p8, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 134
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/RateReviewSection;->refresh()V

    .line 137
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyRatingBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    const v1, 0x7f0b016f

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->setVerticalPadding(I)V

    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyRatingBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    const/4 v1, 0x0

    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    new-instance v3, Lcom/google/android/finsky/layout/RateReviewSection$1;

    invoke-direct {v3, p0, p3}, Lcom/google/android/finsky/layout/RateReviewSection$1;-><init>(Lcom/google/android/finsky/layout/RateReviewSection;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->configure(IILcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewItemLayout:Lcom/google/android/finsky/layout/ReviewItemLayout;

    new-instance v1, Lcom/google/android/finsky/layout/RateReviewSection$2;

    invoke-direct {v1, p0, p3}, Lcom/google/android/finsky/layout/RateReviewSection$2;-><init>(Lcom/google/android/finsky/layout/RateReviewSection;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/ReviewItemLayout;->setActionClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public initialize(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 107
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 82
    const v0, 0x7f0a0322

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyAvatar:Lcom/google/android/play/image/FifeImageView;

    .line 83
    const v0, 0x7f0a0323

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyDisplayName:Landroid/widget/TextView;

    .line 84
    const v0, 0x7f0a0324

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyRatingText:Landroid/widget/TextView;

    .line 85
    const v0, 0x7f0a032c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayRatingBar;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyRatingBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    .line 86
    const v0, 0x7f0a0350

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ReviewItemLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewItemLayout:Lcom/google/android/finsky/layout/ReviewItemLayout;

    .line 87
    const v0, 0x7f0a0361

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewReplyStub:Landroid/view/ViewStub;

    .line 88
    const v0, 0x7f0a0362

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ReviewReplyLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    .line 89
    const v0, 0x7f0a032b

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mRatingLayout:Landroid/widget/LinearLayout;

    .line 90
    return-void
.end method

.method public refresh()V
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v11, 0x0

    const/16 v10, 0x8

    .line 185
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/RateReviewSection;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 192
    .local v9, "res":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mClientMutationCache:Lcom/google/android/finsky/utils/ClientMutationCache;

    iget-object v1, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewFromServer:Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/ClientMutationCache;->getCachedReview(Ljava/lang/String;Lcom/google/android/finsky/protos/DocumentV2$Review;)Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 193
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-eqz v0, :cond_4

    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    iget v8, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->starRating:I

    .line 195
    .local v8, "newRating":I
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewItemLayout:Lcom/google/android/finsky/layout/ReviewItemLayout;

    iget-object v1, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v2, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    const v3, 0x7fffffff

    iget-object v4, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v6, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/ReviewItemLayout;->setReviewInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;ILcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/DocumentV2$Review;->hasReplyText:Z

    if-eqz v0, :cond_3

    .line 198
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    if-nez v0, :cond_0

    .line 199
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewReplyStub:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/ReviewReplyLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    .line 200
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewReplyStub:Landroid/view/ViewStub;

    .line 202
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    iget-object v1, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v2, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->setReviewInfo(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;)V

    .line 208
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewItemLayout:Lcom/google/android/finsky/layout/ReviewItemLayout;

    invoke-virtual {v0, v11}, Lcom/google/android/finsky/layout/ReviewItemLayout;->setVisibility(I)V

    .line 209
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mRatingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 216
    :goto_1
    if-lez v8, :cond_5

    .line 217
    const v0, 0x7f0c02d8

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 218
    .local v7, "header":Ljava/lang/String;
    const v0, 0x7f100004

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v11

    invoke-virtual {v9, v0, v8, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 225
    :goto_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mRatingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eq v0, v10, :cond_2

    .line 226
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyRatingBar:Lcom/google/android/finsky/layout/play/PlayRatingBar;

    invoke-virtual {v0, v8}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->setRating(I)V

    .line 227
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mMyRatingText:Landroid/widget/TextView;

    invoke-static {v7}, Lcom/google/android/finsky/utils/Utils;->getItalicSafeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getPlayDfeApi()Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/layout/RateReviewSection$4;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/RateReviewSection$4;-><init>(Lcom/google/android/finsky/layout/RateReviewSection;)V

    new-instance v2, Lcom/google/android/finsky/layout/RateReviewSection$5;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/layout/RateReviewSection$5;-><init>(Lcom/google/android/finsky/layout/RateReviewSection;)V

    invoke-interface {v0, v1, v2, v5}, Lcom/google/android/play/dfe/api/PlayDfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;

    .line 254
    :cond_2
    return-void

    .line 204
    .end local v7    # "header":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewReplyLayout:Lcom/google/android/finsky/layout/ReviewReplyLayout;

    invoke-virtual {v0, v10}, Lcom/google/android/finsky/layout/ReviewReplyLayout;->setVisibility(I)V

    goto :goto_0

    .line 211
    .end local v8    # "newRating":I
    :cond_4
    const/4 v8, 0x0

    .line 212
    .restart local v8    # "newRating":I
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mReviewItemLayout:Lcom/google/android/finsky/layout/ReviewItemLayout;

    invoke-virtual {v0, v10}, Lcom/google/android/finsky/layout/ReviewItemLayout;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mRatingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 221
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection;->mDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    invoke-static {v9, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getRateString(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v7

    .line 222
    .restart local v7    # "header":Ljava/lang/String;
    const v0, 0x7f0c02da

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/RateReviewSection;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method
