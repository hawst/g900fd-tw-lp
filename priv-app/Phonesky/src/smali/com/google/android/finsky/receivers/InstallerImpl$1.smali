.class Lcom/google/android/finsky/receivers/InstallerImpl$1;
.super Landroid/os/AsyncTask;
.source "InstallerImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/receivers/InstallerImpl;->start(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/finsky/download/DownloadProgress;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

.field final synthetic val$afterStartedRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->val$afterStartedRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 165
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl$1;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 2
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    const-string v1, "patches"

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->removeAllFilesFromCacheDir(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$000(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    const-string v1, "gzipped"

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->removeAllFilesFromCacheDir(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$000(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$100(Lcom/google/android/finsky/receivers/InstallerImpl;)Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/appstate/AppStates;->blockingLoad()V

    .line 180
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$200(Lcom/google/android/finsky/receivers/InstallerImpl;)Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/finsky/download/DownloadQueue;->getRunningDownloads()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 165
    check-cast p1, Ljava/util/List;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl$1;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/download/DownloadProgress;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "runningDownloads":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/download/DownloadProgress;>;"
    const/4 v3, 0x1

    .line 185
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # getter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mAppStates:Lcom/google/android/finsky/appstate/AppStates;
    invoke-static {v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$100(Lcom/google/android/finsky/receivers/InstallerImpl;)Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/appstate/AppStates;->getAppsToInstall()Ljava/util/List;

    move-result-object v2

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->recoverPriorityList(Ljava/util/List;)V
    invoke-static {v1, v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$300(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/util/List;)V

    .line 186
    const/4 v0, 0x1

    .line 187
    .local v0, "immediateStart":Z
    if-eqz p1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->multiUserMode()Z
    invoke-static {v1}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$400(Lcom/google/android/finsky/receivers/InstallerImpl;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 193
    const/4 v0, 0x0

    .line 194
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    new-instance v2, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/finsky/receivers/InstallerImpl$1$1;-><init>(Lcom/google/android/finsky/receivers/InstallerImpl$1;Ljava/util/List;)V

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->bindToMultiUserCoordinator(Ljava/lang/Runnable;)V
    invoke-static {v1, v2}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$900(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/lang/Runnable;)V

    .line 210
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 212
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->pruneSessions()V
    invoke-static {v1}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$600(Lcom/google/android/finsky/receivers/InstallerImpl;)V

    .line 214
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # setter for: Lcom/google/android/finsky/receivers/InstallerImpl;->mRunning:Z
    invoke-static {v1, v3}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$702(Lcom/google/android/finsky/receivers/InstallerImpl;Z)Z

    .line 215
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->kick(Z)V
    invoke-static {v1, v3}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$800(Lcom/google/android/finsky/receivers/InstallerImpl;Z)V

    .line 216
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->val$afterStartedRunnable:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 218
    :cond_1
    return-void

    .line 207
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerImpl$1;->this$0:Lcom/google/android/finsky/receivers/InstallerImpl;

    # invokes: Lcom/google/android/finsky/receivers/InstallerImpl;->recoverRunningDownloads(Ljava/util/List;)V
    invoke-static {v1, p1}, Lcom/google/android/finsky/receivers/InstallerImpl;->access$500(Lcom/google/android/finsky/receivers/InstallerImpl;Ljava/util/List;)V

    goto :goto_0
.end method
