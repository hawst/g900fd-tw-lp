.class Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;
.super Ljava/lang/Object;
.source "SelfUpdateScheduler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/SelfUpdateScheduler;->onComplete(Lcom/google/android/finsky/download/Download;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

.field final synthetic val$download:Lcom/google/android/finsky/download/Download;

.field final synthetic val$packageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler;Ljava/lang/String;Lcom/google/android/finsky/download/Download;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->this$0:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    iput-object p2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->val$packageName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->val$download:Lcom/google/android/finsky/download/Download;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 273
    new-instance v5, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2$1;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2$1;-><init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;)V

    .line 291
    .local v5, "listener":Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;
    iget-object v0, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->val$download:Lcom/google/android/finsky/download/Download;

    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->this$0:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    # getter for: Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadSize:J
    invoke-static {v2}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->access$200(Lcom/google/android/finsky/utils/SelfUpdateScheduler;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$2;->this$0:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    # getter for: Lcom/google/android/finsky/utils/SelfUpdateScheduler;->mDownloadSignature:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/finsky/utils/SelfUpdateScheduler;->access$300(Lcom/google/android/finsky/utils/SelfUpdateScheduler;)Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    const-string v7, ""

    invoke-static/range {v0 .. v7}, Lcom/google/android/finsky/utils/PackageManagerHelper;->installPackage(Landroid/net/Uri;Ljava/lang/String;JLjava/lang/String;Lcom/google/android/finsky/utils/PackageManagerHelper$InstallPackageListener;ZLjava/lang/String;)V

    .line 295
    return-void
.end method
