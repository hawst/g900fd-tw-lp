.class public Lcom/google/android/finsky/services/VpaService;
.super Landroid/app/Service;
.source "VpaService.java"


# static fields
.field private static sInstance:Lcom/google/android/finsky/services/VpaService;


# instance fields
.field private mListener:Lcom/google/android/finsky/services/SetupHoldListener;

.field private mServiceStartId:I

.field private mStartupRefCount:I

.field private mVpaRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/finsky/services/VpaService;->mStartupRefCount:I

    return-void
.end method

.method static synthetic access$010(Lcom/google/android/finsky/services/VpaService;)I
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/finsky/services/VpaService;->mStartupRefCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/google/android/finsky/services/VpaService;->mStartupRefCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/services/VpaService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/finsky/services/VpaService;->handleStartVpaCommand()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/services/VpaService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/finsky/services/VpaService;->stopServiceIfDone()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/services/VpaService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z

    return v0
.end method

.method static synthetic access$302(Lcom/google/android/finsky/services/VpaService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z

    return p1
.end method

.method static synthetic access$400(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/VpaService;->vpaGetDeviceConfig(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/services/VpaService;->vpaGetConsistencyToken(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/services/VpaService;->vpaGetPreloads(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;Lcom/google/android/finsky/protos/Preloads$Preload;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/google/android/finsky/protos/Preloads$Preload;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/services/VpaService;->downloadVpaConfig(Ljava/lang/String;Lcom/google/android/finsky/protos/Preloads$Preload;)V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;[Lcom/google/android/finsky/protos/Preloads$Preload;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # [Lcom/google/android/finsky/protos/Preloads$Preload;
    .param p3, "x3"    # I
    .param p4, "x4"    # Z

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/services/VpaService;->downloadPackages(Ljava/lang/String;[Lcom/google/android/finsky/protos/Preloads$Preload;IZ)V

    return-void
.end method

.method static synthetic access$900(Lcom/google/android/finsky/services/VpaService;ILjava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/VpaService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/services/VpaService;->notifyListener(ILjava/lang/String;Z)V

    return-void
.end method

.method private downloadPackages(Ljava/lang/String;[Lcom/google/android/finsky/protos/Preloads$Preload;IZ)V
    .locals 13
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "preloadPackages"    # [Lcom/google/android/finsky/protos/Preloads$Preload;
    .param p3, "priority"    # I
    .param p4, "visible"    # Z

    .prologue
    .line 303
    array-length v12, p2

    .line 304
    .local v12, "preloadAppsCount":I
    new-array v4, v12, [Ljava/lang/String;

    .line 305
    .local v4, "packageNames":[Ljava/lang/String;
    new-array v5, v12, [I

    .line 306
    .local v5, "versionCodes":[I
    new-array v6, v12, [Ljava/lang/String;

    .line 307
    .local v6, "titles":[Ljava/lang/String;
    new-array v7, v12, [I

    .line 308
    .local v7, "priorities":[I
    new-array v8, v12, [Ljava/lang/String;

    .line 309
    .local v8, "deliveryTokens":[Ljava/lang/String;
    new-array v9, v12, [Ljava/lang/String;

    .line 311
    .local v9, "appIconUrls":[Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    array-length v0, p2

    if-ge v10, v0, :cond_1

    .line 312
    aget-object v11, p2, v10

    .line 313
    .local v11, "preload":Lcom/google/android/finsky/protos/Preloads$Preload;
    iget-object v0, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    aput-object v0, v4, v10

    .line 314
    iget v0, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->versionCode:I

    aput v0, v5, v10

    .line 315
    iget-object v0, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->title:Ljava/lang/String;

    aput-object v0, v6, v10

    .line 316
    aput p3, v7, v10

    .line 317
    iget-object v0, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->deliveryToken:Ljava/lang/String;

    aput-object v0, v8, v10

    .line 318
    iget-object v0, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_0

    iget-object v0, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    if-eqz v0, :cond_0

    iget-object v0, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    if-eqz v0, :cond_0

    iget-object v0, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->icon:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    aput-object v0, v9, v10

    .line 325
    :goto_1
    const-string v0, "Requesting preload of %s:%d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v3, v3, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, v11, Lcom/google/android/finsky/protos/Preloads$Preload;->versionCode:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 311
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 323
    :cond_0
    const/4 v0, 0x0

    aput-object v0, v9, v10

    goto :goto_1

    .line 328
    .end local v11    # "preload":Lcom/google/android/finsky/protos/Preloads$Preload;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/services/VpaService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    move-object v2, p1

    move/from16 v3, p4

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/services/RestoreService;->restorePackages(Landroid/content/Context;ZLjava/lang/String;Z[Ljava/lang/String;[I[Ljava/lang/String;[I[Ljava/lang/String;[Ljava/lang/String;)V

    .line 330
    return-void
.end method

.method private downloadVpaConfig(Ljava/lang/String;Lcom/google/android/finsky/protos/Preloads$Preload;)V
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "preloadConfig"    # Lcom/google/android/finsky/protos/Preloads$Preload;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 283
    new-array v0, v3, [Lcom/google/android/finsky/protos/Preloads$Preload;

    aput-object p2, v0, v2

    .line 285
    .local v0, "preloadPackages":[Lcom/google/android/finsky/protos/Preloads$Preload;
    iget-object v1, p2, Lcom/google/android/finsky/protos/Preloads$Preload;->title:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 286
    iget-object v1, p2, Lcom/google/android/finsky/protos/Preloads$Preload;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    iput-object v1, p2, Lcom/google/android/finsky/protos/Preloads$Preload;->title:Ljava/lang/String;

    .line 288
    :cond_0
    invoke-direct {p0, p1, v0, v3, v2}, Lcom/google/android/finsky/services/VpaService;->downloadPackages(Ljava/lang/String;[Lcom/google/android/finsky/protos/Preloads$Preload;IZ)V

    .line 289
    return-void
.end method

.method private handleStartVpaCommand()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 148
    iget-boolean v3, p0, Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z

    if-eqz v3, :cond_0

    .line 149
    const-string v2, "Received command to load VPA while already handling"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 199
    :goto_0
    return v1

    .line 152
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z

    .line 154
    new-instance v0, Lcom/google/android/finsky/services/VpaService$2;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/services/VpaService$2;-><init>(Lcom/google/android/finsky/services/VpaService;)V

    .line 198
    .local v0, "task":Landroid/os/AsyncTask;, "Landroid/os/AsyncTask<Ljava/lang/Void;Ljava/lang/Void;Ljava/lang/String;>;"
    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/Utils;->executeMultiThreaded(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    move v1, v2

    .line 199
    goto :goto_0
.end method

.method private notifyListener(ILjava/lang/String;Z)V
    .locals 6
    .param p1, "eventCode"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "cancelable"    # Z

    .prologue
    const/4 v3, 0x0

    .line 394
    iget-object v0, p0, Lcom/google/android/finsky/services/VpaService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/google/android/finsky/services/VpaService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    const-string v5, "VpaService"

    move v1, p1

    move-object v2, p2

    move v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/services/SetupHoldListener;->onStatusChange(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 396
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 397
    iput-object v3, p0, Lcom/google/android/finsky/services/VpaService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    .line 400
    :cond_0
    return-void
.end method

.method private registerAndNotifyListener(Lcom/google/android/finsky/services/SetupHoldListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/finsky/services/SetupHoldListener;

    .prologue
    .line 375
    iput-object p1, p0, Lcom/google/android/finsky/services/VpaService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    .line 376
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/finsky/services/VpaService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/finsky/services/VpaService$7;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/services/VpaService$7;-><init>(Lcom/google/android/finsky/services/VpaService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 387
    return-void
.end method

.method public static registerListener(Lcom/google/android/finsky/services/SetupHoldListener;)Z
    .locals 3
    .param p0, "listener"    # Lcom/google/android/finsky/services/SetupHoldListener;

    .prologue
    const/4 v0, 0x1

    .line 355
    if-nez p0, :cond_1

    .line 356
    sget-object v1, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    if-eqz v1, :cond_0

    .line 357
    sget-object v1, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/android/finsky/services/VpaService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    .line 368
    :cond_0
    :goto_0
    return v0

    .line 362
    :cond_1
    sget-object v1, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    iget-boolean v1, v1, Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z

    if-nez v1, :cond_3

    .line 364
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 367
    :cond_3
    sget-object v1, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/services/VpaService;->registerAndNotifyListener(Lcom/google/android/finsky/services/SetupHoldListener;)V

    goto :goto_0
.end method

.method public static shouldHold()Z
    .locals 1

    .prologue
    .line 342
    sget-object v0, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    iget-boolean v0, v0, Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static startVpa()V
    .locals 3

    .prologue
    .line 68
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    .line 69
    .local v0, "appContext":Lcom/google/android/finsky/FinskyApp;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/finsky/services/VpaService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 70
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "playsetupservice://startvpa"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 72
    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 73
    return-void
.end method

.method private stopServiceIfDone()V
    .locals 3

    .prologue
    .line 114
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureOnMainThread()V

    .line 115
    iget v0, p0, Lcom/google/android/finsky/services/VpaService;->mStartupRefCount:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z

    if-nez v0, :cond_0

    .line 116
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/finsky/services/VpaService;->notifyListener(ILjava/lang/String;Z)V

    .line 117
    iget v0, p0, Lcom/google/android/finsky/services/VpaService;->mServiceStartId:I

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/services/VpaService;->stopSelf(I)V

    .line 119
    :cond_0
    return-void
.end method

.method private vpaGetConsistencyToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "configPackageName"    # Ljava/lang/String;
    .param p2, "deviceConfigToken"    # Ljava/lang/String;

    .prologue
    .line 229
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/services/VpaService$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/services/VpaService$4;-><init>(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper;->get(Landroid/content/Context;Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;)V

    .line 235
    return-void
.end method

.method private vpaGetDeviceConfig(Ljava/lang/String;)V
    .locals 3
    .param p1, "configPackageName"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "deviceConfigToken":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    new-instance v2, Lcom/google/android/finsky/services/VpaService$3;

    invoke-direct {v2, p0, p1}, Lcom/google/android/finsky/services/VpaService$3;-><init>(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;)V

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->requestToken(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V

    .line 224
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/services/VpaService;->vpaGetConsistencyToken(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private vpaGetPreloads(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "configPackageName"    # Ljava/lang/String;
    .param p2, "deviceConfigToken"    # Ljava/lang/String;
    .param p3, "consistencyToken"    # Ljava/lang/String;

    .prologue
    .line 240
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi()Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    .line 241
    .local v0, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-interface {v0}, Lcom/google/android/finsky/api/DfeApi;->getAccountName()Ljava/lang/String;

    move-result-object v6

    .line 242
    .local v6, "accountName":Ljava/lang/String;
    new-instance v4, Lcom/google/android/finsky/services/VpaService$5;

    invoke-direct {v4, p0, v6}, Lcom/google/android/finsky/services/VpaService$5;-><init>(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;)V

    new-instance v5, Lcom/google/android/finsky/services/VpaService$6;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/services/VpaService$6;-><init>(Lcom/google/android/finsky/services/VpaService;)V

    move-object v1, p2

    move-object v2, p3

    move-object v3, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/api/DfeApi;->preloads(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 272
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 123
    sput-object p0, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    .line 124
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v1}, Lcom/google/android/finsky/services/VpaService;->notifyListener(ILjava/lang/String;Z)V

    .line 129
    sput-object v2, Lcom/google/android/finsky/services/VpaService;->sInstance:Lcom/google/android/finsky/services/VpaService;

    .line 130
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 88
    iput p3, p0, Lcom/google/android/finsky/services/VpaService;->mServiceStartId:I

    .line 89
    iget v0, p0, Lcom/google/android/finsky/services/VpaService;->mStartupRefCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/services/VpaService;->mStartupRefCount:I

    .line 90
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/services/VpaService$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/services/VpaService$1;-><init>(Lcom/google/android/finsky/services/VpaService;)V

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    .line 105
    const/4 v0, 0x3

    return v0
.end method
