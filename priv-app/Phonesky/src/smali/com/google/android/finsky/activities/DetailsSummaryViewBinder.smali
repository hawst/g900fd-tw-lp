.class public Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
.super Ljava/lang/Object;
.source "DetailsSummaryViewBinder.java"


# instance fields
.field protected final mAccount:Landroid/accounts/Account;

.field protected mBindingDynamicSection:Z

.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field protected mButtonSection:Landroid/view/ViewGroup;

.field protected mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

.field protected mContext:Landroid/content/Context;

.field protected mContinueUrl:Ljava/lang/String;

.field protected mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field protected mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field protected mDoc:Lcom/google/android/finsky/api/model/Document;

.field protected mDynamicSection:Lcom/google/android/finsky/layout/DetailsSummaryDynamic;

.field protected final mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field protected mHideDynamicFeatures:Z

.field protected mIsBinderDestroyed:Z

.field private mIsCancelingPreorder:Z

.field private mIsCompactMode:Z

.field protected mIsPendingRefund:Z

.field private mLayouts:[Landroid/view/View;

.field protected mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field protected mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mRevealTransitionCoverName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/DfeToc;Landroid/accounts/Account;)V
    .locals 2
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "account"    # Landroid/accounts/Account;

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    .line 155
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 156
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 157
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 158
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;
    .param p1, "x1"    # Z

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsCancelingPreorder:Z

    return p1
.end method

.method private addByline(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 451
    const v1, 0x7f04006f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 453
    .local v0, "byline":Landroid/widget/TextView;
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 454
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 455
    return-void
.end method

.method private addExtraLabel(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 515
    const v1, 0x7f040071

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 517
    .local v0, "extraLabel":Landroid/widget/TextView;
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 518
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 519
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 520
    return-void
.end method

.method private configureActionButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/library/Libraries;)V
    .locals 10
    .param p1, "button"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p2, "action"    # Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    .param p3, "backend"    # I
    .param p4, "libraries"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    const/4 v3, 0x0

    .line 906
    invoke-virtual {p1, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 907
    new-instance v6, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;

    invoke-direct {v6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;-><init>()V

    .line 908
    .local v6, "actionStyle":Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 909
    .local v8, "res":Landroid/content/res/Resources;
    const v0, 0x7f0f0007

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    .line 910
    .local v9, "useLongStyle":Z
    if-eqz v9, :cond_0

    .line 911
    invoke-static {p2, p3, v6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionStyleLong(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    .line 915
    :goto_0
    const/4 v7, 0x0

    .line 916
    .local v7, "clickListener":Landroid/view/View$OnClickListener;
    invoke-static {p2}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->canCreateClickListener(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 917
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContinueUrl:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    move-object v0, p2

    move v1, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionClickListener(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/content/Context;)Landroid/view/View$OnClickListener;

    move-result-object v7

    .line 926
    :goto_1
    invoke-virtual {v6, v8}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;->getString(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p3, v0, v7}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 927
    return-void

    .line 913
    .end local v7    # "clickListener":Landroid/view/View$OnClickListener;
    :cond_0
    invoke-static {p2, p3, v6}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionStyle(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/utils/PurchaseButtonHelper$TextStyle;)V

    goto :goto_0

    .line 919
    .restart local v7    # "clickListener":Landroid/view/View$OnClickListener;
    :cond_1
    iget v0, p2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    .line 920
    iget-object v0, p2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->document:Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->getCancelPreorderClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Landroid/view/View$OnClickListener;

    move-result-object v7

    goto :goto_1

    .line 921
    :cond_2
    iget v0, p2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_3

    .line 922
    iget-object v0, p2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->document:Lcom/google/android/finsky/api/model/Document;

    iget-object v1, p2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->account:Landroid/accounts/Account;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->getDownloadClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Landroid/view/View$OnClickListener;

    move-result-object v7

    goto :goto_1

    .line 924
    :cond_3
    const-string v0, "Can\'t create a click listener for action %d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private configureCancelButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
    .locals 3
    .param p1, "cancelButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "owner"    # Landroid/accounts/Account;

    .prologue
    const/4 v0, 0x0

    .line 946
    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 947
    iget-boolean v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsCancelingPreorder:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 948
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const v1, 0x7f0c0228

    invoke-virtual {p0, p2, p3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->getCancelPreorderClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 950
    return-void
.end method

.method private getDiscountOfferNote(Lcom/google/android/finsky/library/Libraries;)Ljava/lang/CharSequence;
    .locals 10
    .param p1, "libraries"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 589
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-virtual {p1, v7}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v2

    .line 590
    .local v2, "library":Lcom/google/android/finsky/library/Library;
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v8, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-static {v7, v8, v2}, Lcom/google/android/finsky/utils/DocUtils;->getOfferWithLargestDiscountIfAny(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v6

    .line 592
    .local v6, "offerWithLargestDiscount":Lcom/google/android/finsky/protos/Common$Offer;
    if-nez v6, :cond_1

    .line 593
    const/4 v3, 0x0

    .line 607
    :cond_0
    :goto_0
    return-object v3

    .line 595
    :cond_1
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v7, v6}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->getListPriceNoteResourceId(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;)I

    move-result v5

    .line 597
    .local v5, "listPriceNoteResourceId":I
    iget-object v0, v6, Lcom/google/android/finsky/protos/Common$Offer;->formattedFullAmount:Ljava/lang/String;

    .line 598
    .local v0, "fullPrice":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    invoke-virtual {v7, v5, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 600
    .local v4, "listPriceNote":Ljava/lang/String;
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v4}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 601
    .local v3, "listPriceBuilder":Landroid/text/SpannableStringBuilder;
    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 602
    .local v1, "fullPriceStart":I
    if-ltz v1, :cond_0

    .line 604
    new-instance v7, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v7}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v1

    const/16 v9, 0x11

    invoke-virtual {v3, v7, v1, v8, v9}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method private static getListPriceNoteResourceId(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/Common$Offer;)I
    .locals 3
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "offer"    # Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    .line 612
    .local v0, "documentType":I
    iget v1, p1, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    .line 613
    .local v1, "offerType":I
    const/4 v2, 0x6

    if-ne v0, v2, :cond_0

    .line 614
    packed-switch v1, :pswitch_data_0

    .line 625
    :cond_0
    :pswitch_0
    const/4 v2, 0x5

    if-ne v0, v2, :cond_1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 626
    const v2, 0x7f0c038a

    .line 628
    :goto_0
    return v2

    .line 616
    :pswitch_1
    const v2, 0x7f0c0388

    goto :goto_0

    .line 618
    :pswitch_2
    const v2, 0x7f0c0389

    goto :goto_0

    .line 620
    :pswitch_3
    const v2, 0x7f0c038b

    goto :goto_0

    .line 622
    :pswitch_4
    const v2, 0x7f0c038c

    goto :goto_0

    .line 628
    :cond_1
    const v2, 0x7f0c0387

    goto :goto_0

    .line 614
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setupBylines()V
    .locals 6

    .prologue
    .line 421
    const v4, 0x7f0a017d

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 422
    .local v0, "bylinesContainer":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 424
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 426
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v2

    .line 428
    .local v2, "isPreorderOffer":Z
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 446
    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-lez v4, :cond_3

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 448
    return-void

    .line 430
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getVideoDetails()Lcom/google/android/finsky/protos/DocDetails$VideoDetails;

    move-result-object v3

    .line 431
    .local v3, "videoDetails":Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    if-nez v2, :cond_1

    iget-object v4, v3, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 432
    iget-object v4, v3, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->releaseDate:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v4}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->addByline(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 434
    :cond_1
    iget-object v4, v3, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 435
    iget-object v4, v3, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->contentRating:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v4}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->addByline(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 440
    :goto_2
    iget-object v4, v3, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 441
    iget-object v4, v3, Lcom/google/android/finsky/protos/DocDetails$VideoDetails;->duration:Ljava/lang/String;

    invoke-direct {p0, v1, v0, v4}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->addByline(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 437
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    const v5, 0x7f0c03d7

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v1, v0, v4}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->addByline(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 446
    .end local v3    # "videoDetails":Lcom/google/android/finsky/protos/DocDetails$VideoDetails;
    :cond_3
    const/16 v4, 0x8

    goto :goto_1

    .line 428
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method private setupExtraLabels()V
    .locals 19

    .prologue
    .line 458
    const v14, 0x7f0a017e

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 459
    .local v3, "extraLabelsContainer":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 461
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-static {v14}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 463
    .local v4, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->shouldDisplayOfferNote()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 464
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v14}, Lcom/google/android/finsky/api/model/Document;->getOfferNote()Ljava/lang/String;

    move-result-object v6

    .line 465
    .local v6, "offerNote":Ljava/lang/CharSequence;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 466
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3, v6}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->addExtraLabel(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 472
    .end local v6    # "offerNote":Ljava/lang/CharSequence;
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v5

    .line 473
    .local v5, "libraries":Lcom/google/android/finsky/library/Libraries;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-static {v14, v5, v15}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v7

    .line 474
    .local v7, "owner":Landroid/accounts/Account;
    if-eqz v7, :cond_2

    .line 476
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v13

    .line 477
    .local v13, "purchaseOffer":Lcom/google/android/finsky/protos/Common$Offer;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v15, 0x7

    invoke-virtual {v14, v15}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v12

    .line 479
    .local v12, "purchaseHdOffer":Lcom/google/android/finsky/protos/Common$Offer;
    const/4 v11, 0x0

    .line 480
    .local v11, "preorderOffer":Lcom/google/android/finsky/protos/Common$Offer;
    invoke-static {v13}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 481
    move-object v11, v13

    .line 486
    :cond_1
    :goto_0
    if-eqz v11, :cond_2

    .line 489
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->shouldShowPreorderOnSaleDate(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 490
    iget-wide v8, v11, Lcom/google/android/finsky/protos/Common$Offer;->onSaleDate:J

    .line 491
    .local v8, "onSaleDate":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    const v15, 0x7f0c0226

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/DateUtils;->formatShortDisplayDateUtc(J)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 496
    .end local v8    # "onSaleDate":J
    .local v10, "preorderNote":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3, v10}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->addExtraLabel(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 500
    .end local v10    # "preorderNote":Ljava/lang/String;
    .end local v11    # "preorderOffer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v12    # "purchaseHdOffer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v13    # "purchaseOffer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_2
    if-nez v7, :cond_3

    .line 502
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v14

    invoke-virtual {v14}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isHideSalesPricesExperimentEnabled()Z

    move-result v14

    if-nez v14, :cond_3

    .line 503
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->getDiscountOfferNote(Lcom/google/android/finsky/library/Libraries;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 504
    .local v2, "discountNote":Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 505
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->addExtraLabel(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 510
    .end local v2    # "discountNote":Ljava/lang/CharSequence;
    :cond_3
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v14

    if-lez v14, :cond_6

    const/4 v14, 0x0

    :goto_2
    invoke-virtual {v3, v14}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 512
    return-void

    .line 482
    .restart local v11    # "preorderOffer":Lcom/google/android/finsky/protos/Common$Offer;
    .restart local v12    # "purchaseHdOffer":Lcom/google/android/finsky/protos/Common$Offer;
    .restart local v13    # "purchaseOffer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_4
    invoke-static {v12}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 483
    move-object v11, v12

    goto :goto_0

    .line 494
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    const v15, 0x7f0c0227

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "preorderNote":Ljava/lang/String;
    goto :goto_1

    .line 510
    .end local v10    # "preorderNote":Ljava/lang/String;
    .end local v11    # "preorderOffer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v12    # "purchaseHdOffer":Lcom/google/android/finsky/protos/Common$Offer;
    .end local v13    # "purchaseOffer":Lcom/google/android/finsky/protos/Common$Offer;
    :cond_6
    const/16 v14, 0x8

    goto :goto_2
.end method

.method private setupExtraLabelsApp()V
    .locals 9

    .prologue
    const/16 v6, 0x8

    .line 523
    const v7, 0x7f0a0183

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 525
    .local v1, "extraLabelsBottomContainer":Landroid/view/ViewGroup;
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v7

    const/4 v8, 0x1

    if-eq v7, v8, :cond_0

    .line 526
    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 560
    :goto_0
    return-void

    .line 530
    :cond_0
    const/4 v3, 0x0

    .line 531
    .local v3, "hasExtraLabelsBottom":Z
    const v7, 0x7f0a0184

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 533
    .local v2, "extraLabelsBottomLeading":Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 535
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 537
    .local v5, "inflater":Landroid/view/LayoutInflater;
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    .line 538
    .local v0, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/experiments/FinskyExperiments;->shouldHideDownloadCountInDetailsTitle()Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v7, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 540
    iget-object v7, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->numDownloads:Ljava/lang/String;

    invoke-virtual {p0, v5, v2, v7}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->addExtraLabelBottom(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 541
    const/4 v3, 0x1

    .line 545
    :cond_1
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->hasOptimalDeviceClassWarning()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 546
    iget-object v7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getOptimalDeviceClassWarning()Lcom/google/android/finsky/protos/DocAnnotations$Warning;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/finsky/protos/DocAnnotations$Warning;->localizedMessage:Ljava/lang/String;

    invoke-virtual {p0, v5, v2, v7}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->addExtraLabelBottom(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V

    .line 548
    const/4 v3, 0x1

    .line 552
    :cond_2
    if-eqz v0, :cond_3

    iget-boolean v7, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->declaresIab:Z

    if-eqz v7, :cond_3

    .line 553
    const v7, 0x7f0a0185

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 555
    .local v4, "inAppPurchaseLabelView":Landroid/widget/TextView;
    const v7, 0x7f0c03b1

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(I)V

    .line 556
    const/4 v3, 0x1

    .line 559
    .end local v4    # "inAppPurchaseLabelView":Landroid/widget/TextView;
    :cond_3
    if-eqz v3, :cond_4

    const/4 v6, 0x0

    :cond_4
    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private shouldShowPreorderOnSaleDate(Lcom/google/android/finsky/protos/Common$Offer;)Z
    .locals 4
    .param p1, "purchaseOffer"    # Lcom/google/android/finsky/protos/Common$Offer;

    .prologue
    .line 573
    iget-boolean v0, p1, Lcom/google/android/finsky/protos/Common$Offer;->hasPreorderFulfillmentDisplayDate:Z

    if-eqz v0, :cond_0

    iget-wide v0, p1, Lcom/google/android/finsky/protos/Common$Offer;->preorderFulfillmentDisplayDate:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 575
    const/4 v0, 0x0

    .line 577
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateButtonActionStyle()V
    .locals 4

    .prologue
    .line 775
    const v1, 0x7fffffff

    .line 777
    .local v1, "highestPriority":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 778
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayActionButton;

    .line 780
    .local v0, "button":Lcom/google/android/play/layout/PlayActionButton;
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayActionButton;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayActionButton;->getPriority()I

    move-result v3

    if-ge v3, v1, :cond_0

    .line 781
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayActionButton;->getPriority()I

    move-result v1

    .line 777
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 785
    .end local v0    # "button":Lcom/google/android/play/layout/PlayActionButton;
    :cond_1
    const/4 v2, 0x0

    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 786
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayActionButton;

    .line 787
    .restart local v0    # "button":Lcom/google/android/play/layout/PlayActionButton;
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayActionButton;->getVisibility()I

    move-result v3

    if-nez v3, :cond_2

    .line 788
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayActionButton;->getPriority()I

    move-result v3

    if-ne v3, v1, :cond_3

    .line 789
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setActionStyle(I)V

    .line 785
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 791
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setActionStyle(I)V

    goto :goto_2

    .line 795
    .end local v0    # "button":Lcom/google/android/play/layout/PlayActionButton;
    :cond_4
    return-void
.end method


# virtual methods
.method protected addExtraLabelBottom(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 564
    const v1, 0x7f040072

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 566
    .local v0, "extraLabel":Landroid/widget/TextView;
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 567
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 568
    return-void
.end method

.method public varargs bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V
    .locals 2
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bindDynamicSection"    # Z
    .param p3, "views"    # [Landroid/view/View;

    .prologue
    const/16 v1, 0x8

    .line 173
    iput-object p3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mLayouts:[Landroid/view/View;

    .line 174
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 175
    iput-boolean p2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBindingDynamicSection:Z

    .line 177
    const v0, 0x7f0a017f

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDynamicSection:Lcom/google/android/finsky/layout/DetailsSummaryDynamic;

    .line 178
    const v0, 0x7f0a0180

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    .line 180
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setupItemDetails()V

    .line 181
    if-eqz p2, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->syncDynamicSection()V

    .line 183
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->updateButtonActionStyle()V

    .line 188
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDynamicSection:Lcom/google/android/finsky/layout/DetailsSummaryDynamic;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->syncContainerVisibility(Landroid/view/ViewGroup;I)V

    .line 189
    return-void

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public bindCoverFromDocument()V
    .locals 5

    .prologue
    .line 413
    const v2, 0x7f0a0186

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 414
    .local v0, "thumbnail":Lcom/google/android/play/layout/PlayCardThumbnail;
    if-eqz v0, :cond_0

    .line 415
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DocImageView;

    .line 416
    .local v1, "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    sget-object v4, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->CORE_IMAGE_TYPES:[I

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 418
    .end local v1    # "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    :cond_0
    return-void
.end method

.method protected configureLaunchButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V
    .locals 4
    .param p1, "launchButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "owner"    # Landroid/accounts/Account;

    .prologue
    .line 934
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 935
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 936
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getOpenButtonStringId(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v2, p2, p3, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getOpenClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 939
    return-void
.end method

.method protected displayActionButtonsIfNecessary(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;)Z
    .locals 17
    .param p1, "launchButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p2, "buyButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p3, "buyButton2"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p4, "tryButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p5, "downloadButton"    # Lcom/google/android/play/layout/PlayActionButton;

    .prologue
    .line 807
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v12

    .line 808
    .local v12, "libraries":Lcom/google/android/finsky/library/Libraries;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v12, v3}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v13

    .line 809
    .local v13, "library":Lcom/google/android/finsky/library/Library;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-static {v3, v12, v4}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v14

    .line 810
    .local v14, "owner":Landroid/accounts/Account;
    if-eqz v14, :cond_1

    .line 812
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 813
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v3, v14}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->configureCancelButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    .line 817
    :goto_0
    const/4 v3, 0x1

    .line 844
    :goto_1
    return v3

    .line 815
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v3, v14}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->configureLaunchButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    goto :goto_0

    .line 821
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-static {v3, v4, v13}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v11

    .line 822
    .local v11, "isAvailable":Z
    if-nez v11, :cond_2

    .line 823
    const/4 v3, 0x0

    goto :goto_1

    .line 827
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setupBuyButtons(Landroid/accounts/Account;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Z)V

    .line 830
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->hasSample()Z

    move-result v3

    if-eqz v3, :cond_3

    if-eqz p4, :cond_3

    .line 831
    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 832
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v4, 0x2

    invoke-static {v3, v13, v4}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 833
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v3

    const v4, 0x7f0c0209

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    invoke-virtual {v5, v6, v7, v8}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getOpenClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 844
    :cond_3
    :goto_2
    const/4 v3, 0x1

    goto :goto_1

    .line 837
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v15

    const v16, 0x7f0c0209

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContinueUrl:Ljava/lang/String;

    const/16 v9, 0xde

    const/4 v10, 0x0

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v3

    move-object/from16 v0, p4

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1, v3}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method protected displayActionButtonsIfNecessaryNew(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;)Z
    .locals 12
    .param p1, "launchButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p2, "buyButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p3, "buyButton2"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p4, "tryButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p5, "downloadButton"    # Lcom/google/android/play/layout/PlayActionButton;

    .prologue
    .line 859
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v3

    .line 861
    .local v3, "libraries":Lcom/google/android/finsky/library/Libraries;
    const/4 v9, 0x0

    .line 862
    .local v9, "buyButtonsConfigured":I
    const/4 v11, 0x0

    .line 864
    .local v11, "totalButtonsConfigured":I
    new-instance v7, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;

    invoke-direct {v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;-><init>()V

    .line 866
    .local v7, "documentActions":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    const/4 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static/range {v1 .. v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getDocumentActions(Landroid/accounts/Account;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;)V

    .line 869
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    iget v1, v7, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->actionCount:I

    if-ge v10, v1, :cond_0

    .line 870
    const/4 v1, 0x2

    if-lt v11, v1, :cond_1

    .line 871
    const-string v1, "Not supposed to have more than %d actions available"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 898
    :cond_0
    invoke-virtual {v7}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->hasAction()Z

    move-result v1

    return v1

    .line 875
    :cond_1
    invoke-virtual {v7, v10}, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->getAction(I)Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    move-result-object v8

    .line 876
    .local v8, "action":Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;
    iget v1, v8, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/4 v2, 0x6

    if-eq v1, v2, :cond_2

    iget v1, v8, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_3

    .line 879
    :cond_2
    iget v1, v7, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    invoke-direct {p0, p1, v8, v1, v3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->configureActionButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/library/Libraries;)V

    .line 880
    add-int/lit8 v11, v11, 0x1

    .line 869
    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 881
    :cond_3
    iget v1, v8, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/16 v2, 0xb

    if-eq v1, v2, :cond_4

    iget v1, v8, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_5

    .line 883
    :cond_4
    iget v1, v7, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    move-object/from16 v0, p4

    invoke-direct {p0, v0, v8, v1, v3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->configureActionButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/library/Libraries;)V

    .line 884
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 885
    :cond_5
    iget v1, v8, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->actionType:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_6

    .line 886
    iget v1, v7, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    move-object/from16 v0, p5

    invoke-direct {p0, v0, v8, v1, v3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->configureActionButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/library/Libraries;)V

    .line 887
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 889
    :cond_6
    if-nez v9, :cond_7

    .line 890
    iget v1, v7, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    invoke-direct {p0, p2, v8, v1, v3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->configureActionButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/library/Libraries;)V

    .line 894
    :goto_2
    add-int/lit8 v9, v9, 0x1

    .line 895
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 892
    :cond_7
    iget v1, v7, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentActions;->backend:I

    invoke-direct {p0, p3, v8, v1, v3}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->configureActionButton(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/library/Libraries;)V

    goto :goto_2
.end method

.method protected findViewById(I)Landroid/view/View;
    .locals 7
    .param p1, "id"    # I

    .prologue
    const/4 v5, 0x0

    .line 210
    iget-object v6, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mLayouts:[Landroid/view/View;

    if-nez v6, :cond_0

    move-object v1, v5

    .line 222
    :goto_0
    return-object v1

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mLayouts:[Landroid/view/View;

    .local v0, "arr$":[Landroid/view/View;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v3, v0, v2

    .line 214
    .local v3, "layout":Landroid/view/View;
    if-nez v3, :cond_2

    .line 213
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 217
    :cond_2
    invoke-virtual {v3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 218
    .local v1, "found":Landroid/view/View;
    if-eqz v1, :cond_1

    goto :goto_0

    .end local v1    # "found":Landroid/view/View;
    .end local v3    # "layout":Landroid/view/View;
    :cond_3
    move-object v1, v5

    .line 222
    goto :goto_0
.end method

.method protected getBuyButtonLoggingElementType(ZI)I
    .locals 3
    .param p1, "isOwnedByUser"    # Z
    .param p2, "offerType"    # I

    .prologue
    const/16 v0, 0xdd

    .line 706
    if-eqz p1, :cond_1

    .line 720
    :cond_0
    :goto_0
    return v0

    .line 711
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/api/model/Document;->needsCheckoutFlow(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 712
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    .line 714
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 716
    const/16 v0, 0xe1

    goto :goto_0

    .line 720
    :cond_2
    const/16 v0, 0xc8

    goto :goto_0
.end method

.method protected final getBuyButtonString(ZI)Ljava/lang/String;
    .locals 8
    .param p1, "isOwnedByUser"    # Z
    .param p2, "offerType"    # I

    .prologue
    const v7, 0x7f0c01e6

    const/4 v6, 0x1

    .line 670
    if-eqz p1, :cond_1

    .line 671
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 697
    :cond_0
    :goto_0
    return-object v0

    .line 675
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4, p2}, Lcom/google/android/finsky/api/model/Document;->needsCheckoutFlow(I)Z

    move-result v1

    .line 676
    .local v1, "needsCheckoutFlow":Z
    if-nez v1, :cond_3

    .line 677
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 678
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 679
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    if-ne v4, v6, :cond_3

    .line 682
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    const v5, 0x7f0c020b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 687
    :cond_3
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v4

    const-string v5, "cl:billing.show_buy_verb_in_button"

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/experiments/FinskyExperiments;->isEnabled(Ljava/lang/String;)Z

    move-result v3

    .line 689
    .local v3, "useBuyVerb":Z
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v4, p2}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v2

    .line 690
    .local v2, "purchaseOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v2, :cond_4

    iget-boolean v4, v2, Lcom/google/android/finsky/protos/Common$Offer;->hasFormattedAmount:Z

    if-eqz v4, :cond_4

    .line 691
    iget-object v0, v2, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    .line 692
    .local v0, "formattedAmount":Ljava/lang/String;
    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    .line 693
    iget-object v4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    const v5, 0x7f0c01f9

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 697
    .end local v0    # "formattedAmount":Ljava/lang/String;
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method protected getCancelPreorderClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "owner"    # Landroid/accounts/Account;

    .prologue
    .line 960
    new-instance v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$2;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)V

    return-object v0
.end method

.method protected getDownloadClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "owner"    # Landroid/accounts/Account;

    .prologue
    .line 1000
    const/4 v0, 0x0

    return-object v0
.end method

.method public init(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/fragments/PageFragment;ZLjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "fragment"    # Lcom/google/android/finsky/fragments/PageFragment;
    .param p5, "trackPackageStatus"    # Z
    .param p6, "continueUrl"    # Ljava/lang/String;
    .param p7, "revealTransitionCoverName"    # Ljava/lang/String;
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    .line 164
    iput-object p2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 165
    iput-object p3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 166
    iput-object p4, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContainerFragment:Lcom/google/android/finsky/fragments/PageFragment;

    .line 167
    iput-object p6, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContinueUrl:Ljava/lang/String;

    .line 168
    iput-object p7, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mRevealTransitionCoverName:Ljava/lang/String;

    .line 169
    iput-object p8, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 170
    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    .line 192
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsBinderDestroyed:Z

    .line 194
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    .line 199
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 200
    .local v0, "buttonCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 201
    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 202
    .local v1, "child":Landroid/view/View;
    instance-of v3, v1, Lcom/google/android/play/layout/PlayActionButton;

    if-eqz v3, :cond_0

    .line 203
    check-cast v1, Lcom/google/android/play/layout/PlayActionButton;

    .end local v1    # "child":Landroid/view/View;
    invoke-virtual {v1}, Lcom/google/android/play/layout/PlayActionButton;->resetClickListener()V

    .line 200
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 207
    .end local v0    # "buttonCount":I
    .end local v2    # "i":I
    :cond_1
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 1095
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    .line 1057
    const/4 v5, 0x7

    if-ne p1, v5, :cond_0

    .line 1058
    const-string v5, "DetailsSummaryViewBinder.doc"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/api/model/Document;

    .line 1059
    .local v1, "doc":Lcom/google/android/finsky/api/model/Document;
    const-string v5, "DetailsSummaryViewBinder.ownerAccountName"

    invoke-virtual {p2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1061
    .local v3, "ownerName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    .line 1062
    .local v0, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    new-instance v4, Lcom/google/android/finsky/library/RevokeListenerWrapper;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v5

    invoke-interface {v0}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v6

    new-instance v7, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$3;

    invoke-direct {v7, p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$3;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;)V

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/finsky/library/RevokeListenerWrapper;-><init>(Lcom/google/android/finsky/library/LibraryReplicators;Landroid/accounts/Account;Lcom/android/volley/Response$Listener;)V

    .line 1073
    .local v4, "revokeListenerWrapper":Lcom/google/android/finsky/library/RevokeListenerWrapper;
    new-instance v2, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$4;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$4;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;)V

    .line 1084
    .local v2, "errorListener":Lcom/android/volley/Response$ErrorListener;
    iput-boolean v8, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsCancelingPreorder:Z

    .line 1085
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->refresh()V

    .line 1086
    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5, v8, v4, v2}, Lcom/google/android/finsky/api/DfeApi;->revoke(Ljava/lang/String;ILcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 1088
    .end local v0    # "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    .end local v1    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v2    # "errorListener":Lcom/android/volley/Response$ErrorListener;
    .end local v3    # "ownerName":Ljava/lang/String;
    .end local v4    # "revokeListenerWrapper":Lcom/google/android/finsky/library/RevokeListenerWrapper;
    :cond_0
    return-void
.end method

.method public refresh()V
    .locals 3

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsBinderDestroyed:Z

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-boolean v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBindingDynamicSection:Z

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mLayouts:[Landroid/view/View;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->bind(Lcom/google/android/finsky/api/model/Document;Z[Landroid/view/View;)V

    .line 231
    :cond_0
    return-void
.end method

.method public setCompactMode(Z)V
    .locals 0
    .param p1, "isCompactMode"    # Z

    .prologue
    .line 1045
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsCompactMode:Z

    .line 1046
    return-void
.end method

.method public setCoverFromBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "coverBitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 405
    const v2, 0x7f0a0186

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 406
    .local v0, "thumbnail":Lcom/google/android/play/layout/PlayCardThumbnail;
    if-eqz v0, :cond_0

    .line 407
    invoke-virtual {v0}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DocImageView;

    .line 408
    .local v1, "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    invoke-virtual {v1, p1}, Lcom/google/android/finsky/layout/DocImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 410
    .end local v1    # "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    :cond_0
    return-void
.end method

.method public setDynamicFeaturesVisibility(Z)V
    .locals 1
    .param p1, "isDynamicVisible"    # Z

    .prologue
    .line 1037
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mHideDynamicFeatures:Z

    .line 1038
    return-void

    .line 1037
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected setupActionButtons(Z)V
    .locals 8
    .param p1, "isInTransientState"    # Z

    .prologue
    const/16 v7, 0x8

    .line 731
    const v0, 0x7f0a013a

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/google/android/play/layout/PlayActionButton;

    .line 732
    .local v4, "tryButton":Lcom/google/android/play/layout/PlayActionButton;
    const v0, 0x7f0a0138

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/layout/PlayActionButton;

    .line 733
    .local v2, "buyButton":Lcom/google/android/play/layout/PlayActionButton;
    const v0, 0x7f0a0139

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/layout/PlayActionButton;

    .line 734
    .local v3, "buyButton2":Lcom/google/android/play/layout/PlayActionButton;
    const v0, 0x7f0a013b

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/PlayActionButton;

    .line 735
    .local v1, "launchButton":Lcom/google/android/play/layout/PlayActionButton;
    const v0, 0x7f0a013e

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/google/android/play/layout/PlayActionButton;

    .line 738
    .local v5, "downloadButton":Lcom/google/android/play/layout/PlayActionButton;
    invoke-virtual {v1, v7}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 739
    if-eqz v4, :cond_0

    .line 740
    invoke-virtual {v4, v7}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 743
    :cond_0
    invoke-virtual {v2, v7}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 745
    if-eqz v3, :cond_1

    .line 746
    invoke-virtual {v3, v7}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 749
    :cond_1
    if-eqz v5, :cond_2

    .line 750
    invoke-virtual {v5, v7}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 753
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mHideDynamicFeatures:Z

    if-eqz v0, :cond_4

    .line 772
    :cond_3
    :goto_0
    return-void

    .line 760
    :cond_4
    if-nez p1, :cond_3

    move-object v0, p0

    .line 764
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->displayActionButtonsIfNecessary(Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;)Z

    move-result v6

    .line 767
    .local v6, "isShowingActionButtons":Z
    if-eqz v6, :cond_3

    .line 771
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->syncButtonSectionVisibility()V

    goto :goto_0
.end method

.method protected setupBuyButtons(Landroid/accounts/Account;Lcom/google/android/play/layout/PlayActionButton;Lcom/google/android/play/layout/PlayActionButton;Z)V
    .locals 11
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "buyButton"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p3, "buyButtonSecondary"    # Lcom/google/android/play/layout/PlayActionButton;
    .param p4, "isOwnedByUser"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 643
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v9

    .line 644
    .local v9, "purchaseOffer":Lcom/google/android/finsky/protos/Common$Offer;
    if-nez v9, :cond_0

    .line 661
    :goto_0
    return-void

    .line 648
    :cond_0
    invoke-virtual {p2, v5}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 651
    invoke-static {v9}, Lcom/google/android/finsky/api/model/Document;->isPreorderOffer(Lcom/google/android/finsky/protos/Common$Offer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 652
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0205

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, v9, Lcom/google/android/finsky/protos/Common$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 653
    .local v8, "buttonText":Ljava/lang/String;
    const/16 v6, 0xea

    .line 658
    .local v6, "logEventType":I
    :goto_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v10

    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget v3, v9, Lcom/google/android/finsky/protos/Common$Offer;->offerType:I

    iget-object v5, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContinueUrl:Ljava/lang/String;

    move-object v1, p1

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p2, v10, v8, v0}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 655
    .end local v6    # "logEventType":I
    .end local v8    # "buttonText":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0, p4, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->getBuyButtonString(ZI)Ljava/lang/String;

    move-result-object v8

    .line 656
    .restart local v8    # "buttonText":Ljava/lang/String;
    invoke-virtual {p0, p4, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->getBuyButtonLoggingElementType(ZI)I

    move-result v6

    .restart local v6    # "logEventType":I
    goto :goto_1
.end method

.method protected setupItemDetails()V
    .locals 21

    .prologue
    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v9

    .line 241
    .local v9, "docType":I
    const v2, 0x7f0a0178

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/TextView;

    .line 242
    .local v19, "title":Landroid/widget/TextView;
    if-eqz v19, :cond_0

    .line 243
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    const/4 v2, 0x1

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 250
    :cond_0
    const v2, 0x7f0a0179

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    .line 251
    .local v8, "creatorPanel":Landroid/view/ViewGroup;
    const v2, 0x7f0a017a

    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 253
    .local v7, "creator":Lcom/google/android/finsky/layout/DecoratedTextView;
    if-eqz v7, :cond_2

    .line 255
    const/4 v2, 0x6

    if-eq v9, v2, :cond_1

    const/4 v2, 0x3

    if-eq v9, v2, :cond_1

    const/4 v2, 0x2

    if-eq v9, v2, :cond_1

    const/4 v2, 0x4

    if-eq v9, v2, :cond_1

    const/4 v2, 0x5

    if-ne v9, v2, :cond_6

    .line 260
    :cond_1
    const/16 v2, 0x8

    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 270
    :cond_2
    :goto_0
    const v2, 0x7f0a017c

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 272
    .local v18, "tipperStickerView":Lcom/google/android/finsky/layout/DecoratedTextView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, v18

    invoke-static {v2, v0}, Lcom/google/android/finsky/utils/BadgeUtils;->configureTipperSticker(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/layout/PlayTextView;)V

    .line 275
    const v2, 0x7f0a019f

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;

    .line 277
    .local v1, "creatorBlock":Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;
    if-eqz v1, :cond_3

    .line 278
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/analytics/FinskyEventLog;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 282
    :cond_3
    const v2, 0x7f0a0177

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;

    .line 284
    .local v20, "wishlist":Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;
    if-eqz v20, :cond_4

    .line 285
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsCompactMode:Z

    if-eqz v2, :cond_7

    .line 286
    const/16 v2, 0x8

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->setVisibility(I)V

    .line 292
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 295
    .local v11, "res":Landroid/content/res/Resources;
    const v2, 0x7f0a0186

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 296
    .local v14, "thumbnail":Lcom/google/android/play/layout/PlayCardThumbnail;
    const v2, 0x7f0f0007

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v10

    .line 298
    .local v10, "isWideLayout":Z
    packed-switch v9, :pswitch_data_0

    .line 327
    :goto_2
    :pswitch_0
    const/4 v12, 0x0

    .line 334
    .local v12, "showThumbnail":Z
    :goto_3
    const v2, 0x7f0a0175

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Lcom/google/android/finsky/layout/DetailsSummary;

    .line 335
    .local v13, "summary":Lcom/google/android/finsky/layout/DetailsSummary;
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsCompactMode:Z

    if-nez v2, :cond_9

    if-nez v12, :cond_9

    .line 336
    const/16 v2, 0x8

    invoke-virtual {v14, v2}, Lcom/google/android/play/layout/PlayCardThumbnail;->setVisibility(I)V

    .line 337
    if-eqz v10, :cond_8

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v13, v2}, Lcom/google/android/finsky/layout/DetailsSummary;->setThumbnailMode(I)V

    .line 397
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsCompactMode:Z

    if-nez v2, :cond_5

    .line 398
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setupBylines()V

    .line 399
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setupExtraLabels()V

    .line 400
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setupExtraLabelsApp()V

    .line 402
    :cond_5
    return-void

    .line 262
    .end local v1    # "creatorBlock":Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;
    .end local v10    # "isWideLayout":Z
    .end local v11    # "res":Landroid/content/res/Resources;
    .end local v12    # "showThumbnail":Z
    .end local v13    # "summary":Lcom/google/android/finsky/layout/DetailsSummary;
    .end local v14    # "thumbnail":Lcom/google/android/play/layout/PlayCardThumbnail;
    .end local v18    # "tipperStickerView":Lcom/google/android/finsky/layout/DecoratedTextView;
    .end local v20    # "wishlist":Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 263
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v2}, Lcom/google/android/finsky/utils/PlayCardUtils;->getDocDisplaySubtitle(Lcom/google/android/finsky/api/model/Document;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-static {v2, v3, v7}, Lcom/google/android/finsky/utils/BadgeUtils;->configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    .line 265
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    const v2, 0x7f0a017b

    invoke-virtual {v8, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-static {v3, v4, v2}, Lcom/google/android/finsky/utils/BadgeUtils;->configureContentRatingBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Landroid/widget/ImageView;)V

    goto/16 :goto_0

    .line 288
    .restart local v1    # "creatorBlock":Lcom/google/android/finsky/layout/DetailsTitleCreatorBlock;
    .restart local v18    # "tipperStickerView":Lcom/google/android/finsky/layout/DecoratedTextView;
    .restart local v20    # "wishlist":Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2, v3}, Lcom/google/android/finsky/layout/DetailsSummaryWishlistView;->configure(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    goto/16 :goto_1

    .line 306
    .restart local v10    # "isWideLayout":Z
    .restart local v11    # "res":Landroid/content/res/Resources;
    .restart local v14    # "thumbnail":Lcom/google/android/play/layout/PlayCardThumbnail;
    :pswitch_1
    const/4 v12, 0x1

    .line 307
    .restart local v12    # "showThumbnail":Z
    goto :goto_3

    .line 314
    .end local v12    # "showThumbnail":Z
    :pswitch_2
    move v12, v10

    .line 315
    .restart local v12    # "showThumbnail":Z
    goto :goto_3

    .line 322
    .end local v12    # "showThumbnail":Z
    :pswitch_3
    const/4 v12, 0x0

    .restart local v12    # "showThumbnail":Z
    goto :goto_2

    .line 337
    .restart local v13    # "summary":Lcom/google/android/finsky/layout/DetailsSummary;
    :cond_8
    const/4 v2, 0x2

    goto :goto_4

    .line 340
    :cond_9
    const/4 v2, 0x0

    invoke-virtual {v14, v2}, Lcom/google/android/play/layout/PlayCardThumbnail;->setVisibility(I)V

    .line 341
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v2

    invoke-virtual {v14, v2}, Lcom/google/android/play/layout/PlayCardThumbnail;->updateCoverPadding(I)V

    .line 342
    invoke-virtual {v14}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v16

    .line 343
    .local v16, "thumbnailLp":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-static {v2, v9}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getRegularDetailsIconWidth(Landroid/content/Context;I)I

    move-result v2

    move-object/from16 v0, v16

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 345
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-static {v2, v9}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getRegularDetailsIconHeight(Landroid/content/Context;I)I

    move-result v2

    move-object/from16 v0, v16

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 347
    invoke-virtual {v14}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v15

    check-cast v15, Lcom/google/android/finsky/layout/DocImageView;

    .line 348
    .local v15, "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v15, v2}, Lcom/google/android/finsky/layout/DocImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 349
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mRevealTransitionCoverName:Ljava/lang/String;

    invoke-virtual {v15, v2}, Lcom/google/android/finsky/layout/DocImageView;->setTransitionName(Ljava/lang/String;)V

    .line 352
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mRevealTransitionCoverName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 354
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    sget-object v4, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->CORE_IMAGE_TYPES:[I

    invoke-virtual {v15, v2, v3, v4}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 357
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBindingDynamicSection:Z

    invoke-virtual {v15, v2}, Lcom/google/android/finsky/layout/DocImageView;->setFocusable(Z)V

    .line 358
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v2, v11}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getItemThumbnailContentDescription(Lcom/google/android/finsky/api/model/Document;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Lcom/google/android/finsky/layout/DocImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 360
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mBindingDynamicSection:Z

    if-eqz v2, :cond_c

    .line 361
    new-instance v2, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder$1;-><init>(Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;)V

    invoke-virtual {v15, v2}, Lcom/google/android/finsky/layout/DocImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 371
    const v2, 0x7f02017e

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v15, v2}, Lcom/google/android/finsky/layout/DocImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 376
    :cond_c
    if-eqz v10, :cond_d

    .line 378
    const/16 v17, 0x0

    .line 394
    .local v17, "thumbnailMode":I
    :goto_6
    move/from16 v0, v17

    invoke-virtual {v13, v0}, Lcom/google/android/finsky/layout/DetailsSummary;->setThumbnailMode(I)V

    goto/16 :goto_5

    .line 381
    .end local v17    # "thumbnailMode":I
    :cond_d
    sparse-switch v9, :sswitch_data_0

    .line 390
    const/16 v17, 0x2

    .restart local v17    # "thumbnailMode":I
    goto :goto_6

    .line 386
    .end local v17    # "thumbnailMode":I
    :sswitch_0
    const/16 v17, 0x1

    .line 387
    .restart local v17    # "thumbnailMode":I
    goto :goto_6

    .line 298
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 381
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x10 -> :sswitch_0
        0x11 -> :sswitch_0
    .end sparse-switch
.end method

.method protected shouldDisplayOfferNote()Z
    .locals 4

    .prologue
    .line 583
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    .line 584
    .local v0, "libraries":Lcom/google/android/finsky/library/Libraries;
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-static {v2, v0, v3}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v1

    .line 585
    .local v1, "owner":Landroid/accounts/Account;
    if-nez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected showDynamicStatus(I)V
    .locals 3
    .param p1, "statusStringId"    # I

    .prologue
    .line 1028
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDynamicSection:Lcom/google/android/finsky/layout/DetailsSummaryDynamic;

    const v2, 0x7f0a0182

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/DetailsSummaryDynamic;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1031
    .local v0, "dynamicStatus":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1032
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1033
    iget-object v1, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1034
    return-void
.end method

.method protected syncButtonSectionVisibility()V
    .locals 2

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->syncContainerVisibility(Landroid/view/ViewGroup;I)V

    .line 1025
    return-void
.end method

.method protected syncDynamicSection()V
    .locals 4

    .prologue
    .line 1004
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mAccount:Landroid/accounts/Account;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v0

    .line 1005
    .local v0, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v3, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-static {v2, v3, v0}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v1

    .line 1006
    .local v1, "isAvailable":Z
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setDynamicFeaturesVisibility(Z)V

    .line 1010
    iget-object v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mButtonSection:Landroid/view/ViewGroup;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1012
    iget-boolean v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mHideDynamicFeatures:Z

    if-eqz v2, :cond_0

    .line 1021
    :goto_0
    return-void

    .line 1016
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->mIsPendingRefund:Z

    if-eqz v2, :cond_1

    .line 1017
    const v2, 0x7f0c02aa

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->showDynamicStatus(I)V

    goto :goto_0

    .line 1020
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/DetailsSummaryViewBinder;->setupActionButtons(Z)V

    goto :goto_0
.end method
