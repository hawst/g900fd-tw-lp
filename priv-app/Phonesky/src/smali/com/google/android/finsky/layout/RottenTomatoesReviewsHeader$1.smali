.class Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;
.super Ljava/lang/Object;
.source "RottenTomatoesReviewsHeader.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->bind(Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/play/image/BitmapLoader;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;

.field final synthetic val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field final synthetic val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field final synthetic val$reviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;Lcom/google/android/finsky/api/model/DfeToc;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;->this$0:Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;

    iput-object p2, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iput-object p3, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;->val$reviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    iput-object p4, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;->val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;->val$reviewsResponse:Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    iget-object v1, v1, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    iget-object v2, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;->val$dfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v3, p0, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader$1;->this$0:Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;

    invoke-virtual {v3}, Lcom/google/android/finsky/layout/RottenTomatoesReviewsHeader;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->resolveLink(Lcom/google/android/finsky/protos/DocAnnotations$Link;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;)V

    .line 96
    return-void
.end method
