.class Lcom/google/android/finsky/layout/AppPermissionAdapter$2;
.super Ljava/lang/Object;
.source "AppPermissionAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/AppPermissionAdapter;->getPermissionView(Landroid/view/ViewGroup;Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;Ljava/lang/String;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/AppPermissionAdapter;

.field final synthetic val$contentView:Landroid/widget/TextView;

.field final synthetic val$expanderIcon:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/AppPermissionAdapter;Landroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->this$0:Lcom/google/android/finsky/layout/AppPermissionAdapter;

    iput-object p2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->val$contentView:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->val$expanderIcon:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 241
    iget-object v2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->val$contentView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-ne v2, v4, :cond_0

    const/4 v0, 0x1

    .line 242
    .local v0, "isCollapsed":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 243
    iget-object v2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->val$expanderIcon:Landroid/widget/ImageView;

    const v3, 0x7f020101

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 244
    iget-object v2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->val$expanderIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->this$0:Lcom/google/android/finsky/layout/AppPermissionAdapter;

    # getter for: Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->access$000(Lcom/google/android/finsky/layout/AppPermissionAdapter;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c02e5

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->val$contentView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 253
    :goto_1
    return-void

    .end local v0    # "isCollapsed":Z
    :cond_0
    move v0, v1

    .line 241
    goto :goto_0

    .line 248
    .restart local v0    # "isCollapsed":Z
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->val$expanderIcon:Landroid/widget/ImageView;

    const v2, 0x7f0200fe

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 249
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->val$expanderIcon:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->this$0:Lcom/google/android/finsky/layout/AppPermissionAdapter;

    # getter for: Lcom/google/android/finsky/layout/AppPermissionAdapter;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->access$000(Lcom/google/android/finsky/layout/AppPermissionAdapter;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0c02e4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 251
    iget-object v1, p0, Lcom/google/android/finsky/layout/AppPermissionAdapter$2;->val$contentView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method
