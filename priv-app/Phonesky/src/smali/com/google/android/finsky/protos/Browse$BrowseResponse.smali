.class public final Lcom/google/android/finsky/protos/Browse$BrowseResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Browse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Browse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BrowseResponse"
.end annotation


# instance fields
.field public backendId:I

.field public breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

.field public browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

.field public category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

.field public contentsUrl:Ljava/lang/String;

.field public hasBackendId:Z

.field public hasContentsUrl:Z

.field public hasLandingTabIndex:Z

.field public hasPromoUrl:Z

.field public hasQuickLinkFallbackTabIndex:Z

.field public hasQuickLinkTabIndex:Z

.field public hasServerLogsCookie:Z

.field public hasTitle:Z

.field public landingTabIndex:I

.field public promoUrl:Ljava/lang/String;

.field public quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

.field public quickLinkFallbackTabIndex:I

.field public quickLinkTabIndex:I

.field public serverLogsCookie:[B

.field public title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->clear()Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    .line 71
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Browse$BrowseResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->title:Ljava/lang/String;

    .line 75
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasTitle:Z

    .line 76
    iput v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->backendId:I

    .line 77
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasBackendId:Z

    .line 78
    invoke-static {}, Lcom/google/android/finsky/protos/Browse$BrowseTab;->emptyArray()[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    .line 79
    iput v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->landingTabIndex:I

    .line 80
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasLandingTabIndex:Z

    .line 81
    invoke-static {}, Lcom/google/android/finsky/protos/Browse$QuickLink;->emptyArray()[Lcom/google/android/finsky/protos/Browse$QuickLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    .line 82
    iput v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkTabIndex:I

    .line 83
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasQuickLinkTabIndex:Z

    .line 84
    iput v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkFallbackTabIndex:I

    .line 85
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasQuickLinkFallbackTabIndex:Z

    .line 86
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->serverLogsCookie:[B

    .line 87
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasServerLogsCookie:Z

    .line 88
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->contentsUrl:Ljava/lang/String;

    .line 89
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasContentsUrl:Z

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->promoUrl:Ljava/lang/String;

    .line 91
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasPromoUrl:Z

    .line 92
    invoke-static {}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->emptyArray()[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 93
    invoke-static {}, Lcom/google/android/finsky/protos/Browse$BrowseLink;->emptyArray()[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 94
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->cachedSize:I

    .line 95
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 162
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 163
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasContentsUrl:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->contentsUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 164
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->contentsUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 167
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasPromoUrl:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->promoUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 168
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->promoUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 171
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 172
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 173
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    aget-object v0, v3, v1

    .line 174
    .local v0, "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-eqz v0, :cond_4

    .line 175
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 172
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .end local v1    # "i":I
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 181
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 182
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    aget-object v0, v3, v1

    .line 183
    .restart local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-eqz v0, :cond_6

    .line 184
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 181
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 189
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .end local v1    # "i":I
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 190
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 191
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    aget-object v0, v3, v1

    .line 192
    .local v0, "element":Lcom/google/android/finsky/protos/Browse$QuickLink;
    if-eqz v0, :cond_8

    .line 193
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 190
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 198
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$QuickLink;
    .end local v1    # "i":I
    :cond_9
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasServerLogsCookie:Z

    if-nez v3, :cond_a

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->serverLogsCookie:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_b

    .line 199
    :cond_a
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->serverLogsCookie:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 202
    :cond_b
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasTitle:Z

    if-nez v3, :cond_c

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 203
    :cond_c
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 206
    :cond_d
    iget v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->backendId:I

    if-nez v3, :cond_e

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasBackendId:Z

    if-eqz v3, :cond_f

    .line 207
    :cond_e
    const/16 v3, 0x8

    iget v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->backendId:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 210
    :cond_f
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    array-length v3, v3

    if-lez v3, :cond_11

    .line 211
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    array-length v3, v3

    if-ge v1, v3, :cond_11

    .line 212
    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    aget-object v0, v3, v1

    .line 213
    .local v0, "element":Lcom/google/android/finsky/protos/Browse$BrowseTab;
    if-eqz v0, :cond_10

    .line 214
    const/16 v3, 0x9

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 211
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 219
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .end local v1    # "i":I
    :cond_11
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasLandingTabIndex:Z

    if-nez v3, :cond_12

    iget v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->landingTabIndex:I

    if-eqz v3, :cond_13

    .line 220
    :cond_12
    const/16 v3, 0xa

    iget v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->landingTabIndex:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 223
    :cond_13
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasQuickLinkTabIndex:Z

    if-nez v3, :cond_14

    iget v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkTabIndex:I

    if-eqz v3, :cond_15

    .line 224
    :cond_14
    const/16 v3, 0xb

    iget v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkTabIndex:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 227
    :cond_15
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasQuickLinkFallbackTabIndex:Z

    if-nez v3, :cond_16

    iget v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkFallbackTabIndex:I

    if-eqz v3, :cond_17

    .line 228
    :cond_16
    const/16 v3, 0xc

    iget v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkFallbackTabIndex:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 231
    :cond_17
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Browse$BrowseResponse;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 239
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 240
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 244
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 245
    :sswitch_0
    return-object p0

    .line 250
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->contentsUrl:Ljava/lang/String;

    .line 251
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasContentsUrl:Z

    goto :goto_0

    .line 255
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->promoUrl:Ljava/lang/String;

    .line 256
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasPromoUrl:Z

    goto :goto_0

    .line 260
    :sswitch_3
    const/16 v6, 0x1a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 262
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-nez v6, :cond_2

    move v1, v5

    .line 263
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 265
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-eqz v1, :cond_1

    .line 266
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 268
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 269
    new-instance v6, Lcom/google/android/finsky/protos/Browse$BrowseLink;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Browse$BrowseLink;-><init>()V

    aput-object v6, v2, v1

    .line 270
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 271
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 268
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 262
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v1, v6

    goto :goto_1

    .line 274
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/Browse$BrowseLink;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Browse$BrowseLink;-><init>()V

    aput-object v6, v2, v1

    .line 275
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 276
    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    goto :goto_0

    .line 280
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    :sswitch_4
    const/16 v6, 0x22

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 282
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-nez v6, :cond_5

    move v1, v5

    .line 283
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Browse$BrowseLink;

    .line 285
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-eqz v1, :cond_4

    .line 286
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 288
    :cond_4
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_6

    .line 289
    new-instance v6, Lcom/google/android/finsky/protos/Browse$BrowseLink;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Browse$BrowseLink;-><init>()V

    aput-object v6, v2, v1

    .line 290
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 291
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 288
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 282
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v1, v6

    goto :goto_3

    .line 294
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    :cond_6
    new-instance v6, Lcom/google/android/finsky/protos/Browse$BrowseLink;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Browse$BrowseLink;-><init>()V

    aput-object v6, v2, v1

    .line 295
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 296
    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    goto/16 :goto_0

    .line 300
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseLink;
    :sswitch_5
    const/16 v6, 0x2a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 302
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    if-nez v6, :cond_8

    move v1, v5

    .line 303
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Browse$QuickLink;

    .line 305
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Browse$QuickLink;
    if-eqz v1, :cond_7

    .line 306
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 308
    :cond_7
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_9

    .line 309
    new-instance v6, Lcom/google/android/finsky/protos/Browse$QuickLink;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Browse$QuickLink;-><init>()V

    aput-object v6, v2, v1

    .line 310
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 311
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 308
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 302
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$QuickLink;
    :cond_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    array-length v1, v6

    goto :goto_5

    .line 314
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$QuickLink;
    :cond_9
    new-instance v6, Lcom/google/android/finsky/protos/Browse$QuickLink;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Browse$QuickLink;-><init>()V

    aput-object v6, v2, v1

    .line 315
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 316
    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    goto/16 :goto_0

    .line 320
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$QuickLink;
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->serverLogsCookie:[B

    .line 321
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasServerLogsCookie:Z

    goto/16 :goto_0

    .line 325
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->title:Ljava/lang/String;

    .line 326
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasTitle:Z

    goto/16 :goto_0

    .line 330
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 331
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    .line 343
    :pswitch_1
    iput v4, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->backendId:I

    .line 344
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasBackendId:Z

    goto/16 :goto_0

    .line 350
    .end local v4    # "value":I
    :sswitch_9
    const/16 v6, 0x4a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 352
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    if-nez v6, :cond_b

    move v1, v5

    .line 353
    .restart local v1    # "i":I
    :goto_7
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/Browse$BrowseTab;

    .line 355
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseTab;
    if-eqz v1, :cond_a

    .line 356
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 358
    :cond_a
    :goto_8
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_c

    .line 359
    new-instance v6, Lcom/google/android/finsky/protos/Browse$BrowseTab;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Browse$BrowseTab;-><init>()V

    aput-object v6, v2, v1

    .line 360
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 361
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 358
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 352
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseTab;
    :cond_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    array-length v1, v6

    goto :goto_7

    .line 364
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseTab;
    :cond_c
    new-instance v6, Lcom/google/android/finsky/protos/Browse$BrowseTab;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/Browse$BrowseTab;-><init>()V

    aput-object v6, v2, v1

    .line 365
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 366
    iput-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    goto/16 :goto_0

    .line 370
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Browse$BrowseTab;
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->landingTabIndex:I

    .line 371
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasLandingTabIndex:Z

    goto/16 :goto_0

    .line 375
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkTabIndex:I

    .line 376
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasQuickLinkTabIndex:Z

    goto/16 :goto_0

    .line 380
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkFallbackTabIndex:I

    .line 381
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasQuickLinkFallbackTabIndex:Z

    goto/16 :goto_0

    .line 240
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch

    .line 331
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Browse$BrowseResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 101
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasContentsUrl:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->contentsUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 102
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->contentsUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 104
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasPromoUrl:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->promoUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 105
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->promoUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 107
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 108
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 109
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->category:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    aget-object v0, v2, v1

    .line 110
    .local v0, "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-eqz v0, :cond_4

    .line 111
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 108
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 115
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .end local v1    # "i":I
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 116
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 117
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->breadcrumb:[Lcom/google/android/finsky/protos/Browse$BrowseLink;

    aget-object v0, v2, v1

    .line 118
    .restart local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    if-eqz v0, :cond_6

    .line 119
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 116
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 123
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseLink;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 124
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 125
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLink:[Lcom/google/android/finsky/protos/Browse$QuickLink;

    aget-object v0, v2, v1

    .line 126
    .local v0, "element":Lcom/google/android/finsky/protos/Browse$QuickLink;
    if-eqz v0, :cond_8

    .line 127
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 124
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 131
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$QuickLink;
    .end local v1    # "i":I
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasServerLogsCookie:Z

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->serverLogsCookie:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_b

    .line 132
    :cond_a
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->serverLogsCookie:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 134
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasTitle:Z

    if-nez v2, :cond_c

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 135
    :cond_c
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 137
    :cond_d
    iget v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->backendId:I

    if-nez v2, :cond_e

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasBackendId:Z

    if-eqz v2, :cond_f

    .line 138
    :cond_e
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->backendId:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 140
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 141
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 142
    iget-object v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->browseTab:[Lcom/google/android/finsky/protos/Browse$BrowseTab;

    aget-object v0, v2, v1

    .line 143
    .local v0, "element":Lcom/google/android/finsky/protos/Browse$BrowseTab;
    if-eqz v0, :cond_10

    .line 144
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 141
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 148
    .end local v0    # "element":Lcom/google/android/finsky/protos/Browse$BrowseTab;
    .end local v1    # "i":I
    :cond_11
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasLandingTabIndex:Z

    if-nez v2, :cond_12

    iget v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->landingTabIndex:I

    if-eqz v2, :cond_13

    .line 149
    :cond_12
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->landingTabIndex:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 151
    :cond_13
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasQuickLinkTabIndex:Z

    if-nez v2, :cond_14

    iget v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkTabIndex:I

    if-eqz v2, :cond_15

    .line 152
    :cond_14
    const/16 v2, 0xb

    iget v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkTabIndex:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 154
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->hasQuickLinkFallbackTabIndex:Z

    if-nez v2, :cond_16

    iget v2, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkFallbackTabIndex:I

    if-eqz v2, :cond_17

    .line 155
    :cond_16
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/android/finsky/protos/Browse$BrowseResponse;->quickLinkFallbackTabIndex:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 157
    :cond_17
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 158
    return-void
.end method
