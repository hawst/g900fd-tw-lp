.class public Lcom/google/android/finsky/api/AccountHandler;
.super Ljava/lang/Object;
.source "AccountHandler.java"


# static fields
.field private static sSupportedAccountTypes:[Ljava/lang/String;


# direct methods
.method public static findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;
    .locals 12
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    .line 116
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    move-object v0, v10

    .line 128
    :cond_0
    :goto_0
    return-object v0

    .line 119
    :cond_1
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 120
    .local v3, "am":Landroid/accounts/AccountManager;
    invoke-static {}, Lcom/google/android/finsky/api/AccountHandler;->getAccountTypes()[Ljava/lang/String;

    move-result-object v4

    .local v4, "arr$":[Ljava/lang/String;
    array-length v8, v4

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v7, v6

    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v8    # "len$":I
    .local v7, "i$":I
    :goto_1
    if-ge v7, v8, :cond_3

    aget-object v1, v4, v7

    .line 121
    .local v1, "accountType":Ljava/lang/String;
    invoke-virtual {v3, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 122
    .local v2, "accounts":[Landroid/accounts/Account;
    move-object v5, v2

    .local v5, "arr$":[Landroid/accounts/Account;
    array-length v9, v5

    .local v9, "len$":I
    const/4 v6, 0x0

    .end local v7    # "i$":I
    .restart local v6    # "i$":I
    :goto_2
    if-ge v6, v9, :cond_2

    aget-object v0, v5, v6

    .line 123
    .local v0, "a":Landroid/accounts/Account;
    iget-object v11, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v11, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 122
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 120
    .end local v0    # "a":Landroid/accounts/Account;
    :cond_2
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    .end local v6    # "i$":I
    .restart local v7    # "i$":I
    goto :goto_1

    .end local v1    # "accountType":Ljava/lang/String;
    .end local v2    # "accounts":[Landroid/accounts/Account;
    .end local v5    # "arr$":[Landroid/accounts/Account;
    .end local v9    # "len$":I
    :cond_3
    move-object v0, v10

    .line 128
    goto :goto_0
.end method

.method public static getAccountFromPreferences(Landroid/content/Context;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;)Landroid/accounts/Account;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/accounts/Account;"
        }
    .end annotation

    .prologue
    .line 151
    .local p1, "preference":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/String;>;"
    invoke-virtual {p1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 152
    .local v0, "currentAccountName":Ljava/lang/String;
    invoke-static {v0, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    .line 153
    .local v1, "result":Landroid/accounts/Account;
    if-eqz v1, :cond_0

    move-object v2, v1

    .line 161
    .end local v1    # "result":Landroid/accounts/Account;
    .local v2, "result":Landroid/accounts/Account;
    :goto_0
    return-object v2

    .line 159
    .end local v2    # "result":Landroid/accounts/Account;
    .restart local v1    # "result":Landroid/accounts/Account;
    :cond_0
    invoke-static {p0}, Lcom/google/android/finsky/api/AccountHandler;->getFirstAccount(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    .line 160
    invoke-static {v1, p1}, Lcom/google/android/finsky/api/AccountHandler;->saveAccountToPreferences(Landroid/accounts/Account;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;)V

    move-object v2, v1

    .line 161
    .end local v1    # "result":Landroid/accounts/Account;
    .restart local v2    # "result":Landroid/accounts/Account;
    goto :goto_0
.end method

.method public static declared-synchronized getAccountTypes()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 40
    const-class v3, Lcom/google/android/finsky/api/AccountHandler;

    monitor-enter v3

    :try_start_0
    sget-object v2, Lcom/google/android/finsky/api/AccountHandler;->sSupportedAccountTypes:[Ljava/lang/String;

    if-nez v2, :cond_0

    .line 42
    sget-object v2, Lcom/google/android/finsky/config/G;->supportedAccountTypes:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v2}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/finsky/utils/Utils;->commaUnpackStrings(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "extraTypes":[Ljava/lang/String;
    array-length v1, v0

    .line 45
    .local v1, "numExtraTypes":I
    add-int/lit8 v2, v1, 0x1

    new-array v2, v2, [Ljava/lang/String;

    sput-object v2, Lcom/google/android/finsky/api/AccountHandler;->sSupportedAccountTypes:[Ljava/lang/String;

    .line 46
    sget-object v2, Lcom/google/android/finsky/api/AccountHandler;->sSupportedAccountTypes:[Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "com.google"

    aput-object v5, v2, v4

    .line 47
    const/4 v2, 0x0

    sget-object v4, Lcom/google/android/finsky/api/AccountHandler;->sSupportedAccountTypes:[Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v0, v2, v4, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 49
    :cond_0
    sget-object v2, Lcom/google/android/finsky/api/AccountHandler;->sSupportedAccountTypes:[Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v3

    return-object v2

    .line 40
    .end local v1    # "numExtraTypes":I
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x0

    .line 58
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 59
    .local v2, "am":Landroid/accounts/AccountManager;
    const/4 v7, 0x0

    .line 60
    .local v7, "result":[Landroid/accounts/Account;
    invoke-static {}, Lcom/google/android/finsky/api/AccountHandler;->getAccountTypes()[Ljava/lang/String;

    move-result-object v3

    .local v3, "arr$":[Ljava/lang/String;
    array-length v5, v3

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v0, v3, v4

    .line 61
    .local v0, "accountType":Ljava/lang/String;
    invoke-virtual {v2, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 62
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v8, v1

    if-nez v8, :cond_0

    .line 60
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 65
    :cond_0
    if-nez v7, :cond_1

    .line 66
    move-object v7, v1

    goto :goto_1

    .line 68
    :cond_1
    array-length v8, v7

    array-length v9, v1

    add-int/2addr v8, v9

    new-array v6, v8, [Landroid/accounts/Account;

    .line 69
    .local v6, "newResult":[Landroid/accounts/Account;
    array-length v8, v7

    invoke-static {v7, v10, v6, v10, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    array-length v8, v7

    array-length v9, v1

    invoke-static {v1, v10, v6, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    move-object v7, v6

    goto :goto_1

    .line 75
    .end local v0    # "accountType":Ljava/lang/String;
    .end local v1    # "accounts":[Landroid/accounts/Account;
    .end local v6    # "newResult":[Landroid/accounts/Account;
    :cond_2
    if-nez v7, :cond_3

    .line 76
    new-array v7, v10, [Landroid/accounts/Account;

    .line 78
    :cond_3
    return-object v7
.end method

.method public static getFirstAccount(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 99
    .local v2, "am":Landroid/accounts/AccountManager;
    invoke-static {}, Lcom/google/android/finsky/api/AccountHandler;->getAccountTypes()[Ljava/lang/String;

    move-result-object v3

    .local v3, "arr$":[Ljava/lang/String;
    array-length v5, v3

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v3, v4

    .line 100
    .local v0, "accountType":Ljava/lang/String;
    invoke-virtual {v2, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 101
    .local v1, "accounts":[Landroid/accounts/Account;
    array-length v6, v1

    if-lez v6, :cond_0

    .line 102
    const/4 v6, 0x0

    aget-object v6, v1, v6

    .line 105
    .end local v0    # "accountType":Ljava/lang/String;
    .end local v1    # "accounts":[Landroid/accounts/Account;
    :goto_1
    return-object v6

    .line 99
    .restart local v0    # "accountType":Ljava/lang/String;
    .restart local v1    # "accounts":[Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 105
    .end local v0    # "accountType":Ljava/lang/String;
    .end local v1    # "accounts":[Landroid/accounts/Account;
    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static hasAccount(Ljava/lang/String;Landroid/content/Context;)Z
    .locals 1
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 139
    invoke-static {p0, p1}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static saveAccountToPreferences(Landroid/accounts/Account;Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;)V
    .locals 1
    .param p0, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "preference":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/String;>;"
    if-nez p0, :cond_0

    .line 92
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    goto :goto_0
.end method
