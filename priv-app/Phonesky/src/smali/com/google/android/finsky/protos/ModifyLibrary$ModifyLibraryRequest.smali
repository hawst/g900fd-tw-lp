.class public final Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ModifyLibrary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ModifyLibrary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ModifyLibraryRequest"
.end annotation


# instance fields
.field public forAddDocid:[Ljava/lang/String;

.field public forArchiveDocid:[Ljava/lang/String;

.field public forRemovalDocid:[Ljava/lang/String;

.field public hasLibraryId:Z

.field public libraryId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 39
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->clear()Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;

    .line 40
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;
    .locals 1

    .prologue
    .line 43
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->libraryId:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->hasLibraryId:Z

    .line 45
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    .line 46
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    .line 47
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->cachedSize:I

    .line 49
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 87
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 88
    .local v4, "size":I
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->hasLibraryId:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->libraryId:Ljava/lang/String;

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 89
    :cond_0
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->libraryId:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v5

    add-int/2addr v4, v5

    .line 92
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_4

    .line 93
    const/4 v0, 0x0

    .line 94
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 95
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_3

    .line 96
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 97
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 98
    add-int/lit8 v0, v0, 0x1

    .line 99
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 95
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 103
    .end local v2    # "element":Ljava/lang/String;
    :cond_3
    add-int/2addr v4, v1

    .line 104
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 106
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_7

    .line 107
    const/4 v0, 0x0

    .line 108
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 109
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_6

    .line 110
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 111
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_5

    .line 112
    add-int/lit8 v0, v0, 0x1

    .line 113
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 109
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 117
    .end local v2    # "element":Ljava/lang/String;
    :cond_6
    add-int/2addr v4, v1

    .line 118
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 120
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    if-eqz v5, :cond_a

    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_a

    .line 121
    const/4 v0, 0x0

    .line 122
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 123
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_9

    .line 124
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 125
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_8

    .line 126
    add-int/lit8 v0, v0, 0x1

    .line 127
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 123
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 131
    .end local v2    # "element":Ljava/lang/String;
    :cond_9
    add-int/2addr v4, v1

    .line 132
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 134
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_a
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 142
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 143
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 147
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 148
    :sswitch_0
    return-object p0

    .line 153
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->libraryId:Ljava/lang/String;

    .line 154
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->hasLibraryId:Z

    goto :goto_0

    .line 158
    :sswitch_2
    const/16 v5, 0x12

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 160
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 161
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 162
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 163
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 166
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 167
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 165
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 160
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 170
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 171
    iput-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    goto :goto_0

    .line 175
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 177
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    if-nez v5, :cond_5

    move v1, v4

    .line 178
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 179
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 180
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 182
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 183
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 184
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 182
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 177
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_3

    .line 187
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 188
    iput-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    goto :goto_0

    .line 192
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 194
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    if-nez v5, :cond_8

    move v1, v4

    .line 195
    .restart local v1    # "i":I
    :goto_5
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 196
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_7

    .line 197
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 199
    :cond_7
    :goto_6
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_9

    .line 200
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 201
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 199
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 194
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_5

    .line 204
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 205
    iput-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    goto/16 :goto_0

    .line 143
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->hasLibraryId:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->libraryId:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 56
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->libraryId:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 58
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 59
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 60
    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forAddDocid:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 61
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 62
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 59
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 66
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 67
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 68
    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forRemovalDocid:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 69
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 70
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 67
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 74
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 75
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 76
    iget-object v2, p0, Lcom/google/android/finsky/protos/ModifyLibrary$ModifyLibraryRequest;->forArchiveDocid:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 77
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 78
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 75
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 82
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 83
    return-void
.end method
