.class Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;
.super Lcom/google/android/finsky/providers/SuggestionFetcher;
.source "MusicSearchSuggestionFetcher.java"


# static fields
.field static final BASE_URI:Landroid/net/Uri;


# instance fields
.field private mRequestUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "http://www.google.com/complete/search"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->BASE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/providers/SuggestionHandler;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "suggestionHandler"    # Lcom/google/android/finsky/providers/SuggestionHandler;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/providers/SuggestionFetcher;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/providers/SuggestionHandler;)V

    .line 30
    invoke-direct {p0}, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->constructUrl()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->mRequestUrl:Ljava/lang/String;

    .line 31
    return-void
.end method

.method private constructUrl()Ljava/lang/String;
    .locals 4

    .prologue
    .line 68
    sget-object v1, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "q"

    iget-object v3, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->mQuery:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "json"

    const-string v3, "1"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "hl"

    invoke-virtual {p0}, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "gl"

    invoke-virtual {p0}, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ds"

    const-string v3, "cse"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "client"

    const-string v3, "partner"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "partnerid"

    const-string v3, "skyjam-store"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 76
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected makeRequest(Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    .prologue
    .line 35
    iget-object v1, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->mQuery:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 36
    invoke-interface {p1}, Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;->onComplete()V

    .line 65
    :goto_0
    return-void

    .line 39
    :cond_0
    new-instance v0, Lcom/android/volley/toolbox/JsonArrayRequest;

    iget-object v1, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->mRequestUrl:Ljava/lang/String;

    new-instance v2, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;-><init>(Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V

    new-instance v3, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$2;

    invoke-direct {v3, p0, p1}, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$2;-><init>(Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/android/volley/toolbox/JsonArrayRequest;-><init>(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)V

    .line 63
    .local v0, "request":Lcom/android/volley/toolbox/JsonArrayRequest;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/RequestQueue;->add(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    .line 64
    invoke-virtual {p0}, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->startRequestLatencyTimer()V

    goto :goto_0
.end method
