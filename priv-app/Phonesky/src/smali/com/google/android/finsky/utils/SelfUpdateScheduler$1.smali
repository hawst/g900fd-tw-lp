.class Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;
.super Ljava/lang/Object;
.source "SelfUpdateScheduler.java"

# interfaces
.implements Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/SelfUpdateScheduler;->checkForSelfUpdate(ILjava/lang/String;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

.field final synthetic val$accountName:Ljava/lang/String;

.field final synthetic val$certificateHash:Ljava/lang/String;

.field final synthetic val$certificateHashSelfUpdateMD5:Ljava/lang/String;

.field final synthetic val$serverMarketVersion:I

.field final synthetic val$vendingPackageName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->this$0:Lcom/google/android/finsky/utils/SelfUpdateScheduler;

    iput-object p2, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$accountName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$vendingPackageName:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$serverMarketVersion:I

    iput-object p5, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$certificateHashSelfUpdateMD5:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$certificateHash:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTokenReceived(Ljava/lang/String;)V
    .locals 13
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 180
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$accountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$vendingPackageName:Ljava/lang/String;

    const/4 v2, 0x1

    iget v4, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$serverMarketVersion:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v7, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$certificateHashSelfUpdateMD5:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;->val$certificateHash:Ljava/lang/String;

    new-instance v11, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1$1;

    invoke-direct {v11, p0}, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1$1;-><init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;)V

    new-instance v12, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1$2;

    invoke-direct {v12, p0}, Lcom/google/android/finsky/utils/SelfUpdateScheduler$1$2;-><init>(Lcom/google/android/finsky/utils/SelfUpdateScheduler$1;)V

    move-object v5, v3

    move-object v6, v3

    move-object v9, v3

    move-object v10, p1

    invoke-interface/range {v0 .. v12}, Lcom/google/android/finsky/api/DfeApi;->delivery(Ljava/lang/String;I[BLjava/lang/Integer;Ljava/lang/Integer;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 215
    return-void
.end method
