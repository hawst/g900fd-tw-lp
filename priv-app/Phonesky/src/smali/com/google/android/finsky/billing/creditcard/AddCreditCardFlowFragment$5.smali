.class Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$5;
.super Ljava/lang/Object;
.source "AddCreditCardFlowFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->displayErrors(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

.field final synthetic val$inputValidationErrors:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 416
    iput-object p1, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$5;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    iput-object p2, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$5;->val$inputValidationErrors:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 420
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 421
    .local v1, "errorFields":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/widget/TextView;>;"
    iget-object v6, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$5;->val$inputValidationErrors:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    .line 422
    .local v3, "numInputValidationErrors":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 423
    iget-object v6, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$5;->val$inputValidationErrors:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .line 424
    .local v0, "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    iget-object v6, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$5;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    # invokes: Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;
    invoke-static {v6, v0}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->access$600(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;

    move-result-object v4

    .line 425
    .local v4, "textView":Landroid/widget/TextView;
    if-eqz v4, :cond_0

    .line 426
    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 422
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 431
    .end local v0    # "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .end local v4    # "textView":Landroid/widget/TextView;
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment$5;->this$0:Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;

    # getter for: Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->mMainView:Landroid/view/ViewGroup;
    invoke-static {v6}, Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;->access$700(Lcom/google/android/finsky/billing/creditcard/AddCreditCardFlowFragment;)Landroid/view/ViewGroup;

    move-result-object v6

    invoke-static {v6, v1}, Lcom/google/android/finsky/billing/BillingUtils;->getTopMostView(Landroid/view/ViewGroup;Ljava/util/Collection;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 432
    .local v5, "topMostErrorField":Landroid/widget/TextView;
    if-eqz v5, :cond_2

    .line 433
    invoke-virtual {v5}, Landroid/widget/TextView;->requestFocus()Z

    .line 435
    :cond_2
    return-void
.end method
