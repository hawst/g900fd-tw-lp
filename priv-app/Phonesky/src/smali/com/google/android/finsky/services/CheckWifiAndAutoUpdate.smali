.class public Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;
.super Landroid/app/Service;
.source "CheckWifiAndAutoUpdate.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;->onLoaded()V

    return-void
.end method

.method private loadLibrariesAndAutoUpdate()V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate$1;-><init>(Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;)V

    .line 54
    .local v0, "continueRunnable":Ljava/lang/Runnable;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/Libraries;->load(Ljava/lang/Runnable;)V

    .line 55
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    .line 56
    invoke-static {v0}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->initialize(Ljava/lang/Runnable;)V

    .line 57
    return-void
.end method

.method private onLoaded()V
    .locals 7

    .prologue
    .line 60
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    .line 61
    .local v2, "libraries":Lcom/google/android/finsky/library/Libraries;
    new-instance v0, Lcom/google/android/finsky/appstate/UpdateChecker;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v4

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/appstate/UpdateChecker;-><init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/utils/Notifier;)V

    .line 68
    .local v0, "updateChecker":Lcom/google/android/finsky/appstate/UpdateChecker;
    new-instance v1, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate$2;-><init>(Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;)V

    new-instance v3, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate$3;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate$3;-><init>(Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;)V

    const-string v4, "wifi_checker"

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/google/android/finsky/appstate/UpdateChecker;->checkForUpdates(Ljava/lang/Runnable;Ljava/lang/Runnable;Ljava/lang/String;Z)V

    .line 81
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 85
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x0

    .line 25
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v0

    .line 27
    .local v0, "installPolicies":Lcom/google/android/finsky/installer/InstallPolicies;
    invoke-virtual {v0}, Lcom/google/android/finsky/installer/InstallPolicies;->isWifiNetwork()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/finsky/installer/InstallPolicies;->isMobileHotspot()Z

    move-result v1

    if-nez v1, :cond_0

    .line 28
    const-string v1, "Checking wifi: enabled, proceeding with auto-update."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    invoke-direct {p0}, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;->loadLibrariesAndAutoUpdate()V

    .line 39
    :goto_0
    const/4 v1, 0x2

    return v1

    .line 31
    :cond_0
    const-string v1, "Checking wifi: disabled, will check wifi again later."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 32
    const-string v1, "wifi_checker"

    invoke-static {v3, v1, v3}, Lcom/google/android/finsky/appstate/UpdateChecker;->logWifiAutoUpdate(ZLjava/lang/String;Z)V

    .line 34
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateScheduler;->scheduleCheck(I)V

    .line 36
    invoke-virtual {p0}, Lcom/google/android/finsky/services/CheckWifiAndAutoUpdate;->stopSelf()V

    goto :goto_0
.end method
