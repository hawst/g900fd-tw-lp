.class public Lcom/google/android/finsky/services/RestoreService;
.super Landroid/app/Service;
.source "RestoreService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/services/RestoreService$5;,
        Lcom/google/android/finsky/services/RestoreService$RestoreResponseListener;,
        Lcom/google/android/finsky/services/RestoreService$RestoreTracker;,
        Lcom/google/android/finsky/services/RestoreService$FetchBitmapStatus;,
        Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;,
        Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    }
.end annotation


# static fields
.field private static final DEBUG_SELF_ANDROID_ID:Ljava/lang/Boolean;

.field private static sErrorRetryBlacklist:[I

.field private static sInstance:Lcom/google/android/finsky/services/RestoreService;


# instance fields
.field private mAddedInstallerListener:Z

.field private mAppIconSize:I

.field private mBitmapContainers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/play/image/BitmapLoader$BitmapContainer;",
            ">;"
        }
    .end annotation
.end field

.field private mDebugCountAlreadyInstalled:I

.field private mDebugCountAlreadyOtherAccount:I

.field private mDebugCountAlreadyTracked:I

.field private mDebugCountMaxAttemptsExceeded:I

.field private mHandledStartupIntent:Z

.field private mListener:Lcom/google/android/finsky/services/SetupHoldListener;

.field private mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

.field private mServiceStartId:I

.field private mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/services/RestoreService;->DEBUG_SELF_ANDROID_ID:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 211
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mBitmapContainers:Ljava/util/Map;

    .line 1439
    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/services/RestoreService;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Z

    .prologue
    .line 149
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/services/RestoreService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;

    .prologue
    .line 149
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mServiceStartId:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/finsky/services/RestoreService;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # I

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService;->inErrorRetryBlacklist(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/services/RestoreService;)Lcom/google/android/finsky/services/RestoreService$RestoreTracker;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    return-object v0
.end method

.method static synthetic access$1400(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-static {p0, p1}, Lcom/google/android/finsky/services/RestoreService;->getPackageRestoreIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/finsky/services/RestoreService;J)J
    .locals 3
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # J

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/services/RestoreService;->jitterDelay(J)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1600(Lcom/google/android/finsky/services/RestoreService;Landroid/content/Intent;J)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # J

    .prologue
    .line 149
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/services/RestoreService;->setAlarm(Landroid/content/Intent;J)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1700(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/services/RestoreService;->startBitmapDownload(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1800(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService;->cancelBitmapDownload(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/finsky/services/RestoreService;Landroid/content/Intent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService;->handleIntent(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/google/android/finsky/services/RestoreService;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/finsky/services/RestoreService;->addInstallerListener()V

    return-void
.end method

.method static synthetic access$2400(Lcom/google/android/finsky/services/RestoreService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;

    .prologue
    .line 149
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyTracked:I

    return v0
.end method

.method static synthetic access$2402(Lcom/google/android/finsky/services/RestoreService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyTracked:I

    return p1
.end method

.method static synthetic access$2500(Lcom/google/android/finsky/services/RestoreService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;

    .prologue
    .line 149
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyOtherAccount:I

    return v0
.end method

.method static synthetic access$2502(Lcom/google/android/finsky/services/RestoreService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyOtherAccount:I

    return p1
.end method

.method static synthetic access$2600(Lcom/google/android/finsky/services/RestoreService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;

    .prologue
    .line 149
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyInstalled:I

    return v0
.end method

.method static synthetic access$2602(Lcom/google/android/finsky/services/RestoreService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyInstalled:I

    return p1
.end method

.method static synthetic access$2700(Lcom/google/android/finsky/services/RestoreService;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;

    .prologue
    .line 149
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountMaxAttemptsExceeded:I

    return v0
.end method

.method static synthetic access$2702(Lcom/google/android/finsky/services/RestoreService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # I

    .prologue
    .line 149
    iput p1, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountMaxAttemptsExceeded:I

    return p1
.end method

.method static synthetic access$2800(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/receivers/Installer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Lcom/google/android/finsky/receivers/Installer;

    .prologue
    .line 149
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/services/RestoreService;->shouldRestore(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/receivers/Installer;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 149
    invoke-static {p0}, Lcom/google/android/finsky/services/RestoreService;->getKickIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3000(Lcom/google/android/finsky/services/RestoreService;Lcom/android/volley/VolleyError;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/RestoreService;->volleyErrorToInstallerError(Lcom/android/volley/VolleyError;)I

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/content/Context;

    .prologue
    .line 149
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/services/RestoreService;->getRestoreIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3200(Lcom/google/android/finsky/services/RestoreService;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mBitmapContainers:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/RestoreService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/graphics/Bitmap;

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/services/RestoreService;->deliverBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method private addInstallerListener()V
    .locals 2

    .prologue
    .line 428
    iget-boolean v0, p0, Lcom/google/android/finsky/services/RestoreService;->mAddedInstallerListener:Z

    if-nez v0, :cond_0

    .line 429
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/receivers/Installer;->addListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/services/RestoreService;->mAddedInstallerListener:Z

    .line 432
    :cond_0
    return-void
.end method

.method private cancelBitmapDownload(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1837
    const-string v1, "Canceling bitmap for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1839
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mBitmapContainers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .line 1840
    .local v0, "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    if-eqz v0, :cond_0

    .line 1841
    invoke-virtual {v0}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->cancelRequest()V

    .line 1844
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {v1, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->finishBitmap(Ljava/lang/String;)V

    .line 1845
    return-void
.end method

.method private deliverBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1851
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->setAppIcon(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 1852
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->finishBitmap(Ljava/lang/String;)V

    .line 1853
    return-void
.end method

.method private doRetryPackage(Ljava/lang/String;)Z
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 1393
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v9

    .line 1394
    .local v9, "installer":Lcom/google/android/finsky/receivers/Installer;
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->tryAgainPackage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1396
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1397
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # invokes: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writePackageStatus(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$2300(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Ljava/lang/String;)V

    move v0, v11

    .line 1431
    :goto_0
    return v0

    .line 1401
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 1403
    .local v10, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    iget v0, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->versionCode:I

    iget-object v1, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->accountName:Ljava/lang/String;

    invoke-direct {p0, p1, v0, v1, v9}, Lcom/google/android/finsky/services/RestoreService;->shouldRestore(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/receivers/Installer;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1404
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1405
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # invokes: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writePackageStatus(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$2300(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Ljava/lang/String;)V

    move v0, v11

    .line 1406
    goto :goto_0

    .line 1410
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    iget v2, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->versionCode:I

    iget-object v3, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->accountName:Ljava/lang/String;

    iget-object v4, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->title:Ljava/lang/String;

    iget v5, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->priority:I

    iget-object v6, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->deliveryToken:Ljava/lang/String;

    iget-boolean v7, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->visible:Z

    iget-object v8, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->appIconUrl:Ljava/lang/String;

    move-object v1, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->startPackage(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    .line 1414
    iget-boolean v0, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->visible:Z

    invoke-interface {v9, p1, v0, v11, v11}, Lcom/google/android/finsky/receivers/Installer;->setVisibility(Ljava/lang/String;ZZZ)V

    .line 1415
    iget-object v0, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->deliveryToken:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1416
    iget-object v0, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->deliveryToken:Ljava/lang/String;

    invoke-interface {v9, p1, v0}, Lcom/google/android/finsky/receivers/Installer;->setDeliveryToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    :cond_2
    iget v2, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->versionCode:I

    iget-object v3, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->accountName:Ljava/lang/String;

    iget-object v4, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->title:Ljava/lang/String;

    const-string v6, "restore"

    iget v7, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->priority:I

    move-object v0, v9

    move-object v1, p1

    move v5, v11

    invoke-interface/range {v0 .. v7}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 1428
    iget-object v0, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->appIconUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1429
    iget-object v0, v10, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->appIconUrl:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/services/RestoreService;->startBitmapDownload(Ljava/lang/String;Ljava/lang/String;)V

    .line 1431
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static getKickIntent(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3
    .param p0, "appContext"    # Landroid/content/Context;

    .prologue
    .line 398
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/finsky/services/RestoreService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 399
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "kick_installer"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 400
    const-string v1, "restoreservice://kick"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 401
    return-object v0
.end method

.method private static getPackageRestoreIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 382
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/finsky/services/RestoreService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 383
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "package"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 384
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 385
    .local v1, "ub":Landroid/net/Uri$Builder;
    const-string v2, "restoreservice"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "restorepackage"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 386
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 387
    return-object v0
.end method

.method private static getRestoreIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;
    .locals 4
    .param p0, "androidId"    # Ljava/lang/String;
    .param p1, "singleAccount"    # Ljava/lang/String;
    .param p2, "appContext"    # Landroid/content/Context;

    .prologue
    .line 363
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/google/android/finsky/services/RestoreService;

    invoke-direct {v0, p2, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 364
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "aid"

    invoke-virtual {v0, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 365
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 366
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 367
    .local v1, "ub":Landroid/net/Uri$Builder;
    const-string v2, "restoreservice"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "restoreaccount"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 368
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 369
    invoke-virtual {v1, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 371
    :cond_0
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 372
    return-object v0
.end method

.method private handleIntent(Landroid/content/Intent;)Z
    .locals 33
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1190
    const-string v4, "startup"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1191
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/services/RestoreService;->handleStartupIntent()Z

    move-result v4

    .line 1293
    :goto_0
    return v4

    .line 1193
    :cond_0
    const-string v4, "kick_installer"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1195
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/finsky/receivers/Installer;->startDeferredInstalls()V

    .line 1196
    const/4 v4, 0x0

    goto :goto_0

    .line 1198
    :cond_1
    const-string v4, "package"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1200
    invoke-direct/range {p0 .. p1}, Lcom/google/android/finsky/services/RestoreService;->handleRetryPackageIntent(Landroid/content/Intent;)Z

    move-result v4

    goto :goto_0

    .line 1204
    :cond_2
    const-string v4, "array_packages"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1205
    const-string v4, "authAccount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1206
    .local v5, "accountName":Ljava/lang/String;
    const-string v4, "visible"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v11

    .line 1207
    .local v11, "visible":Z
    const-string v4, "array_packages"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v26

    .line 1208
    .local v26, "packageNames":[Ljava/lang/String;
    const-string v4, "array_version_codes"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v32

    .line 1209
    .local v32, "versionCodes":[I
    const-string v4, "array_titles"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v31

    .line 1210
    .local v31, "titles":[Ljava/lang/String;
    const-string v4, "array_priorities"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v27

    .line 1211
    .local v27, "priorities":[I
    const-string v4, "array_delivery_tokens"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v18

    .line 1212
    .local v18, "deliveryTokens":[Ljava/lang/String;
    const-string v4, "array_app_icon_urls"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    .line 1214
    .local v16, "appIconUrls":[Ljava/lang/String;
    const/16 v25, 0x0

    .line 1215
    .local v25, "numRestored":I
    const/16 v22, 0x0

    .local v22, "i":I
    :goto_1
    move-object/from16 v0, v26

    array-length v4, v0

    move/from16 v0, v22

    if-ge v0, v4, :cond_5

    .line 1216
    if-eqz v18, :cond_4

    aget-object v10, v18, v22

    .line 1217
    .local v10, "deliveryToken":Ljava/lang/String;
    :goto_2
    aget-object v6, v26, v22

    aget v7, v32, v22

    aget-object v8, v31, v22

    aget v9, v27, v22

    aget-object v12, v16, v22

    move-object/from16 v4, p0

    invoke-direct/range {v4 .. v12}, Lcom/google/android/finsky/services/RestoreService;->restorePackage(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1219
    add-int/lit8 v25, v25, 0x1

    .line 1215
    :cond_3
    add-int/lit8 v22, v22, 0x1

    goto :goto_1

    .line 1216
    .end local v10    # "deliveryToken":Ljava/lang/String;
    :cond_4
    const/4 v10, 0x0

    goto :goto_2

    .line 1222
    :cond_5
    const-string v4, "Start restore of %d packages (%d skipped) for acct:%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, v26

    array-length v8, v0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, v26

    array-length v8, v0

    sub-int v8, v8, v25

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v5}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1225
    if-lez v25, :cond_6

    .line 1226
    sget-object v4, Lcom/google/android/finsky/utils/FinskyPreferences;->setupWizardStartDownloads:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v4}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1229
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/finsky/receivers/Installer;->startDeferredInstalls()V

    .line 1242
    :cond_6
    :goto_3
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1236
    :cond_7
    sget-object v4, Lcom/google/android/finsky/config/G;->appRestoreFailsafeMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    .line 1237
    .local v20, "holdoff":J
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/services/RestoreService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/services/RestoreService;->getKickIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v4

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/finsky/services/RestoreService;->setAlarm(Landroid/content/Intent;J)J

    goto :goto_3

    .line 1248
    .end local v5    # "accountName":Ljava/lang/String;
    .end local v11    # "visible":Z
    .end local v16    # "appIconUrls":[Ljava/lang/String;
    .end local v18    # "deliveryTokens":[Ljava/lang/String;
    .end local v20    # "holdoff":J
    .end local v22    # "i":I
    .end local v25    # "numRestored":I
    .end local v26    # "packageNames":[Ljava/lang/String;
    .end local v27    # "priorities":[I
    .end local v31    # "titles":[Ljava/lang/String;
    .end local v32    # "versionCodes":[I
    :cond_8
    const-string v4, "aid"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1250
    .local v15, "aidString":Ljava/lang/String;
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1251
    const-string v4, "Expecting a non-empty aid extra"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1252
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1256
    :cond_9
    sget-object v4, Lcom/google/android/finsky/services/RestoreService;->DEBUG_SELF_ANDROID_ID:Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_a

    const-string v4, "self"

    invoke-virtual {v15, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1258
    sget-object v4, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v28

    .line 1259
    .local v28, "selfAid":J
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v15

    .line 1260
    const-string v4, "Using own current android-id %s for test restore"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v15, v6, v7

    invoke-static {v4, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1265
    .end local v28    # "selfAid":J
    :cond_a
    const/16 v4, 0x10

    :try_start_0
    invoke-static {v15, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1271
    const-string v4, "authAccount"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    .line 1272
    .local v30, "specificAccount":Ljava/lang/String;
    if-eqz v30, :cond_d

    .line 1274
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    move-object/from16 v0, v30

    invoke-static {v0, v4}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v4

    if-nez v4, :cond_b

    .line 1275
    const-string v4, "Can\'t find restore acct:%s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {v30 .. v30}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1276
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1266
    .end local v30    # "specificAccount":Ljava/lang/String;
    :catch_0
    move-exception v19

    .line 1267
    .local v19, "e":Ljava/lang/NumberFormatException;
    const-string v4, "Provided aid can\'t be parsed as long"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1268
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1278
    .end local v19    # "e":Ljava/lang/NumberFormatException;
    .restart local v30    # "specificAccount":Ljava/lang/String;
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-direct {v0, v15, v1}, Lcom/google/android/finsky/services/RestoreService;->restore(Ljava/lang/String;Ljava/lang/String;)V

    .line 1293
    :cond_c
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1281
    :cond_d
    invoke-static/range {p0 .. p0}, Lcom/google/android/finsky/api/AccountHandler;->getAccounts(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v14

    .line 1284
    .local v14, "accounts":[Landroid/accounts/Account;
    array-length v4, v14

    if-gtz v4, :cond_e

    .line 1285
    const-string v4, "RestoreService can\'t run - no accounts configured on device!"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v6}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1286
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1289
    :cond_e
    move-object/from16 v17, v14

    .local v17, "arr$":[Landroid/accounts/Account;
    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v24, v0

    .local v24, "len$":I
    const/16 v23, 0x0

    .local v23, "i$":I
    :goto_4
    move/from16 v0, v23

    move/from16 v1, v24

    if-ge v0, v1, :cond_c

    aget-object v13, v17, v23

    .line 1290
    .local v13, "account":Landroid/accounts/Account;
    iget-object v4, v13, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v4}, Lcom/google/android/finsky/services/RestoreService;->restore(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    add-int/lit8 v23, v23, 0x1

    goto :goto_4
.end method

.method private handleRetryPackageIntent(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1379
    const-string v1, "package"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1381
    .local v0, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->isDownloadingOrInstalling()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1382
    const/4 v1, 0x1

    .line 1384
    :goto_0
    return v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/finsky/services/RestoreService;->doRetryPackage(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private handleStartupIntent()Z
    .locals 14

    .prologue
    .line 1313
    iget-boolean v9, p0, Lcom/google/android/finsky/services/RestoreService;->mHandledStartupIntent:Z

    if-eqz v9, :cond_1

    .line 1314
    const-string v9, "Redelivery of startup intent - dropping it"

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1315
    const/4 v3, 0x0

    .line 1370
    :cond_0
    return v3

    .line 1317
    :cond_1
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/google/android/finsky/services/RestoreService;->mHandledStartupIntent:Z

    .line 1319
    const/4 v3, 0x0

    .line 1323
    .local v3, "keepServiceRunning":Z
    iget-object v9, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$300(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_2

    .line 1325
    iget-object v9, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$300(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1326
    .local v0, "accountName":Ljava/lang/String;
    const-string v9, "Recover fetch for account %s"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v0}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1327
    iget-object v9, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mAccountStatusMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$300(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;

    .line 1328
    .local v8, "status":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    iget-object v9, v8, Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;->androidId:Ljava/lang/String;

    invoke-direct {p0, v9, v0}, Lcom/google/android/finsky/services/RestoreService;->restore(Ljava/lang/String;Ljava/lang/String;)V

    .line 1329
    const/4 v3, 0x1

    .line 1330
    goto :goto_0

    .line 1333
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v8    # "status":Lcom/google/android/finsky/services/RestoreService$AccountFetchStatus;
    :cond_2
    iget-object v9, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 1334
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v2

    .line 1336
    .local v2, "installer":Lcom/google/android/finsky/receivers/Installer;
    iget-object v9, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/finsky/utils/Lists;->newArrayList(Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v5

    .line 1337
    .local v5, "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1340
    .local v4, "packageName":Ljava/lang/String;
    invoke-interface {v2, v4}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->isDownloadingOrInstalling()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1341
    const/4 v3, 0x1

    .line 1342
    goto :goto_1

    .line 1346
    :cond_4
    iget-object v9, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 1347
    .local v8, "status":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    if-eqz v8, :cond_3

    .line 1351
    iget-wide v10, v8, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->retryTime:J

    const-wide/16 v12, 0x0

    cmp-long v9, v10, v12

    if-eqz v9, :cond_5

    .line 1353
    iget-wide v10, v8, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->retryTime:J

    sget-object v9, Lcom/google/android/finsky/config/G;->appRestoreRetryDownloadHoldoffMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v9}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    add-long v6, v10, v12

    .line 1355
    .local v6, "retryTimePlusBuffer":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    cmp-long v9, v10, v6

    if-ltz v9, :cond_3

    .line 1360
    .end local v6    # "retryTimePlusBuffer":J
    :cond_5
    const-string v9, "Overdue alarm for %s so retry immediately"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v4, v10, v11

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1361
    invoke-direct {p0, v4}, Lcom/google/android/finsky/services/RestoreService;->doRetryPackage(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1365
    iget-object v9, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v9}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v9

    invoke-interface {v9, v4}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1366
    iget-object v9, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # invokes: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->writePackageStatus(Ljava/lang/String;)V
    invoke-static {v9, v4}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$2300(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private inErrorRetryBlacklist(I)Z
    .locals 5
    .param p1, "statusCode"    # I

    .prologue
    .line 1115
    sget-object v3, Lcom/google/android/finsky/services/RestoreService;->sErrorRetryBlacklist:[I

    if-nez v3, :cond_0

    .line 1116
    sget-object v3, Lcom/google/android/finsky/config/G;->appRestoreHttpStatusBlacklist:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/finsky/utils/Utils;->commaUnpackStrings(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1117
    .local v0, "codes":[Ljava/lang/String;
    array-length v3, v0

    new-array v3, v3, [I

    sput-object v3, Lcom/google/android/finsky/services/RestoreService;->sErrorRetryBlacklist:[I

    .line 1118
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, v0

    if-ge v1, v3, :cond_0

    .line 1120
    :try_start_0
    sget-object v3, Lcom/google/android/finsky/services/RestoreService;->sErrorRetryBlacklist:[I

    aget-object v4, v0, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    aput v4, v3, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1118
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1121
    :catch_0
    move-exception v2

    .line 1122
    .local v2, "nfe":Ljava/lang/NumberFormatException;
    sget-object v3, Lcom/google/android/finsky/services/RestoreService;->sErrorRetryBlacklist:[I

    const/high16 v4, -0x80000000

    aput v4, v3, v1

    goto :goto_1

    .line 1127
    .end local v0    # "codes":[Ljava/lang/String;
    .end local v1    # "i":I
    .end local v2    # "nfe":Ljava/lang/NumberFormatException;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    sget-object v3, Lcom/google/android/finsky/services/RestoreService;->sErrorRetryBlacklist:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 1128
    sget-object v3, Lcom/google/android/finsky/services/RestoreService;->sErrorRetryBlacklist:[I

    aget v3, v3, v1

    if-ne p1, v3, :cond_1

    .line 1129
    const/4 v3, 0x1

    .line 1132
    :goto_3
    return v3

    .line 1127
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1132
    :cond_2
    const/4 v3, 0x0

    goto :goto_3
.end method

.method private jitterDelay(J)J
    .locals 11
    .param p1, "delay"    # J

    .prologue
    .line 1140
    const-wide/high16 v4, 0x3fe8000000000000L    # 0.75

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    add-double v0, v4, v6

    .line 1141
    .local v0, "jitter":D
    long-to-double v4, p1

    mul-double/2addr v4, v0

    double-to-long v2, v4

    .line 1142
    .local v2, "result":J
    return-wide v2
.end method

.method private notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6
    .param p1, "eventCode"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "cancelable"    # Z

    .prologue
    .line 1930
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    if-eqz v0, :cond_0

    .line 1931
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    const-string v5, "RestoreService"

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/finsky/services/SetupHoldListener;->onStatusChange(ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 1932
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 1933
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    .line 1936
    :cond_0
    return-void
.end method

.method public static recoverRestore(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 273
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 274
    .local v0, "appContext":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/finsky/services/RestoreService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 275
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "startup"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 276
    const-string v2, "restoreservice://startup"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 277
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 278
    return-void
.end method

.method private registerAndNotifyListener(Lcom/google/android/finsky/services/SetupHoldListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/google/android/finsky/services/SetupHoldListener;

    .prologue
    .line 1899
    iput-object p1, p0, Lcom/google/android/finsky/services/RestoreService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    .line 1901
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/finsky/services/RestoreService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/finsky/services/RestoreService$4;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/services/RestoreService$4;-><init>(Lcom/google/android/finsky/services/RestoreService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1922
    return-void
.end method

.method public static registerListener(Lcom/google/android/finsky/services/SetupHoldListener;)Z
    .locals 3
    .param p0, "listener"    # Lcom/google/android/finsky/services/SetupHoldListener;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 1879
    if-nez p0, :cond_1

    .line 1880
    sget-object v1, Lcom/google/android/finsky/services/RestoreService;->sInstance:Lcom/google/android/finsky/services/RestoreService;

    if-eqz v1, :cond_0

    .line 1881
    sget-object v1, Lcom/google/android/finsky/services/RestoreService;->sInstance:Lcom/google/android/finsky/services/RestoreService;

    iput-object v2, v1, Lcom/google/android/finsky/services/RestoreService;->mListener:Lcom/google/android/finsky/services/SetupHoldListener;

    .line 1892
    :cond_0
    :goto_0
    return v0

    .line 1886
    :cond_1
    sget-object v1, Lcom/google/android/finsky/services/RestoreService;->sInstance:Lcom/google/android/finsky/services/RestoreService;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/finsky/services/RestoreService;->sInstance:Lcom/google/android/finsky/services/RestoreService;

    iget-object v1, v1, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->shouldHold(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1888
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1891
    :cond_3
    sget-object v1, Lcom/google/android/finsky/services/RestoreService;->sInstance:Lcom/google/android/finsky/services/RestoreService;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/services/RestoreService;->registerAndNotifyListener(Lcom/google/android/finsky/services/SetupHoldListener;)V

    goto :goto_0
.end method

.method private restore(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "aid"    # Ljava/lang/String;
    .param p2, "accountName"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1577
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->isAccountInFlight(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1578
    const-string v1, "Skip restore acct:%s already started"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1618
    :goto_0
    return-void

    .line 1581
    :cond_0
    const-wide/16 v8, 0x0

    .line 1583
    .local v8, "aidLong":J
    const/16 v1, 0x10

    :try_start_0
    invoke-static {p1, v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v8

    .line 1588
    move-wide v4, v8

    .line 1590
    .local v4, "finalAid":J
    const-string v1, "Start restore aid:%s acct:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {p2}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1592
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {v1, p2, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->startAccount(Ljava/lang/String;Ljava/lang/String;)V

    .line 1593
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    new-instance v1, Lcom/google/android/finsky/services/RestoreService$2;

    move-object v2, p0

    move-object v3, p2

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/finsky/services/RestoreService$2;-><init>(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;JLjava/lang/String;)V

    invoke-static {v7, v1}, Lcom/google/android/finsky/utils/ConsistencyTokenHelper;->get(Landroid/content/Context;Lcom/google/android/finsky/utils/ConsistencyTokenHelper$Listener;)V

    goto :goto_0

    .line 1584
    .end local v4    # "finalAid":J
    :catch_0
    move-exception v0

    .line 1585
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v1, "Provided aid can\'t be parsed as long: %s"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static restoreAccounts(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "androidId"    # Ljava/lang/String;
    .param p2, "singleAccount"    # Ljava/lang/String;

    .prologue
    .line 294
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->directedRestoreStarted:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 295
    const-string v2, "Skipping restore for %s because directedRestoreStarted=true"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 303
    :goto_0
    return-void

    .line 300
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 301
    .local v0, "appContext":Landroid/content/Context;
    invoke-static {p1, p2, v0}, Lcom/google/android/finsky/services/RestoreService;->getRestoreIntent(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    .line 302
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method private restorePackage(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Z
    .locals 11
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "versionCode"    # I
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "priorityLevel"    # I
    .param p6, "deliveryToken"    # Ljava/lang/String;
    .param p7, "visible"    # Z
    .param p8, "appIconUrl"    # Ljava/lang/String;

    .prologue
    .line 1628
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v10

    .line 1629
    .local v10, "installer":Lcom/google/android/finsky/receivers/Installer;
    invoke-direct {p0, p2, p3, p1, v10}, Lcom/google/android/finsky/services/RestoreService;->shouldRestore(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/receivers/Installer;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1630
    const/4 v1, 0x0

    .line 1658
    :goto_0
    return v1

    .line 1634
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    move-object v2, p2

    move v3, p3

    move-object v4, p1

    move-object v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->startPackage(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    .line 1640
    const/4 v1, 0x0

    const/4 v2, 0x0

    move/from16 v0, p7

    invoke-interface {v10, p2, v0, v1, v2}, Lcom/google/android/finsky/receivers/Installer;->setVisibility(Ljava/lang/String;ZZZ)V

    .line 1642
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1643
    move-object/from16 v0, p6

    invoke-interface {v10, p2, v0}, Lcom/google/android/finsky/receivers/Installer;->setDeliveryToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 1645
    :cond_1
    const/4 v6, 0x1

    const-string v7, "restore"

    move-object v1, v10

    move-object v2, p2

    move v3, p3

    move-object v4, p1

    move-object v5, p4

    move/from16 v8, p5

    invoke-interface/range {v1 .. v8}, Lcom/google/android/finsky/receivers/Installer;->requestInstall(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 1655
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1656
    move-object/from16 v0, p8

    invoke-direct {p0, p2, v0}, Lcom/google/android/finsky/services/RestoreService;->startBitmapDownload(Ljava/lang/String;Ljava/lang/String;)V

    .line 1658
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static restorePackages(Landroid/content/Context;ZLjava/lang/String;Z[Ljava/lang/String;[I[Ljava/lang/String;[I[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "userDirected"    # Z
    .param p2, "accountName"    # Ljava/lang/String;
    .param p3, "visible"    # Z
    .param p4, "packageNames"    # [Ljava/lang/String;
    .param p5, "versionCodes"    # [I
    .param p6, "titles"    # [Ljava/lang/String;
    .param p7, "priorities"    # [I
    .param p8, "deliveryTokens"    # [Ljava/lang/String;
    .param p9, "appIconUrls"    # [Ljava/lang/String;

    .prologue
    .line 333
    if-eqz p1, :cond_0

    .line 334
    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->directedRestoreStarted:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 337
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 339
    .local v0, "appContext":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/finsky/services/RestoreService;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 340
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    const-string v2, "visible"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 342
    const-string v2, "array_packages"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 343
    const-string v2, "array_version_codes"

    invoke-virtual {v1, v2, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 344
    const-string v2, "array_titles"

    invoke-virtual {v1, v2, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 345
    const-string v2, "array_priorities"

    invoke-virtual {v1, v2, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 346
    if-eqz p8, :cond_1

    .line 347
    const-string v2, "array_delivery_tokens"

    invoke-virtual {v1, v2, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 349
    :cond_1
    const-string v2, "array_app_icon_urls"

    invoke-virtual {v1, v2, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    const-string v2, "restoreservice://restorepackages"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 352
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 353
    return-void
.end method

.method private setAlarm(Landroid/content/Intent;J)J
    .locals 10
    .param p1, "withIntent"    # Landroid/content/Intent;
    .param p2, "intervalMs"    # J

    .prologue
    const/4 v8, 0x0

    .line 1552
    invoke-virtual {p1}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1553
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v6, "Alarm intent needs data URI"

    invoke-direct {v3, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1555
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/services/RestoreService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1556
    .local v1, "context":Landroid/content/Context;
    const-string v3, "alarm"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 1557
    .local v0, "alarmManager":Landroid/app/AlarmManager;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long v4, v6, p2

    .line 1558
    .local v4, "wakeTimeMs":J
    invoke-static {v1, v8, p1, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1559
    .local v2, "pending":Landroid/app/PendingIntent;
    invoke-virtual {v0, v8, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 1560
    return-wide v4
.end method

.method public static shouldHold()Z
    .locals 2

    .prologue
    .line 1865
    sget-object v0, Lcom/google/android/finsky/services/RestoreService;->sInstance:Lcom/google/android/finsky/services/RestoreService;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/services/RestoreService;->sInstance:Lcom/google/android/finsky/services/RestoreService;

    iget-object v0, v0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->shouldHold(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldRestore(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/receivers/Installer;)Z
    .locals 11
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "packageVersion"    # I
    .param p3, "accountName"    # Ljava/lang/String;
    .param p4, "installer"    # Lcom/google/android/finsky/receivers/Installer;

    .prologue
    const/4 v10, 0x2

    const/4 v5, 0x0

    const/16 v1, 0x71

    const/4 v9, 0x1

    const/4 v4, 0x0

    .line 1711
    new-instance v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;

    invoke-direct {v6}, Lcom/google/android/finsky/analytics/PlayStore$AppData;-><init>()V

    .line 1712
    .local v6, "appData":Lcom/google/android/finsky/analytics/PlayStore$AppData;
    iput p2, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->version:I

    .line 1713
    iput-boolean v9, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasVersion:Z

    .line 1717
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->tryAgainPackage(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1719
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountMaxAttemptsExceeded:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountMaxAttemptsExceeded:I

    .line 1720
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const-string v3, "retry-expired"

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1764
    :goto_0
    return v4

    .line 1728
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # getter for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mPackageStatusMap:Ljava/util/Map;
    invoke-static {v0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$700(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;

    .line 1729
    .local v8, "packageStatus":Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;
    if-eqz v8, :cond_1

    iget-object v0, v8, Lcom/google/android/finsky/services/RestoreService$PackageInstallStatus;->accountName:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1730
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyOtherAccount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyOtherAccount:I

    .line 1731
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const-string v3, "other-account"

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1734
    const-string v0, "Skipping restore of %s v:%d because already restoring for another account"

    new-array v1, v10, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1740
    :cond_1
    invoke-interface {p4, p1}, Lcom/google/android/finsky/receivers/Installer;->getState(Ljava/lang/String;)Lcom/google/android/finsky/receivers/Installer$InstallerState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/receivers/Installer$InstallerState;->isDownloadingOrInstalling()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1741
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyTracked:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyTracked:I

    .line 1742
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const-string v3, "is-tracked"

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1745
    const-string v0, "Skipping restore of %s because already restoring"

    new-array v1, v9, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1750
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v7

    .line 1751
    .local v7, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-eqz v7, :cond_3

    iget v0, v7, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    if-lt v0, p2, :cond_3

    .line 1752
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyInstalled:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/finsky/services/RestoreService;->mDebugCountAlreadyInstalled:I

    .line 1753
    iget v0, v7, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    iput v0, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->oldVersion:I

    .line 1754
    iput-boolean v9, v6, Lcom/google/android/finsky/analytics/PlayStore$AppData;->hasOldVersion:Z

    .line 1755
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const-string v3, "already-installed"

    move-object v2, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1758
    const-string v0, "Skipping restore of %s v:%d because v:%d is installed"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v9

    iget v2, v7, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v10

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1763
    :cond_3
    const-string v0, "Should attempt restore of %s"

    new-array v1, v9, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v4, v9

    .line 1764
    goto/16 :goto_0
.end method

.method private startBitmapDownload(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "bitmapUrl"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v4, 0x0

    .line 1780
    iget v0, p0, Lcom/google/android/finsky/services/RestoreService;->mAppIconSize:I

    if-gez v0, :cond_1

    .line 1831
    :cond_0
    :goto_0
    return-void

    .line 1784
    :cond_1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1788
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mBitmapContainers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1789
    const-string v0, "Request for already-downloading bitmap for %s"

    new-array v1, v8, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1793
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->startBitmap(Ljava/lang/String;Ljava/lang/String;)V

    .line 1796
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v0

    iget v2, p0, Lcom/google/android/finsky/services/RestoreService;->mAppIconSize:I

    iget v3, p0, Lcom/google/android/finsky/services/RestoreService;->mAppIconSize:I

    new-instance v5, Lcom/google/android/finsky/services/RestoreService$3;

    invoke-direct {v5, p0, p1}, Lcom/google/android/finsky/services/RestoreService$3;-><init>(Lcom/google/android/finsky/services/RestoreService;Ljava/lang/String;)V

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IIZLcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v7

    .line 1821
    .local v7, "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    invoke-virtual {v7}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 1822
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_3

    .line 1823
    const-string v0, "Received cached bitmap for %s"

    new-array v1, v8, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1825
    invoke-direct {p0, p1, v6}, Lcom/google/android/finsky/services/RestoreService;->deliverBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 1827
    :cond_3
    const-string v0, "Waiting for bitmap for %s"

    new-array v1, v8, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1829
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mBitmapContainers:Ljava/util/Map;

    invoke-interface {v0, p1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private volleyErrorToInstallerError(Lcom/android/volley/VolleyError;)I
    .locals 1
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 1665
    instance-of v0, p1, Lcom/android/volley/AuthFailureError;

    if-eqz v0, :cond_0

    .line 1666
    const/16 v0, 0x398

    .line 1689
    :goto_0
    return v0

    .line 1668
    :cond_0
    instance-of v0, p1, Lcom/google/android/volley/DisplayMessageError;

    if-eqz v0, :cond_1

    .line 1669
    const/16 v0, 0x399

    goto :goto_0

    .line 1671
    :cond_1
    instance-of v0, p1, Lcom/google/android/finsky/api/DfeServerError;

    if-eqz v0, :cond_2

    .line 1672
    const/16 v0, 0x39a

    goto :goto_0

    .line 1674
    :cond_2
    instance-of v0, p1, Lcom/android/volley/NetworkError;

    if-eqz v0, :cond_3

    .line 1675
    const/16 v0, 0x39b

    goto :goto_0

    .line 1677
    :cond_3
    instance-of v0, p1, Lcom/android/volley/NoConnectionError;

    if-eqz v0, :cond_4

    .line 1678
    const/16 v0, 0x39c

    goto :goto_0

    .line 1680
    :cond_4
    instance-of v0, p1, Lcom/android/volley/ParseError;

    if-eqz v0, :cond_5

    .line 1681
    const/16 v0, 0x39d

    goto :goto_0

    .line 1683
    :cond_5
    instance-of v0, p1, Lcom/android/volley/ServerError;

    if-eqz v0, :cond_6

    .line 1684
    const/16 v0, 0x39e

    goto :goto_0

    .line 1686
    :cond_6
    instance-of v0, p1, Lcom/android/volley/TimeoutError;

    if-eqz v0, :cond_7

    .line 1687
    const/16 v0, 0x39f

    goto :goto_0

    .line 1689
    :cond_7
    const/16 v0, 0x3a0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1770
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 406
    sput-object p0, Lcom/google/android/finsky/services/RestoreService;->sInstance:Lcom/google/android/finsky/services/RestoreService;

    .line 407
    new-instance v0, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;-><init>(Lcom/google/android/finsky/services/RestoreService;Lcom/google/android/finsky/services/RestoreService$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    .line 408
    invoke-static {}, Lcom/google/android/finsky/installer/PackageInstallerFactory;->getPackageInstaller()Lcom/google/android/finsky/installer/PackageInstallerFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    .line 409
    iget-object v0, p0, Lcom/google/android/finsky/services/RestoreService;->mPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    invoke-interface {v0}, Lcom/google/android/finsky/installer/PackageInstallerFacade;->getAppIconSize()I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/services/RestoreService;->mAppIconSize:I

    .line 410
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 414
    iget-boolean v0, p0, Lcom/google/android/finsky/services/RestoreService;->mAddedInstallerListener:Z

    if-eqz v0, :cond_0

    .line 415
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/receivers/Installer;->removeListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 417
    :cond_0
    iput-object v2, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    .line 418
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v2, v1}, Lcom/google/android/finsky/services/RestoreService;->notifyListener(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 419
    sput-object v2, Lcom/google/android/finsky/services/RestoreService;->sInstance:Lcom/google/android/finsky/services/RestoreService;

    .line 420
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 1157
    iput p3, p0, Lcom/google/android/finsky/services/RestoreService;->mServiceStartId:I

    .line 1158
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    # operator++ for: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->mStartupRefCount:I
    invoke-static {v1}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$1908(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;)I

    .line 1159
    new-instance v0, Lcom/google/android/finsky/services/RestoreService$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/services/RestoreService$1;-><init>(Lcom/google/android/finsky/services/RestoreService;Landroid/content/Intent;)V

    .line 1175
    .local v0, "continueRunnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {p0}, Lcom/google/android/finsky/services/RestoreService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->initAccountStore(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 1176
    iget-object v1, p0, Lcom/google/android/finsky/services/RestoreService;->mTracker:Lcom/google/android/finsky/services/RestoreService$RestoreTracker;

    invoke-virtual {p0}, Lcom/google/android/finsky/services/RestoreService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    # invokes: Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->initPackagesStore(Landroid/content/Context;Ljava/lang/Runnable;)V
    invoke-static {v1, v2, v0}, Lcom/google/android/finsky/services/RestoreService$RestoreTracker;->access$2200(Lcom/google/android/finsky/services/RestoreService$RestoreTracker;Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 1177
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/Libraries;->load(Ljava/lang/Runnable;)V

    .line 1178
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    .line 1180
    const/4 v1, 0x3

    return v1
.end method
