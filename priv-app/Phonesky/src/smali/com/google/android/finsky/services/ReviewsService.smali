.class public Lcom/google/android/finsky/services/ReviewsService;
.super Landroid/app/Service;
.source "ReviewsService.java"


# static fields
.field private static sRequestCounter:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final mBinder:Lcom/android/vending/reviews/IReviewsService$Stub;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/android/finsky/services/ReviewsService;->sRequestCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/finsky/services/ReviewsService$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/services/ReviewsService$1;-><init>(Lcom/google/android/finsky/services/ReviewsService;)V

    iput-object v0, p0, Lcom/google/android/finsky/services/ReviewsService;->mBinder:Lcom/android/vending/reviews/IReviewsService$Stub;

    return-void
.end method

.method static synthetic access$000()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/finsky/services/ReviewsService;->sRequestCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/services/ReviewsService;->mBinder:Lcom/android/vending/reviews/IReviewsService$Stub;

    return-object v0
.end method
