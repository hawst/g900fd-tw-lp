.class final Lcom/google/android/finsky/utils/PurchaseInitiator$3;
.super Ljava/lang/Object;
.source "PurchaseInitiator.java"

# interfaces
.implements Lcom/android/volley/Response$ErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/PurchaseInitiator;->createFreePurchaseErrorListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;IZ)Lcom/android/volley/Response$ErrorListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Landroid/accounts/Account;

.field final synthetic val$doc:Lcom/google/android/finsky/api/model/Document;

.field final synthetic val$offerType:I

.field final synthetic val$showErrors:Z


# direct methods
.method constructor <init>(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;IZ)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$account:Landroid/accounts/Account;

    iput-object p2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$doc:Lcom/google/android/finsky/api/model/Document;

    iput p3, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$offerType:I

    iput-boolean p4, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$showErrors:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 12
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    const-wide/16 v8, -0x1

    .line 213
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$account:Landroid/accounts/Account;

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    .line 215
    .local v1, "eventLog":Lcom/google/android/finsky/analytics/FinskyEventLog;
    const/16 v2, 0x12d

    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$offerType:I

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-wide v10, v8

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPurchaseBackgroundEvent(ILjava/lang/String;ILjava/lang/String;I[BJJ)V

    .line 219
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$showErrors:Z

    if-eqz v0, :cond_0

    .line 220
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    const v2, 0x7f0c01e0

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 221
    .local v3, "title":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v4

    .line 222
    .local v4, "errorMessage":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/finsky/utils/PurchaseInitiator$3;->val$doc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v7

    move-object v5, v4

    invoke-interface/range {v2 .. v7}, Lcom/google/android/finsky/utils/Notifier;->showPurchaseErrorMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    .end local v3    # "title":Ljava/lang/String;
    .end local v4    # "errorMessage":Ljava/lang/String;
    :cond_0
    return-void
.end method
