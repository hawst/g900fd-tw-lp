.class Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader$1;
.super Ljava/lang/Object;
.source "AppIconProvider.java"

# interfaces
.implements Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->loadToFileFromUrl(Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;

.field final synthetic val$listener:Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader$1;->this$0:Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;

    iput-object p2, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader$1;->val$listener:Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 3
    .param p1, "result"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    .line 231
    invoke-virtual {p1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 232
    const-string v0, "Failed to fetch icon."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    iget-object v0, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader$1;->val$listener:Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;

    invoke-virtual {v0}, Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;->callOnComplete()V

    .line 237
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader$1;->this$0:Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;

    invoke-virtual {p1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader$1;->val$listener:Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;

    # invokes: Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->loadToFileFromBitmap(Landroid/graphics/Bitmap;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->access$200(Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;Landroid/graphics/Bitmap;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 228
    check-cast p1, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader$1;->onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    return-void
.end method
