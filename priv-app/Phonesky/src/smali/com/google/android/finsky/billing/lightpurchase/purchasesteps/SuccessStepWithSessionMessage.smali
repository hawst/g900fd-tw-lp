.class public Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "SuccessStepWithSessionMessage.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

.field private mUsedPinBasedAuth:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 32
    const/16 v0, 0x308

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;
    .param p1, "x1"    # I

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->logClick(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->finish()V

    return-void
.end method

.method private finish()V
    .locals 1

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    .line 98
    .local v0, "purchaseFragment":Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
    if-eqz v0, :cond_0

    .line 99
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->finish()V

    .line 101
    :cond_0
    return-void
.end method

.method public static newInstance(Z)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;
    .locals 3
    .param p0, "usedPinBasedAuth"    # Z

    .prologue
    .line 41
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 42
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;-><init>()V

    .line 43
    .local v1, "stepFragment":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;
    const-string v2, "SuccessStepWithSessionMessage.usedPinBasedAuth"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 44
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->setArguments(Landroid/os/Bundle;)V

    .line 45
    return-object v1
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 81
    const v0, 0x7f0c02a0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onContinueButtonClicked()V
    .locals 1

    .prologue
    .line 86
    const/16 v0, 0x30a

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->logClick(I)V

    .line 87
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->finish()V

    .line 88
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 52
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "SuccessStepWithSessionMessage.usedPinBasedAuth"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->mUsedPinBasedAuth:Z

    .line 53
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    const v3, 0x7f0400cd

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 60
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0a00bb

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 61
    .local v1, "textView":Landroid/widget/TextView;
    iget-boolean v3, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->mUsedPinBasedAuth:Z

    if-eqz v3, :cond_0

    const v3, 0x7f0c0116

    :goto_0
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 64
    .local v0, "spanned":Landroid/text/Spanned;
    new-instance v3, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage$1;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage$1;-><init>(Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/SuccessStepWithSessionMessage;)V

    invoke-static {v0, v3}, Lcom/google/android/play/utils/UrlSpanUtils;->selfishifyUrlSpans(Ljava/lang/CharSequence;Lcom/google/android/play/utils/UrlSpanUtils$Listener;)V

    .line 74
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 76
    return-object v2

    .line 61
    .end local v0    # "spanned":Landroid/text/Spanned;
    :cond_0
    const v3, 0x7f0c0115

    goto :goto_0
.end method
