.class public abstract Lcom/google/android/finsky/layout/YoutubeFrameView;
.super Landroid/view/ViewGroup;
.source "YoutubeFrameView.java"

# interfaces
.implements Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;


# instance fields
.field protected mAccessibilityOverlayView:Landroid/view/View;

.field protected mCorpusFillView:Landroid/view/View;

.field protected mCurrentOverlayTopOffset:I

.field protected mHeroImageOverlayView:Landroid/view/View;

.field protected mHeroImageView:Lcom/google/android/play/image/FifeImageView;

.field protected mPlayImageView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/YoutubeFrameView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method


# virtual methods
.method protected getClickIntent(Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 3
    .param p1, "youtubeUrl"    # Ljava/lang/String;
    .param p2, "preferVideosAppForPlayback"    # Z

    .prologue
    .line 115
    if-eqz p2, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/IntentUtils;->createMovieTrailerIntentForUrl(Landroid/content/pm/PackageManager;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 125
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/IntentUtils;->createYouTubeIntentForUrl(Landroid/content/pm/PackageManager;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method public getOverlayableImageHeight()I
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->getHeight()I

    move-result v0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 56
    const v0, 0x7f0a0155

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    .line 57
    const v0, 0x7f0a0157

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mPlayImageView:Landroid/widget/ImageView;

    .line 58
    const v0, 0x7f0a0154

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mCorpusFillView:Landroid/view/View;

    .line 59
    const v0, 0x7f0a0156

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mHeroImageOverlayView:Landroid/view/View;

    .line 60
    const v0, 0x7f0a00d0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mAccessibilityOverlayView:Landroid/view/View;

    .line 61
    return-void
.end method

.method public varargs setContentDescription(I[Ljava/lang/Object;)V
    .locals 3
    .param p1, "resourceId"    # I
    .param p2, "formatArgs"    # [Ljava/lang/Object;

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 134
    .local v1, "context":Landroid/content/Context;
    if-nez p2, :cond_0

    invoke-virtual {v1, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 136
    .local v0, "contentDescription":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mAccessibilityOverlayView:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 137
    return-void

    .line 134
    .end local v0    # "contentDescription":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 105
    if-eqz p1, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mAccessibilityOverlayView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 107
    iget-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mAccessibilityOverlayView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mAccessibilityOverlayView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mAccessibilityOverlayView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setOverlayableImageTopPadding(I)V
    .locals 6
    .param p1, "topPadding"    # I

    .prologue
    const/4 v4, 0x0

    .line 146
    invoke-virtual {p0, v4, p1, v4, v4}, Lcom/google/android/finsky/layout/YoutubeFrameView;->setPadding(IIII)V

    .line 147
    iput p1, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mCurrentOverlayTopOffset:I

    .line 149
    iget-object v4, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->getTop()I

    move-result v2

    .line 150
    .local v2, "top":I
    sub-int v0, p1, v2

    .line 151
    .local v0, "newOffset":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4, v0}, Lcom/google/android/play/image/FifeImageView;->offsetTopAndBottom(I)V

    .line 152
    iget-object v4, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mCorpusFillView:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 153
    iget-object v4, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mCorpusFillView:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 155
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->offsetTopAndBottom(I)V

    .line 156
    iget-object v4, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mAccessibilityOverlayView:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 158
    iget-object v4, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mHeroImageOverlayView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v3

    .line 159
    .local v3, "topOverlay":I
    int-to-float v4, p1

    const/high16 v5, 0x3f000000    # 0.5f

    div-float/2addr v4, v5

    float-to-int v4, v4

    sub-int v1, v4, v3

    .line 161
    .local v1, "newOverlayOffset":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mHeroImageOverlayView:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 162
    return-void
.end method

.method public showHeroOverlay()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mHeroImageOverlayView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 68
    return-void
.end method

.method public showPlayIcon(Ljava/lang/String;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 9
    .param p1, "youtubeUrl"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "preferVideosAppForPlayback"    # Z
    .param p4, "isMature"    # Z
    .param p5, "backend"    # I
    .param p6, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v8, 0x0

    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/layout/YoutubeFrameView;->mPlayImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v4

    .line 79
    .local v4, "accountName":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/layout/YoutubeFrameView$1;

    move-object v1, p0

    move-object v2, p6

    move v3, p4

    move v5, p5

    move-object v6, p1

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/layout/YoutubeFrameView$1;-><init>(Lcom/google/android/finsky/layout/YoutubeFrameView;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ZLjava/lang/String;ILjava/lang/String;Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/YoutubeFrameView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 99
    const v0, 0x7f0c025a

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/YoutubeFrameView;->setContentDescription(I[Ljava/lang/Object;)V

    .line 101
    :cond_0
    return-void
.end method
