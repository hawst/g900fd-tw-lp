.class public Lcom/google/android/finsky/navigationmanager/NavigationManager;
.super Ljava/lang/Object;
.source "NavigationManager.java"


# static fields
.field private static RESPECT_TASKS_IN_UP:Z


# instance fields
.field private mActivity:Lcom/google/android/finsky/activities/MainActivity;

.field private final mBackStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/finsky/navigationmanager/NavigationState;",
            ">;"
        }
    .end annotation
.end field

.field protected mFragmentManager:Landroid/support/v4/app/FragmentManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 83
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->RESPECT_TASKS_IN_UP:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/finsky/activities/MainActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    new-instance v0, Lcom/google/android/finsky/utils/MainThreadStack;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/MainThreadStack;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    .line 107
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->init(Lcom/google/android/finsky/activities/MainActivity;)V

    .line 108
    return-void
.end method

.method public static areTransitionsEnabled()Z
    .locals 2

    .prologue
    .line 116
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getExperiments()Lcom/google/android/finsky/experiments/FinskyExperiments;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/experiments/FinskyExperiments;->shouldEnableTransitionAnimations()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private canNavigate()Z
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MainActivity;->isStateSaved()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getResolvedLinkClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p3, "resolvedLink"    # Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;
    .param p4, "searchQuery"    # Ljava/lang/String;
    .param p5, "searchBackend"    # I
    .param p6, "clickLogNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 839
    new-instance v0, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    move-object v5, p6

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager$4;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Ljava/lang/String;I)V

    return-object v0
.end method

.method private goBack(Z)Z
    .locals 8
    .param p1, "logClickEvent"    # Z

    .prologue
    const/4 v3, 0x0

    .line 644
    iget-object v4, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v4}, Lcom/google/android/finsky/activities/MainActivity;->isStateSaved()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 673
    :cond_0
    :goto_0
    return v3

    .line 648
    :cond_1
    if-eqz p1, :cond_2

    .line 649
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v4

    const/16 v5, 0x258

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 654
    :cond_2
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/utils/FinskyLog;->startTiming()V

    .line 655
    iget-object v4, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/navigationmanager/NavigationState;

    .line 656
    .local v0, "currentEntry":Lcom/google/android/finsky/navigationmanager/NavigationState;
    iget-object v4, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 658
    iget-object v4, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/navigationmanager/NavigationState;
    :try_end_0
    .catch Ljava/util/EmptyStackException; {:try_start_0 .. :try_end_0} :catch_0

    .line 666
    .local v2, "newStackEntry":Lcom/google/android/finsky/navigationmanager/NavigationState;
    const/4 v3, 0x1

    goto :goto_0

    .line 668
    .end local v0    # "currentEntry":Lcom/google/android/finsky/navigationmanager/NavigationState;
    .end local v2    # "newStackEntry":Lcom/google/android/finsky/navigationmanager/NavigationState;
    :catch_0
    move-exception v1

    .line 673
    .local v1, "ex":Ljava/util/EmptyStackException;
    goto :goto_0
.end method

.method private goToAggregatedHome()V
    .locals 1

    .prologue
    .line 205
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 206
    return-void
.end method

.method private goToDocPage(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Landroid/view/View;Landroid/view/View;)V
    .locals 24
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "originalUrl"    # Ljava/lang/String;
    .param p3, "continueUrl"    # Ljava/lang/String;
    .param p4, "replaceTop"    # Z
    .param p5, "overrideAccount"    # Ljava/lang/String;
    .param p6, "sharedPrimaryContainer"    # Landroid/view/View;
    .param p7, "sharedCoverView"    # Landroid/view/View;

    .prologue
    .line 525
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v4

    if-nez v4, :cond_0

    .line 588
    :goto_0
    return-void

    .line 529
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v23

    .line 530
    .local v23, "type":I
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasAntennaInfo()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 531
    const/4 v4, 0x5

    invoke-static/range {p1 .. p2}, Lcom/google/android/finsky/activities/AntennaFragment;->newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Lcom/google/android/finsky/activities/AntennaFragment;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/view/View;

    const/4 v9, 0x0

    aput-object p6, v6, v9

    const/4 v9, 0x1

    aput-object p7, v6, v9

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v4, v5, v1, v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    goto :goto_0

    .line 534
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->hasDealOfTheDayInfo()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 535
    const/4 v4, 0x5

    invoke-static/range {p1 .. p2}, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/view/View;

    const/4 v9, 0x0

    aput-object p6, v6, v9

    const/4 v9, 0x1

    aput-object p7, v6, v9

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v4, v5, v1, v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    goto :goto_0

    .line 540
    :cond_2
    sparse-switch v23, :sswitch_data_0

    .line 580
    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v14, p6

    move-object/from16 v15, p7

    invoke-static/range {v9 .. v15}, Lcom/google/android/finsky/activities/DetailsFragment;->newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/view/View;Landroid/view/View;)Lcom/google/android/finsky/activities/DetailsFragment;

    move-result-object v19

    .line 583
    .local v19, "detailsFragment":Lcom/google/android/finsky/activities/DetailsFragment;
    const/4 v4, 0x5

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/view/View;

    const/4 v6, 0x0

    aput-object p6, v5, v6

    const/4 v6, 0x1

    aput-object p7, v5, v6

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move/from16 v2, p4

    invoke-direct {v0, v4, v1, v2, v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    goto :goto_0

    .line 542
    .end local v19    # "detailsFragment":Lcom/google/android/finsky/activities/DetailsFragment;
    :sswitch_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v4}, Lcom/google/android/finsky/activities/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    .line 543
    .local v22, "resources":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v4}, Lcom/google/android/finsky/activities/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const v5, 0x7f0c01e0

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0c033a

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {v4, v5, v6, v9}, Lcom/google/android/finsky/activities/ErrorDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/finsky/activities/ErrorDialog;

    goto/16 :goto_0

    .line 550
    .end local v22    # "resources":Landroid/content/res/Resources;
    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v4

    iget-object v0, v4, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    move-object/from16 v21, v0

    .line 551
    .local v21, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v18

    .line 553
    .local v18, "currentAccountName":Ljava/lang/String;
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v8, 0x1

    .line 554
    .local v8, "overriding":Z
    :goto_1
    if-eqz v8, :cond_6

    .line 555
    move-object/from16 v7, p5

    .line 562
    .local v7, "accountForDetails":Ljava/lang/String;
    :goto_2
    if-nez v8, :cond_3

    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 563
    :cond_3
    const-string v4, "Selecting account %s for package %s. overriding=[%s]"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v7}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v5, v6

    const/4 v6, 0x1

    aput-object v21, v5, v6

    const/4 v6, 0x2

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    aput-object v9, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    .line 567
    invoke-static/range {v4 .. v10}, Lcom/google/android/finsky/activities/DetailsFragment;->newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLandroid/view/View;Landroid/view/View;)Lcom/google/android/finsky/activities/DetailsFragment;

    move-result-object v16

    .line 570
    .local v16, "appDetailsFragment":Lcom/google/android/finsky/activities/DetailsFragment;
    const/4 v4, 0x5

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/view/View;

    const/4 v6, 0x0

    aput-object p6, v5, v6

    const/4 v6, 0x1

    aput-object p7, v5, v6

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, p4

    invoke-direct {v0, v4, v1, v2, v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    goto/16 :goto_0

    .line 553
    .end local v7    # "accountForDetails":Ljava/lang/String;
    .end local v8    # "overriding":Z
    .end local v16    # "appDetailsFragment":Lcom/google/android/finsky/activities/DetailsFragment;
    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    .line 557
    .restart local v8    # "overriding":Z
    :cond_6
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v17

    .line 558
    .local v17, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v20

    .line 559
    .local v20, "libraries":Lcom/google/android/finsky/library/Libraries;
    move-object/from16 v0, v21

    move-object/from16 v1, v18

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/activities/AppActionAnalyzer;->getAppDetailsAccount(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/library/Libraries;)Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "accountForDetails":Ljava/lang/String;
    goto :goto_2

    .line 574
    .end local v7    # "accountForDetails":Ljava/lang/String;
    .end local v8    # "overriding":Z
    .end local v17    # "appStates":Lcom/google/android/finsky/appstate/AppStates;
    .end local v18    # "currentAccountName":Ljava/lang/String;
    .end local v20    # "libraries":Lcom/google/android/finsky/library/Libraries;
    .end local v21    # "packageName":Ljava/lang/String;
    :sswitch_2
    const/16 v4, 0xb

    invoke-static/range {p1 .. p2}, Lcom/google/android/finsky/activities/PeopleDetailsFragment;->newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Lcom/google/android/finsky/activities/PeopleDetailsFragment;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Landroid/view/View;

    const/4 v9, 0x0

    aput-object p6, v6, v9

    const/4 v9, 0x1

    aput-object p7, v6, v9

    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v4, v5, v1, v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    goto/16 :goto_0

    .line 540
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x7 -> :sswitch_0
        0x1c -> :sswitch_2
    .end sparse-switch
.end method

.method public static hasClickListener(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 1
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 824
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasLinkAnnotation()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasAntennaInfo()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasContainerAnnotation()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 828
    :cond_0
    const/4 v0, 0x1

    .line 830
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resolveCallToActionDismiss(Lcom/google/android/finsky/protos/DocumentV2$CallToAction;Lcom/google/android/finsky/api/DfeApi;)V
    .locals 3
    .param p1, "callToAction"    # Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    .line 1205
    iget-object v0, p1, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->dismissalUrl:Ljava/lang/String;

    new-instance v1, Lcom/google/android/finsky/navigationmanager/NavigationManager$7;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager$7;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->rateSuggestedContent(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 1210
    return-void
.end method

.method private varargs showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V
    .locals 7
    .param p1, "pageType"    # I
    .param p2, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p3, "replaceTop"    # Z
    .param p4, "sharedViews"    # [Landroid/view/View;

    .prologue
    .line 890
    invoke-static {}, Lcom/google/android/finsky/utils/FinskyLog;->startTiming()V

    .line 892
    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    .line 894
    .local v1, "ft":Landroid/support/v4/app/FragmentTransaction;
    invoke-static {}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->areTransitionsEnabled()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz p4, :cond_1

    .line 895
    move-object v0, p4

    .local v0, "arr$":[Landroid/view/View;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 896
    .local v4, "sharedView":Landroid/view/View;
    if-eqz v4, :cond_0

    .line 897
    invoke-virtual {v4}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v4, v6}, Landroid/support/v4/app/FragmentTransaction;->addSharedElement(Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 895
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 901
    .end local v0    # "arr$":[Landroid/view/View;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "sharedView":Landroid/view/View;
    :cond_1
    const/16 v6, 0x1003

    invoke-virtual {v1, v6}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    .line 903
    const v6, 0x7f0a00c4

    invoke-virtual {v1, v6, p2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 905
    if-eqz p3, :cond_2

    .line 906
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->popBackStack()V

    .line 908
    :cond_2
    new-instance v5, Lcom/google/android/finsky/navigationmanager/NavigationState;

    invoke-direct {v5, p1}, Lcom/google/android/finsky/navigationmanager/NavigationState;-><init>(I)V

    .line 910
    .local v5, "state":Lcom/google/android/finsky/navigationmanager/NavigationState;
    iget-object v6, v5, Lcom/google/android/finsky/navigationmanager/NavigationState;->backstackName:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 911
    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v6, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 913
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 914
    return-void
.end method

.method private unwindDetailsStack()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1112
    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v6}, Lcom/google/android/finsky/activities/MainActivity;->isStateSaved()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1162
    :cond_0
    :goto_0
    return-void

    .line 1116
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    .line 1117
    .local v5, "dfeToc":Lcom/google/android/finsky/api/model/DfeToc;
    if-eqz v5, :cond_0

    .line 1126
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1127
    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/navigationmanager/NavigationState;

    .line 1128
    .local v2, "currTop":Lcom/google/android/finsky/navigationmanager/NavigationState;
    iget v3, v2, Lcom/google/android/finsky/navigationmanager/NavigationState;->pageType:I

    .line 1129
    .local v3, "currType":I
    const/4 v6, 0x5

    if-eq v3, v6, :cond_3

    const/4 v6, 0x6

    if-eq v3, v6, :cond_3

    .line 1143
    .end local v2    # "currTop":Lcom/google/android/finsky/navigationmanager/NavigationState;
    .end local v3    # "currType":I
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1144
    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/finsky/navigationmanager/NavigationState;

    iget-object v6, v6, Lcom/google/android/finsky/navigationmanager/NavigationState;->backstackName:Ljava/lang/String;

    invoke-virtual {v7, v6, v8}, Landroid/support/v4/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    goto :goto_0

    .line 1136
    .restart local v2    # "currTop":Lcom/google/android/finsky/navigationmanager/NavigationState;
    .restart local v3    # "currType":I
    :cond_3
    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    goto :goto_1

    .line 1147
    .end local v2    # "currTop":Lcom/google/android/finsky/navigationmanager/NavigationState;
    .end local v3    # "currType":I
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v7, v8}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryAt(I)Landroid/support/v4/app/FragmentManager$BackStackEntry;

    move-result-object v7

    invoke-interface {v7}, Landroid/support/v4/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/support/v4/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 1149
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v4

    .line 1150
    .local v4, "currentDocument":Lcom/google/android/finsky/api/model/Document;
    if-eqz v4, :cond_5

    .line 1151
    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    .line 1152
    .local v0, "backendId":I
    invoke-virtual {v5, v0}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v1

    .line 1153
    .local v1, "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    if-eqz v1, :cond_5

    .line 1154
    iget-object v6, v1, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    iget-object v7, v1, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    invoke-virtual {p0, v6, v7, v0, v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToCorpusHome(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;)V

    goto :goto_0

    .line 1160
    .end local v0    # "backendId":I
    .end local v1    # "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    :cond_5
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome()V

    goto :goto_0
.end method


# virtual methods
.method public addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 1166
    return-void
.end method

.method public buy(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;)V
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "offerType"    # I
    .param p4, "filter"    # Lcom/google/android/finsky/utils/DocUtils$OfferFilter;
    .param p5, "appsContinueUrl"    # Ljava/lang/String;

    .prologue
    .line 973
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 974
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v4

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;[BLjava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 976
    .local v6, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    const/16 v1, 0x21

    invoke-virtual {v0, v6, v1}, Lcom/google/android/finsky/activities/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 978
    .end local v6    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public canGoUp()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1083
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->isBackStackEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 1105
    :cond_0
    :goto_0
    return v4

    .line 1087
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v5}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/navigationmanager/NavigationState;

    .line 1088
    .local v1, "currentState":Lcom/google/android/finsky/navigationmanager/NavigationState;
    iget v5, v1, Lcom/google/android/finsky/navigationmanager/NavigationState;->pageType:I

    if-eq v5, v3, :cond_0

    .line 1092
    iget v5, v1, Lcom/google/android/finsky/navigationmanager/NavigationState;->pageType:I

    const/4 v6, 0x2

    if-eq v5, v6, :cond_2

    move v4, v3

    .line 1094
    goto :goto_0

    .line 1097
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/fragments/PageFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v2

    .line 1098
    .local v2, "toc":Lcom/google/android/finsky/api/model/DfeToc;
    if-eqz v2, :cond_0

    .line 1103
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpusList()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 1105
    .local v0, "corporaCount":I
    if-le v0, v3, :cond_3

    :goto_1
    move v4, v3

    goto :goto_0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method public canSearch()Z
    .locals 1

    .prologue
    .line 1186
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentPageType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1190
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 1188
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1186
    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->removeAllElements()V

    .line 129
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0

    .line 132
    :cond_0
    return-void
.end method

.method public deserialize(Landroid/os/Bundle;)Z
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 678
    const-string v3, "nm_state"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 681
    .local v0, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/navigationmanager/NavigationState;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v3

    if-nez v3, :cond_1

    .line 684
    :cond_0
    const/4 v3, 0x0

    .line 697
    :goto_0
    return v3

    .line 687
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/navigationmanager/NavigationState;

    .line 688
    .local v2, "st":Lcom/google/android/finsky/navigationmanager/NavigationState;
    iget-object v3, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v3, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 695
    .end local v2    # "st":Lcom/google/android/finsky/navigationmanager/NavigationState;
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/fragments/PageFragment;->rebindActionBar()V

    .line 697
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public flush()Z
    .locals 1

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    move-result v0

    return v0
.end method

.method public getActivePage()Lcom/google/android/finsky/fragments/PageFragment;
    .locals 2

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/fragments/PageFragment;

    return-object v0
.end method

.method protected getActivityForResolveLink()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 1214
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    return-object v0
.end method

.method public getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
    .locals 9
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "offerType"    # I
    .param p4, "filter"    # Lcom/google/android/finsky/utils/DocUtils$OfferFilter;
    .param p5, "continueUrl"    # Ljava/lang/String;
    .param p6, "logElementType"    # I
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 955
    if-eqz p7, :cond_0

    move-object/from16 v3, p7

    .line 956
    .local v3, "rootNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :goto_0
    new-instance v0, Lcom/google/android/finsky/navigationmanager/NavigationManager$5;

    move-object v1, p0

    move v2, p6

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/finsky/navigationmanager/NavigationManager$5;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;)V

    return-object v0

    .line 955
    .end local v3    # "rootNode":Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v3

    goto :goto_0
.end method

.method public getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
    .locals 2
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "clickLogNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 758
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Ljava/lang/String;I)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public getClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Ljava/lang/String;I)Landroid/view/View$OnClickListener;
    .locals 7
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "clickLogNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p3, "searchQuery"    # Ljava/lang/String;
    .param p4, "searchBackend"    # I

    .prologue
    .line 764
    invoke-static {p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->hasClickListener(Lcom/google/android/finsky/api/model/Document;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 765
    const/4 v0, 0x0

    .line 793
    :goto_0
    return-object v0

    .line 767
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasLinkAnnotation()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 768
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getLinkAnnotation()Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-eqz v0, :cond_1

    .line 769
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getLinkAnnotation()Lcom/google/android/finsky/protos/DocAnnotations$Link;

    move-result-object v0

    iget-object v3, v0, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getResolvedLinkClickListener(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v0

    goto :goto_0

    .line 773
    :cond_1
    new-instance v0, Lcom/google/android/finsky/navigationmanager/NavigationManager$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/navigationmanager/NavigationManager$1;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0

    .line 782
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->hasContainerAnnotation()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getContainerAnnotation()Lcom/google/android/finsky/protos/Containers$ContainerMetadata;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/finsky/protos/Containers$ContainerMetadata;->browseUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 784
    new-instance v0, Lcom/google/android/finsky/navigationmanager/NavigationManager$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/navigationmanager/NavigationManager$2;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0

    .line 793
    :cond_3
    new-instance v0, Lcom/google/android/finsky/navigationmanager/NavigationManager$3;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/navigationmanager/NavigationManager$3;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0
.end method

.method public getCurrentDocument()Lcom/google/android/finsky/api/model/Document;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1001
    iget-object v2, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    if-nez v2, :cond_1

    .line 1011
    :cond_0
    :goto_0
    return-object v1

    .line 1004
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 1005
    .local v0, "active":Lcom/google/android/finsky/fragments/PageFragment;
    if-eqz v0, :cond_0

    .line 1008
    instance-of v2, v0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;

    if-eqz v2, :cond_0

    .line 1009
    check-cast v0, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;

    .end local v0    # "active":Lcom/google/android/finsky/fragments/PageFragment;
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    goto :goto_0
.end method

.method public getCurrentPageType()I
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/navigationmanager/NavigationState;

    iget v0, v0, Lcom/google/android/finsky/navigationmanager/NavigationState;->pageType:I

    goto :goto_0
.end method

.method public getOpenClickListener(Lcom/google/android/finsky/api/model/Document;Landroid/accounts/Account;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "account"    # Landroid/accounts/Account;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 983
    new-instance v0, Lcom/google/android/finsky/navigationmanager/NavigationManager$6;

    invoke-direct {v0, p0, p3, p2, p1}, Lcom/google/android/finsky/navigationmanager/NavigationManager$6;-><init>(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V

    return-object v0
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 640
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBack(Z)Z

    move-result v0

    return v0
.end method

.method public goBrowse(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "backendId"    # I
    .param p4, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5, "clickLogNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v3, 0x0

    .line 241
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 242
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    invoke-virtual {v1, p5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 245
    invoke-virtual {p4}, Lcom/google/android/finsky/api/model/DfeToc;->getSocialHomeUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 246
    invoke-virtual {p0, p1, p4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToSocialHome(Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 260
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    const/4 v0, 0x4

    .line 251
    .local v0, "pageType":I
    if-eqz p4, :cond_2

    invoke-virtual {p4, p1}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(Ljava/lang/String;)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 254
    const/4 v0, 0x2

    .line 257
    :cond_2
    const/4 v1, 0x0

    invoke-static {p1, p2, p3, p4, v1}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Landroid/util/Pair;)Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    goto :goto_0
.end method

.method public goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;)V
    .locals 9
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 163
    if-nez p1, :cond_0

    .line 191
    :goto_0
    return-void

    .line 166
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 168
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->clear()V

    .line 169
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpusList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v8, :cond_1

    .line 171
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpusList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    .line 172
    .local v0, "corpusMetadata":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    iget-object v3, v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->name:Ljava/lang/String;

    iget v5, v0, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->backend:I

    const/4 v6, 0x0

    invoke-static {v3, v4, v5, p1, v6}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Landroid/util/Pair;)Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    move-result-object v1

    .line 175
    .local v1, "fragment":Lcom/google/android/finsky/activities/TabbedBrowseFragment;
    const/4 v3, 0x2

    new-array v4, v7, [Landroid/view/View;

    invoke-direct {p0, v3, v1, v8, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    goto :goto_0

    .line 178
    .end local v0    # "corpusMetadata":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    .end local v1    # "fragment":Lcom/google/android/finsky/activities/TabbedBrowseFragment;
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/DfeToc;->getHomeUrl()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 179
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/DfeToc;->getHomeUrl()Ljava/lang/String;

    move-result-object v2

    .line 184
    .local v2, "landingUrl":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0, p1, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    goto :goto_0

    .line 182
    .end local v2    # "landingUrl":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v3

    iget-object v2, v3, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->landingUrl:Ljava/lang/String;

    .restart local v2    # "landingUrl":Ljava/lang/String;
    goto :goto_1

    .line 189
    .end local v2    # "landingUrl":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v3}, Lcom/google/android/finsky/activities/MainActivity;->restartOnResume()V

    goto :goto_0
.end method

.method public goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V
    .locals 5
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "landingUrl"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 194
    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    const v2, 0x7f0c01ae

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/activities/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "title":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-static {p2, v0, v3, p1, v1}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Landroid/util/Pair;)Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v4, v1, v4, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 198
    return-void
.end method

.method public goToAllReviews(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "reviewsUrl"    # Ljava/lang/String;
    .param p3, "isRottenTomatoesReviews"    # Z

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MainActivity;->isStateSaved()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 355
    :cond_0
    :goto_0
    return-void

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/finsky/activities/ReviewsActivity;->show(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public goToCorpusHome(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "backendId"    # I
    .param p4, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    const/4 v3, 0x0

    .line 209
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p1, p2, p3, p4, v1}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Landroid/util/Pair;)Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 215
    :cond_0
    return-void
.end method

.method public goToDocPage(Lcom/google/android/finsky/api/model/Document;)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v3, 0x0

    .line 488
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Landroid/view/View;Landroid/view/View;)V

    .line 489
    return-void
.end method

.method public goToDocPage(Lcom/google/android/finsky/api/model/Document;Landroid/view/View;Landroid/view/View;)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "sharedPrimaryContainer"    # Landroid/view/View;
    .param p3, "sharedCoverView"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 497
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v5, v3

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Landroid/view/View;Landroid/view/View;)V

    .line 499
    return-void
.end method

.method public goToDocPage(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 462
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-nez v0, :cond_0

    .line 467
    :goto_0
    return-void

    .line 465
    :cond_0
    const/4 v0, 0x6

    invoke-static {p1, v1, v1}, Lcom/google/android/finsky/utils/DetailsShimFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/utils/DetailsShimFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    goto :goto_0
.end method

.method public goToDocPage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "continueUrl"    # Ljava/lang/String;
    .param p3, "overrideAccount"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 476
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-nez v0, :cond_0

    .line 481
    :goto_0
    return-void

    .line 479
    :cond_0
    const/4 v0, 0x6

    invoke-static {p1, p2, p3}, Lcom/google/android/finsky/utils/DetailsShimFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/utils/DetailsShimFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    goto :goto_0
.end method

.method public goToFlagContent(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 452
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-static {v0, p1}, Lcom/google/android/finsky/activities/FlagItemDialog;->show(Landroid/content/Context;Ljava/lang/String;)V

    .line 455
    :cond_0
    return-void
.end method

.method public goToImagesLightbox(Lcom/google/android/finsky/api/model/Document;II)V
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "initialIndex"    # I
    .param p3, "imageType"    # I

    .prologue
    .line 607
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MainActivity;->isStateSaved()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 612
    :cond_0
    :goto_0
    return-void

    .line 611
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/finsky/activities/ScreenshotsActivity;->show(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;II)V

    goto :goto_0
.end method

.method public goToMyAccount()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 371
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 372
    const/16 v0, 0xd

    invoke-static {}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->newInstance()Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 374
    :cond_0
    return-void
.end method

.method public goToMyDownloads(Lcom/google/android/finsky/api/model/DfeToc;)V
    .locals 4
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    const/4 v3, 0x0

    .line 358
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    const/4 v0, 0x3

    invoke-static {p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;->newInstance(Lcom/google/android/finsky/api/model/DfeToc;)Lcom/google/android/finsky/activities/myapps/MyAppsTabbedFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 362
    :cond_0
    return-void
.end method

.method public goToMyWishlist()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 365
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    const/16 v0, 0xa

    invoke-static {}, Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;->newInstance()Lcom/google/android/finsky/activities/mywishlist/MyWishlistFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 368
    :cond_0
    return-void
.end method

.method public goToOrderHistory(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;)V
    .locals 4
    .param p1, "listUrl"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    const/4 v3, 0x0

    .line 227
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    const/16 v0, 0xd

    invoke-static {p1, p2, p3}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;)Lcom/google/android/finsky/activities/myaccount/OrderHistoryFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 232
    :cond_0
    return-void
.end method

.method public goToScreenshots(Lcom/google/android/finsky/api/model/Document;I)V
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "initialIndex"    # I

    .prologue
    .line 596
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToImagesLightbox(Lcom/google/android/finsky/api/model/Document;II)V

    .line 597
    return-void
.end method

.method public goToSearch(Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "backend"    # I
    .param p3, "clickLogNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 404
    invoke-static {p1, p2}, Lcom/google/android/finsky/api/DfeUtils;->formSearchUrl(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToSearch(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 405
    return-void
.end method

.method public goToSearch(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 4
    .param p1, "searchUrl"    # Ljava/lang/String;
    .param p2, "displayedQuery"    # Ljava/lang/String;
    .param p3, "displayedBackend"    # I
    .param p4, "clickLogNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v3, 0x0

    .line 413
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    const/4 v0, 0x7

    invoke-static {p2, p1, p3}, Lcom/google/android/finsky/activities/SearchFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/finsky/activities/SearchFragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 417
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 419
    :cond_0
    return-void
.end method

.method public goToSettings()V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MainActivity;->openSettings()V

    .line 439
    return-void
.end method

.method public goToSocialHome(Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;)V
    .locals 5
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;

    .prologue
    const/4 v4, 0x0

    .line 218
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 219
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const v2, 0x7f0c0391

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/FinskyApp;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 220
    .local v0, "title":Ljava/lang/String;
    const/16 v1, 0xc

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-static {p1, v0, v2, p2, v3}, Lcom/google/android/finsky/activities/TabbedBrowseFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Landroid/util/Pair;)Lcom/google/android/finsky/activities/TabbedBrowseFragment;

    move-result-object v2

    new-array v3, v4, [Landroid/view/View;

    invoke-direct {p0, v1, v2, v4, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 224
    .end local v0    # "title":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public goToUrl(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 442
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 443
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 444
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.android.browser.application_id"

    iget-object v3, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v3}, Lcom/google/android/finsky/activities/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 445
    iget-object v2, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/activities/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 446
    return-void
.end method

.method public goUp()Z
    .locals 11

    .prologue
    const/4 v6, 0x1

    .line 1019
    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v7}, Lcom/google/android/finsky/activities/MainActivity;->isStateSaved()Z

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v7}, Ljava/util/Stack;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1075
    :cond_0
    :goto_0
    return v6

    .line 1023
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v7

    const/16 v8, 0x25a

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v10

    invoke-virtual {v7, v8, v9, v10}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1026
    sget-boolean v7, Lcom/google/android/finsky/navigationmanager/NavigationManager;->RESPECT_TASKS_IN_UP:Z

    if-eqz v7, :cond_4

    .line 1029
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    .line 1030
    .local v3, "dfeToc":Lcom/google/android/finsky/api/model/DfeToc;
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getCurrentDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 1035
    .local v1, "currentDocument":Lcom/google/android/finsky/api/model/Document;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    .line 1036
    .local v0, "currentBackend":I
    :goto_1
    if-eqz v3, :cond_3

    if-ltz v0, :cond_3

    invoke-virtual {v3, v0}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v7

    if-eqz v7, :cond_3

    if-eqz v0, :cond_3

    const/16 v7, 0x9

    if-eq v0, v7, :cond_3

    .line 1040
    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v8

    invoke-static {v7, v8, v3}, Lcom/google/android/finsky/utils/IntentUtils;->createCorpusIntent(Landroid/content/Context;ILcom/google/android/finsky/api/model/DfeToc;)Landroid/content/Intent;

    move-result-object v5

    .line 1046
    .local v5, "recreationIntent":Landroid/content/Intent;
    :goto_2
    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-static {v7, v5}, Landroid/support/v4/app/NavUtils;->shouldUpRecreateTask(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v4

    .line 1048
    .local v4, "needsTaskRecreation":Z
    if-eqz v4, :cond_4

    .line 1049
    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-static {v7}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/support/v4/app/TaskStackBuilder;->startActivities()V

    .line 1051
    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-static {v7}, Landroid/support/v4/app/ActivityCompat;->finishAffinity(Landroid/app/Activity;)V

    goto :goto_0

    .line 1035
    .end local v0    # "currentBackend":I
    .end local v4    # "needsTaskRecreation":Z
    .end local v5    # "recreationIntent":Landroid/content/Intent;
    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    .line 1043
    .restart local v0    # "currentBackend":I
    :cond_3
    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-static {v7}, Lcom/google/android/finsky/utils/IntentUtils;->createAggregatedHomeIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v5

    .restart local v5    # "recreationIntent":Landroid/content/Intent;
    goto :goto_2

    .line 1056
    .end local v0    # "currentBackend":I
    .end local v1    # "currentDocument":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "dfeToc":Lcom/google/android/finsky/api/model/DfeToc;
    .end local v5    # "recreationIntent":Landroid/content/Intent;
    :cond_4
    iget-object v7, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v7}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/navigationmanager/NavigationState;

    .line 1057
    .local v2, "currentState":Lcom/google/android/finsky/navigationmanager/NavigationState;
    iget v7, v2, Lcom/google/android/finsky/navigationmanager/NavigationState;->pageType:I

    packed-switch v7, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1072
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome()V

    goto :goto_0

    .line 1060
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->unwindDetailsStack()V

    goto :goto_0

    .line 1066
    :pswitch_3
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBack(Z)Z

    move-result v6

    goto/16 :goto_0

    .line 1057
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public handleDeepLink(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "referringPackage"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 154
    const/16 v0, 0x9

    invoke-static {p1, p2}, Lcom/google/android/finsky/fragments/DeepLinkShimFragment;->newInstance(Landroid/net/Uri;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    new-array v2, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 156
    return-void
.end method

.method public handleDeepLink(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "referringPackage"    # Ljava/lang/String;

    .prologue
    .line 150
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->handleDeepLink(Landroid/net/Uri;Ljava/lang/String;)V

    .line 151
    return-void
.end method

.method public init(Lcom/google/android/finsky/activities/MainActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 140
    return-void
.end method

.method public isActionBarOverlayEnabledForCurrent()Z
    .locals 1

    .prologue
    .line 932
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 933
    const/4 v0, 0x0

    .line 935
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/navigationmanager/NavigationState;

    iget-boolean v0, v0, Lcom/google/android/finsky/navigationmanager/NavigationState;->isActionBarOverlayEnabled:Z

    goto :goto_0
.end method

.method public isBackStackEmpty()Z
    .locals 1

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 739
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    if-eqz p2, :cond_0

    .line 740
    const-string v1, "dialog_details_url"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 741
    .local v0, "detailsUrl":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 743
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;)V

    .line 746
    .end local v0    # "detailsUrl":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public openItem(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 749
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    iget-object v3, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/ConsumptionUtils;->openItem(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;I)Z

    .line 751
    return-void
.end method

.method public popBackStack()V
    .locals 1

    .prologue
    .line 940
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 941
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 943
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 944
    return-void
.end method

.method public refreshPage()V
    .locals 3

    .prologue
    .line 708
    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 724
    :cond_0
    :goto_0
    return-void

    .line 712
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivePage()Lcom/google/android/finsky/fragments/PageFragment;

    move-result-object v0

    .line 713
    .local v0, "active":Lcom/google/android/finsky/fragments/PageFragment;
    if-eqz v0, :cond_2

    .line 717
    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->refresh()V

    .line 718
    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->isDataReady()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 719
    invoke-virtual {v0}, Lcom/google/android/finsky/fragments/PageFragment;->onDataChanged()V

    goto :goto_0

    .line 722
    :cond_2
    const-string v1, "Called refresh but there was no active page"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public removeOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;

    .prologue
    .line 1169
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 1170
    return-void
.end method

.method public replaceDetailsShimWithDocPage(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "originalUrl"    # Ljava/lang/String;
    .param p3, "continueUrl"    # Ljava/lang/String;
    .param p4, "overrideAccount"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 506
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Landroid/view/View;Landroid/view/View;)V

    .line 507
    return-void
.end method

.method public resolveCallToAction(Lcom/google/android/finsky/protos/DocumentV2$CallToAction;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;)V
    .locals 2
    .param p1, "callToAction"    # Lcom/google/android/finsky/protos/DocumentV2$CallToAction;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "packageManager"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 1196
    iget v0, p1, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1197
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->resolveCallToActionDismiss(Lcom/google/android/finsky/protos/DocumentV2$CallToAction;Lcom/google/android/finsky/api/DfeApi;)V

    .line 1201
    :cond_0
    :goto_0
    return-void

    .line 1198
    :cond_1
    iget v0, p1, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->type:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1199
    iget-object v0, p1, Lcom/google/android/finsky/protos/DocumentV2$CallToAction;->link:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p0, v0, p3, p4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->resolveLink(Lcom/google/android/finsky/protos/DocAnnotations$Link;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;)V

    goto :goto_0
.end method

.method public resolveLink(Lcom/google/android/finsky/protos/DocAnnotations$Link;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;)V
    .locals 1
    .param p1, "link"    # Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .param p2, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p3, "packageManager"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 304
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->resolveLink(Lcom/google/android/finsky/protos/DocAnnotations$Link;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;)V

    .line 305
    return-void
.end method

.method public resolveLink(Lcom/google/android/finsky/protos/DocAnnotations$Link;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;)V
    .locals 9
    .param p1, "link"    # Lcom/google/android/finsky/protos/DocAnnotations$Link;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "packageManager"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 314
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivityForResolveLink()Landroid/app/Activity;

    move-result-object v6

    .line 317
    .local v6, "activity":Landroid/app/Activity;
    iget-object v0, p1, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    if-eqz v0, :cond_0

    .line 319
    iget-object v1, p1, Lcom/google/android/finsky/protos/DocAnnotations$Link;->resolvedLink:Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;

    const/4 v3, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->resolveLink(Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 346
    :goto_0
    return-void

    .line 320
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/finsky/protos/DocAnnotations$Link;->hasUriBackend:Z

    if-eqz v0, :cond_3

    .line 322
    iget v7, p1, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uriBackend:I

    .line 324
    .local v7, "backendId":I
    invoke-static {p4, v7}, Lcom/google/android/finsky/utils/IntentUtils;->isConsumptionAppInstalled(Landroid/content/pm/PackageManager;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 325
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showAppNeededDialog(I)V

    goto :goto_0

    .line 327
    :cond_1
    iget-object v0, p1, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 330
    iget-object v0, p1, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v7, v0, v1}, Lcom/google/android/finsky/utils/IntentUtils;->buildConsumptionAppUrlIntent(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 335
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v7, v0}, Lcom/google/android/finsky/utils/IntentUtils;->buildConsumptionAppLaunchIntent(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 342
    .end local v7    # "backendId":I
    :cond_3
    new-instance v8, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 343
    .local v8, "intent":Landroid/content/Intent;
    iget-object v0, p1, Lcom/google/android/finsky/protos/DocAnnotations$Link;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 344
    invoke-virtual {v6, v8}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public resolveLink(Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 10
    .param p1, "link"    # Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "backendId"    # I
    .param p4, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5, "clickLogNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v5, 0x0

    .line 264
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 266
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 267
    iget-object v1, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->browseUrl:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBrowse(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 269
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->detailsUrl:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;)V

    goto :goto_0

    .line 270
    :cond_2
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    if-eqz v0, :cond_3

    .line 272
    const-string v0, "Direct purchase deprecated."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 273
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->directPurchase:Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;

    iget-object v6, v0, Lcom/google/android/finsky/protos/ResolveLink$DirectPurchase;->detailsUrl:Ljava/lang/String;

    .line 274
    .local v6, "detailsUrl":Ljava/lang/String;
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Ljava/lang/String;)V

    goto :goto_0

    .line 275
    .end local v6    # "detailsUrl":Ljava/lang/String;
    :cond_3
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 276
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->homeUrl:Ljava/lang/String;

    invoke-virtual {p0, p4, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToAggregatedHome(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    goto :goto_0

    .line 277
    :cond_4
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    if-eqz v0, :cond_7

    .line 278
    const/4 v8, 0x0

    .line 279
    .local v8, "redeemCodePrefill":Ljava/lang/String;
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 280
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v8, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->prefillCode:Ljava/lang/String;

    .line 282
    :cond_5
    const/4 v9, 0x0

    .line 283
    .local v9, "redeemPartnerPayload":Ljava/lang/String;
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v0, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 284
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->redeemGiftCard:Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;

    iget-object v9, v0, Lcom/google/android/finsky/protos/ResolveLink$RedeemGiftCard;->partnerPayload:Ljava/lang/String;

    .line 286
    :cond_6
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0, v1, v8, v9}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->createIntent(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 291
    .local v7, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getActivityForResolveLink()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 292
    .end local v7    # "intent":Landroid/content/Intent;
    .end local v8    # "redeemCodePrefill":Ljava/lang/String;
    .end local v9    # "redeemPartnerPayload":Ljava/lang/String;
    :cond_7
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->searchUrl:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->query:Ljava/lang/String;

    iget v2, p1, Lcom/google/android/finsky/protos/ResolveLink$ResolvedLink;->backend:I

    invoke-virtual {p0, v0, v1, v2, v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToSearch(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_0
.end method

.method public searchFromFullPageReplaced(Ljava/lang/String;I)V
    .locals 4
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "backend"    # I

    .prologue
    .line 430
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 431
    const/4 v0, 0x7

    invoke-static {p1, p2}, Lcom/google/android/finsky/api/DfeUtils;->formSearchUrlWithFprDisabled(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, p2}, Lcom/google/android/finsky/activities/SearchFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/finsky/activities/SearchFragment;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 435
    :cond_0
    return-void
.end method

.method public searchFromSuggestion(Ljava/lang/String;I)V
    .locals 4
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "backend"    # I

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->canNavigate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    const/4 v0, 0x7

    invoke-static {p1, p2}, Lcom/google/android/finsky/api/DfeUtils;->formSearchUrl(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, p2}, Lcom/google/android/finsky/activities/SearchFragment;->newInstance(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/finsky/activities/SearchFragment;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/view/View;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->showPage(ILandroid/support/v4/app/Fragment;Z[Landroid/view/View;)V

    .line 427
    :cond_0
    return-void
.end method

.method public serialize(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 632
    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 635
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 636
    .local v0, "asList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/finsky/navigationmanager/NavigationState;>;"
    const-string v1, "nm_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public setActionBarOverlayEnabledForCurrent(Z)V
    .locals 1
    .param p1, "isActionBarOverlayEnabled"    # Z

    .prologue
    .line 921
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 925
    :goto_0
    return-void

    .line 924
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mBackStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/navigationmanager/NavigationState;

    iput-boolean p1, v0, Lcom/google/android/finsky/navigationmanager/NavigationState;->isActionBarOverlayEnabled:Z

    goto :goto_0
.end method

.method public showAppNeededDialog(I)V
    .locals 4
    .param p1, "docBackend"    # I

    .prologue
    .line 731
    iget-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    iget-object v1, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-static {v0, p1, v1, v2, v3}, Lcom/google/android/finsky/navigationmanager/ConsumptionUtils;->showAppNeededDialog(Landroid/content/Context;ILandroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;I)V

    .line 733
    return-void
.end method

.method public terminate()V
    .locals 1

    .prologue
    .line 623
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/navigationmanager/NavigationManager;->mActivity:Lcom/google/android/finsky/activities/MainActivity;

    .line 624
    return-void
.end method
