.class public Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;
.super Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;
.source "SetupWizardPromptForFopActivity.java"


# instance fields
.field private mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;-><init>()V

    return-void
.end method

.method public static createExternalSetupWizardIntent(Landroid/accounts/Account;)Landroid/content/Intent;
    .locals 3
    .param p0, "account"    # Landroid/accounts/Account;

    .prologue
    .line 61
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 62
    .local v0, "intent":Landroid/content/Intent;
    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->putIntentExtras(Landroid/accounts/Account;[BLandroid/content/Intent;)V

    .line 63
    const-string v1, "via_create_intent"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 64
    return-object v0
.end method

.method public static createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/content/Intent;
    .locals 2
    .param p0, "account"    # Landroid/accounts/Account;
    .param p1, "params"    # Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .prologue
    .line 48
    invoke-static {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->createExternalSetupWizardIntent(Landroid/accounts/Account;)Landroid/content/Intent;

    move-result-object v0

    .line 49
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "setup_wizard_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 50
    return-object v0
.end method


# virtual methods
.method protected createContentFragment()Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->mAccount:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->mServerLogsCookie:[B

    iget-object v2, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopFragment;->newInstance(Landroid/accounts/Account;[BLcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method protected displayMessage(IILjava/lang/String;)V
    .locals 2
    .param p1, "messageId"    # I
    .param p2, "logType"    # I
    .param p3, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 131
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 132
    .local v0, "result":Landroid/content/Intent;
    if-eqz p3, :cond_0

    .line 133
    const-string v1, "redeemed_offer_message_html"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->setResult(ILandroid/content/Intent;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->finish()V

    .line 139
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->finish()V

    .line 159
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/SetupWizardUtils;->animateSliding(Landroid/app/Activity;Z)V

    .line 160
    return-void
.end method

.method protected getActivityLayout()I
    .locals 1

    .prologue
    .line 115
    const v0, 0x7f0401a5

    return v0
.end method

.method protected getAlreadySetupEventType()I
    .locals 1

    .prologue
    .line 153
    const/16 v0, 0x16d

    return v0
.end method

.method protected getBillingProfileErrorEventType()I
    .locals 1

    .prologue
    .line 148
    const/16 v0, 0x16e

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 126
    const/16 v0, 0x37c

    return v0
.end method

.method protected getSnoozeEventType()I
    .locals 1

    .prologue
    .line 143
    const/16 v0, 0x16c

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    .line 73
    .local v8, "intent":Landroid/content/Intent;
    const-string v0, "via_create_intent"

    invoke-virtual {v8, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.android.vending.billing.ADD_CREDIT_CARD"

    invoke-virtual {v8}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    const-string v0, "authAccount"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 77
    const-string v0, "No account name passed."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 78
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->setResult(I)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->finish()V

    .line 111
    :goto_0
    return-void

    .line 82
    :cond_0
    const-string v0, "authAccount"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 83
    .local v7, "accountName":Ljava/lang/String;
    invoke-static {v7, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v6

    .line 84
    .local v6, "account":Landroid/accounts/Account;
    if-nez v6, :cond_1

    .line 85
    const-string v0, "Cannot find the account: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v7, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 86
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->setResult(I)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->finish()V

    goto :goto_0

    .line 90
    :cond_1
    const/4 v0, 0x0

    invoke-static {v6, v0, v8}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->putIntentExtras(Landroid/accounts/Account;[BLandroid/content/Intent;)V

    .line 93
    .end local v6    # "account":Landroid/accounts/Account;
    .end local v7    # "accountName":Ljava/lang/String;
    :cond_2
    const-string v0, "setup_wizard_params"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    iput-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    if-nez v0, :cond_3

    .line 96
    new-instance v0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-direct {v0, v8}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .line 100
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isLightTheme()Z

    move-result v0

    if-eqz v0, :cond_4

    const v9, 0x7f0d01bf

    .line 102
    .local v9, "themeId":I
    :goto_1
    invoke-virtual {p0, v9}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->setTheme(I)V

    .line 104
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/promptforfop/PromptForFopBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 106
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 107
    .local v10, "titleView":Landroid/widget/TextView;
    const v0, 0x7f0c009f

    invoke-virtual {v10, v0}, Landroid/widget/TextView;->setText(I)V

    .line 109
    iget-object v1, p0, Lcom/google/android/finsky/billing/promptforfop/SetupWizardPromptForFopActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/SetupWizardUtils;->configureBasicUi(Landroid/app/Activity;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;IZZZ)V

    goto :goto_0

    .line 100
    .end local v9    # "themeId":I
    .end local v10    # "titleView":Landroid/widget/TextView;
    :cond_4
    const v9, 0x7f0d01be

    goto :goto_1
.end method
