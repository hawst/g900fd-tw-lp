.class public Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;
.super Lcom/google/android/finsky/activities/myapps/MyAppsTab;
.source "MyAppsSubscriptionsTab.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/activities/myapps/MyAppsTab",
        "<",
        "Lcom/google/android/finsky/api/model/DfeBulkDetails;",
        ">;"
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;

.field private mAdapterInitialized:Z

.field private mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

.field mSubscriptionsInLibrary:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;",
            ">;"
        }
    .end annotation
.end field

.field private mSubscriptionsListView:Landroid/widget/ListView;

.field private mSubscriptionsView:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;)V
    .locals 2
    .param p1, "authenticatedActivity"    # Lcom/google/android/finsky/activities/AuthenticatedActivity;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;-><init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V

    .line 59
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapterInitialized:Z

    .line 62
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsInLibrary:Ljava/util/Map;

    .line 64
    new-instance v0, Lcom/google/android/finsky/utils/ObjectMap;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/ObjectMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 72
    new-instance v0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-direct {v0, p1, v1, p5, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;Lcom/google/android/play/image/BitmapLoader;Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;

    .line 74
    return-void
.end method


# virtual methods
.method protected getAdapter()Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;

    return-object v0
.end method

.method protected getDocumentForView(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 208
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method protected getFullView()Landroid/view/View;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    return-object v0
.end method

.method public getView(I)Landroid/view/View;
    .locals 3
    .param p1, "backendId"    # I

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsView:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f0400e0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsView:Landroid/view/ViewGroup;

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onDataChanged()V
    .locals 15

    .prologue
    const/4 v13, 0x0

    const/4 v14, 0x1

    .line 119
    invoke-super {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->onDataChanged()V

    .line 121
    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v11, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    invoke-virtual {v11}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->getDocuments()Ljava/util/List;

    move-result-object v4

    .line 122
    .local v4, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v10

    .line 123
    .local v10, "subsDocs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;>;"
    invoke-static {}, Lcom/google/android/finsky/utils/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v2

    .line 124
    .local v2, "appsDocs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/api/model/Document;

    .line 125
    .local v3, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    goto :goto_0

    .line 127
    :sswitch_0
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v2, v11, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 130
    :sswitch_1
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v10, v11, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 140
    .end local v3    # "doc":Lcom/google/android/finsky/api/model/Document;
    :cond_0
    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsInLibrary:Ljava/util/Map;

    invoke-interface {v11}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    .line 141
    .local v7, "libEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 142
    .local v9, "subsDocId":Ljava/lang/String;
    invoke-interface {v10, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/api/model/Document;

    .line 143
    .local v8, "subDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-static {v9}, Lcom/google/android/finsky/utils/DocUtils;->getPackageNameForSubscription(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "appDocId":Ljava/lang/String;
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 145
    .local v0, "appDoc":Lcom/google/android/finsky/api/model/Document;
    if-nez v8, :cond_1

    .line 146
    const-string v11, "Subscription %s is unavailable, ignoring this entry"

    new-array v12, v14, [Ljava/lang/Object;

    aput-object v9, v12, v13

    invoke-static {v11, v12}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 147
    :cond_1
    if-nez v0, :cond_2

    .line 148
    const-string v11, "Parent app %s of subscription %s is unavailable, ignoring this entry"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v1, v12, v13

    aput-object v9, v12, v14

    invoke-static {v11, v12}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 151
    :cond_2
    iget-object v12, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;

    invoke-virtual {v12, v11, v8, v0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->addEntry(Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;)V

    goto :goto_1

    .line 154
    .end local v0    # "appDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v1    # "appDocId":Ljava/lang/String;
    .end local v7    # "libEntry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;>;"
    .end local v8    # "subDoc":Lcom/google/android/finsky/api/model/Document;
    .end local v9    # "subsDocId":Ljava/lang/String;
    :cond_3
    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;

    invoke-virtual {v11}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->sortDocs()V

    .line 156
    iget-boolean v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapterInitialized:Z

    if-nez v11, :cond_5

    .line 157
    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsView:Landroid/view/ViewGroup;

    const v12, 0x7f0a0276

    invoke-virtual {v11, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ListView;

    iput-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    .line 159
    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    invoke-virtual {v11}, Landroid/widget/ListView;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    invoke-static {v11}, Lcom/google/android/finsky/utils/UiUtils;->getGridHorizontalPadding(Landroid/content/res/Resources;)I

    move-result v5

    .line 161
    .local v5, "horizontalPadding":I
    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    iget-object v12, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    invoke-virtual {v12}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v12

    iget-object v13, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    invoke-virtual {v13}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v13

    invoke-virtual {v11, v5, v12, v5, v13}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 164
    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    iget-object v12, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;

    invoke-virtual {v11, v12}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 165
    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    invoke-virtual {v11, v14}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 167
    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v12, "MyAppsTab.KeyListParcel"

    invoke-virtual {v11, v12}, Lcom/google/android/finsky/utils/ObjectMap;->containsKey(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 168
    iget-object v12, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    iget-object v11, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v13, "MyAppsTab.KeyListParcel"

    invoke-virtual {v11, v13}, Lcom/google/android/finsky/utils/ObjectMap;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/os/Parcelable;

    invoke-virtual {v12, v11}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 172
    :cond_4
    iput-boolean v14, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapterInitialized:Z

    .line 174
    .end local v5    # "horizontalPadding":I
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->syncViewToState()V

    .line 175
    return-void

    .line 125
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xf -> :sswitch_1
    .end sparse-switch
.end method

.method public onInstallPackageEvent(Ljava/lang/String;Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;I)V
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;
    .param p3, "statusCode"    # I

    .prologue
    .line 235
    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->INSTALLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-eq p2, v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;->UNINSTALLED:Lcom/google/android/finsky/installer/InstallerListener$InstallerPackageEvent;

    if-ne p2, v0, :cond_1

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;

    invoke-virtual {v0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->notifyDataSetChanged()V

    .line 239
    :cond_1
    return-void
.end method

.method public onLibraryContentsChanged(Lcom/google/android/finsky/library/AccountLibrary;)V
    .locals 0
    .param p1, "library"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->requestData()V

    .line 244
    return-void
.end method

.method public onRestoreInstanceState(Lcom/google/android/finsky/utils/ObjectMap;)V
    .locals 0
    .param p1, "savedInstanceState"    # Lcom/google/android/finsky/utils/ObjectMap;

    .prologue
    .line 213
    if-eqz p1, :cond_0

    .line 214
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    .line 216
    :cond_0
    return-void
.end method

.method public onSaveInstanceState()Lcom/google/android/finsky/utils/ObjectMap;
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    const-string v1, "MyAppsTab.KeyListParcel"

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/utils/ObjectMap;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSavedInstanceState:Lcom/google/android/finsky/utils/ObjectMap;

    return-object v0
.end method

.method protected requestData()V
    .locals 11

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->clearState()V

    .line 80
    iget-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsInLibrary:Ljava/util/Map;

    invoke-interface {v9}, Ljava/util/Map;->clear()V

    .line 81
    iget-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mAdapter:Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;

    invoke-virtual {v9}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsAdapter;->clear()V

    .line 82
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v7

    .line 84
    .local v7, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v1

    .line 85
    .local v1, "docIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v9}, Lcom/google/android/finsky/api/DfeApi;->getAccount()Landroid/accounts/Account;

    move-result-object v9

    invoke-virtual {v7, v9}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v0

    .line 89
    .local v0, "currentLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-virtual {v7}, Lcom/google/android/finsky/library/Libraries;->getAccountLibraries()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/library/AccountLibrary;

    .line 90
    .local v8, "library":Lcom/google/android/finsky/library/AccountLibrary;
    if-eq v8, v0, :cond_0

    .line 91
    invoke-virtual {v8}, Lcom/google/android/finsky/library/AccountLibrary;->getInAppSubscriptionsList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;

    .line 92
    .local v2, "entry":Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    invoke-virtual {v2}, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->getDocId()Ljava/lang/String;

    move-result-object v3

    .line 93
    .local v3, "entryDocId":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsInLibrary:Ljava/util/Map;

    invoke-interface {v9, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-static {v3}, Lcom/google/android/finsky/utils/DocUtils;->getPackageNameForSubscription(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 100
    .end local v2    # "entry":Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    .end local v3    # "entryDocId":Ljava/lang/String;
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "library":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/finsky/library/AccountLibrary;->getInAppSubscriptionsList()Ljava/util/List;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;

    .line 101
    .restart local v2    # "entry":Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    invoke-virtual {v2}, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->getDocId()Ljava/lang/String;

    move-result-object v3

    .line 102
    .restart local v3    # "entryDocId":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mSubscriptionsInLibrary:Ljava/util/Map;

    invoke-interface {v9, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-static {v3}, Lcom/google/android/finsky/utils/DocUtils;->getPackageNameForSubscription(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 109
    .end local v2    # "entry":Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
    .end local v3    # "entryDocId":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 110
    .local v4, "fetchDocIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v4, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 112
    new-instance v9, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    iget-object v10, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-direct {v9, v10, v4}, Lcom/google/android/finsky/api/model/DfeBulkDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/util/List;)V

    iput-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    .line 113
    iget-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v9, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    invoke-virtual {v9, p0}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 114
    iget-object v9, p0, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v9, Lcom/google/android/finsky/api/model/DfeBulkDetails;

    invoke-virtual {v9, p0}, Lcom/google/android/finsky/api/model/DfeBulkDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 115
    return-void
.end method

.method protected retryFromError()V
    .locals 0

    .prologue
    .line 193
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsSubscriptionsTab;->requestData()V

    .line 194
    return-void
.end method
