.class public final Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DebugInfoProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DebugInfoProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DebugInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    }
.end annotation


# instance fields
.field public message:[Ljava/lang/String;

.field public timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->clear()Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    .line 140
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    .line 144
    invoke-static {}, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;->emptyArray()[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    .line 145
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->cachedSize:I

    .line 146
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 173
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 174
    .local v4, "size":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_2

    .line 175
    const/4 v0, 0x0

    .line 176
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 177
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 178
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 179
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 180
    add-int/lit8 v0, v0, 0x1

    .line 181
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 177
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 185
    .end local v2    # "element":Ljava/lang/String;
    :cond_1
    add-int/2addr v4, v1

    .line 186
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 188
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    array-length v5, v5

    if-lez v5, :cond_4

    .line 189
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    array-length v5, v5

    if-ge v3, v5, :cond_4

    .line 190
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    aget-object v2, v5, v3

    .line 191
    .local v2, "element":Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    if-eqz v2, :cond_3

    .line 192
    const/4 v5, 0x2

    invoke-static {v5, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeGroupSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v5

    add-int/2addr v4, v5

    .line 189
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 197
    .end local v2    # "element":Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    .end local v3    # "i":I
    :cond_4
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x0

    .line 205
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 206
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 210
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 211
    :sswitch_0
    return-object p0

    .line 216
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 218
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    if-nez v5, :cond_2

    move v1, v4

    .line 219
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Ljava/lang/String;

    .line 220
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 221
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 223
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 224
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 225
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 223
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 218
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    array-length v1, v5

    goto :goto_1

    .line 228
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v1

    .line 229
    iput-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    goto :goto_0

    .line 233
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_2
    const/16 v5, 0x13

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 235
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    if-nez v5, :cond_5

    move v1, v4

    .line 236
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    .line 238
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    if-eqz v1, :cond_4

    .line 239
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 241
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 242
    new-instance v5, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;-><init>()V

    aput-object v5, v2, v1

    .line 243
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 244
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 241
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 235
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    array-length v1, v5

    goto :goto_3

    .line 247
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;-><init>()V

    aput-object v5, v2, v1

    .line 248
    aget-object v5, v2, v1

    invoke-virtual {p1, v5, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readGroup(Lcom/google/protobuf/nano/MessageNano;I)V

    .line 249
    iput-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    goto/16 :goto_0

    .line 206
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x13 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 3
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 152
    iget-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 153
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 154
    iget-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->message:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 155
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 156
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 153
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 160
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 161
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 162
    iget-object v2, p0, Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo;->timing:[Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;

    aget-object v0, v2, v1

    .line 163
    .local v0, "element":Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    if-eqz v0, :cond_2

    .line 164
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeGroup(ILcom/google/protobuf/nano/MessageNano;)V

    .line 161
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 168
    .end local v0    # "element":Lcom/google/android/finsky/protos/DebugInfoProto$DebugInfo$Timing;
    .end local v1    # "i":I
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 169
    return-void
.end method
