.class Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;
.super Landroid/os/AsyncTask;
.source "MyAppsInstalledTab.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->requestData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/util/List",
        "<",
        "Ljava/lang/String;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 77
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->doInBackground([Ljava/lang/Void;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/Map;
    .locals 3
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 80
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    .line 81
    .local v1, "libraries":Lcom/google/android/finsky/library/Libraries;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v0

    .line 82
    .local v0, "appStates":Lcom/google/android/finsky/appstate/AppStates;
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/appstate/AppStates;->getOwnedByAccountBlocking(Lcom/google/android/finsky/library/Libraries;Z)Ljava/util/Map;

    move-result-object v2

    return-object v2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 77
    check-cast p1, Ljava/util/Map;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->onPostExecute(Ljava/util/Map;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p1, "docIdsByAccount":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 89
    .local v0, "accountDocs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v0}, Lcom/google/android/finsky/appstate/GmsCoreHelper;->removeGmsCore(Ljava/util/List;)V

    goto :goto_0

    .line 93
    .end local v0    # "accountDocs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/finsky/FinskyApp;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    iget-object v4, v4, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-interface {v4}, Lcom/google/android/finsky/api/DfeApi;->getAccountName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p1}, Lcom/google/android/finsky/api/model/MultiWayUpdateController;->selectAccountsForUpdateChecks(Lcom/google/android/finsky/appstate/InstallerDataStore;Ljava/lang/String;Ljava/util/Map;)V

    .line 99
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    iget-object v3, v3, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    # getter for: Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDocIdsByAccount:Ljava/util/Map;
    invoke-static {v3}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->access$000(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 100
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    invoke-virtual {v3}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->onDataChanged()V

    .line 122
    :goto_1
    return-void

    .line 103
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    # setter for: Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDocIdsByAccount:Ljava/util/Map;
    invoke-static {v3, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->access$002(Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;Ljava/util/Map;)Ljava/util/Map;

    .line 106
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    invoke-virtual {v3}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->clearState()V

    .line 107
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    new-instance v4, Lcom/google/android/finsky/api/model/MultiWayUpdateController;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v5

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/finsky/api/model/MultiWayUpdateController;-><init>(Lcom/google/android/finsky/appstate/InstallerDataStore;Lcom/google/android/finsky/library/Libraries;)V

    iput-object v4, v3, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    .line 109
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    iget-object v3, v3, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v3, Lcom/google/android/finsky/api/model/MultiWayUpdateController;

    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/api/model/MultiWayUpdateController;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 110
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    iget-object v3, v3, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v3, Lcom/google/android/finsky/api/model/MultiWayUpdateController;

    iget-object v4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/api/model/MultiWayUpdateController;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 111
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    iget-object v3, v3, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    check-cast v3, Lcom/google/android/finsky/api/model/MultiWayUpdateController;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/api/model/MultiWayUpdateController;->addRequests(Ljava/util/Map;)V

    .line 116
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 117
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    goto :goto_1

    .line 121
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab$1;->this$0:Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;

    invoke-virtual {v3}, Lcom/google/android/finsky/activities/myapps/MyAppsInstalledTab;->onDataChanged()V

    goto :goto_1
.end method
