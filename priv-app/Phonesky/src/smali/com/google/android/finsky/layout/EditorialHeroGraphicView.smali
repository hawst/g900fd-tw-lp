.class public Lcom/google/android/finsky/layout/EditorialHeroGraphicView;
.super Lcom/google/android/finsky/layout/HeroGraphicView;
.source "EditorialHeroGraphicView.java"


# instance fields
.field private mBottomOverlay:Landroid/view/View;

.field private mFillColor:I

.field private mTitle:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/HeroGraphicView;-><init>(Landroid/content/Context;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/HeroGraphicView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method


# virtual methods
.method public bindDetailsAntenna(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 9
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 51
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getAntennaInfo()Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;

    move-result-object v8

    .line 52
    .local v8, "antenna":Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-super {p0, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getFillColor(I)I

    move-result v0

    invoke-static {v8, v0}, Lcom/google/android/finsky/utils/UiUtils;->getFillColor(Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mFillColor:I

    .line 54
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    invoke-virtual {p0, v0, v5, v5}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->configureDetailsAspectRatio(IZZ)V

    .line 55
    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v6, v5

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mTitle:Landroid/widget/TextView;

    iget-object v1, v8, Lcom/google/android/finsky/protos/DocumentV2$SeriesAntenna;->episodeTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    const v0, 0x7f0c025a

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->setContentDescription(I[Ljava/lang/Object;)V

    .line 58
    return-void
.end method

.method public bindDetailsEditorial(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 9
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 65
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getEditorialSeriesContainer()Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;

    move-result-object v8

    .line 66
    .local v8, "editorial":Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v0

    invoke-super {p0, v0}, Lcom/google/android/finsky/layout/HeroGraphicView;->getFillColor(I)I

    move-result v0

    invoke-static {v8, v0}, Lcom/google/android/finsky/utils/UiUtils;->getFillColor(Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mFillColor:I

    .line 68
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    invoke-virtual {p0, v0, v5, v5}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->configureDetailsAspectRatio(IZZ)V

    .line 69
    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v6, v5

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->bindHero(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;IIZZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 70
    iget-object v1, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mTitle:Landroid/widget/TextView;

    iget-object v0, v8, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v8, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->seriesTitle:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    const v0, 0x7f0c025a

    new-array v1, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->setContentDescription(I[Ljava/lang/Object;)V

    .line 73
    return-void

    .line 70
    :cond_0
    iget-object v0, v8, Lcom/google/android/finsky/protos/DocumentV2$EditorialSeriesContainer;->episodeTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method protected getFillColor(I)I
    .locals 1
    .param p1, "backendId"    # I

    .prologue
    .line 77
    iget v0, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mFillColor:I

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lcom/google/android/finsky/layout/HeroGraphicView;->onFinishInflate()V

    .line 42
    const v0, 0x7f0a01b9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mBottomOverlay:Landroid/view/View;

    .line 43
    const v0, 0x7f0a01ba

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mTitle:Landroid/widget/TextView;

    .line 44
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v4, 0x0

    .line 171
    invoke-super/range {p0 .. p5}, Lcom/google/android/finsky/layout/HeroGraphicView;->onLayout(ZIIII)V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->getWidth()I

    move-result v1

    .line 174
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->getHeight()I

    move-result v0

    .line 176
    .local v0, "height":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mBottomOverlay:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mBottomOverlay:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {v2, v4, v3, v1, v0}, Landroid/view/View;->layout(IIII)V

    .line 177
    iget-object v2, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mTitle:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {v2, v4, v3, v1, v0}, Landroid/widget/TextView;->layout(IIII)V

    .line 178
    return-void
.end method

.method protected onMeasure(II)V
    .locals 21
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 87
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mPlayImageView:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/widget/ImageView;->measure(II)V

    .line 89
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 91
    .local v3, "availableWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mTitle:Landroid/widget/TextView;

    move-object/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/widget/TextView;->measure(II)V

    .line 95
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v18, v0

    div-int/lit8 v11, v18, 0x2

    .line 97
    .local v11, "maxHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    .line 101
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/google/android/finsky/utils/UiUtils;->getActionBarHeight(Landroid/content/Context;)I

    move-result v18

    mul-int/lit8 v18, v18, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mTitle:Landroid/widget/TextView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v19

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 103
    .local v10, "imagelessHeight":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v10}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->setMeasuredDimension(II)V

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mBottomOverlay:Landroid/view/View;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/high16 v20, 0x40000000    # 2.0f

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 153
    .end local v10    # "imagelessHeight":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->getMeasuredHeight()I

    move-result v13

    .line 154
    .local v13, "measuredHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getVisibility()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_0

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mCorpusFillView:Landroid/view/View;

    move-object/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v13, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 159
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageOverlayView:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getVisibility()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageOverlayView:Landroid/view/View;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageOverlayView:Landroid/view/View;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    move-object/from16 v0, v19

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 165
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mBottomOverlay:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getVisibility()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_2

    .line 167
    :cond_2
    return-void

    .line 111
    .end local v13    # "measuredHeight":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/image/FifeImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 112
    .local v4, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/image/FifeImageView;->isLoaded()Z

    move-result v18

    if-eqz v18, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mDefaultAspectRatio:F

    move/from16 v18, v0

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_7

    .line 113
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mDefaultAspectRatio:F

    move/from16 v18, v0

    const/16 v19, 0x0

    cmpl-float v18, v18, v19

    if-lez v18, :cond_6

    int-to-float v0, v3

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mDefaultAspectRatio:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-static {v11, v0}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 116
    .local v5, "height":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v5, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 118
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v5}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->setMeasuredDimension(II)V

    .line 142
    .end local v5    # "height":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mAccessibilityOverlayView:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getVisibility()I

    move-result v18

    const/16 v19, 0x8

    move/from16 v0, v18

    move/from16 v1, v19

    if-eq v0, v1, :cond_5

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v7

    .line 144
    .local v7, "heroImageWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v6

    .line 145
    .local v6, "heroImageHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mAccessibilityOverlayView:Landroid/view/View;

    move-object/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v7, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    const/high16 v20, 0x40000000    # 2.0f

    move/from16 v0, v20

    invoke-static {v6, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    invoke-virtual/range {v18 .. v20}, Landroid/view/View;->measure(II)V

    .line 149
    .end local v6    # "heroImageHeight":I
    .end local v7    # "heroImageWidth":I
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mBottomOverlay:Landroid/view/View;

    move-object/from16 v18, v0

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->getMeasuredHeight()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    const/high16 v20, 0x40000000    # 2.0f

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    goto/16 :goto_0

    :cond_6
    move v5, v11

    .line 113
    goto :goto_1

    .line 120
    :cond_7
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v9

    .line 121
    .local v9, "imWidth":I
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    .line 122
    .local v8, "imHeight":I
    mul-int v18, v3, v8

    div-int v14, v18, v9

    .line 124
    .local v14, "scaledHeight":I
    if-gt v14, v11, :cond_8

    .line 127
    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v18

    invoke-static {v14, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v15

    .line 129
    .local v15, "scaledHeightSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, p1

    invoke-virtual {v0, v1, v15}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 130
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v14}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->setMeasuredDimension(II)V

    goto/16 :goto_2

    .line 133
    .end local v15    # "scaledHeightSpec":I
    :cond_8
    mul-int v18, v11, v9

    div-int v16, v18, v8

    .line 134
    .local v16, "scaledWidth":I
    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v18

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v17

    .line 136
    .local v17, "scaledWidthSpec":I
    const/high16 v18, 0x40000000    # 2.0f

    move/from16 v0, v18

    invoke-static {v11, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 138
    .local v12, "maxHeightSpec":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->mHeroImageView:Lcom/google/android/play/image/FifeImageView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-virtual {v0, v1, v12}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 139
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v11}, Lcom/google/android/finsky/layout/EditorialHeroGraphicView;->setMeasuredDimension(II)V

    goto/16 :goto_2
.end method

.method protected shouldTopAlignHeroImage()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method
