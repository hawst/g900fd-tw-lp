.class public Lcom/google/android/finsky/layout/ReviewsControlContainer;
.super Landroid/widget/LinearLayout;
.source "ReviewsControlContainer.java"


# instance fields
.field private mFilterBox:Landroid/widget/TextView;

.field private mSortBox:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method public configure(Lcom/google/android/finsky/api/model/DfeReviews;Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;)V
    .locals 3
    .param p1, "reviewsData"    # Lcom/google/android/finsky/api/model/DfeReviews;
    .param p2, "listingOptionsHandler"    # Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mSortBox:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/DfeReviews;->getSortType()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/ReviewsSortingUtils;->getDisplayString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mSortBox:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/finsky/layout/ReviewsControlContainer$1;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/layout/ReviewsControlContainer$1;-><init>(Lcom/google/android/finsky/layout/ReviewsControlContainer;Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mFilterBox:Landroid/widget/TextView;

    new-instance v1, Lcom/google/android/finsky/layout/ReviewsControlContainer$2;

    invoke-direct {v1, p0, p2}, Lcom/google/android/finsky/layout/ReviewsControlContainer$2;-><init>(Lcom/google/android/finsky/layout/ReviewsControlContainer;Lcom/google/android/finsky/adapters/ReviewsAdapter$ChooseListingOptionsHandler;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 28
    const v0, 0x7f0a0366

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mSortBox:Landroid/widget/TextView;

    .line 29
    const v0, 0x7f0a0367

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ReviewsControlContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ReviewsControlContainer;->mFilterBox:Landroid/widget/TextView;

    .line 30
    return-void
.end method
