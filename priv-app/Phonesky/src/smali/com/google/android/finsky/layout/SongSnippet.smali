.class public Lcom/google/android/finsky/layout/SongSnippet;
.super Landroid/widget/RelativeLayout;
.source "SongSnippet.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mAddedDrawable:Landroid/widget/ImageView;

.field private mAddedState:Landroid/widget/TextView;

.field private mAlbumDocument:Lcom/google/android/finsky/api/model/Document;

.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

.field private final mConnection:Lcom/google/android/finsky/previews/PreviewController;

.field private mInitialized:Z

.field private mIsNewPurchase:Z

.field private mIsPlayable:Z

.field private mMusicDetails:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mShouldShowArtistName:Z

.field private mSongBadge:Landroid/widget/ImageView;

.field private mSongDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

.field private mSongDocument:Lcom/google/android/finsky/api/model/Document;

.field private mSongDuration:Landroid/widget/TextView;

.field private mSongIndex:Lcom/google/android/finsky/layout/SongIndex;

.field private mSongSubTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

.field private mSongTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

.field private final mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

.field private mTrackNumber:I

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    const/16 v0, 0x1f6

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 355
    new-instance v0, Lcom/google/android/finsky/layout/SongSnippet$5;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/layout/SongSnippet$5;-><init>(Lcom/google/android/finsky/layout/SongSnippet;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    .line 80
    new-instance v0, Lcom/google/android/finsky/previews/PreviewController;

    iget-object v1, p0, Lcom/google/android/finsky/layout/SongSnippet;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-direct {v0, v1}, Lcom/google/android/finsky/previews/PreviewController;-><init>(Lcom/google/android/finsky/previews/StatusListener;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mConnection:Lcom/google/android/finsky/previews/PreviewController;

    .line 81
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/SongSnippet;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongSnippet;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongBadge:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/SongSnippet;)Lcom/google/android/finsky/protos/DocDetails$SongDetails;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongSnippet;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/SongSnippet;)Lcom/google/android/finsky/previews/PreviewController;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongSnippet;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mConnection:Lcom/google/android/finsky/previews/PreviewController;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/SongSnippet;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongSnippet;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/layout/SongSnippet;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongSnippet;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/finsky/layout/SongSnippet;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongSnippet;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/SongSnippet;->mInitialized:Z

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/finsky/layout/SongSnippet;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongSnippet;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SongSnippet;->resetUI()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/finsky/layout/SongSnippet;)Lcom/google/android/finsky/layout/SongIndex;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongSnippet;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongIndex:Lcom/google/android/finsky/layout/SongIndex;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/finsky/layout/SongSnippet;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/SongSnippet;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/SongSnippet;->setHighlighted(Z)V

    return-void
.end method

.method private clearBuyButtonStyle(I)V
    .locals 3
    .param p1, "buttonTextResourceId"    # I

    .prologue
    const/4 v2, 0x2

    .line 337
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setDrawAsLabel(Z)V

    .line 338
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v0, v2}, Lcom/google/android/play/layout/PlayActionButton;->setActionStyle(I)V

    .line 339
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 340
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, p1, v1}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 341
    return-void
.end method

.method private resetUI()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 260
    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/SongSnippet;->setHighlighted(Z)V

    .line 261
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongIndex:Lcom/google/android/finsky/layout/SongIndex;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/SongIndex;->setState(I)V

    .line 262
    return-void
.end method

.method private setBuyButtonStyle()V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setDrawAsLabel(Z)V

    .line 332
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setActionStyle(I)V

    .line 333
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 334
    return-void
.end method

.method private setHighlighted(Z)V
    .locals 9
    .param p1, "isHighlighted"    # Z

    .prologue
    const v8, 0x7f0900fc

    .line 265
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SongSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 266
    .local v5, "r":Landroid/content/res/Resources;
    if-eqz p1, :cond_0

    .line 267
    const v6, 0x7f02003a

    invoke-virtual {p0, v6}, Lcom/google/android/finsky/layout/SongSnippet;->setBackgroundResource(I)V

    .line 268
    const v6, 0x7f09004c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 269
    .local v0, "colorWhite":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setTextColor(I)V

    .line 270
    iget-object v6, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongSubTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v6, v0}, Lcom/google/android/finsky/layout/DecoratedTextView;->setTextColor(I)V

    .line 271
    iget-object v6, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDuration:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 283
    .end local v0    # "colorWhite":I
    :goto_0
    return-void

    .line 273
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SongSnippet;->getPaddingLeft()I

    move-result v2

    .line 274
    .local v2, "paddingLeft":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SongSnippet;->getPaddingRight()I

    move-result v3

    .line 275
    .local v3, "paddingRight":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SongSnippet;->getPaddingTop()I

    move-result v4

    .line 276
    .local v4, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SongSnippet;->getPaddingBottom()I

    move-result v1

    .line 277
    .local v1, "paddingBottom":I
    const v6, 0x7f02017f

    invoke-virtual {p0, v6}, Lcom/google/android/finsky/layout/SongSnippet;->setBackgroundResource(I)V

    .line 278
    invoke-virtual {p0, v2, v4, v3, v1}, Lcom/google/android/finsky/layout/SongSnippet;->setPadding(IIII)V

    .line 279
    iget-object v6, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    const v7, 0x7f09005b

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/layout/DecoratedTextView;->setTextColor(I)V

    .line 280
    iget-object v6, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongSubTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/google/android/finsky/layout/DecoratedTextView;->setTextColor(I)V

    .line 281
    iget-object v6, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDuration:Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private updateAddedState()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 344
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mIsNewPurchase:Z

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDuration:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 346
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mAddedState:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 347
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mAddedDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 353
    :goto_0
    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDuration:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mAddedState:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mAddedDrawable:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateBuyButtonState()V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const v6, 0x7f0c0337

    const/4 v5, 0x4

    const/4 v13, 0x2

    const/4 v3, 0x1

    .line 286
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    .line 287
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v9

    .line 289
    .local v9, "library":Lcom/google/android/finsky/library/Library;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v1

    .line 290
    .local v1, "currentAccount":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v8

    .line 291
    .local v8, "libraries":Lcom/google/android/finsky/library/Libraries;
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v0, v8, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v10

    .line 294
    .local v10, "owner":Landroid/accounts/Account;
    if-eqz v10, :cond_0

    .line 295
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SongSnippet;->setBuyButtonStyle()V

    .line 296
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    const v2, 0x7f0c020e

    new-instance v3, Lcom/google/android/finsky/layout/SongSnippet$4;

    invoke-direct {v3, p0, v10}, Lcom/google/android/finsky/layout/SongSnippet$4;-><init>(Lcom/google/android/finsky/layout/SongSnippet;Landroid/accounts/Account;)V

    invoke-virtual {v0, v13, v2, v3}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 327
    :goto_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SongSnippet;->updateAddedState()V

    .line 328
    return-void

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 306
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SongSnippet;->setBuyButtonStyle()V

    .line 307
    iget-object v11, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/api/model/Document;->getFormattedPrice(I)Ljava/lang/String;

    move-result-object v12

    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v2, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    const/16 v6, 0xc8

    move-object v5, v4

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->getBuyImmediateClickListener(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v11, v13, v12, v0}, Lcom/google/android/play/layout/PlayActionButton;->configure(ILjava/lang/String;Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v2

    invoke-static {v0, v2, v9}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 313
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getAvailabilityRestriction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 318
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    goto :goto_0

    .line 315
    :pswitch_0
    invoke-direct {p0, v6}, Lcom/google/android/finsky/layout/SongSnippet;->clearBuyButtonStyle(I)V

    goto :goto_0

    .line 321
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mAlbumDocument:Lcom/google/android/finsky/api/model/Document;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mAlbumDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 323
    invoke-direct {p0, v6}, Lcom/google/android/finsky/layout/SongSnippet;->clearBuyButtonStyle(I)V

    goto :goto_0

    .line 325
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    invoke-virtual {v0, v5}, Lcom/google/android/play/layout/PlayActionButton;->setVisibility(I)V

    goto :goto_0

    .line 313
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 399
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public initialize()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 235
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mInitialized:Z

    if-nez v0, :cond_0

    .line 236
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/SongSnippet;->setState(I)V

    .line 237
    iput-boolean v1, p0, Lcom/google/android/finsky/layout/SongSnippet;->mInitialized:Z

    .line 239
    :cond_0
    return-void
.end method

.method public isPlayable()Z
    .locals 1

    .prologue
    .line 231
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mIsPlayable:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v8, 0x0

    .line 85
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 87
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mMusicDetails:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    if-nez v9, :cond_0

    .line 88
    invoke-virtual {p0, v13}, Lcom/google/android/finsky/layout/SongSnippet;->setVisibility(I)V

    .line 180
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongIndex:Lcom/google/android/finsky/layout/SongIndex;

    iget v10, p0, Lcom/google/android/finsky/layout/SongSnippet;->mTrackNumber:I

    invoke-virtual {v9, v10}, Lcom/google/android/finsky/layout/SongIndex;->setTrackNumber(I)V

    .line 93
    iget-boolean v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mIsPlayable:Z

    if-eqz v9, :cond_3

    .line 94
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mMusicDetails:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iget v9, v9, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->durationSec:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v3

    .line 95
    .local v3, "formattedDuration":Ljava/lang/String;
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDuration:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDuration:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/SongSnippet;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c03e4

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    aput-object v3, v12, v8

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 99
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDuration:Landroid/widget/TextView;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    .end local v3    # "formattedDuration":Ljava/lang/String;
    :goto_1
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    iget-object v10, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    iget-object v9, v9, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    if-eqz v9, :cond_5

    .line 106
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongBadge:Landroid/widget/ImageView;

    invoke-virtual {v9, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 107
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    iget-object v7, v9, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->badge:Lcom/google/android/finsky/protos/DocAnnotations$Badge;

    .line 108
    .local v7, "songBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongBadge:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    .line 109
    .local v2, "badgeSize":I
    const/4 v9, 0x6

    invoke-static {v7, v9}, Lcom/google/android/finsky/utils/BadgeUtils;->getImage(Lcom/google/android/finsky/protos/DocAnnotations$Badge;I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v1

    .line 111
    .local v1, "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v1, :cond_1

    .line 112
    iget-boolean v9, v1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v9, :cond_4

    move v6, v2

    .line 113
    .local v6, "requestSize":I
    :goto_2
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v10, v1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    new-instance v11, Lcom/google/android/finsky/layout/SongSnippet$1;

    invoke-direct {v11, p0}, Lcom/google/android/finsky/layout/SongSnippet$1;-><init>(Lcom/google/android/finsky/layout/SongSnippet;)V

    invoke-virtual {v9, v10, v6, v6, v11}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v4

    .line 123
    .local v4, "holder":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    invoke-virtual {v4}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 124
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongBadge:Landroid/widget/ImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 132
    .end local v1    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v2    # "badgeSize":I
    .end local v4    # "holder":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .end local v6    # "requestSize":I
    .end local v7    # "songBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_1
    :goto_3
    iget-boolean v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mShouldShowArtistName:Z

    if-eqz v9, :cond_6

    .line 133
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongSubTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    iget-object v10, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v10}, Lcom/google/android/finsky/api/model/Document;->getCreator()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    iget-object v10, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v11, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongSubTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-static {v9, v10, v11}, Lcom/google/android/finsky/utils/BadgeUtils;->configureCreatorBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/DecoratedTextView;)V

    .line 139
    :goto_4
    invoke-direct {p0}, Lcom/google/android/finsky/layout/SongSnippet;->updateBuyButtonState()V

    .line 141
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongIndex:Lcom/google/android/finsky/layout/SongIndex;

    invoke-virtual {v9, v8}, Lcom/google/android/finsky/layout/SongIndex;->setClickable(Z)V

    .line 143
    iget-boolean v8, p0, Lcom/google/android/finsky/layout/SongSnippet;->mIsPlayable:Z

    if-eqz v8, :cond_7

    .line 144
    iget-object v8, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v8}, Lcom/google/android/finsky/api/model/Document;->isMature()Z

    move-result v5

    .line 145
    .local v5, "isMature":Z
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v8

    iget-object v0, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 146
    .local v0, "accountName":Ljava/lang/String;
    new-instance v8, Lcom/google/android/finsky/layout/SongSnippet$2;

    invoke-direct {v8, p0, v5, v0}, Lcom/google/android/finsky/layout/SongSnippet$2;-><init>(Lcom/google/android/finsky/layout/SongSnippet;ZLjava/lang/String;)V

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/SongSnippet;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v5    # "isMature":Z
    :goto_5
    sget-object v8, Lcom/google/android/finsky/config/G;->prePurchaseSharingEnabled:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v8}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 165
    new-instance v8, Lcom/google/android/finsky/layout/SongSnippet$3;

    invoke-direct {v8, p0}, Lcom/google/android/finsky/layout/SongSnippet$3;-><init>(Lcom/google/android/finsky/layout/SongSnippet;)V

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/SongSnippet;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 179
    :cond_2
    iget-object v8, p0, Lcom/google/android/finsky/layout/SongSnippet;->mConnection:Lcom/google/android/finsky/previews/PreviewController;

    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mStatusListener:Lcom/google/android/finsky/previews/StatusListener;

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/previews/PreviewController;->getStatusUpdate(Lcom/google/android/finsky/previews/StatusListener;)V

    goto/16 :goto_0

    .line 101
    :cond_3
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDuration:Landroid/widget/TextView;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .restart local v1    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    .restart local v2    # "badgeSize":I
    .restart local v7    # "songBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_4
    move v6, v8

    .line 112
    goto :goto_2

    .line 128
    .end local v1    # "badgeImage":Lcom/google/android/finsky/protos/Common$Image;
    .end local v2    # "badgeSize":I
    .end local v7    # "songBadge":Lcom/google/android/finsky/protos/DocAnnotations$Badge;
    :cond_5
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongBadge:Landroid/widget/ImageView;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 136
    :cond_6
    iget-object v9, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongSubTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    invoke-virtual {v9, v13}, Lcom/google/android/finsky/layout/DecoratedTextView;->setVisibility(I)V

    goto :goto_4

    .line 161
    :cond_7
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/google/android/finsky/layout/SongSnippet;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mConnection:Lcom/google/android/finsky/previews/PreviewController;

    invoke-virtual {v0}, Lcom/google/android/finsky/previews/PreviewController;->unbind()V

    .line 185
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 186
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 192
    const v0, 0x7f0a0207

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/SongIndex;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongIndex:Lcom/google/android/finsky/layout/SongIndex;

    .line 193
    const v0, 0x7f0a0138

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBuyButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 194
    const v0, 0x7f0a0264

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongBadge:Landroid/widget/ImageView;

    .line 195
    const v0, 0x7f0a0265

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDuration:Landroid/widget/TextView;

    .line 196
    const v0, 0x7f0a0266

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DecoratedTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 197
    const v0, 0x7f0a0267

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DecoratedTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongSubTitle:Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 198
    const v0, 0x7f0a01c9

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mAddedState:Landroid/widget/TextView;

    .line 199
    const v0, 0x7f0a01c8

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SongSnippet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mAddedDrawable:Landroid/widget/ImageView;

    .line 200
    return-void
.end method

.method public setSongDetails(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;IZLcom/google/android/finsky/navigationmanager/NavigationManager;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "albumDocument"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "songDocument"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "trackNumber"    # I
    .param p5, "shouldShowArtistName"    # Z
    .param p6, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p7, "isNewPurchase"    # Z
    .param p8, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/finsky/layout/SongSnippet;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 207
    iput-boolean p5, p0, Lcom/google/android/finsky/layout/SongSnippet;->mShouldShowArtistName:Z

    .line 208
    iput-object p2, p0, Lcom/google/android/finsky/layout/SongSnippet;->mAlbumDocument:Lcom/google/android/finsky/api/model/Document;

    .line 209
    iput-object p3, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    .line 210
    iput-object p6, p0, Lcom/google/android/finsky/layout/SongSnippet;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 211
    iput p4, p0, Lcom/google/android/finsky/layout/SongSnippet;->mTrackNumber:I

    .line 212
    iput-boolean p7, p0, Lcom/google/android/finsky/layout/SongSnippet;->mIsNewPurchase:Z

    .line 213
    iput-object p8, p0, Lcom/google/android/finsky/layout/SongSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 215
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getSongDetails()Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    .line 216
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->details:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mMusicDetails:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    .line 217
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mMusicDetails:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mMusicDetails:Lcom/google/android/finsky/protos/DocDetails$MusicDetails;

    iget v0, v0, Lcom/google/android/finsky/protos/DocDetails$MusicDetails;->durationSec:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDetails:Lcom/google/android/finsky/protos/DocDetails$SongDetails;

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$SongDetails;->previewUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mIsPlayable:Z

    .line 220
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v1, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongDocument:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 223
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 224
    return-void

    .line 217
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setState(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    const/4 v0, 0x0

    .line 242
    packed-switch p1, :pswitch_data_0

    .line 254
    iget-object v1, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongIndex:Lcom/google/android/finsky/layout/SongIndex;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/SongIndex;->setState(I)V

    .line 257
    :goto_0
    return-void

    .line 244
    :pswitch_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/finsky/layout/SongSnippet;->setHighlighted(Z)V

    .line 245
    iget-object v1, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongIndex:Lcom/google/android/finsky/layout/SongIndex;

    iget-boolean v2, p0, Lcom/google/android/finsky/layout/SongSnippet;->mIsPlayable:Z

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/SongIndex;->setState(I)V

    goto :goto_0

    .line 249
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/SongSnippet;->mSongIndex:Lcom/google/android/finsky/layout/SongIndex;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/SongIndex;->setState(I)V

    goto :goto_0

    .line 242
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
