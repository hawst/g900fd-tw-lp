.class public Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;
.super Lcom/google/android/finsky/fragments/PageFragment;
.source "MyAccountFragment.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;


# instance fields
.field private mBreadcrumb:Ljava/lang/String;

.field private mLastRequestTimeMs:J

.field private mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

.field private mOrderHistoryView:Lcom/google/android/finsky/layout/OrderHistorySection;

.field private mResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/PageFragment;-><init>()V

    .line 49
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$002(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;)Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->handleMyAccountResponse()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->switchToError(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private bindOrderHistoryView()V
    .locals 9

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/OrderHistorySection;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/OrderHistorySection;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/OrderHistorySection;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    iget-object v3, v3, Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;->purchaseHistoryMetadata:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v3, v3, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->header:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    iget-object v5, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v7, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    move-object v6, p0

    move-object v8, p0

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/finsky/layout/OrderHistorySection;->bind(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;Lcom/google/android/finsky/api/model/DfeList;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 205
    return-void
.end method

.method private bindPaymentMethodsFragment()V
    .locals 3

    .prologue
    const v2, 0x7f0a026b

    .line 189
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 196
    :goto_0
    return-void

    .line 192
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->newInstance(Landroid/accounts/Account;)Lcom/google/android/finsky/billing/PaymentMethodsFragment;

    move-result-object v0

    .line 193
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const/16 v2, 0x1003

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method private clearOrderHistoryDfeList()V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 161
    :cond_0
    return-void
.end method

.method private handleMyAccountResponse()V
    .locals 4

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->switchToData()V

    .line 144
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->rebindViews()V

    .line 146
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->clearOrderHistoryDfeList()V

    .line 147
    new-instance v0, Lcom/google/android/finsky/api/model/DfeList;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    iget-object v2, v2, Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;->purchaseHistoryMetadata:Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;

    iget-object v2, v2, Lcom/google/android/finsky/protos/DocAnnotations$SectionMetadata;->listUrl:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/finsky/api/model/DfeList;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    .line 149
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->startLoadItems()V

    .line 152
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mLastRequestTimeMs:J

    .line 153
    return-void
.end method

.method public static newInstance()Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;-><init>()V

    .line 60
    .local v0, "fragment":Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->setDfeToc(Lcom/google/android/finsky/api/model/DfeToc;)V

    .line 61
    return-object v0
.end method


# virtual methods
.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 107
    const v0, 0x7f0400d8

    return v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public isDataReady()Z
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeList;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mContext:Landroid/content/Context;

    const v1, 0x7f0c0392

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mBreadcrumb:Ljava/lang/String;

    .line 75
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mDataView:Landroid/view/ViewGroup;

    const v1, 0x7f0a026c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/OrderHistorySection;

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/OrderHistorySection;

    .line 76
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/OrderHistorySection;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/OrderHistorySection;->setVisibility(I)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->rebindActionBar()V

    .line 79
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mActionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-interface {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->disableActionBarOverlay()V

    .line 81
    iget-wide v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mLastRequestTimeMs:J

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/OrderHistoryHelper;->hasMutationOccurredSince(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->clearOrderHistoryDfeList()V

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->isDataReady()Z

    move-result v0

    if-nez v0, :cond_1

    .line 86
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->requestData()V

    .line 90
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->bindPaymentMethodsFragment()V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->switchToLoadingImmediately()V

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->rebindViews()V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 209
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/finsky/fragments/PageFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0a026b

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 213
    .local v0, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v0, :cond_0

    .line 214
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 216
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/PageFragment;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->setRetainInstance(Z)V

    .line 68
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/OrderHistorySection;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/OrderHistorySection;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/OrderHistorySection;->onDestroyView()V

    .line 222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryView:Lcom/google/android/finsky/layout/OrderHistorySection;

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mOrderHistoryDfeList:Lcom/google/android/finsky/api/model/DfeList;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeList;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 227
    :cond_1
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onDestroyView()V

    .line 228
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 282
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 5
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 259
    if-ne p1, v4, :cond_0

    .line 260
    const-string v2, "package_name"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 261
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "account_name"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 262
    .local v0, "appRefundAccount":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    new-instance v3, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$3;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$3;-><init>(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;)V

    invoke-static {v2, v1, v0, v4, v3}, Lcom/google/android/finsky/utils/AppSupport;->silentRefund(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/finsky/utils/AppSupport$RefundListener;)V

    .line 277
    .end local v0    # "appRefundAccount":Ljava/lang/String;
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onRefundAction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "appRefundAccount"    # Ljava/lang/String;

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    .line 236
    .local v3, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v4, "refund_confirm"

    invoke-virtual {v3, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 250
    :goto_0
    return-void

    .line 240
    :cond_0
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 241
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v4, 0x7f0c02a7

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0c01b1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0c01b2

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 244
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 245
    .local v2, "extraArguments":Landroid/os/Bundle;
    const-string v4, "package_name"

    invoke-virtual {v2, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v4, "account_name"

    invoke-virtual {v2, v4, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const/4 v4, 0x1

    invoke-virtual {v0, p0, v4, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 248
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 249
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    const-string v4, "refund_confirm"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onRetry()V
    .locals 3

    .prologue
    .line 175
    invoke-super {p0}, Lcom/google/android/finsky/fragments/PageFragment;->onRetry()V

    .line 176
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0a026b

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/PaymentMethodsFragment;

    .line 179
    .local v0, "fragment":Lcom/google/android/finsky/billing/PaymentMethodsFragment;
    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {v0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->onRetry()V

    .line 182
    :cond_0
    return-void
.end method

.method public rebindActionBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 101
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    iget-object v1, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mBreadcrumb:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v0, v2, v2}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 103
    return-void
.end method

.method protected rebindViews()V
    .locals 4

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 166
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/finsky/utils/UiUtils;->getGridHorizontalPadding(Landroid/content/res/Resources;)I

    move-result v0

    .line 167
    .local v0, "sideMargin":I
    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v1, v0, v2, v0, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 169
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->bindPaymentMethodsFragment()V

    .line 170
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->bindOrderHistoryView()V

    .line 171
    return-void
.end method

.method protected requestData()V
    .locals 3

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mResponse:Lcom/google/android/finsky/protos/MyAccount$MyAccountResponse;

    if-nez v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    new-instance v1, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$1;-><init>(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;)V

    new-instance v2, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment$2;-><init>(Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->getMyAccount(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 139
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/activities/myaccount/MyAccountFragment;->handleMyAccountResponse()V

    goto :goto_0
.end method
