.class public final Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CheckPromoOffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CheckPromoOffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckPromoOfferResponse"
.end annotation


# instance fields
.field public availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

.field public availablePromoOfferStatus:I

.field public checkoutTokenRequired:Z

.field public hasAvailablePromoOfferStatus:Z

.field public hasCheckoutTokenRequired:Z

.field public redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->clear()Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    .line 47
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-static {}, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;->emptyArray()[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    .line 52
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->checkoutTokenRequired:Z

    .line 53
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasCheckoutTokenRequired:Z

    .line 54
    iput v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availablePromoOfferStatus:I

    .line 55
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasAvailablePromoOfferStatus:Z

    .line 56
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->cachedSize:I

    .line 57
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 86
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 87
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 88
    iget-object v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    aget-object v0, v3, v1

    .line 89
    .local v0, "element":Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    if-eqz v0, :cond_0

    .line 90
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 87
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 95
    .end local v0    # "element":Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    .end local v1    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v3, :cond_2

    .line 96
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 99
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasCheckoutTokenRequired:Z

    if-nez v3, :cond_3

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->checkoutTokenRequired:Z

    if-eqz v3, :cond_4

    .line 100
    :cond_3
    const/4 v3, 0x3

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->checkoutTokenRequired:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 103
    :cond_4
    iget v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availablePromoOfferStatus:I

    if-nez v3, :cond_5

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasAvailablePromoOfferStatus:Z

    if-eqz v3, :cond_6

    .line 104
    :cond_5
    const/4 v3, 0x4

    iget v4, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availablePromoOfferStatus:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 107
    :cond_6
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 115
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 116
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 120
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 121
    :sswitch_0
    return-object p0

    .line 126
    :sswitch_1
    const/16 v6, 0xa

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 128
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    if-nez v6, :cond_2

    move v1, v5

    .line 129
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    .line 131
    .local v2, "newArray":[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    if-eqz v1, :cond_1

    .line 132
    iget-object v6, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 134
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 135
    new-instance v6, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;-><init>()V

    aput-object v6, v2, v1

    .line 136
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 137
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 128
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    array-length v1, v6

    goto :goto_1

    .line 140
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;-><init>()V

    aput-object v6, v2, v1

    .line 141
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 142
    iput-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    goto :goto_0

    .line 146
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    :sswitch_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-nez v6, :cond_4

    .line 147
    new-instance v6, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    .line 149
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 153
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->checkoutTokenRequired:Z

    .line 154
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasCheckoutTokenRequired:Z

    goto :goto_0

    .line 158
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 159
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 164
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availablePromoOfferStatus:I

    .line 165
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasAvailablePromoOfferStatus:Z

    goto :goto_0

    .line 116
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    .line 159
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 63
    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 64
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 65
    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availableOffer:[Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;

    aget-object v0, v2, v1

    .line 66
    .local v0, "element":Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    if-eqz v0, :cond_0

    .line 67
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 64
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 71
    .end local v0    # "element":Lcom/google/android/finsky/protos/CheckPromoOffer$AvailablePromoOffer;
    .end local v1    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    if-eqz v2, :cond_2

    .line 72
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->redeemedOffer:Lcom/google/android/finsky/protos/CheckPromoOffer$RedeemedPromoOffer;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 74
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasCheckoutTokenRequired:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->checkoutTokenRequired:Z

    if-eqz v2, :cond_4

    .line 75
    :cond_3
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->checkoutTokenRequired:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 77
    :cond_4
    iget v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availablePromoOfferStatus:I

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->hasAvailablePromoOfferStatus:Z

    if-eqz v2, :cond_6

    .line 78
    :cond_5
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/finsky/protos/CheckPromoOffer$CheckPromoOfferResponse;->availablePromoOfferStatus:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 80
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 81
    return-void
.end method
