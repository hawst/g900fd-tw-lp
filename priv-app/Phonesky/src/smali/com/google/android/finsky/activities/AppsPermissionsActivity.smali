.class public Lcom/google/android/finsky/activities/AppsPermissionsActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "AppsPermissionsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;
.implements Lcom/google/android/finsky/layout/play/RootUiElementNode;


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mContinueButton:Lcom/google/android/finsky/layout/IconButtonGroup;

.field private mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mShowDetailedPermissions:Z

.field private final mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 110
    const/16 v0, 0x316

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method public static createIntent(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Z)Landroid/content/Intent;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "docidStr"    # Ljava/lang/String;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "showDetailedPermissions"    # Z

    .prologue
    .line 115
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/activities/AppsPermissionsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "AppsPermissionsActivity.accountName"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    const-string v1, "AppsPermissionsActivity.docidStr"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    const-string v1, "AppsPermissionsActivity.doc"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 119
    const-string v1, "AppsPermissionsActivity.showDetailedPermissions"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 120
    return-object v0
.end method

.method public static extractAcceptedNewBuckets(Landroid/content/Intent;)Z
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 136
    const-string v0, "AppsPermissionsActivity.acceptedNewBuckets"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static extractDoc(Landroid/content/Intent;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "data"    # Landroid/content/Intent;

    .prologue
    .line 124
    const-string v0, "AppsPermissionsActivity.doc"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method public static extractTitle(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 132
    const-string v0, "AppsPermissionsActivity.appTitle"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static extractVersionCode(Landroid/content/Intent;)I
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 128
    const-string v0, "AppsPermissionsActivity.appVersion"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private finishWithResultOK(Z)V
    .locals 3
    .param p1, "acceptedNewBuckets"    # Z

    .prologue
    .line 344
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 345
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "AppsPermissionsActivity.doc"

    iget-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 346
    const-string v1, "AppsPermissionsActivity.appVersion"

    iget-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v2

    iget v2, v2, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionCode:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 347
    const-string v1, "AppsPermissionsActivity.appTitle"

    iget-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    const-string v1, "AppsPermissionsActivity.appDownloadSizeWarningArguments"

    invoke-direct {p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->getDownloadWarningArguments()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 350
    const-string v1, "AppsPermissionsActivity.acceptedNewBuckets"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 351
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->setResult(ILandroid/content/Intent;)V

    .line 352
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->finish()V

    .line 353
    return-void
.end method

.method public static getDownloadSizeWarningArguments(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 1
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 140
    const-string v0, "AppsPermissionsActivity.appDownloadSizeWarningArguments"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private getDownloadWarningArguments()Landroid/os/Bundle;
    .locals 12

    .prologue
    const/4 v8, 0x1

    .line 356
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v1

    .line 357
    .local v1, "installPolicies":Lcom/google/android/finsky/installer/InstallPolicies;
    invoke-virtual {v1}, Lcom/google/android/finsky/installer/InstallPolicies;->getMaxBytesOverMobileRecommended()J

    move-result-wide v4

    .line 358
    .local v4, "maxRecommendedBytes":J
    invoke-virtual {v1}, Lcom/google/android/finsky/installer/InstallPolicies;->getMaxBytesOverMobile()J

    move-result-wide v2

    .line 359
    .local v2, "maxLimitBytes":J
    iget-object v9, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v9}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v0

    .line 361
    .local v0, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/finsky/installer/InstallPolicies;->hasMobileNetwork()Z

    move-result v9

    if-eqz v9, :cond_1

    const-wide/16 v10, 0x0

    cmp-long v9, v4, v10

    if-lez v9, :cond_1

    invoke-static {v0}, Lcom/google/android/finsky/local/AssetUtils;->totalDeliverySize(Lcom/google/android/finsky/protos/DocDetails$AppDetails;)J

    move-result-wide v10

    cmp-long v9, v10, v4

    if-ltz v9, :cond_1

    .line 371
    iget-object v9, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v9}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v9

    iget-wide v10, v9, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->installationSize:J

    cmp-long v9, v10, v2

    if-gez v9, :cond_0

    move v7, v8

    .line 373
    .local v7, "showWifiOnlyOption":Z
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/finsky/installer/InstallPolicies;->isMobileNetwork()Z

    move-result v6

    .line 374
    .local v6, "onMobileNetwork":Z
    invoke-static {v8, v7, v6}, Lcom/google/android/finsky/billing/DownloadSizeDialogFragment;->makeArguments(ZZZ)Landroid/os/Bundle;

    move-result-object v8

    .line 377
    .end local v6    # "onMobileNetwork":Z
    .end local v7    # "showWifiOnlyOption":Z
    :goto_1
    return-object v8

    .line 371
    :cond_0
    const/4 v7, 0x0

    goto :goto_0

    .line 377
    :cond_1
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private hideLoading()V
    .locals 2

    .prologue
    .line 329
    const v0, 0x7f0a022d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 330
    const v0, 0x7f0a0109

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 331
    return-void
.end method

.method private showError(Ljava/lang/String;)V
    .locals 4
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 413
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 414
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c02a0

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 415
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 416
    .local v1, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "AppsPermissionsActivity.errorDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 417
    return-void
.end method

.method private showLoading()V
    .locals 4

    .prologue
    .line 323
    const v0, 0x7f0a022d

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 324
    const v0, 0x7f0a0109

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v1, 0xd5

    invoke-virtual {v0, v2, v3, v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 326
    return-void
.end method

.method private updateFromDoc()V
    .locals 28

    .prologue
    .line 208
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->hideLoading()V

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v17

    .line 210
    .local v17, "packageName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v23

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->oBSOLETEPermission:[Ljava/lang/String;

    move-object/from16 v19, v0

    .line 213
    .local v19, "permissions":[Ljava/lang/String;
    const v23, 0x7f0a009c

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v21

    check-cast v21, Landroid/widget/TextView;

    .line 214
    .local v21, "titleView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v21

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 217
    const v23, 0x7f0a022b

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 218
    .local v20, "subHeader":Landroid/widget/TextView;
    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    const v23, 0x7f0a022e

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/play/image/FifeImageView;

    .line 222
    .local v7, "appIcon":Lcom/google/android/play/image/FifeImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v23, v0

    invoke-virtual {v7}, Lcom/google/android/play/image/FifeImageView;->getWidth()I

    move-result v24

    invoke-virtual {v7}, Lcom/google/android/play/image/FifeImageView;->getHeight()I

    move-result v25

    sget-object v26, Lcom/google/android/finsky/utils/PlayCardImageTypeSequence;->CORE_IMAGE_TYPES:[I

    invoke-static/range {v23 .. v26}, Lcom/google/android/finsky/utils/image/ThumbnailUtils;->getImageFromDocument(Lcom/google/android/finsky/api/model/Document;II[I)Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v13

    .line 224
    .local v13, "image":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v13, :cond_1

    .line 225
    iget-object v0, v13, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    move-object/from16 v23, v0

    iget-boolean v0, v13, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    move/from16 v24, v0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v25

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v7, v0, v1, v2}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 227
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v7, v0}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 232
    :goto_0
    const v23, 0x7f0a022f

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 233
    .local v11, "footerText":Landroid/widget/TextView;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v14

    .line 235
    .local v14, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    if-eqz v14, :cond_2

    invoke-virtual {v14}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPermissionsVersion()I

    move-result v23

    const/16 v24, 0x1

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    const/4 v12, 0x1

    .line 237
    .local v12, "hasAcceptedBuckets":Z
    :goto_1
    const/4 v6, 0x0

    .line 238
    .local v6, "adapter":Lcom/google/android/finsky/layout/PermissionAdapter;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mShowDetailedPermissions:Z

    move/from16 v23, v0

    if-eqz v23, :cond_4

    .line 240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mSavedInstanceState:Landroid/os/Bundle;

    move-object/from16 v23, v0

    if-nez v23, :cond_0

    .line 241
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 242
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-object/from16 v23, v0

    const-wide/16 v24, 0x0

    const/16 v26, 0x319

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    move/from16 v3, v26

    move-object/from16 v4, p0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 245
    :cond_0
    sget-object v23, Lcom/google/android/finsky/config/G;->permissionBucketsLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 246
    .local v16, "learnMoreLink":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_3

    .line 247
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f0c028b

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    aput-object v16, v25, v26

    invoke-virtual/range {v23 .. v25}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    .line 249
    .local v10, "detailsFooterSeq":Ljava/lang/CharSequence;
    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    new-instance v23, Lcom/google/android/finsky/activities/AppsPermissionsActivity$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/activities/AppsPermissionsActivity$1;-><init>(Lcom/google/android/finsky/activities/AppsPermissionsActivity;Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 256
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 262
    .end local v10    # "detailsFooterSeq":Ljava/lang/CharSequence;
    :goto_2
    new-instance v9, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    invoke-direct {v9, v0, v1, v2}, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    .line 266
    .local v9, "detailedPermissionsAdapter":Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v23

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->versionString:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 267
    .local v22, "version":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f0c0287

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aput-object v22, v25, v26

    invoke-virtual/range {v23 .. v25}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    const v23, 0x7f0a0230

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v23

    const/16 v24, 0x8

    invoke-virtual/range {v23 .. v24}, Landroid/view/View;->setVisibility(I)V

    .line 271
    move-object v6, v9

    .line 318
    .end local v9    # "detailedPermissionsAdapter":Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;
    .end local v16    # "learnMoreLink":Ljava/lang/String;
    .end local v22    # "version":Ljava/lang/String;
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mSavedInstanceState:Landroid/os/Bundle;

    move-object/from16 v25, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v6, v1, v2}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->bindInfo(Lcom/google/android/finsky/layout/PermissionAdapter;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->requestFocus()Z

    .line 320
    return-void

    .line 229
    .end local v6    # "adapter":Lcom/google/android/finsky/layout/PermissionAdapter;
    .end local v11    # "footerText":Landroid/widget/TextView;
    .end local v12    # "hasAcceptedBuckets":Z
    .end local v14    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_1
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v7, v0}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 235
    .restart local v11    # "footerText":Landroid/widget/TextView;
    .restart local v14    # "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :cond_2
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 258
    .restart local v6    # "adapter":Lcom/google/android/finsky/layout/PermissionAdapter;
    .restart local v12    # "hasAcceptedBuckets":Z
    .restart local v16    # "learnMoreLink":Ljava/lang/String;
    :cond_3
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 274
    .end local v16    # "learnMoreLink":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mSavedInstanceState:Landroid/os/Bundle;

    move-object/from16 v23, v0

    if-nez v23, :cond_5

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v24

    invoke-static/range {v23 .. v24}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-object/from16 v23, v0

    const-wide/16 v24, 0x0

    const/16 v26, 0x317

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    move/from16 v3, v26

    move-object/from16 v4, p0

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 280
    :cond_5
    const/16 v23, 0x8

    move/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 283
    new-instance v18, Lcom/google/android/finsky/layout/AppPermissionAdapter;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v17

    move-object/from16 v3, v19

    invoke-direct {v0, v1, v2, v3, v12}, Lcom/google/android/finsky/layout/AppPermissionAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 287
    .local v18, "permissionAdapter":Lcom/google/android/finsky/layout/AppPermissionAdapter;
    if-nez v12, :cond_6

    sget-object v23, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPermissionBucketsLearnMore:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/Boolean;

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v23

    if-nez v23, :cond_6

    .line 289
    const v23, 0x7f0a02b3

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    .line 290
    .local v15, "learnMore":Landroid/view/View;
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/view/View;->setVisibility(I)V

    .line 291
    sget-object v23, Lcom/google/android/finsky/config/G;->permissionBucketsLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual/range {v23 .. v23}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/String;

    .line 292
    .restart local v16    # "learnMoreLink":Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_6

    .line 293
    new-instance v23, Lcom/google/android/finsky/activities/AppsPermissionsActivity$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-direct {v0, v1, v2}, Lcom/google/android/finsky/activities/AppsPermissionsActivity$2;-><init>(Lcom/google/android/finsky/activities/AppsPermissionsActivity;Ljava/lang/String;)V

    move-object/from16 v0, v23

    invoke-virtual {v15, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 304
    .end local v15    # "learnMore":Landroid/view/View;
    .end local v16    # "learnMoreLink":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {v18 .. v18}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->isAppInstalled()Z

    move-result v23

    if-eqz v23, :cond_7

    if-eqz v12, :cond_7

    const v23, 0x7f0c0283

    :goto_4
    move-object/from16 v0, v20

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 308
    const v23, 0x7f0a0231

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Lcom/google/android/finsky/layout/IconButtonGroup;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mContinueButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    .line 309
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const v24, 0x7f0c0035

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 310
    .local v8, "continueString":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mContinueButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v8}, Lcom/google/android/finsky/layout/IconButtonGroup;->setText(Ljava/lang/String;)V

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mContinueButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    move-object/from16 v23, v0

    const/16 v24, 0x3

    invoke-virtual/range {v23 .. v24}, Lcom/google/android/finsky/layout/IconButtonGroup;->setBackendForLabel(I)V

    .line 312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mContinueButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/IconButtonGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mContinueButton:Lcom/google/android/finsky/layout/IconButtonGroup;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Lcom/google/android/finsky/layout/IconButtonGroup;->setEnabled(Z)V

    .line 315
    move-object/from16 v6, v18

    goto/16 :goto_3

    .line 304
    .end local v8    # "continueString":Ljava/lang/String;
    :cond_7
    const v23, 0x7f0c0281

    goto :goto_4
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 444
    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 445
    return-void
.end method

.method public finish()V
    .locals 4

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v2, 0x0

    const/16 v1, 0x25b

    invoke-virtual {v0, v2, v3, v1, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 204
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 205
    return-void
.end method

.method public flushImpression()V
    .locals 2

    .prologue
    .line 454
    const-string v0, "Not using tree impressions."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 455
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 439
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x1

    .line 335
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v1, 0x318

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 339
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->hasSeenPermissionBucketsLearnMore:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 340
    invoke-direct {p0, v3}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->finishWithResultOK(Z)V

    .line 341
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mSavedInstanceState:Landroid/os/Bundle;

    .line 146
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 147
    const v2, 0x7f0400be

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->setContentView(I)V

    .line 148
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 149
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "AppsPermissionsActivity.accountName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mAccountName:Ljava/lang/String;

    .line 150
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mAccountName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 151
    const-string v2, "AppsPermissionsActivity.showDetailedPermissions"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mShowDetailedPermissions:Z

    .line 152
    const-string v2, "AppsPermissionsActivity.docidStr"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 153
    .local v0, "docidStr":Ljava/lang/String;
    const-string v2, "AppsPermissionsActivity.doc"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/api/model/Document;

    iput-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 155
    const v2, 0x7f0a0229

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/AppSecurityPermissions;

    iput-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    .line 158
    iget-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-eqz v2, :cond_0

    .line 159
    iget-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    iget-object v3, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 162
    :cond_0
    if-nez p1, :cond_1

    .line 164
    iget-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v4, v5, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 168
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->showLoading()V

    .line 169
    new-instance v2, Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mAccountName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/finsky/api/DfeUtils;->createDetailsUrlFromId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/finsky/api/model/DfeDetails;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    .line 171
    iget-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 172
    iget-object v2, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 173
    return-void
.end method

.method public onDataChanged()V
    .locals 6

    .prologue
    .line 382
    iget-object v4, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v4}, Lcom/google/android/finsky/api/model/DfeDetails;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 383
    iget-object v4, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    if-nez v4, :cond_0

    .line 384
    const v4, 0x7f0c00e5

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->showError(Ljava/lang/String;)V

    .line 405
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-boolean v4, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mShowDetailedPermissions:Z

    if-nez v4, :cond_1

    .line 392
    iget-object v4, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v4, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 393
    .local v0, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v1

    .line 395
    .local v1, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    iget-object v4, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v5

    invoke-static {v4, v5, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z

    move-result v2

    .line 398
    .local v2, "isAvailable":Z
    if-nez v2, :cond_1

    .line 399
    iget-object v4, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v4}, Lcom/google/android/finsky/utils/DocUtils;->getAvailabilityRestrictionResourceId(Lcom/google/android/finsky/api/model/Document;)I

    move-result v3

    .line 400
    .local v3, "messageId":I
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->showError(Ljava/lang/String;)V

    goto :goto_0

    .line 404
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    .end local v2    # "isAvailable":Z
    .end local v3    # "messageId":I
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->updateFromDoc()V

    goto :goto_0
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 1
    .param p1, "volleyError"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 409
    invoke-static {p0, p1}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->showError(Ljava/lang/String;)V

    .line 410
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 428
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 421
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->setResult(I)V

    .line 422
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->finish()V

    .line 423
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->saveInstanceState(Landroid/os/Bundle;)V

    .line 179
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 184
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->addErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 188
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/finsky/activities/AppsPermissionsActivity;->mDfeDetails:Lcom/google/android/finsky/api/model/DfeDetails;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeDetails;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 196
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 197
    return-void
.end method

.method public startNewImpression()V
    .locals 2

    .prologue
    .line 449
    const-string v0, "Not using impression id\'s."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    return-void
.end method
