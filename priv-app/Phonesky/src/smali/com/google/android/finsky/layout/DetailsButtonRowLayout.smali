.class public Lcom/google/android/finsky/layout/DetailsButtonRowLayout;
.super Landroid/widget/LinearLayout;
.source "DetailsButtonRowLayout.java"


# instance fields
.field private final mChildGap:I

.field private final mIsWideLayout:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 35
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b00ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    div-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->mChildGap:I

    .line 36
    const v1, 0x7f0f0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->mIsWideLayout:Z

    .line 37
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->getWidth()I

    move-result v4

    .line 116
    .local v4, "rightX":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->getChildCount()I

    move-result v1

    .line 117
    .local v1, "childCount":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->getPaddingTop()I

    move-result v5

    .line 119
    .local v5, "y":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_1

    .line 120
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 121
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-eqz v6, :cond_0

    .line 119
    :goto_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 125
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 126
    .local v2, "childWidth":I
    sub-int v6, v4, v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v5

    invoke-virtual {v0, v6, v5, v4, v7}, Landroid/view/View;->layout(IIII)V

    .line 128
    iget v6, p0, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->mChildGap:I

    add-int/2addr v6, v2

    sub-int/2addr v4, v6

    goto :goto_1

    .line 130
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childWidth":I
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 14
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->getChildCount()I

    move-result v1

    .line 43
    .local v1, "childCount":I
    const/4 v8, 0x0

    .line 44
    .local v8, "visibleChildCount":I
    const/4 v3, 0x0

    .line 45
    .local v3, "childTotalWidth":I
    const/4 v5, 0x0

    .line 46
    .local v5, "height":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->getPaddingTop()I

    move-result v12

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->getPaddingBottom()I

    move-result v13

    add-int v7, v12, v13

    .line 48
    .local v7, "paddingVertical":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v1, :cond_1

    .line 49
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 50
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v12

    if-eqz v12, :cond_0

    .line 48
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 54
    :cond_0
    add-int/lit8 v8, v8, 0x1

    .line 55
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v0, v12, v13}, Landroid/view/View;->measure(II)V

    .line 56
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    add-int/2addr v3, v12

    .line 57
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    invoke-static {v5, v12}, Ljava/lang/Math;->max(II)I

    move-result v5

    goto :goto_1

    .line 59
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    if-lez v8, :cond_2

    .line 61
    iget v12, p0, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->mChildGap:I

    add-int/lit8 v13, v8, -0x1

    mul-int/2addr v12, v13

    add-int/2addr v3, v12

    .line 64
    :cond_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 65
    .local v10, "widthMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 66
    .local v11, "widthSize":I
    const/high16 v12, -0x80000000

    if-ne v10, v12, :cond_3

    if-gt v3, v11, :cond_3

    .line 68
    add-int v12, v5, v7

    invoke-virtual {p0, v3, v12}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->setMeasuredDimension(II)V

    .line 109
    :goto_2
    return-void

    .line 77
    :cond_3
    sub-int v4, v11, v3

    .line 78
    .local v4, "extraWidthToDistribute":I
    if-lez v4, :cond_5

    const/4 v12, 0x1

    if-eq v8, v12, :cond_4

    iget-boolean v12, p0, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->mIsWideLayout:Z

    if-eqz v12, :cond_5

    .line 81
    :cond_4
    const/4 v4, 0x0

    .line 83
    :cond_5
    if-eqz v4, :cond_7

    .line 84
    const/4 v5, 0x0

    .line 85
    const/4 v6, 0x0

    :goto_3
    if-ge v6, v1, :cond_7

    .line 86
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 87
    .restart local v0    # "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v12

    if-eqz v12, :cond_6

    .line 85
    :goto_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 91
    :cond_6
    div-int v9, v4, v8

    .line 92
    .local v9, "widthDelta":I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    add-int v2, v12, v9

    .line 93
    .local v2, "childFinalWidth":I
    const/high16 v12, 0x40000000    # 2.0f

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    const/4 v13, 0x0

    invoke-virtual {v0, v12, v13}, Landroid/view/View;->measure(II)V

    .line 102
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    invoke-static {v5, v12}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 103
    sub-int/2addr v4, v9

    .line 104
    add-int/lit8 v8, v8, -0x1

    goto :goto_4

    .line 108
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childFinalWidth":I
    .end local v9    # "widthDelta":I
    :cond_7
    add-int v12, v5, v7

    invoke-virtual {p0, v11, v12}, Lcom/google/android/finsky/layout/DetailsButtonRowLayout;->setMeasuredDimension(II)V

    goto :goto_2
.end method
