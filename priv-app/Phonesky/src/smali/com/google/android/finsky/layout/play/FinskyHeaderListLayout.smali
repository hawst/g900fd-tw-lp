.class public Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout;
.source "FinskyHeaderListLayout.java"


# instance fields
.field private mContentViewId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method


# virtual methods
.method public hideContentView()V
    .locals 2

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->mContentViewId:I

    if-lez v0, :cond_0

    .line 42
    iget v0, p0, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->mContentViewId:I

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 44
    :cond_0
    return-void
.end method

.method public setContentViewId(I)V
    .locals 0
    .param p1, "contentViewId"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->mContentViewId:I

    .line 32
    return-void
.end method

.method public showContentView()V
    .locals 2

    .prologue
    .line 35
    iget v0, p0, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->mContentViewId:I

    if-lez v0, :cond_0

    .line 36
    iget v0, p0, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->mContentViewId:I

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 38
    :cond_0
    return-void
.end method
