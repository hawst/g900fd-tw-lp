.class public Lcom/google/android/finsky/utils/GetSelfUpdateHelper;
.super Ljava/lang/Object;
.source "GetSelfUpdateHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;
    }
.end annotation


# direct methods
.method static synthetic access$000(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
    .locals 0
    .param p0, "x0"    # Z
    .param p1, "x1"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "x2"    # Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    .prologue
    .line 53
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->doGetSelfUpdate(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    return-void
.end method

.method static synthetic access$100(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
    .locals 0
    .param p0, "x0"    # Z
    .param p1, "x1"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "x2"    # Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    .prologue
    .line 53
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->doRequestToken(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    return-void
.end method

.method public static changedDeviceConfigToken(Lcom/google/android/finsky/api/DfeApi;)V
    .locals 0
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    .line 171
    invoke-interface {p0}, Lcom/google/android/finsky/api/DfeApi;->invalidateSelfUpdateCache()V

    .line 172
    return-void
.end method

.method private static doGetSelfUpdate(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
    .locals 3
    .param p0, "allowRetry"    # Z
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "listener"    # Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    .prologue
    .line 126
    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 127
    .local v0, "deviceConfigToken":Ljava/lang/String;
    new-instance v1, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$2;-><init>(ZLcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;Lcom/google/android/finsky/api/DfeApi;)V

    new-instance v2, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$3;

    invoke-direct {v2, p2}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$3;-><init>(Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->getSelfUpdate(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 164
    return-void
.end method

.method private static doRequestToken(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
    .locals 1
    .param p0, "allowRetry"    # Z
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "listener"    # Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    .prologue
    .line 97
    new-instance v0, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper$1;-><init>(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    invoke-static {p1, v0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->requestToken(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V

    .line 111
    return-void
.end method

.method public static getSelfUpdate(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V
    .locals 3
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p1, "listener"    # Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;

    .prologue
    const/4 v2, 0x1

    .line 81
    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getToken()Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "deviceConfigToken":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    invoke-static {v2, p0, p1}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->doRequestToken(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    .line 89
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-static {v2, p0, p1}, Lcom/google/android/finsky/utils/GetSelfUpdateHelper;->doGetSelfUpdate(ZLcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/GetSelfUpdateHelper$Listener;)V

    goto :goto_0
.end method
