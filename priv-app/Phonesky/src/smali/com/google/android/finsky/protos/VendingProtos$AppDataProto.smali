.class public final Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppDataProto"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;


# instance fields
.field public hasKey:Z

.field public hasValue:Z

.field public key:Ljava/lang/String;

.field public value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4892
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4893
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    .line 4894
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    .locals 2

    .prologue
    .line 4873
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    if-nez v0, :cond_1

    .line 4874
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 4876
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    if-nez v0, :cond_0

    .line 4877
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    sput-object v0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    .line 4879
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4881
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    return-object v0

    .line 4879
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4897
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->key:Ljava/lang/String;

    .line 4898
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->hasKey:Z

    .line 4899
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->value:Ljava/lang/String;

    .line 4900
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->hasValue:Z

    .line 4901
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->cachedSize:I

    .line 4902
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 4919
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 4920
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->hasKey:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->key:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4921
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->key:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4924
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->hasValue:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->value:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4925
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->value:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4928
    :cond_3
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 4936
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 4937
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 4941
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4942
    :sswitch_0
    return-object p0

    .line 4947
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->key:Ljava/lang/String;

    .line 4948
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->hasKey:Z

    goto :goto_0

    .line 4952
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->value:Ljava/lang/String;

    .line 4953
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->hasValue:Z

    goto :goto_0

    .line 4937
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4867
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4908
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->hasKey:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->key:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4909
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->key:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4911
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->hasValue:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->value:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4912
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$AppDataProto;->value:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 4914
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4915
    return-void
.end method
