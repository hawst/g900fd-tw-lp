.class public final Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
.super Lcom/google/protobuf/nano/MessageNano;
.source "AndroidAppDelivery.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/AndroidAppDelivery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AndroidAppDeliveryData"
.end annotation


# instance fields
.field public additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

.field public downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

.field public downloadSize:J

.field public downloadUrl:Ljava/lang/String;

.field public encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

.field public forwardLocked:Z

.field public gzippedDownloadSize:J

.field public gzippedDownloadUrl:Ljava/lang/String;

.field public hasDownloadSize:Z

.field public hasDownloadUrl:Z

.field public hasForwardLocked:Z

.field public hasGzippedDownloadSize:Z

.field public hasGzippedDownloadUrl:Z

.field public hasImmediateStartNeeded:Z

.field public hasInstallLocation:Z

.field public hasPostInstallRefundWindowMillis:Z

.field public hasRefundTimeout:Z

.field public hasServerInitiated:Z

.field public hasSignature:Z

.field public immediateStartNeeded:Z

.field public installLocation:I

.field public patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

.field public postInstallRefundWindowMillis:J

.field public refundTimeout:J

.field public serverInitiated:Z

.field public signature:Ljava/lang/String;

.field public splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 885
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 886
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    .line 887
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 1233
    new-instance v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 890
    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    .line 891
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasDownloadSize:Z

    .line 892
    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadSize:J

    .line 893
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasGzippedDownloadSize:Z

    .line 894
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->signature:Ljava/lang/String;

    .line 895
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasSignature:Z

    .line 896
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadUrl:Ljava/lang/String;

    .line 897
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasDownloadUrl:Z

    .line 898
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    .line 899
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasGzippedDownloadUrl:Z

    .line 900
    iput-object v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    .line 901
    invoke-static {}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;->emptyArray()[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    .line 902
    invoke-static {}, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;->emptyArray()[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    .line 903
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->forwardLocked:Z

    .line 904
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasForwardLocked:Z

    .line 905
    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->refundTimeout:J

    .line 906
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasRefundTimeout:Z

    .line 907
    iput-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->postInstallRefundWindowMillis:J

    .line 908
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasPostInstallRefundWindowMillis:Z

    .line 909
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->serverInitiated:Z

    .line 910
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasServerInitiated:Z

    .line 911
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->immediateStartNeeded:Z

    .line 912
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasImmediateStartNeeded:Z

    .line 913
    iput-object v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    .line 914
    invoke-static {}, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;->emptyArray()[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    .line 915
    iput v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->installLocation:I

    .line 916
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasInstallLocation:Z

    .line 917
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->cachedSize:I

    .line 918
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    .line 992
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 993
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasDownloadSize:Z

    if-nez v3, :cond_0

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_1

    .line 994
    :cond_0
    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    invoke-static {v8, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 997
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasSignature:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->signature:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 998
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->signature:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1001
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasDownloadUrl:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 1002
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1005
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    array-length v3, v3

    if-lez v3, :cond_7

    .line 1006
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 1007
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    aget-object v0, v3, v1

    .line 1008
    .local v0, "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    if-eqz v0, :cond_6

    .line 1009
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1006
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1014
    .end local v0    # "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    .end local v1    # "i":I
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    array-length v3, v3

    if-lez v3, :cond_9

    .line 1015
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    array-length v3, v3

    if-ge v1, v3, :cond_9

    .line 1016
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    aget-object v0, v3, v1

    .line 1017
    .local v0, "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    if-eqz v0, :cond_8

    .line 1018
    const/4 v3, 0x5

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1015
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1023
    .end local v0    # "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    .end local v1    # "i":I
    :cond_9
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasForwardLocked:Z

    if-nez v3, :cond_a

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->forwardLocked:Z

    if-eqz v3, :cond_b

    .line 1024
    :cond_a
    const/4 v3, 0x6

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->forwardLocked:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1027
    :cond_b
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasRefundTimeout:Z

    if-nez v3, :cond_c

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->refundTimeout:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_d

    .line 1028
    :cond_c
    const/4 v3, 0x7

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->refundTimeout:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1031
    :cond_d
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasServerInitiated:Z

    if-nez v3, :cond_e

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->serverInitiated:Z

    if-eq v3, v8, :cond_f

    .line 1032
    :cond_e
    const/16 v3, 0x8

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->serverInitiated:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1035
    :cond_f
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasPostInstallRefundWindowMillis:Z

    if-nez v3, :cond_10

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->postInstallRefundWindowMillis:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_11

    .line 1036
    :cond_10
    const/16 v3, 0x9

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->postInstallRefundWindowMillis:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1039
    :cond_11
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasImmediateStartNeeded:Z

    if-nez v3, :cond_12

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->immediateStartNeeded:Z

    if-eqz v3, :cond_13

    .line 1040
    :cond_12
    const/16 v3, 0xa

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->immediateStartNeeded:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1043
    :cond_13
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    if-eqz v3, :cond_14

    .line 1044
    const/16 v3, 0xb

    iget-object v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1047
    :cond_14
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    if-eqz v3, :cond_15

    .line 1048
    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1051
    :cond_15
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasGzippedDownloadUrl:Z

    if-nez v3, :cond_16

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 1052
    :cond_16
    const/16 v3, 0xd

    iget-object v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1055
    :cond_17
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasGzippedDownloadSize:Z

    if-nez v3, :cond_18

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadSize:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_19

    .line 1056
    :cond_18
    const/16 v3, 0xe

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadSize:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 1059
    :cond_19
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    if-eqz v3, :cond_1b

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    array-length v3, v3

    if-lez v3, :cond_1b

    .line 1060
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    array-length v3, v3

    if-ge v1, v3, :cond_1b

    .line 1061
    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    aget-object v0, v3, v1

    .line 1062
    .local v0, "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    if-eqz v0, :cond_1a

    .line 1063
    const/16 v3, 0xf

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1060
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1068
    .end local v0    # "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    .end local v1    # "i":I
    :cond_1b
    iget v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->installLocation:I

    if-nez v3, :cond_1c

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasInstallLocation:Z

    if-eqz v3, :cond_1d

    .line 1069
    :cond_1c
    const/16 v3, 0x10

    iget v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->installLocation:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 1072
    :cond_1d
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 1080
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1081
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1085
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1086
    :sswitch_0
    return-object p0

    .line 1091
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    .line 1092
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasDownloadSize:Z

    goto :goto_0

    .line 1096
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->signature:Ljava/lang/String;

    .line 1097
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasSignature:Z

    goto :goto_0

    .line 1101
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadUrl:Ljava/lang/String;

    .line 1102
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasDownloadUrl:Z

    goto :goto_0

    .line 1106
    :sswitch_4
    const/16 v6, 0x22

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1108
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    if-nez v6, :cond_2

    move v1, v5

    .line 1109
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    .line 1111
    .local v2, "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    if-eqz v1, :cond_1

    .line 1112
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1114
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 1115
    new-instance v6, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;-><init>()V

    aput-object v6, v2, v1

    .line 1116
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1117
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1114
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1108
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    array-length v1, v6

    goto :goto_1

    .line 1120
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    :cond_3
    new-instance v6, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;-><init>()V

    aput-object v6, v2, v1

    .line 1121
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1122
    iput-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    goto :goto_0

    .line 1126
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    :sswitch_5
    const/16 v6, 0x2a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1128
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    if-nez v6, :cond_5

    move v1, v5

    .line 1129
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    .line 1131
    .local v2, "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    if-eqz v1, :cond_4

    .line 1132
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1134
    :cond_4
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_6

    .line 1135
    new-instance v6, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;-><init>()V

    aput-object v6, v2, v1

    .line 1136
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1137
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1134
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1128
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    array-length v1, v6

    goto :goto_3

    .line 1140
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    :cond_6
    new-instance v6, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;-><init>()V

    aput-object v6, v2, v1

    .line 1141
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1142
    iput-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    goto/16 :goto_0

    .line 1146
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->forwardLocked:Z

    .line 1147
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasForwardLocked:Z

    goto/16 :goto_0

    .line 1151
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->refundTimeout:J

    .line 1152
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasRefundTimeout:Z

    goto/16 :goto_0

    .line 1156
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->serverInitiated:Z

    .line 1157
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasServerInitiated:Z

    goto/16 :goto_0

    .line 1161
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->postInstallRefundWindowMillis:J

    .line 1162
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasPostInstallRefundWindowMillis:Z

    goto/16 :goto_0

    .line 1166
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->immediateStartNeeded:Z

    .line 1167
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasImmediateStartNeeded:Z

    goto/16 :goto_0

    .line 1171
    :sswitch_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    if-nez v6, :cond_7

    .line 1172
    new-instance v6, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    .line 1174
    :cond_7
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1178
    :sswitch_c
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    if-nez v6, :cond_8

    .line 1179
    new-instance v6, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;-><init>()V

    iput-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    .line 1181
    :cond_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 1185
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    .line 1186
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasGzippedDownloadUrl:Z

    goto/16 :goto_0

    .line 1190
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadSize:J

    .line 1191
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasGzippedDownloadSize:Z

    goto/16 :goto_0

    .line 1195
    :sswitch_f
    const/16 v6, 0x7a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1197
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    if-nez v6, :cond_a

    move v1, v5

    .line 1198
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    .line 1200
    .local v2, "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    if-eqz v1, :cond_9

    .line 1201
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1203
    :cond_9
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_b

    .line 1204
    new-instance v6, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;-><init>()V

    aput-object v6, v2, v1

    .line 1205
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1206
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1203
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1197
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    :cond_a
    iget-object v6, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    array-length v1, v6

    goto :goto_5

    .line 1209
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    :cond_b
    new-instance v6, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    invoke-direct {v6}, Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;-><init>()V

    aput-object v6, v2, v1

    .line 1210
    aget-object v6, v2, v1

    invoke-virtual {p1, v6}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1211
    iput-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    goto/16 :goto_0

    .line 1215
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 1216
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 1221
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->installLocation:I

    .line 1222
    iput-boolean v8, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasInstallLocation:Z

    goto/16 :goto_0

    .line 1081
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x70 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch

    .line 1216
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 803
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 9
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    .line 924
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasDownloadSize:Z

    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    .line 925
    :cond_0
    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadSize:J

    invoke-virtual {p1, v8, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 927
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasSignature:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->signature:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 928
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->signature:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 930
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasDownloadUrl:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 931
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 933
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 934
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 935
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->additionalFile:[Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;

    aget-object v0, v2, v1

    .line 936
    .local v0, "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    if-eqz v0, :cond_6

    .line 937
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 934
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 941
    .end local v0    # "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$AppFileMetadata;
    .end local v1    # "i":I
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 942
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    array-length v2, v2

    if-ge v1, v2, :cond_9

    .line 943
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->downloadAuthCookie:[Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;

    aget-object v0, v2, v1

    .line 944
    .local v0, "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    if-eqz v0, :cond_8

    .line 945
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 942
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 949
    .end local v0    # "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$HttpCookie;
    .end local v1    # "i":I
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasForwardLocked:Z

    if-nez v2, :cond_a

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->forwardLocked:Z

    if-eqz v2, :cond_b

    .line 950
    :cond_a
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->forwardLocked:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 952
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasRefundTimeout:Z

    if-nez v2, :cond_c

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->refundTimeout:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_d

    .line 953
    :cond_c
    const/4 v2, 0x7

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->refundTimeout:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 955
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasServerInitiated:Z

    if-nez v2, :cond_e

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->serverInitiated:Z

    if-eq v2, v8, :cond_f

    .line 956
    :cond_e
    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->serverInitiated:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 958
    :cond_f
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasPostInstallRefundWindowMillis:Z

    if-nez v2, :cond_10

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->postInstallRefundWindowMillis:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_11

    .line 959
    :cond_10
    const/16 v2, 0x9

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->postInstallRefundWindowMillis:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 961
    :cond_11
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasImmediateStartNeeded:Z

    if-nez v2, :cond_12

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->immediateStartNeeded:Z

    if-eqz v2, :cond_13

    .line 962
    :cond_12
    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->immediateStartNeeded:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 964
    :cond_13
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    if-eqz v2, :cond_14

    .line 965
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->patchData:Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppPatchData;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 967
    :cond_14
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    if-eqz v2, :cond_15

    .line 968
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->encryptionParams:Lcom/google/android/finsky/protos/AndroidAppDelivery$EncryptionParams;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 970
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasGzippedDownloadUrl:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 971
    :cond_16
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 973
    :cond_17
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasGzippedDownloadSize:Z

    if-nez v2, :cond_18

    iget-wide v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadSize:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_19

    .line 974
    :cond_18
    const/16 v2, 0xe

    iget-wide v4, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->gzippedDownloadSize:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 976
    :cond_19
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    array-length v2, v2

    if-lez v2, :cond_1b

    .line 977
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    array-length v2, v2

    if-ge v1, v2, :cond_1b

    .line 978
    iget-object v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->splitDeliveryData:[Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;

    aget-object v0, v2, v1

    .line 979
    .local v0, "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    if-eqz v0, :cond_1a

    .line 980
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 977
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 984
    .end local v0    # "element":Lcom/google/android/finsky/protos/AndroidAppDelivery$SplitDeliveryData;
    .end local v1    # "i":I
    :cond_1b
    iget v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->installLocation:I

    if-nez v2, :cond_1c

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->hasInstallLocation:Z

    if-eqz v2, :cond_1d

    .line 985
    :cond_1c
    const/16 v2, 0x10

    iget v3, p0, Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;->installLocation:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 987
    :cond_1d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 988
    return-void
.end method
