.class public final Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BuyInstruments.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BuyInstruments;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InstrumentSetupInfoResponse"
.end annotation


# instance fields
.field public checkoutTokenRequired:Z

.field public hasCheckoutTokenRequired:Z

.field public setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 865
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 866
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->clear()Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    .line 867
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 870
    invoke-static {}, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;->emptyArray()[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    .line 871
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->checkoutTokenRequired:Z

    .line 872
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->hasCheckoutTokenRequired:Z

    .line 873
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->cachedSize:I

    .line 874
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 896
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 897
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 898
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 899
    iget-object v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    aget-object v0, v3, v1

    .line 900
    .local v0, "element":Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    if-eqz v0, :cond_0

    .line 901
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 898
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 906
    .end local v0    # "element":Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->hasCheckoutTokenRequired:Z

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->checkoutTokenRequired:Z

    if-eqz v3, :cond_3

    .line 907
    :cond_2
    const/4 v3, 0x2

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->checkoutTokenRequired:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 910
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 918
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 919
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 923
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 924
    :sswitch_0
    return-object p0

    .line 929
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 931
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    if-nez v5, :cond_2

    move v1, v4

    .line 932
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    .line 934
    .local v2, "newArray":[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    if-eqz v1, :cond_1

    .line 935
    iget-object v5, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 937
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 938
    new-instance v5, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;-><init>()V

    aput-object v5, v2, v1

    .line 939
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 940
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 937
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 931
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    array-length v1, v5

    goto :goto_1

    .line 943
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;-><init>()V

    aput-object v5, v2, v1

    .line 944
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 945
    iput-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    goto :goto_0

    .line 949
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->checkoutTokenRequired:Z

    .line 950
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->hasCheckoutTokenRequired:Z

    goto :goto_0

    .line 919
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 841
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 880
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 881
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 882
    iget-object v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->setupInfo:[Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;

    aget-object v0, v2, v1

    .line 883
    .local v0, "element":Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    if-eqz v0, :cond_0

    .line 884
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 881
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 888
    .end local v0    # "element":Lcom/google/android/finsky/protos/InstrumentSetupInfoProto$InstrumentSetupInfo;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->hasCheckoutTokenRequired:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->checkoutTokenRequired:Z

    if-eqz v2, :cond_3

    .line 889
    :cond_2
    const/4 v2, 0x2

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/BuyInstruments$InstrumentSetupInfoResponse;->checkoutTokenRequired:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 891
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 892
    return-void
.end method
