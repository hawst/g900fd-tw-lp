.class Lcom/google/android/finsky/services/VpaService$2;
.super Landroid/os/AsyncTask;
.source "VpaService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/services/VpaService;->handleStartVpaCommand()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/services/VpaService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/services/VpaService;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/google/android/finsky/services/VpaService$2;->this$0:Lcom/google/android/finsky/services/VpaService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 154
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/services/VpaService$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/String;
    .locals 13
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    const/4 v10, 0x2

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 158
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 159
    .local v1, "intent":Landroid/content/Intent;
    const-string v9, "android.autoinstalls.config.action.PLAY_AUTO_INSTALL"

    invoke-virtual {v1, v9}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v9, v1, v11}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 162
    .local v6, "receivers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_3

    .line 165
    invoke-interface {v6, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 166
    .local v0, "info":Landroid/content/pm/ResolveInfo;
    iget-object v9, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 168
    .local v4, "packageName":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v9

    invoke-interface {v9, v4}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v5

    .line 170
    .local v5, "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    if-nez v5, :cond_0

    .line 171
    const-string v9, "Null PackageState for potential VPA stub %s"

    new-array v10, v12, [Ljava/lang/Object;

    aput-object v4, v10, v11

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v4, v8

    .line 184
    .end local v0    # "info":Landroid/content/pm/ResolveInfo;
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v5    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :goto_0
    return-object v4

    .line 174
    .restart local v0    # "info":Landroid/content/pm/ResolveInfo;
    .restart local v4    # "packageName":Ljava/lang/String;
    .restart local v5    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    :cond_0
    iget v7, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->installedVersion:I

    .line 175
    .local v7, "versionCode":I
    iget-boolean v2, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isSystemApp:Z

    .line 176
    .local v2, "isSystem":Z
    iget-boolean v3, v5, Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;->isUpdatedSystemApp:Z

    .line 177
    .local v3, "isUpdatedSystem":Z
    if-eq v7, v12, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    if-nez v2, :cond_4

    .line 182
    :cond_2
    const-string v9, "Rejected VPA stub %s:%d"

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v4, v10, v11

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .end local v0    # "info":Landroid/content/pm/ResolveInfo;
    .end local v2    # "isSystem":Z
    .end local v3    # "isUpdatedSystem":Z
    .end local v4    # "packageName":Ljava/lang/String;
    .end local v5    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    .end local v7    # "versionCode":I
    :cond_3
    move-object v4, v8

    .line 184
    goto :goto_0

    .line 179
    .restart local v0    # "info":Landroid/content/pm/ResolveInfo;
    .restart local v2    # "isSystem":Z
    .restart local v3    # "isUpdatedSystem":Z
    .restart local v4    # "packageName":Ljava/lang/String;
    .restart local v5    # "packageState":Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;
    .restart local v7    # "versionCode":I
    :cond_4
    const-string v8, "Found VPA stub %s:%d"

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v4, v9, v11

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 154
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/services/VpaService$2;->onPostExecute(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/String;)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 189
    if-nez p1, :cond_0

    .line 190
    const-string v0, "No VPA stub found - stopping service"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/finsky/services/VpaService$2;->this$0:Lcom/google/android/finsky/services/VpaService;

    # setter for: Lcom/google/android/finsky/services/VpaService;->mVpaRunning:Z
    invoke-static {v0, v2}, Lcom/google/android/finsky/services/VpaService;->access$302(Lcom/google/android/finsky/services/VpaService;Z)Z

    .line 192
    iget-object v0, p0, Lcom/google/android/finsky/services/VpaService$2;->this$0:Lcom/google/android/finsky/services/VpaService;

    # invokes: Lcom/google/android/finsky/services/VpaService;->stopServiceIfDone()V
    invoke-static {v0}, Lcom/google/android/finsky/services/VpaService;->access$200(Lcom/google/android/finsky/services/VpaService;)V

    .line 196
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/services/VpaService$2;->this$0:Lcom/google/android/finsky/services/VpaService;

    # invokes: Lcom/google/android/finsky/services/VpaService;->vpaGetDeviceConfig(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/google/android/finsky/services/VpaService;->access$400(Lcom/google/android/finsky/services/VpaService;Ljava/lang/String;)V

    goto :goto_0
.end method
