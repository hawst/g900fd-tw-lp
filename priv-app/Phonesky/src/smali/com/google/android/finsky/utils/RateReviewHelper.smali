.class public Lcom/google/android/finsky/utils/RateReviewHelper;
.super Ljava/lang/Object;
.source "RateReviewHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/RateReviewHelper$CheckAndConfirmGPlusListener;,
        Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;
    }
.end annotation


# static fields
.field private static final DESCRIPTION_MAP:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/finsky/utils/RateReviewHelper;->DESCRIPTION_MAP:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0c026b
        0x7f0c026c
        0x7f0c026d
        0x7f0c026e
        0x7f0c026f
        0x7f0c0270
    .end array-data
.end method

.method static synthetic access$000(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-static {p0, p1}, Lcom/google/android/finsky/utils/RateReviewHelper;->showReviewError(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-static {p0}, Lcom/google/android/finsky/utils/RateReviewHelper;->showReviewDeleteError(Landroid/content/Context;)V

    return-void
.end method

.method public static checkAndConfirmGPlus(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/utils/RateReviewHelper$CheckAndConfirmGPlusListener;)V
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "listener"    # Lcom/google/android/finsky/utils/RateReviewHelper$CheckAndConfirmGPlusListener;

    .prologue
    .line 76
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getPlayDfeApi()Lcom/google/android/play/dfe/api/PlayDfeApi;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/utils/RateReviewHelper$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/utils/RateReviewHelper$1;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/utils/RateReviewHelper$CheckAndConfirmGPlusListener;)V

    new-instance v2, Lcom/google/android/finsky/utils/RateReviewHelper$2;

    invoke-direct {v2, p0, p1}, Lcom/google/android/finsky/utils/RateReviewHelper$2;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/utils/RateReviewHelper$CheckAndConfirmGPlusListener;)V

    const/4 v3, 0x1

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/play/dfe/api/PlayDfeApi;->getPlusProfile(Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;Z)Lcom/android/volley/Request;

    .line 112
    return-void
.end method

.method public static deleteReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "docDetailsUrl"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "listener"    # Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    .prologue
    .line 189
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v0

    .line 191
    .local v0, "clientMutationCache":Lcom/google/android/finsky/utils/ClientMutationCache;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/utils/ClientMutationCache;->updateCachedReviewDeleted(Ljava/lang/String;)V

    .line 193
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    .line 196
    .local v1, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    new-instance v2, Lcom/google/android/finsky/utils/RateReviewHelper$5;

    invoke-direct {v2, v1, p2, p4}, Lcom/google/android/finsky/utils/RateReviewHelper$5;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V

    new-instance v3, Lcom/google/android/finsky/utils/RateReviewHelper$6;

    invoke-direct {v3, v0, p1, p3, p4}, Lcom/google/android/finsky/utils/RateReviewHelper$6;-><init>(Lcom/google/android/finsky/utils/ClientMutationCache;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V

    invoke-interface {v1, p1, v2, v3}, Lcom/google/android/finsky/api/DfeApi;->deleteReview(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 224
    return-void
.end method

.method public static getRatingDescription(I)I
    .locals 1
    .param p0, "rating"    # I

    .prologue
    .line 286
    sget-object v0, Lcom/google/android/finsky/utils/RateReviewHelper;->DESCRIPTION_MAP:[I

    aget v0, v0, p0

    return v0
.end method

.method public static rateDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;I)V
    .locals 8
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "docDetailsUrl"    # Ljava/lang/String;
    .param p3, "rating"    # I
    .param p4, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p5, "rateListener"    # Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;
    .param p6, "reviewSource"    # I

    .prologue
    .line 240
    new-instance v0, Lcom/google/android/finsky/utils/RateReviewHelper$7;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p5

    move-object v5, p2

    move-object v6, p4

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/finsky/utils/RateReviewHelper$7;-><init>(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;Ljava/lang/String;Landroid/support/v4/app/FragmentActivity;I)V

    invoke-static {p4, v0}, Lcom/google/android/finsky/utils/RateReviewHelper;->checkAndConfirmGPlus(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/finsky/utils/RateReviewHelper$CheckAndConfirmGPlusListener;)V

    .line 269
    return-void
.end method

.method private static showReviewDeleteError(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 280
    if-eqz p0, :cond_0

    .line 281
    const v0, 0x7f0c0267

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 283
    :cond_0
    return-void
.end method

.method private static showReviewError(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "serverMessage"    # Ljava/lang/String;

    .prologue
    .line 272
    if-eqz p0, :cond_0

    .line 273
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c024e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 275
    .local v0, "text":Ljava/lang/CharSequence;
    :goto_0
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 277
    .end local v0    # "text":Ljava/lang/CharSequence;
    :cond_0
    return-void

    :cond_1
    move-object v0, p1

    .line 273
    goto :goto_0
.end method

.method public static updateReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;I)V
    .locals 18
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "docId"    # Ljava/lang/String;
    .param p2, "docDetailsUrl"    # Ljava/lang/String;
    .param p3, "rating"    # I
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "content"    # Ljava/lang/String;
    .param p6, "author"    # Lcom/google/android/finsky/api/model/Document;
    .param p7, "context"    # Landroid/content/Context;
    .param p8, "rateListener"    # Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;
    .param p9, "reviewSource"    # I

    .prologue
    .line 123
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v10

    .line 124
    .local v10, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/FinskyApp;->getClientMutationCache(Ljava/lang/String;)Lcom/google/android/finsky/utils/ClientMutationCache;

    move-result-object v4

    .line 129
    .local v4, "clientMutationCache":Lcom/google/android/finsky/utils/ClientMutationCache;
    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 130
    move-object/from16 p5, p4

    .line 131
    const-string p4, ""

    .line 134
    :cond_0
    move-object/from16 v7, p4

    .line 135
    .local v7, "finalTitle":Ljava/lang/String;
    move-object/from16 v8, p5

    .local v8, "finalContent":Ljava/lang/String;
    move-object/from16 v5, p1

    move/from16 v6, p3

    move-object/from16 v9, p6

    .line 138
    invoke-virtual/range {v4 .. v9}, Lcom/google/android/finsky/utils/ClientMutationCache;->updateCachedReview(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;)V

    .line 145
    const/4 v5, 0x1

    new-instance v9, Lcom/google/android/finsky/utils/RateReviewHelper$3;

    move-object/from16 v11, p2

    move-object/from16 v12, p8

    move/from16 v13, p3

    move-object v14, v7

    move-object v15, v8

    invoke-direct/range {v9 .. v15}, Lcom/google/android/finsky/utils/RateReviewHelper$3;-><init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;ILjava/lang/String;Ljava/lang/String;)V

    new-instance v17, Lcom/google/android/finsky/utils/RateReviewHelper$4;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, p7

    move-object/from16 v3, p8

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/finsky/utils/RateReviewHelper$4;-><init>(Lcom/google/android/finsky/utils/ClientMutationCache;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V

    move-object/from16 v11, p1

    move-object v12, v7

    move-object v13, v8

    move/from16 v14, p3

    move v15, v5

    move-object/from16 v16, v9

    invoke-interface/range {v10 .. v17}, Lcom/google/android/finsky/api/DfeApi;->addReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 177
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v5, v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v6

    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static/range {p5 .. p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    :cond_1
    const/4 v5, 0x1

    :goto_0
    move/from16 v0, p9

    move/from16 v1, p3

    invoke-virtual {v6, v0, v1, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logReviewAdded(IIZ)V

    .line 179
    return-void

    .line 177
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method
