.class public Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;
.super Lcom/google/android/play/layout/ForegroundRelativeLayout;
.source "PlayCardClusterViewHeader.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/Identifiable;


# instance fields
.field protected mHeaderImage:Lcom/google/android/play/image/FifeImageView;

.field private final mHeaderXPadding:I

.field private mIdentifier:Ljava/lang/String;

.field private final mMinHeight:I

.field protected mMoreView:Landroid/widget/TextView;

.field protected mTitleGroup:Landroid/view/View;

.field protected mTitleMain:Landroid/widget/TextView;

.field private mTitleSecondary:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/layout/ForegroundRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    sget-object v1, Lcom/android/vending/R$styleable;->PlayCardClusterViewHeader:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMinHeight:I

    .line 55
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderXPadding:I

    .line 57
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderXPadding:I

    iget v2, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderXPadding:I

    invoke-virtual {p0, v1, v3, v2, v3}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setPadding(IIII)V

    .line 60
    return-void
.end method


# virtual methods
.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mIdentifier:Ljava/lang/String;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/android/play/layout/ForegroundRelativeLayout;->onFinishInflate()V

    .line 66
    const v0, 0x7f0a0140

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    .line 67
    const v0, 0x7f0a0142

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    const v1, 0x7f0a0143

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    const v1, 0x7f0a0144

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0a0145

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    .line 71
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 15
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getWidth()I

    move-result v10

    .line 183
    .local v10, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getHeight()I

    move-result v0

    .line 185
    .local v0, "height":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingTop()I

    move-result v8

    .line 187
    .local v8, "paddingTop":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingLeft()I

    move-result v11

    .line 188
    .local v11, "x":I
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v12}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_0

    .line 189
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v12}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v4

    .line 190
    .local v4, "imageWidth":I
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v12}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v1

    .line 191
    .local v1, "imageHeight":I
    sub-int v12, v0, v1

    sub-int/2addr v12, v8

    div-int/lit8 v12, v12, 0x2

    add-int v3, v8, v12

    .line 192
    .local v3, "imageTop":I
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    add-int v13, v11, v4

    add-int v14, v3, v1

    invoke-virtual {v12, v11, v3, v13, v14}, Lcom/google/android/play/image/FifeImageView;->layout(IIII)V

    .line 193
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v12}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 195
    .local v2, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v12, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v12, v4

    add-int/2addr v11, v12

    .line 199
    .end local v1    # "imageHeight":I
    .end local v2    # "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v3    # "imageTop":I
    .end local v4    # "imageWidth":I
    :cond_0
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    iget-object v13, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    add-int/2addr v13, v11

    invoke-virtual {v12, v11, v8, v13, v0}, Landroid/view/View;->layout(IIII)V

    .line 202
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_1

    .line 203
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingRight()I

    move-result v12

    sub-int v9, v10, v12

    .line 205
    .local v9, "rightX":I
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    .line 206
    .local v7, "moreWidth":I
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v12}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    .line 207
    .local v5, "moreHeight":I
    sub-int v12, v0, v5

    sub-int/2addr v12, v8

    div-int/lit8 v12, v12, 0x2

    add-int v6, v8, v12

    .line 208
    .local v6, "moreTop":I
    iget-object v12, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    sub-int v13, v9, v7

    add-int v14, v6, v5

    invoke-virtual {v12, v13, v6, v9, v14}, Landroid/widget/TextView;->layout(IIII)V

    .line 210
    .end local v5    # "moreHeight":I
    .end local v6    # "moreTop":I
    .end local v7    # "moreWidth":I
    .end local v9    # "rightX":I
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v9, 0x8

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 148
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 150
    .local v3, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingLeft()I

    move-result v4

    sub-int v4, v3, v4

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingRight()I

    move-result v5

    sub-int v2, v4, v5

    .line 152
    .local v2, "titleGroupWidth":I
    const/4 v1, 0x0

    .line 155
    .local v1, "maxContentHeight":I
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->getVisibility()I

    move-result v4

    if-eq v4, v9, :cond_0

    .line 156
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 158
    .local v0, "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/play/image/FifeImageView;->measure(II)V

    .line 161
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->getMeasuredHeight()I

    move-result v1

    .line 162
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v4}, Lcom/google/android/play/image/FifeImageView;->getMeasuredWidth()I

    move-result v4

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v4, v5

    sub-int/2addr v2, v4

    .line 166
    .end local v0    # "imageLp":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_0
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getVisibility()I

    move-result v4

    if-eq v4, v9, :cond_1

    .line 167
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v4, v7, v7}, Landroid/widget/TextView;->measure(II)V

    .line 168
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 169
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v2, v4

    .line 173
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v5, v7}, Landroid/view/View;->measure(II)V

    .line 175
    iget-object v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleGroup:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 177
    iget v4, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMinHeight:I

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setMeasuredDimension(II)V

    .line 178
    return-void
.end method

.method public replaceTitles(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 4
    .param p1, "titleMainNew"    # Ljava/lang/CharSequence;
    .param p2, "titleSecondaryNew"    # Ljava/lang/CharSequence;

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 129
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 134
    return-void

    :cond_0
    move v0, v2

    .line 130
    goto :goto_0

    :cond_1
    move v1, v2

    .line 132
    goto :goto_1
.end method

.method public setContent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 8
    .param p1, "backendId"    # I
    .param p2, "titleMain"    # Ljava/lang/String;
    .param p3, "titleSecondary"    # Ljava/lang/String;
    .param p4, "more"    # Ljava/lang/String;
    .param p5, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v1, 0x0

    .line 75
    move-object v0, p0

    move v2, p1

    move-object v3, v1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setContent(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 76
    return-void
.end method

.method public setContent(Lcom/google/android/play/image/BitmapLoader;ILcom/google/android/finsky/protos/Common$Image;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 8
    .param p1, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p2, "backendId"    # I
    .param p3, "image"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p4, "titleMain"    # Ljava/lang/String;
    .param p5, "titleSecondary"    # Ljava/lang/String;
    .param p6, "more"    # Ljava/lang/String;
    .param p7, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 80
    if-eqz p3, :cond_0

    .line 81
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    iget-object v6, p3, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v7, p3, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-virtual {v5, v6, v7, p1}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 82
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 87
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleMain:Landroid/widget/TextView;

    invoke-virtual {v5, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 90
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    :goto_1
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 97
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    :goto_2
    invoke-virtual {p0, p7}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    if-eqz p7, :cond_3

    const/4 v5, 0x1

    :goto_3
    invoke-virtual {p0, v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setClickable(Z)V

    .line 118
    return-void

    .line 84
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderImage:Lcom/google/android/play/image/FifeImageView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto :goto_0

    .line 92
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    invoke-static {p5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mTitleSecondary:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 99
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {p6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v2

    .line 104
    .local v2, "morePaddingLeft":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    .line 105
    .local v3, "morePaddingRight":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v4

    .line 106
    .local v4, "morePaddingTop":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v1

    .line 107
    .local v1, "morePaddingBottom":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, p2}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPlayActionButtonBaseBackgroundDrawable(Landroid/content/Context;I)I

    move-result v0

    .line 109
    .local v0, "moreDrawable":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 112
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mMoreView:Landroid/widget/TextView;

    invoke-virtual {v5, v2, v4, v3, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_2

    .line 117
    .end local v0    # "moreDrawable":I
    .end local v1    # "morePaddingBottom":I
    .end local v2    # "morePaddingLeft":I
    .end local v3    # "morePaddingRight":I
    .end local v4    # "morePaddingTop":I
    :cond_3
    const/4 v5, 0x0

    goto :goto_3
.end method

.method public setExtraHorizontalPadding(I)V
    .locals 3
    .param p1, "extraHorizontalPadding"    # I

    .prologue
    const/4 v2, 0x0

    .line 121
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderXPadding:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mHeaderXPadding:I

    add-int/2addr v1, p1

    invoke-virtual {p0, v0, v2, v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->setPadding(IIII)V

    .line 123
    return-void
.end method

.method public setIdentifier(Ljava/lang/String;)V
    .locals 0
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewHeader;->mIdentifier:Ljava/lang/String;

    .line 139
    return-void
.end method
