.class public Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;
.super Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;
.source "PeopleDetailsStreamAdapter.java"


# instance fields
.field private final mIsShowingOwnProfile:Z

.field private mIsShowingWarmWelcome:Z

.field private final mPlusDoc:Lcom/google/android/finsky/api/model/Document;

.field private final mWarmWelcomeCardColumns:I

.field private final mWarmWelcomeHideGraphic:Z


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/Document;Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/DfeList;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 18
    .param p1, "plusDoc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p6, "toc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p7, "clientMutationCache"    # Lcom/google/android/finsky/utils/ClientMutationCache;
    .param p8, "dfeList"    # Lcom/google/android/finsky/api/model/DfeList;
    .param p9, "isRestoring"    # Z
    .param p10, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 59
    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/api/model/Document;->getCoreContentHeader()Ljava/lang/String;

    move-result-object v11

    const/4 v13, 0x1

    const/4 v14, 0x2

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v12, p9

    move-object/from16 v15, p10

    invoke-direct/range {v2 .. v15}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/utils/ClientMutationCache;Lcom/google/android/finsky/api/model/ContainerList;[Lcom/google/android/finsky/adapters/QuickLinkHelper$QuickLinkInfo;Ljava/lang/String;ZZILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 62
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    .line 64
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getPersonDetails()Lcom/google/android/finsky/protos/DocDetails$PersonDetails;

    move-result-object v16

    .line 65
    .local v16, "personDetails":Lcom/google/android/finsky/protos/DocDetails$PersonDetails;
    if-eqz v16, :cond_0

    move-object/from16 v0, v16

    iget-boolean v2, v0, Lcom/google/android/finsky/protos/DocDetails$PersonDetails;->personIsRequester:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mIsShowingOwnProfile:Z

    .line 67
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    .line 68
    .local v17, "res":Landroid/content/res/Resources;
    const v2, 0x7f0e0011

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mWarmWelcomeCardColumns:I

    .line 70
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mWarmWelcomeCardColumns:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const v2, 0x7f0f000e

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mWarmWelcomeHideGraphic:Z

    .line 72
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mIsShowingOwnProfile:Z

    if-eqz v2, :cond_2

    sget-object v2, Lcom/google/android/finsky/utils/FinskyPreferences;->warmWelcomeOwnProfileAcknowledged:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v2}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mIsShowingWarmWelcome:Z

    .line 74
    return-void

    .line 65
    .end local v17    # "res":Landroid/content/res/Resources;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 70
    .restart local v17    # "res":Landroid/content/res/Resources;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 72
    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method static synthetic access$002(Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mIsShowingWarmWelcome:Z

    return p1
.end method

.method private bindEmptyStateView(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 251
    move-object v1, p1

    check-cast v1, Lcom/google/android/finsky/layout/IdentifiableTextView;

    .line 252
    .local v1, "result":Lcom/google/android/finsky/layout/IdentifiableTextView;
    iget-boolean v2, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mIsShowingOwnProfile:Z

    if-eqz v2, :cond_0

    const v2, 0x7f0c03bc

    :goto_0
    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/IdentifiableTextView;->setText(I)V

    .line 255
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0169

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget v3, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mCardContentPadding:I

    add-int v0, v2, v3

    .line 257
    .local v0, "padding":I
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v3

    invoke-virtual {v1, v0, v2, v0, v3}, Lcom/google/android/finsky/layout/IdentifiableTextView;->setPadding(IIII)V

    .line 259
    const-string v2, "empty_state"

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/IdentifiableTextView;->setIdentifier(Ljava/lang/String;)V

    .line 260
    return-void

    .line 252
    .end local v0    # "padding":I
    :cond_0
    const v2, 0x7f0c03bd

    goto :goto_0
.end method

.method private bindProfileInfoView(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 196
    move-object v0, p1

    check-cast v0, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;

    .line 197
    .local v0, "profileInfoView":Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;
    const-string v1, "profile_info"

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->setIdentifier(Ljava/lang/String;)V

    .line 198
    iget-object v1, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mPlusDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v2, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iget v3, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/layout/play/PeopleDetailsProfileInfoView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;I)V

    .line 199
    return-void
.end method

.method private bindWarmWelcomeCardView(Landroid/view/View;I)V
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x4

    const/4 v11, 0x0

    const/4 v5, 0x1

    .line 208
    move-object v0, p1

    check-cast v0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;

    .line 209
    .local v0, "warmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0c03c1

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 210
    .local v1, "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0c03c2

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 211
    .local v2, "body":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f0c03c3

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 213
    .local v7, "buttonText":Ljava/lang/String;
    new-instance v10, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v10}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    .line 214
    .local v10, "warmWelcomeGraphic":Lcom/google/android/finsky/protos/Common$Image;
    iput v12, v10, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    .line 215
    iput-boolean v5, v10, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    .line 216
    sget-object v3, Lcom/google/android/finsky/config/G;->peoplePageWarmWelcomeGraphicUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v10, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    .line 217
    iput-boolean v5, v10, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    .line 219
    new-instance v9, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v9}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    .line 220
    .local v9, "warmWelcomeButtonIcon":Lcom/google/android/finsky/protos/Common$Image;
    iput v12, v9, Lcom/google/android/finsky/protos/Common$Image;->imageType:I

    .line 221
    iput-boolean v5, v9, Lcom/google/android/finsky/protos/Common$Image;->hasImageType:Z

    .line 222
    sget-object v3, Lcom/google/android/finsky/config/G;->peoplePageWarmWelcomeButtonIconUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v9, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    .line 223
    iput-boolean v5, v9, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    .line 225
    iget-boolean v3, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mWarmWelcomeHideGraphic:Z

    if-eqz v3, :cond_0

    move-object v3, v6

    :goto_0
    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->configureContent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Common$Image;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;[B)V

    .line 228
    move-object v8, v0

    .line 229
    .local v8, "finalWarmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    new-instance v3, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;

    invoke-direct {v3, p0, v8, p2}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter$1;-><init>(Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;Lcom/google/android/finsky/layout/play/WarmWelcomeCard;I)V

    invoke-virtual {v0, v7, v9, v3}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->configureDismissAction(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V

    .line 244
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->hideSecondaryAction()V

    .line 246
    iget v3, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mCardContentPadding:I

    iget v4, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mCardContentPadding:I

    invoke-virtual {v0, v3, v11, v4, v11}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->setPadding(IIII)V

    .line 247
    const-string v3, "self_warm_welcome"

    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->setIdentifier(Ljava/lang/String;)V

    .line 248
    return-void

    .end local v8    # "finalWarmWelcomeCard":Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
    :cond_0
    move-object v3, v10

    .line 225
    goto :goto_0
.end method

.method private getThisAdapterPrependedRowsCount()I
    .locals 2

    .prologue
    .line 118
    const/4 v0, 0x0

    .line 120
    .local v0, "numPrependedRows":I
    add-int/lit8 v0, v0, 0x1

    .line 121
    iget-boolean v1, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mIsShowingWarmWelcome:Z

    if-eqz v1, :cond_0

    .line 122
    add-int/lit8 v0, v0, 0x1

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 126
    add-int/lit8 v0, v0, 0x1

    .line 128
    :cond_1
    return v0
.end method


# virtual methods
.method public getDataRowsCount()I
    .locals 2

    .prologue
    .line 106
    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getDataRowsCount()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->getThisAdapterPrependedRowsCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 174
    if-nez p1, :cond_0

    .line 175
    const/16 v0, 0x18

    .line 192
    :goto_0
    return v0

    .line 177
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 179
    iget-boolean v0, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mIsShowingWarmWelcome:Z

    if-eqz v0, :cond_2

    .line 180
    if-nez p1, :cond_1

    .line 181
    const/16 v0, 0x1a

    goto :goto_0

    .line 183
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 189
    const/16 v0, 0x19

    goto :goto_0

    .line 192
    :cond_3
    invoke-super {p0, p1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method protected getPlayCardDismissListener()Lcom/google/android/finsky/layout/play/PlayCardDismissListener;
    .locals 1

    .prologue
    .line 271
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPrependedRowsCount()I
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->getPrependedRowsCount()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->getThisAdapterPrependedRowsCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method protected hasExtraLeadingSpacer()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method protected hasLeadingSpacer()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method protected isDismissed(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 1
    .param p1, "document"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 265
    const/4 v0, 0x0

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 2
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 155
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    .line 156
    .local v0, "itemViewType":I
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 157
    .local v1, "view":Landroid/view/View;
    packed-switch v0, :pswitch_data_0

    .line 168
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    .line 170
    :goto_0
    return-void

    .line 159
    :pswitch_0
    invoke-direct {p0, v1}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->bindProfileInfoView(Landroid/view/View;)V

    goto :goto_0

    .line 162
    :pswitch_1
    invoke-direct {p0, v1, p2}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->bindWarmWelcomeCardView(Landroid/view/View;I)V

    goto :goto_0

    .line 165
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->bindEmptyStateView(Landroid/view/View;)V

    goto :goto_0

    .line 157
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v4, 0x0

    .line 134
    packed-switch p2, :pswitch_data_0

    .line 148
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v2

    .line 150
    :goto_0
    return-object v2

    .line 136
    :pswitch_0
    const v2, 0x7f04010d

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 150
    .local v1, "view":Landroid/view/View;
    :goto_1
    new-instance v2, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ViewHolder;

    invoke-direct {v2, v1}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 139
    .end local v1    # "view":Landroid/view/View;
    :pswitch_1
    iget v2, p0, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->mWarmWelcomeCardColumns:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const v0, 0x7f0401d2

    .line 142
    .local v0, "layoutId":I
    :goto_2
    invoke-virtual {p0, v0, p1, v4}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 143
    .restart local v1    # "view":Landroid/view/View;
    goto :goto_1

    .line 139
    .end local v0    # "layoutId":I
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    const v0, 0x7f0401d1

    goto :goto_2

    .line 145
    :pswitch_2
    const v2, 0x7f040163

    invoke-virtual {p0, v2, p1, v4}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 146
    .restart local v1    # "view":Landroid/view/View;
    goto :goto_1

    .line 134
    nop

    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected shouldHidePlainHeaderDuringInitialLoading()Z
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    return v0
.end method

.method protected shouldHidePlainHeaderOnEmpty()Z
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    return v0
.end method
