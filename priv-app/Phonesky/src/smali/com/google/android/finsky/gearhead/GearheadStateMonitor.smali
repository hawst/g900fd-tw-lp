.class public Lcom/google/android/finsky/gearhead/GearheadStateMonitor;
.super Ljava/lang/Object;
.source "GearheadStateMonitor.java"


# static fields
.field private static final GEARHEAD_SUPPORTED:Z

.field private static sApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

.field private static sConnectionLatch:Ljava/util/concurrent/CountDownLatch;

.field private static volatile sHasInitialized:Z

.field private static volatile sIsProjecting:Z

.field private static sOnReadyRunnables:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 29
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->GEARHEAD_SUPPORTED:Z

    .line 33
    sput-boolean v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sIsProjecting:Z

    .line 35
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sOnReadyRunnables:Ljava/util/LinkedList;

    .line 41
    sput-boolean v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sHasInitialized:Z

    .line 47
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sConnectionLatch:Ljava/util/concurrent/CountDownLatch;

    .line 49
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    return-void

    :cond_0
    move v0, v2

    .line 29
    goto :goto_0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 25
    sput-boolean p0, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sIsProjecting:Z

    return p0
.end method

.method static synthetic access$100()V
    .locals 0

    .prologue
    .line 25
    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->onReady()V

    return-void
.end method

.method static synthetic access$200()Lcom/google/android/gms/common/api/GoogleApiClient;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    return-object v0
.end method

.method public static declared-synchronized initialize(Ljava/lang/Runnable;)V
    .locals 8
    .param p0, "onReadyRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 57
    const-class v3, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;

    monitor-enter v3

    :try_start_0
    sget-boolean v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->GEARHEAD_SUPPORTED:Z

    if-nez v2, :cond_1

    .line 59
    if-eqz p0, :cond_0

    .line 60
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :cond_0
    :goto_0
    monitor-exit v3

    return-void

    .line 64
    :cond_1
    :try_start_1
    sget-object v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sConnectionLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_2

    .line 66
    if-eqz p0, :cond_0

    .line 67
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 71
    :cond_2
    if-eqz p0, :cond_3

    .line 73
    :try_start_2
    sget-object v4, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sOnReadyRunnables:Ljava/util/LinkedList;

    monitor-enter v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 74
    :try_start_3
    sget-object v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sOnReadyRunnables:Ljava/util/LinkedList;

    invoke-virtual {v2, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 75
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 78
    :cond_3
    :try_start_4
    sget-boolean v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sHasInitialized:Z

    if-nez v2, :cond_0

    .line 79
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 80
    .local v0, "connectionChangeFilter":Landroid/content/IntentFilter;
    const-string v2, "com.google.android.gms.car.CONNECTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 81
    const-string v2, "com.google.android.gms.car.DISCONNECTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 83
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    .line 85
    .local v1, "context":Landroid/content/Context;
    new-instance v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor$1;

    invoke-direct {v2}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor$1;-><init>()V

    const-string v4, "com.google.android.gms.permission.CAR"

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v0, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 98
    new-instance v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor$2;

    invoke-direct {v2}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor$2;-><init>()V

    new-instance v4, Lcom/google/android/finsky/gearhead/GearheadStateMonitor$3;

    invoke-direct {v4}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor$3;-><init>()V

    new-instance v5, Lcom/google/android/finsky/gearhead/GearheadStateMonitor$4;

    invoke-direct {v5}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor$4;-><init>()V

    invoke-static {v1, v2, v4, v5}, Lcom/google/android/gms/car/Car;->buildGoogleApiClientForCar(Landroid/content/Context;Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;Lcom/google/android/gms/common/api/GoogleApiClient$OnConnectionFailedListener;Lcom/google/android/gms/car/Car$CarConnectionListener;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v2

    sput-object v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    .line 141
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sHasInitialized:Z

    .line 142
    sget-object v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sApiClient:Lcom/google/android/gms/common/api/GoogleApiClient;

    invoke-interface {v2}, Lcom/google/android/gms/common/api/GoogleApiClient;->connect()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 75
    .end local v0    # "connectionChangeFilter":Landroid/content/IntentFilter;
    .end local v1    # "context":Landroid/content/Context;
    :catchall_1
    move-exception v2

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method public static isProjecting()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 173
    sget-boolean v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->GEARHEAD_SUPPORTED:Z

    if-nez v2, :cond_0

    .line 183
    .local v0, "e":Ljava/lang/InterruptedException;
    :goto_0
    return v1

    .line 178
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    :try_start_0
    sget-object v2, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sConnectionLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    sget-boolean v1, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sIsProjecting:Z

    goto :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 180
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    const-string v2, "Interrupted while awaiting projection result!"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static isReady()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 162
    sget-boolean v1, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->GEARHEAD_SUPPORTED:Z

    if-nez v1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sConnectionLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static onReady()V
    .locals 4

    .prologue
    .line 148
    const-string v0, "sIsProjecting:%b"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-boolean v3, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sIsProjecting:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    sget-object v0, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sConnectionLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 151
    sget-object v1, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sOnReadyRunnables:Ljava/util/LinkedList;

    monitor-enter v1

    .line 152
    :goto_0
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sOnReadyRunnables:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    sget-object v0, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->sOnReadyRunnables:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 156
    return-void
.end method
