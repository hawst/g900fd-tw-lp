.class public Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;
.super Landroid/view/ViewGroup;
.source "PlayCardRateAndSuggestContentScroller.java"


# instance fields
.field private mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

.field private final mHideFirstCardHandler:Landroid/os/Handler;

.field private mShouldHideFirstCard:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mHideFirstCardHandler:Landroid/os/Handler;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;)Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    return-object v0
.end method


# virtual methods
.method public hideRateCard()V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mShouldHideFirstCard:Z

    .line 102
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->requestLayout()V

    .line 103
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 50
    const v0, 0x7f0a02b7

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    .line 51
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v3, 0x0

    .line 126
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->layout(IIII)V

    .line 129
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mShouldHideFirstCard:Z

    if-eqz v0, :cond_0

    .line 130
    iput-boolean v3, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mShouldHideFirstCard:Z

    .line 131
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mHideFirstCardHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller$2;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller$2;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 142
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 107
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMetadata()Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;

    move-result-object v1

    .line 108
    .local v1, "clusterMetadata":Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;
    if-nez v1, :cond_0

    .line 110
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 122
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardContentHorizontalPadding()I

    move-result v5

    mul-int/lit8 v4, v5, 0x2

    .line 114
    .local v4, "paddings":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    sub-int v0, v5, v4

    .line 115
    .local v0, "availableWidth":I
    int-to-float v5, v0

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getViewportWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v2, v5, v6

    .line 116
    .local v2, "columnWidth":F
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterMetadata;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    float-to-int v5, v5

    add-int v3, v4, v5

    .line 118
    .local v3, "contentWidth":I
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->measure(II)V

    .line 121
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {p0, v0, v5}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public scrollAwayRateCard()V
    .locals 12

    .prologue
    const-wide/16 v10, 0xc8

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 56
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 91
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1, v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/layout/PlayCardViewBase;->getWidth()I

    move-result v6

    .line 63
    .local v6, "scrollTargetX":I
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-ge v1, v3, :cond_1

    .line 66
    new-instance v0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller$1;

    int-to-float v4, v6

    move-object v1, p0

    move v3, v2

    move v5, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller$1;-><init>(Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;FFFFI)V

    .line 75
    .local v0, "translateAnimation":Landroid/view/animation/TranslateAnimation;
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 76
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 77
    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->startAnimation(Landroid/view/animation/Animation;)V

    .line 79
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1, v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f05000c

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/PlayCardViewBase;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 84
    .end local v0    # "translateAnimation":Landroid/view/animation/TranslateAnimation;
    :cond_1
    const-string v1, "scrollX"

    const/4 v3, 0x1

    new-array v3, v3, [I

    aput v6, v3, v8

    invoke-static {p0, v1, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 85
    .local v7, "animator":Landroid/animation/ObjectAnimator;
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v7, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 86
    invoke-virtual {v7, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 87
    invoke-virtual {v7}, Landroid/animation/ObjectAnimator;->start()V

    .line 89
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateAndSuggestContentScroller;->mClusterContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    invoke-virtual {v1, v8}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->getCardChildAt(I)Lcom/google/android/play/layout/PlayCardViewBase;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/play/layout/PlayCardViewBase;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method
