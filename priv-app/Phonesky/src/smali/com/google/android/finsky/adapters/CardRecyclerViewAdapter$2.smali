.class Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;
.super Ljava/lang/Object;
.source "CardRecyclerViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->bindActionBannerCluster(ILandroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

.field final synthetic val$action:Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

.field final synthetic val$cluster:Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;Lcom/google/android/finsky/protos/DocumentV2$CallToAction;)V
    .locals 0

    .prologue
    .line 1435
    iput-object p1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iput-object p2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;->val$cluster:Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;

    iput-object p3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;->val$action:Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1439
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    const/16 v1, 0x4ce

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;->val$cluster:Lcom/google/android/finsky/layout/play/PlayCardActionBannerClusterView;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 1443
    iget-object v0, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    iget-object v0, v0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;->val$action:Lcom/google/android/finsky/protos/DocumentV2$CallToAction;

    iget-object v2, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;
    invoke-static {v2}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->access$400(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter$2;->this$0:Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;

    # getter for: Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->mToc:Lcom/google/android/finsky/api/model/DfeToc;
    invoke-static {v3}, Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;->access$500(Lcom/google/android/finsky/adapters/CardRecyclerViewAdapter;)Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v3

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->resolveCallToAction(Lcom/google/android/finsky/protos/DocumentV2$CallToAction;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Landroid/content/pm/PackageManager;)V

    .line 1445
    return-void
.end method
