.class Lcom/google/android/finsky/activities/ReviewDialogListener$2;
.super Ljava/lang/Object;
.source "ReviewDialogListener.java"

# interfaces
.implements Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/ReviewDialogListener;->onDeleteReview(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/ReviewDialogListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/ReviewDialogListener;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/finsky/activities/ReviewDialogListener$2;->this$0:Lcom/google/android/finsky/activities/ReviewDialogListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRateReviewCommitted(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "rating"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "content"    # Ljava/lang/String;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener$2;->this$0:Lcom/google/android/finsky/activities/ReviewDialogListener;

    # invokes: Lcom/google/android/finsky/activities/ReviewDialogListener;->refreshUserReview()V
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewDialogListener;->access$000(Lcom/google/android/finsky/activities/ReviewDialogListener;)V

    .line 78
    return-void
.end method

.method public onRateReviewFailed()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/finsky/activities/ReviewDialogListener$2;->this$0:Lcom/google/android/finsky/activities/ReviewDialogListener;

    # invokes: Lcom/google/android/finsky/activities/ReviewDialogListener;->refreshUserReview()V
    invoke-static {v0}, Lcom/google/android/finsky/activities/ReviewDialogListener;->access$000(Lcom/google/android/finsky/activities/ReviewDialogListener;)V

    .line 83
    return-void
.end method
