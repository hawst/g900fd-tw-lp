.class public final Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TosFormField"
.end annotation


# instance fields
.field public hasTosHtml:Z

.field public hasTosHtmlShort:Z

.field public hasTosId:Z

.field public hasTosUrl:Z

.field public tosHtml:Ljava/lang/String;

.field public tosHtmlShort:Ljava/lang/String;

.field public tosId:Ljava/lang/String;

.field public tosUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1942
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1943
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->clear()Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    .line 1944
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1947
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosId:Ljava/lang/String;

    .line 1948
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosId:Z

    .line 1949
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtml:Ljava/lang/String;

    .line 1950
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosHtml:Z

    .line 1951
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtmlShort:Ljava/lang/String;

    .line 1952
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosHtmlShort:Z

    .line 1953
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosUrl:Ljava/lang/String;

    .line 1954
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosUrl:Z

    .line 1955
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->cachedSize:I

    .line 1956
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1979
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1980
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosId:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosId:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1981
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1984
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosHtml:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1985
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1988
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosHtmlShort:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtmlShort:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1989
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtmlShort:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1992
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosUrl:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1993
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1996
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2004
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2005
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2009
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2010
    :sswitch_0
    return-object p0

    .line 2015
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosId:Ljava/lang/String;

    .line 2016
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosId:Z

    goto :goto_0

    .line 2020
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtml:Ljava/lang/String;

    .line 2021
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosHtml:Z

    goto :goto_0

    .line 2025
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtmlShort:Ljava/lang/String;

    .line 2026
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosHtmlShort:Z

    goto :goto_0

    .line 2030
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosUrl:Ljava/lang/String;

    .line 2031
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosUrl:Z

    goto :goto_0

    .line 2005
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1909
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1962
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosId:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosId:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1963
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1965
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosHtml:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1966
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1968
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosHtmlShort:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtmlShort:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1969
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtmlShort:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1971
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->hasTosUrl:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1972
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1974
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1975
    return-void
.end method
