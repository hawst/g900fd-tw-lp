.class public Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;
.super Ljava/lang/Object;
.source "WriteThroughInstallerDataStore.java"

# interfaces
.implements Lcom/google/android/finsky/appstate/InstallerDataStore;


# instance fields
.field private final mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

.field private mIsLoaded:Z

.field private mLoadedCallbacks:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotificationHandler:Landroid/os/Handler;

.field private final mPersistentStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

.field private final mWriteThroughHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/appstate/InstallerDataStore;Lcom/google/android/finsky/appstate/InstallerDataStore;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 1
    .param p1, "inMemoryStore"    # Lcom/google/android/finsky/appstate/InstallerDataStore;
    .param p2, "persistingStore"    # Lcom/google/android/finsky/appstate/InstallerDataStore;
    .param p3, "writeThroughHandler"    # Landroid/os/Handler;
    .param p4, "notificationHandler"    # Landroid/os/Handler;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mLoadedCallbacks:Ljava/util/Collection;

    .line 38
    iput-object p1, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    .line 39
    iput-object p2, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mPersistentStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    .line 40
    iput-object p3, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    .line 41
    iput-object p4, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mNotificationHandler:Landroid/os/Handler;

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;)Lcom/google/android/finsky/appstate/InstallerDataStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mPersistentStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAll()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0}, Lcom/google/android/finsky/appstate/InstallerDataStore;->getAll()Ljava/util/Collection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isLoaded()Z
    .locals 1

    .prologue
    .line 45
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mIsLoaded:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized load()V
    .locals 4

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-boolean v3, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mIsLoaded:Z

    if-nez v3, :cond_1

    .line 55
    iget-object v3, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mPersistentStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->getAll()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .line 56
    .local v1, "installerDataSnapshot":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    iget-object v3, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v3, v1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->put(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 54
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "installerDataSnapshot":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 58
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mIsLoaded:Z

    .line 60
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mLoadedCallbacks:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    .line 61
    .local v2, "loadedCallback":Ljava/lang/Runnable;
    if-eqz v2, :cond_2

    .line 62
    iget-object v3, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 65
    .end local v2    # "loadedCallback":Ljava/lang/Runnable;
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mLoadedCallbacks:Ljava/util/Collection;

    invoke-interface {v3}, Ljava/util/Collection;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized load(Ljava/lang/Runnable;)Z
    .locals 2
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mIsLoaded:Z

    if-eqz v0, :cond_1

    .line 76
    if-eqz p1, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    :cond_0
    const/4 v0, 0x1

    .line 90
    :goto_0
    monitor-exit p0

    return v0

    .line 81
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mLoadedCallbacks:Ljava/util/Collection;

    invoke-interface {v0, p1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 82
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mLoadedCallbacks:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_2

    .line 83
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$1;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 90
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized put(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V
    .locals 2
    .param p1, "state"    # Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->put(Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$2;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeLocalAppState(Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 336
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/appstate/InstallerDataStore;->removeLocalAppState(Ljava/lang/String;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$22;

    invoke-direct {v1, p0, p1}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$22;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 343
    monitor-exit p0

    return-void

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAccountForUpdate(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "accountForUpdate"    # Ljava/lang/String;

    .prologue
    .line 266
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setAccountForUpdate(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$16;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$16;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    monitor-exit p0

    return-void

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAutoAcquireTags(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "autoAcquireTags"    # [Ljava/lang/String;

    .prologue
    .line 278
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setAutoAcquireTags(Ljava/lang/String;[Ljava/lang/String;)V

    .line 279
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$17;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$17;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    monitor-exit p0

    return-void

    .line 278
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAutoUpdate(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "autoUpdate"    # Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setAutoUpdate(Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$4;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;Lcom/google/android/finsky/appstate/InstallerDataStore$AutoUpdateState;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    monitor-exit p0

    return-void

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setContinueUrl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "continueUrl"    # Ljava/lang/String;

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setContinueUrl(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$14;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$14;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    monitor-exit p0

    return-void

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setDeliveryData(Ljava/lang/String;Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;J)V
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "deliveryData"    # Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;
    .param p3, "timestampMs"    # J

    .prologue
    .line 162
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDeliveryData(Ljava/lang/String;Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;J)V

    .line 163
    iget-object v6, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$7;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$7;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;Lcom/google/android/finsky/protos/AndroidAppDelivery$AndroidAppDeliveryData;J)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    monitor-exit p0

    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setDeliveryToken(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "deliveryToken"    # Ljava/lang/String;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDeliveryToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$21;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$21;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 331
    return-void
.end method

.method public declared-synchronized setDesiredVersion(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "desiredVersion"    # I

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setDesiredVersion(Ljava/lang/String;I)V

    .line 139
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$5;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    monitor-exit p0

    return-void

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setExternalReferrer(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "externalReferrer"    # Ljava/lang/String;

    .prologue
    .line 198
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setExternalReferrer(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$10;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$10;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    monitor-exit p0

    return-void

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setExternalReferrerTimestampMs(Ljava/lang/String;J)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "externalReferrerTimestampMs"    # J

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setExternalReferrerTimestampMs(Ljava/lang/String;J)V

    .line 291
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$18;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$18;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 298
    return-void
.end method

.method public declared-synchronized setFirstDownloadMs(Ljava/lang/String;J)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "firstDownloadMs"    # J

    .prologue
    .line 186
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFirstDownloadMs(Ljava/lang/String;J)V

    .line 187
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$9;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$9;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 193
    monitor-exit p0

    return-void

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setFlags(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .prologue
    .line 231
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setFlags(Ljava/lang/String;I)V

    .line 232
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$13;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$13;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    monitor-exit p0

    return-void

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setInstallerState(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "installerState"    # I
    .param p3, "downloadUri"    # Ljava/lang/String;

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setInstallerState(Ljava/lang/String;ILjava/lang/String;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$8;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$8;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    monitor-exit p0

    return-void

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setLastNotifiedVersion(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "lastNotifiedVersion"    # I

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setLastNotifiedVersion(Ljava/lang/String;I)V

    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$6;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$6;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    monitor-exit p0

    return-void

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setLastUpdateTimestampMs(Ljava/lang/String;J)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "lastUpdateTimestampMs"    # J

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setLastUpdateTimestampMs(Ljava/lang/String;J)V

    .line 255
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$15;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$15;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 261
    return-void
.end method

.method public setPermissionsVersion(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "permissionsVersion"    # I

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setPermissionsVersion(Ljava/lang/String;I)V

    .line 314
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$20;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$20;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 320
    return-void
.end method

.method public setPersistentFlags(Ljava/lang/String;I)V
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "persistentFlags"    # I

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mInMemoryStore:Lcom/google/android/finsky/appstate/InstallerDataStore;

    invoke-interface {v0, p1, p2}, Lcom/google/android/finsky/appstate/InstallerDataStore;->setPersistentFlags(Ljava/lang/String;I)V

    .line 303
    iget-object v0, p0, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;->mWriteThroughHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$19;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore$19;-><init>(Lcom/google/android/finsky/appstate/WriteThroughInstallerDataStore;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 309
    return-void
.end method
