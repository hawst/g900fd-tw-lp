.class public Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;
.super Ljava/lang/Object;
.source "AppIconProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/providers/AppIconProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AppIconLoader"
.end annotation


# static fields
.field private static sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;


# instance fields
.field private mAppPackage:Ljava/lang/String;

.field private mIconFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appPackage"    # Ljava/lang/String;

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    sget-object v1, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    if-nez v1, :cond_0

    .line 181
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "AppIconLoader must be initialized before use."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 184
    :cond_0
    iput-object p2, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mAppPackage:Ljava/lang/String;

    .line 185
    sget-object v1, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/File;

    iput-object v1, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    .line 188
    iget-object v1, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb80

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 191
    sget-object v1, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    .line 196
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    if-nez v1, :cond_2

    .line 198
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 199
    .local v0, "dir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mAppPackage:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->fileNameFromPackage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    .line 201
    .end local v0    # "dir":Ljava/io/File;
    :cond_2
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;Landroid/graphics/Bitmap;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;
    .param p1, "x1"    # Landroid/graphics/Bitmap;
    .param p2, "x2"    # Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;

    .prologue
    .line 156
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->loadToFileFromBitmap(Landroid/graphics/Bitmap;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V

    return-void
.end method

.method public static fileNameFromPackage(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "appPackage"    # Ljava/lang/String;

    .prologue
    .line 208
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "thmb_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized initialize([Ljava/io/File;)V
    .locals 8
    .param p0, "files"    # [Ljava/io/File;

    .prologue
    .line 168
    const-class v6, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;

    monitor-enter v6

    :try_start_0
    sget-object v5, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    if-nez v5, :cond_1

    .line 169
    new-instance v5, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    const/16 v7, 0x14

    invoke-direct {v5, v7}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;-><init>(I)V

    sput-object v5, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    .line 170
    move-object v0, p0

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 171
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 172
    .local v2, "filename":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->isTempFileName(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 173
    sget-object v5, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    invoke-static {v2}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->packageNameFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7, v1}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 177
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    .end local v2    # "filename":Ljava/lang/String;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_1
    monitor-exit v6

    return-void

    .line 168
    :catchall_0
    move-exception v5

    monitor-exit v6

    throw v5
.end method

.method public static isTempFileName(Ljava/lang/String;)Z
    .locals 1
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 204
    const-string v0, "thmb_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private loadToFileFromBitmap(Landroid/graphics/Bitmap;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V
    .locals 6
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "listener"    # Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;

    .prologue
    .line 246
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 247
    .local v1, "out":Ljava/io/FileOutputStream;
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 248
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    sget-object v2, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    iget-object v3, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->packageNameFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;->callOnComplete()V

    .line 255
    .end local v1    # "out":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 249
    :catch_0
    move-exception v0

    .line 250
    .local v0, "ioe":Ljava/io/IOException;
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to write icon blob to file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 252
    sget-object v2, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    iget-object v3, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->packageNameFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;->callOnComplete()V

    goto :goto_0

    .line 252
    .end local v0    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    iget-object v4, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->packageNameFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;->callOnComplete()V

    throw v2
.end method

.method public static packageNameFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "filename"    # Ljava/lang/String;

    .prologue
    .line 212
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public getIconFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    return-object v0
.end method

.method public loadToFileFromBlob([BLcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V
    .locals 6
    .param p1, "blob"    # [B
    .param p2, "listener"    # Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;

    .prologue
    .line 259
    sget-object v2, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    iget-object v3, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mAppPackage:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 260
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;->callOnComplete()V

    .line 273
    :goto_0
    return-void

    .line 264
    :cond_0
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 265
    .local v1, "out":Ljava/io/FileOutputStream;
    invoke-virtual {v1, p1}, Ljava/io/FileOutputStream;->write([B)V

    .line 266
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 270
    sget-object v2, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    iget-object v3, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->packageNameFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;->callOnComplete()V

    goto :goto_0

    .line 267
    .end local v1    # "out":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v0

    .line 268
    .local v0, "ioe":Ljava/io/IOException;
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to write icon blob to file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    sget-object v2, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    iget-object v3, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->packageNameFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;->callOnComplete()V

    goto :goto_0

    .line 270
    .end local v0    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    sget-object v3, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    iget-object v4, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->packageNameFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mIconFile:Ljava/io/File;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 271
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;->callOnComplete()V

    throw v2
.end method

.method public loadToFileFromUrl(Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V
    .locals 7
    .param p1, "image"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p2, "listener"    # Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;

    .prologue
    const/4 v4, 0x0

    .line 220
    sget-object v1, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->sIconCache:Lcom/google/android/finsky/providers/AppIconProvider$IconCache;

    iget-object v3, p0, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->mAppPackage:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/finsky/providers/AppIconProvider$IconCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 221
    invoke-virtual {p2}, Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;->callOnComplete()V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v0

    .line 226
    .local v0, "bitmapLoader":Lcom/google/android/play/image/BitmapLoader;
    iget-boolean v1, p1, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v1, :cond_2

    # getter for: Lcom/google/android/finsky/providers/AppIconProvider;->mIconSize:I
    invoke-static {}, Lcom/google/android/finsky/providers/AppIconProvider;->access$100()I

    move-result v2

    .line 227
    .local v2, "requestIconSize":I
    :goto_1
    iget-object v1, p1, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    new-instance v5, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader$1;

    invoke-direct {v5, p0, p2}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader$1;-><init>(Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IIZLcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v6

    .line 239
    .local v6, "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    invoke-virtual {v6}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 240
    invoke-virtual {v6}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/google/android/finsky/providers/AppIconProvider$AppIconLoader;->loadToFileFromBitmap(Landroid/graphics/Bitmap;Lcom/google/android/finsky/providers/AppIconProvider$TimedOnCompleteListener;)V

    goto :goto_0

    .end local v2    # "requestIconSize":I
    .end local v6    # "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    :cond_2
    move v2, v4

    .line 226
    goto :goto_1
.end method
