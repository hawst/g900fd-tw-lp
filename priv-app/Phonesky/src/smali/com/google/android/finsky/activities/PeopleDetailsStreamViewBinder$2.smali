.class Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;
.super Ljava/lang/Object;
.source "PeopleDetailsStreamViewBinder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->rebindAdapter()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

.field final synthetic val$isRestoring:Z


# direct methods
.method constructor <init>(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;Z)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    iput-boolean p2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->val$isRestoring:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    # getter for: Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->access$100(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    # getter for: Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;
    invoke-static {v0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->access$200(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    # getter for: Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->access$100(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    # getter for: Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;
    invoke-static {v1}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->access$200(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 168
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->val$isRestoring:Z

    if-eqz v0, :cond_0

    .line 169
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    # getter for: Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mAdapter:Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;
    invoke-static {v0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->access$200(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    # getter for: Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerView:Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    invoke-static {v1}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->access$100(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    # getter for: Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;
    invoke-static {v2}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->access$300(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/finsky/adapters/PeopleDetailsStreamAdapter;->onRestoreInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder$2;->this$0:Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;

    # getter for: Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->mRecyclerViewRestoreBundle:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;->access$300(Lcom/google/android/finsky/activities/PeopleDetailsStreamViewBinder;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 175
    :cond_0
    return-void
.end method
