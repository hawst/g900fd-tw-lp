.class public abstract Lcom/google/android/finsky/activities/myapps/MyAppsTab;
.super Ljava/lang/Object;
.source "MyAppsTab.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/volley/Response$ErrorListener;
.implements Lcom/google/android/finsky/activities/ViewPagerTab;
.implements Lcom/google/android/finsky/api/model/OnDataChangedListener;
.implements Lcom/google/android/finsky/installer/InstallerListener;
.implements Lcom/google/android/finsky/library/Libraries$Listener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/google/android/finsky/api/model/DfeModel;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/android/volley/Response$ErrorListener;",
        "Lcom/google/android/finsky/activities/ViewPagerTab;",
        "Lcom/google/android/finsky/api/model/OnDataChangedListener;",
        "Lcom/google/android/finsky/installer/InstallerListener;",
        "Lcom/google/android/finsky/library/Libraries$Listener;"
    }
.end annotation


# instance fields
.field protected final mAuthenticatedActivity:Lcom/google/android/finsky/activities/AuthenticatedActivity;

.field protected final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field protected mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected final mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

.field protected final mInstaller:Lcom/google/android/finsky/receivers/Installer;

.field private mIsSelectedTab:Z

.field private mLastVolleyError:Lcom/android/volley/VolleyError;

.field protected final mLayoutInflater:Landroid/view/LayoutInflater;

.field protected final mLibraries:Lcom/google/android/finsky/library/Libraries;

.field protected final mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;


# direct methods
.method protected constructor <init>(Lcom/google/android/finsky/activities/AuthenticatedActivity;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;)V
    .locals 1
    .param p1, "authenticatedActivity"    # Lcom/google/android/finsky/activities/AuthenticatedActivity;
    .param p2, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p3, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .prologue
    .line 52
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mIsSelectedTab:Z

    .line 53
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mAuthenticatedActivity:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mAuthenticatedActivity:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 55
    iput-object p2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 56
    iput-object p3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    .line 57
    iput-object p4, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 59
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    .line 60
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->addListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 62
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    .line 63
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->addListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 64
    return-void
.end method


# virtual methods
.method protected clearState()V
    .locals 1

    .prologue
    .line 95
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeModel;->removeDataChangedListener(Lcom/google/android/finsky/api/model/OnDataChangedListener;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/api/model/DfeModel;->removeErrorListener(Lcom/android/volley/Response$ErrorListener;)V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    .line 100
    :cond_0
    return-void
.end method

.method protected configureEmptyUI(ZI)V
    .locals 6
    .param p1, "showAccountSelector"    # Z
    .param p2, "descriptionTextId"    # I

    .prologue
    .line 216
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->getFullView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0a026e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView;

    .line 218
    .local v0, "emptyView":Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView;
    if-eqz v0, :cond_0

    .line 219
    iget-object v1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mDfeToc:Lcom/google/android/finsky/api/model/DfeToc;

    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mAuthenticatedActivity:Lcom/google/android/finsky/activities/AuthenticatedActivity;

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/myapps/MyAppsEmptyView;->configure(Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/activities/AuthenticatedActivity;ZI)V

    .line 221
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->getListView()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 223
    :cond_0
    return-void
.end method

.method protected finishActiveMode()Z
    .locals 1

    .prologue
    .line 204
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract getAdapter()Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;
.end method

.method protected abstract getDocumentForView(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;
.end method

.method protected abstract getFullView()Landroid/view/View;
.end method

.method protected abstract getListView()Landroid/widget/ListView;
.end method

.method protected getPositionForView(Landroid/view/View;)I
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    const/4 v4, -0x1

    .line 110
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f0a00d0

    if-ne v5, v6, :cond_0

    .line 114
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    .end local p1    # "view":Landroid/view/View;
    check-cast p1, Landroid/view/View;

    .line 116
    .restart local p1    # "view":Landroid/view/View;
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->getDocumentForView(Landroid/view/View;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 117
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    if-nez v0, :cond_2

    .line 140
    :cond_1
    :goto_0
    return v4

    .line 123
    :cond_2
    move-object v3, p1

    .line 124
    .local v3, "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 126
    .local v1, "listView":Landroid/widget/ListView;
    :goto_1
    if-eqz v3, :cond_1

    .line 129
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    .line 130
    .local v2, "parent":Landroid/view/ViewParent;
    if-ne v2, v1, :cond_3

    .line 140
    invoke-virtual {v1, p1}, Landroid/widget/ListView;->getPositionForView(Landroid/view/View;)I

    move-result v4

    goto :goto_0

    .line 134
    :cond_3
    instance-of v5, v2, Landroid/view/View;

    if-eqz v5, :cond_1

    move-object v3, v2

    .line 137
    check-cast v3, Landroid/view/View;

    .line 138
    goto :goto_1
.end method

.method protected final isDataReady()Z
    .locals 1

    .prologue
    .line 84
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mDfeModel:Lcom/google/android/finsky/api/model/DfeModel;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/DfeModel;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final loadData()V
    .locals 1

    .prologue
    .line 88
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->requestData()V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->isDataReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->onDataChanged()V

    .line 92
    :cond_0
    return-void
.end method

.method public onAllLibrariesLoaded()V
    .locals 0

    .prologue
    .line 209
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 104
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 105
    .local v1, "position":I
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->getAdapter()Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/finsky/activities/myapps/MyAppsListAdapter;->getDocument(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    .line 106
    .local v0, "document":Lcom/google/android/finsky/api/model/Document;
    iget-object v2, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goToDocPage(Lcom/google/android/finsky/api/model/Document;)V

    .line 107
    return-void
.end method

.method public onDataChanged()V
    .locals 1

    .prologue
    .line 186
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mLastVolleyError:Lcom/android/volley/VolleyError;

    .line 187
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 68
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->clearState()V

    .line 69
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mInstaller:Lcom/google/android/finsky/receivers/Installer;

    invoke-interface {v0, p0}, Lcom/google/android/finsky/receivers/Installer;->removeListener(Lcom/google/android/finsky/installer/InstallerListener;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mLibraries:Lcom/google/android/finsky/library/Libraries;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/library/Libraries;->removeListener(Lcom/google/android/finsky/library/Libraries$Listener;)V

    .line 71
    return-void
.end method

.method public onErrorResponse(Lcom/android/volley/VolleyError;)V
    .locals 0
    .param p1, "error"    # Lcom/android/volley/VolleyError;

    .prologue
    .line 180
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    iput-object p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mLastVolleyError:Lcom/android/volley/VolleyError;

    .line 181
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->syncViewToState()V

    .line 182
    return-void
.end method

.method protected abstract requestData()V
.end method

.method protected abstract retryFromError()V
.end method

.method public setTabSelected(Z)V
    .locals 0
    .param p1, "isSelected"    # Z

    .prologue
    .line 191
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    iput-boolean p1, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mIsSelectedTab:Z

    .line 192
    return-void
.end method

.method protected syncViewToState()V
    .locals 8

    .prologue
    .local p0, "this":Lcom/google/android/finsky/activities/myapps/MyAppsTab;, "Lcom/google/android/finsky/activities/myapps/MyAppsTab<TT;>;"
    const/4 v6, 0x0

    const/16 v7, 0x8

    .line 144
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->getFullView()Landroid/view/View;

    move-result-object v3

    .line 146
    .local v3, "fullView":Landroid/view/View;
    const v5, 0x7f0a0254

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 147
    .local v4, "loadingIndicator":Landroid/view/View;
    const v5, 0x7f0a02a3

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 148
    .local v1, "errorIndicator":Landroid/view/View;
    const v5, 0x7f0a0276

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 150
    .local v0, "contentListView":Landroid/widget/ListView;
    iget-object v5, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mLastVolleyError:Lcom/android/volley/VolleyError;

    if-eqz v5, :cond_0

    .line 151
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 152
    const v5, 0x7f0a00f4

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 153
    .local v2, "errorMsg":Landroid/widget/TextView;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->mLastVolleyError:Lcom/android/volley/VolleyError;

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/ErrorStrings;->get(Landroid/content/Context;Lcom/android/volley/VolleyError;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    const v5, 0x7f0a01ce

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    new-instance v6, Lcom/google/android/finsky/activities/myapps/MyAppsTab$1;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab$1;-><init>(Lcom/google/android/finsky/activities/myapps/MyAppsTab;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 164
    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 174
    .end local v2    # "errorMsg":Landroid/widget/TextView;
    :goto_0
    return-void

    .line 165
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myapps/MyAppsTab;->isDataReady()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 166
    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 167
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 168
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 171
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 172
    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method
