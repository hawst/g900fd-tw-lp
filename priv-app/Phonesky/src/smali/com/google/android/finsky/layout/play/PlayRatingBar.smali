.class public Lcom/google/android/finsky/layout/play/PlayRatingBar;
.super Landroid/view/ViewGroup;
.source "PlayRatingBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;
    }
.end annotation


# instance fields
.field private mCurrentRating:I

.field private mExtraVerticalPadding:I

.field private mListener:Lcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;

.field private mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    return-void
.end method

.method private resetVisualState()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 123
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 124
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v1, v1, v0

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFocused(Z)V

    .line 125
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v3, v1, v0

    iget v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mCurrentRating:I

    if-ge v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v3, v1}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFilled(Z)V

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    .line 125
    goto :goto_1

    .line 127
    :cond_1
    return-void
.end method


# virtual methods
.method public configure(IILcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;)V
    .locals 3
    .param p1, "initialRating"    # I
    .param p2, "backend"    # I
    .param p3, "listener"    # Lcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;

    .prologue
    .line 103
    iput-object p3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mListener:Lcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;

    .line 105
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 107
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v0, v2, v1

    .line 108
    .local v0, "currentRatingStar":Lcom/google/android/finsky/layout/play/PlayRatingStar;
    invoke-virtual {v0, v1, p2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->configure(II)V

    .line 110
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 112
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setOnPressStateChangeListener(Lcom/google/android/finsky/layout/play/PlayRatingStar$OnPressStateChangeListener;)V

    .line 114
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    if-ge v1, p1, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFilled(Z)V

    .line 105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 116
    :cond_0
    const/4 v2, 0x0

    goto :goto_1

    .line 119
    .end local v0    # "currentRatingStar":Lcom/google/android/finsky/layout/play/PlayRatingStar;
    :cond_1
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mCurrentRating:I

    .line 120
    return-void
.end method

.method public getRating()I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mCurrentRating:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 174
    move-object v1, p1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayRatingStar;

    .line 175
    .local v1, "star":Lcom/google/android/finsky/layout/play/PlayRatingStar;
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->getIndex()I

    move-result v2

    .line 177
    .local v2, "starIndex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 178
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v4, v3, v0

    if-gt v0, v2, :cond_0

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v4, v3}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFilled(Z)V

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 178
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 180
    :cond_1
    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mCurrentRating:I

    .line 182
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mListener:Lcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;

    add-int/lit8 v4, v2, 0x1

    invoke-interface {v3, p0, v4}, Lcom/google/android/finsky/layout/play/PlayRatingBar$OnRatingChangeListener;->onRatingChanged(Lcom/google/android/finsky/layout/play/PlayRatingBar;I)V

    .line 183
    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 69
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 73
    .local v0, "res":Landroid/content/res/Resources;
    const/4 v1, 0x5

    new-array v1, v1, [Lcom/google/android/finsky/layout/play/PlayRatingStar;

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    .line 74
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    const v1, 0x7f0a032d

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aput-object v1, v2, v3

    .line 75
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v1, v1, v3

    const v2, 0x7f0c023a

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    const v1, 0x7f0a032e

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aput-object v1, v2, v4

    .line 77
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v1, v1, v4

    const v2, 0x7f0c0239

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    const v1, 0x7f0a032f

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aput-object v1, v2, v5

    .line 79
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v1, v1, v5

    const v2, 0x7f0c0238

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    const v1, 0x7f0a0330

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aput-object v1, v2, v6

    .line 81
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v1, v1, v6

    const v2, 0x7f0c0237

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    const v1, 0x7f0a0331

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aput-object v1, v2, v7

    .line 83
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v1, v1, v7

    const v2, 0x7f0c0236

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 84
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const/4 v5, 0x0

    .line 131
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 132
    .local v0, "focusedChild":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v6

    if-nez v6, :cond_2

    .line 134
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->resetVisualState()V

    .line 153
    :cond_1
    :goto_0
    return-void

    :cond_2
    move-object v3, p1

    .line 138
    check-cast v3, Lcom/google/android/finsky/layout/play/PlayRatingStar;

    .line 139
    .local v3, "star":Lcom/google/android/finsky/layout/play/PlayRatingStar;
    invoke-virtual {v3}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->getIndex()I

    move-result v4

    .line 141
    .local v4, "starIndex":I
    if-nez p2, :cond_3

    .line 142
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v6, v6, v4

    invoke-virtual {v6, v5}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFocused(Z)V

    .line 143
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v6, v6, v4

    invoke-virtual {v6, v5}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFilled(Z)V

    goto :goto_0

    .line 147
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    array-length v6, v6

    if-ge v1, v6, :cond_1

    .line 148
    if-gt v1, v4, :cond_4

    const/4 v2, 0x1

    .line 149
    .local v2, "isFocusedAndFilled":Z
    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v6, v6, v1

    invoke-virtual {v6, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFocused(Z)V

    .line 150
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v6, v6, v1

    invoke-virtual {v6, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFilled(Z)V

    .line 147
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v2    # "isFocusedAndFilled":Z
    :cond_4
    move v2, v5

    .line 148
    goto :goto_2
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 222
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    array-length v4, v6

    .line 223
    .local v4, "starCount":I
    const/4 v5, 0x0

    .line 224
    .local v5, "x":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 225
    iget-object v6, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v0, v6, v3

    .line 226
    .local v0, "currStar":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 227
    .local v2, "currStarWidth":I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 228
    .local v1, "currStarHeight":I
    const/4 v6, 0x0

    add-int v7, v5, v2

    invoke-virtual {v0, v5, v6, v7, v1}, Landroid/view/View;->layout(IIII)V

    .line 229
    add-int/2addr v5, v2

    .line 224
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 231
    .end local v0    # "currStar":Landroid/view/View;
    .end local v1    # "currStarHeight":I
    .end local v2    # "currStarWidth":I
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    .line 196
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 199
    .local v6, "width":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v7, v7, v8

    invoke-virtual {v7, v8, v8}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->measure(II)V

    .line 200
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v7, v7, v8

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->getMeasuredHeight()I

    move-result v7

    iget v8, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mExtraVerticalPadding:I

    mul-int/lit8 v8, v8, 0x2

    add-int v1, v7, v8

    .line 201
    .local v1, "height":I
    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 203
    .local v2, "heightSpec":I
    move v4, v6

    .line 204
    .local v4, "remainingWidth":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    array-length v5, v7

    .line 205
    .local v5, "starCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_0

    .line 211
    sub-int v7, v5, v3

    div-int v0, v4, v7

    .line 212
    .local v0, "currStarWidth":I
    iget-object v7, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v7, v7, v3

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v7, v8, v2}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->measure(II)V

    .line 214
    sub-int/2addr v4, v0

    .line 205
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 217
    .end local v0    # "currStarWidth":I
    :cond_0
    invoke-virtual {p0, v6, v1}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->setMeasuredDimension(II)V

    .line 218
    return-void
.end method

.method public onPressStateChanged(Landroid/view/View;Z)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "isPressed"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 157
    move-object v1, p1

    check-cast v1, Lcom/google/android/finsky/layout/play/PlayRatingStar;

    .line 158
    .local v1, "star":Lcom/google/android/finsky/layout/play/PlayRatingStar;
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->getIndex()I

    move-result v2

    .line 159
    .local v2, "starIndex":I
    if-nez p2, :cond_1

    .line 161
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 162
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v6, v3, v0

    iget v3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mCurrentRating:I

    if-ge v0, v3, :cond_0

    move v3, v4

    :goto_1
    invoke-virtual {v6, v3}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFilled(Z)V

    .line 161
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v3, v5

    .line 162
    goto :goto_1

    .line 166
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 167
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mStars:[Lcom/google/android/finsky/layout/play/PlayRatingStar;

    aget-object v6, v3, v0

    if-gt v0, v2, :cond_2

    move v3, v4

    :goto_3
    invoke-virtual {v6, v3}, Lcom/google/android/finsky/layout/play/PlayRatingStar;->setFilled(Z)V

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v3, v5

    .line 167
    goto :goto_3

    .line 170
    :cond_3
    return-void
.end method

.method public setRating(I)V
    .locals 0
    .param p1, "rating"    # I

    .prologue
    .line 190
    iput p1, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mCurrentRating:I

    .line 191
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->resetVisualState()V

    .line 192
    return-void
.end method

.method public setVerticalPadding(I)V
    .locals 1
    .param p1, "verticalPaddingResourceId"    # I

    .prologue
    .line 87
    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/finsky/layout/play/PlayRatingBar;->mExtraVerticalPadding:I

    .line 89
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->requestLayout()V

    .line 90
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayRatingBar;->invalidate()V

    .line 91
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
