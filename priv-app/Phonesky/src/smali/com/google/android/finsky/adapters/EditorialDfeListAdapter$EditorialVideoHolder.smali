.class Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
.super Ljava/lang/Object;
.source "EditorialDfeListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EditorialVideoHolder"
.end annotation


# instance fields
.field private mDescription:Landroid/widget/TextView;

.field private mTitle:Landroid/widget/TextView;

.field private mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;

.field private mWrapper:Landroid/view/ViewGroup;

.field final synthetic this$0:Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->this$0:Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;
    .param p2, "x1"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$1;

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;-><init>(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter;)V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mWrapper:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mWrapper:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mTitle:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mDescription:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mDescription:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;)Lcom/google/android/finsky/layout/HeroGraphicView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;Lcom/google/android/finsky/layout/HeroGraphicView;)Lcom/google/android/finsky/layout/HeroGraphicView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;
    .param p1, "x1"    # Lcom/google/android/finsky/layout/HeroGraphicView;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/google/android/finsky/adapters/EditorialDfeListAdapter$EditorialVideoHolder;->mVideoImage:Lcom/google/android/finsky/layout/HeroGraphicView;

    return-object p1
.end method
