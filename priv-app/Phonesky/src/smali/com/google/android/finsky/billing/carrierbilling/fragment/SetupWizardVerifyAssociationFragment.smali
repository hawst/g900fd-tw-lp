.class public Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;
.super Lcom/google/android/finsky/fragments/LoggingFragment;
.source "SetupWizardVerifyAssociationFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mCancelButton:Landroid/widget/Button;

.field private mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;
    .locals 3
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 33
    new-instance v1, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;-><init>()V

    .line 34
    .local v1, "fragment":Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 35
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 37
    return-object v1
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 75
    const/16 v0, 0x381

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->mCancelButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;

    invoke-interface {v0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;->onVerifyCancel()V

    .line 71
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 43
    const v3, 0x7f0401b1

    invoke-virtual {p1, v3, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 45
    .local v2, "v":Landroid/view/View;
    const v3, 0x7f0a039b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    .local v0, "message":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;

    invoke-virtual {v3}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/InstrumentActivity;->getSetupWizardNavBar()Lcom/google/android/finsky/setup/SetupWizardNavBar;

    move-result-object v1

    .line 50
    .local v1, "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    if-eqz v1, :cond_0

    .line 51
    invoke-virtual {v1}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 52
    invoke-virtual {v1}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getBackButton()Landroid/widget/Button;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->mCancelButton:Landroid/widget/Button;

    .line 56
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    iget-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->mCancelButton:Landroid/widget/Button;

    const v4, 0x7f0c00f6

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(I)V

    .line 59
    return-object v2

    .line 54
    :cond_0
    const v3, 0x7f0a0390

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->mCancelButton:Landroid/widget/Button;

    goto :goto_0
.end method

.method public setOnResultListener(Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/finsky/billing/carrierbilling/fragment/SetupWizardVerifyAssociationFragment;->mListener:Lcom/google/android/finsky/billing/carrierbilling/fragment/VerifyAssociationDialogFragment$VerifyAssociationListener;

    .line 64
    return-void
.end method
