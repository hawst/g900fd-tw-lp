.class public Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;
.super Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;
.source "InstrumentManagerPurchaseStep.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep",
        "<",
        "Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/finsky/billing/instrumentmanager/InstrumentManagerStep;-><init>()V

    .line 18
    const/16 v0, 0x2e4

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method public static newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;)Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;
    .locals 2
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "tokens"    # Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;-><init>()V

    .line 24
    .local v0, "result":Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;
    invoke-static {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;->createArgs(Ljava/lang/String;Lcom/google/android/finsky/protos/SingleFopPaymentsIntegrator$SingleFopPaymentsIntegratorContext;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;->setArguments(Landroid/os/Bundle;)V

    .line 25
    return-object v0
.end method


# virtual methods
.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onInstrumentManagerResult(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;->isSuccess(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->preparePurchase()V

    .line 37
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/purchasesteps/InstrumentManagerPurchaseStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;->finish()V

    goto :goto_0
.end method
