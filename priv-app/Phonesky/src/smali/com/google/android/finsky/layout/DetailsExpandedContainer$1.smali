.class Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;
.super Ljava/lang/Object;
.source "DetailsExpandedContainer.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsExpandedContainer;->populatePrimaryExtras(Lcom/google/android/finsky/layout/DetailsTextSection;Lcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/activities/TextSectionStateListener;Landroid/view/LayoutInflater;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

.field final synthetic val$backendId:I

.field final synthetic val$extraPrimary:Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

.field final synthetic val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field final synthetic val$parentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field final synthetic val$section:Lcom/google/android/finsky/layout/DetailsTextSection;

.field final synthetic val$textSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;Lcom/google/android/finsky/activities/TextSectionStateListener;Lcom/google/android/finsky/layout/DetailsTextSection;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->this$0:Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    iput-object p2, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$textSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;

    iput-object p3, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$section:Lcom/google/android/finsky/layout/DetailsTextSection;

    iput-object p4, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iput-object p5, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$extraPrimary:Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    iput p6, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$backendId:I

    iput-object p7, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$parentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$textSectionStateListener:Lcom/google/android/finsky/activities/TextSectionStateListener;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$section:Lcom/google/android/finsky/layout/DetailsTextSection;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/activities/TextSectionStateListener;->onSectionCollapsed(I)V

    .line 203
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$navigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$extraPrimary:Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    iget-object v1, v1, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->url:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$extraPrimary:Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    iget-object v2, v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->title:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$backendId:I

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;->val$parentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/NavigationManager;->goBrowse(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 205
    return-void
.end method
