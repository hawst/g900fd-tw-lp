.class public final Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FormRadioSelector"
.end annotation


# instance fields
.field public hasPostParam:Z

.field public postParam:Ljava/lang/String;

.field public radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2725
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2726
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->clear()Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    .line 2727
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;
    .locals 1

    .prologue
    .line 2730
    invoke-static {}, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;->emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    .line 2731
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->postParam:Ljava/lang/String;

    .line 2732
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->hasPostParam:Z

    .line 2733
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->cachedSize:I

    .line 2734
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 2756
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 2757
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 2758
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 2759
    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    aget-object v0, v3, v1

    .line 2760
    .local v0, "element":Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    if-eqz v0, :cond_0

    .line 2761
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2758
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2766
    .end local v0    # "element":Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->hasPostParam:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->postParam:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2767
    :cond_2
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->postParam:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2770
    :cond_3
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2778
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 2779
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 2783
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2784
    :sswitch_0
    return-object p0

    .line 2789
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 2791
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    if-nez v5, :cond_2

    move v1, v4

    .line 2792
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    .line 2794
    .local v2, "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    if-eqz v1, :cond_1

    .line 2795
    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2797
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 2798
    new-instance v5, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;-><init>()V

    aput-object v5, v2, v1

    .line 2799
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2800
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 2797
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2791
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    array-length v1, v5

    goto :goto_1

    .line 2803
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;-><init>()V

    aput-object v5, v2, v1

    .line 2804
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 2805
    iput-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    goto :goto_0

    .line 2809
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->postParam:Ljava/lang/String;

    .line 2810
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->hasPostParam:Z

    goto :goto_0

    .line 2779
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2701
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2740
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 2741
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 2742
    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->radioButton:[Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;

    aget-object v0, v2, v1

    .line 2743
    .local v0, "element":Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    if-eqz v0, :cond_0

    .line 2744
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2741
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2748
    .end local v0    # "element":Lcom/google/android/finsky/protos/ChallengeProto$FormRadioButton;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->hasPostParam:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->postParam:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2749
    :cond_2
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/ChallengeProto$FormRadioSelector;->postParam:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2751
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2752
    return-void
.end method
