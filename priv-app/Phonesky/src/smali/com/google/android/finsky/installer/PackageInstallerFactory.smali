.class public Lcom/google/android/finsky/installer/PackageInstallerFactory;
.super Ljava/lang/Object;
.source "PackageInstallerFactory.java"


# static fields
.field private static sPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;


# direct methods
.method public static getPackageInstaller()Lcom/google/android/finsky/installer/PackageInstallerFacade;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/finsky/installer/PackageInstallerFactory;->sPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 23
    new-instance v0, Lcom/google/android/finsky/installer/PackageInstallerImpl;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/installer/PackageInstallerImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/finsky/installer/PackageInstallerFactory;->sPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    .line 27
    :goto_0
    return-void

    .line 25
    :cond_0
    new-instance v0, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/installer/PackageInstallerLegacyImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/finsky/installer/PackageInstallerFactory;->sPackageInstaller:Lcom/google/android/finsky/installer/PackageInstallerFacade;

    goto :goto_0
.end method
