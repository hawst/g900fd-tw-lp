.class public Lcom/google/android/finsky/download/obb/ObbFactory;
.super Ljava/lang/Object;
.source "ObbFactory.java"


# static fields
.field private static sObbMasterDirectory:Ljava/io/File;


# direct methods
.method public static create(ZLjava/lang/String;ILjava/lang/String;JI)Lcom/google/android/finsky/download/obb/Obb;
    .locals 10
    .param p0, "isPatch"    # Z
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "versionCode"    # I
    .param p3, "url"    # Ljava/lang/String;
    .param p4, "size"    # J
    .param p6, "state"    # I

    .prologue
    .line 28
    new-instance v1, Lcom/google/android/finsky/download/obb/ObbImpl;

    move v2, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-wide v6, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v8}, Lcom/google/android/finsky/download/obb/ObbImpl;-><init>(ZLjava/lang/String;ILjava/lang/String;JI)V

    return-object v1
.end method

.method public static getParentDirectory(Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    .line 44
    new-instance v0, Ljava/io/File;

    sget-object v1, Lcom/google/android/finsky/download/obb/ObbFactory;->sObbMasterDirectory:Ljava/io/File;

    invoke-direct {v0, v1, p0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public static initialize()V
    .locals 4

    .prologue
    .line 36
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    .line 37
    .local v0, "externalStorageDir":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/io/File;

    const-string v3, "Android"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-string v3, "obb"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/finsky/download/obb/ObbFactory;->sObbMasterDirectory:Ljava/io/File;

    .line 38
    return-void
.end method
