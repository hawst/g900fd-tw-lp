.class public Lcom/google/android/finsky/utils/DeviceConfigurationHelper;
.super Ljava/lang/Object;
.source "DeviceConfigurationHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;,
        Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;
    }
.end annotation


# static fields
.field private static sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

.field private static sRequests:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sRequests:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->doNextRequest(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V

    return-void
.end method

.method private static customizeDeviceConfiguration(Landroid/content/Context;Lcom/google/android/finsky/protos/DeviceConfigurationProto;)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "deviceConfiguration"    # Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 312
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 313
    .local v5, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v5}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v6

    .line 317
    .local v6, "systemAvailableFeatures":[Landroid/content/pm/FeatureInfo;
    if-eqz v6, :cond_2

    .line 318
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 319
    .local v2, "featureNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, v6

    .local v0, "arr$":[Landroid/content/pm/FeatureInfo;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 320
    .local v1, "feature":Landroid/content/pm/FeatureInfo;
    iget-object v7, v1, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    if-eqz v7, :cond_0

    .line 321
    iget-object v7, v1, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 319
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 324
    .end local v1    # "feature":Landroid/content/pm/FeatureInfo;
    :cond_1
    new-array v7, v9, [Ljava/lang/String;

    invoke-interface {v2, v7}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    iput-object v7, p1, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    .line 328
    .end local v0    # "arr$":[Landroid/content/pm/FeatureInfo;
    .end local v2    # "featureNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_2
    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x15

    if-lt v7, v8, :cond_3

    .line 329
    sget-object v7, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    iput-object v7, p1, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    .line 338
    :goto_1
    return-void

    .line 332
    :cond_3
    sget-object v7, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    const-string v8, "unknown"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 333
    new-array v7, v10, [Ljava/lang/String;

    sget-object v8, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    aput-object v8, v7, v9

    iput-object v7, p1, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    goto :goto_1

    .line 335
    :cond_4
    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    sget-object v8, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    aput-object v8, v7, v9

    sget-object v8, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    aput-object v8, v7, v10

    iput-object v7, p1, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    goto :goto_1
.end method

.method private static doNextRequest(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V
    .locals 4
    .param p0, "completedRequest"    # Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    .prologue
    const/4 v3, 0x0

    .line 133
    sget-object v1, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sRequests:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 134
    const-string v1, "Empty request queue"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    sget-object v1, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sRequests:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    .line 138
    .local v0, "runningRequest":Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;
    if-eq v0, p0, :cond_2

    .line 139
    const-string v1, "Completed request mismatch"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    :cond_2
    sget-object v1, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sRequests:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 143
    sget-object v1, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sRequests:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    invoke-static {v1}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->doUploadDeviceConfiguration(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V

    goto :goto_0
.end method

.method private static doUploadDeviceConfiguration(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V
    .locals 14
    .param p0, "request"    # Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    .prologue
    const/4 v6, 0x0

    .line 152
    iget-object v4, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;->dfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 153
    .local v4, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    iget-object v5, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;->listener:Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    .line 154
    .local v5, "listener":Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    .line 160
    .local v1, "eventLog":Lcom/google/android/finsky/analytics/FinskyEventLog;
    iget-boolean v0, p0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;->gcmOnly:Z

    if-eqz v0, :cond_0

    .line 161
    const/4 v3, 0x0

    .line 180
    .local v3, "deviceConfiguration":Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/GcmRegistrationIdHelper;->getRegistrationId(Landroid/content/Context;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v13

    .line 185
    .local v13, "gcmResult":Ljava/lang/String;
    :goto_1
    move-object v2, v13

    .line 187
    .local v2, "gcmRegistrationId":Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getToken()Ljava/lang/String;

    move-result-object v9

    new-instance v0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$1;-><init>(Lcom/google/android/finsky/analytics/FinskyEventLog;Ljava/lang/String;Lcom/google/android/finsky/protos/DeviceConfigurationProto;Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V

    new-instance v11, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;

    invoke-direct {v11, v1, v5, p0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$2;-><init>(Lcom/google/android/finsky/analytics/FinskyEventLog;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V

    move-object v6, v4

    move-object v7, v3

    move-object v8, v2

    move-object v10, v0

    invoke-interface/range {v6 .. v11}, Lcom/google/android/finsky/api/DfeApi;->uploadDeviceConfig(Lcom/google/android/finsky/protos/DeviceConfigurationProto;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 235
    .end local v2    # "gcmRegistrationId":Ljava/lang/String;
    .end local v3    # "deviceConfiguration":Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    .end local v13    # "gcmResult":Ljava/lang/String;
    :goto_2
    return-void

    .line 164
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getDeviceConfiguration()Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    .restart local v3    # "deviceConfiguration":Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    goto :goto_0

    .line 165
    .end local v3    # "deviceConfiguration":Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    :catch_0
    move-exception v12

    .line 166
    .local v12, "e":Ljava/lang/Exception;
    const-string v0, "Exception while getting device configuration."

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v12, v0, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 167
    if-eqz v5, :cond_1

    .line 168
    new-instance v0, Lcom/android/volley/ServerError;

    invoke-direct {v0}, Lcom/android/volley/ServerError;-><init>()V

    invoke-interface {v5, v0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;->onError(Lcom/android/volley/VolleyError;)V

    .line 170
    :cond_1
    invoke-static {p0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->doNextRequest(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V

    goto :goto_2

    .line 181
    .end local v12    # "e":Ljava/lang/Exception;
    .restart local v3    # "deviceConfiguration":Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    :catch_1
    move-exception v12

    .line 182
    .restart local v12    # "e":Ljava/lang/Exception;
    const-string v0, "Exception while getting gcm registration id."

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v12, v0, v6}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 183
    const/4 v13, 0x0

    .restart local v13    # "gcmResult":Ljava/lang/String;
    goto :goto_1
.end method

.method public static declared-synchronized getDeviceConfiguration()Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    .locals 13

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 247
    const-class v11, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;

    monitor-enter v11

    :try_start_0
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    if-nez v8, :cond_0

    .line 248
    new-instance v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-direct {v8}, Lcom/google/android/finsky/protos/DeviceConfigurationProto;-><init>()V

    sput-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    .line 250
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    .line 251
    .local v3, "context":Landroid/content/Context;
    const-string v8, "activity"

    invoke-virtual {v3, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 253
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v1

    .line 255
    .local v1, "ci":Landroid/content/pm/ConfigurationInfo;
    invoke-static {v3}, Lcom/google/android/finsky/utils/VendingUtils;->getScreenDimensions(Landroid/content/Context;)Landroid/util/Pair;

    move-result-object v6

    .line 257
    .local v6, "screenDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const-string v8, "window"

    invoke-virtual {v3, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    .line 258
    .local v7, "wm":Landroid/view/WindowManager;
    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    .line 259
    .local v5, "metrics":Landroid/util/DisplayMetrics;
    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 261
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget v12, v1, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    invoke-static {v12}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getTouchScreenId(I)I

    move-result v12

    iput v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->touchScreen:I

    .line 262
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v12, 0x1

    iput-boolean v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasTouchScreen:Z

    .line 264
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget v12, v1, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    invoke-static {v12}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getKeyboardConfigId(I)I

    move-result v12

    iput v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->keyboard:I

    .line 265
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v12, 0x1

    iput-boolean v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasKeyboard:Z

    .line 267
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget v12, v1, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    invoke-static {v12}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getNavigationId(I)I

    move-result v12

    iput v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->navigation:I

    .line 268
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v12, 0x1

    iput-boolean v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasNavigation:Z

    .line 270
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget v12, v1, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    iput v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glEsVersion:I

    .line 271
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v12, 0x1

    iput-boolean v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasGlEsVersion:Z

    .line 273
    sget-object v12, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget-object v8, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iput v8, v12, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenWidth:I

    .line 274
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v12, 0x1

    iput-boolean v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenWidth:Z

    .line 276
    sget-object v12, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget-object v8, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    iput v8, v12, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenHeight:I

    .line 277
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v12, 0x1

    iput-boolean v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenHeight:Z

    .line 279
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget v12, v5, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenDensity:I

    .line 280
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v12, 0x1

    iput-boolean v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenDensity:Z

    .line 282
    sget-object v12, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget v8, v1, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    and-int/lit8 v8, v8, 0x1

    if-lez v8, :cond_1

    move v8, v9

    :goto_0
    iput-boolean v8, v12, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHardKeyboard:Z

    .line 284
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v12, 0x1

    iput-boolean v12, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasHardKeyboard:Z

    .line 286
    sget-object v12, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget v8, v1, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    and-int/lit8 v8, v8, 0x2

    if-lez v8, :cond_2

    move v8, v9

    :goto_1
    iput-boolean v8, v12, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasFiveWayNavigation:Z

    .line 288
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v9, 0x1

    iput-boolean v9, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasFiveWayNavigation:Z

    .line 290
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 291
    .local v2, "config":Landroid/content/res/Configuration;
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    iget v9, v2, Landroid/content/res/Configuration;->screenLayout:I

    invoke-static {v9}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->getScreenLayoutSizeId(I)I

    move-result v9

    iput v9, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenLayout:I

    .line 292
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v9, 0x1

    iput-boolean v9, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenLayout:Z

    .line 294
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/pm/PackageManager;->getSystemSharedLibraryNames()[Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    .line 297
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/finsky/FinskyApp;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    .line 299
    new-instance v8, Lcom/google/android/finsky/utils/GlExtensionReader;

    invoke-direct {v8}, Lcom/google/android/finsky/utils/GlExtensionReader;-><init>()V

    invoke-virtual {v8}, Lcom/google/android/finsky/utils/GlExtensionReader;->getGlExtensions()Ljava/util/List;

    move-result-object v4

    .line 300
    .local v4, "glExtensions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v9, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/String;

    invoke-interface {v4, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/String;

    iput-object v8, v9, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    .line 303
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    invoke-static {v3, v8}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->customizeDeviceConfiguration(Landroid/content/Context;Lcom/google/android/finsky/protos/DeviceConfigurationProto;)V

    .line 306
    :cond_0
    sget-object v8, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sDeviceConfiguration:Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v11

    return-object v8

    .end local v2    # "config":Landroid/content/res/Configuration;
    .end local v4    # "glExtensions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    move v8, v10

    .line 282
    goto :goto_0

    :cond_2
    move v8, v10

    .line 286
    goto :goto_1

    .line 247
    .end local v0    # "am":Landroid/app/ActivityManager;
    .end local v1    # "ci":Landroid/content/pm/ConfigurationInfo;
    .end local v5    # "metrics":Landroid/util/DisplayMetrics;
    .end local v6    # "screenDimensions":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    .end local v7    # "wm":Landroid/view/WindowManager;
    :catchall_0
    move-exception v8

    monitor-exit v11

    throw v8
.end method

.method private static getKeyboardConfigId(I)I
    .locals 1
    .param p0, "configValue"    # I

    .prologue
    const/4 v0, 0x0

    .line 346
    packed-switch p0, :pswitch_data_0

    .line 357
    :goto_0
    :pswitch_0
    return v0

    .line 350
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 352
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 354
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 346
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getNavigationId(I)I
    .locals 1
    .param p0, "navigationValue"    # I

    .prologue
    .line 385
    packed-switch p0, :pswitch_data_0

    .line 396
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 387
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 389
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 391
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 393
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 385
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getScreenLayoutSizeId(I)I
    .locals 2
    .param p0, "screenLayoutSizeValue"    # I

    .prologue
    .line 405
    and-int/lit8 v0, p0, 0xf

    .line 407
    .local v0, "sizeBits":I
    packed-switch v0, :pswitch_data_0

    .line 418
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 409
    :pswitch_0
    const/4 v1, 0x1

    goto :goto_0

    .line 411
    :pswitch_1
    const/4 v1, 0x2

    goto :goto_0

    .line 413
    :pswitch_2
    const/4 v1, 0x3

    goto :goto_0

    .line 415
    :pswitch_3
    const/4 v1, 0x4

    goto :goto_0

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->deviceConfigToken:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private static getTouchScreenId(I)I
    .locals 1
    .param p0, "touchScreenValue"    # I

    .prologue
    .line 367
    packed-switch p0, :pswitch_data_0

    .line 376
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 369
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 371
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 373
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static invalidateToken()V
    .locals 1

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->deviceConfigToken:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->remove()V

    .line 84
    return-void
.end method

.method private static postUploadRequest(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V
    .locals 3
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p1, "gcmOnly"    # Z
    .param p2, "listener"    # Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    .prologue
    .line 120
    new-instance v0, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;-><init>(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V

    .line 121
    .local v0, "request":Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;
    sget-object v1, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sRequests:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    sget-object v1, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->sRequests:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 124
    invoke-static {v0}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->doUploadDeviceConfiguration(Lcom/google/android/finsky/utils/DeviceConfigurationHelper$RequestRecord;)V

    .line 126
    :cond_0
    return-void
.end method

.method public static requestToken(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V
    .locals 1
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p1, "listener"    # Lcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;

    .prologue
    .line 93
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->postUploadRequest(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V

    .line 94
    return-void
.end method

.method public static uploadGcmRegistrationId(Lcom/google/android/finsky/api/DfeApi;)V
    .locals 2
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;

    .prologue
    .line 105
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/finsky/utils/DeviceConfigurationHelper;->postUploadRequest(Lcom/google/android/finsky/api/DfeApi;ZLcom/google/android/finsky/utils/DeviceConfigurationHelper$Listener;)V

    .line 106
    return-void
.end method
