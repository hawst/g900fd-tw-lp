.class public Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;
.super Ljava/lang/Object;
.source "PurchaseFragment.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PurchaseError"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final errorSubtype:I

.field public final errorType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1194
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError$1;

    invoke-direct {v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError$1;-><init>()V

    sput-object v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "errorType"    # I
    .param p2, "errorSubtype"    # I

    .prologue
    .line 1173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1174
    iput p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;->errorType:I

    .line 1175
    iput p2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;->errorSubtype:I

    .line 1176
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 1180
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1191
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PurchaseError{type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;->errorType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " subtype="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;->errorSubtype:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 1185
    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;->errorType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1186
    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseFragment$PurchaseError;->errorSubtype:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1187
    return-void
.end method
