.class public Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;
.super Landroid/widget/LinearLayout;
.source "SetupWizardIconButtonGroup.java"


# instance fields
.field private mContinueButton:Landroid/widget/Button;

.field private mContinueButtonIcon:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 34
    const v0, 0x7f0a0233

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->mContinueButton:Landroid/widget/Button;

    .line 35
    const v0, 0x7f0a0232

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    .line 36
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 50
    iget-object v1, p0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/16 v0, 0xff

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->mContinueButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 52
    return-void

    .line 50
    :cond_0
    const/16 v0, 0x7f

    goto :goto_0
.end method

.method public setIconDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->mContinueButtonIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 66
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->mContinueButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    iget-object v1, p0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->mContinueButton:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setClickable(Z)V

    .line 42
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/finsky/layout/SetupWizardIconButtonGroup;->mContinueButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 59
    return-void
.end method
