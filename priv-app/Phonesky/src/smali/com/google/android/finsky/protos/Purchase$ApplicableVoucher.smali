.class public final Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Purchase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Purchase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ApplicableVoucher"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;


# instance fields
.field public applied:Z

.field public doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public formattedDiscountAmount:Ljava/lang/String;

.field public hasApplied:Z

.field public hasFormattedDiscountAmount:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 772
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 773
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->clear()Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    .line 774
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    .locals 2

    .prologue
    .line 750
    sget-object v0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->_emptyArray:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    if-nez v0, :cond_1

    .line 751
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 753
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->_emptyArray:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    if-nez v0, :cond_0

    .line 754
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    sput-object v0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->_emptyArray:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    .line 756
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 758
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->_emptyArray:[Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    return-object v0

    .line 756
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 777
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 778
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->formattedDiscountAmount:Ljava/lang/String;

    .line 779
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->hasFormattedDiscountAmount:Z

    .line 780
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->applied:Z

    .line 781
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->hasApplied:Z

    .line 782
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->cachedSize:I

    .line 783
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 803
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 804
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v1, :cond_0

    .line 805
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 808
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->hasFormattedDiscountAmount:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->formattedDiscountAmount:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 809
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->formattedDiscountAmount:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 812
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->hasApplied:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->applied:Z

    if-eqz v1, :cond_4

    .line 813
    :cond_3
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->applied:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 816
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 824
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 825
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 829
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 830
    :sswitch_0
    return-object p0

    .line 835
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v1, :cond_1

    .line 836
    new-instance v1, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 838
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 842
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->formattedDiscountAmount:Ljava/lang/String;

    .line 843
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->hasFormattedDiscountAmount:Z

    goto :goto_0

    .line 847
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->applied:Z

    .line 848
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->hasApplied:Z

    goto :goto_0

    .line 825
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 744
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 789
    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v0, :cond_0

    .line 790
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->doc:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 792
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->hasFormattedDiscountAmount:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->formattedDiscountAmount:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 793
    :cond_1
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->formattedDiscountAmount:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 795
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->hasApplied:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->applied:Z

    if-eqz v0, :cond_4

    .line 796
    :cond_3
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Purchase$ApplicableVoucher;->applied:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 798
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 799
    return-void
.end method
