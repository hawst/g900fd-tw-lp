.class Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$5;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->configureMenu(Landroid/app/Activity;Landroid/view/Menu;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V
    .locals 0

    .prologue
    .line 506
    iput-object p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$5;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "queryTextFocused"    # Z

    .prologue
    .line 509
    if-nez p2, :cond_0

    .line 513
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$5;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # invokes: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->clearSearch()V
    invoke-static {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$100(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V

    .line 518
    :goto_0
    return-void

    .line 516
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$5;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mSearchView:Landroid/support/v7/widget/SearchView;
    invoke-static {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$700(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Landroid/support/v7/widget/SearchView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$5;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mDefaultSearchQuery:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$600(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method
