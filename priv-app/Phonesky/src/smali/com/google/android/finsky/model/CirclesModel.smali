.class public Lcom/google/android/finsky/model/CirclesModel;
.super Ljava/lang/Object;
.source "CirclesModel.java"

# interfaces
.implements Lcom/google/android/finsky/utils/GPlusUtils$CirclePickerListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;
    }
.end annotation


# instance fields
.field private mCircles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field private mCirclesModelListener:Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;

.field private mHasRequestedCircles:Z

.field private final mOwnerAccountName:Ljava/lang/String;

.field private final mTargetPersonDoc:Lcom/google/android/finsky/api/model/Document;


# direct methods
.method public constructor <init>(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)V
    .locals 0
    .param p1, "targetPerson"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "ownerAccountName"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/finsky/model/CirclesModel;->mTargetPersonDoc:Lcom/google/android/finsky/api/model/Document;

    .line 70
    iput-object p2, p0, Lcom/google/android/finsky/model/CirclesModel;->mOwnerAccountName:Ljava/lang/String;

    .line 71
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/finsky/model/CirclesModel;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/model/CirclesModel;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/android/finsky/model/CirclesModel;->mCircles:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/model/CirclesModel;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/model/CirclesModel;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/finsky/model/CirclesModel;->invokeListener()V

    return-void
.end method

.method private invokeListener()V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mCirclesModelListener:Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mCirclesModelListener:Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;

    iget-object v1, p0, Lcom/google/android/finsky/model/CirclesModel;->mCircles:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;->onCirclesUpdate(Ljava/util/List;)V

    .line 173
    :cond_0
    return-void
.end method


# virtual methods
.method public getCircles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mCircles:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getOwnerAccountName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mOwnerAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetPersonDoc()Lcom/google/android/finsky/api/model/Document;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mTargetPersonDoc:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method public launchCirclePicker(Landroid/support/v4/app/FragmentActivity;)V
    .locals 2
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mCircles:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 157
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mTargetPersonDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/model/CirclesModel;->mCircles:Ljava/util/ArrayList;

    invoke-static {p1, v0, v1, p0}, Lcom/google/android/finsky/utils/GPlusUtils;->checkGPlusAndLaunchCirclePicker(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/util/ArrayList;Lcom/google/android/finsky/utils/GPlusUtils$CirclePickerListener;)V

    goto :goto_0
.end method

.method public loadCircles(Landroid/content/Context;Lcom/google/android/gms/people/PeopleClient;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "peopleClient"    # Lcom/google/android/gms/people/PeopleClient;

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mHasRequestedCircles:Z

    if-eqz v0, :cond_0

    .line 143
    :goto_0
    return-void

    .line 117
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mHasRequestedCircles:Z

    .line 120
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mTargetPersonDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/model/CirclesModel$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/model/CirclesModel$1;-><init>(Lcom/google/android/finsky/model/CirclesModel;)V

    invoke-static {p1, p2, v0, v1}, Lcom/google/android/finsky/utils/GPlusUtils;->getCircles(Landroid/content/Context;Lcom/google/android/gms/people/PeopleClient;Ljava/lang/String;Lcom/google/android/finsky/utils/GPlusUtils$GetCirclesListener;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/finsky/model/CirclesModel;->mTargetPersonDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v0}, Lcom/google/android/finsky/api/model/Document;->getBackendDocId()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/finsky/utils/GPlusUtils;->reattachToActiveCirclePickerIfMatches(Ljava/lang/String;Lcom/google/android/finsky/utils/GPlusUtils$CirclePickerListener;)V

    goto :goto_0
.end method

.method public onCirclesSelected(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/common/people/data/AudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    .local p1, "selectedCircles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/gms/common/people/data/AudienceMember;>;"
    iput-object p1, p0, Lcom/google/android/finsky/model/CirclesModel;->mCircles:Ljava/util/ArrayList;

    .line 166
    invoke-direct {p0}, Lcom/google/android/finsky/model/CirclesModel;->invokeListener()V

    .line 167
    return-void
.end method

.method public setCirclesModelListener(Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/android/finsky/model/CirclesModel;->mCirclesModelListener:Lcom/google/android/finsky/model/CirclesModel$CirclesModelListener;

    .line 102
    return-void
.end method
