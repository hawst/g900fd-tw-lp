.class Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$1;
.super Ljava/lang/Object;
.source "DetailsSecondaryActionsSection.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$1;->this$0:Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;)V
    .locals 3
    .param p1, "plusOneResponse"    # Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$1;->this$0:Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$1;->this$0:Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;

    iget-object v1, v1, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDetailsUrl:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->invalidateDetailsCache(Ljava/lang/String;Z)V

    .line 178
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$1;->this$0:Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;

    iget-object v0, v0, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {v0}, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->onMutationOccurred(Lcom/google/android/finsky/api/DfeApi;)V

    .line 179
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 172
    check-cast p1, Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/DetailsSecondaryActionsSection$1;->onResponse(Lcom/google/android/finsky/protos/PlusOne$PlusOneResponse;)V

    return-void
.end method
