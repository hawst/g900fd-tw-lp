.class Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$3;
.super Ljava/lang/Object;
.source "FinskyDrawerLayout.java"

# interfaces
.implements Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->configure(Lcom/google/android/finsky/activities/MainActivity;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)V
    .locals 0

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$3;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackStackChanged()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$3;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$3;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    # invokes: Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->isTopLevelSideDrawerDestination()Z
    invoke-static {v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->access$000(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->setDrawerIndicatorEnabled(Z)V

    .line 198
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$3;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    # getter for: Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mRefreshHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->access$200(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout$3;->this$0:Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;

    # getter for: Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->mRefreshRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;->access$100(Lcom/google/android/finsky/layout/play/FinskyDrawerLayout;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 200
    return-void
.end method
