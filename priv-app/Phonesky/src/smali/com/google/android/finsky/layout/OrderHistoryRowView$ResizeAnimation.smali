.class Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;
.super Landroid/view/animation/Animation;
.source "OrderHistoryRowView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/OrderHistoryRowView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ResizeAnimation"
.end annotation


# instance fields
.field public startHeight:I

.field public targetHeight:I

.field public final view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 351
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 352
    iput-object p1, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->view:Landroid/view/View;

    .line 353
    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 4
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 362
    iget v1, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->startHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->targetHeight:I

    iget v3, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->startHeight:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    float-to-int v0, v1

    .line 363
    .local v0, "newHeight":I
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 364
    iget-object v1, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 365
    return-void
.end method

.method public setHeights(II)V
    .locals 0
    .param p1, "startHeight"    # I
    .param p2, "targetHeight"    # I

    .prologue
    .line 356
    iput p1, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->startHeight:I

    .line 357
    iput p2, p0, Lcom/google/android/finsky/layout/OrderHistoryRowView$ResizeAnimation;->targetHeight:I

    .line 358
    return-void
.end method
