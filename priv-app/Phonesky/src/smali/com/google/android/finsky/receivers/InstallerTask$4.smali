.class Lcom/google/android/finsky/receivers/InstallerTask$4;
.super Ljava/lang/Object;
.source "InstallerTask.java"

# interfaces
.implements Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/receivers/InstallerTask;->getInstallerListener(Landroid/net/Uri;)Lcom/google/android/finsky/installer/PackageInstallerFacade$InstallListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/receivers/InstallerTask;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/receivers/InstallerTask;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1481
    iput-object p1, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iput-object p2, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->val$uri:Landroid/net/Uri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private commonFinish(IILjava/lang/String;)V
    .locals 7
    .param p1, "logType"    # I
    .param p2, "logErrorCode"    # I
    .param p3, "logExceptionType"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 1516
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->val$uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/receivers/InstallerTask;->releaseInstalledUri(Landroid/net/Uri;)V

    .line 1519
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v2, v1, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mLogAppData:Lcom/google/android/finsky/analytics/PlayStore$AppData;
    invoke-static {v1}, Lcom/google/android/finsky/receivers/InstallerTask;->access$500(Lcom/google/android/finsky/receivers/InstallerTask;)Lcom/google/android/finsky/analytics/PlayStore$AppData;

    move-result-object v6

    move v1, p1

    move v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(ILjava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/finsky/analytics/PlayStore$AppData;)V

    .line 1523
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    const/16 v1, 0x46

    check-cast v3, Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->setInstallerState(ILjava/lang/String;)V
    invoke-static {v0, v1, v3}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1600(Lcom/google/android/finsky/receivers/InstallerTask;ILjava/lang/String;)V

    .line 1524
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->advanceState()V
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1700(Lcom/google/android/finsky/receivers/InstallerTask;)V

    .line 1525
    return-void
.end method


# virtual methods
.method public installBeginning()V
    .locals 5

    .prologue
    .line 1484
    const-string v1, "Begin install of %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v4, v4, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1485
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mShowProgress:Z
    invoke-static {v1}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1000(Lcom/google/android/finsky/receivers/InstallerTask;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1486
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v0

    .line 1487
    .local v0, "notifier":Lcom/google/android/finsky/utils/Notifier;
    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mTitle:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1100(Lcom/google/android/finsky/receivers/InstallerTask;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v2, v2, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mIsUpdate:Z
    invoke-static {v3}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1200(Lcom/google/android/finsky/receivers/InstallerTask;)Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/Notifier;->showInstallingMessage(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1489
    .end local v0    # "notifier":Lcom/google/android/finsky/utils/Notifier;
    :cond_0
    return-void
.end method

.method public installFailed(ILjava/lang/String;)V
    .locals 4
    .param p1, "errorCode"    # I
    .param p2, "exceptionType"    # Ljava/lang/String;

    .prologue
    .line 1505
    const-string v0, "Install failure of %s: %d %s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v3, v3, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1507
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mShowErrorNotifications:Z
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1400(Lcom/google/android/finsky/receivers/InstallerTask;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1508
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v1, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v1, v1, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    # invokes: Lcom/google/android/finsky/receivers/InstallerTask;->notifyFailedInstall(Ljava/lang/String;I)V
    invoke-static {v0, v1, p1}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1500(Lcom/google/android/finsky/receivers/InstallerTask;Ljava/lang/String;I)V

    .line 1510
    :cond_0
    const/16 v0, 0x6f

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/finsky/receivers/InstallerTask$4;->commonFinish(IILjava/lang/String;)V

    .line 1512
    return-void
.end method

.method public installSucceeded()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1493
    const-string v0, "Successful install of %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    iget-object v2, v2, Lcom/google/android/finsky/receivers/InstallerTask;->packageName:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1495
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mShowCompletionNotifications:Z
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1300(Lcom/google/android/finsky/receivers/InstallerTask;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    # getter for: Lcom/google/android/finsky/receivers/InstallerTask;->mIsUpdate:Z
    invoke-static {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->access$1200(Lcom/google/android/finsky/receivers/InstallerTask;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/finsky/utils/VendingPreferences;->AUTO_ADD_SHORTCUTS:Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1497
    iget-object v0, p0, Lcom/google/android/finsky/receivers/InstallerTask$4;->this$0:Lcom/google/android/finsky/receivers/InstallerTask;

    invoke-virtual {v0}, Lcom/google/android/finsky/receivers/InstallerTask;->addAppShortcut()V

    .line 1500
    :cond_0
    const/16 v0, 0x6e

    const/4 v1, 0x0

    invoke-direct {p0, v0, v3, v1}, Lcom/google/android/finsky/receivers/InstallerTask$4;->commonFinish(IILjava/lang/String;)V

    .line 1501
    return-void
.end method
