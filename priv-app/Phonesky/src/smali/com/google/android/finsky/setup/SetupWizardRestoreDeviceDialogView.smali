.class public Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;
.super Landroid/widget/LinearLayout;
.source "SetupWizardRestoreDeviceDialogView.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$ConfigurableView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;
    }
.end annotation


# instance fields
.field private mCurrentDeviceSelection:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    return-void
.end method

.method static synthetic access$002(Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;->mCurrentDeviceSelection:I

    return p1
.end method


# virtual methods
.method public configureView(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "arguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 54
    const-string v4, "SetupWizardRestoreDeviceDialogView.selectedDevicePosition"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;->mCurrentDeviceSelection:I

    .line 56
    const-string v4, "SetupWizardRestoreDeviceDialogView.selectedDevices"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 57
    .local v3, "parcelableBackupDeviceInfos":[Landroid/os/Parcelable;
    array-length v4, v3

    new-array v0, v4, [Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    .line 59
    .local v0, "backupDeviceInfos":[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 60
    aget-object v4, v3, v1

    check-cast v4, Lcom/google/android/finsky/utils/ParcelableProto;

    invoke-virtual {v4}, Lcom/google/android/finsky/utils/ParcelableProto;->getPayload()Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    aput-object v4, v0, v1

    .line 59
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 64
    :cond_0
    const v4, 0x7f0a039a

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    .line 66
    .local v2, "listView":Landroid/widget/ListView;
    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 67
    new-instance v4, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$BackupDeviceInfoArrayAdapter;-><init>(Landroid/content/Context;[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;)V

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 68
    iget v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;->mCurrentDeviceSelection:I

    invoke-virtual {v2, v4, v6}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 69
    new-instance v4, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$1;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView$1;-><init>(Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;)V

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 74
    return-void
.end method

.method public getResult()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 97
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 98
    .local v0, "result":Landroid/os/Bundle;
    const-string v1, "SetupWizardRestoreDeviceDialogView.selectedDevicePosition"

    iget v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;->mCurrentDeviceSelection:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    return-object v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 86
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 87
    check-cast v0, Landroid/os/Bundle;

    .line 88
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "SetupWizardRestoreDeviceDialogView.selectedDevicePosition"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;->mCurrentDeviceSelection:I

    .line 89
    const-string v1, "SetupWizardRestoreDeviceDialogView.parent_instance_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 93
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 92
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 78
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 79
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "SetupWizardRestoreDeviceDialogView.parent_instance_state"

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 80
    const-string v1, "SetupWizardRestoreDeviceDialogView.selectedDevicePosition"

    iget v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreDeviceDialogView;->mCurrentDeviceSelection:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    return-object v0
.end method
