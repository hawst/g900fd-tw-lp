.class public Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;
.source "SetupWizardRedeemCodeActivity.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;


# instance fields
.field private mSetupWizardParam:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;-><init>()V

    return-void
.end method

.method public static createIntent(Ljava/lang/String;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;)Landroid/content/Intent;
    .locals 8
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "params"    # Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .prologue
    const/4 v4, 0x0

    .line 45
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .local v0, "intent":Landroid/content/Intent;
    const/4 v2, 0x4

    const/4 v3, -0x1

    const/4 v5, 0x0

    move-object v1, p0

    move-object v6, v4

    move-object v7, v4

    invoke-static/range {v0 .. v7}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->putIntentExtras(Landroid/content/Intent;Ljava/lang/String;IILcom/google/android/finsky/protos/Common$Docid;ILjava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v1, "setup_wizard_params"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 50
    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->finish()V

    .line 113
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/SetupWizardUtils;->animateSliding(Landroid/app/Activity;Z)V

    .line 114
    return-void
.end method

.method protected getActivityLayout()I
    .locals 1

    .prologue
    .line 70
    const v0, 0x7f0401a5

    return v0
.end method

.method protected getBillingUiMode()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 118
    const/16 v0, 0x382

    return v0
.end method

.method protected handleMessageAndFinish(Ljava/lang/String;)V
    .locals 8
    .param p1, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 88
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->mSetupWizardParam:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->hasSetupCompleteScreen()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    new-instance v6, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 90
    .local v6, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v0, p0}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v5

    .line 91
    .local v5, "account":Landroid/accounts/Account;
    invoke-virtual {v6, p1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c02a0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x4c4

    const/16 v3, 0x4c5

    const/4 v4, -0x1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCanceledOnTouchOutside(Z)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 99
    invoke-virtual {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v7

    .line 100
    .local v7, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "redeemed_promo_offer"

    invoke-virtual {v7, v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 105
    .end local v5    # "account":Landroid/accounts/Account;
    .end local v6    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v7    # "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 55
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    .line 56
    .local v6, "intent":Landroid/content/Intent;
    const-string v0, "setup_wizard_params"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->mSetupWizardParam:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .line 58
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->mSetupWizardParam:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isLightTheme()Z

    move-result v0

    if-eqz v0, :cond_0

    const v7, 0x7f0d01bf

    .line 60
    .local v7, "themeId":I
    :goto_0
    invoke-virtual {p0, v7}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->setTheme(I)V

    .line 62
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 63
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->mTitleView:Landroid/widget/TextView;

    .line 64
    iget-object v1, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->mSetupWizardParam:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    const/4 v2, 0x0

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/SetupWizardUtils;->configureBasicUi(Landroid/app/Activity;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;IZZZ)V

    .line 66
    return-void

    .line 58
    .end local v7    # "themeId":I
    :cond_0
    const v7, 0x7f0d01be

    goto :goto_0
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 132
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 125
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->finish()V

    .line 128
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/SetupWizardRedeemCodeActivity;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    return-void
.end method
