.class Lcom/google/android/finsky/billing/PaymentMethodsFragment$3;
.super Ljava/lang/Object;
.source "PaymentMethodsFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/billing/PaymentMethodsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/billing/PaymentMethodsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/billing/PaymentMethodsFragment;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment$3;->this$0:Lcom/google/android/finsky/billing/PaymentMethodsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 126
    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment$3;->this$0:Lcom/google/android/finsky/billing/PaymentMethodsFragment;

    iget-object v1, v1, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->mEventLog:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v2, 0xa3f

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment$3;->this$0:Lcom/google/android/finsky/billing/PaymentMethodsFragment;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 129
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    # getter for: Lcom/google/android/finsky/billing/PaymentMethodsFragment;->EDIT_FOOTER_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->access$200()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 130
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/billing/PaymentMethodsFragment$3;->this$0:Lcom/google/android/finsky/billing/PaymentMethodsFragment;

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/PaymentMethodsFragment;->startActivity(Landroid/content/Intent;)V

    .line 131
    return-void
.end method
