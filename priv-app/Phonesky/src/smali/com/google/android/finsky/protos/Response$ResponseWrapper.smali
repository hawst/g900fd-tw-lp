.class public final Lcom/google/android/finsky/protos/Response$ResponseWrapper;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResponseWrapper"
.end annotation


# instance fields
.field public commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

.field public notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

.field public payload:Lcom/google/android/finsky/protos/Response$Payload;

.field public preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

.field public serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

.field public targets:Lcom/google/android/finsky/protos/Targeting$Targets;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->clear()Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    .line 45
    return-void
.end method

.method public static parseFrom([B)Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    .locals 1
    .param p0, "data"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    .line 221
    new-instance v0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    invoke-direct {v0}, Lcom/google/android/finsky/protos/Response$ResponseWrapper;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[B)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    return-object v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 48
    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->payload:Lcom/google/android/finsky/protos/Response$Payload;

    .line 49
    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    .line 50
    invoke-static {}, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;->emptyArray()[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    .line 51
    invoke-static {}, Lcom/google/android/finsky/protos/Notifications$Notification;->emptyArray()[Lcom/google/android/finsky/protos/Notifications$Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    .line 52
    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    .line 53
    iput-object v1, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->targets:Lcom/google/android/finsky/protos/Targeting$Targets;

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->cachedSize:I

    .line 55
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 94
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 95
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->payload:Lcom/google/android/finsky/protos/Response$Payload;

    if-eqz v3, :cond_0

    .line 96
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->payload:Lcom/google/android/finsky/protos/Response$Payload;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 99
    :cond_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    if-eqz v3, :cond_1

    .line 100
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 103
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v3, v3

    if-lez v3, :cond_3

    .line 104
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v3, v3

    if-ge v1, v3, :cond_3

    .line 105
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    aget-object v0, v3, v1

    .line 106
    .local v0, "element":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    if-eqz v0, :cond_2

    .line 107
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 104
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 112
    .end local v0    # "element":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    .end local v1    # "i":I
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 113
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 114
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    aget-object v0, v3, v1

    .line 115
    .local v0, "element":Lcom/google/android/finsky/protos/Notifications$Notification;
    if-eqz v0, :cond_4

    .line 116
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 113
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 121
    .end local v0    # "element":Lcom/google/android/finsky/protos/Notifications$Notification;
    .end local v1    # "i":I
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    if-eqz v3, :cond_6

    .line 122
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 125
    :cond_6
    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->targets:Lcom/google/android/finsky/protos/Targeting$Targets;

    if-eqz v3, :cond_7

    .line 126
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->targets:Lcom/google/android/finsky/protos/Targeting$Targets;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 129
    :cond_7
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Response$ResponseWrapper;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 137
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 138
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 142
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 143
    :sswitch_0
    return-object p0

    .line 148
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->payload:Lcom/google/android/finsky/protos/Response$Payload;

    if-nez v5, :cond_1

    .line 149
    new-instance v5, Lcom/google/android/finsky/protos/Response$Payload;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Response$Payload;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->payload:Lcom/google/android/finsky/protos/Response$Payload;

    .line 151
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->payload:Lcom/google/android/finsky/protos/Response$Payload;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 155
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    if-nez v5, :cond_2

    .line 156
    new-instance v5, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    .line 158
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 162
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 164
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    if-nez v5, :cond_4

    move v1, v4

    .line 165
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    .line 167
    .local v2, "newArray":[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    if-eqz v1, :cond_3

    .line 168
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 170
    :cond_3
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 171
    new-instance v5, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;-><init>()V

    aput-object v5, v2, v1

    .line 172
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 173
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 170
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 164
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v1, v5

    goto :goto_1

    .line 176
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    :cond_5
    new-instance v5, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;-><init>()V

    aput-object v5, v2, v1

    .line 177
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 178
    iput-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    goto :goto_0

    .line 182
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    :sswitch_4
    const/16 v5, 0x22

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 184
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    if-nez v5, :cond_7

    move v1, v4

    .line 185
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Notifications$Notification;

    .line 187
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Notifications$Notification;
    if-eqz v1, :cond_6

    .line 188
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 190
    :cond_6
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_8

    .line 191
    new-instance v5, Lcom/google/android/finsky/protos/Notifications$Notification;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Notifications$Notification;-><init>()V

    aput-object v5, v2, v1

    .line 192
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 193
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 190
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 184
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Notifications$Notification;
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v1, v5

    goto :goto_3

    .line 196
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Notifications$Notification;
    :cond_8
    new-instance v5, Lcom/google/android/finsky/protos/Notifications$Notification;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Notifications$Notification;-><init>()V

    aput-object v5, v2, v1

    .line 197
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 198
    iput-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    goto/16 :goto_0

    .line 202
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Notifications$Notification;
    :sswitch_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    if-nez v5, :cond_9

    .line 203
    new-instance v5, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    .line 205
    :cond_9
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 209
    :sswitch_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->targets:Lcom/google/android/finsky/protos/Targeting$Targets;

    if-nez v5, :cond_a

    .line 210
    new-instance v5, Lcom/google/android/finsky/protos/Targeting$Targets;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Targeting$Targets;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->targets:Lcom/google/android/finsky/protos/Targeting$Targets;

    .line 212
    :cond_a
    iget-object v5, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->targets:Lcom/google/android/finsky/protos/Targeting$Targets;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 138
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Response$ResponseWrapper;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->payload:Lcom/google/android/finsky/protos/Response$Payload;

    if-eqz v2, :cond_0

    .line 62
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->payload:Lcom/google/android/finsky/protos/Response$Payload;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 64
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    if-eqz v2, :cond_1

    .line 65
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->commands:Lcom/google/android/finsky/protos/ResponseMessages$ServerCommands;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 67
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 68
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 69
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->preFetch:[Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;

    aget-object v0, v2, v1

    .line 70
    .local v0, "element":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    if-eqz v0, :cond_2

    .line 71
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 68
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 75
    .end local v0    # "element":Lcom/google/android/finsky/protos/ResponseMessages$PreFetch;
    .end local v1    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 76
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 77
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->notification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    aget-object v0, v2, v1

    .line 78
    .local v0, "element":Lcom/google/android/finsky/protos/Notifications$Notification;
    if-eqz v0, :cond_4

    .line 79
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 76
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 83
    .end local v0    # "element":Lcom/google/android/finsky/protos/Notifications$Notification;
    .end local v1    # "i":I
    :cond_5
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    if-eqz v2, :cond_6

    .line 84
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->serverMetadata:Lcom/google/android/finsky/protos/ResponseMessages$ServerMetadata;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 86
    :cond_6
    iget-object v2, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->targets:Lcom/google/android/finsky/protos/Targeting$Targets;

    if-eqz v2, :cond_7

    .line 87
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Response$ResponseWrapper;->targets:Lcom/google/android/finsky/protos/Targeting$Targets;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 89
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 90
    return-void
.end method
