.class final Lcom/google/android/finsky/utils/BadgeUtils$1;
.super Ljava/lang/Object;
.source "BadgeUtils.java"

# interfaces
.implements Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/BadgeUtils;->configureContentRatingBadge(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Landroid/widget/ImageView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ratingView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>(Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/finsky/utils/BadgeUtils$1;->val$ratingView:Landroid/widget/ImageView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 1
    .param p1, "result"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/finsky/utils/BadgeUtils$1;->val$ratingView:Landroid/widget/ImageView;

    # invokes: Lcom/google/android/finsky/utils/BadgeUtils;->bitmapLoaded(Landroid/widget/ImageView;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/BadgeUtils;->access$000(Landroid/widget/ImageView;Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    .line 64
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 60
    check-cast p1, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/BadgeUtils$1;->onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    return-void
.end method
