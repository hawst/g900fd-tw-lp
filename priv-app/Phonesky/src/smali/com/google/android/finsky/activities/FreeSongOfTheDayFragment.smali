.class public Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;
.super Lcom/google/android/finsky/activities/DetailsDataBasedFragment;
.source "FreeSongOfTheDayFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;)Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;
    .locals 2
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;-><init>()V

    .line 24
    .local v0, "fragment":Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getToc()Lcom/google/android/finsky/api/model/DfeToc;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->setDfeTocAndUrl(Lcom/google/android/finsky/api/model/DfeToc;Ljava/lang/String;)V

    .line 25
    invoke-virtual {v0, p0}, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->setInitialDocument(Lcom/google/android/finsky/api/model/Document;)V

    .line 26
    return-object v0
.end method


# virtual methods
.method protected getLayoutRes()I
    .locals 1

    .prologue
    .line 51
    const v0, 0x7f0400a5

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 94
    const/16 v0, 0x8

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/google/android/finsky/activities/DetailsDataBasedFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 33
    iget-object v0, p0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-interface {v0}, Lcom/google/android/finsky/fragments/PageFragmentHost;->getActionBarController()Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->disableActionBarOverlay()V

    .line 34
    return-void
.end method

.method protected onInitViewBinders()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method public rebindActionBar()V
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateBreadcrumb(Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->mPageFragmentHost:Lcom/google/android/finsky/fragments/PageFragmentHost;

    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/fragments/PageFragmentHost;->updateCurrentBackendId(IZ)V

    .line 46
    :cond_0
    return-void
.end method

.method protected rebindViews(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->rebindActionBar()V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->getDocument()Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    .line 58
    .local v3, "doc":Lcom/google/android/finsky/api/model/Document;
    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v7

    .line 59
    .local v7, "songDoc":Lcom/google/android/finsky/api/model/Document;
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDealOfTheDayInfo()Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;

    move-result-object v1

    .line 61
    .local v1, "dealOfTheDayInfo":Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->getView()Landroid/view/View;

    move-result-object v4

    .line 63
    .local v4, "fragmentView":Landroid/view/View;
    const v9, 0x7f0a0173

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 64
    .local v5, "header":Landroid/widget/TextView;
    iget-object v9, v1, Lcom/google/android/finsky/protos/DocumentV2$DealOfTheDay;->featuredHeader:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v9, p0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v10

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getSecondaryTextColor(Landroid/content/Context;I)Landroid/content/res/ColorStateList;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 67
    const v9, 0x7f0a0202

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 68
    .local v6, "headerSeparator":Landroid/view/View;
    iget-object v9, p0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v10

    invoke-static {v9, v10}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v9

    invoke-virtual {v6, v9}, Landroid/view/View;->setBackgroundColor(I)V

    .line 71
    const v9, 0x7f0a0203

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/google/android/finsky/layout/FreeSongOfTheDaySummary;

    .line 73
    .local v8, "summary":Lcom/google/android/finsky/layout/FreeSongOfTheDaySummary;
    iget-object v9, p0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-virtual {v8, v7, v9, p0}, Lcom/google/android/finsky/layout/FreeSongOfTheDaySummary;->showSummary(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 75
    const v9, 0x7f0a00bb

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 76
    .local v2, "description":Landroid/widget/TextView;
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDescription()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    const v9, 0x7f0a0204

    invoke-virtual {v4, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;

    .line 80
    .local v0, "albumPanel":Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;
    if-eqz v0, :cond_0

    .line 81
    iget-object v9, p0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v10, p0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    iget-object v11, p0, Lcom/google/android/finsky/activities/FreeSongOfTheDayFragment;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    invoke-virtual {v0, v9, v10, v11}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->init(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;)V

    .line 82
    invoke-virtual {v7}, Lcom/google/android/finsky/api/model/Document;->getDetailsUrl()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v3, v9, p0}, Lcom/google/android/finsky/layout/FreeSongOfTheDayAlbumView;->bind(Lcom/google/android/finsky/api/model/Document;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 84
    :cond_0
    return-void
.end method

.method protected recordState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    return-void
.end method
