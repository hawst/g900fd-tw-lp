.class public Lcom/google/android/finsky/billing/BillingUtils;
.super Ljava/lang/Object;
.source "BillingUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/BillingUtils$AddressMode;
    }
.end annotation


# static fields
.field private static final PATTERN_EM_TAG:Ljava/util/regex/Pattern;

.field private static final PATTERN_END_EM_TAG:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-string v0, "<em>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/billing/BillingUtils;->PATTERN_EM_TAG:Ljava/util/regex/Pattern;

    .line 69
    const-string v0, "</em>"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/billing/BillingUtils;->PATTERN_END_EM_TAG:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static addressDataFromInstrumentAddress(Lcom/google/android/finsky/protos/BillingAddress$Address;)Lcom/android/i18n/addressinput/AddressData;
    .locals 2
    .param p0, "address"    # Lcom/google/android/finsky/protos/BillingAddress$Address;

    .prologue
    .line 273
    new-instance v0, Lcom/android/i18n/addressinput/AddressData$Builder;

    invoke-direct {v0}, Lcom/android/i18n/addressinput/AddressData$Builder;-><init>()V

    .line 275
    .local v0, "builder":Lcom/android/i18n/addressinput/AddressData$Builder;
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 276
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setCountry(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 278
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 279
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setAddressLine1(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 281
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine2:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 282
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setAddressLine2(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 284
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->state:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 285
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setAdminArea(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 287
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 288
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setLocality(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 290
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->dependentLocality:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 291
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->dependentLocality:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setDependentLocality(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 293
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 294
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setPostalCode(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 296
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->sortingCode:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 297
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->sortingCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setSortingCode(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 299
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 300
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setRecipient(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 302
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->languageCode:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 303
    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->languageCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/AddressData$Builder;->setLanguageCode(Ljava/lang/String;)Lcom/android/i18n/addressinput/AddressData$Builder;

    .line 305
    :cond_9
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/AddressData$Builder;->build()Lcom/android/i18n/addressinput/AddressData;

    move-result-object v1

    return-object v1
.end method

.method public static findCountry(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .locals 3
    .param p0, "isoCode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;",
            ">;)",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;"
        }
    .end annotation

    .prologue
    .line 236
    .local p1, "countries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;>;"
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 237
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    .line 238
    .local v0, "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    iget-object v2, v0, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryCode:Ljava/lang/String;

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 246
    .end local v0    # "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    .end local v1    # "i$":Ljava/util/Iterator;
    :goto_0
    return-object v0

    .line 243
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 244
    const/4 v2, 0x0

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    move-object v0, v2

    goto :goto_0

    .line 246
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getAddressFormOptions(Lcom/google/android/finsky/billing/BillingUtils$AddressMode;)Lcom/android/i18n/addressinput/FormOptions;
    .locals 3
    .param p0, "mode"    # Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    .prologue
    .line 256
    new-instance v1, Lcom/android/i18n/addressinput/FormOptions$Builder;

    invoke-direct {v1}, Lcom/android/i18n/addressinput/FormOptions$Builder;-><init>()V

    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->COUNTRY:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    move-result-object v1

    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->RECIPIENT:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    move-result-object v1

    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->ORGANIZATION:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    move-result-object v1

    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->DEPENDENT_LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    move-result-object v1

    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->SORTING_CODE:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    move-result-object v0

    .line 259
    .local v0, "builder":Lcom/android/i18n/addressinput/FormOptions$Builder;
    sget-object v1, Lcom/google/android/finsky/billing/BillingUtils$AddressMode;->REDUCED_ADDRESS:Lcom/google/android/finsky/billing/BillingUtils$AddressMode;

    if-ne p0, v1, :cond_0

    .line 260
    sget-object v1, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_1:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    move-result-object v1

    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->ADDRESS_LINE_2:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    .line 261
    sget-object v1, Lcom/android/i18n/addressinput/AddressField;->LOCALITY:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    sget-object v1, Lcom/android/i18n/addressinput/AddressField;->ADMIN_AREA:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v0, v1}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    move-result-object v1

    sget-object v2, Lcom/android/i18n/addressinput/AddressField;->STREET_ADDRESS:Lcom/android/i18n/addressinput/AddressField;

    invoke-virtual {v1, v2}, Lcom/android/i18n/addressinput/FormOptions$Builder;->hide(Lcom/android/i18n/addressinput/AddressField;)Lcom/android/i18n/addressinput/FormOptions$Builder;

    .line 264
    :cond_0
    invoke-virtual {v0}, Lcom/android/i18n/addressinput/FormOptions$Builder;->build()Lcom/android/i18n/addressinput/FormOptions;

    move-result-object v1

    return-object v1
.end method

.method public static getDefaultCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "country"    # Ljava/lang/String;

    .prologue
    .line 100
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-static {p0}, Lcom/google/android/finsky/billing/BillingUtils;->getSimCountry(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p1

    .line 105
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    const-string p1, "US"

    .line 109
    :cond_1
    return-object p1
.end method

.method public static getFopVersion(Lcom/google/android/finsky/protos/CommonDevice$Instrument;)I
    .locals 4
    .param p0, "instrument"    # Lcom/google/android/finsky/protos/CommonDevice$Instrument;

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x0

    .line 397
    iget v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->instrumentFamily:I

    if-ne v3, v1, :cond_2

    .line 398
    iget-object v3, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    if-eqz v3, :cond_1

    .line 399
    iget-object v0, p0, Lcom/google/android/finsky/protos/CommonDevice$Instrument;->carrierBillingStatus:Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;

    .line 400
    .local v0, "status":Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    iget-boolean v3, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->hasApiVersion:Z

    if-eqz v3, :cond_1

    .line 401
    iget v1, v0, Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;->apiVersion:I

    .line 411
    .end local v0    # "status":Lcom/google/android/finsky/protos/CommonDevice$CarrierBillingInstrumentStatus;
    :cond_0
    :goto_0
    return v1

    .line 405
    :cond_1
    sget-boolean v3, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 406
    const-string v3, "No api version in CarrierBillingInstrumentStatus. Return DCB_VERSION_2"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v3, v2}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 411
    goto :goto_0
.end method

.method public static getLine1Number(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 204
    const-string v4, "phone"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/telephony/TelephonyManager;

    .line 206
    .local v3, "telephonyManager":Landroid/telephony/TelephonyManager;
    const/4 v1, 0x1

    .line 208
    .local v1, "isVoiceCapable":Z
    :try_start_0
    const-class v0, Landroid/telephony/TelephonyManager;

    .line 209
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/telephony/TelephonyManager;>;"
    const-string v4, "isVoiceCapable"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Class;

    invoke-virtual {v0, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 210
    .local v2, "method":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 214
    .end local v0    # "cls":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/telephony/TelephonyManager;>;"
    .end local v2    # "method":Ljava/lang/reflect/Method;
    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v4

    :goto_1
    return-object v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_1

    .line 211
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method public static getRiskHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 224
    sget-object v1, Lcom/google/android/finsky/api/DfeApiConfig;->androidId:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 225
    .local v0, "androidId":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/finsky/utils/Sha1Util;->secureHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getSimCountry(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 117
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 119
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "iso":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getTopMostView(Landroid/view/ViewGroup;Ljava/util/Collection;)Landroid/view/View;
    .locals 5
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/Collection",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 183
    .local p1, "views":Ljava/util/Collection;, "Ljava/util/Collection<TT;>;"
    const/4 v1, 0x0

    .line 184
    .local v1, "topMost":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;TT;>;"
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 185
    .local v2, "view":Landroid/view/View;, "TT;"
    invoke-static {p0, v2}, Lcom/google/android/finsky/billing/BillingUtils;->getViewOffsetToChild(Landroid/view/ViewGroup;Landroid/view/View;)I

    move-result v3

    .line 186
    .local v3, "yOffset":I
    if-eqz v1, :cond_1

    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 187
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    goto :goto_0

    .line 190
    .end local v2    # "view":Landroid/view/View;, "TT;"
    .end local v3    # "yOffset":I
    :cond_2
    if-eqz v1, :cond_3

    .line 191
    iget-object v4, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Landroid/view/View;

    .line 193
    :goto_1
    return-object v4

    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static getViewOffsetToChild(Landroid/view/ViewGroup;Landroid/view/View;)I
    .locals 2
    .param p0, "parent"    # Landroid/view/ViewGroup;
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 170
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 171
    .local v0, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0, p1, v0}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 172
    iget v1, v0, Landroid/graphics/Rect;->top:I

    return v1
.end method

.method public static instrumentAddressFromAddressData(Lcom/android/i18n/addressinput/AddressData;[I)Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 4
    .param p0, "addressData"    # Lcom/android/i18n/addressinput/AddressData;
    .param p1, "requiredFields"    # [I

    .prologue
    const/4 v3, 0x1

    .line 317
    new-instance v1, Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/BillingAddress$Address;-><init>()V

    .line 318
    .local v1, "result":Lcom/google/android/finsky/protos/BillingAddress$Address;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 319
    aget v2, p1, v0

    packed-switch v2, :pswitch_data_0

    .line 318
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 321
    :pswitch_0
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getPostalCountry()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 322
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getPostalCountry()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    .line 323
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCountry:Z

    goto :goto_1

    .line 327
    :pswitch_1
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getAddressLine1()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 328
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getAddressLine1()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    .line 329
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine1:Z

    goto :goto_1

    .line 333
    :pswitch_2
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getAddressLine2()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 334
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getAddressLine2()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine2:Ljava/lang/String;

    .line 335
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine2:Z

    goto :goto_1

    .line 339
    :pswitch_3
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getAdministrativeArea()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 340
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getAdministrativeArea()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->state:Ljava/lang/String;

    .line 341
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasState:Z

    goto :goto_1

    .line 345
    :pswitch_4
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getLocality()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 346
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getLocality()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    .line 347
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasCity:Z

    goto :goto_1

    .line 351
    :pswitch_5
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getDependentLocality()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 352
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getDependentLocality()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->dependentLocality:Ljava/lang/String;

    .line 353
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDependentLocality:Z

    goto :goto_1

    .line 357
    :pswitch_6
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getPostalCode()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 358
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getPostalCode()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    .line 359
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCode:Z

    goto :goto_1

    .line 364
    :pswitch_7
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getRecipient()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 365
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getRecipient()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    .line 366
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasName:Z

    goto :goto_1

    .line 374
    :cond_1
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getSortingCode()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 375
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getSortingCode()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->sortingCode:Ljava/lang/String;

    .line 376
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasSortingCode:Z

    .line 378
    :cond_2
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getLanguageCode()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 379
    invoke-virtual {p0}, Lcom/android/i18n/addressinput/AddressData;->getLanguageCode()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->languageCode:Ljava/lang/String;

    .line 380
    iput-boolean v3, v1, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLanguageCode:Z

    .line 382
    :cond_3
    return-object v1

    .line 319
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_7
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public static parseHtmlAndColorizeEm(Ljava/lang/String;I)Landroid/text/Spanned;
    .locals 1
    .param p0, "html"    # Ljava/lang/String;
    .param p1, "highlightColor"    # I

    .prologue
    .line 79
    invoke-static {p0, p1}, Lcom/google/android/finsky/billing/BillingUtils;->parseHtmlAndColorizeEmAsString(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method static parseHtmlAndColorizeEmAsString(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p0, "html"    # Ljava/lang/String;
    .param p1, "highlightColor"    # I

    .prologue
    .line 84
    const-string v1, "#%06X"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const v4, 0xffffff

    and-int/2addr v4, p1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 85
    .local v0, "hexColor":Ljava/lang/String;
    sget-object v1, Lcom/google/android/finsky/billing/BillingUtils;->PATTERN_EM_TAG:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<b><font color=\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\">"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 86
    sget-object v1, Lcom/google/android/finsky/billing/BillingUtils;->PATTERN_END_EM_TAG:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    const-string v2, "</font></b>"

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 87
    return-object p0
.end method

.method public static replaceLanguageAndRegion(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 151
    const-string v1, "%lang%"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "%region%"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 152
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 153
    .local v0, "locale":Ljava/util/Locale;
    const-string v1, "%lang%"

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 154
    const-string v1, "%region%"

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 156
    .end local v0    # "locale":Ljava/util/Locale;
    :cond_1
    return-object p0
.end method

.method public static replaceLocale(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 133
    const-string v2, "%locale%"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 135
    .local v0, "locale":Ljava/util/Locale;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "tmp":Ljava/lang/String;
    const-string v2, "%locale%"

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 138
    .end local v0    # "locale":Ljava/util/Locale;
    .end local v1    # "tmp":Ljava/lang/String;
    :cond_0
    return-object p0
.end method
