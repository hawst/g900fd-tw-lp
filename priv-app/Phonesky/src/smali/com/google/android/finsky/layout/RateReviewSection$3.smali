.class Lcom/google/android/finsky/layout/RateReviewSection$3;
.super Ljava/lang/Object;
.source "RateReviewSection.java"

# interfaces
.implements Lcom/google/android/finsky/utils/RateReviewHelper$CheckAndConfirmGPlusListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/RateReviewSection;->launchReviewsDialog(Landroid/support/v4/app/Fragment;Lcom/google/android/finsky/protos/DocumentV2$Review;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/RateReviewSection;

.field final synthetic val$prefilledRating:I

.field final synthetic val$review:Lcom/google/android/finsky/protos/DocumentV2$Review;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/RateReviewSection;Lcom/google/android/finsky/protos/DocumentV2$Review;I)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/finsky/layout/RateReviewSection$3;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    iput-object p2, p0, Lcom/google/android/finsky/layout/RateReviewSection$3;->val$review:Lcom/google/android/finsky/protos/DocumentV2$Review;

    iput p3, p0, Lcom/google/android/finsky/layout/RateReviewSection$3;->val$prefilledRating:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckAndConfirmGPlusFailed()V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection$3;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/RateReviewSection;->refresh()V

    .line 180
    return-void
.end method

.method public onCheckAndConfirmGPlusPassed(Lcom/google/android/finsky/api/model/Document;)V
    .locals 7
    .param p1, "plusDoc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 170
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/layout/RateReviewSection$3;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    # getter for: Lcom/google/android/finsky/layout/RateReviewSection;->mDocument:Lcom/google/android/finsky/api/model/Document;
    invoke-static {v1}, Lcom/google/android/finsky/layout/RateReviewSection;->access$300(Lcom/google/android/finsky/layout/RateReviewSection;)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/finsky/layout/RateReviewSection$3;->val$review:Lcom/google/android/finsky/protos/DocumentV2$Review;

    iget v4, p0, Lcom/google/android/finsky/layout/RateReviewSection$3;->val$prefilledRating:I

    const/4 v5, 0x0

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/activities/RateReviewActivity;->createIntent(Ljava/lang/String;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/protos/DocumentV2$Review;IZ)Landroid/content/Intent;

    move-result-object v6

    .line 173
    .local v6, "rateReviewIntent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/finsky/layout/RateReviewSection$3;->this$0:Lcom/google/android/finsky/layout/RateReviewSection;

    # getter for: Lcom/google/android/finsky/layout/RateReviewSection;->mFragment:Landroid/support/v4/app/Fragment;
    invoke-static {v0}, Lcom/google/android/finsky/layout/RateReviewSection;->access$400(Lcom/google/android/finsky/layout/RateReviewSection;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    const/16 v1, 0x2b

    invoke-virtual {v0, v6, v1}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 175
    return-void
.end method
