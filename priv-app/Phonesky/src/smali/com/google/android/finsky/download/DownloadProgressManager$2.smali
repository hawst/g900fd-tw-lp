.class Lcom/google/android/finsky/download/DownloadProgressManager$2;
.super Ljava/lang/Object;
.source "DownloadProgressManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/download/DownloadProgressManager;->cleanup()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/download/DownloadProgressManager;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/download/DownloadProgressManager;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadProgressManager$2;->this$0:Lcom/google/android/finsky/download/DownloadProgressManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager$2;->this$0:Lcom/google/android/finsky/download/DownloadProgressManager;

    # getter for: Lcom/google/android/finsky/download/DownloadProgressManager;->mDownloadManager:Lcom/google/android/finsky/download/DownloadManagerFacade;
    invoke-static {v0}, Lcom/google/android/finsky/download/DownloadProgressManager;->access$100(Lcom/google/android/finsky/download/DownloadProgressManager;)Lcom/google/android/finsky/download/DownloadManagerFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/download/DownloadProgressManager$2;->this$0:Lcom/google/android/finsky/download/DownloadProgressManager;

    invoke-interface {v0, v1}, Lcom/google/android/finsky/download/DownloadManagerFacade;->unregisterListener(Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/finsky/download/DownloadProgressManager$2;->this$0:Lcom/google/android/finsky/download/DownloadProgressManager;

    # getter for: Lcom/google/android/finsky/download/DownloadProgressManager;->mHandlerThread:Landroid/os/HandlerThread;
    invoke-static {v0}, Lcom/google/android/finsky/download/DownloadProgressManager;->access$200(Lcom/google/android/finsky/download/DownloadProgressManager;)Landroid/os/HandlerThread;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 77
    return-void
.end method
