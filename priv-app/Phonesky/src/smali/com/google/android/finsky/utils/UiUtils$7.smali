.class final Lcom/google/android/finsky/utils/UiUtils$7;
.super Ljava/lang/Object;
.source "UiUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/UiUtils;->syncActionBarAlpha(Lcom/google/android/finsky/layout/ObservableScrollView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

.field final synthetic val$heroTopPadding:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;I)V
    .locals 0

    .prologue
    .line 528
    iput-object p1, p0, Lcom/google/android/finsky/utils/UiUtils$7;->val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    iput p2, p0, Lcom/google/android/finsky/utils/UiUtils$7;->val$heroTopPadding:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 531
    iget-object v0, p0, Lcom/google/android/finsky/utils/UiUtils$7;->val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    iget v1, p0, Lcom/google/android/finsky/utils/UiUtils$7;->val$heroTopPadding:I

    invoke-interface {v0, v1}, Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;->setOverlayableImageTopPadding(I)V

    .line 533
    # getter for: Lcom/google/android/finsky/utils/UiUtils;->sPendingPaddingRequests:Ljava/util/Map;
    invoke-static {}, Lcom/google/android/finsky/utils/UiUtils;->access$100()Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/utils/UiUtils$7;->val$heroImageHost:Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    return-void
.end method
