.class public final Lcom/google/android/finsky/protos/Buy$LineItem;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Buy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Buy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LineItem"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/Buy$LineItem;


# instance fields
.field public amount:Lcom/google/android/finsky/protos/Buy$Money;

.field public description:Ljava/lang/String;

.field public hasDescription:Z

.field public hasName:Z

.field public name:Ljava/lang/String;

.field public offer:Lcom/google/android/finsky/protos/Common$Offer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1738
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1739
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Buy$LineItem;->clear()Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 1740
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/Buy$LineItem;
    .locals 2

    .prologue
    .line 1713
    sget-object v0, Lcom/google/android/finsky/protos/Buy$LineItem;->_emptyArray:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-nez v0, :cond_1

    .line 1714
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1716
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/Buy$LineItem;->_emptyArray:[Lcom/google/android/finsky/protos/Buy$LineItem;

    if-nez v0, :cond_0

    .line 1717
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/Buy$LineItem;

    sput-object v0, Lcom/google/android/finsky/protos/Buy$LineItem;->_emptyArray:[Lcom/google/android/finsky/protos/Buy$LineItem;

    .line 1719
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1721
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/Buy$LineItem;->_emptyArray:[Lcom/google/android/finsky/protos/Buy$LineItem;

    return-object v0

    .line 1719
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Buy$LineItem;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1743
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->name:Ljava/lang/String;

    .line 1744
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->hasName:Z

    .line 1745
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->description:Ljava/lang/String;

    .line 1746
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->hasDescription:Z

    .line 1747
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    .line 1748
    iput-object v2, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->amount:Lcom/google/android/finsky/protos/Buy$Money;

    .line 1749
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->cachedSize:I

    .line 1750
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1773
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1774
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->hasName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1775
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1778
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->hasDescription:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->description:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1779
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->description:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1782
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v1, :cond_4

    .line 1783
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1786
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->amount:Lcom/google/android/finsky/protos/Buy$Money;

    if-eqz v1, :cond_5

    .line 1787
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->amount:Lcom/google/android/finsky/protos/Buy$Money;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1790
    :cond_5
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$LineItem;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1798
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1799
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1803
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1804
    :sswitch_0
    return-object p0

    .line 1809
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->name:Ljava/lang/String;

    .line 1810
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->hasName:Z

    goto :goto_0

    .line 1814
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->description:Ljava/lang/String;

    .line 1815
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->hasDescription:Z

    goto :goto_0

    .line 1819
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    if-nez v1, :cond_1

    .line 1820
    new-instance v1, Lcom/google/android/finsky/protos/Common$Offer;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Offer;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    .line 1822
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1826
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->amount:Lcom/google/android/finsky/protos/Buy$Money;

    if-nez v1, :cond_2

    .line 1827
    new-instance v1, Lcom/google/android/finsky/protos/Buy$Money;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Buy$Money;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->amount:Lcom/google/android/finsky/protos/Buy$Money;

    .line 1829
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->amount:Lcom/google/android/finsky/protos/Buy$Money;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1799
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1707
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Buy$LineItem;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Buy$LineItem;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1756
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->hasName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1757
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1759
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->hasDescription:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->description:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1760
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->description:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1762
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    if-eqz v0, :cond_4

    .line 1763
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->offer:Lcom/google/android/finsky/protos/Common$Offer;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1765
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->amount:Lcom/google/android/finsky/protos/Buy$Money;

    if-eqz v0, :cond_5

    .line 1766
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Buy$LineItem;->amount:Lcom/google/android/finsky/protos/Buy$Money;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1768
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1769
    return-void
.end method
