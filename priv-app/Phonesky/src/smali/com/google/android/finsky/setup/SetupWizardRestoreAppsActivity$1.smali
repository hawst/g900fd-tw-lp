.class Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;
.super Ljava/lang/Object;
.source "SetupWizardRestoreAppsActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureNavNextButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

.field final synthetic val$setupWizardNavBar:Lcom/google/android/finsky/setup/SetupWizardNavBar;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;Lcom/google/android/finsky/setup/SetupWizardNavBar;)V
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    iput-object p2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->val$setupWizardNavBar:Lcom/google/android/finsky/setup/SetupWizardNavBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, -0x1

    .line 379
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->val$setupWizardNavBar:Lcom/google/android/finsky/setup/SetupWizardNavBar;

    invoke-virtual {v2}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 380
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # invokes: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->shouldSetupAsNewDevice()Z
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$000(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 381
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # invokes: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->setResultAndFinish(I)V
    invoke-static {v2, v4}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$100(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;I)V

    .line 390
    :goto_0
    return-void

    .line 384
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # invokes: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->restoreSelectedPackages()V
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$200(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)V

    .line 385
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 386
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$300(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I
    invoke-static {v3}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->access$400(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)I

    move-result v3

    aget-object v2, v2, v3

    iget-object v1, v2, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->restoreToken:Ljava/lang/String;

    .line 387
    .local v1, "restoreToken":Ljava/lang/String;
    const-string v2, "restoreToken"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 388
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    invoke-virtual {v2, v4, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->setResult(ILandroid/content/Intent;)V

    .line 389
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;->this$0:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    invoke-virtual {v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->finish()V

    goto :goto_0
.end method
