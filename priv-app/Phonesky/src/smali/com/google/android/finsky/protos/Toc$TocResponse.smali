.class public final Lcom/google/android/finsky/protos/Toc$TocResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Toc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Toc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TocResponse"
.end annotation


# instance fields
.field public ageVerificationRequired:Z

.field public billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

.field public corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

.field public experiments:Lcom/google/android/finsky/protos/Toc$Experiments;

.field public gplusSignupEnabled:Z

.field public hasAgeVerificationRequired:Z

.field public hasGplusSignupEnabled:Z

.field public hasHomeUrl:Z

.field public hasIconOverrideUrl:Z

.field public hasRecsWidgetUrl:Z

.field public hasRedeemEnabled:Z

.field public hasRequiresUploadDeviceConfig:Z

.field public hasSocialHomeUrl:Z

.field public hasTosCheckboxTextMarketingEmails:Z

.field public hasTosContent:Z

.field public hasTosToken:Z

.field public hasTosVersionDeprecated:Z

.field public homeUrl:Ljava/lang/String;

.field public iconOverrideUrl:Ljava/lang/String;

.field public recsWidgetUrl:Ljava/lang/String;

.field public redeemEnabled:Z

.field public requiresUploadDeviceConfig:Z

.field public selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

.field public socialHomeUrl:Ljava/lang/String;

.field public tosCheckboxTextMarketingEmails:Ljava/lang/String;

.field public tosContent:Ljava/lang/String;

.field public tosToken:Ljava/lang/String;

.field public tosVersionDeprecated:I

.field public userSettings:Lcom/google/android/finsky/protos/Toc$UserSettings;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Toc$TocResponse;->clear()Lcom/google/android/finsky/protos/Toc$TocResponse;

    .line 90
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Toc$TocResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 93
    invoke-static {}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;->emptyArray()[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    .line 94
    iput v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosVersionDeprecated:I

    .line 95
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosVersionDeprecated:Z

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosContent:Ljava/lang/String;

    .line 97
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosContent:Z

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosCheckboxTextMarketingEmails:Ljava/lang/String;

    .line 99
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosCheckboxTextMarketingEmails:Z

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosToken:Ljava/lang/String;

    .line 101
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosToken:Z

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->homeUrl:Ljava/lang/String;

    .line 103
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasHomeUrl:Z

    .line 104
    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->experiments:Lcom/google/android/finsky/protos/Toc$Experiments;

    .line 105
    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->userSettings:Lcom/google/android/finsky/protos/Toc$UserSettings;

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->iconOverrideUrl:Ljava/lang/String;

    .line 107
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasIconOverrideUrl:Z

    .line 108
    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    .line 109
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->requiresUploadDeviceConfig:Z

    .line 110
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRequiresUploadDeviceConfig:Z

    .line 111
    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->recsWidgetUrl:Ljava/lang/String;

    .line 113
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRecsWidgetUrl:Z

    .line 114
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->socialHomeUrl:Ljava/lang/String;

    .line 115
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasSocialHomeUrl:Z

    .line 116
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->ageVerificationRequired:Z

    .line 117
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasAgeVerificationRequired:Z

    .line 118
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->gplusSignupEnabled:Z

    .line 119
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasGplusSignupEnabled:Z

    .line 120
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->redeemEnabled:Z

    .line 121
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRedeemEnabled:Z

    .line 122
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->cachedSize:I

    .line 123
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 190
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 191
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 192
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 193
    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    aget-object v0, v3, v1

    .line 194
    .local v0, "element":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    if-eqz v0, :cond_0

    .line 195
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 192
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 200
    .end local v0    # "element":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosVersionDeprecated:Z

    if-nez v3, :cond_2

    iget v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosVersionDeprecated:I

    if-eqz v3, :cond_3

    .line 201
    :cond_2
    const/4 v3, 0x2

    iget v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosVersionDeprecated:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 204
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosContent:Z

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosContent:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 205
    :cond_4
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosContent:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 208
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasHomeUrl:Z

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->homeUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 209
    :cond_6
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->homeUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 212
    :cond_7
    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->experiments:Lcom/google/android/finsky/protos/Toc$Experiments;

    if-eqz v3, :cond_8

    .line 213
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->experiments:Lcom/google/android/finsky/protos/Toc$Experiments;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 216
    :cond_8
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosCheckboxTextMarketingEmails:Z

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosCheckboxTextMarketingEmails:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 217
    :cond_9
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosCheckboxTextMarketingEmails:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 220
    :cond_a
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosToken:Z

    if-nez v3, :cond_b

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosToken:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 221
    :cond_b
    const/4 v3, 0x7

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosToken:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 224
    :cond_c
    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->userSettings:Lcom/google/android/finsky/protos/Toc$UserSettings;

    if-eqz v3, :cond_d

    .line 225
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->userSettings:Lcom/google/android/finsky/protos/Toc$UserSettings;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 228
    :cond_d
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasIconOverrideUrl:Z

    if-nez v3, :cond_e

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->iconOverrideUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    .line 229
    :cond_e
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->iconOverrideUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 232
    :cond_f
    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    if-eqz v3, :cond_10

    .line 233
    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 236
    :cond_10
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRequiresUploadDeviceConfig:Z

    if-nez v3, :cond_11

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->requiresUploadDeviceConfig:Z

    if-eqz v3, :cond_12

    .line 237
    :cond_11
    const/16 v3, 0xb

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->requiresUploadDeviceConfig:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 240
    :cond_12
    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    if-eqz v3, :cond_13

    .line 241
    const/16 v3, 0xc

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 244
    :cond_13
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRecsWidgetUrl:Z

    if-nez v3, :cond_14

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->recsWidgetUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_15

    .line 245
    :cond_14
    const/16 v3, 0xd

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->recsWidgetUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 248
    :cond_15
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasSocialHomeUrl:Z

    if-nez v3, :cond_16

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->socialHomeUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    .line 249
    :cond_16
    const/16 v3, 0xf

    iget-object v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->socialHomeUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 252
    :cond_17
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasAgeVerificationRequired:Z

    if-nez v3, :cond_18

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->ageVerificationRequired:Z

    if-eqz v3, :cond_19

    .line 253
    :cond_18
    const/16 v3, 0x10

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->ageVerificationRequired:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 256
    :cond_19
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasGplusSignupEnabled:Z

    if-nez v3, :cond_1a

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->gplusSignupEnabled:Z

    if-eqz v3, :cond_1b

    .line 257
    :cond_1a
    const/16 v3, 0x11

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->gplusSignupEnabled:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 260
    :cond_1b
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRedeemEnabled:Z

    if-nez v3, :cond_1c

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->redeemEnabled:Z

    if-eqz v3, :cond_1d

    .line 261
    :cond_1c
    const/16 v3, 0x12

    iget-boolean v4, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->redeemEnabled:Z

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v3

    add-int/2addr v2, v3

    .line 264
    :cond_1d
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$TocResponse;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 272
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 273
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 277
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 278
    :sswitch_0
    return-object p0

    .line 283
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 285
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    if-nez v5, :cond_2

    move v1, v4

    .line 286
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    .line 288
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    if-eqz v1, :cond_1

    .line 289
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 291
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 292
    new-instance v5, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;-><init>()V

    aput-object v5, v2, v1

    .line 293
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 294
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 291
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 285
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    array-length v1, v5

    goto :goto_1

    .line 297
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Toc$CorpusMetadata;-><init>()V

    aput-object v5, v2, v1

    .line 298
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 299
    iput-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    goto :goto_0

    .line 303
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosVersionDeprecated:I

    .line 304
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosVersionDeprecated:Z

    goto :goto_0

    .line 308
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosContent:Ljava/lang/String;

    .line 309
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosContent:Z

    goto :goto_0

    .line 313
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->homeUrl:Ljava/lang/String;

    .line 314
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasHomeUrl:Z

    goto :goto_0

    .line 318
    :sswitch_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->experiments:Lcom/google/android/finsky/protos/Toc$Experiments;

    if-nez v5, :cond_4

    .line 319
    new-instance v5, Lcom/google/android/finsky/protos/Toc$Experiments;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Toc$Experiments;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->experiments:Lcom/google/android/finsky/protos/Toc$Experiments;

    .line 321
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->experiments:Lcom/google/android/finsky/protos/Toc$Experiments;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 325
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosCheckboxTextMarketingEmails:Ljava/lang/String;

    .line 326
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosCheckboxTextMarketingEmails:Z

    goto/16 :goto_0

    .line 330
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosToken:Ljava/lang/String;

    .line 331
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosToken:Z

    goto/16 :goto_0

    .line 335
    :sswitch_8
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->userSettings:Lcom/google/android/finsky/protos/Toc$UserSettings;

    if-nez v5, :cond_5

    .line 336
    new-instance v5, Lcom/google/android/finsky/protos/Toc$UserSettings;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Toc$UserSettings;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->userSettings:Lcom/google/android/finsky/protos/Toc$UserSettings;

    .line 338
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->userSettings:Lcom/google/android/finsky/protos/Toc$UserSettings;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 342
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->iconOverrideUrl:Ljava/lang/String;

    .line 343
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasIconOverrideUrl:Z

    goto/16 :goto_0

    .line 347
    :sswitch_a
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    if-nez v5, :cond_6

    .line 348
    new-instance v5, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    .line 350
    :cond_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 354
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->requiresUploadDeviceConfig:Z

    .line 355
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRequiresUploadDeviceConfig:Z

    goto/16 :goto_0

    .line 359
    :sswitch_c
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    if-nez v5, :cond_7

    .line 360
    new-instance v5, Lcom/google/android/finsky/protos/Toc$BillingConfig;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Toc$BillingConfig;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    .line 362
    :cond_7
    iget-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 366
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->recsWidgetUrl:Ljava/lang/String;

    .line 367
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRecsWidgetUrl:Z

    goto/16 :goto_0

    .line 371
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->socialHomeUrl:Ljava/lang/String;

    .line 372
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasSocialHomeUrl:Z

    goto/16 :goto_0

    .line 376
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->ageVerificationRequired:Z

    .line 377
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasAgeVerificationRequired:Z

    goto/16 :goto_0

    .line 381
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->gplusSignupEnabled:Z

    .line 382
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasGplusSignupEnabled:Z

    goto/16 :goto_0

    .line 386
    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->redeemEnabled:Z

    .line 387
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRedeemEnabled:Z

    goto/16 :goto_0

    .line 273
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x7a -> :sswitch_e
        0x80 -> :sswitch_f
        0x88 -> :sswitch_10
        0x90 -> :sswitch_11
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Toc$TocResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$TocResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 129
    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 130
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 131
    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->corpus:[Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    aget-object v0, v2, v1

    .line 132
    .local v0, "element":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    if-eqz v0, :cond_0

    .line 133
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 130
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 137
    .end local v0    # "element":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosVersionDeprecated:Z

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosVersionDeprecated:I

    if-eqz v2, :cond_3

    .line 138
    :cond_2
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosVersionDeprecated:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 140
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosContent:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosContent:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 141
    :cond_4
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosContent:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 143
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasHomeUrl:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->homeUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 144
    :cond_6
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->homeUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 146
    :cond_7
    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->experiments:Lcom/google/android/finsky/protos/Toc$Experiments;

    if-eqz v2, :cond_8

    .line 147
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->experiments:Lcom/google/android/finsky/protos/Toc$Experiments;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 149
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosCheckboxTextMarketingEmails:Z

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosCheckboxTextMarketingEmails:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 150
    :cond_9
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosCheckboxTextMarketingEmails:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 152
    :cond_a
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasTosToken:Z

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosToken:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 153
    :cond_b
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->tosToken:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 155
    :cond_c
    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->userSettings:Lcom/google/android/finsky/protos/Toc$UserSettings;

    if-eqz v2, :cond_d

    .line 156
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->userSettings:Lcom/google/android/finsky/protos/Toc$UserSettings;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 158
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasIconOverrideUrl:Z

    if-nez v2, :cond_e

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->iconOverrideUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    .line 159
    :cond_e
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->iconOverrideUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 161
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    if-eqz v2, :cond_10

    .line 162
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->selfUpdateConfig:Lcom/google/android/finsky/protos/Toc$SelfUpdateConfig;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 164
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRequiresUploadDeviceConfig:Z

    if-nez v2, :cond_11

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->requiresUploadDeviceConfig:Z

    if-eqz v2, :cond_12

    .line 165
    :cond_11
    const/16 v2, 0xb

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->requiresUploadDeviceConfig:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 167
    :cond_12
    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    if-eqz v2, :cond_13

    .line 168
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->billingConfig:Lcom/google/android/finsky/protos/Toc$BillingConfig;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 170
    :cond_13
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRecsWidgetUrl:Z

    if-nez v2, :cond_14

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->recsWidgetUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 171
    :cond_14
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->recsWidgetUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 173
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasSocialHomeUrl:Z

    if-nez v2, :cond_16

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->socialHomeUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 174
    :cond_16
    const/16 v2, 0xf

    iget-object v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->socialHomeUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 176
    :cond_17
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasAgeVerificationRequired:Z

    if-nez v2, :cond_18

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->ageVerificationRequired:Z

    if-eqz v2, :cond_19

    .line 177
    :cond_18
    const/16 v2, 0x10

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->ageVerificationRequired:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 179
    :cond_19
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasGplusSignupEnabled:Z

    if-nez v2, :cond_1a

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->gplusSignupEnabled:Z

    if-eqz v2, :cond_1b

    .line 180
    :cond_1a
    const/16 v2, 0x11

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->gplusSignupEnabled:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 182
    :cond_1b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->hasRedeemEnabled:Z

    if-nez v2, :cond_1c

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->redeemEnabled:Z

    if-eqz v2, :cond_1d

    .line 183
    :cond_1c
    const/16 v2, 0x12

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Toc$TocResponse;->redeemEnabled:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 185
    :cond_1d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 186
    return-void
.end method
