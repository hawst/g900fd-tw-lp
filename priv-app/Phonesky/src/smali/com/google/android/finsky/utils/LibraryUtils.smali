.class public Lcom/google/android/finsky/utils/LibraryUtils;
.super Ljava/lang/Object;
.source "LibraryUtils.java"


# static fields
.field private static sLibraryEntryForOwnership:Lcom/google/android/finsky/library/LibraryEntry;

.field private static sMusicAllAccessLibraryEntry:Lcom/google/android/finsky/library/LibraryEntry;


# direct methods
.method public static getFirstOwner(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;)Landroid/accounts/Account;
    .locals 5
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "libraries"    # Lcom/google/android/finsky/library/Libraries;

    .prologue
    .line 125
    invoke-virtual {p1}, Lcom/google/android/finsky/library/Libraries;->getAccountLibraries()Ljava/util/List;

    move-result-object v0

    .line 126
    .local v0, "accountLibraries":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/AccountLibrary;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 127
    .local v2, "accountLibraryCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 128
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/library/AccountLibrary;

    .line 129
    .local v1, "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-static {p0, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 130
    invoke-virtual {v1}, Lcom/google/android/finsky/library/AccountLibrary;->getAccount()Landroid/accounts/Account;

    move-result-object v4

    .line 133
    .end local v1    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :goto_1
    return-object v4

    .line 127
    .restart local v1    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 133
    .end local v1    # "accountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static getMusicAllAccessLibraryEntry()Lcom/google/android/finsky/library/LibraryEntry;
    .locals 7

    .prologue
    const/4 v3, 0x2

    .line 229
    sget-object v0, Lcom/google/android/finsky/utils/LibraryUtils;->sMusicAllAccessLibraryEntry:Lcom/google/android/finsky/library/LibraryEntry;

    if-nez v0, :cond_0

    .line 230
    new-instance v0, Lcom/google/android/finsky/library/LibraryEntry;

    sget-object v1, Lcom/google/android/finsky/library/LibraryEntry;->UNKNOWN_ACCOUNT:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/finsky/library/AccountLibrary;->getLibraryIdFromBackend(I)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/android/finsky/config/G;->musicAllAccessSubscriptionBackendDocid:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v4}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const/16 v5, 0xf

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/library/LibraryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/google/android/finsky/utils/LibraryUtils;->sMusicAllAccessLibraryEntry:Lcom/google/android/finsky/library/LibraryEntry;

    .line 235
    :cond_0
    sget-object v0, Lcom/google/android/finsky/utils/LibraryUtils;->sMusicAllAccessLibraryEntry:Lcom/google/android/finsky/library/LibraryEntry;

    return-object v0
.end method

.method public static getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 4
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p2, "currentAccount"    # Landroid/accounts/Account;

    .prologue
    .line 86
    invoke-virtual {p1, p2}, Lcom/google/android/finsky/library/Libraries;->getAccountLibrary(Landroid/accounts/Account;)Lcom/google/android/finsky/library/AccountLibrary;

    move-result-object v0

    .line 87
    .local v0, "currentAccountLibrary":Lcom/google/android/finsky/library/AccountLibrary;
    invoke-static {p0, v0}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v1

    .line 88
    .local v1, "isOwnedByCurrentAccount":Z
    if-eqz v1, :cond_0

    .line 96
    .end local p2    # "currentAccount":Landroid/accounts/Account;
    :goto_0
    return-object p2

    .line 91
    .restart local p2    # "currentAccount":Landroid/accounts/Account;
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 94
    invoke-static {p0, p1}, Lcom/google/android/finsky/utils/LibraryUtils;->getFirstOwner(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;)Landroid/accounts/Account;

    move-result-object p2

    goto :goto_0

    .line 96
    :cond_1
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public static getOwnerWithCurrentAccount(Ljava/util/List;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;
    .locals 4
    .param p1, "libraries"    # Lcom/google/android/finsky/library/Libraries;
    .param p2, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/api/model/Document;",
            ">;",
            "Lcom/google/android/finsky/library/Libraries;",
            "Landroid/accounts/Account;",
            ")",
            "Landroid/accounts/Account;"
        }
    .end annotation

    .prologue
    .line 107
    .local p0, "docs":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/api/model/Document;>;"
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    .line 108
    .local v1, "docCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 109
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    .line 110
    .local v0, "doc":Lcom/google/android/finsky/api/model/Document;
    invoke-static {v0, p1, p2}, Lcom/google/android/finsky/utils/LibraryUtils;->getOwnerWithCurrentAccount(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Libraries;Landroid/accounts/Account;)Landroid/accounts/Account;

    move-result-object v3

    .line 111
    .local v3, "owner":Landroid/accounts/Account;
    if-eqz v3, :cond_0

    .line 115
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "owner":Landroid/accounts/Account;
    :goto_1
    return-object v3

    .line 108
    .restart local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .restart local v3    # "owner":Landroid/accounts/Account;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 115
    .end local v0    # "doc":Lcom/google/android/finsky/api/model/Document;
    .end local v3    # "owner":Landroid/accounts/Account;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public static getVoucherIds(Lcom/google/android/finsky/library/AccountLibrary;)Ljava/util/Collection;
    .locals 6
    .param p0, "accountLibrary"    # Lcom/google/android/finsky/library/AccountLibrary;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/finsky/library/AccountLibrary;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    const/4 v3, 0x0

    .line 259
    .local v3, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v4, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_COMMERCE:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/library/AccountLibrary;->getLibrary(Ljava/lang/String;)Lcom/google/android/finsky/library/HashingLibrary;

    move-result-object v0

    .line 260
    .local v0, "commerceLibrary":Lcom/google/android/finsky/library/Library;
    invoke-interface {v0}, Lcom/google/android/finsky/library/Library;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/library/LibraryEntry;

    .line 262
    .local v1, "entry":Lcom/google/android/finsky/library/LibraryEntry;
    invoke-virtual {v1}, Lcom/google/android/finsky/library/LibraryEntry;->getDocType()I

    move-result v4

    const/16 v5, 0x1d

    if-ne v4, v5, :cond_0

    .line 263
    if-nez v3, :cond_1

    .line 264
    new-instance v3, Ljava/util/ArrayList;

    .end local v3    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 266
    .restart local v3    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/finsky/library/LibraryEntry;->getDocId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 269
    .end local v1    # "entry":Lcom/google/android/finsky/library/LibraryEntry;
    :cond_2
    return-object v3
.end method

.method public static hasApplicableVouchers(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/AccountLibrary;)Z
    .locals 6
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "accountLibrary"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    const/4 v4, 0x0

    .line 277
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->hasVoucherIds()Z

    move-result v5

    if-nez v5, :cond_1

    .line 296
    :cond_0
    :goto_0
    return v4

    .line 281
    :cond_1
    invoke-static {p1}, Lcom/google/android/finsky/utils/LibraryUtils;->getVoucherIds(Lcom/google/android/finsky/library/AccountLibrary;)Ljava/util/Collection;

    move-result-object v1

    .line 283
    .local v1, "accountVouchers":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 287
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getVoucherIds()Ljava/util/Set;

    move-result-object v2

    .line 291
    .local v2, "docVouchers":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 292
    .local v0, "accountVoucher":Ljava/lang/String;
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 293
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public static hasVouchers(Lcom/google/android/finsky/library/AccountLibrary;)Z
    .locals 5
    .param p0, "accountLibrary"    # Lcom/google/android/finsky/library/AccountLibrary;

    .prologue
    .line 243
    sget-object v3, Lcom/google/android/finsky/library/AccountLibrary;->LIBRARY_ID_COMMERCE:Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/android/finsky/library/AccountLibrary;->getLibrary(Ljava/lang/String;)Lcom/google/android/finsky/library/HashingLibrary;

    move-result-object v0

    .line 244
    .local v0, "commerceLibrary":Lcom/google/android/finsky/library/Library;
    invoke-interface {v0}, Lcom/google/android/finsky/library/Library;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/library/LibraryEntry;

    .line 246
    .local v1, "entry":Lcom/google/android/finsky/library/LibraryEntry;
    invoke-virtual {v1}, Lcom/google/android/finsky/library/LibraryEntry;->getDocType()I

    move-result v3

    const/16 v4, 0x1d

    if-ne v3, v4, :cond_0

    .line 247
    const/4 v3, 0x1

    .line 250
    .end local v1    # "entry":Lcom/google/android/finsky/library/LibraryEntry;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isAvailable(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/library/Library;)Z
    .locals 9
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p2, "library"    # Lcom/google/android/finsky/library/Library;

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    if-eqz v5, :cond_2

    .line 52
    if-eqz p1, :cond_1

    .line 53
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/google/android/finsky/api/model/DfeToc;->getCorpus(I)Lcom/google/android/finsky/protos/Toc$CorpusMetadata;

    move-result-object v0

    .line 54
    .local v0, "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    if-nez v0, :cond_2

    .line 55
    const-string v5, "Corpus for %s is not available."

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v3

    invoke-static {v5, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v3

    .line 74
    .end local v0    # "corpus":Lcom/google/android/finsky/protos/Toc$CorpusMetadata;
    :cond_0
    :goto_0
    return v1

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v5

    const/4 v6, 0x3

    if-eq v5, v6, :cond_2

    move v1, v3

    .line 60
    goto :goto_0

    .line 64
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getAvailabilityRestriction()I

    move-result v2

    .line 65
    .local v2, "restriction":I
    if-ne v2, v4, :cond_4

    move v1, v4

    .line 66
    .local v1, "isAvailable":Z
    :goto_1
    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->isAvailableIfOwned()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-static {p0, p2}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 67
    const-string v5, "%s available because owned, overriding [restriction=%d]."

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    const/4 v1, 0x1

    .line 71
    :cond_3
    if-nez v1, :cond_0

    .line 72
    const-string v5, "%s not available [restriction=%d]."

    new-array v6, v8, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v4

    invoke-static {v5, v6}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .end local v1    # "isAvailable":Z
    :cond_4
    move v1, v3

    .line 65
    goto :goto_1
.end method

.method public static isAvailableInMusicAllAccess(Lcom/google/android/finsky/api/model/Document;)Z
    .locals 10
    .param p0, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 200
    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/api/model/Document;->getOffer(I)Lcom/google/android/finsky/protos/Common$Offer;

    move-result-object v7

    .line 201
    .local v7, "offer":Lcom/google/android/finsky/protos/Common$Offer;
    if-eqz v7, :cond_0

    iget-object v1, v7, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    if-nez v1, :cond_2

    :cond_0
    move v6, v9

    .line 217
    :cond_1
    :goto_0
    return v6

    .line 204
    :cond_2
    iget-object v1, v7, Lcom/google/android/finsky/protos/Common$Offer;->subscriptionContentTerms:Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;

    iget-object v8, v1, Lcom/google/android/finsky/protos/Common$SubscriptionContentTerms;->requiredSubscription:Lcom/google/android/finsky/protos/Common$Docid;

    .line 205
    .local v8, "requiredSub":Lcom/google/android/finsky/protos/Common$Docid;
    iget v3, v8, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    .line 206
    .local v3, "backend":I
    invoke-static {v3}, Lcom/google/android/finsky/library/AccountLibrary;->getLibraryIdFromBackend(I)Ljava/lang/String;

    move-result-object v2

    .line 207
    .local v2, "libraryId":Ljava/lang/String;
    iget-object v4, v8, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 209
    .local v4, "backendDocId":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/library/LibraryEntry;

    sget-object v1, Lcom/google/android/finsky/library/LibraryEntry;->UNKNOWN_ACCOUNT:Ljava/lang/String;

    iget v5, v8, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/library/LibraryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;II)V

    .line 214
    .local v0, "entry":Lcom/google/android/finsky/library/LibraryEntry;
    invoke-static {}, Lcom/google/android/finsky/utils/LibraryUtils;->getMusicAllAccessLibraryEntry()Lcom/google/android/finsky/library/LibraryEntry;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/library/LibraryEntry;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move v6, v9

    .line 215
    goto :goto_0
.end method

.method public static isMusicAllAcessSubscriber(Lcom/google/android/finsky/library/Library;)Z
    .locals 1
    .param p0, "library"    # Lcom/google/android/finsky/library/Library;

    .prologue
    .line 225
    invoke-static {}, Lcom/google/android/finsky/utils/LibraryUtils;->getMusicAllAccessLibraryEntry()Lcom/google/android/finsky/library/LibraryEntry;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/finsky/library/Library;->contains(Lcom/google/android/finsky/library/LibraryEntry;)Z

    move-result v0

    return v0
.end method

.method public static declared-synchronized isOfferOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;I)Z
    .locals 2
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "library"    # Lcom/google/android/finsky/library/Library;
    .param p2, "offerType"    # I

    .prologue
    .line 174
    const-class v1, Lcom/google/android/finsky/utils/LibraryUtils;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getFullDocid()Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized isOfferOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;I)Z
    .locals 5
    .param p0, "docId"    # Lcom/google/android/finsky/protos/Common$Docid;
    .param p1, "library"    # Lcom/google/android/finsky/library/Library;
    .param p2, "offerType"    # I

    .prologue
    const/4 v2, 0x0

    .line 181
    const-class v3, Lcom/google/android/finsky/utils/LibraryUtils;

    monitor-enter v3

    :try_start_0
    iget v4, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    invoke-static {v4}, Lcom/google/android/finsky/library/AccountLibrary;->getLibraryIdFromBackend(I)Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "libraryId":Ljava/lang/String;
    sget-object v4, Lcom/google/android/finsky/utils/LibraryUtils;->sLibraryEntryForOwnership:Lcom/google/android/finsky/library/LibraryEntry;

    if-nez v4, :cond_1

    .line 183
    sget-object v4, Lcom/google/android/finsky/library/LibraryEntry;->UNKNOWN_ACCOUNT:Ljava/lang/String;

    invoke-static {v4, v1, p0, p2}, Lcom/google/android/finsky/library/LibraryEntry;->fromDocId(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;I)Lcom/google/android/finsky/library/LibraryEntry;

    move-result-object v4

    sput-object v4, Lcom/google/android/finsky/utils/LibraryUtils;->sLibraryEntryForOwnership:Lcom/google/android/finsky/library/LibraryEntry;

    .line 188
    :goto_0
    sget-object v4, Lcom/google/android/finsky/utils/LibraryUtils;->sLibraryEntryForOwnership:Lcom/google/android/finsky/library/LibraryEntry;

    invoke-interface {p1, v4}, Lcom/google/android/finsky/library/Library;->get(Lcom/google/android/finsky/library/LibraryEntry;)Lcom/google/android/finsky/library/LibraryEntry;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 189
    .local v0, "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    if-nez v0, :cond_2

    .line 192
    :cond_0
    :goto_1
    monitor-exit v3

    return v2

    .line 186
    .end local v0    # "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    :cond_1
    :try_start_1
    sget-object v4, Lcom/google/android/finsky/utils/LibraryUtils;->sLibraryEntryForOwnership:Lcom/google/android/finsky/library/LibraryEntry;

    invoke-virtual {v4, v1, p0, p2}, Lcom/google/android/finsky/library/LibraryEntry;->updateInPlace(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Docid;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 181
    .end local v1    # "libraryId":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 192
    .restart local v0    # "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    .restart local v1    # "libraryId":Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/finsky/library/LibraryEntry;->isExpired()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    const/4 v2, 0x1

    goto :goto_1
.end method

.method public static isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z
    .locals 1
    .param p0, "document"    # Lcom/google/android/finsky/api/model/Document;
    .param p1, "library"    # Lcom/google/android/finsky/library/Library;

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/finsky/api/model/Document;->getFullDocid()Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;)Z

    move-result v0

    return v0
.end method

.method public static isOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;)Z
    .locals 5
    .param p0, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;
    .param p1, "library"    # Lcom/google/android/finsky/library/Library;

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x4

    const/4 v1, 0x1

    .line 152
    invoke-static {p0, p1, v1}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v1

    .line 155
    :cond_1
    iget v3, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    if-ne v3, v1, :cond_5

    :cond_2
    move v0, v1

    .line 157
    .local v0, "rentalsAvailable":Z
    :goto_1
    if-eqz v0, :cond_3

    const/4 v3, 0x3

    invoke-static {p0, p1, v3}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 161
    :cond_3
    iget v3, p0, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    if-ne v3, v4, :cond_4

    const/4 v3, 0x7

    invoke-static {p0, p1, v3}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;I)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p0, p1, v4}, Lcom/google/android/finsky/utils/LibraryUtils;->isOfferOwned(Lcom/google/android/finsky/protos/Common$Docid;Lcom/google/android/finsky/library/Library;I)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    .line 166
    goto :goto_0

    .end local v0    # "rentalsAvailable":Z
    :cond_5
    move v0, v2

    .line 155
    goto :goto_1
.end method
