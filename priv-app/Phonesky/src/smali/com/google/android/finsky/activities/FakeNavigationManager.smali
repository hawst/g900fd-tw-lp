.class Lcom/google/android/finsky/activities/FakeNavigationManager;
.super Lcom/google/android/finsky/navigationmanager/NavigationManager;
.source "FakeNavigationManager.java"


# instance fields
.field private mActivity:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/navigationmanager/NavigationManager;-><init>(Lcom/google/android/finsky/activities/MainActivity;)V

    .line 31
    iput-object p1, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    .line 32
    return-void
.end method


# virtual methods
.method public addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;

    .prologue
    .line 75
    return-void
.end method

.method public buy(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;Ljava/lang/String;)V
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p3, "offerType"    # I
    .param p4, "filter"    # Lcom/google/android/finsky/utils/DocUtils$OfferFilter;
    .param p5, "appsContinueUrl"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-virtual {p2}, Lcom/google/android/finsky/api/model/Document;->getServerLogsCookie()[B

    move-result-object v4

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move-object v3, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/billing/lightpurchase/LightPurchaseFlowActivity;->createIntent(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;ILcom/google/android/finsky/utils/DocUtils$OfferFilter;[BLjava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    .line 109
    .local v6, "intent":Landroid/content/Intent;
    iget-object v0, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    const/16 v1, 0x21

    invoke-virtual {v0, v6, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 110
    return-void
.end method

.method public canGoUp()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method public canSearch()Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public getActivePage()Lcom/google/android/finsky/fragments/PageFragment;
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getActivityForResolveLink()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public goBack()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public goBrowse(Ljava/lang/String;Ljava/lang/String;ILcom/google/android/finsky/api/model/DfeToc;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "backendId"    # I
    .param p4, "dfeToc"    # Lcom/google/android/finsky/api/model/DfeToc;
    .param p5, "clickLogNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 87
    iget-object v1, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-static {v1, p1, p2, p3, v2}, Lcom/google/android/finsky/utils/IntentUtils;->createBrowseIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)Landroid/content/Intent;

    move-result-object v0

    .line 89
    .local v0, "browseIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 90
    iget-object v1, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 91
    return-void
.end method

.method public goToDocPage(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 79
    iget-object v1, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    invoke-static {v1, p1}, Lcom/google/android/finsky/utils/IntentUtils;->createViewDocumentUrlIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 80
    .local v0, "detailsIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 81
    iget-object v1, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 82
    return-void
.end method

.method public goToImagesLightbox(Lcom/google/android/finsky/api/model/Document;II)V
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p2, "initialIndex"    # I
    .param p3, "imageType"    # I

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/finsky/activities/ScreenshotsActivity;->show(Landroid/content/Context;Lcom/google/android/finsky/api/model/Document;II)V

    goto :goto_0
.end method

.method public goUp()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 46
    const/4 v0, 0x1

    return v0
.end method

.method public init(Lcom/google/android/finsky/activities/MainActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/android/finsky/activities/MainActivity;

    .prologue
    .line 40
    return-void
.end method

.method public isBackStackEmpty()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public openItem(Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;)V
    .locals 6
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mActivity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/navigationmanager/ConsumptionUtils;->openItem(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/finsky/api/model/Document;Landroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;I)Z

    .line 116
    return-void
.end method

.method public setFragmentManager(Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/google/android/finsky/activities/FakeNavigationManager;->mFragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 36
    return-void
.end method
