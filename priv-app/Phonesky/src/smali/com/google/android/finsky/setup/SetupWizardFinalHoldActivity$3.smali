.class Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$3;
.super Ljava/lang/Object;
.source "SetupWizardFinalHoldActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;)V
    .locals 0

    .prologue
    .line 313
    iput-object p1, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 316
    const-string v2, "Watchdog fired, skipping hold."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;

    # invokes: Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->setResultAndFinish(I)V
    invoke-static {v2, v4}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->access$200(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;I)V

    .line 320
    invoke-static {}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreBackgroundActionEvent()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v0

    .line 322
    .local v0, "event":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;
    const/16 v2, 0x7e

    iput v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->type:I

    .line 323
    iput-boolean v4, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasType:Z

    .line 324
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogLastPackage:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->access$300(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 325
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity$3;->this$0:Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;

    # getter for: Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->mWatchdogLastPackage:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;->access$300(Lcom/google/android/finsky/setup/SetupWizardFinalHoldActivity;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->document:Ljava/lang/String;

    .line 326
    iput-boolean v4, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;->hasDocument:Z

    .line 328
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    .line 329
    .local v1, "eventLogger":Lcom/google/android/finsky/analytics/FinskyEventLog;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 330
    return-void
.end method
