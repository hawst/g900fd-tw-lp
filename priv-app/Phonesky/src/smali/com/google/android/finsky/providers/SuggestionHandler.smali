.class final Lcom/google/android/finsky/providers/SuggestionHandler;
.super Ljava/lang/Object;
.source "SuggestionHandler.java"


# static fields
.field private static final COLUMNS:[Ljava/lang/String;


# instance fields
.field private final mAlreadyAddedSuggestions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final mContext:Landroid/content/Context;

.field private mEncodedReport:Ljava/lang/String;

.field protected final mQuery:Ljava/lang/String;

.field private mReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

.field private final mResults:Landroid/database/MatrixCursor;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_intent_query"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/finsky/providers/SuggestionHandler;->COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {}, Lcom/google/android/finsky/utils/Sets;->newHashSet()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mAlreadyAddedSuggestions:Ljava/util/Set;

    .line 56
    new-instance v0, Landroid/database/MatrixCursor;

    sget-object v1, Lcom/google/android/finsky/providers/SuggestionHandler;->COLUMNS:[Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mResults:Landroid/database/MatrixCursor;

    .line 62
    iput-object v2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mEncodedReport:Ljava/lang/String;

    .line 68
    iput-object v2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    .line 71
    iput-object p1, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mQuery:Ljava/lang/String;

    .line 72
    iput-object p2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mContext:Landroid/content/Context;

    .line 73
    return-void
.end method


# virtual methods
.method public addRow(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 6
    .param p1, "id"    # I
    .param p2, "icon"    # Ljava/lang/Object;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;
    .param p5, "action"    # Ljava/lang/String;
    .param p6, "data"    # Ljava/lang/String;
    .param p7, "serverLogsCookie"    # [B

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x1

    .line 127
    iget-object v2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mAlreadyAddedSuggestions:Ljava/util/Set;

    invoke-interface {v2, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    :goto_0
    return-void

    .line 130
    :cond_0
    sget-object v2, Lcom/google/android/finsky/providers/SuggestionHandler;->COLUMNS:[Ljava/lang/String;

    array-length v2, v2

    new-array v1, v2, [Ljava/lang/Object;

    .line 131
    .local v1, "row":[Ljava/lang/Object;
    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 132
    aput-object p2, v1, v4

    .line 133
    const/4 v2, 0x2

    aput-object p3, v1, v2

    .line 134
    const/4 v2, 0x3

    aput-object p4, v1, v2

    .line 135
    const/4 v2, 0x4

    aput-object p3, v1, v2

    .line 136
    const/4 v2, 0x5

    aput-object p5, v1, v2

    .line 137
    const/4 v2, 0x6

    aput-object p6, v1, v2

    .line 141
    if-eqz p7, :cond_1

    array-length v2, p7

    if-lez v2, :cond_1

    .line 142
    iget-object v2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    iput-object p7, v2, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->suggestionServerLogsCookie:[B

    .line 143
    iget-object v2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    iput-boolean v4, v2, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSuggestionServerLogsCookie:Z

    .line 144
    iget-object v2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    invoke-static {v2}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    .line 145
    .local v0, "reportBytes":[B
    const/16 v2, 0xa

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 151
    .end local v0    # "reportBytes":[B
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mAlreadyAddedSuggestions:Ljava/util/Set;

    invoke-interface {v2, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mResults:Landroid/database/MatrixCursor;

    invoke-virtual {v2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 148
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mEncodedReport:Ljava/lang/String;

    aput-object v2, v1, v5

    goto :goto_1
.end method

.method public addRow(ILjava/lang/Object;Ljava/lang/String;[B)V
    .locals 8
    .param p1, "id"    # I
    .param p2, "icon"    # Ljava/lang/Object;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "serverLogsCookie"    # [B

    .prologue
    const/4 v4, 0x0

    .line 119
    const-string v5, "android.intent.action.SEARCH"

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v4

    move-object v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/providers/SuggestionHandler;->addRow(ILjava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 120
    return-void
.end method

.method public getSuggestions()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mResults:Landroid/database/MatrixCursor;

    return-object v0
.end method

.method public sendSuggestionsReceivedLog(II[BJ)V
    .locals 10
    .param p1, "sourceServerId"    # I
    .param p2, "count"    # I
    .param p3, "serverLogsCookie"    # [B
    .param p4, "startTimeMs"    # J

    .prologue
    const/4 v8, 0x1

    .line 83
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v2, v6, p4

    .line 84
    .local v2, "latencyMs":J
    new-instance v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    invoke-direct {v1}, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;-><init>()V

    .line 85
    .local v1, "report":Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;
    iget-object v5, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mQuery:Ljava/lang/String;

    iput-object v5, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->query:Ljava/lang/String;

    .line 86
    iput-boolean v8, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasQuery:Z

    .line 87
    iput-wide v2, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->clientLatencyMs:J

    .line 88
    iput-boolean v8, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasClientLatencyMs:Z

    .line 89
    iput p1, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->source:I

    .line 90
    iput-boolean v8, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasSource:Z

    .line 91
    iput p2, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->resultCount:I

    .line 92
    iput-boolean v8, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResultCount:Z

    .line 93
    if-eqz p3, :cond_0

    array-length v5, p3

    if-lez v5, :cond_0

    .line 94
    iput-object p3, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->responseServerLogsCookie:[B

    .line 95
    iput-boolean v8, v1, Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;->hasResponseServerLogsCookie:Z

    .line 98
    :cond_0
    new-instance v5, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    const/16 v6, 0x1fe

    invoke-direct {v5, v6}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v5, v1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setSearchSuggestionReport(Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 101
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 103
    invoke-static {v1}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v4

    .line 104
    .local v4, "reportBytes":[B
    const/16 v5, 0xa

    invoke-static {v4, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mEncodedReport:Ljava/lang/String;

    .line 105
    iput-object v1, p0, Lcom/google/android/finsky/providers/SuggestionHandler;->mReport:Lcom/google/android/finsky/analytics/PlayStore$SearchSuggestionReport;

    .line 106
    return-void
.end method
