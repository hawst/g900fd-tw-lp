.class Lcom/google/android/finsky/utils/UninstallRefundTracker$2;
.super Ljava/lang/Object;
.source "UninstallRefundTracker.java"

# interfaces
.implements Lcom/google/android/finsky/utils/AppSupport$RefundListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/UninstallRefundTracker;->refundIfNecessary(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/utils/UninstallRefundTracker;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/utils/UninstallRefundTracker;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/finsky/utils/UninstallRefundTracker$2;->this$0:Lcom/google/android/finsky/utils/UninstallRefundTracker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onRefundComplete(Z)V
    .locals 2
    .param p1, "succeeded"    # Z

    .prologue
    .line 80
    if-eqz p1, :cond_0

    .line 82
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 83
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/google/android/finsky/utils/UninstallRefundTracker$2$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/utils/UninstallRefundTracker$2$1;-><init>(Lcom/google/android/finsky/utils/UninstallRefundTracker$2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 91
    .end local v0    # "handler":Landroid/os/Handler;
    :cond_0
    return-void
.end method

.method public onRefundStart()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method
