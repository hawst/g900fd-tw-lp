.class public final Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PromoCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/PromoCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RedemptionSuccess"
.end annotation


# instance fields
.field public buttonLabel:Ljava/lang/String;

.field public hasButtonLabel:Z

.field public hasMessageHtml:Z

.field public hasTitle:Z

.field public hasTitleBylineHtml:Z

.field public libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

.field public messageHtml:Ljava/lang/String;

.field public postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

.field public thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

.field public title:Ljava/lang/String;

.field public titleBylineHtml:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 888
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 889
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->clear()Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    .line 890
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 893
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    .line 894
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasMessageHtml:Z

    .line 895
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->buttonLabel:Ljava/lang/String;

    .line 896
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasButtonLabel:Z

    .line 897
    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    .line 898
    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 899
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    .line 900
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasTitle:Z

    .line 901
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->titleBylineHtml:Ljava/lang/String;

    .line 902
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasTitleBylineHtml:Z

    .line 903
    iput-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 904
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->cachedSize:I

    .line 905
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 937
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 938
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasMessageHtml:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 939
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 942
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasButtonLabel:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->buttonLabel:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 943
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->buttonLabel:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 946
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    if-eqz v1, :cond_4

    .line 947
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 950
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v1, :cond_5

    .line 951
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 954
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasTitle:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 955
    :cond_6
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 958
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasTitleBylineHtml:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->titleBylineHtml:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 959
    :cond_8
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->titleBylineHtml:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 962
    :cond_9
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v1, :cond_a

    .line 963
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 966
    :cond_a
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 974
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 975
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 979
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 980
    :sswitch_0
    return-object p0

    .line 985
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    .line 986
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasMessageHtml:Z

    goto :goto_0

    .line 990
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->buttonLabel:Ljava/lang/String;

    .line 991
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasButtonLabel:Z

    goto :goto_0

    .line 995
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    if-nez v1, :cond_1

    .line 996
    new-instance v1, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    .line 998
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1002
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-nez v1, :cond_2

    .line 1003
    new-instance v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 1005
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1009
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    .line 1010
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasTitle:Z

    goto :goto_0

    .line 1014
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->titleBylineHtml:Ljava/lang/String;

    .line 1015
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasTitleBylineHtml:Z

    goto :goto_0

    .line 1019
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v1, :cond_3

    .line 1020
    new-instance v1, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 1022
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 975
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 846
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 911
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasMessageHtml:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 912
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->messageHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 914
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasButtonLabel:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->buttonLabel:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 915
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->buttonLabel:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 917
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    if-eqz v0, :cond_4

    .line 918
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->postSuccessAction:Lcom/google/android/finsky/protos/PromoCode$PostSuccessAction;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 920
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    if-eqz v0, :cond_5

    .line 921
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 923
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasTitle:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 924
    :cond_6
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 926
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->hasTitleBylineHtml:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->titleBylineHtml:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 927
    :cond_8
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->titleBylineHtml:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 929
    :cond_9
    iget-object v0, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_a

    .line 930
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/PromoCode$RedemptionSuccess;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 932
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 933
    return-void
.end method
