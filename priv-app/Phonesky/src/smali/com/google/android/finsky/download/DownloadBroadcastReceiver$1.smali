.class Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;
.super Landroid/os/AsyncTask;
.source "DownloadBroadcastReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/download/DownloadBroadcastReceiver;

.field final synthetic val$contentUri:Landroid/net/Uri;

.field final synthetic val$finalIsClicked:Z

.field final synthetic val$finalIsCompleted:Z


# direct methods
.method constructor <init>(Lcom/google/android/finsky/download/DownloadBroadcastReceiver;Landroid/net/Uri;ZZ)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->this$0:Lcom/google/android/finsky/download/DownloadBroadcastReceiver;

    iput-object p2, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->val$contentUri:Landroid/net/Uri;

    iput-boolean p3, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->val$finalIsClicked:Z

    iput-boolean p4, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->val$finalIsCompleted:Z

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 85
    # getter for: Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;
    invoke-static {}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->access$000()Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/finsky/download/DownloadQueue;->getDownloadManager()Lcom/google/android/finsky/download/DownloadManagerFacade;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->val$contentUri:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/google/android/finsky/download/DownloadManagerFacade;->query(Landroid/net/Uri;Lcom/google/android/finsky/download/DownloadManagerFacade$Listener;)Ljava/util/List;

    move-result-object v0

    .line 87
    .local v0, "status":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/download/DownloadProgress;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v5, :cond_0

    .line 88
    const-string v1, "Unable to find %s in download manager"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->val$contentUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 89
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 91
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/download/DownloadProgress;

    iget v1, v1, Lcom/google/android/finsky/download/DownloadProgress;->statusCode:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 82
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 5
    .param p1, "httpStatus"    # Ljava/lang/Integer;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 96
    # getter for: Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;
    invoke-static {}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->access$000()Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->val$contentUri:Landroid/net/Uri;

    invoke-interface {v1, v2}, Lcom/google/android/finsky/download/DownloadQueue;->getDownloadByContentUri(Landroid/net/Uri;)Lcom/google/android/finsky/download/Download;

    move-result-object v0

    .line 97
    .local v0, "download":Lcom/google/android/finsky/download/Download;
    if-nez v0, :cond_0

    .line 98
    const-string v1, "Did not find download in queue for %s"

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->val$contentUri:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 125
    :goto_0
    return-void

    .line 103
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 104
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/finsky/download/Download;->setHttpStatus(I)V

    .line 109
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->val$finalIsClicked:Z

    if-eqz v1, :cond_2

    .line 110
    # getter for: Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;
    invoke-static {}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->access$000()Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/finsky/download/DownloadQueue;->notifyClicked(Lcom/google/android/finsky/download/Download;)V

    goto :goto_0

    .line 106
    :cond_1
    const-string v1, "DownloadBroadcastReceiver received invalid HTTP status of -1"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 111
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->val$finalIsCompleted:Z

    if-eqz v1, :cond_5

    .line 112
    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 114
    const-string v1, "Received ACTION_DOWNLOAD_COMPLETE %d for %s - dropping because already in state %s."

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v4

    aput-object v0, v2, v3

    const/4 v3, 0x2

    invoke-interface {v0}, Lcom/google/android/finsky/download/Download;->getState()Lcom/google/android/finsky/download/Download$DownloadState;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 117
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/download/DownloadManagerConstants;->isStatusSuccess(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 118
    # getter for: Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;
    invoke-static {}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->access$000()Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/download/Download$DownloadState;->SUCCESS:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-interface {v1, v0, v2}, Lcom/google/android/finsky/download/DownloadQueue;->setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V

    goto :goto_0

    .line 120
    :cond_4
    # getter for: Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->sDownloadQueue:Lcom/google/android/finsky/download/DownloadQueue;
    invoke-static {}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver;->access$000()Lcom/google/android/finsky/download/DownloadQueue;

    move-result-object v1

    sget-object v2, Lcom/google/android/finsky/download/Download$DownloadState;->ERROR:Lcom/google/android/finsky/download/Download$DownloadState;

    invoke-interface {v1, v0, v2}, Lcom/google/android/finsky/download/DownloadQueue;->setDownloadState(Lcom/google/android/finsky/download/Download;Lcom/google/android/finsky/download/Download$DownloadState;)V

    goto :goto_0

    .line 123
    :cond_5
    const-string v1, "Invalid DownloadBroadcastReceiver intent"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 82
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/download/DownloadBroadcastReceiver$1;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
