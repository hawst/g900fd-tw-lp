.class public Lcom/google/android/finsky/layout/ListingSection;
.super Landroid/widget/RelativeLayout;
.source "ListingSection.java"


# instance fields
.field private mDescriptionContainer:Landroid/widget/LinearLayout;

.field private mIcon:Landroid/widget/ImageView;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mTitle:Landroid/widget/TextView;

.field private mTopSeparator:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/ListingSection;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingSection;->mInflater:Landroid/view/LayoutInflater;

    .line 41
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 47
    const v0, 0x7f0a0259

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingSection;->mTitle:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0a025a

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingSection;->mDescriptionContainer:Landroid/widget/LinearLayout;

    .line 49
    const v0, 0x7f0a0170

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingSection;->mIcon:Landroid/widget/ImageView;

    .line 50
    const v0, 0x7f0a0258

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/ListingSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/ListingSection;->mTopSeparator:Landroid/view/View;

    .line 51
    return-void
.end method
