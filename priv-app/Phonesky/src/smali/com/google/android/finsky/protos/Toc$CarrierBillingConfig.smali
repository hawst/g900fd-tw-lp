.class public final Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Toc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Toc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CarrierBillingConfig"
.end annotation


# instance fields
.field public apiVersion:I

.field public credentialsUrl:Ljava/lang/String;

.field public hasApiVersion:Z

.field public hasCredentialsUrl:Z

.field public hasId:Z

.field public hasName:Z

.field public hasPerTransactionCredentialsRequired:Z

.field public hasProvisioningUrl:Z

.field public hasSendSubscriberIdWithCarrierBillingRequests:Z

.field public hasTosRequired:Z

.field public id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public perTransactionCredentialsRequired:Z

.field public provisioningUrl:Ljava/lang/String;

.field public sendSubscriberIdWithCarrierBillingRequests:Z

.field public tosRequired:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1038
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1039
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->clear()Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    .line 1040
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1043
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->id:Ljava/lang/String;

    .line 1044
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasId:Z

    .line 1045
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->name:Ljava/lang/String;

    .line 1046
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasName:Z

    .line 1047
    iput v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->apiVersion:I

    .line 1048
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasApiVersion:Z

    .line 1049
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->provisioningUrl:Ljava/lang/String;

    .line 1050
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasProvisioningUrl:Z

    .line 1051
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->credentialsUrl:Ljava/lang/String;

    .line 1052
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasCredentialsUrl:Z

    .line 1053
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->tosRequired:Z

    .line 1054
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasTosRequired:Z

    .line 1055
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->perTransactionCredentialsRequired:Z

    .line 1056
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasPerTransactionCredentialsRequired:Z

    .line 1057
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->sendSubscriberIdWithCarrierBillingRequests:Z

    .line 1058
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasSendSubscriberIdWithCarrierBillingRequests:Z

    .line 1059
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->cachedSize:I

    .line 1060
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1095
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1096
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasId:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->id:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1097
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1100
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasName:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1101
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1104
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasApiVersion:Z

    if-nez v1, :cond_4

    iget v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->apiVersion:I

    if-eqz v1, :cond_5

    .line 1105
    :cond_4
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->apiVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1108
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasProvisioningUrl:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->provisioningUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1109
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->provisioningUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1112
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasCredentialsUrl:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->credentialsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1113
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->credentialsUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1116
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasTosRequired:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->tosRequired:Z

    if-eqz v1, :cond_b

    .line 1117
    :cond_a
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->tosRequired:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1120
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasPerTransactionCredentialsRequired:Z

    if-nez v1, :cond_c

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->perTransactionCredentialsRequired:Z

    if-eqz v1, :cond_d

    .line 1121
    :cond_c
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->perTransactionCredentialsRequired:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1124
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasSendSubscriberIdWithCarrierBillingRequests:Z

    if-nez v1, :cond_e

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->sendSubscriberIdWithCarrierBillingRequests:Z

    if-eqz v1, :cond_f

    .line 1125
    :cond_e
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->sendSubscriberIdWithCarrierBillingRequests:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1128
    :cond_f
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1136
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1137
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1141
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1142
    :sswitch_0
    return-object p0

    .line 1147
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->id:Ljava/lang/String;

    .line 1148
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasId:Z

    goto :goto_0

    .line 1152
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->name:Ljava/lang/String;

    .line 1153
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasName:Z

    goto :goto_0

    .line 1157
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->apiVersion:I

    .line 1158
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasApiVersion:Z

    goto :goto_0

    .line 1162
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->provisioningUrl:Ljava/lang/String;

    .line 1163
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasProvisioningUrl:Z

    goto :goto_0

    .line 1167
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->credentialsUrl:Ljava/lang/String;

    .line 1168
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasCredentialsUrl:Z

    goto :goto_0

    .line 1172
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->tosRequired:Z

    .line 1173
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasTosRequired:Z

    goto :goto_0

    .line 1177
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->perTransactionCredentialsRequired:Z

    .line 1178
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasPerTransactionCredentialsRequired:Z

    goto :goto_0

    .line 1182
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->sendSubscriberIdWithCarrierBillingRequests:Z

    .line 1183
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasSendSubscriberIdWithCarrierBillingRequests:Z

    goto :goto_0

    .line 1137
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 989
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1066
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasId:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->id:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1067
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->id:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1069
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasName:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1070
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1072
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasApiVersion:Z

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->apiVersion:I

    if-eqz v0, :cond_5

    .line 1073
    :cond_4
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->apiVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1075
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasProvisioningUrl:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->provisioningUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1076
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->provisioningUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1078
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasCredentialsUrl:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->credentialsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1079
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->credentialsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1081
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasTosRequired:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->tosRequired:Z

    if-eqz v0, :cond_b

    .line 1082
    :cond_a
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->tosRequired:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1084
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasPerTransactionCredentialsRequired:Z

    if-nez v0, :cond_c

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->perTransactionCredentialsRequired:Z

    if-eqz v0, :cond_d

    .line 1085
    :cond_c
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->perTransactionCredentialsRequired:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1087
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->hasSendSubscriberIdWithCarrierBillingRequests:Z

    if-nez v0, :cond_e

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->sendSubscriberIdWithCarrierBillingRequests:Z

    if-eqz v0, :cond_f

    .line 1088
    :cond_e
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/Toc$CarrierBillingConfig;->sendSubscriberIdWithCarrierBillingRequests:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 1090
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1091
    return-void
.end method
