.class public Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;
.super Lcom/google/android/finsky/layout/PermissionAdapter;
.source "DetailedPermissionsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;",
            ">;"
        }
    .end annotation
.end field

.field private final mIsAppInstalled:Z

.field private final mLayoutInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 15
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "serverPermissions"    # [Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/finsky/layout/PermissionAdapter;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v11

    iput-object v11, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mData:Ljava/util/List;

    .line 41
    move-object/from16 v0, p1

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mContext:Landroid/content/Context;

    .line 42
    invoke-static/range {p1 .. p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    iput-object v11, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 44
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    move-object/from16 v0, p2

    invoke-static {v11, v0}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->getPackageInfo(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v9

    .line 46
    .local v9, "packageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v9, :cond_1

    const/4 v11, 0x1

    :goto_0
    iput-boolean v11, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mIsAppInstalled:Z

    .line 47
    invoke-static {v9}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->loadLocalAssetPermissions(Landroid/content/pm/PackageInfo;)Ljava/util/Set;

    move-result-object v6

    .line 55
    .local v6, "installedPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 56
    .local v3, "existingPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v6, :cond_0

    .line 59
    invoke-static/range {p3 .. p3}, Lcom/google/android/finsky/utils/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    .line 60
    .local v8, "newPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v8, v6}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 63
    invoke-static/range {p3 .. p3}, Lcom/google/android/finsky/utils/Sets;->newHashSet([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    .line 64
    invoke-interface {v3, v8}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 68
    .end local v8    # "newPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    const/4 v11, 0x1

    const/4 v12, 0x1

    move-object/from16 v0, p3

    invoke-static {v0, v3, v11, v12}, Lcom/google/android/finsky/utils/PermissionsBucketer;->getPermissionBuckets([Ljava/lang/String;Ljava/util/Set;ZZ)Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;

    move-result-object v10

    .line 72
    .local v10, "permissionData":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;
    const/16 v11, 0x11

    new-array v7, v11, [Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    .line 74
    .local v7, "newBuckets":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    const/16 v11, 0x11

    new-array v2, v11, [Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    .line 76
    .local v2, "existingBuckets":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    iget-object v11, v10, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mNewPermissions:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    .line 77
    .local v1, "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    iget v11, v1, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketId:I

    aput-object v1, v7, v11

    goto :goto_1

    .line 46
    .end local v1    # "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v2    # "existingBuckets":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v3    # "existingPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "installedPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v7    # "newBuckets":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .end local v10    # "permissionData":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;
    :cond_1
    const/4 v11, 0x0

    goto :goto_0

    .line 80
    .restart local v2    # "existingBuckets":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .restart local v3    # "existingPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "installedPermissions":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v7    # "newBuckets":[Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    .restart local v10    # "permissionData":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;
    :cond_2
    iget-object v11, v10, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionData;->mExistingPermissionsBucket:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;

    .line 81
    .restart local v1    # "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    iget v11, v1, Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;->mBucketId:I

    aput-object v1, v2, v11

    goto :goto_2

    .line 85
    .end local v1    # "bucket":Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;
    :cond_3
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_3
    const/16 v11, 0x11

    if-ge v4, v11, :cond_6

    .line 88
    aget-object v11, v7, v4

    if-nez v11, :cond_4

    aget-object v11, v2, v4

    if-eqz v11, :cond_5

    .line 89
    :cond_4
    iget-object v11, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mData:Ljava/util/List;

    new-instance v12, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;

    aget-object v13, v7, v4

    aget-object v14, v2, v4

    invoke-direct {v12, v13, v14}, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;-><init>(Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;Lcom/google/android/finsky/utils/PermissionsBucketer$PermissionBucket;)V

    invoke-interface {v11, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 92
    :cond_6
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mData:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "i"    # I

    .prologue
    .line 106
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 122
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mData:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;

    .line 123
    .local v1, "bucket":Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v12, 0x7f04010e

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v11, v12, v0, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 124
    .local v10, "view":Landroid/view/View;
    const v11, 0x7f0a0173

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 125
    .local v7, "headerView":Landroid/widget/TextView;
    const v11, 0x7f0a00b4

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 126
    .local v4, "contentView":Landroid/widget/TextView;
    const v11, 0x7f0a01d3

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 127
    .local v5, "expanderIcon":Landroid/widget/ImageView;
    const v11, 0x7f0a01d2

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 128
    .local v2, "bucketIcon":Landroid/widget/ImageView;
    const/16 v11, 0x8

    invoke-virtual {v5, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 129
    iget v11, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mBucketTitle:I

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setText(I)V

    .line 131
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 132
    .local v3, "builder":Ljava/lang/StringBuilder;
    iget-object v11, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mExistingPermissionDescriptions:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_2

    const/4 v6, 0x1

    .line 133
    .local v6, "hasExistingPermissions":Z
    :goto_0
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    iget-object v11, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mNewPermissionDescriptions:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v8, v11, :cond_4

    .line 135
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mIsAppInstalled:Z

    if-eqz v11, :cond_3

    .line 137
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mContext:Landroid/content/Context;

    const v12, 0x7f0c028c

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    iget-object v15, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mNewPermissionDescriptions:Ljava/util/List;

    invoke-interface {v15, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 142
    .local v9, "newPermission":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mContext:Landroid/content/Context;

    const v12, 0x7f0c02d2

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v9, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    iget-object v11, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mNewPermissionDescriptions:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    if-lt v8, v11, :cond_0

    if-eqz v6, :cond_1

    .line 146
    :cond_0
    const-string v11, "<br>"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 132
    .end local v6    # "hasExistingPermissions":Z
    .end local v8    # "i":I
    .end local v9    # "newPermission":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 140
    .restart local v6    # "hasExistingPermissions":Z
    .restart local v8    # "i":I
    :cond_3
    iget-object v11, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mNewPermissionDescriptions:Ljava/util/List;

    invoke-interface {v11, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .restart local v9    # "newPermission":Ljava/lang/String;
    goto :goto_2

    .line 150
    .end local v9    # "newPermission":Ljava/lang/String;
    :cond_4
    const/4 v8, 0x0

    :goto_3
    iget-object v11, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mExistingPermissionDescriptions:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v8, v11, :cond_6

    .line 151
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mContext:Landroid/content/Context;

    const v12, 0x7f0c02d2

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    iget-object v15, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mExistingPermissionDescriptions:Ljava/util/List;

    invoke-interface {v15, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    iget-object v11, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mExistingPermissionDescriptions:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    if-ge v8, v11, :cond_5

    .line 154
    const-string v11, "<br>"

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 158
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v11

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget v11, v1, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter$DetailedPermissionBucket;->mBucketIcon:I

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 160
    const/4 v11, 0x0

    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    return-object v10
.end method

.method public isAppInstalled()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mIsAppInstalled:Z

    return v0
.end method

.method public showTheNoPermissionMessage()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailedPermissionsAdapter;->mData:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
