.class public Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;
.super Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;
.source "OrderHistoryAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private final mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private final mIsFullList:Z

.field private final mLeadingExtraSpacerHeight:I

.field private final mLeadingSpacerHeight:I

.field private final mOnRefundActionListener:Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;

.field private final mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mSelectedPosition:I

.field private mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/finsky/api/model/DfeList;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;ZLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "dfeList"    # Lcom/google/android/finsky/api/model/DfeList;
    .param p3, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p4, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p5, "onRefundActionListener"    # Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;
    .param p6, "isFullList"    # Z
    .param p7, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 58
    invoke-direct {p0, p1, p4, p2}, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;-><init>(Landroid/content/Context;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/api/model/ContainerList;)V

    .line 52
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedPosition:I

    .line 59
    iput-object p3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 60
    iput-object p5, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mOnRefundActionListener:Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;

    .line 61
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mLeadingSpacerHeight:I

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 64
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b0176

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0b0092

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mLeadingExtraSpacerHeight:I

    .line 67
    iput-boolean p6, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mIsFullList:Z

    .line 68
    iput-object p7, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 69
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getEventLogger()Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    .prologue
    .line 32
    iget v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedPosition:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedPosition:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;)Lcom/google/android/finsky/layout/OrderHistoryRowView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;Lcom/google/android/finsky/layout/OrderHistoryRowView;)Lcom/google/android/finsky/layout/OrderHistoryRowView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;
    .param p1, "x1"    # Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    return-object p1
.end method

.method private bindExtraLeadingSpacer(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 222
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mLeadingExtraSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 223
    return-void
.end method

.method private bindLeadingSpacer(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 218
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mLeadingSpacerHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 219
    return-void
.end method

.method private bindOrderHistoryRowView(Landroid/view/View;I)V
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 226
    move-object v0, p1

    check-cast v0, Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .line 227
    .local v0, "rowView":Lcom/google/android/finsky/layout/OrderHistoryRowView;
    iget v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedPosition:I

    if-ne v4, p2, :cond_2

    move v10, v2

    .line 230
    .local v10, "selected":Z
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    if-ne v4, v0, :cond_0

    .line 231
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .line 234
    :cond_0
    if-eqz v10, :cond_1

    .line 235
    iput-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedRowView:Lcom/google/android/finsky/layout/OrderHistoryRowView;

    .line 238
    :cond_1
    invoke-direct {p0, p2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->getItem(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v1

    .line 240
    .local v1, "item":Lcom/google/android/finsky/api/model/Document;
    iget-boolean v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mIsFullList:Z

    if-eqz v4, :cond_8

    .line 245
    iget-object v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v4, v3}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    if-ne v1, v4, :cond_3

    move v8, v2

    .line 246
    .local v8, "isFirstOrder":Z
    :goto_1
    iget-object v4, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    iget-object v5, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    if-ne v1, v4, :cond_4

    move v9, v2

    .line 247
    .local v9, "isLastOrder":Z
    :goto_2
    if-eqz v8, :cond_5

    if-eqz v9, :cond_5

    .line 249
    const v6, 0x7f02014b

    .line 262
    .end local v8    # "isFirstOrder":Z
    .end local v9    # "isLastOrder":Z
    .local v6, "collapsedBackgroundResourceId":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    iget-object v3, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    iget-object v7, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mOnRefundActionListener:Lcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/Boolean;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;ILcom/google/android/finsky/layout/OrderHistoryRowView$OnRefundActionListener;)V

    .line 264
    new-instance v2, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;

    invoke-direct {v2, p0, v0, p2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$1;-><init>(Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;Lcom/google/android/finsky/layout/OrderHistoryRowView;I)V

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/layout/OrderHistoryRowView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    return-void

    .end local v1    # "item":Lcom/google/android/finsky/api/model/Document;
    .end local v6    # "collapsedBackgroundResourceId":I
    .end local v10    # "selected":Z
    :cond_2
    move v10, v3

    .line 227
    goto :goto_0

    .restart local v1    # "item":Lcom/google/android/finsky/api/model/Document;
    .restart local v10    # "selected":Z
    :cond_3
    move v8, v3

    .line 245
    goto :goto_1

    .restart local v8    # "isFirstOrder":Z
    :cond_4
    move v9, v3

    .line 246
    goto :goto_2

    .line 250
    .restart local v9    # "isLastOrder":Z
    :cond_5
    if-eqz v8, :cond_6

    .line 251
    const v6, 0x7f020149

    .restart local v6    # "collapsedBackgroundResourceId":I
    goto :goto_3

    .line 252
    .end local v6    # "collapsedBackgroundResourceId":I
    :cond_6
    if-eqz v9, :cond_7

    .line 253
    const v6, 0x7f02014a

    .restart local v6    # "collapsedBackgroundResourceId":I
    goto :goto_3

    .line 255
    .end local v6    # "collapsedBackgroundResourceId":I
    :cond_7
    const v6, 0x7f090088

    .restart local v6    # "collapsedBackgroundResourceId":I
    goto :goto_3

    .line 260
    .end local v6    # "collapsedBackgroundResourceId":I
    .end local v8    # "isFirstOrder":Z
    .end local v9    # "isLastOrder":Z
    :cond_8
    const/4 v6, 0x0

    .restart local v6    # "collapsedBackgroundResourceId":I
    goto :goto_3
.end method

.method private getItem(I)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mIsFullList:Z

    if-eqz v0, :cond_1

    .line 142
    const/4 v0, 0x1

    if-gt p1, v0, :cond_0

    .line 145
    const/4 v0, 0x0

    .line 149
    :goto_0
    return-object v0

    .line 147
    :cond_0
    add-int/lit8 p1, p1, -0x2

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/api/model/ContainerList;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/api/model/Document;

    goto :goto_0
.end method


# virtual methods
.method public getItemCount()I
    .locals 4

    .prologue
    .line 87
    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v1

    .line 88
    .local v1, "countData":I
    move v0, v1

    .line 90
    .local v0, "countAll":I
    iget-boolean v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mIsFullList:Z

    if-eqz v2, :cond_0

    .line 93
    add-int/lit8 v0, v0, 0x2

    .line 96
    :cond_0
    if-nez v1, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/ContainerList;->isMoreAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    .line 97
    add-int/lit8 v0, v0, 0x1

    .line 100
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    move-result-object v2

    sget-object v3, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-eq v2, v3, :cond_2

    .line 101
    add-int/lit8 v0, v0, 0x1

    .line 103
    :cond_2
    return v0
.end method

.method public getItemViewType(I)I
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x5

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 108
    iget-boolean v5, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mIsFullList:Z

    if-eqz v5, :cond_1

    .line 109
    if-nez p1, :cond_0

    .line 137
    :goto_0
    return v1

    .line 112
    :cond_0
    if-ne p1, v2, :cond_1

    move v1, v2

    .line 114
    goto :goto_0

    .line 118
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/ContainerList;->getCount()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mContainerList:Lcom/google/android/finsky/api/model/ContainerList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/ContainerList;->isMoreAvailable()Z

    move-result v2

    if-nez v2, :cond_2

    .line 119
    const/4 v1, 0x3

    goto :goto_0

    .line 123
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->getItemCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_6

    .line 124
    invoke-virtual {p0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->getFooterMode()Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    move-result-object v0

    .line 125
    .local v0, "footerMode":Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;
    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->LOADING:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-ne v0, v2, :cond_3

    .line 126
    const/4 v1, 0x4

    goto :goto_0

    .line 127
    :cond_3
    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->ERROR:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-ne v0, v2, :cond_4

    move v1, v3

    .line 128
    goto :goto_0

    .line 129
    :cond_4
    sget-object v2, Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;->NONE:Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;

    if-ne v0, v2, :cond_5

    move v1, v4

    .line 130
    goto :goto_0

    .line 132
    :cond_5
    const-string v2, "No footer or item in last row"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v3

    .line 133
    goto :goto_0

    .end local v0    # "footerMode":Lcom/google/android/finsky/adapters/PaginatedRecyclerViewAdapter$FooterMode;
    :cond_6
    move v1, v4

    .line 137
    goto :goto_0
.end method

.method public getView(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 298
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->getItemViewType(I)I

    move-result v1

    .line 299
    .local v1, "type":I
    invoke-virtual {p0, p2, v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->createViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    .line 300
    .local v0, "holder":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    invoke-virtual {p0, v0, p1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->bindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V

    .line 301
    iget-object v2, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    return-object v2
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 5
    .param p1, "viewHolder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 181
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v0

    .line 182
    .local v0, "itemViewType":I
    iget-object v1, p1, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 183
    .local v1, "v":Landroid/view/View;
    packed-switch v0, :pswitch_data_0

    .line 203
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown type for getView "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 185
    :pswitch_0
    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->bindLeadingSpacer(Landroid/view/View;)V

    .line 206
    :goto_0
    :pswitch_1
    return-void

    .line 188
    :pswitch_2
    invoke-direct {p0, v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->bindExtraLeadingSpacer(Landroid/view/View;)V

    goto :goto_0

    .line 191
    :pswitch_3
    invoke-direct {p0, v1, p2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->bindOrderHistoryRowView(Landroid/view/View;I)V

    goto :goto_0

    .line 197
    :pswitch_4
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->bindLoadingFooterView(Landroid/view/View;)V

    goto :goto_0

    .line 200
    :pswitch_5
    invoke-virtual {p0, v1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->bindErrorFooterView(Landroid/view/View;)Landroid/view/View;

    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    const/4 v2, 0x0

    .line 155
    packed-switch p2, :pswitch_data_0

    .line 173
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown type for getView "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 158
    :pswitch_0
    const v1, 0x7f04014d

    invoke-virtual {p0, v1, p1, v2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 176
    .local v0, "v":Landroid/view/View;
    :goto_0
    new-instance v1, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$ViewHolder;

    invoke-direct {v1, v0}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v1

    .line 161
    .end local v0    # "v":Landroid/view/View;
    :pswitch_1
    const v1, 0x7f040102

    invoke-virtual {p0, v1, p1, v2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 162
    .restart local v0    # "v":Landroid/view/View;
    goto :goto_0

    .line 164
    .end local v0    # "v":Landroid/view/View;
    :pswitch_2
    const v1, 0x7f040101

    invoke-virtual {p0, v1, p1, v2}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 165
    .restart local v0    # "v":Landroid/view/View;
    goto :goto_0

    .line 167
    .end local v0    # "v":Landroid/view/View;
    :pswitch_3
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->createLoadingFooterView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 168
    .restart local v0    # "v":Landroid/view/View;
    goto :goto_0

    .line 170
    .end local v0    # "v":Landroid/view/View;
    :pswitch_4
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->createErrorFooterView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 171
    .restart local v0    # "v":Landroid/view/View;
    goto :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 215
    return-void
.end method

.method public onRestoreInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .param p2, "restoreBundle"    # Landroid/os/Bundle;

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->onRestoreInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 82
    const-string v0, "selectedPosition"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedPosition:I

    .line 83
    return-void
.end method

.method public onSaveInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Lcom/google/android/finsky/layout/play/PlayRecyclerView;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/adapters/FinskyRecyclerViewAdapter;->onSaveInstanceState(Lcom/google/android/finsky/layout/play/PlayRecyclerView;Landroid/os/Bundle;)V

    .line 76
    const-string v0, "selectedPosition"

    iget v1, p0, Lcom/google/android/finsky/activities/myaccount/OrderHistoryAdapter;->mSelectedPosition:I

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    return-void
.end method
