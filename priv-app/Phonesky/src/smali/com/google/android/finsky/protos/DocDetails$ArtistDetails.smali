.class public final Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DocDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/DocDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ArtistDetails"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;


# instance fields
.field public detailsUrl:Ljava/lang/String;

.field public externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

.field public hasDetailsUrl:Z

.field public hasName:Z

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2440
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2441
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->clear()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 2442
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .locals 2

    .prologue
    .line 2418
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-nez v0, :cond_1

    .line 2419
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 2421
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    if-nez v0, :cond_0

    .line 2422
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    sput-object v0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    .line 2424
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2426
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->_emptyArray:[Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    return-object v0

    .line 2424
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2445
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    .line 2446
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasDetailsUrl:Z

    .line 2447
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    .line 2448
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasName:Z

    .line 2449
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    .line 2450
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->cachedSize:I

    .line 2451
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2471
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2472
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasDetailsUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2473
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2476
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasName:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2477
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2480
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    if-eqz v1, :cond_4

    .line 2481
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2484
    :cond_4
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2492
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2493
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2497
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2498
    :sswitch_0
    return-object p0

    .line 2503
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    .line 2504
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasDetailsUrl:Z

    goto :goto_0

    .line 2508
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    .line 2509
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasName:Z

    goto :goto_0

    .line 2513
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    if-nez v1, :cond_1

    .line 2514
    new-instance v1, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    .line 2516
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 2493
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2412
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2457
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasDetailsUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2458
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->detailsUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2460
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->hasName:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2461
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2463
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    if-eqz v0, :cond_4

    .line 2464
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/DocDetails$ArtistDetails;->externalLinks:Lcom/google/android/finsky/protos/DocDetails$ArtistExternalLinks;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 2466
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2467
    return-void
.end method
