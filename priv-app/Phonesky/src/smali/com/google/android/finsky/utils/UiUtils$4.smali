.class final Lcom/google/android/finsky/utils/UiUtils$4;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "UiUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/UiUtils;->enableActionBarOverlayScrolling(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/finsky/layout/actionbar/ActionBarController;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/actionbar/ActionBarController;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/google/android/finsky/utils/UiUtils$4;->val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0
    .param p1, "view"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "scrollState"    # I

    .prologue
    .line 371
    return-void
.end method

.method public onScrolled(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 12
    .param p1, "view"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 375
    const/4 v3, 0x0

    .line 376
    .local v3, "firstVisibleItem":I
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v8

    .line 377
    .local v8, "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    instance-of v9, v8, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v9, :cond_0

    .line 378
    check-cast v8, Landroid/support/v7/widget/LinearLayoutManager;

    .end local v8    # "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    invoke-virtual {v8}, Landroid/support/v7/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    move-result v3

    .line 382
    :cond_0
    if-lez v3, :cond_2

    .line 385
    iget-object v9, p0, Lcom/google/android/finsky/utils/UiUtils$4;->val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    const/16 v10, 0xff

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->setActionBarAlpha(IZ)V

    .line 425
    :cond_1
    :goto_0
    return-void

    .line 396
    :cond_2
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v9

    if-lez v9, :cond_1

    .line 397
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 398
    .local v4, "firstVisibleView":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v2

    .line 400
    .local v2, "firstRowPixelOffset":I
    const/4 v7, 0x0

    .line 401
    .local v7, "imageHost":Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;
    instance-of v9, v4, Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    if-eqz v9, :cond_3

    move-object v7, v4

    .line 402
    check-cast v7, Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;

    .line 406
    :cond_3
    if-eqz v7, :cond_4

    invoke-interface {v7}, Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;->getOverlayableImageHeight()I

    move-result v1

    .line 410
    .local v1, "firstRowPixelHeight":I
    :goto_1
    if-nez v1, :cond_5

    const/high16 v5, 0x3f800000    # 1.0f

    .line 412
    .local v5, "fraction":F
    :goto_2
    const/high16 v9, 0x3f800000    # 1.0f

    invoke-static {v9, v5}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 413
    const/high16 v9, 0x437f0000    # 255.0f

    mul-float/2addr v9, v5

    float-to-int v0, v9

    .line 414
    .local v0, "alpha":I
    iget-object v9, p0, Lcom/google/android/finsky/utils/UiUtils$4;->val$actionBarController:Lcom/google/android/finsky/layout/actionbar/ActionBarController;

    const/4 v10, 0x0

    invoke-interface {v9, v0, v10}, Lcom/google/android/finsky/layout/actionbar/ActionBarController;->setActionBarAlpha(IZ)V

    .line 416
    if-eqz v7, :cond_1

    .line 419
    int-to-float v9, v1

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v9, v10

    mul-float/2addr v9, v5

    float-to-int v6, v9

    .line 421
    .local v6, "heroTopPadding":I
    invoke-interface {v7, v6}, Lcom/google/android/finsky/layout/actionbar/OverlayableImageHost;->setOverlayableImageTopPadding(I)V

    goto :goto_0

    .line 406
    .end local v0    # "alpha":I
    .end local v1    # "firstRowPixelHeight":I
    .end local v5    # "fraction":F
    .end local v6    # "heroTopPadding":I
    :cond_4
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_1

    .line 410
    .restart local v1    # "firstRowPixelHeight":I
    :cond_5
    int-to-float v9, v2

    int-to-float v10, v1

    div-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v5

    goto :goto_2
.end method
