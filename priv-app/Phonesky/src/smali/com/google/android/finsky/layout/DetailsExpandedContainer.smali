.class public Lcom/google/android/finsky/layout/DetailsExpandedContainer;
.super Landroid/widget/LinearLayout;
.source "DetailsExpandedContainer.java"

# interfaces
.implements Lcom/google/android/finsky/activities/TextSectionTranslatable;


# instance fields
.field private mDetailsBodyForTranslation:Lcom/google/android/finsky/layout/DetailsTextBlock;

.field private mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

.field private mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

.field private mDetailsExpandedCallout:Landroid/widget/TextView;

.field private mDetailsExpandedExtras:Landroid/view/ViewGroup;

.field private mIsShowingTranslation:Z

.field private mOriginalBody:Ljava/lang/CharSequence;

.field private mTranslatedBody:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)Lcom/google/android/finsky/layout/DetailsTextBlock;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)Lcom/google/android/finsky/layout/DetailsTextBlock;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/DetailsExpandedContainer;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private hasExtras()Z
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private populateAttributions(Lcom/google/android/finsky/layout/DetailsTextSection;Landroid/view/LayoutInflater;)V
    .locals 6
    .param p1, "section"    # Lcom/google/android/finsky/layout/DetailsTextSection;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    const/4 v5, 0x0

    .line 273
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExtraAttributionString()Ljava/lang/String;

    move-result-object v0

    .line 276
    .local v0, "attributions":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 291
    :goto_0
    return-void

    .line 280
    :cond_0
    const v3, 0x7f04007b

    iget-object v4, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {p2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    .line 283
    .local v2, "row":Lcom/google/android/finsky/layout/SeparatorLinearLayout;
    const v3, 0x7f040078

    invoke-virtual {p2, v3, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 285
    .local v1, "attributionsView":Landroid/widget/TextView;
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 286
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 287
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 289
    invoke-virtual {v2, v1}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->addView(Landroid/view/View;)V

    .line 290
    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method private populatePrimaryCredits(Lcom/google/android/finsky/layout/DetailsTextSection;Landroid/view/LayoutInflater;)V
    .locals 11
    .param p1, "section"    # Lcom/google/android/finsky/layout/DetailsTextSection;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    const/4 v10, 0x0

    .line 144
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExtraCreditsHeader()Ljava/lang/String;

    move-result-object v4

    .line 145
    .local v4, "extraCreditsHeader":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExtraCreditsList()Ljava/util/List;

    move-result-object v5

    .line 146
    .local v5, "extraCreditsList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraCredits;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 148
    .local v3, "extraCreditsCount":I
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    if-nez v3, :cond_1

    .line 168
    :cond_0
    return-void

    .line 152
    :cond_1
    const v8, 0x7f04005a

    iget-object v9, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {p2, v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 154
    .local v6, "header":Landroid/view/ViewGroup;
    const v8, 0x7f0a014f

    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/layout/DecoratedTextView;

    .line 156
    .local v7, "headerTextView":Lcom/google/android/finsky/layout/DecoratedTextView;
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/finsky/layout/DecoratedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v8, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 159
    const/4 v0, 0x0

    .local v0, "creditsIndex":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 161
    const v8, 0x7f040079

    iget-object v9, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {p2, v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/DetailsExpandedExtraCreditsView;

    .line 164
    .local v1, "entryCreditsView":Lcom/google/android/finsky/layout/DetailsExpandedExtraCreditsView;
    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraCredits;

    .line 165
    .local v2, "extraCredits":Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraCredits;
    invoke-virtual {v1, v2}, Lcom/google/android/finsky/layout/DetailsExpandedExtraCreditsView;->populate(Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraCredits;)V

    .line 166
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v8, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private populatePrimaryExtras(Lcom/google/android/finsky/layout/DetailsTextSection;Lcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/activities/TextSectionStateListener;Landroid/view/LayoutInflater;)V
    .locals 23
    .param p1, "section"    # Lcom/google/android/finsky/layout/DetailsTextSection;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "backendId"    # I
    .param p4, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p5, "textSectionStateListener"    # Lcom/google/android/finsky/activities/TextSectionStateListener;
    .param p6, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 175
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v10

    .line 177
    .local v10, "bitmapLoader":Lcom/google/android/play/image/BitmapLoader;
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExtraPrimaryList()Ljava/util/List;

    move-result-object v15

    .line 178
    .local v15, "extraPrimaryList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;>;"
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v14

    .line 180
    .local v14, "extraPrimaryCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    .line 181
    .local v18, "itemsInRow":I
    move/from16 v0, v18

    invoke-static {v14, v0}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v21

    .line 183
    .local v21, "rowCount":I
    const/16 v22, 0x0

    .local v22, "rowIndex":I
    :goto_0
    move/from16 v0, v22

    move/from16 v1, v21

    if-ge v0, v1, :cond_8

    .line 184
    const v2, 0x7f04007b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    const/4 v4, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v20

    check-cast v20, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    .line 186
    .local v20, "row":Lcom/google/android/finsky/layout/SeparatorLinearLayout;
    const/4 v11, 0x0

    .local v11, "colIndex":I
    :goto_1
    move/from16 v0, v18

    if-ge v11, v0, :cond_7

    .line 187
    mul-int v2, v18, v22

    add-int v13, v2, v11

    .line 188
    .local v13, "extraIndex":I
    const v2, 0x7f04007a

    const/4 v3, 0x0

    move-object/from16 v0, p6

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;

    .line 191
    .local v12, "entryPrimaryView":Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;
    if-lt v13, v14, :cond_1

    .line 193
    const/4 v2, 0x4

    invoke-virtual {v12, v2}, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->setVisibility(I)V

    .line 211
    :goto_2
    if-nez v22, :cond_3

    const/16 v16, 0x1

    .line 212
    .local v16, "isFirstRow":Z
    :goto_3
    add-int/lit8 v2, v21, -0x1

    move/from16 v0, v22

    if-ne v0, v2, :cond_4

    const/16 v17, 0x1

    .line 213
    .local v17, "isLastRow":Z
    :goto_4
    if-nez v16, :cond_0

    .line 218
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->hideSeparator()V

    .line 220
    :cond_0
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getPaddingLeft()I

    move-result v4

    if-eqz v16, :cond_5

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getPaddingTop()I

    move-result v2

    :goto_5
    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getPaddingRight()I

    move-result v5

    if-eqz v17, :cond_6

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getPaddingBottom()I

    move-result v3

    :goto_6
    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v2, v5, v3}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->setPadding(IIII)V

    .line 223
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->addView(Landroid/view/View;)V

    .line 186
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 195
    .end local v16    # "isFirstRow":Z
    .end local v17    # "isLastRow":Z
    :cond_1
    invoke-interface {v15, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;

    .line 196
    .local v7, "extraPrimary":Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;
    iget-object v2, v7, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;->url:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/16 v19, 0x0

    .line 207
    .local v19, "primaryClickListener":Landroid/view/View$OnClickListener;
    :goto_7
    move-object/from16 v0, v19

    invoke-virtual {v12, v7, v10, v0}, Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;->populate(Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;Lcom/google/android/play/image/BitmapLoader;Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 196
    .end local v19    # "primaryClickListener":Landroid/view/View$OnClickListener;
    :cond_2
    new-instance v2, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;

    move-object/from16 v3, p0

    move-object/from16 v4, p5

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move/from16 v8, p3

    move-object/from16 v9, p4

    invoke-direct/range {v2 .. v9}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$1;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;Lcom/google/android/finsky/activities/TextSectionStateListener;Lcom/google/android/finsky/layout/DetailsTextSection;Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    move-object/from16 v19, v2

    goto :goto_7

    .line 211
    .end local v7    # "extraPrimary":Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraPrimary;
    :cond_3
    const/16 v16, 0x0

    goto :goto_3

    .line 212
    .restart local v16    # "isFirstRow":Z
    :cond_4
    const/16 v17, 0x0

    goto :goto_4

    .line 220
    .restart local v17    # "isLastRow":Z
    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    :cond_6
    const/4 v3, 0x0

    goto :goto_6

    .line 225
    .end local v12    # "entryPrimaryView":Lcom/google/android/finsky/layout/DetailsExpandedExtraPrimaryView;
    .end local v13    # "extraIndex":I
    .end local v16    # "isFirstRow":Z
    .end local v17    # "isLastRow":Z
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 183
    add-int/lit8 v22, v22, 0x1

    goto/16 :goto_0

    .line 227
    .end local v11    # "colIndex":I
    .end local v20    # "row":Lcom/google/android/finsky/layout/SeparatorLinearLayout;
    :cond_8
    return-void
.end method

.method private populateSecondaryExtras(Lcom/google/android/finsky/layout/DetailsTextSection;Landroid/view/LayoutInflater;)V
    .locals 17
    .param p1, "section"    # Lcom/google/android/finsky/layout/DetailsTextSection;
    .param p2, "inflater"    # Landroid/view/LayoutInflater;

    .prologue
    .line 230
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExtraSecondaryList()Ljava/util/List;

    move-result-object v6

    .line 232
    .local v6, "extraSecondaryList":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    .line 233
    .local v5, "extraSecondaryCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0e0016

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    .line 235
    .local v9, "itemsInRow":I
    invoke-static {v5, v9}, Lcom/google/android/finsky/utils/IntMath;->ceil(II)I

    move-result v11

    .line 237
    .local v11, "rowCount":I
    const/4 v12, 0x0

    .local v12, "rowIndex":I
    :goto_0
    if-ge v12, v11, :cond_7

    .line 238
    const v13, 0x7f04007b

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    const/4 v15, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/layout/SeparatorLinearLayout;

    .line 240
    .local v10, "row":Lcom/google/android/finsky/layout/SeparatorLinearLayout;
    const/4 v1, 0x0

    .local v1, "colIndex":I
    :goto_1
    if-ge v1, v9, :cond_1

    .line 241
    mul-int v13, v9, v12

    add-int v3, v13, v1

    .line 242
    .local v3, "extraIndex":I
    const v13, 0x7f04007c

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v10, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;

    .line 245
    .local v2, "entrySecondaryView":Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;
    if-lt v3, v5, :cond_0

    .line 247
    const/4 v13, 0x4

    invoke-virtual {v2, v13}, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->setVisibility(I)V

    .line 252
    :goto_2
    invoke-virtual {v10, v2}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->addView(Landroid/view/View;)V

    .line 240
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 249
    :cond_0
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;

    .line 250
    .local v4, "extraSecondary":Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;
    invoke-virtual {v2, v4}, Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;->populate(Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;)V

    goto :goto_2

    .line 256
    .end local v2    # "entrySecondaryView":Lcom/google/android/finsky/layout/DetailsExpandedExtraSecondaryView;
    .end local v3    # "extraIndex":I
    .end local v4    # "extraSecondary":Lcom/google/android/finsky/layout/DetailsTextSection$DetailsExtraSecondary;
    :cond_1
    if-nez v12, :cond_3

    const/4 v7, 0x1

    .line 257
    .local v7, "isFirstRow":Z
    :goto_3
    add-int/lit8 v13, v11, -0x1

    if-ne v12, v13, :cond_4

    const/4 v8, 0x1

    .line 258
    .local v8, "isLastRow":Z
    :goto_4
    if-nez v7, :cond_2

    .line 263
    invoke-virtual {v10}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->hideSeparator()V

    .line 265
    :cond_2
    invoke-virtual {v10}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getPaddingLeft()I

    move-result v15

    if-eqz v7, :cond_5

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getPaddingTop()I

    move-result v13

    :goto_5
    invoke-virtual {v10}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getPaddingRight()I

    move-result v16

    if-eqz v8, :cond_6

    invoke-virtual {v10}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->getPaddingBottom()I

    move-result v14

    :goto_6
    move/from16 v0, v16

    invoke-virtual {v10, v15, v13, v0, v14}, Lcom/google/android/finsky/layout/SeparatorLinearLayout;->setPadding(IIII)V

    .line 268
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v13, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 237
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 256
    .end local v7    # "isFirstRow":Z
    .end local v8    # "isLastRow":Z
    :cond_3
    const/4 v7, 0x0

    goto :goto_3

    .line 257
    .restart local v7    # "isFirstRow":Z
    :cond_4
    const/4 v8, 0x0

    goto :goto_4

    .line 265
    .restart local v8    # "isLastRow":Z
    :cond_5
    const/4 v13, 0x0

    goto :goto_5

    :cond_6
    const/4 v14, 0x0

    goto :goto_6

    .line 270
    .end local v1    # "colIndex":I
    .end local v7    # "isFirstRow":Z
    .end local v8    # "isLastRow":Z
    .end local v10    # "row":Lcom/google/android/finsky/layout/SeparatorLinearLayout;
    :cond_7
    return-void
.end method

.method private shouldShowBody1()Z
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->hasBody()Z

    move-result v0

    return v0
.end method

.method private shouldShowBody2()Z
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->hasBody()Z

    move-result v0

    return v0
.end method

.method private shouldShowCallout()Z
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addFadeInAnimatorsIcs(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    const-wide/16 v4, 0x96

    const/16 v3, 0x96

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 401
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowCallout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    new-instance v6, Lcom/google/android/finsky/layout/DetailsExpandedContainer$6;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$6;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v9

    .line 412
    .local v9, "fadeInExpandedCallout":Landroid/animation/Animator;
    invoke-interface {p1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 415
    .end local v9    # "fadeInExpandedCallout":Landroid/animation/Animator;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowBody1()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 417
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    new-instance v6, Lcom/google/android/finsky/layout/DetailsExpandedContainer$7;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$7;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v7

    .line 426
    .local v7, "fadeInExpandedBody1":Landroid/animation/Animator;
    invoke-interface {p1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 429
    .end local v7    # "fadeInExpandedBody1":Landroid/animation/Animator;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowBody2()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 431
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    new-instance v6, Lcom/google/android/finsky/layout/DetailsExpandedContainer$8;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$8;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v8

    .line 440
    .local v8, "fadeInExpandedBody2":Landroid/animation/Animator;
    invoke-interface {p1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 444
    .end local v8    # "fadeInExpandedBody2":Landroid/animation/Animator;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->hasExtras()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 445
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    new-instance v6, Lcom/google/android/finsky/layout/DetailsExpandedContainer$9;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$9;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v10

    .line 454
    .local v10, "fadeInExpandedExtras":Landroid/animation/Animator;
    invoke-interface {p1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 456
    .end local v10    # "fadeInExpandedExtras":Landroid/animation/Animator;
    :cond_3
    return-void
.end method

.method public addFadeOutAnimatorsIcs(Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/animation/Animator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "animators":Ljava/util/List;, "Ljava/util/List<Landroid/animation/Animator;>;"
    const-wide/16 v4, 0x4b

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 513
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v9

    .line 517
    .local v9, "fadeOutExpandedCallout":Landroid/animation/Animator;
    invoke-interface {p1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 520
    .end local v9    # "fadeOutExpandedCallout":Landroid/animation/Animator;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowBody1()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 521
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v7

    .line 524
    .local v7, "fadeOutExpandedBody1":Landroid/animation/Animator;
    invoke-interface {p1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 527
    .end local v7    # "fadeOutExpandedBody1":Landroid/animation/Animator;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowBody2()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 528
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v8

    .line 531
    .local v8, "fadeOutExpandedBody2":Landroid/animation/Animator;
    invoke-interface {p1, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    .end local v8    # "fadeOutExpandedBody2":Landroid/animation/Animator;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->hasExtras()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 535
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-static/range {v0 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeAnimator(Landroid/view/View;FFIJLandroid/animation/Animator$AnimatorListener;)Landroid/animation/Animator;

    move-result-object v10

    .line 538
    .local v10, "fadeOutExpandedExtras":Landroid/animation/Animator;
    invoke-interface {p1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 540
    .end local v10    # "fadeOutExpandedExtras":Landroid/animation/Animator;
    :cond_3
    return-void
.end method

.method public fadeInContentPreIcs()V
    .locals 10

    .prologue
    const-wide/16 v2, 0x96

    .line 342
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 344
    .local v1, "context":Landroid/content/Context;
    new-instance v6, Lcom/google/android/finsky/layout/DetailsExpandedContainer$2;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$2;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v8

    .line 354
    .local v8, "fadeInExpandedCallout":Landroid/view/animation/Animation;
    new-instance v6, Lcom/google/android/finsky/layout/DetailsExpandedContainer$3;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$3;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 364
    .local v0, "fadeInExpandedBody1":Landroid/view/animation/Animation;
    new-instance v6, Lcom/google/android/finsky/layout/DetailsExpandedContainer$4;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$4;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v7

    .line 374
    .local v7, "fadeInExpandedBody2":Landroid/view/animation/Animation;
    new-instance v6, Lcom/google/android/finsky/layout/DetailsExpandedContainer$5;

    invoke-direct {v6, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$5;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeInAnimation(Landroid/content/Context;JJLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v9

    .line 382
    .local v9, "fadeInExpandedExtras":Landroid/view/animation/Animation;
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowCallout()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 383
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 385
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowBody1()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 386
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v2, v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->startAnimation(Landroid/view/animation/Animation;)V

    .line 388
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowBody2()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 389
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v2, v7}, Lcom/google/android/finsky/layout/DetailsTextBlock;->startAnimation(Landroid/view/animation/Animation;)V

    .line 391
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->hasExtras()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 392
    iget-object v2, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v2, v9}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 394
    :cond_3
    return-void
.end method

.method public fadeOutContentPreIcs()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x4b

    .line 462
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 464
    .local v0, "context":Landroid/content/Context;
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    .line 465
    new-instance v5, Lcom/google/android/finsky/layout/DetailsExpandedContainer$10;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$10;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    invoke-static {v0, v6, v7, v5}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v3

    .line 472
    .local v3, "fadeOutExpandedCallout":Landroid/view/animation/Animation;
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 475
    .end local v3    # "fadeOutExpandedCallout":Landroid/view/animation/Animation;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowBody1()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 476
    new-instance v5, Lcom/google/android/finsky/layout/DetailsExpandedContainer$11;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$11;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    invoke-static {v0, v6, v7, v5}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 483
    .local v1, "fadeOutExpandedBody1":Landroid/view/animation/Animation;
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v5, v1}, Lcom/google/android/finsky/layout/DetailsTextBlock;->startAnimation(Landroid/view/animation/Animation;)V

    .line 486
    .end local v1    # "fadeOutExpandedBody1":Landroid/view/animation/Animation;
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowBody2()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 487
    new-instance v5, Lcom/google/android/finsky/layout/DetailsExpandedContainer$12;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$12;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    invoke-static {v0, v6, v7, v5}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v2

    .line 494
    .local v2, "fadeOutExpandedBody2":Landroid/view/animation/Animation;
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v5, v2}, Lcom/google/android/finsky/layout/DetailsTextBlock;->startAnimation(Landroid/view/animation/Animation;)V

    .line 497
    .end local v2    # "fadeOutExpandedBody2":Landroid/view/animation/Animation;
    :cond_2
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->hasExtras()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 498
    new-instance v5, Lcom/google/android/finsky/layout/DetailsExpandedContainer$13;

    invoke-direct {v5, p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer$13;-><init>(Lcom/google/android/finsky/layout/DetailsExpandedContainer;)V

    invoke-static {v0, v6, v7, v5}, Lcom/google/android/finsky/utils/PlayAnimationUtils;->getFadeOutAnimation(Landroid/content/Context;JLandroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v4

    .line 505
    .local v4, "fadeOutExpandedExtras":Landroid/view/animation/Animation;
    iget-object v5, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 507
    .end local v4    # "fadeOutExpandedExtras":Landroid/view/animation/Animation;
    :cond_3
    return-void
.end method

.method public hasTranslation()Z
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mTranslatedBody:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideAllViews()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 297
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 298
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 301
    return-void
.end method

.method public isShowingTranslation()Z
    .locals 1

    .prologue
    .line 570
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mIsShowingTranslation:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 74
    const v0, 0x7f0a0214

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 76
    const v0, 0x7f0a0215

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsTextBlock;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    .line 77
    const v0, 0x7f0a0216

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/DetailsTextBlock;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    .line 78
    const v0, 0x7f0a0217

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    .line 79
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 552
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 553
    check-cast v0, Landroid/os/Bundle;

    .line 554
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "expanded_container.translation_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mIsShowingTranslation:Z

    .line 555
    const-string v1, "expanded_container.parent_instance_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 559
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 558
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 544
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 545
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "expanded_container.parent_instance_state"

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 546
    const-string v1, "expanded_container.translation_state"

    iget-boolean v2, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mIsShowingTranslation:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 547
    return-object v0
.end method

.method public populateFromSection(Lcom/google/android/finsky/layout/DetailsTextSection;Lcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/activities/TextSectionStateListener;)V
    .locals 11
    .param p1, "section"    # Lcom/google/android/finsky/layout/DetailsTextSection;
    .param p2, "navigationManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p3, "backendId"    # I
    .param p4, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p5, "textSectionStateListener"    # Lcom/google/android/finsky/activities/TextSectionStateListener;

    .prologue
    .line 90
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExpandedBody()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mOriginalBody:Ljava/lang/CharSequence;

    .line 91
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExpandedTranslatedBody()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mTranslatedBody:Ljava/lang/CharSequence;

    .line 93
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExpandedCallout()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExpandedCalloutGravity()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 98
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExpandedPromoteWhatsNew()Z

    move-result v9

    .line 101
    .local v9, "promoteWhatsNew":Z
    if-eqz v9, :cond_1

    .line 103
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    .line 104
    .local v8, "bodyForWhatsNew":Lcom/google/android/finsky/layout/DetailsTextBlock;
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    .line 111
    .local v7, "bodyForDescription":Lcom/google/android/finsky/layout/DetailsTextBlock;
    :goto_0
    iput-object v7, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsBodyForTranslation:Lcom/google/android/finsky/layout/DetailsTextBlock;

    .line 113
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExpandedWhatsNewHeader()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExpandedWhatsNew()Ljava/lang/CharSequence;

    move-result-object v1

    const v2, 0x7fffffff

    invoke-virtual {v8, v0, v1, v2}, Lcom/google/android/finsky/layout/DetailsTextBlock;->bind(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 119
    .local v10, "whatsNewVerticalMargin":I
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExpandedBackend()I

    move-result v0

    div-int/lit8 v1, v10, 0x2

    mul-int/lit8 v2, v10, 0x3

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v8, v0, v1, v2}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setCorpusStyle(III)V

    .line 121
    invoke-virtual {v8}, Lcom/google/android/finsky/layout/DetailsTextBlock;->hasBody()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    const/16 v0, 0x8

    invoke-virtual {v8, v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setVisibility(I)V

    .line 125
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/finsky/layout/DetailsTextSection;->getExpandedBodyHeader()Ljava/lang/CharSequence;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mIsShowingTranslation:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mTranslatedBody:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mTranslatedBody:Ljava/lang/CharSequence;

    :goto_1
    const v2, 0x7fffffff

    invoke-virtual {v7, v1, v0, v2}, Lcom/google/android/finsky/layout/DetailsTextBlock;->bind(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 128
    invoke-virtual {v7}, Lcom/google/android/finsky/layout/DetailsTextBlock;->removeCorpusStyle()V

    .line 130
    invoke-virtual {v7}, Lcom/google/android/finsky/layout/DetailsTextBlock;->getPaddingLeft()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v7}, Lcom/google/android/finsky/layout/DetailsTextBlock;->getPaddingRight()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v7, v0, v1, v2, v3}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setPadding(IIII)V

    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 136
    .local v6, "inflater":Landroid/view/LayoutInflater;
    invoke-direct {p0, p1, v6}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->populatePrimaryCredits(Lcom/google/android/finsky/layout/DetailsTextSection;Landroid/view/LayoutInflater;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    .line 137
    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->populatePrimaryExtras(Lcom/google/android/finsky/layout/DetailsTextSection;Lcom/google/android/finsky/navigationmanager/NavigationManager;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Lcom/google/android/finsky/activities/TextSectionStateListener;Landroid/view/LayoutInflater;)V

    .line 139
    invoke-direct {p0, p1, v6}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->populateSecondaryExtras(Lcom/google/android/finsky/layout/DetailsTextSection;Landroid/view/LayoutInflater;)V

    .line 140
    invoke-direct {p0, p1, v6}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->populateAttributions(Lcom/google/android/finsky/layout/DetailsTextSection;Landroid/view/LayoutInflater;)V

    .line 141
    return-void

    .line 107
    .end local v6    # "inflater":Landroid/view/LayoutInflater;
    .end local v7    # "bodyForDescription":Lcom/google/android/finsky/layout/DetailsTextBlock;
    .end local v8    # "bodyForWhatsNew":Lcom/google/android/finsky/layout/DetailsTextBlock;
    .end local v10    # "whatsNewVerticalMargin":I
    :cond_1
    iget-object v7, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    .line 108
    .restart local v7    # "bodyForDescription":Lcom/google/android/finsky/layout/DetailsTextBlock;
    iget-object v8, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    .restart local v8    # "bodyForWhatsNew":Lcom/google/android/finsky/layout/DetailsTextBlock;
    goto :goto_0

    .line 125
    .restart local v10    # "whatsNewVerticalMargin":I
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mOriginalBody:Ljava/lang/CharSequence;

    goto :goto_1
.end method

.method public resetContent()V
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->resetContent()V

    .line 309
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->resetContent()V

    .line 310
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedExtras:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 311
    return-void
.end method

.method public setTopPaddingOnTopView(I)V
    .locals 4
    .param p1, "topPadding"    # I

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowCallout()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedCallout:Landroid/widget/TextView;

    .line 335
    .local v0, "topView":Landroid/view/View;
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 336
    return-void

    .line 333
    .end local v0    # "topView":Landroid/view/View;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->shouldShowBody1()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody1:Lcom/google/android/finsky/layout/DetailsTextBlock;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsExpandedBody2:Lcom/google/android/finsky/layout/DetailsTextBlock;

    goto :goto_0
.end method

.method public toggleTranslation()V
    .locals 2

    .prologue
    .line 576
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mIsShowingTranslation:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mIsShowingTranslation:Z

    .line 577
    iget-object v1, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mDetailsBodyForTranslation:Lcom/google/android/finsky/layout/DetailsTextBlock;

    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mIsShowingTranslation:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mTranslatedBody:Ljava/lang/CharSequence;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/layout/DetailsTextBlock;->setBody(Ljava/lang/CharSequence;)V

    .line 578
    return-void

    .line 576
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 577
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/DetailsExpandedContainer;->mOriginalBody:Ljava/lang/CharSequence;

    goto :goto_1
.end method
