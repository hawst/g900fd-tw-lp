.class public Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;
.super Lcom/google/android/finsky/layout/DetailsSectionStack;
.source "DetailsInnerColumnLayout.java"


# instance fields
.field private mBlockLayout:Z

.field private mExpandCardSections:Z

.field private final mExtraCardSectionWidth:I

.field private mSideMargin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/DetailsSectionStack;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mSideMargin:I

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mExtraCardSectionWidth:I

    .line 55
    return-void
.end method

.method private updateSectionMargins()V
    .locals 9

    .prologue
    .line 101
    const v7, 0x7f0a016a

    invoke-virtual {p0, v7}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/ScreenshotGallery;

    .line 103
    .local v1, "screenshotGallery":Lcom/google/android/finsky/layout/ScreenshotGallery;
    if-eqz v1, :cond_0

    .line 107
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 108
    .local v0, "res":Landroid/content/res/Resources;
    const v7, 0x7f0b010d

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 109
    .local v2, "screenshotsGap":I
    const v7, 0x7f0b010e

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 111
    .local v4, "screenshotsLeadingPeek":I
    iget v7, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mSideMargin:I

    sub-int/2addr v7, v4

    const/4 v8, 0x0

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 112
    .local v3, "screenshotsLeadingMargin":I
    invoke-virtual {v1, v3, v2}, Lcom/google/android/finsky/layout/ScreenshotGallery;->setMargins(II)V

    .line 115
    const v7, 0x7f0a015c

    invoke-virtual {v1, v7}, Lcom/google/android/finsky/layout/ScreenshotGallery;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 116
    .local v5, "stripBackground":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout$LayoutParams;

    .line 118
    .local v6, "stripBackgroundLp":Landroid/widget/FrameLayout$LayoutParams;
    iget v7, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mSideMargin:I

    iput v7, v6, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 119
    iget v7, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mSideMargin:I

    iput v7, v6, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 121
    .end local v0    # "res":Landroid/content/res/Resources;
    .end local v2    # "screenshotsGap":I
    .end local v3    # "screenshotsLeadingMargin":I
    .end local v4    # "screenshotsLeadingPeek":I
    .end local v5    # "stripBackground":Landroid/view/View;
    .end local v6    # "stripBackgroundLp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    return-void
.end method


# virtual methods
.method public blockLayoutRequests()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mBlockLayout:Z

    .line 59
    return-void
.end method

.method public getSideMargin()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mSideMargin:I

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 14
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 184
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getWidth()I

    move-result v9

    .line 186
    .local v9, "width":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildCount()I

    move-result v1

    .line 187
    .local v1, "childCount":I
    const/4 v8, 0x0

    .line 188
    .local v8, "visibleChildrenSoFar":I
    const/4 v10, 0x0

    .line 189
    .local v10, "y":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v1, :cond_2

    .line 190
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 191
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_0

    .line 189
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 194
    :cond_0
    add-int/lit8 v8, v8, 0x1

    .line 196
    instance-of v11, v0, Lcom/google/android/finsky/layout/DetailsPeekingSection;

    if-eqz v11, :cond_1

    const/4 v11, 0x1

    if-le v8, v11, :cond_1

    move-object v11, v0

    .line 201
    check-cast v11, Lcom/google/android/finsky/layout/DetailsPeekingSection;

    invoke-interface {v11}, Lcom/google/android/finsky/layout/DetailsPeekingSection;->getTopPeekAmount()I

    move-result v7

    .line 202
    .local v7, "thumbnailPeek":I
    sub-int/2addr v10, v7

    .line 205
    .end local v7    # "thumbnailPeek":I
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 206
    .local v2, "childHeight":I
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 210
    .local v4, "childWidth":I
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 211
    .local v3, "childLp":Landroid/widget/LinearLayout$LayoutParams;
    iget v11, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    sub-int v12, v9, v4

    iget v13, v3, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v12, v13

    iget v13, v3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v12, v13

    div-int/lit8 v12, v12, 0x2

    add-int v5, v11, v12

    .line 213
    .local v5, "childX":I
    add-int v11, v5, v4

    add-int v12, v10, v2

    invoke-virtual {v0, v5, v10, v11, v12}, Landroid/view/View;->layout(IIII)V

    .line 215
    add-int/2addr v10, v2

    goto :goto_1

    .line 217
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "childHeight":I
    .end local v3    # "childLp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v4    # "childWidth":I
    .end local v5    # "childX":I
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 14
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 125
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v10

    .line 126
    .local v10, "width":I
    const/4 v5, 0x0

    .line 129
    .local v5, "height":I
    iget v11, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mSideMargin:I

    mul-int/lit8 v11, v11, 0x2

    sub-int v7, v10, v11

    .line 132
    .local v7, "mainContentWidth":I
    move v0, v7

    .line 133
    .local v0, "cardSectionWidth":I
    iget-boolean v11, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mExpandCardSections:Z

    if-eqz v11, :cond_0

    .line 134
    iget v11, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mExtraCardSectionWidth:I

    add-int/2addr v0, v11

    .line 137
    :cond_0
    const/4 v9, 0x0

    .line 138
    .local v9, "visibleChildrenSoFar":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildCount()I

    move-result v2

    .line 139
    .local v2, "childCount":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v2, :cond_5

    .line 140
    invoke-virtual {p0, v6}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 141
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v11

    const/16 v12, 0x8

    if-ne v11, v12, :cond_1

    .line 139
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 144
    :cond_1
    add-int/lit8 v9, v9, 0x1

    .line 145
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 146
    .local v4, "childLp":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v11, 0x0

    const/4 v12, 0x0

    iget v13, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-static {v11, v12, v13}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->getChildMeasureSpec(III)I

    move-result v3

    .line 148
    .local v3, "childHeightSpec":I
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v11

    const v12, 0x7f0a016a

    if-ne v11, v12, :cond_3

    .line 150
    invoke-virtual {v1, p1, v3}, Landroid/view/View;->measure(II)V

    .line 167
    :goto_2
    instance-of v11, v1, Lcom/google/android/finsky/layout/DetailsPeekingSection;

    if-eqz v11, :cond_2

    const/4 v11, 0x1

    if-le v9, v11, :cond_2

    move-object v11, v1

    .line 172
    check-cast v11, Lcom/google/android/finsky/layout/DetailsPeekingSection;

    invoke-interface {v11}, Lcom/google/android/finsky/layout/DetailsPeekingSection;->getTopPeekAmount()I

    move-result v8

    .line 173
    .local v8, "thumbnailPeek":I
    sub-int/2addr v5, v8

    .line 176
    .end local v8    # "thumbnailPeek":I
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v5, v11

    goto :goto_1

    .line 151
    :cond_3
    instance-of v11, v1, Lcom/google/android/finsky/layout/DetailsPackSection;

    if-eqz v11, :cond_4

    .line 153
    iget v11, v4, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    sub-int v11, v0, v11

    iget v12, v4, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v1, v11, v3}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 160
    :cond_4
    iget v11, v4, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    sub-int v11, v7, v11

    iget v12, v4, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v11, v12

    const/high16 v12, 0x40000000    # 2.0f

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v1, v11, v3}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 179
    .end local v1    # "child":Landroid/view/View;
    .end local v3    # "childHeightSpec":I
    .end local v4    # "childLp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_5
    invoke-virtual {p0, v10, v5}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->setMeasuredDimension(II)V

    .line 180
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mBlockLayout:Z

    if-nez v0, :cond_0

    .line 68
    invoke-super {p0}, Lcom/google/android/finsky/layout/DetailsSectionStack;->requestLayout()V

    .line 70
    :cond_0
    return-void
.end method

.method public setExpandCardSections(Z)V
    .locals 1
    .param p1, "expandCardSections"    # Z

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mExpandCardSections:Z

    if-eq v0, p1, :cond_0

    .line 84
    iput-boolean p1, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mExpandCardSections:Z

    .line 85
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->requestLayout()V

    .line 87
    :cond_0
    return-void
.end method

.method public setSideMargin(I)V
    .locals 1
    .param p1, "sideMargin"    # I

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mSideMargin:I

    if-eq v0, p1, :cond_0

    .line 77
    iput p1, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mSideMargin:I

    .line 78
    invoke-direct {p0}, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->updateSectionMargins()V

    .line 80
    :cond_0
    return-void
.end method

.method public unblockLayoutRequests()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/DetailsInnerColumnLayout;->mBlockLayout:Z

    .line 63
    return-void
.end method
