.class public Lcom/google/android/finsky/layout/play/PlayRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;
.source "PlayRecyclerView.java"


# instance fields
.field private mEmptyView:Landroid/view/View;

.field private mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/play/PlayRecyclerView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/layout/play/PlayRecyclerView;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->updateEmptyStatus()V

    return-void
.end method

.method private updateEmptyStatus()V
    .locals 6

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 63
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    .line 64
    .local v0, "adapter":Landroid/support/v7/widget/RecyclerView$Adapter;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    const/4 v1, 0x1

    .line 65
    .local v1, "empty":Z
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->mEmptyView:Landroid/view/View;

    if-eqz v3, :cond_1

    .line 66
    iget-object v5, p0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->mEmptyView:Landroid/view/View;

    if-eqz v1, :cond_3

    move v3, v2

    :goto_1
    invoke-virtual {v5, v3}, Landroid/view/View;->setVisibility(I)V

    .line 68
    :cond_1
    if-eqz v1, :cond_4

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->mEmptyView:Landroid/view/View;

    if-eqz v3, :cond_4

    :goto_2
    invoke-virtual {p0, v4}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->setVisibility(I)V

    .line 69
    return-void

    .end local v1    # "empty":Z
    :cond_2
    move v1, v2

    .line 64
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_3
    move v3, v4

    .line 66
    goto :goto_1

    :cond_4
    move v4, v2

    .line 68
    goto :goto_2
.end method


# virtual methods
.method public restoreInstanceState(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 35
    return-void
.end method

.method public saveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 30
    invoke-super {p0}, Landroid/support/v7/widget/RecyclerView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 2
    .param p1, "emptyView"    # Landroid/view/View;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->mEmptyView:Landroid/view/View;

    .line 43
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->updateEmptyStatus()V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    .line 45
    .local v0, "adapter":Landroid/support/v7/widget/RecyclerView$Adapter;
    if-eqz v0, :cond_1

    .line 46
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    if-eqz v1, :cond_0

    .line 47
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->unregisterAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;)V

    .line 48
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    .line 51
    :cond_0
    new-instance v1, Lcom/google/android/finsky/layout/play/PlayRecyclerView$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/play/PlayRecyclerView$1;-><init>(Lcom/google/android/finsky/layout/play/PlayRecyclerView;)V

    iput-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    .line 58
    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayRecyclerView;->mObserver:Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView$Adapter;->registerAdapterDataObserver(Landroid/support/v7/widget/RecyclerView$AdapterDataObserver;)V

    .line 60
    :cond_1
    return-void
.end method
