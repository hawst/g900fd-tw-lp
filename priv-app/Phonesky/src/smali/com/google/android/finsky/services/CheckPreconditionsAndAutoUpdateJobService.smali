.class public Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;
.super Landroid/app/job/JobService;
.source "CheckPreconditionsAndAutoUpdateJobService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/job/JobService;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;Landroid/app/job/JobParameters;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;
    .param p1, "x1"    # Landroid/app/job/JobParameters;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;->onLoaded(Landroid/app/job/JobParameters;)V

    return-void
.end method

.method private getRetryReason(Landroid/app/job/JobParameters;)I
    .locals 3
    .param p1, "parameters"    # Landroid/app/job/JobParameters;

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->getExtras()Landroid/os/PersistableBundle;

    move-result-object v0

    .line 98
    .local v0, "extras":Landroid/os/PersistableBundle;
    if-eqz v0, :cond_0

    const-string v2, "Finksy.AutoUpdateRetryReason"

    invoke-virtual {v0, v2, v1}, Landroid/os/PersistableBundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    :cond_0
    return v1
.end method

.method private onLoaded(Landroid/app/job/JobParameters;)V
    .locals 8
    .param p1, "jobParameters"    # Landroid/app/job/JobParameters;

    .prologue
    const/4 v7, 0x1

    .line 58
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->isOverrideDeadlineExpired()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;->getRetryReason(Landroid/app/job/JobParameters;)I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->isProjecting()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    invoke-virtual {p0, p1, v7}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 89
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    .line 66
    .local v1, "finskyApp":Lcom/google/android/finsky/FinskyApp;
    new-instance v0, Lcom/google/android/finsky/appstate/UpdateChecker;

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstallPolicies()Lcom/google/android/finsky/installer/InstallPolicies;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getInstaller()Lcom/google/android/finsky/receivers/Installer;

    move-result-object v5

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getNotifier()Lcom/google/android/finsky/utils/Notifier;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/finsky/appstate/UpdateChecker;-><init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/appstate/AppStates;Lcom/google/android/finsky/installer/InstallPolicies;Lcom/google/android/finsky/receivers/Installer;Lcom/google/android/finsky/utils/Notifier;)V

    .line 73
    .local v0, "updateChecker":Lcom/google/android/finsky/appstate/UpdateChecker;
    new-instance v2, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService$2;

    invoke-direct {v2, p0, p1}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService$2;-><init>(Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;Landroid/app/job/JobParameters;)V

    new-instance v3, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService$3;

    invoke-direct {v3, p0, p1}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService$3;-><init>(Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;Landroid/app/job/JobParameters;)V

    const-string v4, "wifi_checker"

    invoke-virtual {v0, v2, v3, v4, v7}, Lcom/google/android/finsky/appstate/UpdateChecker;->checkForUpdates(Ljava/lang/Runnable;Ljava/lang/Runnable;Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public onStartJob(Landroid/app/job/JobParameters;)Z
    .locals 5
    .param p1, "jobParameters"    # Landroid/app/job/JobParameters;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 27
    invoke-virtual {p1}, Landroid/app/job/JobParameters;->isOverrideDeadlineExpired()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;->getRetryReason(Landroid/app/job/JobParameters;)I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_0

    .line 30
    const-string v3, "Timed out waiting for job to be scheduled."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    invoke-virtual {p0, p1, v2}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 52
    :goto_0
    return v1

    .line 36
    :cond_0
    const-string v3, "JobScheduler invoked, loading libraries."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    new-instance v0, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService$1;-><init>(Lcom/google/android/finsky/services/CheckPreconditionsAndAutoUpdateJobService;Landroid/app/job/JobParameters;)V

    .line 49
    .local v0, "continueRunnable":Ljava/lang/Runnable;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/library/Libraries;->load(Ljava/lang/Runnable;)V

    .line 50
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/finsky/appstate/AppStates;->load(Ljava/lang/Runnable;)Z

    .line 51
    invoke-static {v0}, Lcom/google/android/finsky/gearhead/GearheadStateMonitor;->initialize(Ljava/lang/Runnable;)V

    move v1, v2

    .line 52
    goto :goto_0
.end method

.method public onStopJob(Landroid/app/job/JobParameters;)Z
    .locals 1
    .param p1, "jobParameters"    # Landroid/app/job/JobParameters;

    .prologue
    .line 93
    const/4 v0, 0x0

    return v0
.end method
