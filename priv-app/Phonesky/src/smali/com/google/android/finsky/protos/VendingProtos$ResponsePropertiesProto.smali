.class public final Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ResponsePropertiesProto"
.end annotation


# instance fields
.field public hasResult:Z

.field public result:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2782
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2783
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    .line 2784
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2787
    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->result:I

    .line 2788
    iput-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->hasResult:Z

    .line 2789
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->cachedSize:I

    .line 2790
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2804
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2805
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->result:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->hasResult:Z

    if-eqz v1, :cond_1

    .line 2806
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->result:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2809
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2817
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2818
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2822
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2823
    :sswitch_0
    return-object p0

    .line 2828
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2829
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2835
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->result:I

    .line 2836
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->hasResult:Z

    goto :goto_0

    .line 2818
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    .line 2829
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2754
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2796
    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->result:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->hasResult:Z

    if-eqz v0, :cond_1

    .line 2797
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$ResponsePropertiesProto;->result:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2799
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2800
    return-void
.end method
