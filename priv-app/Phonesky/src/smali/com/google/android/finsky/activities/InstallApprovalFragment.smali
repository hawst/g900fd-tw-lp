.class public Lcom/google/android/finsky/activities/InstallApprovalFragment;
.super Landroid/support/v4/app/Fragment;
.source "InstallApprovalFragment.java"


# instance fields
.field private mContentTextView:Landroid/widget/TextView;

.field private mPermissionsLearnMoreHeader:Landroid/view/View;

.field private mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

.field private mProgressTextView:Landroid/widget/TextView;

.field private mSavedInstanceState:Landroid/os/Bundle;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private displayInfo()V
    .locals 17

    .prologue
    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/InstallApprovalFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 107
    .local v11, "resources":Landroid/content/res/Resources;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/InstallApprovalFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 108
    .local v4, "arguments":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mProgressTextView:Landroid/widget/TextView;

    const v13, 0x7f0c0294

    const/4 v14, 0x2

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "InstallApprovalFragment.installNumber"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const-string v16, "InstallApprovalFragment.totalInstalls"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v11, v13, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    const-string v12, "InstallApprovalFragment.approvalType"

    invoke-virtual {v4, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 111
    .local v3, "approvalType":I
    packed-switch v3, :pswitch_data_0

    .line 150
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/InstallApprovalFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "InstallApprovalFragment.showPermissionsLearnMoreHeader"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 151
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsLearnMoreHeader:Landroid/view/View;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/view/View;->setVisibility(I)V

    .line 152
    sget-object v12, Lcom/google/android/finsky/config/G;->permissionBucketsLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v12}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 153
    .local v8, "learnMoreLink":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 154
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsLearnMoreHeader:Landroid/view/View;

    new-instance v13, Lcom/google/android/finsky/activities/InstallApprovalFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v8}, Lcom/google/android/finsky/activities/InstallApprovalFragment$1;-><init>(Lcom/google/android/finsky/activities/InstallApprovalFragment;Ljava/lang/String;)V

    invoke-virtual {v12, v13}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    .end local v8    # "learnMoreLink":Ljava/lang/String;
    :cond_0
    return-void

    .line 113
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mTitleView:Landroid/widget/TextView;

    const v13, 0x7f0c0290

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 114
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mContentTextView:Landroid/widget/TextView;

    const v13, 0x7f0c0291

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "InstallApprovalFragment.packageTitle"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v11, v13, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mContentTextView:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 118
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->setVisibility(I)V

    goto :goto_0

    .line 121
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mTitleView:Landroid/widget/TextView;

    const v13, 0x7f0c0292

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 122
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mContentTextView:Landroid/widget/TextView;

    const v13, 0x7f0c0293

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const-string v16, "InstallApprovalFragment.packageTitle"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-virtual {v11, v13, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mContentTextView:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 126
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    const/16 v13, 0x8

    invoke-virtual {v12, v13}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->setVisibility(I)V

    goto/16 :goto_0

    .line 129
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mTitleView:Landroid/widget/TextView;

    const v13, 0x7f0c0279

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(I)V

    .line 130
    const-string v12, "InstallApprovalFragment.packageName"

    invoke-virtual {v4, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 131
    .local v9, "packageName":Ljava/lang/String;
    const-string v12, "InstallApprovalFragment.permissions"

    invoke-virtual {v4, v12}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 133
    .local v10, "permissions":[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/finsky/FinskyApp;->getAppStates()Lcom/google/android/finsky/appstate/AppStates;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/android/finsky/appstate/AppStates;->getInstallerDataStore()Lcom/google/android/finsky/appstate/InstallerDataStore;

    move-result-object v12

    invoke-interface {v12, v9}, Lcom/google/android/finsky/appstate/InstallerDataStore;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;

    move-result-object v7

    .line 135
    .local v7, "installerData":Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;
    if-eqz v7, :cond_1

    invoke-virtual {v7}, Lcom/google/android/finsky/appstate/InstallerDataStore$InstallerData;->getPermissionsVersion()I

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_1

    const/4 v5, 0x1

    .line 138
    .local v5, "hasAcceptedBuckets":Z
    :goto_1
    new-instance v1, Lcom/google/android/finsky/layout/AppPermissionAdapter;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/InstallApprovalFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    invoke-direct {v1, v12, v9, v10, v5}, Lcom/google/android/finsky/layout/AppPermissionAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 140
    .local v1, "adapter":Lcom/google/android/finsky/layout/AppPermissionAdapter;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/activities/InstallApprovalFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v12

    const-string v13, "InstallApprovalFragment.packageTitle"

    invoke-virtual {v12, v13}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 141
    .local v2, "appName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-virtual {v12, v1, v2, v13}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->bindInfo(Lcom/google/android/finsky/layout/PermissionAdapter;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 142
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->setVisibility(I)V

    .line 143
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mContentTextView:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    invoke-virtual {v1}, Lcom/google/android/finsky/layout/AppPermissionAdapter;->isAppInstalled()Z

    move-result v12

    if-eqz v12, :cond_2

    if-eqz v5, :cond_2

    const v6, 0x7f0c0284

    .line 146
    .local v6, "headerId":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mContentTextView:Landroid/widget/TextView;

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v2, v13, v14

    invoke-virtual {v11, v6, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 135
    .end local v1    # "adapter":Lcom/google/android/finsky/layout/AppPermissionAdapter;
    .end local v2    # "appName":Ljava/lang/String;
    .end local v5    # "hasAcceptedBuckets":Z
    .end local v6    # "headerId":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 144
    .restart local v1    # "adapter":Lcom/google/android/finsky/layout/AppPermissionAdapter;
    .restart local v2    # "appName":Ljava/lang/String;
    .restart local v5    # "hasAcceptedBuckets":Z
    :cond_2
    const v6, 0x7f0c0282

    goto :goto_2

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static newInstance(Ljava/lang/String;Ljava/lang/String;III[Ljava/lang/String;Z)Lcom/google/android/finsky/activities/InstallApprovalFragment;
    .locals 3
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "packageTitle"    # Ljava/lang/String;
    .param p2, "installNumber"    # I
    .param p3, "totalInstalls"    # I
    .param p4, "approvalType"    # I
    .param p5, "permissions"    # [Ljava/lang/String;
    .param p6, "showLearnMoreHeader"    # Z

    .prologue
    .line 62
    new-instance v1, Lcom/google/android/finsky/activities/InstallApprovalFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/activities/InstallApprovalFragment;-><init>()V

    .line 63
    .local v1, "installApprovalFragment":Lcom/google/android/finsky/activities/InstallApprovalFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "InstallApprovalFragment.packageName"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v2, "InstallApprovalFragment.packageTitle"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v2, "InstallApprovalFragment.installNumber"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 67
    const-string v2, "InstallApprovalFragment.totalInstalls"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    const-string v2, "InstallApprovalFragment.approvalType"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    const-string v2, "InstallApprovalFragment.showPermissionsLearnMoreHeader"

    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 70
    const-string v2, "InstallApprovalFragment.permissions"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 71
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/activities/InstallApprovalFragment;->setArguments(Landroid/os/Bundle;)V

    .line 72
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 78
    iput-object p1, p0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mSavedInstanceState:Landroid/os/Bundle;

    .line 79
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    const v1, 0x7f0400b8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 94
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0a009c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mTitleView:Landroid/widget/TextView;

    .line 95
    const v1, 0x7f0a0227

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mProgressTextView:Landroid/widget/TextView;

    .line 96
    const v1, 0x7f0a0228

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mContentTextView:Landroid/widget/TextView;

    .line 97
    const v1, 0x7f0a02b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsLearnMoreHeader:Landroid/view/View;

    .line 98
    const v1, 0x7f0a0229

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/layout/AppSecurityPermissions;

    iput-object v1, p0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    .line 100
    invoke-direct {p0}, Lcom/google/android/finsky/activities/InstallApprovalFragment;->displayInfo()V

    .line 102
    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/finsky/activities/InstallApprovalFragment;->mPermissionsView:Lcom/google/android/finsky/layout/AppSecurityPermissions;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/AppSecurityPermissions;->saveInstanceState(Landroid/os/Bundle;)V

    .line 87
    :cond_0
    return-void
.end method
