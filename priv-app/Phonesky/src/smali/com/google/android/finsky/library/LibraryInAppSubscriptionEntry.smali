.class public Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;
.super Lcom/google/android/finsky/library/LibrarySubscriptionEntry;
.source "LibraryInAppSubscriptionEntry.java"


# instance fields
.field public final signature:Ljava/lang/String;

.field public final signedPurchaseData:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJJJZLjava/lang/String;Ljava/lang/String;)V
    .locals 18
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "libraryId"    # Ljava/lang/String;
    .param p3, "backend"    # I
    .param p4, "docId"    # Ljava/lang/String;
    .param p5, "offerType"    # I
    .param p6, "documentHash"    # J
    .param p8, "validUntilTimestampMs"    # J
    .param p10, "initiationTimestampMs"    # J
    .param p12, "trialUntilTimestampMs"    # J
    .param p14, "autoRenewing"    # Z
    .param p15, "signedPurchaseData"    # Ljava/lang/String;
    .param p16, "signature"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move-wide/from16 v9, p6

    move-wide/from16 v12, p10

    move-wide/from16 v14, p12

    move/from16 v16, p14

    invoke-direct/range {v3 .. v16}, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJLjava/lang/Long;JJZ)V

    .line 24
    move-object/from16 v0, p15

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->signedPurchaseData:Ljava/lang/String;

    .line 25
    move-object/from16 v0, p16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;->signature:Ljava/lang/String;

    .line 26
    return-void
.end method
