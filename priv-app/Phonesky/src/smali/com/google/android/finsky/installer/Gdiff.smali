.class public Lcom/google/android/finsky/installer/Gdiff;
.super Ljava/lang/Object;
.source "Gdiff.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/installer/Gdiff$FileFormatException;
    }
.end annotation


# direct methods
.method private static copyFromOriginal([BLjava/io/RandomAccessFile;Ljava/io/OutputStream;JI)V
    .locals 7
    .param p0, "buffer"    # [B
    .param p1, "inputFile"    # Ljava/io/RandomAccessFile;
    .param p2, "output"    # Ljava/io/OutputStream;
    .param p3, "inputOffset"    # J
    .param p5, "copyLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x4000

    .line 179
    if-gez p5, :cond_0

    .line 180
    new-instance v2, Ljava/io/IOException;

    const-string v3, "copyLength negative"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 182
    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v3, p3, v4

    if-gez v3, :cond_1

    .line 183
    new-instance v2, Ljava/io/IOException;

    const-string v3, "inputOffset negative"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 186
    :cond_1
    :try_start_0
    invoke-virtual {p1, p3, p4}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 187
    :goto_0
    if-lez p5, :cond_3

    .line 188
    if-ge p5, v2, :cond_2

    move v1, p5

    .line 189
    .local v1, "spanLength":I
    :goto_1
    const/4 v3, 0x0

    invoke-virtual {p1, p0, v3, v1}, Ljava/io/RandomAccessFile;->readFully([BII)V

    .line 190
    const/4 v3, 0x0

    invoke-virtual {p2, p0, v3, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    sub-int/2addr p5, v1

    .line 192
    goto :goto_0

    .end local v1    # "spanLength":I
    :cond_2
    move v1, v2

    .line 188
    goto :goto_1

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Ljava/io/EOFException;
    new-instance v2, Ljava/io/IOException;

    const-string v3, "patch underrun"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 196
    .end local v0    # "e":Ljava/io/EOFException;
    :cond_3
    return-void
.end method

.method private static copyFromPatch([BLjava/io/DataInputStream;Ljava/io/OutputStream;I)V
    .locals 4
    .param p0, "buffer"    # [B
    .param p1, "patchDataStream"    # Ljava/io/DataInputStream;
    .param p2, "output"    # Ljava/io/OutputStream;
    .param p3, "copyLength"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0x4000

    .line 159
    if-gez p3, :cond_0

    .line 160
    new-instance v2, Ljava/io/IOException;

    const-string v3, "copyLength negative"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 163
    :cond_0
    :goto_0
    if-lez p3, :cond_2

    .line 164
    if-ge p3, v2, :cond_1

    move v1, p3

    .line 165
    .local v1, "spanLength":I
    :goto_1
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, p0, v3, v1}, Ljava/io/DataInputStream;->readFully([BII)V

    .line 166
    const/4 v3, 0x0

    invoke-virtual {p2, p0, v3, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    sub-int/2addr p3, v1

    .line 168
    goto :goto_0

    .end local v1    # "spanLength":I
    :cond_1
    move v1, v2

    .line 164
    goto :goto_1

    .line 169
    .restart local v1    # "spanLength":I
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Ljava/io/EOFException;
    new-instance v2, Ljava/io/IOException;

    const-string v3, "patch underrun"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 172
    .end local v0    # "e":Ljava/io/EOFException;
    .end local v1    # "spanLength":I
    :cond_2
    return-void
.end method

.method public static patch(Ljava/io/RandomAccessFile;Ljava/io/InputStream;Ljava/io/OutputStream;J)J
    .locals 19
    .param p0, "inputFile"    # Ljava/io/RandomAccessFile;
    .param p1, "patchFile"    # Ljava/io/InputStream;
    .param p2, "output"    # Ljava/io/OutputStream;
    .param p3, "maxOutputLength"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    const/16 v4, 0x4000

    new-array v3, v4, [B

    .line 63
    .local v3, "buffer":[B
    const-wide/16 v10, 0x0

    .line 66
    .local v10, "outputSize":J
    new-instance v13, Ljava/io/BufferedInputStream;

    const/16 v4, 0x100

    move-object/from16 v0, p1

    invoke-direct {v13, v0, v4}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    .line 68
    .end local p1    # "patchFile":Ljava/io/InputStream;
    .local v13, "patchFile":Ljava/io/InputStream;
    new-instance v12, Ljava/io/DataInputStream;

    invoke-direct {v12, v13}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 71
    .local v12, "patchDataStream":Ljava/io/DataInputStream;
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v9

    .line 72
    .local v9, "magic":I
    const v4, -0x2e002e01

    if-eq v9, v4, :cond_0

    .line 73
    new-instance v4, Lcom/google/android/finsky/installer/Gdiff$FileFormatException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Unexpected magic="

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v15, "%x"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/finsky/installer/Gdiff$FileFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 76
    :cond_0
    invoke-virtual {v12}, Ljava/io/DataInputStream;->read()I

    move-result v14

    .line 77
    .local v14, "version":I
    const/4 v4, 0x4

    if-eq v14, v4, :cond_1

    .line 78
    new-instance v4, Lcom/google/android/finsky/installer/Gdiff$FileFormatException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Unexpected version="

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/finsky/installer/Gdiff$FileFormatException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 85
    :cond_1
    invoke-virtual {v12}, Ljava/io/DataInputStream;->read()I

    move-result v2

    .line 86
    .local v2, "command":I
    sparse-switch v2, :sswitch_data_0

    .line 143
    move v8, v2

    .line 144
    .local v8, "copyLength":I
    move-object/from16 v0, p2

    invoke-static {v3, v12, v0, v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromPatch([BLjava/io/DataInputStream;Ljava/io/OutputStream;I)V

    .line 147
    :goto_0
    int-to-long v4, v8

    add-long/2addr v10, v4

    .line 148
    cmp-long v4, v10, p3

    if-lez v4, :cond_1

    .line 149
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Output length overrun"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 88
    .end local v8    # "copyLength":I
    :sswitch_0
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Patch file overrun"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 92
    :sswitch_1
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v8

    .line 93
    .restart local v8    # "copyLength":I
    move-object/from16 v0, p2

    invoke-static {v3, v12, v0, v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromPatch([BLjava/io/DataInputStream;Ljava/io/OutputStream;I)V

    goto :goto_0

    .line 96
    .end local v8    # "copyLength":I
    :sswitch_2
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    .line 97
    .restart local v8    # "copyLength":I
    move-object/from16 v0, p2

    invoke-static {v3, v12, v0, v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromPatch([BLjava/io/DataInputStream;Ljava/io/OutputStream;I)V

    goto :goto_0

    .line 100
    .end local v8    # "copyLength":I
    :sswitch_3
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v4

    int-to-long v6, v4

    .line 101
    .local v6, "copyOffset":J
    invoke-virtual {v12}, Ljava/io/DataInputStream;->read()I

    move-result v8

    .line 102
    .restart local v8    # "copyLength":I
    const/4 v4, -0x1

    if-ne v8, v4, :cond_2

    .line 103
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Unexpected end of patch"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    .line 105
    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromOriginal([BLjava/io/RandomAccessFile;Ljava/io/OutputStream;JI)V

    goto :goto_0

    .line 108
    .end local v6    # "copyOffset":J
    .end local v8    # "copyLength":I
    :sswitch_4
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v4

    int-to-long v6, v4

    .line 109
    .restart local v6    # "copyOffset":J
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v8

    .restart local v8    # "copyLength":I
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    .line 110
    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromOriginal([BLjava/io/RandomAccessFile;Ljava/io/OutputStream;JI)V

    goto :goto_0

    .line 113
    .end local v6    # "copyOffset":J
    .end local v8    # "copyLength":I
    :sswitch_5
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v4

    int-to-long v6, v4

    .line 114
    .restart local v6    # "copyOffset":J
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    .restart local v8    # "copyLength":I
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    .line 115
    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromOriginal([BLjava/io/RandomAccessFile;Ljava/io/OutputStream;JI)V

    goto :goto_0

    .line 118
    .end local v6    # "copyOffset":J
    .end local v8    # "copyLength":I
    :sswitch_6
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    int-to-long v6, v4

    .line 119
    .restart local v6    # "copyOffset":J
    invoke-virtual {v12}, Ljava/io/DataInputStream;->read()I

    move-result v8

    .line 120
    .restart local v8    # "copyLength":I
    const/4 v4, -0x1

    if-ne v8, v4, :cond_3

    .line 121
    new-instance v4, Ljava/io/IOException;

    const-string v5, "Unexpected end of patch"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_3
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    .line 123
    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromOriginal([BLjava/io/RandomAccessFile;Ljava/io/OutputStream;JI)V

    goto/16 :goto_0

    .line 126
    .end local v6    # "copyOffset":J
    .end local v8    # "copyLength":I
    :sswitch_7
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    int-to-long v6, v4

    .line 127
    .restart local v6    # "copyOffset":J
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-result v8

    .restart local v8    # "copyLength":I
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    .line 128
    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromOriginal([BLjava/io/RandomAccessFile;Ljava/io/OutputStream;JI)V

    goto/16 :goto_0

    .line 131
    .end local v6    # "copyOffset":J
    .end local v8    # "copyLength":I
    :sswitch_8
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    int-to-long v6, v4

    .line 132
    .restart local v6    # "copyOffset":J
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    .restart local v8    # "copyLength":I
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    .line 133
    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromOriginal([BLjava/io/RandomAccessFile;Ljava/io/OutputStream;JI)V

    goto/16 :goto_0

    .line 136
    .end local v6    # "copyOffset":J
    .end local v8    # "copyLength":I
    :sswitch_9
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readLong()J

    move-result-wide v6

    .line 137
    .restart local v6    # "copyOffset":J
    invoke-virtual {v12}, Ljava/io/DataInputStream;->readInt()I

    move-result v8

    .restart local v8    # "copyLength":I
    move-object/from16 v4, p0

    move-object/from16 v5, p2

    .line 138
    invoke-static/range {v3 .. v8}, Lcom/google/android/finsky/installer/Gdiff;->copyFromOriginal([BLjava/io/RandomAccessFile;Ljava/io/OutputStream;JI)V

    goto/16 :goto_0

    .line 90
    .end local v6    # "copyOffset":J
    .end local v8    # "copyLength":I
    :sswitch_a
    return-wide v10

    .line 86
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_a
        0xf7 -> :sswitch_1
        0xf8 -> :sswitch_2
        0xf9 -> :sswitch_3
        0xfa -> :sswitch_4
        0xfb -> :sswitch_5
        0xfc -> :sswitch_6
        0xfd -> :sswitch_7
        0xfe -> :sswitch_8
        0xff -> :sswitch_9
    .end sparse-switch
.end method
