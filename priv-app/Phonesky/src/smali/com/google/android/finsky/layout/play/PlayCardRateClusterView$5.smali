.class Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$5;
.super Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;
.source "PlayCardRateClusterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->transitionToEmptyState(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$5;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    invoke-direct {p0}, Lcom/google/android/finsky/utils/PlayAnimationUtils$AnimationListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$5;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    iget-object v0, v0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mContent:Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/PlayCardClusterViewContent;->setVisibility(I)V

    .line 271
    sget-boolean v0, Lcom/google/android/finsky/layout/play/PlayListView;->ENABLE_ANIMATION:Z

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$5;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    iget-object v1, p0, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView$5;->this$0:Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;

    # getter for: Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->mClusterFadeOutListener:Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;
    invoke-static {v1}, Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;->access$400(Lcom/google/android/finsky/layout/play/PlayCardRateClusterView;)Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;

    move-result-object v1

    const-wide/16 v2, 0x9c4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/UiUtils;->fadeOutCluster(Landroid/view/View;Lcom/google/android/finsky/utils/UiUtils$ClusterFadeOutListener;J)V

    .line 275
    :cond_0
    return-void
.end method
