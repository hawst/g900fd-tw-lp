.class public final Lcom/google/android/finsky/protos/BillingAddress$Address;
.super Lcom/google/protobuf/nano/MessageNano;
.source "BillingAddress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/BillingAddress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Address"
.end annotation


# instance fields
.field public addressLine1:Ljava/lang/String;

.field public addressLine2:Ljava/lang/String;

.field public city:Ljava/lang/String;

.field public dependentLocality:Ljava/lang/String;

.field public deprecatedIsReduced:Z

.field public email:Ljava/lang/String;

.field public firstName:Ljava/lang/String;

.field public hasAddressLine1:Z

.field public hasAddressLine2:Z

.field public hasCity:Z

.field public hasDependentLocality:Z

.field public hasDeprecatedIsReduced:Z

.field public hasEmail:Z

.field public hasFirstName:Z

.field public hasLanguageCode:Z

.field public hasLastName:Z

.field public hasName:Z

.field public hasPhoneNumber:Z

.field public hasPostalCode:Z

.field public hasPostalCountry:Z

.field public hasSortingCode:Z

.field public hasState:Z

.field public languageCode:Ljava/lang/String;

.field public lastName:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public phoneNumber:Ljava/lang/String;

.field public postalCode:Ljava/lang/String;

.field public postalCountry:Ljava/lang/String;

.field public sortingCode:Ljava/lang/String;

.field public state:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/BillingAddress$Address;->clear()Lcom/google/android/finsky/protos/BillingAddress$Address;

    .line 87
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    .line 91
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasName:Z

    .line 92
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->firstName:Ljava/lang/String;

    .line 93
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasFirstName:Z

    .line 94
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->lastName:Ljava/lang/String;

    .line 95
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLastName:Z

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    .line 97
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine1:Z

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine2:Ljava/lang/String;

    .line 99
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine2:Z

    .line 100
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    .line 101
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasCity:Z

    .line 102
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->state:Ljava/lang/String;

    .line 103
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasState:Z

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    .line 105
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCode:Z

    .line 106
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    .line 107
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCountry:Z

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->dependentLocality:Ljava/lang/String;

    .line 109
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDependentLocality:Z

    .line 110
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->sortingCode:Ljava/lang/String;

    .line 111
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasSortingCode:Z

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->languageCode:Ljava/lang/String;

    .line 113
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLanguageCode:Z

    .line 114
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    .line 115
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPhoneNumber:Z

    .line 116
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->email:Ljava/lang/String;

    .line 117
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasEmail:Z

    .line 118
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->deprecatedIsReduced:Z

    .line 119
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDeprecatedIsReduced:Z

    .line 120
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->cachedSize:I

    .line 121
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 177
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 178
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasName:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 179
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine1:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 183
    :cond_2
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine2:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine2:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 187
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine2:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 190
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasCity:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 191
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasState:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->state:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 195
    :cond_8
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->state:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCode:Z

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 199
    :cond_a
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 202
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCountry:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 203
    :cond_c
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 206
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDependentLocality:Z

    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->dependentLocality:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 207
    :cond_e
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->dependentLocality:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasSortingCode:Z

    if-nez v1, :cond_10

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->sortingCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    .line 211
    :cond_10
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->sortingCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 214
    :cond_11
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLanguageCode:Z

    if-nez v1, :cond_12

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->languageCode:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    .line 215
    :cond_12
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->languageCode:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    :cond_13
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPhoneNumber:Z

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    .line 219
    :cond_14
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    :cond_15
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDeprecatedIsReduced:Z

    if-nez v1, :cond_16

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->deprecatedIsReduced:Z

    if-eqz v1, :cond_17

    .line 223
    :cond_16
    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->deprecatedIsReduced:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 226
    :cond_17
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasFirstName:Z

    if-nez v1, :cond_18

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->firstName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_19

    .line 227
    :cond_18
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->firstName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    :cond_19
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLastName:Z

    if-nez v1, :cond_1a

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->lastName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1b

    .line 231
    :cond_1a
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->lastName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_1b
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasEmail:Z

    if-nez v1, :cond_1c

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->email:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 235
    :cond_1c
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->email:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    :cond_1d
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 3
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 246
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 247
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 251
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 252
    :sswitch_0
    return-object p0

    .line 257
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    .line 258
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasName:Z

    goto :goto_0

    .line 262
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    .line 263
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine1:Z

    goto :goto_0

    .line 267
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine2:Ljava/lang/String;

    .line 268
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine2:Z

    goto :goto_0

    .line 272
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    .line 273
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasCity:Z

    goto :goto_0

    .line 277
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->state:Ljava/lang/String;

    .line 278
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasState:Z

    goto :goto_0

    .line 282
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    .line 283
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCode:Z

    goto :goto_0

    .line 287
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    .line 288
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCountry:Z

    goto :goto_0

    .line 292
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->dependentLocality:Ljava/lang/String;

    .line 293
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDependentLocality:Z

    goto :goto_0

    .line 297
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->sortingCode:Ljava/lang/String;

    .line 298
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasSortingCode:Z

    goto :goto_0

    .line 302
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->languageCode:Ljava/lang/String;

    .line 303
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLanguageCode:Z

    goto :goto_0

    .line 307
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    .line 308
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPhoneNumber:Z

    goto :goto_0

    .line 312
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->deprecatedIsReduced:Z

    .line 313
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDeprecatedIsReduced:Z

    goto :goto_0

    .line 317
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->firstName:Ljava/lang/String;

    .line 318
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasFirstName:Z

    goto/16 :goto_0

    .line 322
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->lastName:Ljava/lang/String;

    .line 323
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLastName:Z

    goto/16 :goto_0

    .line 327
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->email:Ljava/lang/String;

    .line 328
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasEmail:Z

    goto/16 :goto_0

    .line 247
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/BillingAddress$Address;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasName:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 128
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->name:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 130
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine1:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 131
    :cond_2
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine1:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 133
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasAddressLine2:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine2:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 134
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->addressLine2:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 136
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasCity:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 137
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->city:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 139
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasState:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->state:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 140
    :cond_8
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->state:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 142
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCode:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 143
    :cond_a
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 145
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPostalCountry:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 146
    :cond_c
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 148
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDependentLocality:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->dependentLocality:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 149
    :cond_e
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->dependentLocality:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 151
    :cond_f
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasSortingCode:Z

    if-nez v0, :cond_10

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->sortingCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 152
    :cond_10
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->sortingCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 154
    :cond_11
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLanguageCode:Z

    if-nez v0, :cond_12

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->languageCode:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 155
    :cond_12
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->languageCode:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 157
    :cond_13
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasPhoneNumber:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 158
    :cond_14
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->phoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 160
    :cond_15
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasDeprecatedIsReduced:Z

    if-nez v0, :cond_16

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->deprecatedIsReduced:Z

    if-eqz v0, :cond_17

    .line 161
    :cond_16
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->deprecatedIsReduced:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 163
    :cond_17
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasFirstName:Z

    if-nez v0, :cond_18

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->firstName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_19

    .line 164
    :cond_18
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->firstName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 166
    :cond_19
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasLastName:Z

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->lastName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 167
    :cond_1a
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->lastName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 169
    :cond_1b
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->hasEmail:Z

    if-nez v0, :cond_1c

    iget-object v0, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->email:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1d

    .line 170
    :cond_1c
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/finsky/protos/BillingAddress$Address;->email:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 172
    :cond_1d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 173
    return-void
.end method
