.class Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;
.super Ljava/lang/Object;
.source "CreditCardNumberEditText.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/layout/CreditCardNumberEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NumberFormatter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;


# direct methods
.method private constructor <init>(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/finsky/layout/CreditCardNumberEditText;Lcom/google/android/finsky/layout/CreditCardNumberEditText$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/finsky/layout/CreditCardNumberEditText;
    .param p2, "x1"    # Lcom/google/android/finsky/layout/CreditCardNumberEditText$1;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;-><init>(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 12
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v11, 0x0

    .line 77
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "input":Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->getTypeForPrefix(Ljava/lang/String;)Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    move-result-object v3

    .line 82
    .local v3, "newType":Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    if-eqz v3, :cond_3

    move-object v0, v3

    .line 85
    .local v0, "formatType":Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    :goto_0
    invoke-static {v2}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->normalizeNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->limitLength(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 87
    .local v4, "normalizedLenghtLimited":Ljava/lang/String;
    invoke-virtual {v0, v4}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "formatted":Ljava/lang/String;
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 89
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v8

    invoke-interface {p1, v11, v8, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 92
    :cond_0
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    # getter for: Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    invoke-static {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->access$100(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    move-result-object v8

    if-eq v8, v3, :cond_1

    .line 93
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    # getter for: Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    invoke-static {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->access$100(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    move-result-object v5

    .line 94
    .local v5, "oldType":Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    # setter for: Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mCurrentType:Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    invoke-static {v8, v3}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->access$102(Lcom/google/android/finsky/layout/CreditCardNumberEditText;Lcom/google/android/finsky/billing/creditcard/CreditCardType;)Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    .line 97
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    # getter for: Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOnCreditCardTypeChangedListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;
    invoke-static {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->access$200(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 98
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    # getter for: Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOnCreditCardTypeChangedListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;
    invoke-static {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->access$200(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;

    move-result-object v8

    invoke-interface {v8, v5, v3}, Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnCreditCardTypeChangedListener;->onCreditCardTypeChanged(Lcom/google/android/finsky/billing/creditcard/CreditCardType;Lcom/google/android/finsky/billing/creditcard/CreditCardType;)V

    .line 102
    .end local v5    # "oldType":Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    :cond_1
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v8

    iget v9, v0, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->length:I

    if-ne v8, v9, :cond_5

    .line 103
    invoke-virtual {v0, v4}, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->isValidNumber(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 104
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    # getter for: Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOnNumberEnteredListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;
    invoke-static {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->access$300(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 105
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    # getter for: Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOnNumberEnteredListener:Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;
    invoke-static {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->access$300(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText$OnValidNumberEnteredListener;->onNumberEntered()V

    .line 124
    :cond_2
    :goto_1
    return-void

    .line 82
    .end local v0    # "formatType":Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    .end local v1    # "formatted":Ljava/lang/String;
    .end local v4    # "normalizedLenghtLimited":Ljava/lang/String;
    :cond_3
    sget-object v0, Lcom/google/android/finsky/billing/creditcard/CreditCardType;->MC:Lcom/google/android/finsky/billing/creditcard/CreditCardType;

    goto :goto_0

    .line 108
    .restart local v0    # "formatType":Lcom/google/android/finsky/billing/creditcard/CreditCardType;
    .restart local v1    # "formatted":Ljava/lang/String;
    .restart local v4    # "normalizedLenghtLimited":Ljava/lang/String;
    :cond_4
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    iget-object v9, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v9}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0900c5

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setTextColor(I)V

    .line 109
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-static {v8, v9}, Lcom/google/android/finsky/utils/UiUtils;->playShakeAnimationIfPossible(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 113
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 114
    .local v6, "res":Landroid/content/res/Resources;
    const v8, 0x7f0c03ad

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const v10, 0x7f0c00a9

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v11

    const/4 v10, 0x1

    const v11, 0x7f0c0129

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v6, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 118
    .local v7, "textToAnnounce":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-virtual {v8}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    invoke-static {v8, v7, v9}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    goto :goto_1

    .line 122
    .end local v6    # "res":Landroid/content/res/Resources;
    .end local v7    # "textToAnnounce":Ljava/lang/String;
    :cond_5
    iget-object v8, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    iget-object v9, p0, Lcom/google/android/finsky/layout/CreditCardNumberEditText$NumberFormatter;->this$0:Lcom/google/android/finsky/layout/CreditCardNumberEditText;

    # getter for: Lcom/google/android/finsky/layout/CreditCardNumberEditText;->mOriginalTextColors:Landroid/content/res/ColorStateList;
    invoke-static {v9}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->access$400(Lcom/google/android/finsky/layout/CreditCardNumberEditText;)Landroid/content/res/ColorStateList;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/finsky/layout/CreditCardNumberEditText;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_1
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 128
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 132
    return-void
.end method
