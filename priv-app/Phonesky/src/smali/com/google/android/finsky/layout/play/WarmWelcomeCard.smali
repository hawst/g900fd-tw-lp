.class public Lcom/google/android/finsky/layout/play/WarmWelcomeCard;
.super Lcom/google/android/finsky/layout/IdentifiableFrameLayout;
.source "WarmWelcomeCard.java"

# interfaces
.implements Lcom/google/android/finsky/adapters/Recyclable;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mBody:Landroid/widget/TextView;

.field private mButtonDismiss:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

.field private mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

.field private mButtonSeparator:Landroid/view/View;

.field private final mDefaultTitleTopPadding:I

.field private mGraphic:Lcom/google/android/play/image/FifeImageView;

.field private mGraphicBox:Landroid/view/View;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mSupportsDynamicTitleTopPadding:Z

.field private mTitle:Landroid/widget/TextView;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    sget-object v1, Lcom/android/vending/R$styleable;->WarmWelcomeCard:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 58
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mSupportsDynamicTitleTopPadding:Z

    .line 60
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0173

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mDefaultTitleTopPadding:I

    .line 64
    return-void
.end method

.method private syncButtonAlignment()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonDismiss:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->alignStart()V

    .line 154
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonDismiss:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->alignCenter()V

    .line 152
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->alignCenter()V

    goto :goto_0
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 177
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public configureContent(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/finsky/protos/Common$Image;ILcom/google/android/finsky/layout/play/PlayStoreUiElementNode;[B)V
    .locals 7
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "body"    # Ljava/lang/CharSequence;
    .param p3, "graphic"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p4, "backendId"    # I
    .param p5, "parentNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .param p6, "serverLogsCookie"    # [B

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 82
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 83
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mBody:Landroid/widget/TextView;

    invoke-virtual {v3, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    if-eqz p4, :cond_0

    const/16 v3, 0x9

    if-eq p4, v3, :cond_0

    move v0, v1

    .line 86
    .local v0, "isContentCorpus":Z
    :goto_0
    if-eqz p3, :cond_2

    .line 87
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mGraphicBox:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 88
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mGraphic:Lcom/google/android/play/image/FifeImageView;

    iget-object v4, p3, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-boolean v5, p3, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 90
    if-eqz v0, :cond_1

    .line 91
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mGraphicBox:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p4}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getPrimaryColor(Landroid/content/Context;I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    .line 107
    :goto_1
    if-eqz p3, :cond_3

    if-nez v0, :cond_3

    iget-boolean v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mSupportsDynamicTitleTopPadding:Z

    if-eqz v3, :cond_3

    .line 109
    .local v1, "setTitleTopPaddingToZero":Z
    :goto_2
    if-eqz v1, :cond_4

    .line 110
    .local v2, "titleTopPadding":I
    :goto_3
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v3, v4, v2, v5, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 114
    const/16 v3, 0x204

    invoke-static {v3}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 116
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    invoke-static {v3, p6}, Lcom/google/android/finsky/analytics/FinskyEventLog;->setServerLogCookie(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;[B)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    .line 117
    iput-object p5, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 120
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v3

    invoke-interface {v3, p0}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 121
    return-void

    .end local v0    # "isContentCorpus":Z
    .end local v1    # "setTitleTopPaddingToZero":Z
    .end local v2    # "titleTopPadding":I
    :cond_0
    move v0, v2

    .line 84
    goto :goto_0

    .line 97
    .restart local v0    # "isContentCorpus":Z
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mGraphicBox:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 100
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mGraphicBox:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v1, v2

    .line 107
    goto :goto_2

    .line 109
    .restart local v1    # "setTitleTopPaddingToZero":Z
    :cond_4
    iget v2, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mDefaultTitleTopPadding:I

    goto :goto_3
.end method

.method public configureDismissAction(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "buttonText"    # Ljava/lang/String;
    .param p2, "buttonIcon"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p3, "dismissCallback"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonDismiss:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->configure(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/play/image/BitmapLoader;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonDismiss:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0, p3}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->syncButtonAlignment()V

    .line 143
    return-void
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public hideSecondaryAction()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 133
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSeparator:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 134
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->setVisibility(I)V

    .line 135
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->syncButtonAlignment()V

    .line 136
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0}, Lcom/google/android/finsky/layout/IdentifiableFrameLayout;->onFinishInflate()V

    .line 70
    const v0, 0x7f0a03c2

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mTitle:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f0a03c3

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mBody:Landroid/widget/TextView;

    .line 72
    const v0, 0x7f0a03bf

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mGraphicBox:Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mGraphicBox:Landroid/view/View;

    const v1, 0x7f0a03c0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mGraphic:Lcom/google/android/play/image/FifeImageView;

    .line 74
    const v0, 0x7f0a03be

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonDismiss:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    .line 75
    const v0, 0x7f0a03bc

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    .line 76
    const v0, 0x7f0a03bd

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSeparator:Landroid/view/View;

    .line 77
    return-void
.end method

.method public onRecycle()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mGraphic:Lcom/google/android/play/image/FifeImageView;

    invoke-virtual {v0}, Lcom/google/android/play/image/FifeImageView;->clearImage()V

    .line 159
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonDismiss:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->onRecycle()V

    .line 160
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->onRecycle()V

    .line 161
    return-void
.end method

.method public showSecondaryButton(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "buttonText"    # Ljava/lang/String;
    .param p2, "buttonIcon"    # Lcom/google/android/finsky/protos/Common$Image;
    .param p3, "actionCallback"    # Landroid/view/View$OnClickListener;

    .prologue
    const/4 v1, 0x0

    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSeparator:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 126
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->setVisibility(I)V

    .line 127
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->configure(Ljava/lang/String;Lcom/google/android/finsky/protos/Common$Image;Lcom/google/android/play/image/BitmapLoader;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    invoke-virtual {v0, p3}, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    invoke-direct {p0}, Lcom/google/android/finsky/layout/play/WarmWelcomeCard;->syncButtonAlignment()V

    .line 130
    return-void
.end method
