.class public Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;
.super Landroid/support/v4/app/Fragment;
.source "AddressChallengeFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;
    }
.end annotation


# instance fields
.field private mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

.field private mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

.field private mCancelButton:Landroid/widget/Button;

.field private mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

.field private mCountries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

.field private mMainView:Landroid/view/ViewGroup;

.field private mPreviousState:Landroid/os/Bundle;

.field private mSaveButton:Landroid/widget/Button;

.field private mSavedInstanceState:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 401
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;)Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;)Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;)Lcom/google/android/finsky/layout/BillingAddress;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;)Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->saveMyState(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->onBillingCountriesLoaded()V

    return-void
.end method

.method private clearErrorMessages()V
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0}, Lcom/google/android/finsky/layout/BillingAddress;->clearErrorMessage()V

    .line 234
    return-void
.end method

.method private displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;
    .locals 1
    .param p1, "error"    # Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v0, p1}, Lcom/google/android/finsky/layout/BillingAddress;->displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method private displayErrors(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 328
    .local p1, "inputValidationErrors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;>;"
    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->clearErrorMessages()V

    .line 330
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 331
    .local v1, "errorFields":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/widget/TextView;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;

    .line 332
    .local v0, "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->displayError(Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;)Landroid/widget/TextView;

    move-result-object v3

    .line 333
    .local v3, "textView":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    .line 334
    invoke-interface {v1, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 338
    .end local v0    # "error":Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;
    .end local v3    # "textView":Landroid/widget/TextView;
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-static {v5, v1}, Lcom/google/android/finsky/billing/BillingUtils;->getTopMostView(Landroid/view/ViewGroup;Ljava/util/Collection;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 339
    .local v4, "topMostErrorField":Landroid/widget/TextView;
    if-eqz v4, :cond_2

    .line 340
    invoke-virtual {v4}, Landroid/widget/TextView;->requestFocus()Z

    .line 342
    :cond_2
    return-void
.end method

.method private getAddressOrShowErrors()Lcom/google/android/finsky/protos/BillingAddress$Address;
    .locals 2

    .prologue
    .line 360
    iget-object v1, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/BillingAddress;->getAddressValidationErrors()Ljava/util/List;

    move-result-object v0

    .line 361
    .local v0, "validationErrors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/protos/ChallengeProto$InputValidationError;>;"
    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->displayErrors(Ljava/util/List;)V

    .line 362
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 363
    iget-object v1, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/BillingAddress;->getAddress()Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v1

    .line 365
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getCheckboxState()[Z
    .locals 6

    .prologue
    .line 312
    iget-object v4, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v1, v4

    .line 313
    .local v1, "checkboxCount":I
    new-array v3, v1, [Z

    .line 314
    .local v3, "result":[Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 315
    iget-object v4, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 316
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    aput-boolean v4, v3, v2

    .line 314
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 318
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    :cond_0
    return-object v3
.end method

.method private initializeCountriesFromChallenge()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 178
    iget-object v5, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v5, v5, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    array-length v5, v5

    invoke-static {v5}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCountries:Ljava/util/List;

    .line 179
    iget-object v5, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v0, v5, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    .local v0, "arr$":[Lcom/google/android/finsky/protos/ChallengeProto$Country;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v2, v0, v1

    .line 180
    .local v2, "inCountry":Lcom/google/android/finsky/protos/ChallengeProto$Country;
    new-instance v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    invoke-direct {v4}, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;-><init>()V

    .line 181
    .local v4, "outCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    iget-object v5, v2, Lcom/google/android/finsky/protos/ChallengeProto$Country;->regionCode:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryCode:Ljava/lang/String;

    .line 182
    iput-boolean v6, v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->hasCountryCode:Z

    .line 183
    iget-object v5, v2, Lcom/google/android/finsky/protos/ChallengeProto$Country;->displayName:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->countryName:Ljava/lang/String;

    .line 184
    iput-boolean v6, v4, Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;->hasCountryName:Z

    .line 185
    iget-object v5, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCountries:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 187
    .end local v2    # "inCountry":Lcom/google/android/finsky/protos/ChallengeProto$Country;
    .end local v4    # "outCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->syncContinueButton()V

    .line 188
    iget-object v5, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0, v5}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->setupWidgets(Landroid/os/Bundle;)V

    .line 189
    return-void
.end method

.method private loadBillingCountries()V
    .locals 4

    .prologue
    .line 192
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    if-eqz v2, :cond_0

    .line 193
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    invoke-interface {v2}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;->onInitializing()V

    .line 195
    :cond_0
    new-instance v1, Lcom/google/android/finsky/billing/GetBillingCountriesAction;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/GetBillingCountriesAction;-><init>()V

    .line 196
    .local v1, "getBillingCountriesAction":Lcom/google/android/finsky/billing/GetBillingCountriesAction;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "authAccount"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "account":Ljava/lang/String;
    new-instance v2, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$3;

    invoke-direct {v2, p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$3;-><init>(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/finsky/billing/GetBillingCountriesAction;->run(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 203
    return-void
.end method

.method public static newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;Landroid/os/Bundle;)Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;
    .locals 4
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "addressChallenge"    # Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;
    .param p2, "previousState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    new-instance v1, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;-><init>()V

    .line 72
    .local v1, "result":Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 73
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    const-string v2, "address_challenge"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 75
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->setArguments(Landroid/os/Bundle;)V

    .line 78
    iput-object p2, v1, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mPreviousState:Landroid/os/Bundle;

    .line 79
    return-object v1
.end method

.method private onBillingCountriesLoaded()V
    .locals 5

    .prologue
    .line 206
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_0

    .line 227
    :goto_0
    return-void

    .line 209
    :cond_0
    invoke-static {}, Lcom/google/android/finsky/billing/BillingLocator;->getBillingCountries()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCountries:Ljava/util/List;

    .line 210
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCountries:Ljava/util/List;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCountries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 211
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    if-eqz v2, :cond_1

    .line 212
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    invoke-interface {v2}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;->onInitialized()V

    .line 214
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->syncContinueButton()V

    .line 215
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mSavedInstanceState:Landroid/os/Bundle;

    invoke-direct {p0, v2}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->setupWidgets(Landroid/os/Bundle;)V

    goto :goto_0

    .line 218
    :cond_2
    const-string v2, "BillingCountries not loaded."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 219
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 220
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v2, 0x7f0c00ea

    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c01d7

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0c0134

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v2, 0x0

    check-cast v2, Landroid/os/Bundle;

    invoke-virtual {v3, p0, v4, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 224
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 225
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "error"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private saveMyState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 243
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    .line 244
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v3, v3, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 245
    .local v0, "checkbox":Landroid/widget/CheckBox;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "checkbox_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 243
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 247
    .end local v0    # "checkbox":Landroid/widget/CheckBox;
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    if-eqz v2, :cond_1

    .line 248
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/layout/BillingAddress;->saveInstanceState(Landroid/os/Bundle;)V

    .line 250
    :cond_1
    return-void
.end method

.method private setupWidgets(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 253
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    iget-object v3, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCountries:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/google/android/finsky/layout/BillingAddress;->setBillingCountries(Ljava/util/List;)V

    .line 254
    if-eqz p1, :cond_0

    .line 255
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    invoke-virtual {v2, p1}, Lcom/google/android/finsky/layout/BillingAddress;->restoreInstanceState(Landroid/os/Bundle;)V

    .line 277
    :goto_0
    return-void

    .line 257
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    iget-object v2, v2, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 259
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v2, v2, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    iget-object v2, v2, Lcom/google/android/finsky/protos/BillingAddress$Address;->postalCountry:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCountries:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/BillingUtils;->findCountry(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    move-result-object v0

    .line 261
    .local v0, "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    iget-object v3, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    iget-object v4, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->address:Lcom/google/android/finsky/protos/BillingAddress$Address;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/finsky/layout/BillingAddress;->setAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;Lcom/google/android/finsky/protos/BillingAddress$Address;)V

    .line 268
    .end local v0    # "country":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    :goto_1
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$4;

    invoke-direct {v3, p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$4;-><init>(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 263
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/BillingUtils;->getDefaultCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCountries:Ljava/util/List;

    invoke-static {v2, v3}, Lcom/google/android/finsky/billing/BillingUtils;->findCountry(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;

    move-result-object v1

    .line 265
    .local v1, "defaultCountry":Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;
    iget-object v2, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    iget-object v3, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-virtual {v2, v1, v3}, Lcom/google/android/finsky/layout/BillingAddress;->setAddressSpec(Lcom/google/android/finsky/protos/VendingProtos$PurchaseMetadataResponseProto$Countries$Country;Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;)V

    goto :goto_1
.end method

.method private syncContinueButton()V
    .locals 5

    .prologue
    .line 280
    const/4 v1, 0x1

    .line 281
    .local v1, "enabled":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v4, v4

    if-ge v3, v4, :cond_2

    .line 282
    iget-object v4, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v4, v4, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    aget-object v2, v4, v3

    .line 283
    .local v2, "formCheckbox":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    iget-object v4, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v4, v2}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 284
    .local v0, "checkBox":Landroid/widget/CheckBox;
    if-eqz v1, :cond_1

    iget-boolean v4, v2, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->required:Z

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 281
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 284
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 286
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    .end local v2    # "formCheckbox":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    :cond_2
    iget-object v4, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mSaveButton:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 287
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0
    .param p1, "compoundButton"    # Landroid/widget/CompoundButton;
    .param p2, "b"    # Z

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->syncContinueButton()V

    .line 382
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 295
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 309
    :cond_0
    :goto_0
    return-void

    .line 297
    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->getAddressOrShowErrors()Lcom/google/android/finsky/protos/BillingAddress$Address;

    move-result-object v0

    .line 298
    .local v0, "address":Lcom/google/android/finsky/protos/BillingAddress$Address;
    if-eqz v0, :cond_0

    .line 301
    iget-object v1, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    sget-object v2, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener$Result;->SUCCESS:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener$Result;

    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->getCheckboxState()[Z

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;->onAddressChallengeResult(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener$Result;Lcom/google/android/finsky/protos/BillingAddress$Address;[Z)V

    goto :goto_0

    .line 305
    .end local v0    # "address":Lcom/google/android/finsky/protos/BillingAddress$Address;
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    sget-object v2, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener$Result;->CANCELED:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener$Result;

    invoke-interface {v1, v2, v3, v3}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;->onAddressChallengeResult(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener$Result;Lcom/google/android/finsky/protos/BillingAddress$Address;[Z)V

    goto :goto_0

    .line 295
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0a00f8 -> :sswitch_0
        0x7f0a0115 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 14
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 85
    if-nez p2, :cond_0

    .line 86
    const/4 v10, 0x0

    .line 174
    :goto_0
    return-object v10

    .line 88
    :cond_0
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mSavedInstanceState:Landroid/os/Bundle;

    .line 90
    const v10, 0x7f04002e

    const/4 v11, 0x0

    move-object/from16 v0, p2

    invoke-virtual {p1, v10, v0, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    iput-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    .line 92
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v10

    const-string v11, "address_challenge"

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iput-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 93
    new-instance v10, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    invoke-direct {v10}, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;-><init>()V

    iput-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    .line 94
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    array-length v8, v10

    .line 95
    .local v8, "oldLength":I
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    new-array v11, v8, [I

    iput-object v11, v10, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    .line 96
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->requiredField:[I

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mAddressSpec:Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;

    iget-object v12, v12, Lcom/google/android/finsky/protos/CommonDevice$BillingAddressSpec;->requiredField:[I

    const/4 v13, 0x0

    invoke-static {v10, v11, v12, v13, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 100
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->errorHtml:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    if-nez p3, :cond_1

    .line 103
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    new-instance v11, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$1;

    invoke-direct {v11, p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$1;-><init>(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;)V

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 112
    :cond_1
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v11, 0x7f0a00f1

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 113
    .local v9, "title":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 114
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->title:Ljava/lang/String;

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    :goto_1
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v11, 0x7f0a00f2

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 121
    .local v4, "description":Landroid/widget/TextView;
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 122
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->descriptionHtml:Ljava/lang/String;

    invoke-static {v10}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 129
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 130
    .local v7, "layoutInflater":Landroid/view/LayoutInflater;
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v11, 0x7f0a00b4

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 131
    .local v3, "contentView":Landroid/view/ViewGroup;
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v10

    add-int/lit8 v1, v10, 0x1

    .line 132
    .local v1, "baseIndex":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    array-length v10, v10

    if-ge v6, v10, :cond_6

    .line 133
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->checkbox:[Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;

    aget-object v5, v10, v6

    .line 134
    .local v5, "formCheckbox":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    const v10, 0x7f04002d

    iget-object v11, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const/4 v12, 0x0

    invoke-virtual {v7, v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 136
    .local v2, "checkBox":Landroid/widget/CheckBox;
    iget-object v10, v5, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->description:Ljava/lang/String;

    invoke-virtual {v2, v10}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 137
    invoke-virtual {v2, v5}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 138
    if-nez p3, :cond_4

    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mPreviousState:Landroid/os/Bundle;

    if-nez v10, :cond_4

    .line 139
    iget-boolean v10, v5, Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;->checked:Z

    invoke-virtual {v2, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 145
    :goto_4
    invoke-virtual {v2, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 146
    add-int v10, v1, v6

    invoke-virtual {v3, v2, v10}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 132
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 116
    .end local v1    # "baseIndex":I
    .end local v2    # "checkBox":Landroid/widget/CheckBox;
    .end local v3    # "contentView":Landroid/view/ViewGroup;
    .end local v4    # "description":Landroid/widget/TextView;
    .end local v5    # "formCheckbox":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    .end local v6    # "i":I
    .end local v7    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_2
    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 125
    .restart local v4    # "description":Landroid/widget/TextView;
    :cond_3
    const/16 v10, 0x8

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 140
    .restart local v1    # "baseIndex":I
    .restart local v2    # "checkBox":Landroid/widget/CheckBox;
    .restart local v3    # "contentView":Landroid/view/ViewGroup;
    .restart local v5    # "formCheckbox":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    .restart local v6    # "i":I
    .restart local v7    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_4
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mPreviousState:Landroid/os/Bundle;

    if-eqz v10, :cond_5

    .line 141
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mPreviousState:Landroid/os/Bundle;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "checkbox_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    invoke-virtual {v2, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_4

    .line 143
    :cond_5
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkbox_"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v10

    invoke-virtual {v2, v10}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_4

    .line 149
    .end local v2    # "checkBox":Landroid/widget/CheckBox;
    .end local v5    # "formCheckbox":Lcom/google/android/finsky/protos/ChallengeProto$FormCheckbox;
    :cond_6
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v11, 0x7f0a00e6

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/google/android/finsky/layout/BillingAddress;

    iput-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    .line 150
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mBillingAddress:Lcom/google/android/finsky/layout/BillingAddress;

    new-instance v11, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$2;

    invoke-direct {v11, p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$2;-><init>(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;)V

    invoke-virtual {v10, v11}, Lcom/google/android/finsky/layout/BillingAddress;->setBillingCountryChangeListener(Lcom/google/android/finsky/layout/BillingAddress$BillingCountryChangeListener;)V

    .line 162
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v11, 0x7f0a00f8

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mSaveButton:Landroid/widget/Button;

    .line 163
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mSaveButton:Landroid/widget/Button;

    invoke-virtual {v10, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mSaveButton:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setEnabled(Z)V

    .line 165
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mSaveButton:Landroid/widget/Button;

    const v11, 0x7f0c020d

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(I)V

    .line 166
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    const v11, 0x7f0a0115

    invoke-virtual {v10, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCancelButton:Landroid/widget/Button;

    .line 167
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCancelButton:Landroid/widget/Button;

    invoke-virtual {v10, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mCancelButton:Landroid/widget/Button;

    const v11, 0x7f0c0134

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setText(I)V

    .line 169
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    iget-object v10, v10, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;->supportedCountry:[Lcom/google/android/finsky/protos/ChallengeProto$Country;

    array-length v10, v10

    if-lez v10, :cond_7

    .line 170
    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->initializeCountriesFromChallenge()V

    .line 174
    :goto_5
    iget-object v10, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mMainView:Landroid/view/ViewGroup;

    goto/16 :goto_0

    .line 172
    :cond_7
    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->loadBillingCountries()V

    goto :goto_5
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 393
    iget-object v0, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    sget-object v1, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener$Result;->CANCELED:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener$Result;

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;->onAddressChallengeResult(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener$Result;Lcom/google/android/finsky/protos/BillingAddress$Address;[Z)V

    .line 395
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 388
    invoke-direct {p0}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->loadBillingCountries()V

    .line 389
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 238
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 239
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->saveMyState(Landroid/os/Bundle;)V

    .line 240
    return-void
.end method

.method public setOnResultListener(Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment;->mListener:Lcom/google/android/finsky/billing/challenge/AddressChallengeFragment$AddressChallengeResultListener;

    .line 291
    return-void
.end method
