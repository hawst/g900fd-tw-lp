.class public Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;
.super Lcom/google/android/finsky/fragments/LoggingFragment;
.source "UsernamePasswordFormFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final mEmptyTextWatcher:Landroid/text/TextWatcher;

.field private mFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

.field private mLearnMoreButton:Landroid/view/View;

.field private mLoginButton:Lcom/google/android/play/layout/PlayActionButton;

.field private mMainView:Landroid/view/ViewGroup;

.field private mPasswordEditText:Landroid/widget/EditText;

.field private mProblemWithLoginTextView:Landroid/widget/TextView;

.field private mTosTextView:Landroid/widget/TextView;

.field private mUsernameEditText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;-><init>()V

    .line 66
    new-instance v0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment$1;-><init>(Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->syncLoginButtonState()V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->handleLoginClick(Z)V

    return-void
.end method

.method private buildCreateInstrumentRequest()Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 259
    iget-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    iget-object v0, v3, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    .line 261
    .local v0, "form":Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;-><init>()V

    .line 262
    .local v1, "input":Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;
    iget-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mUsernameEditText:Landroid/widget/EditText;

    iget-object v4, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-static {v3, v4}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentUtils;->createTextFormInput(Landroid/widget/EditText;Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;)Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->username:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    .line 264
    iget-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    iget-object v4, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-static {v3, v4}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentUtils;->createTextFormInput(Landroid/widget/EditText;Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;)Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->password:Lcom/google/android/finsky/protos/CreateInstrument$TextFormInput;

    .line 267
    new-instance v3, Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    invoke-direct {v3}, Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;-><init>()V

    iput-object v3, v1, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    .line 268
    iget-object v3, v1, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    iget-object v4, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    iget-object v4, v4, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosId:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;->tosId:Ljava/lang/String;

    .line 269
    iget-object v3, v1, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;->tos:Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/google/android/finsky/protos/CreateInstrument$TosAcceptanceFormInput;->hasTosId:Z

    .line 271
    new-instance v2, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;

    invoke-direct {v2}, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;-><init>()V

    .line 272
    .local v2, "request":Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;
    iget-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    iget-object v3, v3, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->handle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    iput-object v3, v2, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->flowHandle:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowHandle;

    .line 273
    iput-object v1, v2, Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;->usernamePasswordFormInput:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordFormInput;

    .line 274
    return-object v2
.end method

.method private fromHtml(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 214
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/finsky/utils/Utils;->trim(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private handleLoginClick(Z)V
    .locals 6
    .param p1, "isImeAction"    # Z

    .prologue
    const/4 v5, 0x1

    .line 227
    iget-object v4, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mUsernameEditText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    const/4 v0, 0x0

    .line 233
    .local v0, "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    if-eqz p1, :cond_2

    .line 234
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .end local v0    # "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 235
    .restart local v0    # "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    iput-boolean v5, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    .line 236
    iput-boolean v5, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasIsImeAction:Z

    .line 238
    :cond_2
    const/16 v4, 0x5fc

    invoke-virtual {p0, v4, v0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->logClickEvent(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;)V

    .line 242
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->buildCreateInstrumentRequest()Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 251
    .local v3, "request":Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    invoke-static {v4, v5}, Lcom/google/android/finsky/utils/UiUtils;->hideKeyboard(Landroid/app/Activity;Landroid/view/View;)V

    .line 253
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;

    .line 255
    .local v2, "parent":Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;
    invoke-virtual {v2, v3}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;->createInstrument(Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;)V

    goto :goto_0

    .line 243
    .end local v2    # "parent":Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentHostFragment;
    .end local v3    # "request":Lcom/google/android/finsky/protos/BuyInstruments$CreateInstrumentRequest;
    :catch_0
    move-exception v1

    .line 247
    .local v1, "e":Ljava/security/cert/CertificateException;
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Unable to construct CreateInstrumentRequest"

    invoke-direct {v4, v5, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
.end method

.method public static newInstance(Ljava/lang/String;Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;)Landroid/support/v4/app/Fragment;
    .locals 4
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "flowState"    # Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    .prologue
    .line 83
    new-instance v1, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;-><init>()V

    .line 84
    .local v1, "fragment":Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 85
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const-string v2, "UsernamePasswordFlowFragment.flow_state"

    invoke-static {p1}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 87
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->setArguments(Landroid/os/Bundle;)V

    .line 88
    return-object v1
.end method

.method private syncLoginButtonState()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 219
    iget-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mUsernameEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v2

    .line 221
    .local v0, "isEmpty":Z
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mLoginButton:Lcom/google/android/play/layout/PlayActionButton;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/play/layout/PlayActionButton;->setEnabled(Z)V

    .line 222
    return-void

    .end local v0    # "isEmpty":Z
    :cond_1
    move v0, v1

    .line 219
    goto :goto_0

    .restart local v0    # "isEmpty":Z
    :cond_2
    move v2, v1

    .line 221
    goto :goto_1
.end method


# virtual methods
.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 279
    const/16 v0, 0x5fa

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mLoginButton:Lcom/google/android/play/layout/PlayActionButton;

    if-ne p1, v0, :cond_1

    .line 199
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->handleLoginClick(Z)V

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mLearnMoreButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 201
    const/16 v0, 0x5fb

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->logClickEvent(I)V

    .line 204
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mTosTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    iget-object v1, v1, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    iget-object v1, v1, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    iget-object v1, v1, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtml:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->fromHtml(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mLearnMoreButton:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/google/android/finsky/fragments/LoggingFragment;->onCreate(Landroid/os/Bundle;)V

    .line 94
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "UsernamePasswordFlowFragment.flow_state"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    iput-object v0, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    .line 95
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 100
    const v6, 0x7f040034

    const/4 v7, 0x0

    invoke-virtual {p1, v6, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    iput-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    .line 103
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mFlowState:Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;

    iget-object v0, v6, Lcom/google/android/finsky/protos/CreateInstrument$DeviceCreateInstrumentFlowState;->usernamePasswordForm:Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;

    .line 105
    .local v0, "form":Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    const v7, 0x7f0a00f9

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/image/FifeImageView;

    .line 106
    .local v1, "imageView":Lcom/google/android/play/image/FifeImageView;
    iget-object v6, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v6, :cond_2

    .line 107
    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    .line 113
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    const v7, 0x7f0a00fa

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 114
    .local v4, "titleTextView":Landroid/widget/TextView;
    iget-object v6, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->actionTitleText:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    const v7, 0x7f0a00fb

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mUsernameEditText:Landroid/widget/EditText;

    .line 117
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mUsernameEditText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 119
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    const v7, 0x7f0a00fc

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    iput-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    .line 120
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    iget-object v7, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mEmptyTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 121
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    new-instance v7, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment$2;

    invoke-direct {v7, p0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment$2;-><init>(Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;)V

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 132
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    const v7, 0x7f0a00fd

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mProblemWithLoginTextView:Landroid/widget/TextView;

    .line 133
    iget-object v6, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->helpUrl:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 134
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mProblemWithLoginTextView:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    :goto_1
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    const v7, 0x7f0a00fe

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mTosTextView:Landroid/widget/TextView;

    .line 143
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mTosTextView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 144
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mTosTextView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mTosTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 146
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    const v7, 0x7f0a00ff

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mLearnMoreButton:Landroid/view/View;

    .line 147
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mLearnMoreButton:Landroid/view/View;

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v5, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    .line 150
    .local v5, "tosField":Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;
    iget-object v6, v5, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtml:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 152
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mLearnMoreButton:Landroid/view/View;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 154
    :cond_0
    iget-object v6, v5, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtmlShort:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 155
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mTosTextView:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    :goto_2
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    const v7, 0x7f0a00f8

    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/play/layout/PlayActionButton;

    iput-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mLoginButton:Lcom/google/android/play/layout/PlayActionButton;

    .line 161
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mLoginButton:Lcom/google/android/play/layout/PlayActionButton;

    const/4 v7, 0x3

    const v8, 0x7f0c012c

    invoke-virtual {v6, v7, v8, p0}, Lcom/google/android/play/layout/PlayActionButton;->configure(IILandroid/view/View$OnClickListener;)V

    .line 163
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mUsernameEditText:Landroid/widget/EditText;

    iget-object v7, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->usernameField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-static {v6, v7}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentUtils;->updateEditText(Landroid/widget/EditText;Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;)V

    .line 164
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    iget-object v7, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->passwordField:Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;

    invoke-static {v6, v7}, Lcom/google/android/finsky/billing/genericinstrument/GenericInstrumentUtils;->updateEditText(Landroid/widget/EditText;Lcom/google/android/finsky/protos/CreateInstrument$TextFormField;)V

    .line 167
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getInputType()I

    move-result v2

    .line 168
    .local v2, "inputType":I
    const/16 v6, 0x81

    if-eq v2, v6, :cond_1

    .line 169
    const-string v6, "Password field incorrect input type: %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 170
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    const/16 v7, 0x81

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setInputType(I)V

    .line 174
    :cond_1
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mPasswordEditText:Landroid/widget/EditText;

    const/4 v7, 0x1

    new-array v7, v7, [Landroid/text/InputFilter;

    const/4 v8, 0x0

    new-instance v9, Landroid/text/InputFilter$LengthFilter;

    const/16 v10, 0xff

    invoke-direct {v9, v10}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 179
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mMainView:Landroid/view/ViewGroup;

    return-object v6

    .line 109
    .end local v2    # "inputType":I
    .end local v4    # "titleTextView":Landroid/widget/TextView;
    .end local v5    # "tosField":Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;
    :cond_2
    iget-object v6, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v6, v6, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    iget-object v7, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->logoImage:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v7, v7, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v8

    invoke-virtual {v1, v6, v7, v8}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    goto/16 :goto_0

    .line 136
    .restart local v4    # "titleTextView":Landroid/widget/TextView;
    :cond_3
    const v6, 0x7f0c012a

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v0, Lcom/google/android/finsky/protos/CreateInstrument$UsernamePasswordForm;->helpUrl:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 137
    .local v3, "text":Landroid/text/Spanned;
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mProblemWithLoginTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mProblemWithLoginTextView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 139
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mProblemWithLoginTextView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mProblemWithLoginTextView:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    goto/16 :goto_1

    .line 157
    .end local v3    # "text":Landroid/text/Spanned;
    .restart local v5    # "tosField":Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;
    :cond_4
    iget-object v6, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mTosTextView:Landroid/widget/TextView;

    iget-object v7, v5, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;->tosHtmlShort:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->fromHtml(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 192
    invoke-super {p0}, Lcom/google/android/finsky/fragments/LoggingFragment;->onResume()V

    .line 193
    invoke-direct {p0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->syncLoginButtonState()V

    .line 194
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 184
    invoke-super {p0, p1, p2}, Lcom/google/android/finsky/fragments/LoggingFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 185
    if-nez p2, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/billing/genericinstrument/UsernamePasswordFormFragment;->mUsernameEditText:Landroid/widget/EditText;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/UiUtils;->showKeyboard(Landroid/app/Activity;Landroid/widget/EditText;)V

    .line 188
    :cond_0
    return-void
.end method
