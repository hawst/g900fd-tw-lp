.class public Lcom/google/android/finsky/billing/iab/InAppBillingService;
.super Landroid/app/Service;
.source "InAppBillingService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/billing/iab/InAppBillingService$1;,
        Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;
    }
.end annotation


# instance fields
.field private final mBinder:Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 24
    new-instance v0, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;-><init>(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingService$1;)V

    iput-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mBinder:Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;

    .line 52
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/InAppBillingService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->validatePackageName(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/InAppBillingService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->getAccountName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/iab/InAppBillingService;Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/InAppBillingService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/iab/InAppBillingService;Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/iab/InAppBillingService;
    .param p1, "x1"    # Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private createBundleResponse(Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;)Landroid/os/Bundle;
    .locals 3
    .param p1, "responseCode"    # Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    .prologue
    .line 47
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 48
    .local v0, "response":Landroid/os/Bundle;
    const-string v1, "RESPONSE_CODE"

    invoke-virtual {p1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;->responseCode()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 49
    return-object v0
.end method

.method private getAccountName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-static {p1, p0}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->getPreferredAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    .line 38
    .local v0, "account":Landroid/accounts/Account;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method private getManager(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingManager;
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/finsky/billing/iab/InAppBillingManager;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getLibraries()Lcom/google/android/finsky/library/Libraries;

    move-result-object v1

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getLibraryReplicators()Lcom/google/android/finsky/library/LibraryReplicators;

    move-result-object v2

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/finsky/billing/iab/InAppBillingManager;-><init>(Landroid/content/Context;Lcom/google/android/finsky/library/Libraries;Lcom/google/android/finsky/library/LibraryReplicators;Lcom/google/android/finsky/api/DfeApi;)V

    return-object v0
.end method

.method private validatePackageName(Ljava/lang/String;)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;
    .locals 2
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/iab/InAppBillingService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/google/android/finsky/billing/iab/InAppBillingUtils;->validatePackageName(Ljava/lang/String;Landroid/content/pm/PackageManager;I)Lcom/google/android/finsky/billing/iab/InAppBillingUtils$ResponseCode;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/finsky/billing/iab/InAppBillingService;->mBinder:Lcom/google/android/finsky/billing/iab/InAppBillingService$Stub;

    return-object v0
.end method
