.class public final Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;
.super Lcom/google/protobuf/nano/MessageNano;
.source "PlayStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/analytics/PlayStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PlayStoreDeepLinkEvent"
.end annotation


# instance fields
.field public canResolve:Z

.field public externalReferrer:Ljava/lang/String;

.field public externalUrl:Ljava/lang/String;

.field public hasCanResolve:Z

.field public hasExternalReferrer:Z

.field public hasExternalUrl:Z

.field public hasMinVersion:Z

.field public hasNewEnough:Z

.field public hasPackageName:Z

.field public hasResolvedType:Z

.field public hasServerLogsCookie:Z

.field public minVersion:I

.field public newEnough:Z

.field public packageName:Ljava/lang/String;

.field public resolvedType:I

.field public serverLogsCookie:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2020
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 2021
    invoke-virtual {p0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    .line 2022
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2025
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalUrl:Ljava/lang/String;

    .line 2026
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalUrl:Z

    .line 2027
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->resolvedType:I

    .line 2028
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasResolvedType:Z

    .line 2029
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->packageName:Ljava/lang/String;

    .line 2030
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasPackageName:Z

    .line 2031
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->minVersion:I

    .line 2032
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasMinVersion:Z

    .line 2033
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->newEnough:Z

    .line 2034
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasNewEnough:Z

    .line 2035
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->canResolve:Z

    .line 2036
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasCanResolve:Z

    .line 2037
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->serverLogsCookie:[B

    .line 2038
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasServerLogsCookie:Z

    .line 2039
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalReferrer:Ljava/lang/String;

    .line 2040
    iput-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalReferrer:Z

    .line 2041
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->cachedSize:I

    .line 2042
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 2077
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 2078
    .local v0, "size":I
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalUrl:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2079
    :cond_0
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2082
    :cond_1
    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->resolvedType:I

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasResolvedType:Z

    if-eqz v1, :cond_3

    .line 2083
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->resolvedType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2086
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasPackageName:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->packageName:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2087
    :cond_4
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->packageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2090
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasMinVersion:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->minVersion:I

    if-eqz v1, :cond_7

    .line 2091
    :cond_6
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->minVersion:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2094
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasNewEnough:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->newEnough:Z

    if-eqz v1, :cond_9

    .line 2095
    :cond_8
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->newEnough:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2098
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasCanResolve:Z

    if-nez v1, :cond_a

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->canResolve:Z

    if-eqz v1, :cond_b

    .line 2099
    :cond_a
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->canResolve:Z

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2102
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasServerLogsCookie:Z

    if-nez v1, :cond_c

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->serverLogsCookie:[B

    sget-object v2, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_d

    .line 2103
    :cond_c
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->serverLogsCookie:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2106
    :cond_d
    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalReferrer:Z

    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalReferrer:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 2107
    :cond_e
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalReferrer:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2110
    :cond_f
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;
    .locals 4
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 2118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 2119
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 2123
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2124
    :sswitch_0
    return-object p0

    .line 2129
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalUrl:Ljava/lang/String;

    .line 2130
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalUrl:Z

    goto :goto_0

    .line 2134
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 2135
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2148
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->resolvedType:I

    .line 2149
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasResolvedType:Z

    goto :goto_0

    .line 2155
    .end local v1    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->packageName:Ljava/lang/String;

    .line 2156
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasPackageName:Z

    goto :goto_0

    .line 2160
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->minVersion:I

    .line 2161
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasMinVersion:Z

    goto :goto_0

    .line 2165
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->newEnough:Z

    .line 2166
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasNewEnough:Z

    goto :goto_0

    .line 2170
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->canResolve:Z

    .line 2171
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasCanResolve:Z

    goto :goto_0

    .line 2175
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->serverLogsCookie:[B

    .line 2176
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasServerLogsCookie:Z

    goto :goto_0

    .line 2180
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalReferrer:Ljava/lang/String;

    .line 2181
    iput-boolean v3, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalReferrer:Z

    goto :goto_0

    .line 2119
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    .line 2135
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1957
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2048
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalUrl:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2049
    :cond_0
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2051
    :cond_1
    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->resolvedType:I

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasResolvedType:Z

    if-eqz v0, :cond_3

    .line 2052
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->resolvedType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2054
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasPackageName:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->packageName:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2055
    :cond_4
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2057
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasMinVersion:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->minVersion:I

    if-eqz v0, :cond_7

    .line 2058
    :cond_6
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->minVersion:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 2060
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasNewEnough:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->newEnough:Z

    if-eqz v0, :cond_9

    .line 2061
    :cond_8
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->newEnough:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2063
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasCanResolve:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->canResolve:Z

    if-eqz v0, :cond_b

    .line 2064
    :cond_a
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->canResolve:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 2066
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasServerLogsCookie:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->serverLogsCookie:[B

    sget-object v1, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_d

    .line 2067
    :cond_c
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->serverLogsCookie:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 2069
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->hasExternalReferrer:Z

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalReferrer:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 2070
    :cond_e
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreDeepLinkEvent;->externalReferrer:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 2072
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 2073
    return-void
.end method
