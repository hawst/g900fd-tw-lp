.class final Lcom/google/android/finsky/utils/RateReviewHelper$5;
.super Ljava/lang/Object;
.source "RateReviewHelper.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/RateReviewHelper;->deleteReview(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lcom/google/android/finsky/protos/Rev$ReviewResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

.field final synthetic val$docDetailsUrl:Ljava/lang/String;

.field final synthetic val$listener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/api/DfeApi;Ljava/lang/String;Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/finsky/utils/RateReviewHelper$5;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iput-object p2, p0, Lcom/google/android/finsky/utils/RateReviewHelper$5;->val$docDetailsUrl:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/finsky/utils/RateReviewHelper$5;->val$listener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/finsky/protos/Rev$ReviewResponse;)V
    .locals 4
    .param p1, "response"    # Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$5;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    iget-object v1, p0, Lcom/google/android/finsky/utils/RateReviewHelper$5;->val$docDetailsUrl:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->invalidateDetailsCache(Ljava/lang/String;Z)V

    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$5;->val$dfeApi:Lcom/google/android/finsky/api/DfeApi;

    invoke-static {v0}, Lcom/google/android/finsky/utils/MyPeoplePageHelper;->onMutationOccurred(Lcom/google/android/finsky/api/DfeApi;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$5;->val$listener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/utils/RateReviewHelper$5;->val$listener:Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;

    const/4 v1, -0x1

    const-string v2, ""

    const-string v3, ""

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/finsky/utils/RateReviewHelper$RateReviewListener;->onRateReviewCommitted(ILjava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 197
    check-cast p1, Lcom/google/android/finsky/protos/Rev$ReviewResponse;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/utils/RateReviewHelper$5;->onResponse(Lcom/google/android/finsky/protos/Rev$ReviewResponse;)V

    return-void
.end method
