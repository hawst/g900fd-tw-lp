.class public final Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
.super Lcom/google/protobuf/nano/MessageNano;
.source "ChallengeProto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/ChallengeProto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Challenge"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;


# instance fields
.field public addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

.field public ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

.field public authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

.field public cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

.field public error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

.field public paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

.field public smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

.field public webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->clear()Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 51
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    sput-object v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->_emptyArray:[Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 55
    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    .line 56
    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    .line 57
    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    .line 58
    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    .line 59
    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    .line 60
    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    .line 61
    iput-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cachedSize:I

    .line 63
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 98
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 99
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-eqz v1, :cond_0

    .line 100
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    if-eqz v1, :cond_1

    .line 104
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    if-eqz v1, :cond_2

    .line 108
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    if-eqz v1, :cond_3

    .line 112
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    if-eqz v1, :cond_4

    .line 116
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    if-eqz v1, :cond_5

    .line 120
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    if-eqz v1, :cond_6

    .line 124
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    if-eqz v1, :cond_7

    .line 128
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$Challenge;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 139
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 140
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 144
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 145
    :sswitch_0
    return-object p0

    .line 150
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-nez v1, :cond_1

    .line 151
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    .line 153
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 157
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    if-nez v1, :cond_2

    .line 158
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    .line 160
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 164
    :sswitch_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    if-nez v1, :cond_3

    .line 165
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    .line 167
    :cond_3
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 171
    :sswitch_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    if-nez v1, :cond_4

    .line 172
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    .line 174
    :cond_4
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 178
    :sswitch_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    if-nez v1, :cond_5

    .line 179
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    .line 181
    :cond_5
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 185
    :sswitch_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    if-nez v1, :cond_6

    .line 186
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    .line 188
    :cond_6
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 192
    :sswitch_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    if-nez v1, :cond_7

    .line 193
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    .line 195
    :cond_7
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 199
    :sswitch_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    if-nez v1, :cond_8

    .line 200
    new-instance v1, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    .line 202
    :cond_8
    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto/16 :goto_0

    .line 140
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/ChallengeProto$Challenge;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    if-eqz v0, :cond_0

    .line 70
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->addressChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AddressChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    if-eqz v0, :cond_1

    .line 73
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->authenticationChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AuthenticationChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    if-eqz v0, :cond_2

    .line 76
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->webViewChallenge:Lcom/google/android/finsky/protos/ChallengeProto$WebViewChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 78
    :cond_2
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    if-eqz v0, :cond_3

    .line 79
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->ageChallenge:Lcom/google/android/finsky/protos/ChallengeProto$AgeChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    if-eqz v0, :cond_4

    .line 82
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->smsCodeChallenge:Lcom/google/android/finsky/protos/ChallengeProto$SmsCodeChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 84
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    if-eqz v0, :cond_5

    .line 85
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->error:Lcom/google/android/finsky/protos/ChallengeProto$ChallengeError;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 87
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    if-eqz v0, :cond_6

    .line 88
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->cvnChallenge:Lcom/google/android/finsky/protos/ChallengeProto$CvnChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 90
    :cond_6
    iget-object v0, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    if-eqz v0, :cond_7

    .line 91
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/finsky/protos/ChallengeProto$Challenge;->paymentsUpdateChallenge:Lcom/google/android/finsky/protos/ChallengeProto$PaymentsUpdateChallenge;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 93
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 94
    return-void
.end method
