.class public final Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FileMetadataProto"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;


# instance fields
.field public downloadUrl:Ljava/lang/String;

.field public fileType:I

.field public hasDownloadUrl:Z

.field public hasFileType:Z

.field public hasSize:Z

.field public hasVersionCode:Z

.field public size:J

.field public versionCode:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1555
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1556
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    .line 1557
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    .locals 2

    .prologue
    .line 1528
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    if-nez v0, :cond_1

    .line 1529
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1531
    :try_start_0
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    if-nez v0, :cond_0

    .line 1532
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    sput-object v0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    .line 1534
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1536
    :cond_1
    sget-object v0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->_emptyArray:[Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    return-object v0

    .line 1534
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1560
    iput v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->fileType:I

    .line 1561
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasFileType:Z

    .line 1562
    iput v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->versionCode:I

    .line 1563
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasVersionCode:Z

    .line 1564
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->size:J

    .line 1565
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasSize:Z

    .line 1566
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->downloadUrl:Ljava/lang/String;

    .line 1567
    iput-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasDownloadUrl:Z

    .line 1568
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->cachedSize:I

    .line 1569
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 1592
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1593
    .local v0, "size":I
    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->fileType:I

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasFileType:Z

    if-eqz v1, :cond_1

    .line 1594
    :cond_0
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->fileType:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1597
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasVersionCode:Z

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->versionCode:I

    if-eqz v1, :cond_3

    .line 1598
    :cond_2
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->versionCode:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1601
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasSize:Z

    if-nez v1, :cond_4

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->size:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 1602
    :cond_4
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->size:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1605
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasDownloadUrl:Z

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->downloadUrl:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1606
    :cond_6
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->downloadUrl:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1609
    :cond_7
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;
    .locals 5
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1617
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1618
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1622
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1623
    :sswitch_0
    return-object p0

    .line 1628
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v1

    .line 1629
    .local v1, "value":I
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1632
    :pswitch_0
    iput v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->fileType:I

    .line 1633
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasFileType:Z

    goto :goto_0

    .line 1639
    .end local v1    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v2

    iput v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->versionCode:I

    .line 1640
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasVersionCode:Z

    goto :goto_0

    .line 1644
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->size:J

    .line 1645
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasSize:Z

    goto :goto_0

    .line 1649
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->downloadUrl:Ljava/lang/String;

    .line 1650
    iput-boolean v4, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasDownloadUrl:Z

    goto :goto_0

    .line 1618
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    .line 1629
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1522
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1575
    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->fileType:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasFileType:Z

    if-eqz v0, :cond_1

    .line 1576
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->fileType:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1578
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasVersionCode:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->versionCode:I

    if-eqz v0, :cond_3

    .line 1579
    :cond_2
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->versionCode:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1581
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasSize:Z

    if-nez v0, :cond_4

    iget-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->size:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 1582
    :cond_4
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->size:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 1584
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->hasDownloadUrl:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->downloadUrl:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1585
    :cond_6
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/finsky/protos/VendingProtos$FileMetadataProto;->downloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1587
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1588
    return-void
.end method
