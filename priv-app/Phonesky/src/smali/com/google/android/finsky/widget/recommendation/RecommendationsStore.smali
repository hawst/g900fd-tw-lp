.class public Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;
.super Ljava/lang/Object;
.source "RecommendationsStore.java"


# static fields
.field private static final CACHE_FILE_PREFIX:Ljava/lang/String;

.field private static final sWriteThread:Ljava/util/concurrent/ExecutorService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/finsky/utils/BackgroundThreadFactory;

    invoke-direct {v0}, Lcom/google/android/finsky/utils/BackgroundThreadFactory;-><init>()V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->sWriteThread:Ljava/util/concurrent/ExecutorService;

    .line 50
    const-class v0, Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->CACHE_FILE_PREFIX:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/protos/DocList$ListResponse;ILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/protos/DocList$ListResponse;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/finsky/library/Library;

    .prologue
    .line 44
    invoke-static {p0, p1, p2}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->parseNetworkResponse(Lcom/google/android/finsky/protos/DocList$ListResponse;ILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->sWriteThread:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method public static deleteCachedRecommendations(Landroid/content/Context;I)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backendId"    # I

    .prologue
    .line 89
    invoke-static {p0, p1}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getCacheFile(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v0

    .line 90
    .local v0, "cacheFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 91
    return-void
.end method

.method public static getBitmap(Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/widget/recommendation/Recommendation;I)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "loader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p1, "item"    # Lcom/google/android/finsky/widget/recommendation/Recommendation;
    .param p2, "maxHeight"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v7, 0x0

    .line 223
    invoke-virtual {p1}, Lcom/google/android/finsky/widget/recommendation/Recommendation;->getImage()Lcom/google/android/finsky/protos/Common$Image;

    move-result-object v3

    .line 225
    .local v3, "image":Lcom/google/android/finsky/protos/Common$Image;
    if-nez v3, :cond_0

    .line 226
    const/4 v6, 0x0

    .line 253
    :goto_0
    return-object v6

    .line 228
    :cond_0
    iget-boolean v6, v3, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v6, :cond_2

    move v5, p2

    .line 230
    .local v5, "requestHeight":I
    :goto_1
    new-instance v4, Ljava/util/concurrent/Semaphore;

    invoke-direct {v4, v7}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    .line 231
    .local v4, "lock":Ljava/util/concurrent/Semaphore;
    new-array v0, v11, [Landroid/graphics/Bitmap;

    .line 232
    .local v0, "bitmap":[Landroid/graphics/Bitmap;
    iget-object v6, v3, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    new-instance v8, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore$3;

    invoke-direct {v8, v0, v4}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore$3;-><init>([Landroid/graphics/Bitmap;Ljava/util/concurrent/Semaphore;)V

    invoke-virtual {p0, v6, v7, v5, v8}, Lcom/google/android/play/image/BitmapLoader;->get(Ljava/lang/String;IILcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;)Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    move-result-object v1

    .line 240
    .local v1, "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    invoke-virtual {v1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_3

    .line 241
    invoke-virtual {v1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    aput-object v6, v0, v7

    .line 253
    :cond_1
    :goto_2
    aget-object v6, v0, v7

    goto :goto_0

    .end local v0    # "bitmap":[Landroid/graphics/Bitmap;
    .end local v1    # "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .end local v4    # "lock":Ljava/util/concurrent/Semaphore;
    .end local v5    # "requestHeight":I
    :cond_2
    move v5, v7

    .line 228
    goto :goto_1

    .line 244
    .restart local v0    # "bitmap":[Landroid/graphics/Bitmap;
    .restart local v1    # "container":Lcom/google/android/play/image/BitmapLoader$BitmapContainer;
    .restart local v4    # "lock":Ljava/util/concurrent/Semaphore;
    .restart local v5    # "requestHeight":I
    :cond_3
    :try_start_0
    sget-object v6, Lcom/google/android/finsky/config/G;->recommendationsFetchTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v6}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v8, v9, v6}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 246
    const-string v6, "Timed out while fetching %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, v3, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    aput-object v10, v8, v9

    invoke-static {v6, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 248
    :catch_0
    move-exception v2

    .line 249
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v6, "Interrupted while fetching %s"

    new-array v8, v11, [Ljava/lang/Object;

    iget-object v9, v3, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    aput-object v9, v8, v7

    invoke-static {v6, v8}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public static getCacheFile(Landroid/content/Context;I)Ljava/io/File;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "backend"    # I

    .prologue
    .line 257
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "recs"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 258
    .local v0, "cacheDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 259
    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->CACHE_FILE_PREFIX:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".cache"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method

.method public static getRecommendations(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;ILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "backendId"    # I
    .param p3, "library"    # Lcom/google/android/finsky/library/Library;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 66
    const/4 v2, 0x0

    .line 68
    .local v2, "recList":Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    invoke-static {p0, p2}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getCacheFile(Landroid/content/Context;I)Ljava/io/File;

    move-result-object v0

    .line 69
    .local v0, "cacheFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 70
    invoke-static {v0}, Lcom/google/android/finsky/utils/ParcelUtils;->readFromDisk(Ljava/io/File;)Landroid/os/Parcelable;

    move-result-object v2

    .end local v2    # "recList":Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    check-cast v2, Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    .line 71
    .restart local v2    # "recList":Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    if-eqz v2, :cond_0

    .line 72
    invoke-virtual {v2}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->removeExpiredRecommendations()I

    .line 76
    :cond_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 77
    :cond_1
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->loadDocumentsFromNetwork(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;ILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    move-result-object v2

    .line 79
    :try_start_0
    invoke-static {v0, v2}, Lcom/google/android/finsky/utils/ParcelUtils;->writeToDisk(Ljava/io/File;Landroid/os/Parcelable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :cond_2
    :goto_0
    return-object v2

    .line 80
    :catch_0
    move-exception v1

    .line 81
    .local v1, "e":Ljava/io/IOException;
    const-string v3, "Unable to cache recs for %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v1, v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static getRecommendationsOrShowError(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;IILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "backendId"    # I
    .param p3, "appWidgetId"    # I
    .param p4, "library"    # Lcom/google/android/finsky/library/Library;

    .prologue
    .line 101
    invoke-static {}, Lcom/google/android/finsky/utils/Utils;->ensureNotOnMainThread()V

    .line 103
    const/4 v2, 0x0

    .line 104
    .local v2, "recList":Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    const/4 v1, 0x0

    .line 106
    .local v1, "errorString":Ljava/lang/String;
    :try_start_0
    invoke-static {p0, p1, p2, p4}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecommendations(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;ILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 120
    if-eqz v1, :cond_0

    .line 121
    invoke-static {p0, p3, v1}, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;->showError(Landroid/content/Context;ILjava/lang/String;)V

    .line 125
    :cond_0
    :goto_0
    return-object v2

    .line 108
    :catch_0
    move-exception v0

    .line 109
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    const-string v3, "Error loading recs widget: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    const v3, 0x7f0c01d9

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 120
    if-eqz v1, :cond_0

    .line 121
    invoke-static {p0, p3, v1}, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;->showError(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 111
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 112
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    :try_start_2
    const-string v3, "Error loading recs widget: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    const v3, 0x7f0c01d9

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v1

    .line 120
    if-eqz v1, :cond_0

    .line 121
    invoke-static {p0, p3, v1}, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;->showError(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 114
    .end local v0    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v0

    .line 115
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_3
    const-string v3, "Error loading recs widget: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 116
    const v3, 0x7f0c01da

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 120
    if-eqz v1, :cond_0

    .line 121
    invoke-static {p0, p3, v1}, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;->showError(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 117
    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_3
    move-exception v0

    .line 118
    .local v0, "e":Ljava/lang/IllegalStateException;
    const v3, 0x7f0c01db

    :try_start_4
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    .line 120
    if-eqz v1, :cond_0

    .line 121
    invoke-static {p0, p3, v1}, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;->showError(Landroid/content/Context;ILjava/lang/String;)V

    goto :goto_0

    .line 120
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_1

    .line 121
    invoke-static {p0, p3, v1}, Lcom/google/android/finsky/widget/recommendation/RecommendedWidgetProvider;->showError(Landroid/content/Context;ILjava/lang/String;)V

    :cond_1
    throw v3
.end method

.method public static getRecsWidgetUrl(I)Ljava/lang/String;
    .locals 1
    .param p0, "backendId"    # I

    .prologue
    .line 175
    sget-object v0, Lcom/google/android/finsky/utils/FinskyPreferences;->widgetUrlsByBackend:Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(I)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private static loadDocumentsFromNetwork(Landroid/content/Context;Lcom/google/android/finsky/api/DfeApi;ILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "backendId"    # I
    .param p3, "library"    # Lcom/google/android/finsky/library/Library;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/util/concurrent/ExecutionException;,
            Ljava/util/concurrent/TimeoutException;
        }
    .end annotation

    .prologue
    .line 131
    invoke-static {}, Lcom/android/volley/toolbox/RequestFuture;->newFuture()Lcom/android/volley/toolbox/RequestFuture;

    move-result-object v0

    .line 132
    .local v0, "future":Lcom/android/volley/toolbox/RequestFuture;, "Lcom/android/volley/toolbox/RequestFuture<Lcom/google/android/finsky/protos/DocList$ListResponse;>;"
    invoke-static {p2}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecsWidgetUrl(I)Ljava/lang/String;

    move-result-object v1

    .line 133
    .local v1, "recsUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134
    const-string v3, "No recs widget url provided in loadDocsFromNetwork()."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 135
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "No recs url provided"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 137
    :cond_0
    invoke-interface {p1, v1, v0, v0}, Lcom/google/android/finsky/api/DfeApi;->getList(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    .line 139
    sget-object v3, Lcom/google/android/finsky/config/G;->recommendationsFetchTimeoutMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v3}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5, v3}, Lcom/android/volley/toolbox/RequestFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/protos/DocList$ListResponse;

    .line 141
    .local v2, "response":Lcom/google/android/finsky/protos/DocList$ListResponse;
    invoke-static {v2, p2, p3}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->parseNetworkResponse(Lcom/google/android/finsky/protos/DocList$ListResponse;ILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    move-result-object v3

    return-object v3
.end method

.method private static parseNetworkResponse(Lcom/google/android/finsky/protos/DocList$ListResponse;ILcom/google/android/finsky/library/Library;)Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    .locals 13
    .param p0, "response"    # Lcom/google/android/finsky/protos/DocList$ListResponse;
    .param p1, "backendId"    # I
    .param p2, "library"    # Lcom/google/android/finsky/library/Library;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 180
    iget-object v10, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    array-length v10, v10

    if-nez v10, :cond_1

    .line 181
    const/4 v6, 0x0

    .line 211
    :cond_0
    return-object v6

    .line 184
    :cond_1
    new-instance v2, Lcom/google/android/finsky/api/model/Document;

    iget-object v10, p0, Lcom/google/android/finsky/protos/DocList$ListResponse;->doc:[Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    aget-object v10, v10, v9

    invoke-direct {v2, v10}, Lcom/google/android/finsky/api/model/Document;-><init>(Lcom/google/android/finsky/protos/DocumentV2$DocV2;)V

    .line 185
    .local v2, "container":Lcom/google/android/finsky/api/model/Document;
    new-instance v6, Lcom/google/android/finsky/widget/recommendation/RecommendationList;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v6, v10, p1}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;-><init>(Ljava/lang/String;I)V

    .line 187
    .local v6, "recList":Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getChildCount()I

    move-result v0

    .line 190
    .local v0, "N":I
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/finsky/FinskyApp;->getPackageInfoRepository()Lcom/google/android/finsky/appstate/PackageStateRepository;

    move-result-object v7

    .line 192
    .local v7, "repo":Lcom/google/android/finsky/appstate/PackageStateRepository;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v0, :cond_0

    .line 193
    invoke-virtual {v2, v4}, Lcom/google/android/finsky/api/model/Document;->getChildAt(I)Lcom/google/android/finsky/api/model/Document;

    move-result-object v3

    .line 195
    .local v3, "document":Lcom/google/android/finsky/api/model/Document;
    const/4 v5, 0x0

    .line 196
    .local v5, "isInstalledApp":Z
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v10

    if-ne v10, v8, :cond_2

    .line 197
    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getAppDetails()Lcom/google/android/finsky/protos/DocDetails$AppDetails;

    move-result-object v1

    .line 198
    .local v1, "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    iget-object v10, v1, Lcom/google/android/finsky/protos/DocDetails$AppDetails;->packageName:Ljava/lang/String;

    invoke-interface {v7, v10}, Lcom/google/android/finsky/appstate/PackageStateRepository;->get(Ljava/lang/String;)Lcom/google/android/finsky/appstate/PackageStateRepository$PackageState;

    move-result-object v10

    if-eqz v10, :cond_5

    move v5, v8

    .line 201
    .end local v1    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    :cond_2
    :goto_1
    invoke-static {v3, p2}, Lcom/google/android/finsky/utils/LibraryUtils;->isOwned(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/finsky/library/Library;)Z

    move-result v10

    if-nez v10, :cond_3

    if-eqz v5, :cond_6

    .line 202
    :cond_3
    sget-boolean v10, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v10, :cond_4

    .line 203
    const-string v10, "Already own %s, skipping"

    new-array v11, v8, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v11, v9

    invoke-static {v10, v11}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 192
    :cond_4
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .restart local v1    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    :cond_5
    move v5, v9

    .line 198
    goto :goto_1

    .line 208
    .end local v1    # "appDetails":Lcom/google/android/finsky/protos/DocDetails$AppDetails;
    :cond_6
    new-instance v10, Lcom/google/android/finsky/widget/recommendation/Recommendation;

    invoke-direct {v10, v3}, Lcom/google/android/finsky/widget/recommendation/Recommendation;-><init>(Lcom/google/android/finsky/api/model/Document;)V

    invoke-virtual {v6, v10}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->add(Lcom/google/android/finsky/widget/recommendation/Recommendation;)Z

    goto :goto_2
.end method

.method public static performBackFill(Lcom/google/android/finsky/api/DfeApi;Landroid/content/Context;Lcom/google/android/finsky/widget/recommendation/RecommendationList;Lcom/google/android/finsky/library/Library;I)V
    .locals 3
    .param p0, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "oldRecs"    # Lcom/google/android/finsky/widget/recommendation/RecommendationList;
    .param p3, "library"    # Lcom/google/android/finsky/library/Library;
    .param p4, "appWidgetId"    # I

    .prologue
    .line 146
    invoke-virtual {p2}, Lcom/google/android/finsky/widget/recommendation/RecommendationList;->getBackendId()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore;->getRecsWidgetUrl(I)Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "recsUrl":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    const-string v1, "No recs widget url provided in performBackFill()"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 172
    :goto_0
    return-void

    .line 151
    :cond_0
    new-instance v1, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore$1;

    invoke-direct {v1, p2, p3, p1, p4}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore$1;-><init>(Lcom/google/android/finsky/widget/recommendation/RecommendationList;Lcom/google/android/finsky/library/Library;Landroid/content/Context;I)V

    new-instance v2, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore$2;

    invoke-direct {v2}, Lcom/google/android/finsky/widget/recommendation/RecommendationsStore$2;-><init>()V

    invoke-interface {p0, v0, v1, v2}, Lcom/google/android/finsky/api/DfeApi;->getList(Ljava/lang/String;Lcom/android/volley/Response$Listener;Lcom/android/volley/Response$ErrorListener;)Lcom/android/volley/Request;

    goto :goto_0
.end method
