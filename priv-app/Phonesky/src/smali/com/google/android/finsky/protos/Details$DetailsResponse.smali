.class public final Lcom/google/android/finsky/protos/Details$DetailsResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Details.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Details;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DetailsResponse"
.end annotation


# instance fields
.field public analyticsCookie:Ljava/lang/String;

.field public discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

.field public docV1:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

.field public docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

.field public footerHtml:Ljava/lang/String;

.field public hasAnalyticsCookie:Z

.field public hasFooterHtml:Z

.field public hasServerLogsCookie:Z

.field public serverLogsCookie:[B

.field public userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->clear()Lcom/google/android/finsky/protos/Details$DetailsResponse;

    .line 51
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 54
    iput-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    .line 55
    iput-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->analyticsCookie:Ljava/lang/String;

    .line 57
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasAnalyticsCookie:Z

    .line 58
    iput-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 59
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml:Ljava/lang/String;

    .line 60
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasFooterHtml:Z

    .line 61
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie:[B

    .line 62
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasServerLogsCookie:Z

    .line 63
    invoke-static {}, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;->emptyArray()[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->cachedSize:I

    .line 65
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 102
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 103
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    if-eqz v3, :cond_0

    .line 104
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 107
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasAnalyticsCookie:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->analyticsCookie:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 108
    :cond_1
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->analyticsCookie:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 111
    :cond_2
    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-eqz v3, :cond_3

    .line 112
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 115
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v3, :cond_4

    .line 116
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 119
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasFooterHtml:Z

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 120
    :cond_5
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 123
    :cond_6
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasServerLogsCookie:Z

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_8

    .line 124
    :cond_7
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 127
    :cond_8
    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    array-length v3, v3

    if-lez v3, :cond_a

    .line 128
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    array-length v3, v3

    if-ge v1, v3, :cond_a

    .line 129
    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    aget-object v0, v3, v1

    .line 130
    .local v0, "element":Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    if-eqz v0, :cond_9

    .line 131
    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 128
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 136
    .end local v0    # "element":Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .end local v1    # "i":I
    :cond_a
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$DetailsResponse;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 144
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 145
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 149
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 150
    :sswitch_0
    return-object p0

    .line 155
    :sswitch_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    if-nez v5, :cond_1

    .line 156
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV1$DocV1;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    .line 158
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 162
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->analyticsCookie:Ljava/lang/String;

    .line 163
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasAnalyticsCookie:Z

    goto :goto_0

    .line 167
    :sswitch_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-nez v5, :cond_2

    .line 168
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Review;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 170
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 174
    :sswitch_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-nez v5, :cond_3

    .line 175
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$DocV2;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    .line 177
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 181
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml:Ljava/lang/String;

    .line 182
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasFooterHtml:Z

    goto :goto_0

    .line 186
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie:[B

    .line 187
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasServerLogsCookie:Z

    goto :goto_0

    .line 191
    :sswitch_7
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 193
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    if-nez v5, :cond_5

    move v1, v4

    .line 194
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    .line 196
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    if-eqz v1, :cond_4

    .line 197
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 199
    :cond_4
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 200
    new-instance v5, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;-><init>()V

    aput-object v5, v2, v1

    .line 201
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 202
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 199
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 193
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    array-length v1, v5

    goto :goto_1

    .line 205
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Details$DiscoveryBadge;-><init>()V

    aput-object v5, v2, v1

    .line 206
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 207
    iput-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    goto/16 :goto_0

    .line 145
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Details$DetailsResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Details$DetailsResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 71
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    if-eqz v2, :cond_0

    .line 72
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV1:Lcom/google/android/finsky/protos/DocumentV1$DocV1;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 74
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasAnalyticsCookie:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->analyticsCookie:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 75
    :cond_1
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->analyticsCookie:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 77
    :cond_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-eqz v2, :cond_3

    .line 78
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->userReview:Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 80
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    if-eqz v2, :cond_4

    .line 81
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->docV2:Lcom/google/android/finsky/protos/DocumentV2$DocV2;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 83
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasFooterHtml:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 84
    :cond_5
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->footerHtml:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 86
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->hasServerLogsCookie:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_8

    .line 87
    :cond_7
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->serverLogsCookie:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 89
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    array-length v2, v2

    if-lez v2, :cond_a

    .line 90
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    array-length v2, v2

    if-ge v1, v2, :cond_a

    .line 91
    iget-object v2, p0, Lcom/google/android/finsky/protos/Details$DetailsResponse;->discoveryBadge:[Lcom/google/android/finsky/protos/Details$DiscoveryBadge;

    aget-object v0, v2, v1

    .line 92
    .local v0, "element":Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    if-eqz v0, :cond_9

    .line 93
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 90
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 97
    .end local v0    # "element":Lcom/google/android/finsky/protos/Details$DiscoveryBadge;
    .end local v1    # "i":I
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 98
    return-void
.end method
