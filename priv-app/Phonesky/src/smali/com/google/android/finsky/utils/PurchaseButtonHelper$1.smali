.class final Lcom/google/android/finsky/utils/PurchaseButtonHelper$1;
.super Ljava/lang/Object;
.source "PurchaseButtonHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/utils/PurchaseButtonHelper;->getActionClickListener(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;ILcom/google/android/finsky/navigationmanager/NavigationManager;Ljava/lang/String;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;Landroid/content/Context;)Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$action:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 893
    iput-object p1, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$1;->val$action:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iput-object p2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 896
    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$1;->val$action:Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;

    iget-object v2, v2, Lcom/google/android/finsky/utils/PurchaseButtonHelper$DocumentAction;->document:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v2}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    .line 897
    .local v0, "packageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/finsky/utils/PurchaseButtonHelper$1;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 898
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 900
    return-void
.end method
