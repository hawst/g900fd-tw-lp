.class Lcom/google/android/finsky/setup/PlaySetupService$1;
.super Lcom/android/vending/setup/IPlaySetupService$Stub;
.source "PlaySetupService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/setup/PlaySetupService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/setup/PlaySetupService;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/setup/PlaySetupService;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/finsky/setup/PlaySetupService$1;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    invoke-direct {p0}, Lcom/android/vending/setup/IPlaySetupService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public cancelEarlyUpdate()Z
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService$1;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # invokes: Lcom/google/android/finsky/setup/PlaySetupService;->doCancelEarlyUpdate()Z
    invoke-static {v0}, Lcom/google/android/finsky/setup/PlaySetupService;->access$200(Lcom/google/android/finsky/setup/PlaySetupService;)Z

    move-result v0

    return v0
.end method

.method public getEarlyUpdate()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService$1;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # invokes: Lcom/google/android/finsky/setup/PlaySetupService;->doGetEarlyUpdate()Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/finsky/setup/PlaySetupService;->access$000(Lcom/google/android/finsky/setup/PlaySetupService;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getFinalHoldFlow()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService$1;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # invokes: Lcom/google/android/finsky/setup/PlaySetupService;->doGetFinalHoldFlow()Landroid/os/Bundle;
    invoke-static {v0}, Lcom/google/android/finsky/setup/PlaySetupService;->access$500(Lcom/google/android/finsky/setup/PlaySetupService;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getRestoreFlow(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService$1;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # invokes: Lcom/google/android/finsky/setup/PlaySetupService;->doGetRestoreFlow(Ljava/lang/String;)Landroid/os/Bundle;
    invoke-static {v0, p1}, Lcom/google/android/finsky/setup/PlaySetupService;->access$300(Lcom/google/android/finsky/setup/PlaySetupService;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public startDownloads()V
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService$1;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # invokes: Lcom/google/android/finsky/setup/PlaySetupService;->doStartDownloads()V
    invoke-static {v0}, Lcom/google/android/finsky/setup/PlaySetupService;->access$600(Lcom/google/android/finsky/setup/PlaySetupService;)V

    .line 141
    return-void
.end method

.method public startEarlyUpdate()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService$1;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # invokes: Lcom/google/android/finsky/setup/PlaySetupService;->doStartEarlyUpdate()V
    invoke-static {v0}, Lcom/google/android/finsky/setup/PlaySetupService;->access$100(Lcom/google/android/finsky/setup/PlaySetupService;)V

    .line 116
    return-void
.end method

.method public startVpa()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/finsky/setup/PlaySetupService$1;->this$0:Lcom/google/android/finsky/setup/PlaySetupService;

    # invokes: Lcom/google/android/finsky/setup/PlaySetupService;->doStartVpa()V
    invoke-static {v0}, Lcom/google/android/finsky/setup/PlaySetupService;->access$400(Lcom/google/android/finsky/setup/PlaySetupService;)V

    .line 131
    return-void
.end method
