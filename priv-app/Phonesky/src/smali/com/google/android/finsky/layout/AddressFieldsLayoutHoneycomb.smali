.class public Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;
.super Lcom/google/android/finsky/layout/AddressFieldsLayout;
.source "AddressFieldsLayoutHoneycomb.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$2;,
        Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;
    }
.end annotation


# static fields
.field private static KEY_PARENT_STATE:Ljava/lang/String;

.field private static KEY_SUPPORT_SHOWING_ONE_FIELD:Ljava/lang/String;


# instance fields
.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mAnimator1:Landroid/animation/ValueAnimator;

.field private mAnimator2:Landroid/animation/ValueAnimator;

.field private mExpandCalledWhileContracting:Z

.field private mFirstFieldHint:Ljava/lang/CharSequence;

.field private mNewFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mOnHeightChangedListener:Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

.field private mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

.field private mSupportShowingOneField:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "support_showing_one_field"

    sput-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->KEY_SUPPORT_SHOWING_ONE_FIELD:Ljava/lang/String;

    .line 42
    const-string v0, "parent_instance_state"

    sput-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->KEY_PARENT_STATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/finsky/layout/AddressFieldsLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->SHOWING_PROGRESS_BAR:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mSupportShowingOneField:Z

    .line 72
    return-void
.end method

.method private addViews(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 279
    .local p1, "fields":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 280
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 283
    .local v0, "field":Landroid/view/View;
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 284
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 286
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v3, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 288
    .end local v0    # "field":Landroid/view/View;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_0
    return-void
.end method

.method private getViewHeightAtTransitionStart()I
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_ONE_FIELD_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 346
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method private invokeOnHeightChanged(F)V
    .locals 1
    .param p1, "heightOffset"    # F

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mOnHeightChangedListener:Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mOnHeightChangedListener:Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

    invoke-interface {v0, p1}, Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;->onHeightOffsetChanged(F)V

    .line 259
    :cond_0
    return-void
.end method

.method private makeOnlyFirstFieldVisible()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 265
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->setChildrenViewVisibility(I)V

    .line 266
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 269
    :cond_0
    return-void
.end method

.method private setChildrenViewVisibility(I)V
    .locals 3
    .param p1, "visibility"    # I

    .prologue
    .line 272
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    .line 273
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 274
    iget-object v2, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setVisibility(I)V

    .line 273
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 276
    :cond_0
    return-void
.end method

.method private switchAnimator()V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator1:Landroid/animation/ValueAnimator;

    if-ne v0, v1, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator2:Landroid/animation/ValueAnimator;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    .line 300
    :goto_0
    return-void

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator1:Landroid/animation/ValueAnimator;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    goto :goto_0
.end method


# virtual methods
.method public disableOneFieldMode()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    iput-boolean v2, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mSupportShowingOneField:Z

    .line 232
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$2;->$SwitchMap$com$google$android$finsky$layout$AddressFieldsLayoutHoneycomb$State:[I

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 247
    :goto_0
    :pswitch_0
    return-void

    .line 234
    :pswitch_1
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->setChildrenViewVisibility(I)V

    goto :goto_0

    .line 240
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 241
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->SHOWING_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    .line 242
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->setChildrenViewVisibility(I)V

    goto :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public hideUpperRightProgressBar()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mUpperRightProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$1;

    invoke-direct {v1, p0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$1;-><init>(Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 125
    return-void
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 413
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    const/4 v4, 0x4

    .line 379
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 381
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_TO_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-ne v0, v1, :cond_3

    .line 382
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->SHOWING_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    .line 383
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 395
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mNewFields:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 396
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mNewFields:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->setFields(Ljava/util/List;)V

    .line 397
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mNewFields:Ljava/util/List;

    .line 402
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mExpandCalledWhileContracting:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->SHOWING_PROGRESS_BAR:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-ne v0, v1, :cond_2

    .line 403
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mExpandCalledWhileContracting:Z

    if-eqz v0, :cond_2

    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mExpandCalledWhileContracting:Z

    .line 405
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->switchAnimator()V

    .line 406
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->showFields()V

    .line 409
    :cond_2
    return-void

    .line 384
    :cond_3
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_TO_ONE_FIELD:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-ne v0, v1, :cond_4

    .line 385
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->SHOWING_ONE_FIELD:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    .line 386
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 387
    :cond_4
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_TO_PROGRESS_BAR:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-ne v0, v1, :cond_5

    .line 388
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->SHOWING_PROGRESS_BAR:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    .line 389
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->setChildrenViewVisibility(I)V

    goto :goto_0

    .line 390
    :cond_5
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_ONE_FIELD_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-ne v0, v1, :cond_0

    .line 391
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->SHOWING_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 417
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 375
    return-void
.end method

.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 352
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 353
    .local v4, "t":F
    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    .line 354
    .local v1, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_2

    .line 355
    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 358
    .local v0, "child":Landroid/view/View;
    if-nez v3, :cond_0

    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v6, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_ONE_FIELD_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-eq v5, v6, :cond_1

    .line 359
    :cond_0
    invoke-virtual {v0, v4}, Landroid/view/View;->setAlpha(F)V

    .line 361
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v4

    invoke-virtual {v0, v5}, Landroid/view/View;->setY(F)V

    .line 354
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 363
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v6, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_ONE_FIELD_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-ne v5, v6, :cond_3

    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFieldContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-lez v5, :cond_3

    .line 365
    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setAlpha(F)V

    .line 369
    :goto_1
    sub-float v5, v7, v4

    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->getViewHeightAtTransitionStart()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v6, v6

    mul-float v2, v5, v6

    .line 370
    .local v2, "heightOffset":F
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->invokeOnHeightChanged(F)V

    .line 371
    return-void

    .line 367
    .end local v2    # "heightOffset":F
    :cond_3
    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mProgressBar:Landroid/widget/ProgressBar;

    sub-float v6, v7, v4

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setAlpha(F)V

    goto :goto_1
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 99
    invoke-super {p0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->onFinishInflate()V

    .line 100
    new-array v0, v3, [F

    aput v1, v0, v2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator1:Landroid/animation/ValueAnimator;

    .line 101
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator1:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 103
    new-array v0, v3, [F

    aput v1, v0, v2

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator2:Landroid/animation/ValueAnimator;

    .line 104
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator2:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator1:Landroid/animation/ValueAnimator;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    .line 107
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    const/4 v2, 0x0

    .line 315
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->SHOWING_ONE_FIELD:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_TO_ONE_FIELD:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    if-ne v0, v1, :cond_1

    .line 317
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 318
    iput-boolean v2, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mSupportShowingOneField:Z

    .line 319
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_ONE_FIELD_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    .line 320
    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->setChildrenViewVisibility(I)V

    .line 321
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 322
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 323
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 324
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 327
    check-cast p1, Landroid/widget/EditText;

    .end local p1    # "v":Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFirstFieldHint:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->onAnimationUpdate(Landroid/animation/ValueAnimator;)V

    .line 333
    :cond_1
    return-void

    .line 321
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 88
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 89
    check-cast v0, Landroid/os/Bundle;

    .line 90
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->KEY_SUPPORT_SHOWING_ONE_FIELD:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mSupportShowingOneField:Z

    .line 91
    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->KEY_PARENT_STATE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 95
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 76
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 77
    .local v0, "bundle":Landroid/os/Bundle;
    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->KEY_PARENT_STATE:Ljava/lang/String;

    invoke-super {p0}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 82
    sget-object v1, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->KEY_SUPPORT_SHOWING_ONE_FIELD:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mSupportShowingOneField:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 83
    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 304
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/layout/AddressFieldsLayout;->onSizeChanged(IIII)V

    .line 308
    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sub-float v1, v2, v1

    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->getViewHeightAtTransitionStart()I

    move-result v2

    sub-int/2addr v2, p2

    int-to-float v2, v2

    mul-float v0, v1, v2

    .line 310
    .local v0, "translationY":F
    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->invokeOnHeightChanged(F)V

    .line 311
    return-void
.end method

.method public setFields(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "fields":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 129
    iget-boolean v4, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mSupportShowingOneField:Z

    if-eqz v4, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v2, :cond_1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Landroid/widget/EditText;

    if-eqz v4, :cond_1

    move v1, v2

    .line 131
    .local v1, "oneFieldModePossible":Z
    :goto_0
    sget-object v4, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$2;->$SwitchMap$com$google$android$finsky$layout$AddressFieldsLayoutHoneycomb$State:[I

    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    invoke-virtual {v5}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 156
    const-string v4, "enum %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    aput-object v5, v2, v3

    invoke-static {v4, v2}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 163
    :goto_1
    if-eqz v1, :cond_2

    .line 164
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 168
    .local v0, "firstField":Landroid/widget/EditText;
    invoke-virtual {v0}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mFirstFieldHint:Ljava/lang/CharSequence;

    .line 169
    const v2, 0x7f0c0375

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setHint(I)V

    .line 171
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 179
    .end local v0    # "firstField":Landroid/widget/EditText;
    :cond_0
    :goto_2
    return-void

    .end local v1    # "oneFieldModePossible":Z
    :cond_1
    move v1, v3

    .line 129
    goto :goto_0

    .line 133
    .restart local v1    # "oneFieldModePossible":Z
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->addViews(Ljava/util/List;)V

    goto :goto_1

    .line 138
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->addViews(Ljava/util/List;)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->makeOnlyFirstFieldVisible()V

    goto :goto_1

    .line 148
    :pswitch_2
    iput-object p1, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mNewFields:Ljava/util/List;

    goto :goto_1

    .line 152
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->addViews(Ljava/util/List;)V

    .line 153
    const/16 v2, 0x8

    invoke-direct {p0, v2}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->setChildrenViewVisibility(I)V

    goto :goto_1

    .line 176
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mSupportShowingOneField:Z

    if-eqz v2, :cond_0

    .line 177
    iput-boolean v3, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mSupportShowingOneField:Z

    goto :goto_2

    .line 131
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setOnHeightOffsetChangedListener(Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;)V
    .locals 0
    .param p1, "onHeightChangedListener"    # Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

    .prologue
    .line 252
    iput-object p1, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mOnHeightChangedListener:Lcom/google/android/finsky/layout/OnHeightOffsetChangedListener;

    .line 253
    return-void
.end method

.method public showFields()V
    .locals 2

    .prologue
    .line 183
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$2;->$SwitchMap$com$google$android$finsky$layout$AddressFieldsLayoutHoneycomb$State:[I

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    .line 204
    :goto_0
    return-void

    .line 185
    :sswitch_0
    iget-boolean v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mSupportShowingOneField:Z

    if-eqz v0, :cond_0

    .line 186
    invoke-direct {p0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->makeOnlyFirstFieldVisible()V

    .line 187
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_TO_ONE_FIELD:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    .line 192
    :goto_1
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 193
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 189
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->setChildrenViewVisibility(I)V

    .line 190
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_TO_ALL_FIELDS:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    goto :goto_1

    .line 199
    :sswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mExpandCalledWhileContracting:Z

    goto :goto_0

    .line 183
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x7 -> :sswitch_0
    .end sparse-switch

    .line 192
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public showProgressBar()V
    .locals 4

    .prologue
    .line 208
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$2;->$SwitchMap$com$google$android$finsky$layout$AddressFieldsLayoutHoneycomb$State:[I

    iget-object v1, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    invoke-virtual {v1}, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 227
    :goto_0
    :pswitch_0
    return-void

    .line 211
    :pswitch_1
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_TO_PROGRESS_BAR:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    .line 212
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 213
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 214
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 215
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 216
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 221
    :pswitch_2
    sget-object v0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;->TRANSITION_TO_PROGRESS_BAR:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mState:Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb$State;

    .line 222
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_0

    .line 208
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 213
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public showUpperRightProgressBar()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mUpperRightProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mUpperRightProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setAlpha(F)V

    .line 113
    iget-object v0, p0, Lcom/google/android/finsky/layout/AddressFieldsLayoutHoneycomb;->mUpperRightProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 114
    return-void
.end method
