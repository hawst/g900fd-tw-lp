.class public Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;
.super Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;
.source "RedeemCodeActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeBaseActivity;-><init>()V

    return-void
.end method

.method public static createBuyFlowIntent(Ljava/lang/String;ILcom/google/android/finsky/protos/Common$Docid;I)Landroid/content/Intent;
    .locals 8
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "backend"    # I
    .param p2, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;
    .param p3, "offerType"    # I

    .prologue
    const/4 v6, 0x0

    .line 58
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    .local v0, "intent":Landroid/content/Intent;
    const/4 v2, 0x1

    move-object v1, p0

    move v3, p1

    move-object v4, p2

    move v5, p3

    move-object v7, v6

    invoke-static/range {v0 .. v7}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->putIntentExtras(Landroid/content/Intent;Ljava/lang/String;IILcom/google/android/finsky/protos/Common$Docid;ILjava/lang/String;Ljava/lang/String;)V

    .line 61
    return-object v0
.end method

.method public static createIntent(Ljava/lang/String;I)Landroid/content/Intent;
    .locals 1
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "redemptionContext"    # I

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-static {p0, p1, v0, v0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->createIntent(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createIntent(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 8
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "redemptionContext"    # I
    .param p2, "prefillCode"    # Ljava/lang/String;
    .param p3, "partnerPayload"    # Ljava/lang/String;

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    const-class v2, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    .local v0, "intent":Landroid/content/Intent;
    const/4 v3, 0x3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    move v2, p1

    move-object v6, p2

    move-object v7, p3

    invoke-static/range {v0 .. v7}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->putIntentExtras(Landroid/content/Intent;Ljava/lang/String;IILcom/google/android/finsky/protos/Common$Docid;ILjava/lang/String;Ljava/lang/String;)V

    .line 44
    return-object v0
.end method


# virtual methods
.method protected getActivityLayout()I
    .locals 1

    .prologue
    .line 66
    const v0, 0x7f040181

    return v0
.end method

.method protected getBillingUiMode()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method protected getPlayStoreUiElementType()I
    .locals 1

    .prologue
    .line 81
    const/16 v0, 0x370

    return v0
.end method

.method protected handleMessageAndFinish(Ljava/lang/String;)V
    .locals 0
    .param p1, "redeemedOfferHtml"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/lightpurchase/billingprofile/instruments/RedeemCodeActivity;->finish()V

    .line 77
    return-void
.end method
