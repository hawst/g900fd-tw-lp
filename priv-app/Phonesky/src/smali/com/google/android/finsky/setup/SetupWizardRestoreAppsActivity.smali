.class public Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "SetupWizardRestoreAppsActivity.java"

# interfaces
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/fragments/SidecarFragment$Listener;
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# static fields
.field private static final mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

.field private final mAppsMenuOnClickListener:Landroid/view/View$OnClickListener;

.field private mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

.field private mCurrentDevicePosition:I

.field private mCurrentSelectedBackupDocs:[Z

.field private mDeviceListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

.field private final mDeviceMenuOnClickListener:Landroid/view/View$OnClickListener;

.field private mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mIsBackupDocsLoaded:Z

.field private mIsCreatedFinished:Z

.field protected mMainView:Landroid/view/ViewGroup;

.field private mParcelableBackupDeviceInfos:[Landroid/os/Parcelable;

.field private mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

.field private mShowAppDialogPostLoad:Z

.field private mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const/16 v0, 0x9c4

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    sput-object v0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    .line 70
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getDeviceMenuOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mDeviceMenuOnClickListener:Landroid/view/View$OnClickListener;

    .line 71
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getAppsMenuOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppsMenuOnClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->shouldSetupAsNewDevice()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;
    .param p1, "x1"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->setResultAndFinish(I)V

    return-void
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)[Landroid/os/Parcelable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mParcelableBackupDeviceInfos:[Landroid/os/Parcelable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->restoreSelectedPackages()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Lcom/google/android/finsky/analytics/FinskyEventLog;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)Lcom/google/android/finsky/setup/RestoreAppsSidecar;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->showAppListDialog()V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsBackupDocsLoaded:Z

    return p1
.end method

.method static synthetic access$902(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mShowAppDialogPostLoad:Z

    return p1
.end method

.method private configureAppsMenuClickListener(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    if-eqz v0, :cond_0

    .line 370
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppsMenuOnClickListener:Landroid/view/View$OnClickListener;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 372
    :cond_0
    return-void

    .line 370
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private configureDeviceMenuClickListener(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mDeviceListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    if-eqz v0, :cond_0

    .line 364
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mDeviceListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mDeviceMenuOnClickListener:Landroid/view/View$OnClickListener;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 366
    :cond_0
    return-void

    .line 364
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private configureNavNextButton()V
    .locals 3

    .prologue
    .line 375
    invoke-static {p0}, Lcom/google/android/finsky/utils/SetupWizardUtils;->getNavBarIfPossible(Landroid/app/Activity;)Lcom/google/android/finsky/setup/SetupWizardNavBar;

    move-result-object v0

    .line 376
    .local v0, "setupWizardNavBar":Lcom/google/android/finsky/setup/SetupWizardNavBar;
    invoke-virtual {v0}, Lcom/google/android/finsky/setup/SetupWizardNavBar;->getNextButton()Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$1;-><init>(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;Lcom/google/android/finsky/setup/SetupWizardNavBar;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    return-void
.end method

.method public static createIntent(Ljava/lang/String;[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;)Landroid/content/Intent;
    .locals 7
    .param p0, "accountName"    # Ljava/lang/String;
    .param p1, "backupDeviceInfos"    # [Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    .prologue
    .line 90
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/google/android/finsky/FinskyApp;->getDfeApi(Ljava/lang/String;)Lcom/google/android/finsky/api/DfeApi;

    move-result-object v1

    .line 91
    .local v1, "dfeApi":Lcom/google/android/finsky/api/DfeApi;
    new-instance v3, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    const-class v6, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;

    invoke-direct {v3, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 92
    .local v3, "intent":Landroid/content/Intent;
    const-string v5, "authAccount"

    invoke-virtual {v3, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    array-length v5, p1

    new-array v4, v5, [Lcom/google/android/finsky/utils/ParcelableProto;

    .line 95
    .local v4, "parcelableBackupDeviceInfos":[Lcom/google/android/finsky/utils/ParcelableProto;, "[Lcom/google/android/finsky/utils/ParcelableProto<Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, p1

    if-ge v2, v5, :cond_0

    .line 96
    aget-object v5, p1, v2

    invoke-static {v5}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v5

    aput-object v5, v4, v2

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 101
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 102
    .local v0, "backupDeviceBundle":Landroid/os/Bundle;
    const-string v5, "SetupWizardRestoreAppsActivity.backup_device_infos"

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 103
    const-string v5, "SetupWizardRestoreAppsActivity.backup_device_infos_bundle"

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 104
    return-object v3
.end method

.method private finishCreate()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 168
    iget-boolean v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsCreatedFinished:Z

    if-eqz v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 171
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsCreatedFinished:Z

    .line 173
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 174
    .local v7, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0401a5

    const/4 v1, 0x0

    invoke-virtual {v7, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mMainView:Landroid/view/ViewGroup;

    .line 175
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->setContentView(Landroid/view/View;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mMainView:Landroid/view/ViewGroup;

    const v1, 0x7f0a009c

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 178
    .local v9, "title":Landroid/widget/TextView;
    const v0, 0x7f0c03e6

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mMainView:Landroid/view/ViewGroup;

    const v1, 0x7f0a00c4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup;

    .line 180
    .local v6, "contentFrame":Landroid/view/ViewGroup;
    const v0, 0x7f0401ae

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mMainView:Landroid/view/ViewGroup;

    invoke-virtual {v7, v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 182
    .local v8, "restoreAppsView":Landroid/view/View;
    invoke-virtual {v6, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 184
    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/finsky/utils/SetupWizardUtils;->configureBasicUi(Landroid/app/Activity;Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;IZZZ)V

    .line 188
    const v0, 0x7f0a0397

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mDeviceListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    .line 190
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->syncDeviceSelector()V

    .line 191
    invoke-direct {p0, v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureDeviceMenuClickListener(Z)V

    .line 194
    const v0, 0x7f0a0399

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    .line 196
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->syncAppSelector()V

    .line 197
    invoke-direct {p0, v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureAppsMenuClickListener(Z)V

    .line 199
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureNavNextButton()V

    goto :goto_0
.end method

.method private getAppsMenuOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 395
    new-instance v0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$2;-><init>(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)V

    return-object v0
.end method

.method private getDeviceMenuOnClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 417
    new-instance v0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity$3;-><init>(Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;)V

    return-object v0
.end method

.method private getSelectedAppsCount()I
    .locals 5

    .prologue
    .line 279
    const/4 v4, 0x0

    .line 280
    .local v4, "selectedAppsCount":I
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentSelectedBackupDocs:[Z

    .local v0, "arr$":[Z
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-boolean v2, v0, v1

    .line 281
    .local v2, "isSelected":Z
    if-eqz v2, :cond_0

    .line 282
    add-int/lit8 v4, v4, 0x1

    .line 280
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 285
    .end local v2    # "isSelected":Z
    :cond_1
    return v4
.end method

.method private restoreSelectedPackages()V
    .locals 15

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    .line 241
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getState()I

    move-result v0

    const/4 v2, 0x5

    if-eq v0, v2, :cond_0

    .line 276
    :goto_0
    return-void

    .line 244
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getSelectedAppsCount()I

    move-result v14

    .line 245
    .local v14, "selectedAppsCount":I
    new-array v4, v14, [Ljava/lang/String;

    .line 246
    .local v4, "packageNames":[Ljava/lang/String;
    new-array v5, v14, [I

    .line 247
    .local v5, "versionCodes":[I
    new-array v6, v14, [Ljava/lang/String;

    .line 248
    .local v6, "titles":[Ljava/lang/String;
    new-array v7, v14, [I

    .line 249
    .local v7, "priorities":[I
    new-array v9, v14, [Ljava/lang/String;

    .line 250
    .local v9, "appIconUrls":[Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getBackupDocumentInfos()[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    move-result-object v11

    .line 251
    .local v11, "currentBackupDocumentInfos":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    const/4 v13, 0x0

    .local v13, "i":I
    const/4 v10, 0x0

    .local v10, "curPosition":I
    :goto_1
    array-length v0, v11

    if-ge v13, v0, :cond_4

    .line 252
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentSelectedBackupDocs:[Z

    aget-boolean v0, v0, v13

    if-eqz v0, :cond_2

    .line 253
    aget-object v12, v11, v13

    .line 254
    .local v12, "documentInfo":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    iget-object v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    aput-object v0, v4, v10

    .line 255
    iget v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->versionCode:I

    aput v0, v5, v10

    .line 256
    iget-object v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->title:Ljava/lang/String;

    aput-object v0, v6, v10

    .line 258
    const/4 v0, 0x3

    aput v0, v7, v10

    .line 259
    iget-boolean v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->hasRestorePriority:Z

    if-eqz v0, :cond_1

    iget v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->restorePriority:I

    const/16 v2, 0x64

    if-ge v0, v2, :cond_1

    .line 260
    aput v1, v7, v10

    .line 263
    :cond_1
    iget-object v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v0, :cond_3

    iget-object v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/Common$Image;->hasImageUrl:Z

    if-eqz v0, :cond_3

    iget-object v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/Common$Image;->hasSupportsFifeUrlOptions:Z

    if-eqz v0, :cond_3

    iget-object v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/Common$Image;->supportsFifeUrlOptions:Z

    if-eqz v0, :cond_3

    .line 267
    iget-object v0, v12, Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    iget-object v0, v0, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    aput-object v0, v9, v10

    .line 271
    :goto_2
    add-int/lit8 v10, v10, 0x1

    .line 251
    .end local v12    # "documentInfo":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 269
    .restart local v12    # "documentInfo":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    :cond_3
    aput-object v8, v9, v10

    goto :goto_2

    .line 274
    .end local v12    # "documentInfo":Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAccountName:Ljava/lang/String;

    move v3, v1

    invoke-static/range {v0 .. v9}, Lcom/google/android/finsky/services/RestoreService;->restorePackages(Landroid/content/Context;ZLjava/lang/String;Z[Ljava/lang/String;[I[Ljava/lang/String;[I[Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setResultAndFinish(I)V
    .locals 0
    .param p1, "result"    # I

    .prologue
    .line 289
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->setResult(I)V

    .line 290
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->finish()V

    .line 291
    return-void
.end method

.method private shouldSetupAsNewDevice()Z
    .locals 2

    .prologue
    .line 294
    iget v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showAppListDialog()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 203
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->shouldSetupAsNewDevice()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v0}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isLightTheme()Z

    move-result v0

    if-eqz v0, :cond_1

    const v12, 0x7f0d01bf

    .line 208
    .local v12, "themeId":I
    :goto_1
    new-instance v8, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v8}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 209
    .local v8, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    const v0, 0x7f0401ac

    invoke-virtual {v8, v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setLayoutId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0386

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0c0134

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setNegativeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v12}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setThemeId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x9c9

    const/16 v3, 0x9ca

    const/16 v4, 0x9cb

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccount()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v2, v1, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 219
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    invoke-virtual {v0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getBackupDocumentInfos()[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    move-result-object v7

    .line 220
    .local v7, "backupDocumentInfos":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    new-instance v10, Ljava/util/ArrayList;

    array-length v0, v7

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 222
    .local v10, "parcelableBackupDocumentInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    array-length v0, v7

    if-ge v9, v0, :cond_2

    .line 223
    aget-object v0, v7, v9

    invoke-static {v0}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 206
    .end local v7    # "backupDocumentInfos":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    .end local v8    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v9    # "i":I
    .end local v10    # "parcelableBackupDocumentInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .end local v12    # "themeId":I
    :cond_1
    const v12, 0x7f0d01be

    goto :goto_1

    .line 226
    .restart local v7    # "backupDocumentInfos":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    .restart local v8    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .restart local v9    # "i":I
    .restart local v10    # "parcelableBackupDocumentInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .restart local v12    # "themeId":I
    :cond_2
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 227
    .local v6, "argumentsBundle":Landroid/os/Bundle;
    const-string v0, "SetupWizardAppListDialog.backupDocs"

    invoke-virtual {v6, v0, v10}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 229
    const-string v0, "SetupWizardAppListDialog.selectedBackupDocs"

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentSelectedBackupDocs:[Z

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 232
    invoke-virtual {v8, v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setViewConfiguration(Landroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 233
    invoke-virtual {v8}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v11

    .line 234
    .local v11, "sad":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "SetupWizardRestoreAppsActivity.backupAppsDialog"

    invoke-virtual {v11, v0, v1}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private syncAppSelector()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 318
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    if-nez v4, :cond_0

    .line 359
    :goto_0
    return-void

    .line 321
    :cond_0
    const v4, 0x7f0a0398

    invoke-virtual {p0, v4}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 323
    .local v0, "appListSelectorTitle":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 325
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->shouldSetupAsNewDevice()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 326
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    invoke-virtual {v4, v6}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setVisibility(I)V

    .line 327
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 330
    :cond_1
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setVisibility(I)V

    .line 331
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 333
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getState()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 335
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    const v5, 0x7f0c01d6

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setTexts(Ljava/lang/String;)V

    goto :goto_0

    .line 338
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    array-length v1, v4

    .line 339
    .local v1, "numOfDevices":I
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    const v5, 0x7f10000e

    invoke-virtual {v2, v5, v1}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setTexts(Ljava/lang/String;)V

    goto :goto_0

    .line 343
    .end local v1    # "numOfDevices":I
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getSelectedAppsCount()I

    move-result v3

    .line 344
    .local v3, "selectedAppsCount":I
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    invoke-virtual {v4}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getBackupDocumentInfos()[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    move-result-object v4

    array-length v4, v4

    if-ne v3, v4, :cond_2

    .line 345
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    const v5, 0x7f0c03ee

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setTexts(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 347
    :cond_2
    if-nez v3, :cond_3

    .line 348
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    const v5, 0x7f0c03ef

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setTexts(Ljava/lang/String;)V

    goto :goto_0

    .line 351
    :cond_3
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAppListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    const v5, 0x7f10000f

    invoke-virtual {v2, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setTexts(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 333
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private syncDeviceSelector()V
    .locals 10

    .prologue
    .line 299
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->shouldSetupAsNewDevice()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 300
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mDeviceListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c03ec

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setTexts(Ljava/lang/String;)V

    .line 315
    :goto_0
    return-void

    .line 306
    :cond_0
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v5, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    iget v8, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I

    aget-object v5, v5, v8

    iget-wide v8, v5, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->lastCheckinTimeMs:J

    sub-long/2addr v6, v8

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v0

    .line 308
    .local v0, "days":J
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 309
    .local v3, "resources":Landroid/content/res/Resources;
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-nez v4, :cond_1

    const v4, 0x7f0c03f1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 313
    .local v2, "lastUsedTitle":Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mDeviceListSelector:Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;

    iget-object v5, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    iget v6, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5, v2}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsSelector;->setTexts(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 309
    .end local v2    # "lastUsedTitle":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f100010

    long-to-int v6, v0

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 559
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unwanted children."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 549
    sget-object v0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 109
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 112
    .local v2, "intent":Landroid/content/Intent;
    new-instance v4, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-direct {v4, v2}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;-><init>(Landroid/content/Intent;)V

    iput-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    .line 113
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSetupWizardParams:Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;

    invoke-virtual {v4}, Lcom/google/android/finsky/utils/SetupWizardUtils$SetupWizardParams;->isLightTheme()Z

    move-result v4

    if-eqz v4, :cond_0

    const v3, 0x7f0d01bf

    .line 115
    .local v3, "themeId":I
    :goto_0
    invoke-virtual {p0, v3}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->setTheme(I)V

    .line 116
    const-string v4, "authAccount"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAccountName:Ljava/lang/String;

    .line 117
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAccountName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 118
    const-string v4, "SetupWizardRestoreAppsActivity.backup_device_infos_bundle"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 119
    .local v0, "backupDeviceBundle":Landroid/os/Bundle;
    const-string v4, "SetupWizardRestoreAppsActivity.backup_device_infos"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mParcelableBackupDeviceInfos:[Landroid/os/Parcelable;

    .line 121
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mParcelableBackupDeviceInfos:[Landroid/os/Parcelable;

    array-length v4, v4

    new-array v4, v4, [Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    iput-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    .line 122
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mParcelableBackupDeviceInfos:[Landroid/os/Parcelable;

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 123
    iget-object v5, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mParcelableBackupDeviceInfos:[Landroid/os/Parcelable;

    aget-object v4, v4, v1

    check-cast v4, Lcom/google/android/finsky/utils/ParcelableProto;

    invoke-virtual {v4}, Lcom/google/android/finsky/utils/ParcelableProto;->getPayload()Lcom/google/protobuf/nano/MessageNano;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    aput-object v4, v5, v1

    .line 122
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 113
    .end local v0    # "backupDeviceBundle":Landroid/os/Bundle;
    .end local v1    # "i":I
    .end local v3    # "themeId":I
    :cond_0
    const v3, 0x7f0d01be

    goto :goto_0

    .line 127
    .restart local v0    # "backupDeviceBundle":Landroid/os/Bundle;
    .restart local v1    # "i":I
    .restart local v3    # "themeId":I
    :cond_1
    if-eqz p1, :cond_3

    .line 128
    const-string v4, "SetupWizardRestoreAppsActivity.current_device"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I

    .line 129
    const-string v4, "SetupWizardRestoreAppsActivity.backup_docs_loaded"

    invoke-virtual {p1, v4, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsBackupDocsLoaded:Z

    .line 130
    const-string v4, "SetupWizardRestoreAppsActivity.current_selected_backup_docs"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentSelectedBackupDocs:[Z

    .line 136
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "SetupWizardRestoreAppsActivity.sidecar"

    invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    check-cast v4, Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    iput-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    .line 137
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    if-nez v4, :cond_2

    .line 139
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mAccountName:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->newInstance(Ljava/lang/String;)Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    .line 140
    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    const-string v6, "SetupWizardRestoreAppsActivity.sidecar"

    invoke-virtual {v4, v5, v6}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 145
    :cond_2
    return-void

    .line 133
    :cond_3
    iget-object v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7, p0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    goto :goto_2
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 544
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 508
    packed-switch p1, :pswitch_data_0

    .line 539
    :cond_0
    :goto_0
    return-void

    .line 510
    :pswitch_0
    const-string v2, "SetupWizardRestoreDeviceDialogView.selectedDevicePosition"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 512
    .local v1, "newSelection":I
    iget v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I

    if-eq v1, v2, :cond_0

    .line 513
    iput v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I

    .line 514
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->shouldSetupAsNewDevice()Z

    move-result v2

    if-nez v2, :cond_1

    .line 515
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const/16 v3, 0x9c6

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEvent(I[BLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 518
    invoke-direct {p0, v6}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureDeviceMenuClickListener(Z)V

    .line 519
    iput-boolean v6, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsBackupDocsLoaded:Z

    .line 520
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    iget-object v3, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    aget-object v3, v3, v1

    iget-wide v4, v3, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->androidId:J

    invoke-virtual {v2, v4, v5}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->fetchBackupDocs(J)V

    .line 524
    :goto_1
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->syncDeviceSelector()V

    .line 525
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->syncAppSelector()V

    goto :goto_0

    .line 522
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsBackupDocsLoaded:Z

    goto :goto_1

    .line 529
    .end local v1    # "newSelection":I
    :pswitch_1
    const-string v2, "SetupWizardAppListDialog.selectedBackupDocs"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getBooleanArray(Ljava/lang/String;)[Z

    move-result-object v0

    .line 531
    .local v0, "currentSelection":[Z
    array-length v2, v0

    iget-object v3, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentSelectedBackupDocs:[Z

    array-length v3, v3

    if-eq v2, v3, :cond_2

    .line 532
    const-string v2, "Length mismatched, can\'t update"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 535
    :cond_2
    iput-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentSelectedBackupDocs:[Z

    .line 536
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->syncAppSelector()V

    goto :goto_0

    .line 508
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 149
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 150
    const-string v0, "SetupWizardRestoreAppsActivity.current_device"

    iget v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 151
    const-string v0, "SetupWizardRestoreAppsActivity.current_selected_backup_docs"

    iget-object v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentSelectedBackupDocs:[Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 152
    const-string v0, "SetupWizardRestoreAppsActivity.backup_docs_loaded"

    iget-boolean v1, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsBackupDocsLoaded:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 153
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 157
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStart()V

    .line 158
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    invoke-virtual {v0, p0}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 159
    return-void
.end method

.method public onStateChange(Lcom/google/android/finsky/fragments/SidecarFragment;)V
    .locals 6
    .param p1, "fragment"    # Lcom/google/android/finsky/fragments/SidecarFragment;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 458
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    if-eq p1, v2, :cond_1

    .line 459
    const-string v2, "Received state change for unknown fragment: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 503
    :cond_0
    :goto_0
    return-void

    .line 462
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getState()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 464
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    iget-object v3, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mBackupDeviceInfos:[Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;

    iget v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentDevicePosition:I

    aget-object v3, v3, v4

    iget-wide v4, v3, Lcom/google/android/finsky/protos/Restore$BackupDeviceInfo;->androidId:J

    invoke-virtual {v2, v4, v5}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->fetchBackupDocs(J)V

    .line 465
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->finishCreate()V

    goto :goto_0

    .line 470
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->finishCreate()V

    .line 471
    invoke-direct {p0, v4}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureDeviceMenuClickListener(Z)V

    .line 472
    invoke-direct {p0, v4}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureAppsMenuClickListener(Z)V

    .line 473
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->syncAppSelector()V

    goto :goto_0

    .line 476
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->finishCreate()V

    .line 477
    invoke-direct {p0, v3}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureDeviceMenuClickListener(Z)V

    .line 478
    invoke-direct {p0, v3}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureAppsMenuClickListener(Z)V

    .line 479
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    invoke-virtual {v2}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->getBackupDocumentInfos()[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;

    move-result-object v0

    .line 480
    .local v0, "backupDocumentInfo":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    iget-boolean v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsBackupDocsLoaded:Z

    if-nez v2, :cond_3

    .line 481
    array-length v2, v0

    new-array v2, v2, [Z

    iput-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentSelectedBackupDocs:[Z

    .line 483
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 484
    iget-object v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mCurrentSelectedBackupDocs:[Z

    aput-boolean v3, v2, v1

    .line 483
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 486
    :cond_2
    iput-boolean v3, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mIsBackupDocsLoaded:Z

    .line 488
    .end local v1    # "i":I
    :cond_3
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->syncAppSelector()V

    .line 489
    iget-boolean v2, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mShowAppDialogPostLoad:Z

    if-eqz v2, :cond_0

    .line 490
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->showAppListDialog()V

    .line 491
    iput-boolean v4, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mShowAppDialogPostLoad:Z

    goto :goto_0

    .line 495
    .end local v0    # "backupDocumentInfo":[Lcom/google/android/finsky/protos/Restore$BackupDocumentInfo;
    :pswitch_4
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->finishCreate()V

    .line 496
    invoke-direct {p0, v3}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureDeviceMenuClickListener(Z)V

    .line 497
    invoke-direct {p0, v4}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->configureAppsMenuClickListener(Z)V

    .line 498
    invoke-direct {p0}, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->syncAppSelector()V

    goto :goto_0

    .line 462
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/finsky/setup/SetupWizardRestoreAppsActivity;->mSidecar:Lcom/google/android/finsky/setup/RestoreAppsSidecar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/setup/RestoreAppsSidecar;->setListener(Lcom/google/android/finsky/fragments/SidecarFragment$Listener;)V

    .line 164
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onStop()V

    .line 165
    return-void
.end method
