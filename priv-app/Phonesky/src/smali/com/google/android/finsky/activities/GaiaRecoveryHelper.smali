.class public Lcom/google/android/finsky/activities/GaiaRecoveryHelper;
.super Ljava/lang/Object;
.source "GaiaRecoveryHelper.java"


# static fields
.field private static sCurrentAccountName:Ljava/lang/String;

.field private static sGaiaAuthIntent:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    sput-object v0, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sGaiaAuthIntent:Landroid/app/PendingIntent;

    .line 35
    sput-object v0, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sCurrentAccountName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sCurrentAccountName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Landroid/app/PendingIntent;)Landroid/app/PendingIntent;
    .locals 0
    .param p0, "x0"    # Landroid/app/PendingIntent;

    .prologue
    .line 31
    sput-object p0, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sGaiaAuthIntent:Landroid/app/PendingIntent;

    return-object p0
.end method

.method private static getRecoveryIntentIfExists()Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 154
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/finsky/FinskyApp;->getCurrentAccountName()Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "currentAccount":Ljava/lang/String;
    sget-object v1, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sCurrentAccountName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sGaiaAuthIntent:Landroid/app/PendingIntent;

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static launchGaiaRecoveryDialog(Landroid/content/res/Resources;Landroid/support/v4/app/FragmentManager;ILjava/lang/String;)V
    .locals 9
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "requestCodeForResponse"    # I
    .param p3, "tagToUse"    # Ljava/lang/String;

    .prologue
    const v5, 0x7f0c020d

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 103
    invoke-static {}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->shouldShowGaiaRecoveryDialog()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    :goto_0
    return-void

    .line 107
    :cond_0
    const v0, 0x7f0c01b6

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 109
    .local v8, "message":Ljava/lang/String;
    new-instance v6, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 110
    .local v6, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v6, v8}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessage(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCanceledOnTouchOutside(Z)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    const/16 v1, 0x133

    const/16 v3, 0xf7

    const/16 v4, 0xf8

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setEventLog(I[BIILandroid/accounts/Account;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2, p2, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 117
    invoke-virtual {v6}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v7

    .line 118
    .local v7, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {v7, p1, p3}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static onPositiveGaiaRecoveryDialogResponse()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 132
    invoke-static {}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->getRecoveryIntentIfExists()Landroid/app/PendingIntent;

    move-result-object v1

    .line 133
    .local v1, "intent":Landroid/app/PendingIntent;
    if-nez v1, :cond_0

    .line 134
    const-string v2, "Called Gaia recovery flow but PendingIntent didn\'t exist."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    :goto_0
    return-void

    .line 139
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    sput-object v4, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sGaiaAuthIntent:Landroid/app/PendingIntent;

    goto :goto_0

    .line 140
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Landroid/app/PendingIntent$CanceledException;
    :try_start_1
    const-string v2, "PendingIntent for GAIA auth was canceled"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/utils/FinskyLog;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    sput-object v4, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sGaiaAuthIntent:Landroid/app/PendingIntent;

    goto :goto_0

    .end local v0    # "e":Landroid/app/PendingIntent$CanceledException;
    :catchall_0
    move-exception v2

    sput-object v4, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sGaiaAuthIntent:Landroid/app/PendingIntent;

    throw v2
.end method

.method public static prefetchAndCacheGaiaAuthRecoveryIntent(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "currentAccount"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 43
    sget-object v0, Lcom/google/android/finsky/config/G;->enableGaiaRecovery:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 44
    const-string v0, "Skipping fetching recovery intent -- gaia recovery disabled"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    sget-object v0, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sGaiaAuthIntent:Landroid/app/PendingIntent;

    if-eqz v0, :cond_2

    .line 51
    sget-object v0, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sCurrentAccountName:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    sput-object v1, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sCurrentAccountName:Ljava/lang/String;

    .line 57
    sput-object v1, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sGaiaAuthIntent:Landroid/app/PendingIntent;

    .line 60
    :cond_2
    sput-object p1, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sCurrentAccountName:Ljava/lang/String;

    .line 64
    sget-object v0, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->sCurrentAccountName:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 65
    const-string v0, "Skipping fetching recovery intent -- no user set."

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    :cond_3
    new-instance v0, Lcom/google/android/finsky/activities/GaiaRecoveryHelper$1;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper$1;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/content/Context;

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public static shouldShowGaiaRecoveryDialog()Z
    .locals 1

    .prologue
    .line 125
    invoke-static {}, Lcom/google/android/finsky/activities/GaiaRecoveryHelper;->getRecoveryIntentIfExists()Landroid/app/PendingIntent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
