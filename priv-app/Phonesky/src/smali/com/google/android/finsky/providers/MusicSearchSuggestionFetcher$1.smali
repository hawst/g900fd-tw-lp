.class Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;
.super Ljava/lang/Object;
.source "MusicSearchSuggestionFetcher.java"

# interfaces
.implements Lcom/android/volley/Response$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->makeRequest(Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/volley/Response$Listener",
        "<",
        "Lorg/json/JSONArray;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;

.field final synthetic val$listener:Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;->this$0:Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;

    iput-object p2, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;->val$listener:Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 39
    check-cast p1, Lorg/json/JSONArray;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;->onResponse(Lorg/json/JSONArray;)V

    return-void
.end method

.method public onResponse(Lorg/json/JSONArray;)V
    .locals 7
    .param p1, "responseArray"    # Lorg/json/JSONArray;

    .prologue
    .line 43
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object p1

    .line 44
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 45
    .local v2, "responseCount":I
    iget-object v0, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;->this$0:Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;

    iget-object v0, v0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->mSuggestionHandler:Lcom/google/android/finsky/providers/SuggestionHandler;

    const/4 v1, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;->this$0:Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;

    iget-wide v4, v4, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->mStartTimeMs:J

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/finsky/providers/SuggestionHandler;->sendSuggestionsReceivedLog(II[BJ)V

    .line 48
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v2, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;->this$0:Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;

    iget-object v0, v0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher;->mSuggestionHandler:Lcom/google/android/finsky/providers/SuggestionHandler;

    const v1, 0x108004f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v6}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v6, v1, v3, v4}, Lcom/google/android/finsky/providers/SuggestionHandler;->addRow(ILjava/lang/Object;Ljava/lang/String;[B)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;->val$listener:Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    invoke-interface {v0}, Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;->onComplete()V

    .line 56
    .end local v2    # "responseCount":I
    .end local v6    # "i":I
    :goto_1
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 54
    iget-object v0, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;->val$listener:Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    invoke-interface {v0}, Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;->onComplete()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/finsky/providers/MusicSearchSuggestionFetcher$1;->val$listener:Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;

    invoke-interface {v1}, Lcom/google/android/finsky/providers/SuggestionFetcher$OnCompleteListener;->onComplete()V

    throw v0
.end method
