.class Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$2;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"

# interfaces
.implements Lcom/google/android/play/image/BitmapLoader$BitmapLoadedHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->syncEnvironment()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)V
    .locals 0

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$2;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V
    .locals 3
    .param p1, "result"    # Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .prologue
    .line 386
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$2;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$300(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 388
    .local v0, "actionBarIcon":Landroid/graphics/drawable/BitmapDrawable;
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$2;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$400(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Landroid/view/MenuItem;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 389
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$2;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    # getter for: Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->mEnvironmentItem:Landroid/view/MenuItem;
    invoke-static {v1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->access$400(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;)Landroid/view/MenuItem;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 390
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 383
    check-cast p1, Lcom/google/android/play/image/BitmapLoader$BitmapContainer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$2;->onResponse(Lcom/google/android/play/image/BitmapLoader$BitmapContainer;)V

    return-void
.end method
