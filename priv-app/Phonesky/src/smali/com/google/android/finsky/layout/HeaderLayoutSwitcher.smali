.class public Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;
.super Lcom/google/android/finsky/layout/LayoutSwitcher;
.source "HeaderLayoutSwitcher.java"


# direct methods
.method public constructor <init>(Landroid/view/View;IIILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;I)V
    .locals 0
    .param p1, "pageLayout"    # Landroid/view/View;
    .param p2, "dataLayoutId"    # I
    .param p3, "errorLayoutId"    # I
    .param p4, "loadingLayoutId"    # I
    .param p5, "listener"    # Lcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;
    .param p6, "initialState"    # I

    .prologue
    .line 21
    invoke-direct/range {p0 .. p6}, Lcom/google/android/finsky/layout/LayoutSwitcher;-><init>(Landroid/view/View;IIILcom/google/android/finsky/layout/LayoutSwitcher$RetryButtonListener;I)V

    .line 22
    return-void
.end method


# virtual methods
.method protected setDataVisible(ZZ)V
    .locals 4
    .param p1, "show"    # Z
    .param p2, "goingToLoadingMode"    # Z

    .prologue
    const/4 v3, 0x0

    .line 26
    iget v1, p0, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;->mDataLayoutId:I

    if-gtz v1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;->mContentLayout:Landroid/view/View;

    iget v2, p0, Lcom/google/android/finsky/layout/HeaderLayoutSwitcher;->mDataLayoutId:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;

    .line 31
    .local v0, "dataView":Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;
    if-eqz v0, :cond_0

    .line 32
    if-nez p1, :cond_3

    .line 33
    if-nez p2, :cond_2

    .line 34
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setVisibility(I)V

    goto :goto_0

    .line 36
    :cond_2
    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setVisibility(I)V

    .line 37
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->hideContentView()V

    goto :goto_0

    .line 40
    :cond_3
    invoke-virtual {v0, v3}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->setVisibility(I)V

    .line 41
    invoke-virtual {v0}, Lcom/google/android/finsky/layout/play/FinskyHeaderListLayout;->showContentView()V

    goto :goto_0
.end method
