.class public Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
.super Ljava/lang/Object;
.source "PurchaseParams.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private appContinueUrl:Ljava/lang/String;

.field private appTitle:Ljava/lang/String;

.field private appVersionCode:I

.field private docid:Lcom/google/android/finsky/protos/Common$Docid;

.field private docidStr:Ljava/lang/String;

.field private iabParameters:Lcom/google/android/finsky/billing/IabParameters;

.field private offerType:I

.field private voucherId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Lcom/google/android/finsky/protos/Common$Docid;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->docidStr:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .prologue
    .line 146
    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->offerType:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .prologue
    .line 146
    iget v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->appVersionCode:I

    return v0
.end method

.method static synthetic access$400(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->appTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->appContinueUrl:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->voucherId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;)Lcom/google/android/finsky/billing/IabParameters;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;
    .locals 2

    .prologue
    .line 188
    new-instance v0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams;-><init>(Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$1;)V

    return-object v0
.end method

.method public setAppData(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    .locals 0
    .param p1, "appVersionCode"    # I
    .param p2, "appTitle"    # Ljava/lang/String;
    .param p3, "appContinueUrl"    # Ljava/lang/String;

    .prologue
    .line 174
    iput p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->appVersionCode:I

    .line 175
    iput-object p2, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->appTitle:Ljava/lang/String;

    .line 176
    iput-object p3, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->appContinueUrl:Ljava/lang/String;

    .line 177
    return-object p0
.end method

.method public setDocid(Lcom/google/android/finsky/protos/Common$Docid;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    .locals 0
    .param p1, "docid"    # Lcom/google/android/finsky/protos/Common$Docid;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    .line 163
    return-object p0
.end method

.method public setDocidStr(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    .locals 0
    .param p1, "docidStr"    # Ljava/lang/String;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->docidStr:Ljava/lang/String;

    .line 167
    return-object p0
.end method

.method public setDocument(Lcom/google/android/finsky/api/model/Document;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    .locals 1
    .param p1, "doc"    # Lcom/google/android/finsky/api/model/Document;

    .prologue
    .line 157
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getFullDocid()Lcom/google/android/finsky/protos/Common$Docid;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocid(Lcom/google/android/finsky/protos/Common$Docid;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .line 158
    invoke-virtual {p1}, Lcom/google/android/finsky/api/model/Document;->getDocId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->setDocidStr(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;

    .line 159
    return-object p0
.end method

.method public setIabParameters(Lcom/google/android/finsky/billing/IabParameters;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    .locals 0
    .param p1, "iabParameters"    # Lcom/google/android/finsky/billing/IabParameters;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->iabParameters:Lcom/google/android/finsky/billing/IabParameters;

    .line 185
    return-object p0
.end method

.method public setOfferType(I)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    .locals 0
    .param p1, "offerType"    # I

    .prologue
    .line 170
    iput p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->offerType:I

    .line 171
    return-object p0
.end method

.method public setVoucherId(Ljava/lang/String;)Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;
    .locals 0
    .param p1, "voucherId"    # Ljava/lang/String;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/finsky/billing/lightpurchase/PurchaseParams$Builder;->voucherId:Ljava/lang/String;

    .line 181
    return-object p0
.end method
