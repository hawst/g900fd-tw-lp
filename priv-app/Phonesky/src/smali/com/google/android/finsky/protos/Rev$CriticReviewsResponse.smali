.class public final Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;
.super Lcom/google/protobuf/nano/MessageNano;
.source "Rev.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/Rev;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CriticReviewsResponse"
.end annotation


# instance fields
.field public aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

.field public hasPercentFavorable:Z

.field public hasSourceText:Z

.field public hasTitle:Z

.field public hasTotalNumReviews:Z

.field public percentFavorable:I

.field public review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

.field public source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

.field public sourceText:Ljava/lang/String;

.field public title:Ljava/lang/String;

.field public totalNumReviews:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 241
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->clear()Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    .line 242
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 245
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->title:Ljava/lang/String;

    .line 246
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasTitle:Z

    .line 247
    iput-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    .line 248
    iput v1, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->totalNumReviews:I

    .line 249
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasTotalNumReviews:Z

    .line 250
    iput v1, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->percentFavorable:I

    .line 251
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasPercentFavorable:Z

    .line 252
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->sourceText:Ljava/lang/String;

    .line 253
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasSourceText:Z

    .line 254
    iput-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 255
    invoke-static {}, Lcom/google/android/finsky/protos/DocumentV2$Review;->emptyArray()[Lcom/google/android/finsky/protos/DocumentV2$Review;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 256
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->cachedSize:I

    .line 257
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 294
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 295
    .local v2, "size":I
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasTitle:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->title:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 296
    :cond_0
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->title:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 299
    :cond_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v3, :cond_2

    .line 300
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 303
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasTotalNumReviews:Z

    if-nez v3, :cond_3

    iget v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->totalNumReviews:I

    if-eqz v3, :cond_4

    .line 304
    :cond_3
    const/4 v3, 0x3

    iget v4, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->totalNumReviews:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 307
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasPercentFavorable:Z

    if-nez v3, :cond_5

    iget v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->percentFavorable:I

    if-eqz v3, :cond_6

    .line 308
    :cond_5
    const/4 v3, 0x4

    iget v4, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->percentFavorable:I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeUInt32Size(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 311
    :cond_6
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasSourceText:Z

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->sourceText:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 312
    :cond_7
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->sourceText:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 315
    :cond_8
    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v3, :cond_9

    .line 316
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 319
    :cond_9
    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v3, v3

    if-lez v3, :cond_b

    .line 320
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v3, v3

    if-ge v1, v3, :cond_b

    .line 321
    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    aget-object v0, v3, v1

    .line 322
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-eqz v0, :cond_a

    .line 323
    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 320
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 328
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$Review;
    .end local v1    # "i":I
    :cond_b
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;
    .locals 7
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 336
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 337
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 341
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 342
    :sswitch_0
    return-object p0

    .line 347
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->title:Ljava/lang/String;

    .line 348
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasTitle:Z

    goto :goto_0

    .line 352
    :sswitch_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    if-nez v5, :cond_1

    .line 353
    new-instance v5, Lcom/google/android/finsky/protos/Common$Image;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Common$Image;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    .line 355
    :cond_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 359
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->totalNumReviews:I

    .line 360
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasTotalNumReviews:Z

    goto :goto_0

    .line 364
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readUInt32()I

    move-result v5

    iput v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->percentFavorable:I

    .line 365
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasPercentFavorable:Z

    goto :goto_0

    .line 369
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->sourceText:Ljava/lang/String;

    .line 370
    iput-boolean v6, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasSourceText:Z

    goto :goto_0

    .line 374
    :sswitch_6
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-nez v5, :cond_2

    .line 375
    new-instance v5, Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocAnnotations$Link;-><init>()V

    iput-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    .line 377
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 381
    :sswitch_7
    const/16 v5, 0x3a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 383
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-nez v5, :cond_4

    move v1, v4

    .line 384
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/DocumentV2$Review;

    .line 386
    .local v2, "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-eqz v1, :cond_3

    .line 387
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 389
    :cond_3
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_5

    .line 390
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Review;-><init>()V

    aput-object v5, v2, v1

    .line 391
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 392
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 389
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 383
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Review;
    :cond_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v1, v5

    goto :goto_1

    .line 395
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/DocumentV2$Review;
    :cond_5
    new-instance v5, Lcom/google/android/finsky/protos/DocumentV2$Review;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/DocumentV2$Review;-><init>()V

    aput-object v5, v2, v1

    .line 396
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 397
    iput-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    goto/16 :goto_0

    .line 337
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 198
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 263
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasTitle:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->title:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 264
    :cond_0
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->title:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 266
    :cond_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    if-eqz v2, :cond_2

    .line 267
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->aggregateSentiment:Lcom/google/android/finsky/protos/Common$Image;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 269
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasTotalNumReviews:Z

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->totalNumReviews:I

    if-eqz v2, :cond_4

    .line 270
    :cond_3
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->totalNumReviews:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 272
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasPercentFavorable:Z

    if-nez v2, :cond_5

    iget v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->percentFavorable:I

    if-eqz v2, :cond_6

    .line 273
    :cond_5
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->percentFavorable:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeUInt32(II)V

    .line 275
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->hasSourceText:Z

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->sourceText:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 276
    :cond_7
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->sourceText:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 278
    :cond_8
    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    if-eqz v2, :cond_9

    .line 279
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->source:Lcom/google/android/finsky/protos/DocAnnotations$Link;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 281
    :cond_9
    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v2, v2

    if-lez v2, :cond_b

    .line 282
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    array-length v2, v2

    if-ge v1, v2, :cond_b

    .line 283
    iget-object v2, p0, Lcom/google/android/finsky/protos/Rev$CriticReviewsResponse;->review:[Lcom/google/android/finsky/protos/DocumentV2$Review;

    aget-object v0, v2, v1

    .line 284
    .local v0, "element":Lcom/google/android/finsky/protos/DocumentV2$Review;
    if-eqz v0, :cond_a

    .line 285
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 282
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 289
    .end local v0    # "element":Lcom/google/android/finsky/protos/DocumentV2$Review;
    .end local v1    # "i":I
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 290
    return-void
.end method
