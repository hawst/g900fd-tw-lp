.class public Lcom/google/android/finsky/adapters/ImageStripAdapter;
.super Ljava/lang/Object;
.source "ImageStripAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/adapters/ImageStripAdapter$OnImageChildViewTapListener;
    }
.end annotation


# instance fields
.field private final mDataSetObservable:Landroid/database/DataSetObservable;

.field private mImageChildTappedListener:Lcom/google/android/finsky/adapters/ImageStripAdapter$OnImageChildViewTapListener;

.field private final mImageCount:I

.field private final mImageDimensions:[Lcom/google/android/finsky/protos/Common$Image$Dimension;

.field private final mImages:[Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(ILcom/google/android/finsky/adapters/ImageStripAdapter$OnImageChildViewTapListener;)V
    .locals 1
    .param p1, "imageCount"    # I
    .param p2, "imageTapListener"    # Lcom/google/android/finsky/adapters/ImageStripAdapter$OnImageChildViewTapListener;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput p1, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageCount:I

    .line 30
    iget v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageCount:I

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImages:[Landroid/graphics/drawable/Drawable;

    .line 31
    iget v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageCount:I

    new-array v0, v0, [Lcom/google/android/finsky/protos/Common$Image$Dimension;

    iput-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageDimensions:[Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .line 32
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    .line 34
    iput-object p2, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageChildTappedListener:Lcom/google/android/finsky/adapters/ImageStripAdapter$OnImageChildViewTapListener;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/adapters/ImageStripAdapter;)Lcom/google/android/finsky/adapters/ImageStripAdapter$OnImageChildViewTapListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/adapters/ImageStripAdapter;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageChildTappedListener:Lcom/google/android/finsky/adapters/ImageStripAdapter$OnImageChildViewTapListener;

    return-object v0
.end method


# virtual methods
.method public getChildCount()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageCount:I

    return v0
.end method

.method public getImageAt(I)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImages:[Landroid/graphics/drawable/Drawable;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getImageDimensionAt(ILcom/google/android/finsky/protos/Common$Image$Dimension;F)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "dimension"    # Lcom/google/android/finsky/protos/Common$Image$Dimension;
    .param p3, "scaleFactor"    # F

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->getImageAt(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 74
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p3

    float-to-int v1, v1

    iput v1, p2, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    .line 76
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p3

    float-to-int v1, v1

    iput v1, p2, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    .line 84
    :goto_0
    iput-boolean v3, p2, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasWidth:Z

    .line 85
    iput-boolean v3, p2, Lcom/google/android/finsky/protos/Common$Image$Dimension;->hasHeight:Z

    .line 86
    return-void

    .line 77
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageDimensions:[Lcom/google/android/finsky/protos/Common$Image$Dimension;

    aget-object v1, v1, p1

    if-eqz v1, :cond_1

    .line 78
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageDimensions:[Lcom/google/android/finsky/protos/Common$Image$Dimension;

    aget-object v1, v1, p1

    iget v1, v1, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    iput v1, p2, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    .line 79
    iget-object v1, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageDimensions:[Lcom/google/android/finsky/protos/Common$Image$Dimension;

    aget-object v1, v1, p1

    iget v1, v1, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    iput v1, p2, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    goto :goto_0

    .line 81
    :cond_1
    iput v2, p2, Lcom/google/android/finsky/protos/Common$Image$Dimension;->width:I

    .line 82
    iput v2, p2, Lcom/google/android/finsky/protos/Common$Image$Dimension;->height:I

    goto :goto_0
.end method

.method public getViewAt(Landroid/content/Context;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "index"    # I

    .prologue
    .line 53
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 54
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040025

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 55
    .local v0, "childView":Landroid/view/View;
    new-instance v2, Lcom/google/android/finsky/adapters/ImageStripAdapter$1;

    invoke-direct {v2, p0, p3}, Lcom/google/android/finsky/adapters/ImageStripAdapter$1;-><init>(Lcom/google/android/finsky/adapters/ImageStripAdapter;I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-object v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 113
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyInvalidated()V

    .line 117
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 97
    return-void
.end method

.method public setImageAt(ILandroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImages:[Landroid/graphics/drawable/Drawable;

    aput-object p2, v0, p1

    .line 49
    invoke-virtual {p0}, Lcom/google/android/finsky/adapters/ImageStripAdapter;->notifyDataSetChanged()V

    .line 50
    return-void
.end method

.method public setImageDimensionAt(ILcom/google/android/finsky/protos/Common$Image$Dimension;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "dim"    # Lcom/google/android/finsky/protos/Common$Image$Dimension;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mImageDimensions:[Lcom/google/android/finsky/protos/Common$Image$Dimension;

    aput-object p2, v0, p1

    .line 45
    return-void
.end method

.method public unregisterAll()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/finsky/adapters/ImageStripAdapter;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->unregisterAll()V

    .line 105
    return-void
.end method
