.class public Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;
.super Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;
.source "ConfirmationStep.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment",
        "<",
        "Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;",
        ">;"
    }
.end annotation


# instance fields
.field private mBillingUiMode:I

.field private mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

.field private mMainView:Landroid/view/View;

.field private mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;-><init>()V

    .line 43
    const/16 v0, 0x373

    invoke-static {v0}, Lcom/google/android/finsky/analytics/FinskyEventLog;->obtainPlayStoreUiElement(I)Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    return-object v0
.end method

.method public static newInstance(Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;IZ)Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;
    .locals 4
    .param p0, "challenge"    # Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;
    .param p1, "mode"    # I
    .param p2, "codeScreenSkipped"    # Z

    .prologue
    .line 59
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 60
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "ConfirmationStep.challenge"

    invoke-static {p0}, Lcom/google/android/finsky/utils/ParcelableProto;->forProto(Lcom/google/protobuf/nano/MessageNano;)Lcom/google/android/finsky/utils/ParcelableProto;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 61
    const-string v2, "ui_mode"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    const-string v2, "ConfirmationStep.code_screen_skipped"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 63
    new-instance v1, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;

    invoke-direct {v1}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;-><init>()V

    .line 65
    .local v1, "result":Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;
    iput-object p0, v1, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    .line 66
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->setArguments(Landroid/os/Bundle;)V

    .line 67
    return-object v1
.end method


# virtual methods
.method public getContinueButtonLabel(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v0, v0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->buttonLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mPlayStoreUiElement:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method public onContinueButtonClicked()V
    .locals 1

    .prologue
    .line 211
    const/16 v0, 0x374

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->logClick(I)V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getMultiStepFragment()Lcom/google/android/finsky/billing/lightpurchase/multistep/MultiStepFragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;

    invoke-virtual {v0}, Lcom/google/android/finsky/billing/giftcard/RedeemCodeFragment;->confirm()V

    .line 213
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ConfirmationStep.challenge"

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/ParcelableProto;->getProtoFromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iput-object v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    .line 74
    invoke-virtual {p0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ui_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mBillingUiMode:I

    .line 75
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 19
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mBillingUiMode:I

    if-nez v14, :cond_2

    const v4, 0x7f040183

    .line 82
    .local v4, "layoutId":I
    :goto_0
    const/4 v14, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v4, v1, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    .line 84
    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mBillingUiMode:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_0

    .line 86
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v14

    const v15, 0x7f0c00a3

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Landroid/support/v4/app/FragmentActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 88
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a009c

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 89
    .local v10, "titleView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->title:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_3

    .line 90
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->title:Ljava/lang/String;

    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a0347

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 95
    .local v9, "titleBylineView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->titleBylineHtml:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_4

    .line 96
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->titleBylineHtml:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 98
    invoke-virtual {v9}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v14

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 103
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a0099

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/play/image/FifeImageView;

    .line 104
    .local v3, "imageView":Lcom/google/android/play/image/FifeImageView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v2, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->thumbnailImage:Lcom/google/android/finsky/protos/Common$Image;

    .line 105
    .local v2, "image":Lcom/google/android/finsky/protos/Common$Image;
    if-eqz v2, :cond_5

    .line 107
    new-instance v14, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep$1;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep$1;-><init>(Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;)V

    invoke-virtual {v3, v14}, Lcom/google/android/play/image/FifeImageView;->setOnLoadedListener(Lcom/google/android/play/image/FifeImageView$OnLoadedListener;)V

    .line 123
    iget-object v14, v2, Lcom/google/android/finsky/protos/Common$Image;->imageUrl:Ljava/lang/String;

    const/4 v15, 0x0

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/finsky/FinskyApp;->getBitmapLoader()Lcom/google/android/play/image/BitmapLoader;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v3, v14, v15, v0}, Lcom/google/android/play/image/FifeImageView;->setImage(Ljava/lang/String;ZLcom/google/android/play/image/BitmapLoader;)V

    .line 128
    :goto_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a0246

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 129
    .local v8, "priceView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->formattedPrice:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_6

    .line 130
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->formattedPrice:Ljava/lang/String;

    invoke-virtual {v8, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a0238

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 135
    .local v7, "priceBylineView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->priceBylineHtml:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_7

    .line 136
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v6, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->priceBylineHtml:Ljava/lang/String;

    .line 137
    .local v6, "priceBylineHtml":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v14

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v14

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 139
    invoke-virtual {v7}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v14

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 143
    const-string v14, "<strike>"

    invoke-virtual {v6, v14}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    const-string v14, "</strike>"

    invoke-virtual {v6, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 145
    invoke-virtual {v7}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v14

    or-int/lit8 v14, v14, 0x10

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 148
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c0125

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 152
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0c0126

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual {v8}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 159
    .end local v6    # "priceBylineHtml":Ljava/lang/String;
    :goto_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a00c3

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 160
    .local v5, "messageView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->messageHtml:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_8

    .line 161
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->messageHtml:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 163
    invoke-virtual {v5}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 168
    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a0348

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 169
    .local v13, "voucherFooterView":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->footerHtml:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 170
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v14, v14, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->footerHtml:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 172
    invoke-virtual {v13}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 178
    :goto_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    const v15, 0x7f0a0129

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 179
    .local v11, "tosFooterView":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getArguments()Landroid/os/Bundle;

    move-result-object v14

    const-string v15, "ConfirmationStep.code_screen_skipped"

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 180
    sget-object v14, Lcom/google/android/finsky/config/G;->redeemTermsAndConditionsUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v14}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-static {v14}, Lcom/google/android/finsky/billing/BillingUtils;->replaceLocale(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 181
    .local v12, "tosString":Ljava/lang/String;
    const v14, 0x7f0c0118

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v12, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 184
    invoke-virtual {v11}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    .line 189
    .end local v12    # "tosString":Ljava/lang/String;
    :goto_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    return-object v14

    .line 80
    .end local v2    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .end local v3    # "imageView":Lcom/google/android/play/image/FifeImageView;
    .end local v4    # "layoutId":I
    .end local v5    # "messageView":Landroid/widget/TextView;
    .end local v7    # "priceBylineView":Landroid/widget/TextView;
    .end local v8    # "priceView":Landroid/widget/TextView;
    .end local v9    # "titleBylineView":Landroid/widget/TextView;
    .end local v10    # "titleView":Landroid/widget/TextView;
    .end local v11    # "tosFooterView":Landroid/widget/TextView;
    .end local v13    # "voucherFooterView":Landroid/widget/TextView;
    :cond_2
    const v4, 0x7f0401a9

    goto/16 :goto_0

    .line 92
    .restart local v4    # "layoutId":I
    .restart local v10    # "titleView":Landroid/widget/TextView;
    :cond_3
    const/16 v14, 0x8

    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 100
    .restart local v9    # "titleBylineView":Landroid/widget/TextView;
    :cond_4
    const/16 v14, 0x8

    invoke-virtual {v9, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 125
    .restart local v2    # "image":Lcom/google/android/finsky/protos/Common$Image;
    .restart local v3    # "imageView":Lcom/google/android/play/image/FifeImageView;
    :cond_5
    const/16 v14, 0x8

    invoke-virtual {v3, v14}, Lcom/google/android/play/image/FifeImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 132
    .restart local v8    # "priceView":Landroid/widget/TextView;
    :cond_6
    const/16 v14, 0x8

    invoke-virtual {v8, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 156
    .restart local v7    # "priceBylineView":Landroid/widget/TextView;
    :cond_7
    const/16 v14, 0x8

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 165
    .restart local v5    # "messageView":Landroid/widget/TextView;
    :cond_8
    const/16 v14, 0x8

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_6

    .line 174
    .restart local v13    # "voucherFooterView":Landroid/widget/TextView;
    :cond_9
    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 186
    .restart local v11    # "tosFooterView":Landroid/widget/TextView;
    :cond_a
    const/16 v14, 0x8

    invoke-virtual {v11, v14}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 194
    invoke-super {p0}, Lcom/google/android/finsky/billing/lightpurchase/multistep/StepFragment;->onResume()V

    .line 198
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 199
    .local v0, "context":Landroid/content/Context;
    iget v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mBillingUiMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const v2, 0x7f0c00a3

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 201
    .local v1, "textToAnnounce":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mMainView:Landroid/view/View;

    invoke-static {v0, v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    .line 202
    return-void

    .line 199
    .end local v1    # "textToAnnounce":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/billing/giftcard/steps/ConfirmationStep;->mChallenge:Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;

    iget-object v1, v2, Lcom/google/android/finsky/protos/PromoCode$UserConfirmationChallenge;->title:Ljava/lang/String;

    goto :goto_0
.end method
