.class public Lcom/google/android/finsky/layout/play/PreferredEmailCard;
.super Lcom/google/android/finsky/layout/IdentifiableFrameLayout;
.source "PreferredEmailCard.java"

# interfaces
.implements Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;


# instance fields
.field private mBody:Landroid/widget/TextView;

.field private mButtonDismiss:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

.field private mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

.field private mButtonSeparator:Landroid/view/View;

.field private final mDefaultTitleTopPadding:I

.field private mGraphic:Lcom/google/android/play/image/FifeImageView;

.field private mGraphicBox:Landroid/view/View;

.field private mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private final mSupportsDynamicTitleTopPadding:Z

.field private mTitle:Landroid/widget/TextView;

.field private mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/PreferredEmailCard;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/layout/IdentifiableFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    sget-object v1, Lcom/android/vending/R$styleable;->WarmWelcomeCard:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 56
    .local v0, "viewAttrs":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mSupportsDynamicTitleTopPadding:Z

    .line 58
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0173

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mDefaultTitleTopPadding:I

    .line 62
    return-void
.end method


# virtual methods
.method public childImpression(Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .prologue
    .line 163
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted children"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getParentNode()Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mParentNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    return-object v0
.end method

.method public getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mUiElementProto:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Lcom/google/android/finsky/layout/IdentifiableFrameLayout;->onFinishInflate()V

    .line 68
    const v0, 0x7f0a0318

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mTitle:Landroid/widget/TextView;

    .line 69
    const v0, 0x7f0a0319

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mBody:Landroid/widget/TextView;

    .line 70
    const v0, 0x7f0a0315

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mGraphicBox:Landroid/view/View;

    .line 71
    iget-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mGraphicBox:Landroid/view/View;

    const v1, 0x7f0a0316

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/image/FifeImageView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mGraphic:Lcom/google/android/play/image/FifeImageView;

    .line 72
    const v0, 0x7f0a03be

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mButtonDismiss:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    .line 73
    const v0, 0x7f0a03bc

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mButtonSecondary:Lcom/google/android/finsky/layout/play/WarmWelcomeCardButton;

    .line 74
    const v0, 0x7f0a03bd

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/PreferredEmailCard;->mButtonSeparator:Landroid/view/View;

    .line 75
    return-void
.end method
