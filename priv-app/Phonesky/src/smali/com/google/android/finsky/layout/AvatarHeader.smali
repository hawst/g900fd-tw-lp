.class public Lcom/google/android/finsky/layout/AvatarHeader;
.super Landroid/view/ViewGroup;
.source "AvatarHeader.java"

# interfaces
.implements Lcom/google/android/finsky/layout/DetailsPeekingSection;
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoBottomSeparator;
.implements Lcom/google/android/finsky/layout/DetailsSectionStack$NoTopSeparator;


# instance fields
.field private mAvatarVerticalPeek:I

.field private mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

.field private mDoc:Lcom/google/android/finsky/api/model/Document;

.field private mHasAvatar:Z

.field private mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

.field private final mNoAvatarTitlePadding:I

.field private mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

.field private mTitle:Lcom/google/android/play/layout/PlayTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/AvatarHeader;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 50
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0b00df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mAvatarVerticalPeek:I

    .line 51
    const v1, 0x7f0b00e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mNoAvatarTitlePadding:I

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/layout/AvatarHeader;)Lcom/google/android/finsky/api/model/Document;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AvatarHeader;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mDoc:Lcom/google/android/finsky/api/model/Document;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/finsky/layout/AvatarHeader;)Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/layout/AvatarHeader;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    return-object v0
.end method


# virtual methods
.method public bind(Lcom/google/android/finsky/navigationmanager/NavigationManager;Lcom/google/android/play/image/BitmapLoader;Lcom/google/android/finsky/api/model/Document;I)V
    .locals 10
    .param p1, "navManager"    # Lcom/google/android/finsky/navigationmanager/NavigationManager;
    .param p2, "bitmapLoader"    # Lcom/google/android/play/image/BitmapLoader;
    .param p3, "doc"    # Lcom/google/android/finsky/api/model/Document;
    .param p4, "avatarImageType"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 88
    iput-object p1, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mNavigationManager:Lcom/google/android/finsky/navigationmanager/NavigationManager;

    .line 89
    iput-object p2, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    .line 90
    iput-object p3, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mDoc:Lcom/google/android/finsky/api/model/Document;

    .line 92
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v5}, Lcom/google/android/finsky/api/model/Document;->getDocumentType()I

    move-result v0

    .line 93
    .local v0, "docType":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvatarHeader;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 96
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mTitle:Lcom/google/android/play/layout/PlayTextView;

    iget-object v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/play/layout/PlayTextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mTitle:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v5, v8}, Lcom/google/android/play/layout/PlayTextView;->setSelected(Z)V

    .line 100
    invoke-virtual {p3, p4}, Lcom/google/android/finsky/api/model/Document;->hasImages(I)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mHasAvatar:Z

    .line 102
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvatarHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f09009c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 103
    .local v4, "whiteColor":I
    iget-boolean v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mHasAvatar:Z

    invoke-virtual {p0, v4, v5}, Lcom/google/android/finsky/layout/AvatarHeader;->updatePeekBackground(IZ)V

    .line 106
    iget-boolean v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mHasAvatar:Z

    if-eqz v5, :cond_0

    .line 107
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v5, v9}, Lcom/google/android/play/layout/PlayCardThumbnail;->setVisibility(I)V

    .line 108
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    iget-object v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-virtual {v6}, Lcom/google/android/finsky/api/model/Document;->getBackend()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->updateCoverPadding(I)V

    .line 109
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 110
    .local v3, "thumbnailLp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvatarHeader;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getRegularDetailsIconWidth(Landroid/content/Context;I)I

    move-result v5

    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 112
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvatarHeader;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v0}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getRegularDetailsIconHeight(Landroid/content/Context;I)I

    move-result v5

    iput v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 114
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v5}, Lcom/google/android/play/layout/PlayCardThumbnail;->getImageView()Landroid/widget/ImageView;

    move-result-object v2

    check-cast v2, Lcom/google/android/finsky/layout/DocImageView;

    .line 115
    .local v2, "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    sget-object v5, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/layout/DocImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 116
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mDoc:Lcom/google/android/finsky/api/model/Document;

    iget-object v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mBitmapLoader:Lcom/google/android/play/image/BitmapLoader;

    new-array v7, v8, [I

    aput p4, v7, v9

    invoke-virtual {v2, v5, v6, v7}, Lcom/google/android/finsky/layout/DocImageView;->bind(Lcom/google/android/finsky/api/model/Document;Lcom/google/android/play/image/BitmapLoader;[I)V

    .line 117
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mDoc:Lcom/google/android/finsky/api/model/Document;

    invoke-static {v5, v1}, Lcom/google/android/finsky/utils/CorpusResourceUtils;->getItemThumbnailContentDescription(Lcom/google/android/finsky/api/model/Document;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/layout/DocImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 119
    invoke-static {v1}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/layout/DocImageView;->setBitmapTransformation(Lcom/google/android/play/image/BitmapTransformation;)V

    .line 120
    invoke-virtual {v2, v8}, Lcom/google/android/finsky/layout/DocImageView;->setFocusable(Z)V

    .line 121
    new-instance v5, Lcom/google/android/finsky/layout/AvatarHeader$1;

    invoke-direct {v5, p0, p4}, Lcom/google/android/finsky/layout/AvatarHeader$1;-><init>(Lcom/google/android/finsky/layout/AvatarHeader;I)V

    invoke-virtual {v2, v5}, Lcom/google/android/finsky/layout/DocImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    .end local v2    # "thumbnailCover":Lcom/google/android/finsky/layout/DocImageView;
    .end local v3    # "thumbnailLp":Landroid/view/ViewGroup$LayoutParams;
    :goto_0
    return-void

    .line 130
    :cond_0
    iget-object v5, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->setVisibility(I)V

    goto :goto_0
.end method

.method public getTopPeekAmount()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mAvatarVerticalPeek:I

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 59
    const v0, 0x7f0a00d5

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AvatarHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardThumbnail;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    .line 60
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AvatarHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mTitle:Lcom/google/android/play/layout/PlayTextView;

    .line 61
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvatarHeader;->getWidth()I

    move-result v6

    div-int/lit8 v0, v6, 0x2

    .line 159
    .local v0, "centerX":I
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvatarHeader;->getPaddingTop()I

    move-result v5

    .line 162
    .local v5, "topY":I
    iget-boolean v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mHasAvatar:Z

    if-eqz v6, :cond_0

    .line 163
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredWidth()I

    move-result v1

    .line 164
    .local v1, "thumbnailFrameWidth":I
    div-int/lit8 v6, v1, 0x2

    sub-int v2, v0, v6

    .line 165
    .local v2, "thumbnailFrameX":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    add-int v7, v2, v1

    iget-object v8, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v8}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {v6, v2, v5, v7, v8}, Lcom/google/android/play/layout/PlayCardThumbnail;->layout(IIII)V

    .line 167
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v5, v6

    .line 173
    .end local v1    # "thumbnailFrameWidth":I
    .end local v2    # "thumbnailFrameX":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mTitle:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v6}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredWidth()I

    move-result v3

    .line 174
    .local v3, "titleWidth":I
    div-int/lit8 v6, v3, 0x2

    sub-int v4, v0, v6

    .line 175
    .local v4, "titleX":I
    iget-object v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mTitle:Lcom/google/android/play/layout/PlayTextView;

    add-int v7, v4, v3

    iget-object v8, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mTitle:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v8}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v5

    invoke-virtual {v6, v4, v5, v7, v8}, Lcom/google/android/play/layout/PlayTextView;->layout(IIII)V

    .line 176
    return-void

    .line 169
    .end local v3    # "titleWidth":I
    .end local v4    # "titleX":I
    :cond_0
    iget v6, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mNoAvatarTitlePadding:I

    add-int/2addr v5, v6

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 136
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 137
    .local v2, "width":I
    const/4 v0, 0x0

    .line 140
    .local v0, "height":I
    iget-boolean v3, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mHasAvatar:Z

    if-eqz v3, :cond_0

    .line 141
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v3}, Lcom/google/android/play/layout/PlayCardThumbnail;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 142
    .local v1, "thumbnailFrameLp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/play/layout/PlayCardThumbnail;->measure(II)V

    .line 150
    .end local v1    # "thumbnailFrameLp":Landroid/view/ViewGroup$LayoutParams;
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mTitle:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v3, v6, v6}, Lcom/google/android/play/layout/PlayTextView;->measure(II)V

    .line 152
    iget-object v3, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mTitle:Lcom/google/android/play/layout/PlayTextView;

    invoke-virtual {v3}, Lcom/google/android/play/layout/PlayTextView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mThumbnailFrame:Lcom/google/android/play/layout/PlayCardThumbnail;

    invoke-virtual {v4}, Lcom/google/android/play/layout/PlayCardThumbnail;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 153
    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvatarHeader;->getPaddingTop()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvatarHeader;->getPaddingBottom()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/finsky/layout/AvatarHeader;->setMeasuredDimension(II)V

    .line 154
    return-void

    .line 146
    :cond_0
    iget v3, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mNoAvatarTitlePadding:I

    add-int/2addr v0, v3

    goto :goto_0
.end method

.method public updatePeekBackground(IZ)V
    .locals 6
    .param p1, "color"    # I
    .param p2, "isPeeking"    # Z

    .prologue
    const/4 v2, 0x0

    .line 72
    if-eqz p2, :cond_0

    .line 76
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    new-instance v1, Landroid/graphics/drawable/PaintDrawable;

    invoke-direct {v1, p1}, Landroid/graphics/drawable/PaintDrawable;-><init>(I)V

    invoke-virtual {p0}, Lcom/google/android/finsky/layout/AvatarHeader;->getTopPeekAmount()I

    move-result v3

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/AvatarHeader;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 83
    :goto_0
    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/google/android/finsky/layout/AvatarHeader;->setPadding(IIII)V

    .line 84
    return-void

    .line 79
    :cond_0
    iput v2, p0, Lcom/google/android/finsky/layout/AvatarHeader;->mAvatarVerticalPeek:I

    .line 80
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/layout/AvatarHeader;->setBackgroundColor(I)V

    goto :goto_0
.end method
