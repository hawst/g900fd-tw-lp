.class public Lcom/google/android/finsky/layout/CheckableLinearLayout;
.super Landroid/widget/LinearLayout;
.source "CheckableLinearLayout.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private mCheckedTextView:Landroid/widget/CheckedTextView;

.field private final mResId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v2, 0x0

    .line 21
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    sget-object v1, Lcom/android/vending/R$styleable;->CheckableLinearLayout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 24
    .local v0, "attributes":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mResId:I

    .line 25
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 26
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mCheckedTextView:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mCheckedTextView:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    iget v0, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mResId:I

    if-eqz v0, :cond_0

    .line 51
    iget v0, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mResId:I

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/CheckableLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iput-object v0, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mCheckedTextView:Landroid/widget/CheckedTextView;

    .line 53
    :cond_0
    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mCheckedTextView:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mCheckedTextView:Landroid/widget/CheckedTextView;

    invoke-virtual {v0, p1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 38
    :cond_0
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mCheckedTextView:Landroid/widget/CheckedTextView;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/google/android/finsky/layout/CheckableLinearLayout;->mCheckedTextView:Landroid/widget/CheckedTextView;

    invoke-virtual {v0}, Landroid/widget/CheckedTextView;->toggle()V

    .line 45
    :cond_0
    return-void
.end method
