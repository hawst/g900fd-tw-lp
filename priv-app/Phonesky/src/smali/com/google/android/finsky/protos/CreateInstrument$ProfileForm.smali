.class public final Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;
.super Lcom/google/protobuf/nano/MessageNano;
.source "CreateInstrument.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/CreateInstrument;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProfileForm"
.end annotation


# instance fields
.field public addressField:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

.field public tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1053
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1054
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->clear()Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    .line 1055
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1058
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->addressField:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

    .line 1059
    iput-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    .line 1060
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->cachedSize:I

    .line 1061
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 3

    .prologue
    .line 1078
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v0

    .line 1079
    .local v0, "size":I
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->addressField:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

    if-eqz v1, :cond_0

    .line 1080
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->addressField:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1083
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    if-eqz v1, :cond_1

    .line 1084
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1087
    :cond_1
    return v0
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;
    .locals 2
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1095
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v0

    .line 1096
    .local v0, "tag":I
    sparse-switch v0, :sswitch_data_0

    .line 1100
    invoke-static {p1, v0}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1101
    :sswitch_0
    return-object p0

    .line 1106
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->addressField:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

    if-nez v1, :cond_1

    .line 1107
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->addressField:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

    .line 1109
    :cond_1
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->addressField:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1113
    :sswitch_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    if-nez v1, :cond_2

    .line 1114
    new-instance v1, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;-><init>()V

    iput-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    .line 1116
    :cond_2
    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1096
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1030
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 2
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1067
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->addressField:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

    if-eqz v0, :cond_0

    .line 1068
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->addressField:Lcom/google/android/finsky/protos/CreateInstrument$AddressFormField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1070
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    if-eqz v0, :cond_1

    .line 1071
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/finsky/protos/CreateInstrument$ProfileForm;->tosField:Lcom/google/android/finsky/protos/CreateInstrument$TosFormField;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1073
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1074
    return-void
.end method
