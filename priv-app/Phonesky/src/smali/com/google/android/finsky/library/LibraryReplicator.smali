.class public Lcom/google/android/finsky/library/LibraryReplicator;
.super Ljava/lang/Object;
.source "LibraryReplicator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;,
        Lcom/google/android/finsky/library/LibraryReplicator$LibraryUpdateListener;,
        Lcom/google/android/finsky/library/LibraryReplicator$Listener;,
        Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;
    }
.end annotation


# static fields
.field private static final RESCHEDULE_REPLICATION_MS:J


# instance fields
.field private final mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

.field private final mBackgroundHandler:Landroid/os/Handler;

.field private mCurrentRequest:Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;

.field private mDebugEvents:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final mDfeApi:Lcom/google/android/finsky/api/DfeApi;

.field private final mEnableDebugging:Z

.field private final mListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/library/LibraryReplicator$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotificationHandler:Landroid/os/Handler;

.field private final mReplicationRequests:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;",
            ">;"
        }
    .end annotation
.end field

.field private final mReplicationRunnable:Ljava/lang/Runnable;

.field private final mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/finsky/config/G;->libraryReplicatorRescheduleDelayMs:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v0}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/google/android/finsky/library/LibraryReplicator;->RESCHEDULE_REPLICATION_MS:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/finsky/api/DfeApi;Lcom/google/android/finsky/library/SQLiteLibrary;Lcom/google/android/finsky/library/AccountLibrary;Landroid/os/Handler;Landroid/os/Handler;Z)V
    .locals 1
    .param p1, "dfeApi"    # Lcom/google/android/finsky/api/DfeApi;
    .param p2, "sqLiteLibrary"    # Lcom/google/android/finsky/library/SQLiteLibrary;
    .param p3, "accountLibrary"    # Lcom/google/android/finsky/library/AccountLibrary;
    .param p4, "notificationHandler"    # Landroid/os/Handler;
    .param p5, "backgroundHandler"    # Landroid/os/Handler;
    .param p6, "enableDebugging"    # Z

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mListeners:Ljava/util/List;

    .line 101
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mReplicationRequests:Ljava/util/List;

    .line 107
    new-instance v0, Lcom/google/android/finsky/library/LibraryReplicator$1;

    invoke-direct {v0, p0}, Lcom/google/android/finsky/library/LibraryReplicator$1;-><init>(Lcom/google/android/finsky/library/LibraryReplicator;)V

    iput-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mReplicationRunnable:Ljava/lang/Runnable;

    .line 138
    iput-object p1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    .line 139
    iput-object p2, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    .line 140
    iput-object p3, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    .line 141
    iput-object p4, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mNotificationHandler:Landroid/os/Handler;

    .line 142
    iput-object p5, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mBackgroundHandler:Landroid/os/Handler;

    .line 143
    iput-boolean p6, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mEnableDebugging:Z

    .line 144
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/library/LibraryReplicator;)Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mCurrentRequest:Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/finsky/library/LibraryReplicator;Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;)Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;
    .param p1, "x1"    # Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mCurrentRequest:Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/finsky/library/LibraryReplicator;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mReplicationRequests:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/finsky/library/LibraryReplicator;Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;
    .param p1, "x1"    # Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/finsky/library/LibraryReplicator;->scheduleRequestAtFront(Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/google/android/finsky/library/LibraryReplicator;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mNotificationHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200()J
    .locals 2

    .prologue
    .line 51
    sget-wide v0, Lcom/google/android/finsky/library/LibraryReplicator;->RESCHEDULE_REPLICATION_MS:J

    return-wide v0
.end method

.method static synthetic access$1300(Lcom/google/android/finsky/library/LibraryReplicator;J)V
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;
    .param p1, "x1"    # J

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/library/LibraryReplicator;->handleNextRequest(J)V

    return-void
.end method

.method static synthetic access$1400(Lcom/google/android/finsky/library/LibraryReplicator;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mBackgroundHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/finsky/library/LibraryReplicator;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mEnableDebugging:Z

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/finsky/library/LibraryReplicator;ILcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/android/volley/VolleyError;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .param p3, "x3"    # Lcom/android/volley/VolleyError;
    .param p4, "x4"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/finsky/library/LibraryReplicator;->recordDebugEvent(ILcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/google/android/finsky/library/LibraryReplicator;Ljava/lang/String;)Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/finsky/library/LibraryReplicator;->buildLibraryState(Ljava/lang/String;)Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/finsky/library/LibraryReplicator;)Lcom/google/android/finsky/api/DfeApi;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDfeApi:Lcom/google/android/finsky/api/DfeApi;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/finsky/library/LibraryReplicator;)Lcom/google/android/finsky/library/SQLiteLibrary;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/finsky/library/LibraryReplicator;Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;
    .param p1, "x1"    # Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/finsky/library/LibraryReplicator;->internalApplyLibraryUpdate(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/google/android/finsky/library/LibraryReplicator;)Lcom/google/android/finsky/library/AccountLibrary;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/finsky/library/LibraryReplicator;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    return-object v0
.end method

.method private buildLibraryState(Ljava/lang/String;)Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;
    .locals 7
    .param p1, "libraryId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 216
    iget-object v3, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/library/AccountLibrary;->getLibrary(Ljava/lang/String;)Lcom/google/android/finsky/library/HashingLibrary;

    move-result-object v0

    .line 217
    .local v0, "library":Lcom/google/android/finsky/library/HashingLibrary;
    new-instance v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;

    invoke-direct {v1}, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;-><init>()V

    .line 219
    .local v1, "result":Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;
    invoke-static {p1}, Lcom/google/android/finsky/library/AccountLibrary;->getBackendFromLibraryId(Ljava/lang/String;)I

    move-result v3

    iput v3, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->corpus:I

    .line 220
    iput-boolean v6, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasCorpus:Z

    .line 221
    iput-object p1, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->libraryId:Ljava/lang/String;

    .line 222
    iput-boolean v6, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibraryId:Z

    .line 223
    invoke-virtual {v0}, Lcom/google/android/finsky/library/HashingLibrary;->getHash()J

    move-result-wide v4

    iput-wide v4, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hashCodeSum:J

    .line 224
    iput-boolean v6, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasHashCodeSum:Z

    .line 225
    invoke-virtual {v0}, Lcom/google/android/finsky/library/HashingLibrary;->size()I

    move-result v3

    iput v3, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->librarySize:I

    .line 226
    iput-boolean v6, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasLibrarySize:Z

    .line 227
    iget-object v3, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v3, p1}, Lcom/google/android/finsky/library/AccountLibrary;->getServerToken(Ljava/lang/String;)[B

    move-result-object v2

    .line 228
    .local v2, "token":[B
    if-eqz v2, :cond_0

    .line 229
    iput-object v2, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->serverToken:[B

    .line 230
    iput-boolean v6, v1, Lcom/google/android/finsky/protos/LibraryUpdateProto$ClientLibraryState;->hasServerToken:Z

    .line 232
    :cond_0
    return-object v1
.end method

.method private checkOnBackgroundHandler()V
    .locals 2

    .prologue
    .line 347
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mBackgroundHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 348
    const-string v0, "This method must be called from the background handler."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 350
    :cond_0
    return-void
.end method

.method private createLibraryEntry(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;Ljava/lang/String;)Lcom/google/android/finsky/library/LibraryEntry;
    .locals 54
    .param p1, "mutation"    # Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    .param p2, "libraryId"    # Ljava/lang/String;

    .prologue
    .line 353
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v3}, Lcom/google/android/finsky/library/AccountLibrary;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v4, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 354
    .local v4, "accountName":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget-object v5, v3, Lcom/google/android/finsky/protos/Common$Docid;->backendDocid:Ljava/lang/String;

    .line 355
    .local v5, "docId":Ljava/lang/String;
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v0, v3, Lcom/google/android/finsky/protos/Common$Docid;->backend:I

    move/from16 v17, v0

    .line 356
    .local v17, "backend":I
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->docid:Lcom/google/android/finsky/protos/Common$Docid;

    iget v0, v3, Lcom/google/android/finsky/protos/Common$Docid;->type:I

    move/from16 v46, v0

    .line 357
    .local v46, "docType":I
    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->offerType:I

    .line 358
    .local v6, "offerType":I
    move-object/from16 v0, p1

    iget-wide v7, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->documentHash:J

    .line 359
    .local v7, "documentHash":J
    const/16 v39, 0x0

    .line 360
    .local v39, "validUntilTimestampMs":Ljava/lang/Long;
    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->hasValidUntilTimestampMsec:Z

    if-eqz v3, :cond_0

    .line 361
    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->validUntilTimestampMsec:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v39

    .line 363
    :cond_0
    const-string v3, "u-wl"

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->deleted:Z

    if-nez v3, :cond_8

    .line 364
    const/4 v3, 0x1

    move/from16 v0, v46

    if-ne v0, v3, :cond_3

    .line 365
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->appDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;

    .line 366
    .local v2, "appDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;
    iget-object v9, v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->certificateHash:[Ljava/lang/String;

    .line 367
    .local v9, "certificateHashes":[Ljava/lang/String;
    const-wide/16 v10, 0x0

    .line 368
    .local v10, "refundPreDeliveryEndtimeMs":J
    iget-boolean v3, v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasRefundTimeoutTimestampMsec:Z

    if-eqz v3, :cond_1

    .line 369
    iget-wide v10, v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->refundTimeoutTimestampMsec:J

    .line 371
    :cond_1
    const-wide/16 v12, 0x0

    .line 372
    .local v12, "refundPostDeliveryWindowMs":J
    iget-boolean v3, v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->hasPostDeliveryRefundWindowMsec:Z

    if-eqz v3, :cond_2

    .line 373
    iget-wide v12, v2, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;->postDeliveryRefundWindowMsec:J

    .line 375
    :cond_2
    new-instance v3, Lcom/google/android/finsky/library/LibraryAppEntry;

    invoke-direct/range {v3 .. v13}, Lcom/google/android/finsky/library/LibraryAppEntry;-><init>(Ljava/lang/String;Ljava/lang/String;IJ[Ljava/lang/String;JJ)V

    move-object/from16 v41, v3

    .line 416
    .end local v2    # "appDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryAppDetails;
    .end local v9    # "certificateHashes":[Ljava/lang/String;
    .end local v10    # "refundPreDeliveryEndtimeMs":J
    .end local v12    # "refundPostDeliveryWindowMs":J
    .end local v46    # "docType":I
    :goto_0
    return-object v41

    .line 377
    .restart local v46    # "docType":I
    :cond_3
    const/16 v3, 0xf

    move/from16 v0, v46

    if-ne v0, v3, :cond_7

    .line 378
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->subscriptionDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;

    move-object/from16 v53, v0

    .line 379
    .local v53, "subscriptionDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;
    move-object/from16 v0, v53

    iget-wide v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->initiationTimestampMsec:J

    move-wide/from16 v24, v0

    .line 382
    .local v24, "initiationTimeStampMs":J
    if-nez v39, :cond_4

    .line 383
    move-object/from16 v0, v53

    iget-wide v14, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->deprecatedValidUntilTimestampMsec:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v39

    .line 386
    :cond_4
    move-object/from16 v0, v53

    iget-boolean v3, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->hasTrialUntilTimestampMsec:Z

    if-eqz v3, :cond_5

    move-object/from16 v0, v53

    iget-wide v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->trialUntilTimestampMsec:J

    move-wide/from16 v26, v0

    .line 388
    .local v26, "trialUntilTimestampMs":J
    :goto_1
    move-object/from16 v0, v53

    iget-boolean v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->autoRenewing:Z

    move/from16 v28, v0

    .line 389
    .local v28, "isAutoRenewing":Z
    const/4 v3, 0x3

    move/from16 v0, v17

    if-ne v0, v3, :cond_6

    .line 391
    move-object/from16 v0, v53

    iget-object v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signedPurchaseData:Ljava/lang/String;

    move-object/from16 v29, v0

    .line 392
    .local v29, "signedPurchaseData":Ljava/lang/String;
    move-object/from16 v0, v53

    iget-object v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;->signature:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 394
    .local v30, "signature":Ljava/lang/String;
    new-instance v14, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    move-object v15, v4

    move-object/from16 v16, p2

    move-object/from16 v18, v5

    move/from16 v19, v6

    move-wide/from16 v20, v7

    invoke-direct/range {v14 .. v30}, Lcom/google/android/finsky/library/LibraryInAppSubscriptionEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJJJJZLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v41, v14

    goto :goto_0

    .line 386
    .end local v26    # "trialUntilTimestampMs":J
    .end local v28    # "isAutoRenewing":Z
    .end local v29    # "signedPurchaseData":Ljava/lang/String;
    .end local v30    # "signature":Ljava/lang/String;
    :cond_5
    const-wide/16 v26, 0x0

    goto :goto_1

    .line 399
    .restart local v26    # "trialUntilTimestampMs":J
    .restart local v28    # "isAutoRenewing":Z
    :cond_6
    new-instance v31, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;

    move-object/from16 v32, v4

    move-object/from16 v33, p2

    move/from16 v34, v17

    move-object/from16 v35, v5

    move/from16 v36, v6

    move-wide/from16 v37, v7

    move-wide/from16 v40, v24

    move-wide/from16 v42, v26

    move/from16 v44, v28

    invoke-direct/range {v31 .. v44}, Lcom/google/android/finsky/library/LibrarySubscriptionEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IJLjava/lang/Long;JJZ)V

    move-object/from16 v41, v31

    goto :goto_0

    .line 403
    .end local v24    # "initiationTimeStampMs":J
    .end local v26    # "trialUntilTimestampMs":J
    .end local v28    # "isAutoRenewing":Z
    .end local v53    # "subscriptionDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibrarySubscriptionDetails;
    :cond_7
    const/16 v3, 0xb

    move/from16 v0, v46

    if-ne v0, v3, :cond_8

    .line 405
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    if-eqz v3, :cond_8

    .line 406
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->inAppDetails:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;

    move-object/from16 v52, v0

    .line 407
    .local v52, "inAppDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;
    move-object/from16 v0, v52

    iget-object v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signedPurchaseData:Ljava/lang/String;

    move-object/from16 v29, v0

    .line 408
    .restart local v29    # "signedPurchaseData":Ljava/lang/String;
    move-object/from16 v0, v52

    iget-object v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;->signature:Ljava/lang/String;

    move-object/from16 v30, v0

    .line 409
    .restart local v30    # "signature":Ljava/lang/String;
    new-instance v41, Lcom/google/android/finsky/library/LibraryInAppEntry;

    move-object/from16 v42, v4

    move-object/from16 v43, p2

    move-object/from16 v44, v5

    move/from16 v45, v6

    move-object/from16 v46, v29

    move-object/from16 v47, v30

    move-wide/from16 v48, v7

    invoke-direct/range {v41 .. v49}, Lcom/google/android/finsky/library/LibraryInAppEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    .line 414
    .end local v29    # "signedPurchaseData":Ljava/lang/String;
    .end local v30    # "signature":Ljava/lang/String;
    .end local v52    # "inAppDetails":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryInAppDetails;
    :cond_8
    if-eqz v39, :cond_9

    invoke-virtual/range {v39 .. v39}, Ljava/lang/Long;->longValue()J

    move-result-wide v50

    .line 416
    .local v50, "safeValidUntilTimestampMs":J
    :goto_2
    new-instance v41, Lcom/google/android/finsky/library/LibraryEntry;

    move-object/from16 v42, v4

    move-object/from16 v43, p2

    move/from16 v44, v17

    move-object/from16 v45, v5

    move/from16 v47, v6

    move-wide/from16 v48, v7

    invoke-direct/range {v41 .. v51}, Lcom/google/android/finsky/library/LibraryEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IIJJ)V

    goto/16 :goto_0

    .line 414
    .end local v50    # "safeValidUntilTimestampMs":J
    :cond_9
    const-wide v50, 0x7fffffffffffffffL

    goto :goto_2
.end method

.method private getSupportedLibraries([Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1, "libraryIds"    # [Ljava/lang/String;

    .prologue
    .line 178
    const/4 v6, 0x0

    .line 179
    .local v6, "unsupportedLibraries":I
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 180
    .local v3, "libraryId":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v7, v3}, Lcom/google/android/finsky/library/AccountLibrary;->supportsLibrary(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 181
    add-int/lit8 v6, v6, 0x1

    .line 179
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 186
    .end local v3    # "libraryId":Ljava/lang/String;
    :cond_1
    array-length v7, p1

    if-ne v6, v7, :cond_3

    .line 187
    const/4 v4, 0x0

    .line 200
    :cond_2
    :goto_1
    return-object v4

    .line 188
    :cond_3
    if-lez v6, :cond_5

    .line 189
    array-length v7, p1

    sub-int/2addr v7, v6

    new-array v4, v7, [Ljava/lang/String;

    .line 190
    .local v4, "result":[Ljava/lang/String;
    const/4 v5, 0x0

    .line 191
    .local v5, "supportedLibraryIndex":I
    move-object v0, p1

    array-length v2, v0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 192
    .restart local v3    # "libraryId":Ljava/lang/String;
    iget-object v7, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v7, v3}, Lcom/google/android/finsky/library/AccountLibrary;->supportsLibrary(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 193
    aput-object v3, v4, v5

    .line 194
    add-int/lit8 v5, v5, 0x1

    .line 191
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 198
    .end local v3    # "libraryId":Ljava/lang/String;
    .end local v4    # "result":[Ljava/lang/String;
    .end local v5    # "supportedLibraryIndex":I
    :cond_5
    move-object v4, p1

    .restart local v4    # "result":[Ljava/lang/String;
    goto :goto_1
.end method

.method private declared-synchronized handleNextRequest(J)V
    .locals 3
    .param p1, "delayMs"    # J

    .prologue
    .line 211
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mReplicationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mBackgroundHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mReplicationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    monitor-exit p0

    return-void

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private internalApplyLibraryUpdate(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)Z
    .locals 18
    .param p1, "libraryUpdate"    # Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .param p2, "debugTag"    # Ljava/lang/String;

    .prologue
    .line 262
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mEnableDebugging:Z

    if-eqz v14, :cond_0

    .line 263
    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v14, v1, v15, v2}, Lcom/google/android/finsky/library/LibraryReplicator;->recordDebugEvent(ILcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    .line 265
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/finsky/library/LibraryReplicator;->checkOnBackgroundHandler()V

    .line 268
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->libraryId:Ljava/lang/String;

    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 269
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->libraryId:Ljava/lang/String;

    .line 274
    .local v10, "libraryId":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v14, v10}, Lcom/google/android/finsky/library/AccountLibrary;->supportsLibrary(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_2

    .line 275
    const-string v14, "Ignoring library update for unsupported library %s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v10, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    const/4 v14, 0x5

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p2

    invoke-direct {v0, v14, v15, v1, v2}, Lcom/google/android/finsky/library/LibraryReplicator;->recordDebugEvent(ILcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    .line 277
    const/4 v14, 0x0

    .line 332
    :goto_1
    return v14

    .line 271
    .end local v10    # "libraryId":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->corpus:I

    invoke-static {v14}, Lcom/google/android/finsky/library/AccountLibrary;->getLibraryIdFromBackend(I)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "libraryId":Ljava/lang/String;
    goto :goto_0

    .line 279
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v14}, Lcom/google/android/finsky/library/AccountLibrary;->disableListeners()V

    .line 281
    :try_start_0
    move-object/from16 v0, p1

    iget v14, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->status:I

    packed-switch v14, :pswitch_data_0

    .line 313
    const-string v14, "Unknown LibraryUpdate.status: libraryId=%s, status=%d"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v10, v15, v16

    const/16 v16, 0x1

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->status:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->wtf(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    :cond_3
    :goto_2
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->serverToken:[B

    array-length v14, v14

    if-lez v14, :cond_4

    .line 317
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->serverToken:[B

    .line 318
    .local v12, "token":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v14, v10, v12}, Lcom/google/android/finsky/library/AccountLibrary;->setServerToken(Ljava/lang/String;[B)V

    .line 319
    invoke-static {v10}, Lcom/google/android/finsky/utils/FinskyPreferences;->getLibraryServerToken(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v15}, Lcom/google/android/finsky/library/AccountLibrary;->getAccount()Landroid/accounts/Account;

    move-result-object v15

    iget-object v15, v15, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v14, v15}, Lcom/google/android/finsky/config/PreferenceFile$PrefixSharedPreference;->get(Ljava/lang/String;)Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;

    move-result-object v13

    .line 322
    .local v13, "tokenPref":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/String;>;"
    const/4 v14, 0x0

    invoke-static {v12, v14}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    .line 323
    .local v5, "encodedToken":Ljava/lang/String;
    invoke-virtual {v13, v5}, Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;->put(Ljava/lang/Object;)V

    .line 324
    sget-boolean v14, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v14, :cond_4

    .line 325
    const-string v14, "Updated server token: libraryId=%s serverToken=%s"

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v10, v15, v16

    const/16 v16, 0x1

    aput-object v5, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    .end local v5    # "encodedToken":Ljava/lang/String;
    .end local v12    # "token":[B
    .end local v13    # "tokenPref":Lcom/google/android/finsky/config/PreferenceFile$SharedPreference;, "Lcom/google/android/finsky/config/PreferenceFile$SharedPreference<Ljava/lang/String;>;"
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v14}, Lcom/google/android/finsky/library/AccountLibrary;->enableListeners()V

    .line 332
    move-object/from16 v0, p1

    iget-boolean v14, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->hasMore:Z

    goto :goto_1

    .line 283
    :pswitch_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v15}, Lcom/google/android/finsky/library/AccountLibrary;->getAccount()Landroid/accounts/Account;

    move-result-object v15

    invoke-virtual {v14, v15, v10}, Lcom/google/android/finsky/library/SQLiteLibrary;->resetAccountLibrary(Landroid/accounts/Account;Ljava/lang/String;)V

    .line 284
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v14, v10}, Lcom/google/android/finsky/library/AccountLibrary;->resetLibrary(Ljava/lang/String;)V

    .line 288
    :pswitch_1
    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    array-length v14, v14

    invoke-static {v14}, Lcom/google/android/finsky/utils/Lists;->newArrayList(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 290
    .local v3, "additions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryEntry;>;"
    const/4 v6, 0x0

    .line 291
    .local v6, "hasRemovals":Z
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;->mutation:[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;

    .local v4, "arr$":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    array-length v8, v4

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_3
    if-ge v7, v8, :cond_6

    aget-object v11, v4, v7

    .line 292
    .local v11, "mutation":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v10}, Lcom/google/android/finsky/library/LibraryReplicator;->createLibraryEntry(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;Ljava/lang/String;)Lcom/google/android/finsky/library/LibraryEntry;

    move-result-object v9

    .line 293
    .local v9, "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    iget-boolean v14, v11, Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;->deleted:Z

    if-eqz v14, :cond_5

    .line 294
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v14, v9}, Lcom/google/android/finsky/library/AccountLibrary;->remove(Lcom/google/android/finsky/library/LibraryEntry;)V

    .line 295
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    invoke-virtual {v14, v9}, Lcom/google/android/finsky/library/SQLiteLibrary;->remove(Lcom/google/android/finsky/library/LibraryEntry;)V

    .line 296
    const/4 v6, 0x1

    .line 291
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 298
    :cond_5
    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 330
    .end local v3    # "additions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryEntry;>;"
    .end local v4    # "arr$":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    .end local v6    # "hasRemovals":Z
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    .end local v9    # "libraryEntry":Lcom/google/android/finsky/library/LibraryEntry;
    .end local v11    # "mutation":Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    :catchall_0
    move-exception v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v15}, Lcom/google/android/finsky/library/AccountLibrary;->enableListeners()V

    throw v14

    .line 301
    .restart local v3    # "additions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryEntry;>;"
    .restart local v4    # "arr$":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    .restart local v6    # "hasRemovals":Z
    .restart local v7    # "i$":I
    .restart local v8    # "len$":I
    :cond_6
    :try_start_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v14, v3}, Lcom/google/android/finsky/library/AccountLibrary;->addAll(Ljava/util/Collection;)V

    .line 302
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mSQLiteLibrary:Lcom/google/android/finsky/library/SQLiteLibrary;

    invoke-virtual {v14, v3}, Lcom/google/android/finsky/library/SQLiteLibrary;->addAll(Ljava/util/Collection;)V

    .line 303
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v14

    if-eqz v14, :cond_7

    if-eqz v6, :cond_3

    .line 304
    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/google/android/finsky/library/LibraryReplicator;->notifyMutationListeners(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 308
    .end local v3    # "additions":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/finsky/library/LibraryEntry;>;"
    .end local v4    # "arr$":[Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryMutation;
    .end local v6    # "hasRemovals":Z
    .end local v7    # "i$":I
    .end local v8    # "len$":I
    :pswitch_2
    sget-boolean v14, Lcom/google/android/finsky/utils/FinskyLog;->DEBUG:Z

    if-eqz v14, :cond_8

    .line 309
    const-string v14, "NOT_MODIFIED received for libraryId=%s"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v10, v15, v16

    invoke-static {v14, v15}, Lcom/google/android/finsky/utils/FinskyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 311
    :cond_8
    const/4 v14, 0x0

    .line 330
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v15}, Lcom/google/android/finsky/library/AccountLibrary;->enableListeners()V

    goto/16 :goto_1

    .line 281
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private declared-synchronized notifyMutationListeners(Ljava/lang/String;)V
    .locals 4
    .param p1, "libraryId"    # Ljava/lang/String;

    .prologue
    .line 336
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mListeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/finsky/library/LibraryReplicator$Listener;

    .line 337
    .local v1, "listener":Lcom/google/android/finsky/library/LibraryReplicator$Listener;
    iget-object v2, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mNotificationHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/finsky/library/LibraryReplicator$3;

    invoke-direct {v3, p0, v1, p1}, Lcom/google/android/finsky/library/LibraryReplicator$3;-><init>(Lcom/google/android/finsky/library/LibraryReplicator;Lcom/google/android/finsky/library/LibraryReplicator$Listener;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 336
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "listener":Lcom/google/android/finsky/library/LibraryReplicator$Listener;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 344
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized recordDebugEvent(ILcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/android/volley/VolleyError;Ljava/lang/String;)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "libraryUpdate"    # Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .param p3, "volleyError"    # Lcom/android/volley/VolleyError;
    .param p4, "tag"    # Ljava/lang/String;

    .prologue
    .line 526
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDebugEvents:Ljava/util/Queue;

    if-nez v1, :cond_0

    .line 527
    invoke-static {}, Lcom/google/android/finsky/utils/Lists;->newLinkedList()Ljava/util/LinkedList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDebugEvents:Ljava/util/Queue;

    .line 529
    :cond_0
    new-instance v0, Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;-><init>(Lcom/google/android/finsky/library/LibraryReplicator$1;)V

    .line 530
    .local v0, "debugEvent":Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    # setter for: Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->timestampMs:J
    invoke-static {v0, v2, v3}, Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->access$1602(Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;J)J

    .line 531
    # setter for: Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->type:I
    invoke-static {v0, p1}, Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->access$1702(Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;I)I

    .line 532
    # setter for: Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->tag:Ljava/lang/String;
    invoke-static {v0, p4}, Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->access$1802(Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;Ljava/lang/String;)Ljava/lang/String;

    .line 533
    # setter for: Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->libraryUpdate:Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    invoke-static {v0, p2}, Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->access$1902(Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;)Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;

    .line 534
    # setter for: Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->volleyError:Lcom/android/volley/VolleyError;
    invoke-static {v0, p3}, Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->access$2002(Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;Lcom/android/volley/VolleyError;)Lcom/android/volley/VolleyError;

    .line 535
    iget-object v1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDebugEvents:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 536
    iget-object v1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDebugEvents:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    const/16 v2, 0x64

    if-le v1, v2, :cond_1

    .line 537
    iget-object v1, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDebugEvents:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 539
    :cond_1
    monitor-exit p0

    return-void

    .line 526
    .end local v0    # "debugEvent":Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized scheduleRequestAtFront(Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mEnableDebugging:Z

    if-eqz v0, :cond_0

    .line 205
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;->debugTag:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/finsky/library/LibraryReplicator;->recordDebugEvent(ILcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mReplicationRequests:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    monitor-exit p0

    return-void

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public declared-synchronized addListener(Lcom/google/android/finsky/library/LibraryReplicator$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/finsky/library/LibraryReplicator$Listener;

    .prologue
    .line 147
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    monitor-exit p0

    return-void

    .line 147
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public applyLibraryUpdate(Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)V
    .locals 2
    .param p1, "libraryUpdate"    # Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;
    .param p2, "debugTag"    # Ljava/lang/String;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mBackgroundHandler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/finsky/library/LibraryReplicator$2;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/finsky/library/LibraryReplicator$2;-><init>(Lcom/google/android/finsky/library/LibraryReplicator;Lcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 247
    return-void
.end method

.method public dumpState(Ljava/lang/String;)V
    .locals 6
    .param p1, "indent"    # Ljava/lang/String;

    .prologue
    .line 545
    iget-object v3, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mAccountLibrary:Lcom/google/android/finsky/library/AccountLibrary;

    invoke-virtual {v3}, Lcom/google/android/finsky/library/AccountLibrary;->getAccount()Landroid/accounts/Account;

    move-result-object v3

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/finsky/utils/FinskyLog;->scrubPii(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 546
    .local v2, "scrubbedAccount":Ljava/lang/String;
    const-string v3, "FinskyLibrary"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "LibraryReplicator (account="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") {"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    iget-object v3, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDebugEvents:Ljava/util/Queue;

    if-eqz v3, :cond_0

    .line 548
    const-string v3, "FinskyLibrary"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  eventsCount="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDebugEvents:Ljava/util/Queue;

    invoke-interface {v5}, Ljava/util/Queue;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    iget-object v3, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mDebugEvents:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;

    .line 550
    .local v0, "debugEvent":Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;
    invoke-virtual {v0, p1}, Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;->dumpState(Ljava/lang/String;)V

    goto :goto_0

    .line 553
    .end local v0    # "debugEvent":Lcom/google/android/finsky/library/LibraryReplicator$DebugEvent;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    const-string v3, "FinskyLibrary"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  eventsCount=0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 555
    :cond_1
    const-string v3, "FinskyLibrary"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "} (account="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    return-void
.end method

.method public declared-synchronized replicate([Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;)V
    .locals 3
    .param p1, "libraryIds"    # [Ljava/lang/String;
    .param p2, "finishRunnable"    # Ljava/lang/Runnable;
    .param p3, "debugTag"    # Ljava/lang/String;

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mEnableDebugging:Z

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " libraryIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 161
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2, p3}, Lcom/google/android/finsky/library/LibraryReplicator;->recordDebugEvent(ILcom/google/android/finsky/protos/LibraryUpdateProto$LibraryUpdate;Lcom/android/volley/VolleyError;Ljava/lang/String;)V

    .line 163
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/finsky/library/LibraryReplicator;->getSupportedLibraries([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 164
    if-nez p1, :cond_1

    .line 165
    const-string v0, "Skipping replication request since all libraries are unsupported."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mNotificationHandler:Landroid/os/Handler;

    invoke-virtual {v0, p2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :goto_0
    monitor-exit p0

    return-void

    .line 169
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/finsky/library/LibraryReplicator;->mReplicationRequests:Ljava/util/List;

    new-instance v1, Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/finsky/library/LibraryReplicator$ReplicationRequest;-><init>([Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/library/LibraryReplicator;->handleNextRequest(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
