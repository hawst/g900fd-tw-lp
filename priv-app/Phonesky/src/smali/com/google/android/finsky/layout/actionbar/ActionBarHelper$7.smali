.class Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$7;
.super Landroid/view/animation/AlphaAnimation;
.source "ActionBarHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->exitCurrentActionBarMode(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

.field final synthetic val$originalActionBarAlpha:I


# direct methods
.method constructor <init>(Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;FFI)V
    .locals 0
    .param p2, "x0"    # F
    .param p3, "x1"    # F

    .prologue
    .line 692
    iput-object p1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$7;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    iput p4, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$7;->val$originalActionBarAlpha:I

    invoke-direct {p0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    return-void
.end method


# virtual methods
.method protected applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3
    .param p1, "interpolatedTime"    # F
    .param p2, "t"    # Landroid/view/animation/Transformation;

    .prologue
    .line 695
    iget v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$7;->val$originalActionBarAlpha:I

    rsub-int v1, v1, 0xff

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    rsub-int v0, v1, 0xff

    .line 698
    .local v0, "currAlphaValue":I
    iget-object v1, p0, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper$7;->this$0:Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/finsky/layout/actionbar/ActionBarHelper;->setActionBarAlpha(IZ)V

    .line 699
    return-void
.end method
