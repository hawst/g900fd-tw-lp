.class public final Lcom/google/android/finsky/protos/DeviceConfigurationProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "DeviceConfigurationProto.java"


# instance fields
.field public glEsVersion:I

.field public glExtension:[Ljava/lang/String;

.field public hasFiveWayNavigation:Z

.field public hasGlEsVersion:Z

.field public hasHardKeyboard:Z

.field public hasHasFiveWayNavigation:Z

.field public hasHasHardKeyboard:Z

.field public hasKeyboard:Z

.field public hasMaxApkDownloadSizeMb:Z

.field public hasNavigation:Z

.field public hasScreenDensity:Z

.field public hasScreenHeight:Z

.field public hasScreenLayout:Z

.field public hasScreenWidth:Z

.field public hasTouchScreen:Z

.field public keyboard:I

.field public maxApkDownloadSizeMb:I

.field public nativePlatform:[Ljava/lang/String;

.field public navigation:I

.field public screenDensity:I

.field public screenHeight:I

.field public screenLayout:I

.field public screenWidth:I

.field public systemAvailableFeature:[Ljava/lang/String;

.field public systemSharedLibrary:[Ljava/lang/String;

.field public systemSupportedLocale:[Ljava/lang/String;

.field public touchScreen:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->clear()Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    .line 110
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    iput v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->touchScreen:I

    .line 114
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasTouchScreen:Z

    .line 115
    iput v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->keyboard:I

    .line 116
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasKeyboard:Z

    .line 117
    iput v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->navigation:I

    .line 118
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasNavigation:Z

    .line 119
    iput v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenLayout:I

    .line 120
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenLayout:Z

    .line 121
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHardKeyboard:Z

    .line 122
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasHardKeyboard:Z

    .line 123
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasFiveWayNavigation:Z

    .line 124
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasFiveWayNavigation:Z

    .line 125
    iput v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenDensity:I

    .line 126
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenDensity:Z

    .line 127
    iput v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenWidth:I

    .line 128
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenWidth:Z

    .line 129
    iput v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenHeight:I

    .line 130
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenHeight:Z

    .line 131
    iput v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glEsVersion:I

    .line 132
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasGlEsVersion:Z

    .line 133
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    .line 134
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    .line 135
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    .line 136
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    .line 137
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_STRING_ARRAY:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    .line 138
    const/16 v0, 0x32

    iput v0, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->maxApkDownloadSizeMb:I

    .line 139
    iput-boolean v1, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasMaxApkDownloadSizeMb:Z

    .line 140
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->cachedSize:I

    .line 141
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 7

    .prologue
    .line 225
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v4

    .line 226
    .local v4, "size":I
    iget v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->touchScreen:I

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasTouchScreen:Z

    if-eqz v5, :cond_1

    .line 227
    :cond_0
    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->touchScreen:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 230
    :cond_1
    iget v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->keyboard:I

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasKeyboard:Z

    if-eqz v5, :cond_3

    .line 231
    :cond_2
    const/4 v5, 0x2

    iget v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->keyboard:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 234
    :cond_3
    iget v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->navigation:I

    if-nez v5, :cond_4

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasNavigation:Z

    if-eqz v5, :cond_5

    .line 235
    :cond_4
    const/4 v5, 0x3

    iget v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->navigation:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 238
    :cond_5
    iget v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenLayout:I

    if-nez v5, :cond_6

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenLayout:Z

    if-eqz v5, :cond_7

    .line 239
    :cond_6
    const/4 v5, 0x4

    iget v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenLayout:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 242
    :cond_7
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasHardKeyboard:Z

    if-nez v5, :cond_8

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHardKeyboard:Z

    if-eqz v5, :cond_9

    .line 243
    :cond_8
    const/4 v5, 0x5

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHardKeyboard:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 246
    :cond_9
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasFiveWayNavigation:Z

    if-nez v5, :cond_a

    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasFiveWayNavigation:Z

    if-eqz v5, :cond_b

    .line 247
    :cond_a
    const/4 v5, 0x6

    iget-boolean v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasFiveWayNavigation:Z

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBoolSize(IZ)I

    move-result v5

    add-int/2addr v4, v5

    .line 250
    :cond_b
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenDensity:Z

    if-nez v5, :cond_c

    iget v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenDensity:I

    if-eqz v5, :cond_d

    .line 251
    :cond_c
    const/4 v5, 0x7

    iget v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenDensity:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 254
    :cond_d
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasGlEsVersion:Z

    if-nez v5, :cond_e

    iget v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glEsVersion:I

    if-eqz v5, :cond_f

    .line 255
    :cond_e
    const/16 v5, 0x8

    iget v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glEsVersion:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 258
    :cond_f
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    if-eqz v5, :cond_12

    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_12

    .line 259
    const/4 v0, 0x0

    .line 260
    .local v0, "dataCount":I
    const/4 v1, 0x0

    .line 261
    .local v1, "dataSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_11

    .line 262
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 263
    .local v2, "element":Ljava/lang/String;
    if-eqz v2, :cond_10

    .line 264
    add-int/lit8 v0, v0, 0x1

    .line 265
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 261
    :cond_10
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 269
    .end local v2    # "element":Ljava/lang/String;
    :cond_11
    add-int/2addr v4, v1

    .line 270
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 272
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_12
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    if-eqz v5, :cond_15

    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_15

    .line 273
    const/4 v0, 0x0

    .line 274
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 275
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_14

    .line 276
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 277
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_13

    .line 278
    add-int/lit8 v0, v0, 0x1

    .line 279
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 275
    :cond_13
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 283
    .end local v2    # "element":Ljava/lang/String;
    :cond_14
    add-int/2addr v4, v1

    .line 284
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 286
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_15
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    if-eqz v5, :cond_18

    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_18

    .line 287
    const/4 v0, 0x0

    .line 288
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 289
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_17

    .line 290
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 291
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_16

    .line 292
    add-int/lit8 v0, v0, 0x1

    .line 293
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 289
    :cond_16
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 297
    .end local v2    # "element":Ljava/lang/String;
    :cond_17
    add-int/2addr v4, v1

    .line 298
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 300
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_18
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenWidth:Z

    if-nez v5, :cond_19

    iget v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenWidth:I

    if-eqz v5, :cond_1a

    .line 301
    :cond_19
    const/16 v5, 0xc

    iget v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenWidth:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 304
    :cond_1a
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenHeight:Z

    if-nez v5, :cond_1b

    iget v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenHeight:I

    if-eqz v5, :cond_1c

    .line 305
    :cond_1b
    const/16 v5, 0xd

    iget v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenHeight:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 308
    :cond_1c
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    if-eqz v5, :cond_1f

    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_1f

    .line 309
    const/4 v0, 0x0

    .line 310
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 311
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_3
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_1e

    .line 312
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 313
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_1d

    .line 314
    add-int/lit8 v0, v0, 0x1

    .line 315
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 311
    :cond_1d
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 319
    .end local v2    # "element":Ljava/lang/String;
    :cond_1e
    add-int/2addr v4, v1

    .line 320
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 322
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_1f
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    if-eqz v5, :cond_22

    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    array-length v5, v5

    if-lez v5, :cond_22

    .line 323
    const/4 v0, 0x0

    .line 324
    .restart local v0    # "dataCount":I
    const/4 v1, 0x0

    .line 325
    .restart local v1    # "dataSize":I
    const/4 v3, 0x0

    .restart local v3    # "i":I
    :goto_4
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    array-length v5, v5

    if-ge v3, v5, :cond_21

    .line 326
    iget-object v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    aget-object v2, v5, v3

    .line 327
    .restart local v2    # "element":Ljava/lang/String;
    if-eqz v2, :cond_20

    .line 328
    add-int/lit8 v0, v0, 0x1

    .line 329
    invoke-static {v2}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSizeNoTag(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v1, v5

    .line 325
    :cond_20
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 333
    .end local v2    # "element":Ljava/lang/String;
    :cond_21
    add-int/2addr v4, v1

    .line 334
    mul-int/lit8 v5, v0, 0x1

    add-int/2addr v4, v5

    .line 336
    .end local v0    # "dataCount":I
    .end local v1    # "dataSize":I
    .end local v3    # "i":I
    :cond_22
    iget-boolean v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasMaxApkDownloadSizeMb:Z

    if-nez v5, :cond_23

    iget v5, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->maxApkDownloadSizeMb:I

    const/16 v6, 0x32

    if-eq v5, v6, :cond_24

    .line 337
    :cond_23
    const/16 v5, 0x11

    iget v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->maxApkDownloadSizeMb:I

    invoke-static {v5, v6}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v5

    add-int/2addr v4, v5

    .line 340
    :cond_24
    return v4
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DeviceConfigurationProto;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 348
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 349
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 353
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 354
    :sswitch_0
    return-object p0

    .line 359
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 360
    .local v4, "value":I
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 365
    :pswitch_0
    iput v4, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->touchScreen:I

    .line 366
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasTouchScreen:Z

    goto :goto_0

    .line 372
    .end local v4    # "value":I
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 373
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 378
    :pswitch_1
    iput v4, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->keyboard:I

    .line 379
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasKeyboard:Z

    goto :goto_0

    .line 385
    .end local v4    # "value":I
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 386
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_2

    goto :goto_0

    .line 392
    :pswitch_2
    iput v4, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->navigation:I

    .line 393
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasNavigation:Z

    goto :goto_0

    .line 399
    .end local v4    # "value":I
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v4

    .line 400
    .restart local v4    # "value":I
    packed-switch v4, :pswitch_data_3

    goto :goto_0

    .line 406
    :pswitch_3
    iput v4, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenLayout:I

    .line 407
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenLayout:Z

    goto :goto_0

    .line 413
    .end local v4    # "value":I
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHardKeyboard:Z

    .line 414
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasHardKeyboard:Z

    goto :goto_0

    .line 418
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBool()Z

    move-result v6

    iput-boolean v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasFiveWayNavigation:Z

    .line 419
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasFiveWayNavigation:Z

    goto :goto_0

    .line 423
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenDensity:I

    .line 424
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenDensity:Z

    goto :goto_0

    .line 428
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glEsVersion:I

    .line 429
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasGlEsVersion:Z

    goto :goto_0

    .line 433
    :sswitch_9
    const/16 v6, 0x4a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 435
    .local v0, "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    if-nez v6, :cond_2

    move v1, v5

    .line 436
    .local v1, "i":I
    :goto_1
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 437
    .local v2, "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 438
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 440
    :cond_1
    :goto_2
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_3

    .line 441
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 442
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 440
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 435
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_1

    .line 445
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 446
    iput-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    goto/16 :goto_0

    .line 450
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_a
    const/16 v6, 0x52

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 452
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    if-nez v6, :cond_5

    move v1, v5

    .line 453
    .restart local v1    # "i":I
    :goto_3
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 454
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 455
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 457
    :cond_4
    :goto_4
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_6

    .line 458
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 459
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 457
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 452
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_5
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_3

    .line 462
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 463
    iput-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    goto/16 :goto_0

    .line 467
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_b
    const/16 v6, 0x5a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 469
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    if-nez v6, :cond_8

    move v1, v5

    .line 470
    .restart local v1    # "i":I
    :goto_5
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 471
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_7

    .line 472
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 474
    :cond_7
    :goto_6
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_9

    .line 475
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 476
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 474
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 469
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_8
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_5

    .line 479
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 480
    iput-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    goto/16 :goto_0

    .line 484
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenWidth:I

    .line 485
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenWidth:Z

    goto/16 :goto_0

    .line 489
    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenHeight:I

    .line 490
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenHeight:Z

    goto/16 :goto_0

    .line 494
    :sswitch_e
    const/16 v6, 0x72

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 496
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    if-nez v6, :cond_b

    move v1, v5

    .line 497
    .restart local v1    # "i":I
    :goto_7
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 498
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_a

    .line 499
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 501
    :cond_a
    :goto_8
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_c

    .line 502
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 503
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 501
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 496
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_b
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_7

    .line 506
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 507
    iput-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    goto/16 :goto_0

    .line 511
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_f
    const/16 v6, 0x7a

    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 513
    .restart local v0    # "arrayLength":I
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    if-nez v6, :cond_e

    move v1, v5

    .line 514
    .restart local v1    # "i":I
    :goto_9
    add-int v6, v1, v0

    new-array v2, v6, [Ljava/lang/String;

    .line 515
    .restart local v2    # "newArray":[Ljava/lang/String;
    if-eqz v1, :cond_d

    .line 516
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    invoke-static {v6, v5, v2, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 518
    :cond_d
    :goto_a
    array-length v6, v2

    add-int/lit8 v6, v6, -0x1

    if-ge v1, v6, :cond_f

    .line 519
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 520
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 518
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 513
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :cond_e
    iget-object v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    array-length v1, v6

    goto :goto_9

    .line 523
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Ljava/lang/String;
    :cond_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v1

    .line 524
    iput-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    goto/16 :goto_0

    .line 528
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Ljava/lang/String;
    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v6

    iput v6, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->maxApkDownloadSizeMb:I

    .line 529
    iput-boolean v7, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasMaxApkDownloadSizeMb:Z

    goto/16 :goto_0

    .line 349
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x88 -> :sswitch_10
    .end sparse-switch

    .line 360
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 373
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    .line 386
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    .line 400
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/DeviceConfigurationProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 147
    iget v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->touchScreen:I

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasTouchScreen:Z

    if-eqz v2, :cond_1

    .line 148
    :cond_0
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->touchScreen:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 150
    :cond_1
    iget v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->keyboard:I

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasKeyboard:Z

    if-eqz v2, :cond_3

    .line 151
    :cond_2
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->keyboard:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 153
    :cond_3
    iget v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->navigation:I

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasNavigation:Z

    if-eqz v2, :cond_5

    .line 154
    :cond_4
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->navigation:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 156
    :cond_5
    iget v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenLayout:I

    if-nez v2, :cond_6

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenLayout:Z

    if-eqz v2, :cond_7

    .line 157
    :cond_6
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenLayout:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 159
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasHardKeyboard:Z

    if-nez v2, :cond_8

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHardKeyboard:Z

    if-eqz v2, :cond_9

    .line 160
    :cond_8
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHardKeyboard:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 162
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasHasFiveWayNavigation:Z

    if-nez v2, :cond_a

    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasFiveWayNavigation:Z

    if-eqz v2, :cond_b

    .line 163
    :cond_a
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasFiveWayNavigation:Z

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBool(IZ)V

    .line 165
    :cond_b
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenDensity:Z

    if-nez v2, :cond_c

    iget v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenDensity:I

    if-eqz v2, :cond_d

    .line 166
    :cond_c
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenDensity:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 168
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasGlEsVersion:Z

    if-nez v2, :cond_e

    iget v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glEsVersion:I

    if-eqz v2, :cond_f

    .line 169
    :cond_e
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glEsVersion:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 171
    :cond_f
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_11

    .line 172
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_11

    .line 173
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSharedLibrary:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 174
    .local v0, "element":Ljava/lang/String;
    if-eqz v0, :cond_10

    .line 175
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 172
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 179
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_11
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_13

    .line 180
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_13

    .line 181
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemAvailableFeature:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 182
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_12

    .line 183
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 180
    :cond_12
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 187
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_13
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_15

    .line 188
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    .line 189
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->nativePlatform:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 190
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_14

    .line 191
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 188
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 195
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_15
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenWidth:Z

    if-nez v2, :cond_16

    iget v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenWidth:I

    if-eqz v2, :cond_17

    .line 196
    :cond_16
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenWidth:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 198
    :cond_17
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasScreenHeight:Z

    if-nez v2, :cond_18

    iget v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenHeight:I

    if-eqz v2, :cond_19

    .line 199
    :cond_18
    const/16 v2, 0xd

    iget v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->screenHeight:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 201
    :cond_19
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1b

    .line 202
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1b

    .line 203
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->systemSupportedLocale:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 204
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_1a

    .line 205
    const/16 v2, 0xe

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 202
    :cond_1a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 209
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1b
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    if-eqz v2, :cond_1d

    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_1d

    .line 210
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_4
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1d

    .line 211
    iget-object v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->glExtension:[Ljava/lang/String;

    aget-object v0, v2, v1

    .line 212
    .restart local v0    # "element":Ljava/lang/String;
    if-eqz v0, :cond_1c

    .line 213
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 210
    :cond_1c
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 217
    .end local v0    # "element":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1d
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->hasMaxApkDownloadSizeMb:Z

    if-nez v2, :cond_1e

    iget v2, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->maxApkDownloadSizeMb:I

    const/16 v3, 0x32

    if-eq v2, v3, :cond_1f

    .line 218
    :cond_1e
    const/16 v2, 0x11

    iget v3, p0, Lcom/google/android/finsky/protos/DeviceConfigurationProto;->maxApkDownloadSizeMb:I

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 220
    :cond_1f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 221
    return-void
.end method
