.class public final Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;
.super Lcom/google/protobuf/nano/MessageNano;
.source "VendingProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/finsky/protos/VendingProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PendingNotificationsProto"
.end annotation


# instance fields
.field public finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

.field public hasNextCheckMillis:Z

.field public nextCheckMillis:J

.field public notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4597
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 4598
    invoke-virtual {p0}, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->clear()Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    .line 4599
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;
    .locals 2

    .prologue
    .line 4602
    invoke-static {}, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;->emptyArray()[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    .line 4603
    invoke-static {}, Lcom/google/android/finsky/protos/Notifications$Notification;->emptyArray()[Lcom/google/android/finsky/protos/Notifications$Notification;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    .line 4604
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->nextCheckMillis:J

    .line 4605
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->hasNextCheckMillis:Z

    .line 4606
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->cachedSize:I

    .line 4607
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 8

    .prologue
    .line 4637
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 4638
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    array-length v3, v3

    if-lez v3, :cond_1

    .line 4639
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 4640
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    aget-object v0, v3, v1

    .line 4641
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    if-eqz v0, :cond_0

    .line 4642
    const/4 v3, 0x1

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4639
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4647
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->hasNextCheckMillis:Z

    if-nez v3, :cond_2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->nextCheckMillis:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 4648
    :cond_2
    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->nextCheckMillis:J

    invoke-static {v3, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt64Size(IJ)I

    move-result v3

    add-int/2addr v2, v3

    .line 4651
    :cond_3
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 4652
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 4653
    iget-object v3, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    aget-object v0, v3, v1

    .line 4654
    .local v0, "element":Lcom/google/android/finsky/protos/Notifications$Notification;
    if-eqz v0, :cond_4

    .line 4655
    const/4 v3, 0x3

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4652
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4660
    .end local v0    # "element":Lcom/google/android/finsky/protos/Notifications$Notification;
    .end local v1    # "i":I
    :cond_5
    return v2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;
    .locals 8
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 4668
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 4669
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 4673
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4674
    :sswitch_0
    return-object p0

    .line 4679
    :sswitch_1
    const/16 v5, 0xa

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4681
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    if-nez v5, :cond_2

    move v1, v4

    .line 4682
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    .line 4684
    .local v2, "newArray":[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    if-eqz v1, :cond_1

    .line 4685
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4687
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 4688
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;-><init>()V

    aput-object v5, v2, v1

    .line 4689
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4690
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4687
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4681
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    :cond_2
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    array-length v1, v5

    goto :goto_1

    .line 4693
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    :cond_3
    new-instance v5, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;-><init>()V

    aput-object v5, v2, v1

    .line 4694
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4695
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    goto :goto_0

    .line 4699
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt64()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->nextCheckMillis:J

    .line 4700
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->hasNextCheckMillis:Z

    goto :goto_0

    .line 4704
    :sswitch_3
    const/16 v5, 0x1a

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 4706
    .restart local v0    # "arrayLength":I
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    if-nez v5, :cond_5

    move v1, v4

    .line 4707
    .restart local v1    # "i":I
    :goto_3
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/finsky/protos/Notifications$Notification;

    .line 4709
    .local v2, "newArray":[Lcom/google/android/finsky/protos/Notifications$Notification;
    if-eqz v1, :cond_4

    .line 4710
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 4712
    :cond_4
    :goto_4
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_6

    .line 4713
    new-instance v5, Lcom/google/android/finsky/protos/Notifications$Notification;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Notifications$Notification;-><init>()V

    aput-object v5, v2, v1

    .line 4714
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4715
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 4712
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 4706
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/finsky/protos/Notifications$Notification;
    :cond_5
    iget-object v5, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v1, v5

    goto :goto_3

    .line 4718
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/finsky/protos/Notifications$Notification;
    :cond_6
    new-instance v5, Lcom/google/android/finsky/protos/Notifications$Notification;

    invoke-direct {v5}, Lcom/google/android/finsky/protos/Notifications$Notification;-><init>()V

    aput-object v5, v2, v1

    .line 4719
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 4720
    iput-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    goto/16 :goto_0

    .line 4669
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4570
    invoke-virtual {p0, p1}, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 6
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 4613
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    array-length v2, v2

    if-lez v2, :cond_1

    .line 4614
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    .line 4615
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->notification:[Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;

    aget-object v0, v2, v1

    .line 4616
    .local v0, "element":Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    if-eqz v0, :cond_0

    .line 4617
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4614
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4621
    .end local v0    # "element":Lcom/google/android/finsky/protos/VendingProtos$DataMessageProto;
    .end local v1    # "i":I
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->hasNextCheckMillis:Z

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->nextCheckMillis:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 4622
    :cond_2
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->nextCheckMillis:J

    invoke-virtual {p1, v2, v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt64(IJ)V

    .line 4624
    :cond_3
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 4625
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 4626
    iget-object v2, p0, Lcom/google/android/finsky/protos/VendingProtos$PendingNotificationsProto;->finskyNotification:[Lcom/google/android/finsky/protos/Notifications$Notification;

    aget-object v0, v2, v1

    .line 4627
    .local v0, "element":Lcom/google/android/finsky/protos/Notifications$Notification;
    if-eqz v0, :cond_4

    .line 4628
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 4625
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4632
    .end local v0    # "element":Lcom/google/android/finsky/protos/Notifications$Notification;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 4633
    return-void
.end method
