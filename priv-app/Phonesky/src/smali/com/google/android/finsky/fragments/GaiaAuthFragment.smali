.class public Lcom/google/android/finsky/fragments/GaiaAuthFragment;
.super Landroid/support/v4/app/Fragment;
.source "GaiaAuthFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/finsky/activities/SimpleAlertDialog$Listener;
.implements Lcom/google/android/finsky/auth/AuthResponseListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;
    }
.end annotation


# instance fields
.field private mAccountName:Ljava/lang/String;

.field private mAuthRequest:Lcom/android/volley/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/volley/Request",
            "<*>;"
        }
    .end annotation
.end field

.field private mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

.field private mFailedCount:I

.field private mInput:Landroid/widget/EditText;

.field private mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

.field private mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

.field private mShowWarning:Z

.field private mUseGmsCoreForAuth:Z

.field private mUsePinBasedAuth:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 71
    iput-object v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthRequest:Lcom/android/volley/Request;

    .line 81
    new-instance v0, Lcom/google/android/finsky/layout/play/GenericUiElementNode;

    const/16 v1, 0x13a

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/android/finsky/layout/play/GenericUiElementNode;-><init>(I[BLcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    iput-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    .line 368
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/finsky/fragments/GaiaAuthFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/fragments/GaiaAuthFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->logClickAndVerifyInput(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->success()V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failure()V

    return-void
.end method

.method private failWithMaxAttemptsExceeded()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 347
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getRecoveryUrl()Ljava/lang/String;

    move-result-object v3

    .line 348
    .local v3, "recoveryUrl":Ljava/lang/String;
    iget-boolean v4, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUsePinBasedAuth:Z

    if-eqz v4, :cond_0

    const v4, 0x7f0c00fc

    :goto_0
    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 351
    .local v2, "maxAttemptsExceededHtml":Ljava/lang/String;
    new-instance v0, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    invoke-direct {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;-><init>()V

    .line 352
    .local v0, "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    invoke-virtual {v0, v2}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setMessageHtml(Ljava/lang/String;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0c02a0

    invoke-virtual {v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setPositiveId(I)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, p0, v7, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->setCallback(Landroid/support/v4/app/Fragment;ILandroid/os/Bundle;)Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;

    .line 355
    invoke-virtual {v0}, Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;->build()Lcom/google/android/finsky/activities/SimpleAlertDialog;

    move-result-object v1

    .line 356
    .local v1, "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "GaiaAuthFragment.errorDialog"

    invoke-virtual {v1, v4, v5}, Lcom/google/android/finsky/activities/SimpleAlertDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 357
    return-void

    .line 348
    .end local v0    # "builder":Lcom/google/android/finsky/activities/SimpleAlertDialog$Builder;
    .end local v1    # "dialog":Lcom/google/android/finsky/activities/SimpleAlertDialog;
    .end local v2    # "maxAttemptsExceededHtml":Ljava/lang/String;
    :cond_0
    const v4, 0x7f0c00fb

    goto :goto_0
.end method

.method private failure()V
    .locals 1

    .prologue
    .line 327
    const v0, 0x7f0c00db

    invoke-direct {p0, v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failure(I)V

    .line 328
    return-void
.end method

.method private failure(I)V
    .locals 3
    .param p1, "errorStringId"    # I

    .prologue
    .line 331
    iget v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mFailedCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mFailedCount:I

    .line 332
    const/16 v1, 0x1f6

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->logBackgroundEvent(IZ)V

    .line 333
    iget v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mFailedCount:I

    sget-object v1, Lcom/google/android/finsky/config/G;->passwordMaxFailedAttempts:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lt v2, v1, :cond_0

    .line 334
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failWithMaxAttemptsExceeded()V

    .line 344
    :goto_0
    return-void

    .line 337
    :cond_0
    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 338
    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 339
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-static {v1, v2}, Lcom/google/android/finsky/utils/UiUtils;->showKeyboard(Landroid/app/Activity;Landroid/widget/EditText;)V

    .line 340
    iget-boolean v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUsePinBasedAuth:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0c010e

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 343
    .local v0, "textViewLabel":Ljava/lang/String;
    :goto_1
    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {p0, p1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/finsky/utils/UiUtils;->setErrorOnTextView(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 340
    .end local v0    # "textViewLabel":Ljava/lang/String;
    :cond_1
    const v1, 0x7f0c010d

    invoke-virtual {p0, v1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private getAuthContext()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 425
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;-><init>()V

    .line 426
    .local v0, "authContext":Lcom/google/android/finsky/analytics/PlayStore$AuthContext;
    iget-boolean v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUsePinBasedAuth:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    :goto_0
    iput v1, v0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->mode:I

    .line 427
    iput-boolean v2, v0, Lcom/google/android/finsky/analytics/PlayStore$AuthContext;->hasMode:Z

    .line 428
    return-object v0

    :cond_0
    move v1, v2

    .line 426
    goto :goto_0
.end method

.method private getRecoveryUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 189
    iget-boolean v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUsePinBasedAuth:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/finsky/config/G;->resetPinUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v0, v1

    .line 190
    .local v0, "recoveryUrl":Ljava/lang/String;
    :goto_0
    const-string v1, "%email%"

    iget-object v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 189
    .end local v0    # "recoveryUrl":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/google/android/finsky/config/G;->passwordRecoveryUrl:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v1}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v0, v1

    goto :goto_0
.end method

.method private logBackgroundEvent(IZ)V
    .locals 3
    .param p1, "eventType"    # I
    .param p2, "operationSuccess"    # Z

    .prologue
    .line 408
    new-instance v1, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    invoke-direct {v1, p1}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;-><init>(I)V

    invoke-virtual {v1, p2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setOperationSuccess(Z)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getAuthContext()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->setAuthContext(Lcom/google/android/finsky/analytics/PlayStore$AuthContext;)Lcom/google/android/finsky/analytics/BackgroundEventBuilder;

    move-result-object v0

    .line 411
    .local v0, "builder":Lcom/google/android/finsky/analytics/BackgroundEventBuilder;
    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    invoke-virtual {v0}, Lcom/google/android/finsky/analytics/BackgroundEventBuilder;->build()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logBackgroundEvent(Lcom/google/android/finsky/analytics/PlayStore$PlayStoreBackgroundActionEvent;)V

    .line 412
    return-void
.end method

.method private logClick(IZ)V
    .locals 3
    .param p1, "leafType"    # I
    .param p2, "isImeAction"    # Z

    .prologue
    const/4 v1, 0x1

    .line 415
    new-instance v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {v0}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    .line 416
    .local v0, "clientLogsCookie":Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;
    if-eqz p2, :cond_0

    .line 417
    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->isImeAction:Z

    .line 418
    iput-boolean v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->hasIsImeAction:Z

    .line 420
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getAuthContext()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 421
    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    iget-object v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logClickEventWithClientCookie(ILcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 422
    return-void
.end method

.method private logClickAndVerifyInput(Z)V
    .locals 3
    .param p1, "isImeAction"    # Z

    .prologue
    .line 214
    const/16 v1, 0x109

    invoke-direct {p0, v1, p1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->logClick(IZ)V

    .line 216
    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "input":Ljava/lang/String;
    iget-object v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 218
    invoke-direct {p0, v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->verifyGaia(Ljava/lang/String;)V

    .line 219
    return-void
.end method

.method public static newInstance(Ljava/lang/String;ZZZ)Lcom/google/android/finsky/fragments/GaiaAuthFragment;
    .locals 3
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "showWarning"    # Z
    .param p2, "useGmsCoreForAuth"    # Z
    .param p3, "usePinBasedAuth"    # Z

    .prologue
    .line 87
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    .local v0, "arguments":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v2, "GaiaAuthFragment_useGmsCoreForAuth"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 90
    const-string v2, "GaiaAuthFragment_usePinBasedAuth"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 91
    const-string v2, "GaiaAuthFragment_showWarning"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 92
    new-instance v1, Lcom/google/android/finsky/fragments/GaiaAuthFragment;

    invoke-direct {v1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;-><init>()V

    .line 93
    .local v1, "fragment":Lcom/google/android/finsky/fragments/GaiaAuthFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->setArguments(Landroid/os/Bundle;)V

    .line 94
    return-object v1
.end method

.method private success()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 360
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 361
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 362
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    if-eqz v0, :cond_0

    .line 363
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mFailedCount:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;->onSuccess(II)V

    .line 365
    :cond_0
    const/16 v0, 0x1f6

    invoke-direct {p0, v0, v3}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->logBackgroundEvent(IZ)V

    .line 366
    return-void
.end method

.method private verifyGaia(Ljava/lang/String;)V
    .locals 3
    .param p1, "userInput"    # Ljava/lang/String;

    .prologue
    .line 230
    iget-boolean v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUseGmsCoreForAuth:Z

    if-eqz v2, :cond_1

    .line 231
    new-instance v0, Lcom/google/android/finsky/auth/GmsCoreAuthApi;

    invoke-direct {v0}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;-><init>()V

    .line 232
    .local v0, "authApi":Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    iget-boolean v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUsePinBasedAuth:Z

    if-eqz v2, :cond_0

    .line 233
    iget-object v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v2, p1, p0}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->validateUserPin(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V

    .line 241
    .end local v0    # "authApi":Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    :goto_0
    return-void

    .line 235
    .restart local v0    # "authApi":Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    :cond_0
    iget-object v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {v0, v2, p1, p0}, Lcom/google/android/finsky/auth/GmsCoreAuthApi;->validateUserPassword(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)V

    goto :goto_0

    .line 238
    .end local v0    # "authApi":Lcom/google/android/finsky/auth/GmsCoreAuthApi;
    :cond_1
    new-instance v1, Lcom/google/android/finsky/billing/challenge/ClientLoginApi;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/finsky/FinskyApp;->getRequestQueue()Lcom/android/volley/RequestQueue;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/finsky/billing/challenge/ClientLoginApi;-><init>(Lcom/android/volley/RequestQueue;)V

    .line 239
    .local v1, "clientLoginApi":Lcom/google/android/finsky/billing/challenge/ClientLoginApi;
    iget-object v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {v1, v2, p1, p0}, Lcom/google/android/finsky/billing/challenge/ClientLoginApi;->validateUser(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/finsky/auth/AuthResponseListener;)Lcom/android/volley/Request;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthRequest:Lcom/android/volley/Request;

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 375
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 376
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 377
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->success()V

    .line 384
    :goto_0
    return-void

    .line 379
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failure()V

    goto :goto_0

    .line 382
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAuthFailure(I)V
    .locals 7
    .param p1, "err"    # I

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->isResumed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 255
    const-string v3, "Not resumed, ignoring auth challenge failure."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    :goto_0
    return-void

    .line 259
    :cond_0
    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    const/4 v3, 0x6

    if-ne p1, v3, :cond_2

    .line 264
    :cond_1
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->success()V

    goto :goto_0

    .line 268
    :cond_2
    const/4 v3, 0x4

    if-ne p1, v3, :cond_4

    .line 269
    iget-boolean v3, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUsePinBasedAuth:Z

    if-eqz v3, :cond_3

    const v3, 0x7f0c00f7

    :goto_1
    invoke-direct {p0, v3}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failure(I)V

    goto :goto_0

    :cond_3
    const v3, 0x7f0c00f8

    goto :goto_1

    .line 273
    :cond_4
    const/4 v3, 0x3

    if-eq p1, v3, :cond_5

    .line 274
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->failure()V

    goto :goto_0

    .line 279
    :cond_5
    iget-object v3, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/finsky/api/AccountHandler;->findAccount(Ljava/lang/String;Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v1

    .line 280
    .local v1, "account":Landroid/accounts/Account;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 281
    .local v0, "accountManager":Landroid/accounts/AccountManager;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 282
    .local v2, "options":Landroid/os/Bundle;
    iget-object v3, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 283
    .local v6, "password":Ljava/lang/String;
    const-string v3, "password"

    invoke-virtual {v2, v3, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v4, Lcom/google/android/finsky/fragments/GaiaAuthFragment$2;

    invoke-direct {v4, p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$2;-><init>(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)V

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/accounts/AccountManager;->confirmCredentials(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method public onAuthSuccess()V
    .locals 2

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 246
    const-string v0, "Not resumed, ignoring auth challenge success."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/finsky/utils/FinskyLog;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 250
    :goto_0
    return-void

    .line 249
    :cond_0
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->success()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 195
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 197
    :sswitch_0
    invoke-direct {p0, v1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->logClickAndVerifyInput(Z)V

    goto :goto_0

    .line 200
    :sswitch_1
    const/16 v0, 0x10a

    invoke-direct {p0, v0, v1}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->logClick(IZ)V

    .line 201
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    invoke-interface {v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;->onCancel()V

    goto :goto_0

    .line 195
    :sswitch_data_0
    .sparse-switch
        0x7f0a00f8 -> :sswitch_0
        0x7f0a0115 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 99
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    .line 101
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GaiaAuthFragment_useGmsCoreForAuth"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUseGmsCoreForAuth:Z

    .line 102
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GaiaAuthFragment_usePinBasedAuth"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUsePinBasedAuth:Z

    .line 103
    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GaiaAuthFragment_showWarning"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mShowWarning:Z

    .line 104
    if-eqz p1, :cond_0

    .line 105
    const-string v0, "GaiaAuthFragment_retryCount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mFailedCount:I

    .line 107
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 13
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 118
    const v9, 0x7f0400a8

    const/4 v10, 0x0

    invoke-virtual {p1, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 120
    .local v8, "result":Landroid/view/View;
    invoke-static {}, Lcom/google/android/finsky/FinskyApp;->get()Lcom/google/android/finsky/FinskyApp;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/google/android/finsky/FinskyApp;->getEventLogger(Ljava/lang/String;)Lcom/google/android/finsky/analytics/FinskyEventLog;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    .line 121
    if-nez p3, :cond_0

    .line 123
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {v9}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v9

    new-instance v10, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {v10}, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;-><init>()V

    iput-object v10, v9, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    .line 124
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-interface {v9}, Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;->getPlayStoreUiElement()Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;

    move-result-object v9

    iget-object v9, v9, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElement;->clientLogsCookie:Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;

    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getAuthContext()Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    move-result-object v10

    iput-object v10, v9, Lcom/google/android/finsky/analytics/PlayStore$PlayStoreUiElementInfo;->authContext:Lcom/google/android/finsky/analytics/PlayStore$AuthContext;

    .line 125
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mEventLogger:Lcom/google/android/finsky/analytics/FinskyEventLog;

    const-wide/16 v10, 0x0

    iget-object v12, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mNode:Lcom/google/android/finsky/layout/play/PlayStoreUiElementNode;

    invoke-virtual {v9, v10, v11, v12}, Lcom/google/android/finsky/analytics/FinskyEventLog;->logPathImpression(JLcom/google/android/finsky/layout/play/PlayStoreUiElementNode;)V

    .line 128
    :cond_0
    const v9, 0x7f0a00f2

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 129
    .local v1, "descriptionView":Landroid/widget/TextView;
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAccountName:Ljava/lang/String;

    invoke-virtual {v1, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    const v9, 0x7f0a020c

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/EditText;

    iput-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    .line 132
    iget-boolean v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUsePinBasedAuth:Z

    if-eqz v9, :cond_2

    .line 133
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const v10, 0x7f0c010e

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setHint(I)V

    .line 134
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c010e

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const/16 v10, 0x12

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setInputType(I)V

    .line 144
    :goto_0
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setVisibility(I)V

    .line 145
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    new-instance v10, Lcom/google/android/finsky/fragments/GaiaAuthFragment$1;

    invoke-direct {v10, p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$1;-><init>(Lcom/google/android/finsky/fragments/GaiaAuthFragment;)V

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 156
    const v9, 0x7f0a020e

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 157
    .local v2, "footer":Landroid/widget/TextView;
    invoke-direct {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getRecoveryUrl()Ljava/lang/String;

    move-result-object v7

    .line 158
    .local v7, "recoveryUrl":Ljava/lang/String;
    if-eqz v7, :cond_4

    .line 159
    iget-boolean v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mUsePinBasedAuth:Z

    if-eqz v9, :cond_3

    const v9, 0x7f0c02d1

    :goto_1
    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v7, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 162
    .local v6, "recoveryMessageHtml":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v9

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 164
    const/4 v9, 0x0

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    .end local v6    # "recoveryMessageHtml":Ljava/lang/String;
    :goto_2
    iget-boolean v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mShowWarning:Z

    if-eqz v9, :cond_1

    .line 170
    const v9, 0x7f0a020d

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 171
    .local v4, "optOutInfo":Landroid/widget/TextView;
    const v9, 0x7f0c02d3

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    sget-object v12, Lcom/google/android/finsky/config/G;->gaiaOptOutLearnMoreLink:Lcom/google/android/play/utils/config/GservicesValue;

    invoke-virtual {v12}, Lcom/google/android/play/utils/config/GservicesValue;->get()Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 173
    .local v5, "optOutString":Ljava/lang/String;
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 175
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 178
    .end local v4    # "optOutInfo":Landroid/widget/TextView;
    .end local v5    # "optOutString":Ljava/lang/String;
    :cond_1
    const v9, 0x7f0a00f8

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 179
    .local v3, "okButton":Landroid/widget/Button;
    const v9, 0x7f0c02a0

    invoke-virtual {v3, v9}, Landroid/widget/Button;->setText(I)V

    .line 180
    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    const v9, 0x7f0a0115

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 183
    .local v0, "cancelButton":Landroid/widget/Button;
    const v9, 0x7f0c0134

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setText(I)V

    .line 184
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    return-object v8

    .line 138
    .end local v0    # "cancelButton":Landroid/widget/Button;
    .end local v2    # "footer":Landroid/widget/TextView;
    .end local v3    # "okButton":Landroid/widget/Button;
    .end local v7    # "recoveryUrl":Ljava/lang/String;
    :cond_2
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const v10, 0x7f0c010d

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setHint(I)V

    .line 139
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c010d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 140
    iget-object v9, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mInput:Landroid/widget/EditText;

    const/16 v10, 0x81

    invoke-virtual {v9, v10}, Landroid/widget/EditText;->setInputType(I)V

    goto/16 :goto_0

    .line 159
    .restart local v2    # "footer":Landroid/widget/TextView;
    .restart local v7    # "recoveryUrl":Ljava/lang/String;
    :cond_3
    const v9, 0x7f0c02d0

    goto/16 :goto_1

    .line 166
    :cond_4
    const/16 v9, 0x8

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthRequest:Lcom/android/volley/Request;

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mAuthRequest:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->cancel()V

    .line 323
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 324
    return-void
.end method

.method public onNegativeClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 402
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    invoke-interface {v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;->onCancel()V

    .line 405
    :cond_0
    return-void
.end method

.method public onPositiveClick(ILandroid/os/Bundle;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "extraArguments"    # Landroid/os/Bundle;

    .prologue
    .line 389
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    invoke-interface {v0}, Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;->onCancel()V

    .line 392
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 111
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    const-string v0, "GaiaAuthFragment_retryCount"

    iget v1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mFailedCount:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 113
    return-void
.end method

.method public setListener(Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/finsky/fragments/GaiaAuthFragment;->mListener:Lcom/google/android/finsky/fragments/GaiaAuthFragment$Listener;

    .line 210
    return-void
.end method
