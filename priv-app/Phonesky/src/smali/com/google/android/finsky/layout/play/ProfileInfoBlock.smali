.class public Lcom/google/android/finsky/layout/play/ProfileInfoBlock;
.super Landroid/widget/LinearLayout;
.source "ProfileInfoBlock.java"


# instance fields
.field private final mActionButtonArea:Landroid/graphics/Rect;

.field private final mOldActionButtonArea:Landroid/graphics/Rect;

.field private mProfileActionButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mOldActionButtonArea:Landroid/graphics/Rect;

    .line 38
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 44
    const v0, 0x7f0a02be

    invoke-virtual {p0, v0}, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mProfileActionButton:Landroid/view/View;

    .line 45
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 50
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 54
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mProfileActionButton:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    div-int/lit8 v0, v2, 0x2

    .line 55
    .local v0, "heightExtension":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mProfileActionButton:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v1, v2, 0x3

    .line 56
    .local v1, "widthExtension":I
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mProfileActionButton:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 57
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->top:I

    .line 58
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v0

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    .line 59
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 60
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 61
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mOldActionButtonArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mOldActionButtonArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mOldActionButtonArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mOldActionButtonArea:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    if-ne v2, v3, :cond_0

    .line 74
    :goto_0
    return-void

    .line 71
    :cond_0
    new-instance v2, Lcom/google/android/play/utils/PlayTouchDelegate;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mProfileActionButton:Landroid/view/View;

    invoke-direct {v2, v3, v4}, Lcom/google/android/play/utils/PlayTouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v2}, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 72
    iget-object v2, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mOldActionButtonArea:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/finsky/layout/play/ProfileInfoBlock;->mActionButtonArea:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0
.end method
